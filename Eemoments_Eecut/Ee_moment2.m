-2*m2^2*(3 + 2*m2)*Log[m2] + 2*m2^2*(3 + 2*m2)*Log[1 - Ycut] + 
 (rhoLS*(((-1 + m2 + Ycut)*(-8 + 46*m2 - 23*m2^2 + 73*m2^3 - 35*m2^4 + 
        7*m2^5 + 24*Ycut - 146*m2*Ycut + 71*m2^2*Ycut - 264*m2^3*Ycut + 
        133*m2^4*Ycut - 28*m2^5*Ycut - 24*Ycut^2 + 154*m2*Ycut^2 - 
        63*m2^2*Ycut^2 + 333*m2^3*Ycut^2 - 182*m2^4*Ycut^2 + 42*m2^5*Ycut^2 + 
        8*Ycut^3 - 54*m2*Ycut^3 + 3*m2^2*Ycut^3 - 152*m2^3*Ycut^3 + 
        98*m2^4*Ycut^3 - 28*m2^5*Ycut^3 + 9*m2^2*Ycut^4 + 3*m2^3*Ycut^4 - 
        7*m2^4*Ycut^4 + 7*m2^5*Ycut^4 + 45*m2^2*Ycut^5 - 7*m2^4*Ycut^5 - 
        Ycut^6 - m2*Ycut^6 - 25*m2^2*Ycut^6 + 7*m2^3*Ycut^6 + 3*Ycut^7 + 
        2*m2*Ycut^7 + m2^2*Ycut^7 - 3*Ycut^8 - m2*Ycut^8 + Ycut^9))/
      (36*(-1 + Ycut)^4) - (m2^2*(3 + 2*m2)*Log[m2])/3 + 
     (m2^2*(3 + 2*m2)*Log[1 - Ycut])/3 + 
     c[VR]^2*(((-1 + m2 + Ycut)*(-10 + 62*m2 - 23*m2^2 + 57*m2^3 - 33*m2^4 + 
          7*m2^5 + 10*Ycut - 72*m2*Ycut + 15*m2^2*Ycut - 88*m2^3*Ycut + 
          59*m2^4*Ycut - 14*m2^5*Ycut + 20*m2^2*Ycut^2 + 12*m2^3*Ycut^2 - 
          19*m2^4*Ycut^2 + 7*m2^5*Ycut^2 + 12*m2^3*Ycut^3 - 7*m2^4*Ycut^3 - 
          5*m2^2*Ycut^4 + 7*m2^3*Ycut^4 - 7*m2^2*Ycut^5 - 2*Ycut^6 - 
          2*m2*Ycut^6 + 2*Ycut^7))/(24*(-1 + Ycut)^2) - (5*m2^2*Log[m2])/2 + 
       (5*m2^2*Log[1 - Ycut])/2) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(-8 + 46*m2 - 23*m2^2 + 73*m2^3 - 35*m2^4 + 
          7*m2^5 + 24*Ycut - 146*m2*Ycut + 71*m2^2*Ycut - 264*m2^3*Ycut + 
          133*m2^4*Ycut - 28*m2^5*Ycut - 24*Ycut^2 + 154*m2*Ycut^2 - 
          63*m2^2*Ycut^2 + 333*m2^3*Ycut^2 - 182*m2^4*Ycut^2 + 
          42*m2^5*Ycut^2 + 8*Ycut^3 - 54*m2*Ycut^3 + 3*m2^2*Ycut^3 - 
          152*m2^3*Ycut^3 + 98*m2^4*Ycut^3 - 28*m2^5*Ycut^3 + 9*m2^2*Ycut^4 + 
          3*m2^3*Ycut^4 - 7*m2^4*Ycut^4 + 7*m2^5*Ycut^4 + 45*m2^2*Ycut^5 - 
          7*m2^4*Ycut^5 - Ycut^6 - m2*Ycut^6 - 25*m2^2*Ycut^6 + 
          7*m2^3*Ycut^6 + 3*Ycut^7 + 2*m2*Ycut^7 + m2^2*Ycut^7 - 3*Ycut^8 - 
          m2*Ycut^8 + Ycut^9))/(36*(-1 + Ycut)^4) - (m2^2*(3 + 2*m2)*Log[m2])/
        3 + (m2^2*(3 + 2*m2)*Log[1 - Ycut])/3) + 
     c[SR]*c[T]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*(37 + 29*m2 - 7*m2^2 + 
           m2^3 - 49*Ycut - 52*m2*Ycut + 13*m2^2*Ycut - 2*m2^3*Ycut + 
           6*Ycut^2 + 18*m2*Ycut^2 - 5*m2^2*Ycut^2 + m2^3*Ycut^2 + 2*Ycut^3 + 
           4*m2*Ycut^3 - m2^2*Ycut^3 + Ycut^4 + m2*Ycut^4 - 3*Ycut^5))/
         (-1 + Ycut)^2 + 4*Sqrt[m2]*(1 + 4*m2)*Log[m2] - 
       4*Sqrt[m2]*(1 + 4*m2)*Log[1 - Ycut]) + 
     c[T]^2*(((-1 + m2 + Ycut)*(-74 + 112*m2 + 124*m2^2 + 172*m2^3 - 
          122*m2^4 + 28*m2^5 + 222*Ycut - 266*m2*Ycut - 550*m2^2*Ycut - 
          594*m2^3*Ycut + 460*m2^4*Ycut - 112*m2^5*Ycut - 222*Ycut^2 + 
          124*m2*Ycut^2 + 906*m2^2*Ycut^2 + 684*m2^3*Ycut^2 - 
          620*m2^4*Ycut^2 + 168*m2^5*Ycut^2 + 74*Ycut^3 + 78*m2*Ycut^3 - 
          624*m2^2*Ycut^3 - 236*m2^3*Ycut^3 + 320*m2^4*Ycut^3 - 
          112*m2^5*Ycut^3 - 36*m2*Ycut^4 + 102*m2^2*Ycut^4 - 36*m2^3*Ycut^4 - 
          10*m2^4*Ycut^4 + 28*m2^5*Ycut^4 + 36*m2*Ycut^5 + 66*m2^2*Ycut^5 - 
          18*m2^3*Ycut^5 - 28*m2^4*Ycut^5 - 7*Ycut^6 - 55*m2*Ycut^6 + 
          14*m2^2*Ycut^6 + 28*m2^3*Ycut^6 + 21*Ycut^7 + 14*m2*Ycut^7 - 
          20*m2^2*Ycut^7 - 21*Ycut^8 - 7*m2*Ycut^8 + 7*Ycut^9))/
        (9*(-1 + Ycut)^4) - (8*m2*(-6 + 15*m2 + m2^2)*Log[m2])/3 + 
       (8*m2*(-6 + 15*m2 + m2^2)*Log[1 - Ycut])/3) + 
     c[SL]^2*(((-1 + m2 + Ycut)*(28 + 166*m2 - 239*m2^2 + 145*m2^3 - 
          47*m2^4 + 7*m2^5 - 84*Ycut - 614*m2*Ycut + 875*m2^2*Ycut - 
          540*m2^3*Ycut + 181*m2^4*Ycut - 28*m2^5*Ycut + 84*Ycut^2 + 
          802*m2*Ycut^2 - 1131*m2^2*Ycut^2 + 717*m2^3*Ycut^2 - 
          254*m2^4*Ycut^2 + 42*m2^5*Ycut^2 - 28*Ycut^3 - 402*m2*Ycut^3 + 
          555*m2^2*Ycut^3 - 368*m2^3*Ycut^3 + 146*m2^4*Ycut^3 - 
          28*m2^5*Ycut^3 + 36*m2*Ycut^4 - 39*m2^2*Ycut^4 + 27*m2^3*Ycut^4 - 
          19*m2^4*Ycut^4 + 7*m2^5*Ycut^4 - 36*m2*Ycut^5 + 33*m2^2*Ycut^5 + 
          12*m2^3*Ycut^5 - 7*m2^4*Ycut^5 - Ycut^6 + 47*m2*Ycut^6 - 
          37*m2^2*Ycut^6 + 7*m2^3*Ycut^6 + 3*Ycut^7 + 2*m2*Ycut^7 + 
          m2^2*Ycut^7 - 3*Ycut^8 - m2*Ycut^8 + Ycut^9))/(144*(-1 + Ycut)^4) - 
       (m2*(12 - 9*m2 + 2*m2^2)*Log[m2])/12 + 
       (m2*(12 - 9*m2 + 2*m2^2)*Log[1 - Ycut])/12) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(28 + 166*m2 - 239*m2^2 + 145*m2^3 - 
          47*m2^4 + 7*m2^5 - 84*Ycut - 614*m2*Ycut + 875*m2^2*Ycut - 
          540*m2^3*Ycut + 181*m2^4*Ycut - 28*m2^5*Ycut + 84*Ycut^2 + 
          802*m2*Ycut^2 - 1131*m2^2*Ycut^2 + 717*m2^3*Ycut^2 - 
          254*m2^4*Ycut^2 + 42*m2^5*Ycut^2 - 28*Ycut^3 - 402*m2*Ycut^3 + 
          555*m2^2*Ycut^3 - 368*m2^3*Ycut^3 + 146*m2^4*Ycut^3 - 
          28*m2^5*Ycut^3 + 36*m2*Ycut^4 - 39*m2^2*Ycut^4 + 27*m2^3*Ycut^4 - 
          19*m2^4*Ycut^4 + 7*m2^5*Ycut^4 - 36*m2*Ycut^5 + 33*m2^2*Ycut^5 + 
          12*m2^3*Ycut^5 - 7*m2^4*Ycut^5 - Ycut^6 + 47*m2*Ycut^6 - 
          37*m2^2*Ycut^6 + 7*m2^3*Ycut^6 + 3*Ycut^7 + 2*m2*Ycut^7 + 
          m2^2*Ycut^7 - 3*Ycut^8 - m2*Ycut^8 + Ycut^9))/(144*(-1 + Ycut)^4) - 
       (m2*(12 - 9*m2 + 2*m2^2)*Log[m2])/12 + 
       (m2*(12 - 9*m2 + 2*m2^2)*Log[1 - Ycut])/12) + 
     c[VR]*(-1/60*(Sqrt[m2]*(-1 + m2 + Ycut)*(184 - 1181*m2 + 379*m2^2 - 
           201*m2^3 + 39*m2^4 - 304*Ycut + 1785*m2*Ycut - 596*m2^2*Ycut + 
           363*m2^3*Ycut - 78*m2^4*Ycut + 60*Ycut^2 - 330*m2*Ycut^2 + 
           94*m2^2*Ycut^2 - 123*m2^3*Ycut^2 + 39*m2^4*Ycut^2 + 20*Ycut^3 - 
           130*m2*Ycut^3 + 84*m2^2*Ycut^3 - 39*m2^3*Ycut^3 + 10*Ycut^4 - 
           75*m2*Ycut^4 + 39*m2^2*Ycut^4 + 6*Ycut^5 + 21*m2*Ycut^5 + 
           24*Ycut^6))/(-1 + Ycut)^2 - Sqrt[m2]*(-2 + 9*m2 + 6*m2^2)*
        Log[m2] + Sqrt[m2]*(-2 + 9*m2 + 6*m2^2)*Log[1 - Ycut]) + 
     c[SL]*(c[T]*(-1/72*((-1 + m2 + Ycut)*(-14 + 94*m2 - 23*m2^2 + 25*m2^3 - 
             29*m2^4 + 7*m2^5 + 42*Ycut - 296*m2*Ycut + 41*m2^2*Ycut - 
             78*m2^3*Ycut + 109*m2^4*Ycut - 28*m2^5*Ycut - 42*Ycut^2 + 
             310*m2*Ycut^2 + 27*m2^2*Ycut^2 + 69*m2^3*Ycut^2 - 
             146*m2^4*Ycut^2 + 42*m2^5*Ycut^2 + 14*Ycut^3 - 108*m2*Ycut^3 - 
             81*m2^2*Ycut^3 + 4*m2^3*Ycut^3 + 74*m2^4*Ycut^3 - 
             28*m2^5*Ycut^3 + 27*m2^2*Ycut^4 - 21*m2^3*Ycut^4 - m2^4*Ycut^4 + 
             7*m2^5*Ycut^4 - 81*m2^2*Ycut^5 - 6*m2^3*Ycut^5 - 7*m2^4*Ycut^5 - 
             4*Ycut^6 - 4*m2*Ycut^6 + 77*m2^2*Ycut^6 + 7*m2^3*Ycut^6 + 
             12*Ycut^7 + 8*m2*Ycut^7 - 23*m2^2*Ycut^7 - 12*Ycut^8 - 
             4*m2*Ycut^8 + 4*Ycut^9))/(-1 + Ycut)^4 - 
         (m2^2*(-9 + 4*m2)*Log[m2])/6 + (m2^2*(-9 + 4*m2)*Log[1 - Ycut])/6) + 
       c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-80 - 115*m2 + 21*m2^2 - 7*m2^3 + 
            m2^4 + 104*Ycut + 191*m2*Ycut - 36*m2^2*Ycut + 13*m2^3*Ycut - 
            2*m2^4*Ycut - 12*Ycut^2 - 54*m2*Ycut^2 + 10*m2^2*Ycut^2 - 
            5*m2^3*Ycut^2 + m2^4*Ycut^2 - 4*Ycut^3 - 14*m2*Ycut^3 + 
            4*m2^2*Ycut^3 - m2^3*Ycut^3 - 2*Ycut^4 - 5*m2*Ycut^4 + 
            m2^2*Ycut^4 - 6*Ycut^5 + 3*m2*Ycut^5))/(8*(-1 + Ycut)^2) + 
         (3*Sqrt[m2]*(2 + 11*m2 + 2*m2^2)*Log[m2])/2 - 
         (3*Sqrt[m2]*(2 + 11*m2 + 2*m2^2)*Log[1 - Ycut])/2)) + 
     c[VL]*(((-1 + m2 + Ycut)*(-8 + 46*m2 - 23*m2^2 + 73*m2^3 - 35*m2^4 + 
          7*m2^5 + 24*Ycut - 146*m2*Ycut + 71*m2^2*Ycut - 264*m2^3*Ycut + 
          133*m2^4*Ycut - 28*m2^5*Ycut - 24*Ycut^2 + 154*m2*Ycut^2 - 
          63*m2^2*Ycut^2 + 333*m2^3*Ycut^2 - 182*m2^4*Ycut^2 + 
          42*m2^5*Ycut^2 + 8*Ycut^3 - 54*m2*Ycut^3 + 3*m2^2*Ycut^3 - 
          152*m2^3*Ycut^3 + 98*m2^4*Ycut^3 - 28*m2^5*Ycut^3 + 9*m2^2*Ycut^4 + 
          3*m2^3*Ycut^4 - 7*m2^4*Ycut^4 + 7*m2^5*Ycut^4 + 45*m2^2*Ycut^5 - 
          7*m2^4*Ycut^5 - Ycut^6 - m2*Ycut^6 - 25*m2^2*Ycut^6 + 
          7*m2^3*Ycut^6 + 3*Ycut^7 + 2*m2*Ycut^7 + m2^2*Ycut^7 - 3*Ycut^8 - 
          m2*Ycut^8 + Ycut^9))/(18*(-1 + Ycut)^4) - 
       (2*m2^2*(3 + 2*m2)*Log[m2])/3 + (2*m2^2*(3 + 2*m2)*Log[1 - Ycut])/3 + 
       c[VR]*(-1/60*(Sqrt[m2]*(-1 + m2 + Ycut)*(184 - 1181*m2 + 379*m2^2 - 
             201*m2^3 + 39*m2^4 - 304*Ycut + 1785*m2*Ycut - 596*m2^2*Ycut + 
             363*m2^3*Ycut - 78*m2^4*Ycut + 60*Ycut^2 - 330*m2*Ycut^2 + 
             94*m2^2*Ycut^2 - 123*m2^3*Ycut^2 + 39*m2^4*Ycut^2 + 20*Ycut^3 - 
             130*m2*Ycut^3 + 84*m2^2*Ycut^3 - 39*m2^3*Ycut^3 + 10*Ycut^4 - 
             75*m2*Ycut^4 + 39*m2^2*Ycut^4 + 6*Ycut^5 + 21*m2*Ycut^5 + 
             24*Ycut^6))/(-1 + Ycut)^2 - Sqrt[m2]*(-2 + 9*m2 + 6*m2^2)*
          Log[m2] + Sqrt[m2]*(-2 + 9*m2 + 6*m2^2)*Log[1 - Ycut]))) + 
   rhoD*(-1/180*(678 - 2730*m2 + 2335*m2^2 - 400*m2^3 + 30*m2^4 + 122*m2^5 - 
        35*m2^6 - 3750*Ycut + 14370*m2*Ycut - 10895*m2^2*Ycut + 
        2120*m2^3*Ycut - 150*m2^4*Ycut - 610*m2^5*Ycut + 175*m2^6*Ycut + 
        8400*Ycut^2 - 30540*m2*Ycut^2 + 19840*m2^2*Ycut^2 - 
        4540*m2^3*Ycut^2 + 300*m2^4*Ycut^2 + 1220*m2^5*Ycut^2 - 
        350*m2^6*Ycut^2 - 9600*Ycut^3 + 32940*m2*Ycut^3 - 17240*m2^2*Ycut^3 + 
        4940*m2^3*Ycut^3 - 300*m2^4*Ycut^3 - 1220*m2^5*Ycut^3 + 
        350*m2^6*Ycut^3 + 5700*Ycut^4 - 18270*m2*Ycut^4 + 6670*m2^2*Ycut^4 - 
        2770*m2^3*Ycut^4 + 150*m2^4*Ycut^4 + 610*m2^5*Ycut^4 - 
        175*m2^6*Ycut^4 - 1356*Ycut^5 + 4230*m2*Ycut^5 - 410*m2^2*Ycut^5 + 
        530*m2^3*Ycut^5 - 30*m2^4*Ycut^5 - 122*m2^5*Ycut^5 + 35*m2^6*Ycut^5 - 
        595*Ycut^6 + 360*m2*Ycut^6 - 275*m2^2*Ycut^6 + 1175*Ycut^7 - 
        600*m2*Ycut^7 - 65*m2^2*Ycut^7 - 1000*Ycut^8 + 300*m2*Ycut^8 + 
        40*m2^2*Ycut^8 + 400*Ycut^9 - 60*m2*Ycut^9 - 47*Ycut^10 - 5*Ycut^11)/
       (-1 + Ycut)^5 + ((6 - 12*m2 - 13*m2^2 - 2*m2^3)*Log[m2])/3 + 
     ((-6 + 12*m2 + 13*m2^2 + 2*m2^3)*Log[1 - Ycut])/3 + 
     c[VR]^2*(-1/120*(522 - 1080*m2 + 615*m2^2 + 80*m2^3 - 270*m2^4 + 
          168*m2^5 - 35*m2^6 - 1806*Ycut + 3240*m2*Ycut - 1665*m2^2*Ycut - 
          240*m2^3*Ycut + 810*m2^4*Ycut - 504*m2^5*Ycut + 105*m2^6*Ycut + 
          2166*Ycut^2 - 3240*m2*Ycut^2 + 1395*m2^2*Ycut^2 + 240*m2^3*Ycut^2 - 
          810*m2^4*Ycut^2 + 504*m2^5*Ycut^2 - 105*m2^6*Ycut^2 - 962*Ycut^3 + 
          1080*m2*Ycut^3 - 285*m2^2*Ycut^3 - 80*m2^3*Ycut^3 + 
          270*m2^4*Ycut^3 - 168*m2^5*Ycut^3 + 35*m2^6*Ycut^3 + 60*Ycut^4 - 
          45*m2^2*Ycut^4 + 108*Ycut^5 - 105*m2^2*Ycut^5 - 234*Ycut^6 + 
          35*m2^2*Ycut^6 + 174*Ycut^7 + 15*m2^2*Ycut^7 - 18*Ycut^8 - 
          10*Ycut^9)/(-1 + Ycut)^3 + ((4 - 3*m2^2)*Log[m2])/2 + 
       ((-4 + 3*m2^2)*Log[1 - Ycut])/2) + 
     c[VR]*((Sqrt[m2]*(-5196 + 2285*m2 + 2040*m2^2 + 1380*m2^3 - 620*m2^4 + 
          111*m2^5 + 22584*Ycut - 3440*m2*Ycut - 7080*m2^2*Ycut - 
          5520*m2^3*Ycut + 2480*m2^4*Ycut - 444*m2^5*Ycut - 37476*Ycut^2 - 
          6240*m2*Ycut^2 + 8460*m2^2*Ycut^2 + 8280*m2^3*Ycut^2 - 
          3720*m2^4*Ycut^2 + 666*m2^5*Ycut^2 + 28584*Ycut^3 + 
          15560*m2*Ycut^3 - 3480*m2^2*Ycut^3 - 5520*m2^3*Ycut^3 + 
          2480*m2^4*Ycut^3 - 444*m2^5*Ycut^3 - 8946*Ycut^4 - 9590*m2*Ycut^4 - 
          210*m2^2*Ycut^4 + 1380*m2^3*Ycut^4 - 620*m2^4*Ycut^4 + 
          111*m2^5*Ycut^4 + 216*Ycut^5 + 1140*m2*Ycut^5 + 360*m2^2*Ycut^5 + 
          696*Ycut^6 - 340*m2*Ycut^6 + 210*m2^2*Ycut^6 - 984*Ycut^7 + 
          940*m2*Ycut^7 - 120*m2^2*Ycut^7 + 666*Ycut^8 - 315*m2*Ycut^8 - 
          144*Ycut^9))/(180*(-1 + Ycut)^4) - (Sqrt[m2]*(30 + 95*m2 + 18*m2^2)*
         Log[m2])/3 + (Sqrt[m2]*(30 + 95*m2 + 18*m2^2)*Log[1 - Ycut])/3) + 
     c[T]^2*(-1/45*(1182 - 5470*m2 + 4780*m2^2 - 160*m2^3 - 830*m2^4 + 
          638*m2^5 - 140*m2^6 - 6630*Ycut + 29270*m2*Ycut - 22580*m2^2*Ycut + 
          920*m2^3*Ycut + 4150*m2^4*Ycut - 3190*m2^5*Ycut + 700*m2^6*Ycut + 
          15060*Ycut^2 - 63340*m2*Ycut^2 + 41860*m2^2*Ycut^2 - 
          2140*m2^3*Ycut^2 - 8300*m2^4*Ycut^2 + 6380*m2^5*Ycut^2 - 
          1400*m2^6*Ycut^2 - 17460*Ycut^3 + 69740*m2*Ycut^3 - 
          37460*m2^2*Ycut^3 + 2540*m2^3*Ycut^3 + 8300*m2^4*Ycut^3 - 
          6380*m2^5*Ycut^3 + 1400*m2^6*Ycut^3 + 10530*Ycut^4 - 
          39670*m2*Ycut^4 + 15430*m2^2*Ycut^4 - 1570*m2^3*Ycut^4 - 
          4150*m2^4*Ycut^4 + 3190*m2^5*Ycut^4 - 700*m2^6*Ycut^4 - 
          2394*Ycut^5 + 9710*m2*Ycut^5 - 1910*m2^2*Ycut^5 + 290*m2^3*Ycut^5 + 
          830*m2^4*Ycut^5 - 638*m2^5*Ycut^5 + 140*m2^6*Ycut^5 - 1885*Ycut^6 + 
          200*m2*Ycut^6 + 325*m2^2*Ycut^6 + 3665*Ycut^7 - 760*m2*Ycut^7 - 
          545*m2^2*Ycut^7 - 3190*Ycut^8 + 440*m2*Ycut^8 + 55*m2^2*Ycut^8 + 
          1270*Ycut^9 - 120*m2*Ycut^9 + 45*m2^2*Ycut^9 - 113*Ycut^10 - 
          35*Ycut^11)/(-1 + Ycut)^5 - (8*(-6 + 16*m2 + 11*m2^2 + m2^3)*
         Log[m2])/3 + (8*(-6 + 16*m2 + 11*m2^2 + m2^3)*Log[1 - Ycut])/3) + 
     c[SL]^2*(-1/144*(348 - 646*m2 + 347*m2^2 - 80*m2^3 + 16*m2^4 + 22*m2^5 - 
          7*m2^6 - 1884*Ycut + 3134*m2*Ycut - 1579*m2^2*Ycut + 
          424*m2^3*Ycut - 80*m2^4*Ycut - 110*m2^5*Ycut + 35*m2^6*Ycut + 
          4128*Ycut^2 - 6028*m2*Ycut^2 + 2768*m2^2*Ycut^2 - 908*m2^3*Ycut^2 + 
          160*m2^4*Ycut^2 + 220*m2^5*Ycut^2 - 70*m2^6*Ycut^2 - 4608*Ycut^3 + 
          5708*m2*Ycut^3 - 2248*m2^2*Ycut^3 + 988*m2^3*Ycut^3 - 
          160*m2^4*Ycut^3 - 220*m2^5*Ycut^3 + 70*m2^6*Ycut^3 + 2664*Ycut^4 - 
          2614*m2*Ycut^4 + 734*m2^2*Ycut^4 - 554*m2^3*Ycut^4 + 
          80*m2^4*Ycut^4 + 110*m2^5*Ycut^4 - 35*m2^6*Ycut^4 - 648*Ycut^5 + 
          398*m2*Ycut^5 + 38*m2^2*Ycut^5 + 106*m2^3*Ycut^5 - 16*m2^4*Ycut^5 - 
          22*m2^5*Ycut^5 + 7*m2^6*Ycut^5 - 95*Ycut^6 + 104*m2*Ycut^6 - 
          55*m2^2*Ycut^6 + 187*Ycut^7 - 88*m2*Ycut^7 - 13*m2^2*Ycut^7 - 
          122*Ycut^8 + 32*m2*Ycut^8 + 8*m2^2*Ycut^8 + 26*Ycut^9 + 5*Ycut^10 - 
          Ycut^11)/(-1 + Ycut)^5 + ((12 + 8*m2 - 13*m2^2 - 2*m2^3)*Log[m2])/
        12 + ((-12 - 8*m2 + 13*m2^2 + 2*m2^3)*Log[1 - Ycut])/12) + 
     c[SR]^2*(-1/144*(348 - 646*m2 + 347*m2^2 - 80*m2^3 + 16*m2^4 + 22*m2^5 - 
          7*m2^6 - 1884*Ycut + 3134*m2*Ycut - 1579*m2^2*Ycut + 
          424*m2^3*Ycut - 80*m2^4*Ycut - 110*m2^5*Ycut + 35*m2^6*Ycut + 
          4128*Ycut^2 - 6028*m2*Ycut^2 + 2768*m2^2*Ycut^2 - 908*m2^3*Ycut^2 + 
          160*m2^4*Ycut^2 + 220*m2^5*Ycut^2 - 70*m2^6*Ycut^2 - 4608*Ycut^3 + 
          5708*m2*Ycut^3 - 2248*m2^2*Ycut^3 + 988*m2^3*Ycut^3 - 
          160*m2^4*Ycut^3 - 220*m2^5*Ycut^3 + 70*m2^6*Ycut^3 + 2664*Ycut^4 - 
          2614*m2*Ycut^4 + 734*m2^2*Ycut^4 - 554*m2^3*Ycut^4 + 
          80*m2^4*Ycut^4 + 110*m2^5*Ycut^4 - 35*m2^6*Ycut^4 - 648*Ycut^5 + 
          398*m2*Ycut^5 + 38*m2^2*Ycut^5 + 106*m2^3*Ycut^5 - 16*m2^4*Ycut^5 - 
          22*m2^5*Ycut^5 + 7*m2^6*Ycut^5 - 95*Ycut^6 + 104*m2*Ycut^6 - 
          55*m2^2*Ycut^6 + 187*Ycut^7 - 88*m2*Ycut^7 - 13*m2^2*Ycut^7 - 
          122*Ycut^8 + 32*m2*Ycut^8 + 8*m2^2*Ycut^8 + 26*Ycut^9 + 5*Ycut^10 - 
          Ycut^11)/(-1 + Ycut)^5 + ((12 + 8*m2 - 13*m2^2 - 2*m2^3)*Log[m2])/
        12 + ((-12 - 8*m2 + 13*m2^2 + 2*m2^3)*Log[1 - Ycut])/12) + 
     c[VL]^2*(-1/180*(678 - 2730*m2 + 2335*m2^2 - 400*m2^3 + 30*m2^4 + 
          122*m2^5 - 35*m2^6 - 3750*Ycut + 14370*m2*Ycut - 10895*m2^2*Ycut + 
          2120*m2^3*Ycut - 150*m2^4*Ycut - 610*m2^5*Ycut + 175*m2^6*Ycut + 
          8400*Ycut^2 - 30540*m2*Ycut^2 + 19840*m2^2*Ycut^2 - 
          4540*m2^3*Ycut^2 + 300*m2^4*Ycut^2 + 1220*m2^5*Ycut^2 - 
          350*m2^6*Ycut^2 - 9600*Ycut^3 + 32940*m2*Ycut^3 - 
          17240*m2^2*Ycut^3 + 4940*m2^3*Ycut^3 - 300*m2^4*Ycut^3 - 
          1220*m2^5*Ycut^3 + 350*m2^6*Ycut^3 + 5700*Ycut^4 - 
          18270*m2*Ycut^4 + 6670*m2^2*Ycut^4 - 2770*m2^3*Ycut^4 + 
          150*m2^4*Ycut^4 + 610*m2^5*Ycut^4 - 175*m2^6*Ycut^4 - 1356*Ycut^5 + 
          4230*m2*Ycut^5 - 410*m2^2*Ycut^5 + 530*m2^3*Ycut^5 - 
          30*m2^4*Ycut^5 - 122*m2^5*Ycut^5 + 35*m2^6*Ycut^5 - 595*Ycut^6 + 
          360*m2*Ycut^6 - 275*m2^2*Ycut^6 + 1175*Ycut^7 - 600*m2*Ycut^7 - 
          65*m2^2*Ycut^7 - 1000*Ycut^8 + 300*m2*Ycut^8 + 40*m2^2*Ycut^8 + 
          400*Ycut^9 - 60*m2*Ycut^9 - 47*Ycut^10 - 5*Ycut^11)/(-1 + Ycut)^5 + 
       ((6 - 12*m2 - 13*m2^2 - 2*m2^3)*Log[m2])/3 + 
       ((-6 + 12*m2 + 13*m2^2 + 2*m2^3)*Log[1 - Ycut])/3) + 
     c[VL]*(-1/90*(678 - 2730*m2 + 2335*m2^2 - 400*m2^3 + 30*m2^4 + 
          122*m2^5 - 35*m2^6 - 3750*Ycut + 14370*m2*Ycut - 10895*m2^2*Ycut + 
          2120*m2^3*Ycut - 150*m2^4*Ycut - 610*m2^5*Ycut + 175*m2^6*Ycut + 
          8400*Ycut^2 - 30540*m2*Ycut^2 + 19840*m2^2*Ycut^2 - 
          4540*m2^3*Ycut^2 + 300*m2^4*Ycut^2 + 1220*m2^5*Ycut^2 - 
          350*m2^6*Ycut^2 - 9600*Ycut^3 + 32940*m2*Ycut^3 - 
          17240*m2^2*Ycut^3 + 4940*m2^3*Ycut^3 - 300*m2^4*Ycut^3 - 
          1220*m2^5*Ycut^3 + 350*m2^6*Ycut^3 + 5700*Ycut^4 - 
          18270*m2*Ycut^4 + 6670*m2^2*Ycut^4 - 2770*m2^3*Ycut^4 + 
          150*m2^4*Ycut^4 + 610*m2^5*Ycut^4 - 175*m2^6*Ycut^4 - 1356*Ycut^5 + 
          4230*m2*Ycut^5 - 410*m2^2*Ycut^5 + 530*m2^3*Ycut^5 - 
          30*m2^4*Ycut^5 - 122*m2^5*Ycut^5 + 35*m2^6*Ycut^5 - 595*Ycut^6 + 
          360*m2*Ycut^6 - 275*m2^2*Ycut^6 + 1175*Ycut^7 - 600*m2*Ycut^7 - 
          65*m2^2*Ycut^7 - 1000*Ycut^8 + 300*m2*Ycut^8 + 40*m2^2*Ycut^8 + 
          400*Ycut^9 - 60*m2*Ycut^9 - 47*Ycut^10 - 5*Ycut^11)/(-1 + Ycut)^5 - 
       (2*(-6 + 12*m2 + 13*m2^2 + 2*m2^3)*Log[m2])/3 + 
       (2*(-6 + 12*m2 + 13*m2^2 + 2*m2^3)*Log[1 - Ycut])/3 + 
       c[VR]*((Sqrt[m2]*(-5196 + 2285*m2 + 2040*m2^2 + 1380*m2^3 - 620*m2^4 + 
            111*m2^5 + 22584*Ycut - 3440*m2*Ycut - 7080*m2^2*Ycut - 
            5520*m2^3*Ycut + 2480*m2^4*Ycut - 444*m2^5*Ycut - 37476*Ycut^2 - 
            6240*m2*Ycut^2 + 8460*m2^2*Ycut^2 + 8280*m2^3*Ycut^2 - 
            3720*m2^4*Ycut^2 + 666*m2^5*Ycut^2 + 28584*Ycut^3 + 
            15560*m2*Ycut^3 - 3480*m2^2*Ycut^3 - 5520*m2^3*Ycut^3 + 
            2480*m2^4*Ycut^3 - 444*m2^5*Ycut^3 - 8946*Ycut^4 - 
            9590*m2*Ycut^4 - 210*m2^2*Ycut^4 + 1380*m2^3*Ycut^4 - 
            620*m2^4*Ycut^4 + 111*m2^5*Ycut^4 + 216*Ycut^5 + 1140*m2*Ycut^5 + 
            360*m2^2*Ycut^5 + 696*Ycut^6 - 340*m2*Ycut^6 + 210*m2^2*Ycut^6 - 
            984*Ycut^7 + 940*m2*Ycut^7 - 120*m2^2*Ycut^7 + 666*Ycut^8 - 
            315*m2*Ycut^8 - 144*Ycut^9))/(180*(-1 + Ycut)^4) - 
         (Sqrt[m2]*(30 + 95*m2 + 18*m2^2)*Log[m2])/3 + 
         (Sqrt[m2]*(30 + 95*m2 + 18*m2^2)*Log[1 - Ycut])/3)) + 
     c[SL]*(c[T]*((42 + 444*m2 - 565*m2^2 + 208*m2^3 - 174*m2^4 + 52*m2^5 - 
           7*m2^6 - 210*Ycut - 2508*m2*Ycut + 2621*m2^2*Ycut - 
           1088*m2^3*Ycut + 870*m2^4*Ycut - 260*m2^5*Ycut + 35*m2^6*Ycut + 
           420*Ycut^2 + 5736*m2*Ycut^2 - 4732*m2^2*Ycut^2 + 
           2296*m2^3*Ycut^2 - 1740*m2^4*Ycut^2 + 520*m2^5*Ycut^2 - 
           70*m2^6*Ycut^2 - 420*Ycut^3 - 6696*m2*Ycut^3 + 4052*m2^2*Ycut^3 - 
           2456*m2^3*Ycut^3 + 1740*m2^4*Ycut^3 - 520*m2^5*Ycut^3 + 
           70*m2^6*Ycut^3 + 210*Ycut^4 + 4068*m2*Ycut^4 - 1516*m2^2*Ycut^4 + 
           1348*m2^3*Ycut^4 - 870*m2^4*Ycut^4 + 260*m2^5*Ycut^4 - 
           35*m2^6*Ycut^4 - 42*Ycut^5 - 1044*m2*Ycut^5 - 16*m2^2*Ycut^5 - 
           260*m2^3*Ycut^5 + 174*m2^4*Ycut^5 - 52*m2^5*Ycut^5 + 
           7*m2^6*Ycut^5 + 4*Ycut^6 - 144*m2*Ycut^6 + 230*m2^2*Ycut^6 - 
           20*Ycut^7 + 240*m2*Ycut^7 - 70*m2^2*Ycut^7 + 40*Ycut^8 - 
           120*m2*Ycut^8 - 13*m2^2*Ycut^8 - 40*Ycut^9 + 24*m2*Ycut^9 + 
           9*m2^2*Ycut^9 + 20*Ycut^10 - 4*Ycut^11)/(72*(-1 + Ycut)^5) - 
         (m2*(24 + 17*m2 + 4*m2^2)*Log[m2])/6 + 
         (m2*(24 + 17*m2 + 4*m2^2)*Log[1 - Ycut])/6) + 
       c[SR]*(-1/72*(Sqrt[m2]*(-1512 + 397*m2 + 1128*m2^2 + 36*m2^3 - 
             64*m2^4 + 15*m2^5 + 6552*Ycut + 272*m2*Ycut - 4296*m2^2*Ycut - 
             144*m2^3*Ycut + 256*m2^4*Ycut - 60*m2^5*Ycut - 10836*Ycut^2 - 
             4128*m2*Ycut^2 + 6012*m2^2*Ycut^2 + 216*m2^3*Ycut^2 - 
             384*m2^4*Ycut^2 + 90*m2^5*Ycut^2 + 8232*Ycut^3 + 
             6472*m2*Ycut^3 - 3576*m2^2*Ycut^3 - 144*m2^3*Ycut^3 + 
             256*m2^4*Ycut^3 - 60*m2^5*Ycut^3 - 2562*Ycut^4 - 
             3478*m2*Ycut^4 + 678*m2^2*Ycut^4 + 36*m2^3*Ycut^4 - 
             64*m2^4*Ycut^4 + 15*m2^5*Ycut^4 + 72*Ycut^5 + 372*m2*Ycut^5 + 
             72*m2^2*Ycut^5 + 120*Ycut^6 - 20*m2*Ycut^6 + 42*m2^2*Ycut^6 - 
             120*Ycut^7 + 140*m2*Ycut^7 - 24*m2^2*Ycut^7 + 54*Ycut^8 - 
             27*m2*Ycut^8))/(-1 + Ycut)^4 + (Sqrt[m2]*(42 + 155*m2 + 18*m2^2)*
           Log[m2])/6 - (Sqrt[m2]*(42 + 155*m2 + 18*m2^2)*Log[1 - Ycut])/
          6))))/mb^3 + 
 (mupi*(-1/36*((-1 + m2 + Ycut)*(4 - 50*m2 - 245*m2^2 - 5*m2^3 - 5*m2^4 + 
         m2^5 - 12*Ycut + 154*m2*Ycut + 869*m2^2*Ycut + 24*m2^3*Ycut + 
         19*m2^4*Ycut - 4*m2^5*Ycut + 12*Ycut^2 - 158*m2*Ycut^2 - 
         1089*m2^2*Ycut^2 - 45*m2^3*Ycut^2 - 26*m2^4*Ycut^2 + 6*m2^5*Ycut^2 - 
         4*Ycut^3 + 54*m2*Ycut^3 + 525*m2^2*Ycut^3 + 40*m2^3*Ycut^3 + 
         14*m2^4*Ycut^3 - 4*m2^5*Ycut^3 - 45*m2^2*Ycut^4 - 15*m2^3*Ycut^4 - 
         m2^4*Ycut^4 + m2^5*Ycut^4 - 9*m2^2*Ycut^5 - m2^4*Ycut^5 + 5*Ycut^6 + 
         5*m2*Ycut^6 + 17*m2^2*Ycut^6 + m2^3*Ycut^6 - 15*Ycut^7 - 
         10*m2*Ycut^7 - 5*m2^2*Ycut^7 + 15*Ycut^8 + 5*m2*Ycut^8 - 5*Ycut^9))/
       (-1 + Ycut)^4 - (5*m2^2*(3 + 2*m2)*Log[m2])/3 + 
     (5*m2^2*(3 + 2*m2)*Log[1 - Ycut])/3 + 
     c[VR]^2*(-1/24*((-1 + m2 + Ycut)*(2 - 22*m2 - 57*m2^2 + 23*m2^3 - 
           7*m2^4 + m2^5 - 2*Ycut + 24*m2*Ycut + 97*m2^2*Ycut - 
           40*m2^3*Ycut + 13*m2^4*Ycut - 2*m2^5*Ycut - 28*m2^2*Ycut^2 + 
           12*m2^3*Ycut^2 - 5*m2^4*Ycut^2 + m2^5*Ycut^2 - 8*m2^2*Ycut^3 + 
           4*m2^3*Ycut^3 - m2^4*Ycut^3 - 3*m2^2*Ycut^4 + m2^3*Ycut^4 - 
           m2^2*Ycut^5 + 10*Ycut^6 + 10*m2*Ycut^6 - 10*Ycut^7))/
         (-1 + Ycut)^2 - (5*m2^2*Log[m2])/2 + (5*m2^2*Log[1 - Ycut])/2) + 
     c[T]^2*(-1/9*((-1 + m2 + Ycut)*(10 - 116*m2 - 416*m2^2 + 64*m2^3 - 
           26*m2^4 + 4*m2^5 - 30*Ycut + 358*m2*Ycut + 1502*m2^2*Ycut - 
           234*m2^3*Ycut + 100*m2^4*Ycut - 16*m2^5*Ycut + 30*Ycut^2 - 
           368*m2*Ycut^2 - 1926*m2^2*Ycut^2 + 300*m2^3*Ycut^2 - 
           140*m2^4*Ycut^2 + 24*m2^5*Ycut^2 - 10*Ycut^3 + 126*m2*Ycut^3 + 
           960*m2^2*Ycut^3 - 140*m2^3*Ycut^3 + 80*m2^4*Ycut^3 - 
           16*m2^5*Ycut^3 - 90*m2^2*Ycut^4 - 10*m2^4*Ycut^4 + 4*m2^5*Ycut^4 - 
           18*m2^2*Ycut^5 + 6*m2^3*Ycut^5 - 4*m2^4*Ycut^5 + 35*Ycut^6 + 
           35*m2*Ycut^6 + 14*m2^2*Ycut^6 + 4*m2^3*Ycut^6 - 105*Ycut^7 - 
           70*m2*Ycut^7 - 8*m2^2*Ycut^7 + 105*Ycut^8 + 35*m2*Ycut^8 - 
           35*Ycut^9))/(-1 + Ycut)^4 - (40*m2^2*(3 + m2)*Log[m2])/3 + 
       (40*m2^2*(3 + m2)*Log[1 - Ycut])/3) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-6 - 131*m2 - 51*m2^2 + 9*m2^3 - 
          m2^4 + 12*Ycut + 316*m2*Ycut + 145*m2^2*Ycut - 26*m2^3*Ycut + 
          3*m2^4*Ycut - 6*Ycut^2 - 215*m2*Ycut^2 - 130*m2^2*Ycut^2 + 
          24*m2^3*Ycut^2 - 3*m2^4*Ycut^2 + 20*m2*Ycut^3 + 30*m2^2*Ycut^3 - 
          6*m2^3*Ycut^3 + m2^4*Ycut^3 + 5*m2*Ycut^4 + 5*m2^2*Ycut^4 - 
          m2^3*Ycut^4 + 2*m2*Ycut^5 + m2^2*Ycut^5 - 9*m2*Ycut^6))/
        (12*(-1 + Ycut)^3) - 5*m2^(3/2)*(1 + 2*m2)*Log[m2] + 
       5*m2^(3/2)*(1 + 2*m2)*Log[1 - Ycut]) + 
     c[SL]^2*(-1/144*((-1 + m2 + Ycut)*(4 - 50*m2 - 245*m2^2 - 5*m2^3 - 
           5*m2^4 + m2^5 - 12*Ycut + 154*m2*Ycut + 869*m2^2*Ycut + 
           24*m2^3*Ycut + 19*m2^4*Ycut - 4*m2^5*Ycut + 12*Ycut^2 - 
           158*m2*Ycut^2 - 1089*m2^2*Ycut^2 - 45*m2^3*Ycut^2 - 
           26*m2^4*Ycut^2 + 6*m2^5*Ycut^2 - 4*Ycut^3 + 54*m2*Ycut^3 + 
           525*m2^2*Ycut^3 + 40*m2^3*Ycut^3 + 14*m2^4*Ycut^3 - 
           4*m2^5*Ycut^3 - 45*m2^2*Ycut^4 - 15*m2^3*Ycut^4 - m2^4*Ycut^4 + 
           m2^5*Ycut^4 - 9*m2^2*Ycut^5 - m2^4*Ycut^5 + 5*Ycut^6 + 
           5*m2*Ycut^6 + 17*m2^2*Ycut^6 + m2^3*Ycut^6 - 15*Ycut^7 - 
           10*m2*Ycut^7 - 5*m2^2*Ycut^7 + 15*Ycut^8 + 5*m2*Ycut^8 - 
           5*Ycut^9))/(-1 + Ycut)^4 - (5*m2^2*(3 + 2*m2)*Log[m2])/12 + 
       (5*m2^2*(3 + 2*m2)*Log[1 - Ycut])/12) + 
     c[SR]^2*(-1/144*((-1 + m2 + Ycut)*(4 - 50*m2 - 245*m2^2 - 5*m2^3 - 
           5*m2^4 + m2^5 - 12*Ycut + 154*m2*Ycut + 869*m2^2*Ycut + 
           24*m2^3*Ycut + 19*m2^4*Ycut - 4*m2^5*Ycut + 12*Ycut^2 - 
           158*m2*Ycut^2 - 1089*m2^2*Ycut^2 - 45*m2^3*Ycut^2 - 
           26*m2^4*Ycut^2 + 6*m2^5*Ycut^2 - 4*Ycut^3 + 54*m2*Ycut^3 + 
           525*m2^2*Ycut^3 + 40*m2^3*Ycut^3 + 14*m2^4*Ycut^3 - 
           4*m2^5*Ycut^3 - 45*m2^2*Ycut^4 - 15*m2^3*Ycut^4 - m2^4*Ycut^4 + 
           m2^5*Ycut^4 - 9*m2^2*Ycut^5 - m2^4*Ycut^5 + 5*Ycut^6 + 
           5*m2*Ycut^6 + 17*m2^2*Ycut^6 + m2^3*Ycut^6 - 15*Ycut^7 - 
           10*m2*Ycut^7 - 5*m2^2*Ycut^7 + 15*Ycut^8 + 5*m2*Ycut^8 - 
           5*Ycut^9))/(-1 + Ycut)^4 - (5*m2^2*(3 + 2*m2)*Log[m2])/12 + 
       (5*m2^2*(3 + 2*m2)*Log[1 - Ycut])/12) + 
     c[VL]^2*(-1/36*((-1 + m2 + Ycut)*(4 - 50*m2 - 245*m2^2 - 5*m2^3 - 
           5*m2^4 + m2^5 - 12*Ycut + 154*m2*Ycut + 869*m2^2*Ycut + 
           24*m2^3*Ycut + 19*m2^4*Ycut - 4*m2^5*Ycut + 12*Ycut^2 - 
           158*m2*Ycut^2 - 1089*m2^2*Ycut^2 - 45*m2^3*Ycut^2 - 
           26*m2^4*Ycut^2 + 6*m2^5*Ycut^2 - 4*Ycut^3 + 54*m2*Ycut^3 + 
           525*m2^2*Ycut^3 + 40*m2^3*Ycut^3 + 14*m2^4*Ycut^3 - 
           4*m2^5*Ycut^3 - 45*m2^2*Ycut^4 - 15*m2^3*Ycut^4 - m2^4*Ycut^4 + 
           m2^5*Ycut^4 - 9*m2^2*Ycut^5 - m2^4*Ycut^5 + 5*Ycut^6 + 
           5*m2*Ycut^6 + 17*m2^2*Ycut^6 + m2^3*Ycut^6 - 15*Ycut^7 - 
           10*m2*Ycut^7 - 5*m2^2*Ycut^7 + 15*Ycut^8 + 5*m2*Ycut^8 - 
           5*Ycut^9))/(-1 + Ycut)^4 - (5*m2^2*(3 + 2*m2)*Log[m2])/3 + 
       (5*m2^2*(3 + 2*m2)*Log[1 - Ycut])/3) + 
     c[VL]*(-1/18*((-1 + m2 + Ycut)*(4 - 50*m2 - 245*m2^2 - 5*m2^3 - 5*m2^4 + 
           m2^5 - 12*Ycut + 154*m2*Ycut + 869*m2^2*Ycut + 24*m2^3*Ycut + 
           19*m2^4*Ycut - 4*m2^5*Ycut + 12*Ycut^2 - 158*m2*Ycut^2 - 
           1089*m2^2*Ycut^2 - 45*m2^3*Ycut^2 - 26*m2^4*Ycut^2 + 
           6*m2^5*Ycut^2 - 4*Ycut^3 + 54*m2*Ycut^3 + 525*m2^2*Ycut^3 + 
           40*m2^3*Ycut^3 + 14*m2^4*Ycut^3 - 4*m2^5*Ycut^3 - 45*m2^2*Ycut^4 - 
           15*m2^3*Ycut^4 - m2^4*Ycut^4 + m2^5*Ycut^4 - 9*m2^2*Ycut^5 - 
           m2^4*Ycut^5 + 5*Ycut^6 + 5*m2*Ycut^6 + 17*m2^2*Ycut^6 + 
           m2^3*Ycut^6 - 15*Ycut^7 - 10*m2*Ycut^7 - 5*m2^2*Ycut^7 + 
           15*Ycut^8 + 5*m2*Ycut^8 - 5*Ycut^9))/(-1 + Ycut)^4 - 
       (10*m2^2*(3 + 2*m2)*Log[m2])/3 + (10*m2^2*(3 + 2*m2)*Log[1 - Ycut])/
        3 + c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-6 - 131*m2 - 51*m2^2 + 
            9*m2^3 - m2^4 + 12*Ycut + 316*m2*Ycut + 145*m2^2*Ycut - 
            26*m2^3*Ycut + 3*m2^4*Ycut - 6*Ycut^2 - 215*m2*Ycut^2 - 
            130*m2^2*Ycut^2 + 24*m2^3*Ycut^2 - 3*m2^4*Ycut^2 + 20*m2*Ycut^3 + 
            30*m2^2*Ycut^3 - 6*m2^3*Ycut^3 + m2^4*Ycut^3 + 5*m2*Ycut^4 + 
            5*m2^2*Ycut^4 - m2^3*Ycut^4 + 2*m2*Ycut^5 + m2^2*Ycut^5 - 
            9*m2*Ycut^6))/(12*(-1 + Ycut)^3) - 5*m2^(3/2)*(1 + 2*m2)*
          Log[m2] + 5*m2^(3/2)*(1 + 2*m2)*Log[1 - Ycut])) + 
     c[SL]*(c[SR]*(-1/24*(Sqrt[m2]*(-1 + m2 + Ycut)*(-6 - 131*m2 - 51*m2^2 + 
             9*m2^3 - m2^4 + 12*Ycut + 316*m2*Ycut + 145*m2^2*Ycut - 
             26*m2^3*Ycut + 3*m2^4*Ycut - 6*Ycut^2 - 215*m2*Ycut^2 - 
             130*m2^2*Ycut^2 + 24*m2^3*Ycut^2 - 3*m2^4*Ycut^2 + 
             20*m2*Ycut^3 + 30*m2^2*Ycut^3 - 6*m2^3*Ycut^3 + m2^4*Ycut^3 + 
             5*m2*Ycut^4 + 5*m2^2*Ycut^4 - m2^3*Ycut^4 + 2*m2*Ycut^5 + 
             m2^2*Ycut^5 - 9*m2*Ycut^6))/(-1 + Ycut)^3 + 
         (5*m2^(3/2)*(1 + 2*m2)*Log[m2])/2 - (5*m2^(3/2)*(1 + 2*m2)*
           Log[1 - Ycut])/2) + 
       c[T]*(((-1 + m2 + Ycut)*(-2 + 34*m2 + 319*m2^2 + 79*m2^3 - 11*m2^4 + 
            m2^5 + 6*Ycut - 104*m2*Ycut - 1105*m2^2*Ycut - 306*m2^3*Ycut + 
            43*m2^4*Ycut - 4*m2^5*Ycut - 6*Ycut^2 + 106*m2*Ycut^2 + 
            1341*m2^2*Ycut^2 + 435*m2^3*Ycut^2 - 62*m2^4*Ycut^2 + 
            6*m2^5*Ycut^2 + 2*Ycut^3 - 36*m2*Ycut^3 - 615*m2^2*Ycut^3 - 
            260*m2^3*Ycut^3 + 38*m2^4*Ycut^3 - 4*m2^5*Ycut^3 + 
            45*m2^2*Ycut^4 + 45*m2^3*Ycut^4 - 7*m2^4*Ycut^4 + m2^5*Ycut^4 + 
            9*m2^2*Ycut^5 + 6*m2^3*Ycut^5 - m2^4*Ycut^5 + 20*Ycut^6 + 
            20*m2*Ycut^6 - 37*m2^2*Ycut^6 + m2^3*Ycut^6 - 60*Ycut^7 - 
            40*m2*Ycut^7 + 7*m2^2*Ycut^7 + 60*Ycut^8 + 20*m2*Ycut^8 - 
            20*Ycut^9))/(72*(-1 + Ycut)^4) - (5*m2^2*(3 + 4*m2)*Log[m2])/6 + 
         (5*m2^2*(3 + 4*m2)*Log[1 - Ycut])/6))) + 
   muG*(-1/180*((-1 + m2 + Ycut)*(104 + 224*m2 - 901*m2^2 - 301*m2^3 + 
         119*m2^4 - 25*m2^5 - 208*Ycut - 704*m2*Ycut + 2310*m2^2*Ycut + 
         809*m2^3*Ycut - 332*m2^4*Ycut + 75*m2^5*Ycut + 104*Ycut^2 + 
         660*m2*Ycut^2 - 1755*m2^2*Ycut^2 - 646*m2^3*Ycut^2 + 
         282*m2^4*Ycut^2 - 75*m2^5*Ycut^2 - 120*m2*Ycut^3 + 240*m2^2*Ycut^3 + 
         94*m2^3*Ycut^3 - 44*m2^4*Ycut^3 + 25*m2^5*Ycut^3 - 30*m2*Ycut^4 + 
         75*m2^2*Ycut^4 + 19*m2^3*Ycut^4 - 25*m2^4*Ycut^4 + 36*Ycut^5 - 
         84*m2*Ycut^5 - 144*m2^2*Ycut^5 + 25*m2^3*Ycut^5 - 47*Ycut^6 + 
         79*m2*Ycut^6 + 25*m2^2*Ycut^6 - 14*Ycut^7 - 25*m2*Ycut^7 + 
         25*Ycut^8))/(-1 + Ycut)^3 + (m2*(-6 + 9*m2 + 10*m2^2)*Log[m2])/3 - 
     (m2*(-6 + 9*m2 + 10*m2^2)*Log[1 - Ycut])/3 + 
     c[VR]^2*(-1/120*((-1 + m2 + Ycut)*(22 - 98*m2 + 297*m2^2 - 263*m2^3 + 
           127*m2^4 - 25*m2^5 + 22*m2*Ycut - 136*m2^2*Ycut + 161*m2^3*Ycut - 
           102*m2^4*Ycut + 25*m2^5*Ycut + 22*m2*Ycut^2 - 84*m2^2*Ycut^2 + 
           77*m2^3*Ycut^2 - 25*m2^4*Ycut^2 + 22*m2*Ycut^3 - 52*m2^2*Ycut^3 + 
           25*m2^3*Ycut^3 + 22*m2*Ycut^4 - 25*m2^2*Ycut^4 - 72*Ycut^5 - 
           50*m2*Ycut^5 + 50*Ycut^6))/(-1 + Ycut) - (m2^2*Log[m2])/2 + 
       (m2^2*Log[1 - Ycut])/2) + 
     c[T]^2*(-1/45*((-1 + m2 + Ycut)*(386 + 1316*m2 - 784*m2^2 - 784*m2^3 + 
           446*m2^4 - 100*m2^5 - 772*Ycut - 3686*m2*Ycut + 2190*m2^2*Ycut + 
           2006*m2^3*Ycut - 1238*m2^4*Ycut + 300*m2^5*Ycut + 386*Ycut^2 + 
           3090*m2*Ycut^2 - 1920*m2^2*Ycut^2 - 1414*m2^3*Ycut^2 + 
           1038*m2^4*Ycut^2 - 300*m2^5*Ycut^2 - 480*m2*Ycut^3 + 
           360*m2^2*Ycut^3 + 46*m2^3*Ycut^3 - 146*m2^4*Ycut^3 + 
           100*m2^5*Ycut^3 - 120*m2*Ycut^4 + 150*m2^2*Ycut^4 + 
           46*m2^3*Ycut^4 - 100*m2^4*Ycut^4 - 396*Ycut^5 - 336*m2*Ycut^5 - 
           96*m2^2*Ycut^5 + 100*m2^3*Ycut^5 + 967*Ycut^6 + 391*m2*Ycut^6 - 
           50*m2^2*Ycut^6 - 746*Ycut^7 - 175*m2*Ycut^7 + 175*Ycut^8))/
         (-1 + Ycut)^3 + (8*m2*(-12 + 3*m2 + 5*m2^2)*Log[m2])/3 - 
       (8*m2*(-12 + 3*m2 + 5*m2^2)*Log[1 - Ycut])/3) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(232 - 873*m2 + 207*m2^2 - 133*m2^3 + 
          27*m2^4 - 352*Ycut + 1285*m2*Ycut - 308*m2^2*Ycut + 239*m2^3*Ycut - 
          54*m2^4*Ycut + 60*Ycut^2 - 210*m2*Ycut^2 + 22*m2^2*Ycut^2 - 
          79*m2^3*Ycut^2 + 27*m2^4*Ycut^2 + 20*Ycut^3 - 90*m2*Ycut^3 + 
          52*m2^2*Ycut^3 - 27*m2^3*Ycut^3 + 10*Ycut^4 - 55*m2*Ycut^4 + 
          27*m2^2*Ycut^4 + 78*Ycut^5 + 33*m2*Ycut^5 - 48*Ycut^6))/
        (60*(-1 + Ycut)^2) + Sqrt[m2]*(-2 + 5*m2 + 6*m2^2)*Log[m2] - 
       Sqrt[m2]*(-2 + 5*m2 + 6*m2^2)*Log[1 - Ycut]) + 
     c[VL]^2*(-1/180*((-1 + m2 + Ycut)*(104 + 224*m2 - 901*m2^2 - 301*m2^3 + 
           119*m2^4 - 25*m2^5 - 208*Ycut - 704*m2*Ycut + 2310*m2^2*Ycut + 
           809*m2^3*Ycut - 332*m2^4*Ycut + 75*m2^5*Ycut + 104*Ycut^2 + 
           660*m2*Ycut^2 - 1755*m2^2*Ycut^2 - 646*m2^3*Ycut^2 + 
           282*m2^4*Ycut^2 - 75*m2^5*Ycut^2 - 120*m2*Ycut^3 + 
           240*m2^2*Ycut^3 + 94*m2^3*Ycut^3 - 44*m2^4*Ycut^3 + 
           25*m2^5*Ycut^3 - 30*m2*Ycut^4 + 75*m2^2*Ycut^4 + 19*m2^3*Ycut^4 - 
           25*m2^4*Ycut^4 + 36*Ycut^5 - 84*m2*Ycut^5 - 144*m2^2*Ycut^5 + 
           25*m2^3*Ycut^5 - 47*Ycut^6 + 79*m2*Ycut^6 + 25*m2^2*Ycut^6 - 
           14*Ycut^7 - 25*m2*Ycut^7 + 25*Ycut^8))/(-1 + Ycut)^3 + 
       (m2*(-6 + 9*m2 + 10*m2^2)*Log[m2])/3 - 
       (m2*(-6 + 9*m2 + 10*m2^2)*Log[1 - Ycut])/3) + 
     c[SL]^2*(-1/720*((-1 + m2 + Ycut)*(-256 - 4306*m2 - 1351*m2^2 - 
           391*m2^3 + 149*m2^4 - 25*m2^5 + 512*Ycut + 10516*m2*Ycut + 
           3720*m2^2*Ycut + 1049*m2^3*Ycut - 422*m2^4*Ycut + 75*m2^5*Ycut - 
           256*Ycut^2 - 7290*m2*Ycut^2 - 3255*m2^2*Ycut^2 - 826*m2^3*Ycut^2 + 
           372*m2^4*Ycut^2 - 75*m2^5*Ycut^2 + 720*m2*Ycut^3 + 
           780*m2^2*Ycut^3 + 94*m2^3*Ycut^3 - 74*m2^4*Ycut^3 + 
           25*m2^5*Ycut^3 + 180*m2*Ycut^4 + 105*m2^2*Ycut^4 + 
           49*m2^3*Ycut^4 - 25*m2^4*Ycut^4 + 36*Ycut^5 + 216*m2*Ycut^5 - 
           174*m2^2*Ycut^5 + 25*m2^3*Ycut^5 - 47*Ycut^6 - 11*m2*Ycut^6 + 
           25*m2^2*Ycut^6 - 14*Ycut^7 - 25*m2*Ycut^7 + 25*Ycut^8))/
         (-1 + Ycut)^3 + (m2*(36 + 57*m2 + 10*m2^2)*Log[m2])/12 - 
       (m2*(36 + 57*m2 + 10*m2^2)*Log[1 - Ycut])/12) + 
     c[SR]^2*(-1/720*((-1 + m2 + Ycut)*(-256 - 4306*m2 - 1351*m2^2 - 
           391*m2^3 + 149*m2^4 - 25*m2^5 + 512*Ycut + 10516*m2*Ycut + 
           3720*m2^2*Ycut + 1049*m2^3*Ycut - 422*m2^4*Ycut + 75*m2^5*Ycut - 
           256*Ycut^2 - 7290*m2*Ycut^2 - 3255*m2^2*Ycut^2 - 826*m2^3*Ycut^2 + 
           372*m2^4*Ycut^2 - 75*m2^5*Ycut^2 + 720*m2*Ycut^3 + 
           780*m2^2*Ycut^3 + 94*m2^3*Ycut^3 - 74*m2^4*Ycut^3 + 
           25*m2^5*Ycut^3 + 180*m2*Ycut^4 + 105*m2^2*Ycut^4 + 
           49*m2^3*Ycut^4 - 25*m2^4*Ycut^4 + 36*Ycut^5 + 216*m2*Ycut^5 - 
           174*m2^2*Ycut^5 + 25*m2^3*Ycut^5 - 47*Ycut^6 - 11*m2*Ycut^6 + 
           25*m2^2*Ycut^6 - 14*Ycut^7 - 25*m2*Ycut^7 + 25*Ycut^8))/
         (-1 + Ycut)^3 + (m2*(36 + 57*m2 + 10*m2^2)*Log[m2])/12 - 
       (m2*(36 + 57*m2 + 10*m2^2)*Log[1 - Ycut])/12) + 
     c[VL]*(-1/90*((-1 + m2 + Ycut)*(104 + 224*m2 - 901*m2^2 - 301*m2^3 + 
           119*m2^4 - 25*m2^5 - 208*Ycut - 704*m2*Ycut + 2310*m2^2*Ycut + 
           809*m2^3*Ycut - 332*m2^4*Ycut + 75*m2^5*Ycut + 104*Ycut^2 + 
           660*m2*Ycut^2 - 1755*m2^2*Ycut^2 - 646*m2^3*Ycut^2 + 
           282*m2^4*Ycut^2 - 75*m2^5*Ycut^2 - 120*m2*Ycut^3 + 
           240*m2^2*Ycut^3 + 94*m2^3*Ycut^3 - 44*m2^4*Ycut^3 + 
           25*m2^5*Ycut^3 - 30*m2*Ycut^4 + 75*m2^2*Ycut^4 + 19*m2^3*Ycut^4 - 
           25*m2^4*Ycut^4 + 36*Ycut^5 - 84*m2*Ycut^5 - 144*m2^2*Ycut^5 + 
           25*m2^3*Ycut^5 - 47*Ycut^6 + 79*m2*Ycut^6 + 25*m2^2*Ycut^6 - 
           14*Ycut^7 - 25*m2*Ycut^7 + 25*Ycut^8))/(-1 + Ycut)^3 + 
       (2*m2*(-6 + 9*m2 + 10*m2^2)*Log[m2])/3 - 
       (2*m2*(-6 + 9*m2 + 10*m2^2)*Log[1 - Ycut])/3 + 
       c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(232 - 873*m2 + 207*m2^2 - 
            133*m2^3 + 27*m2^4 - 352*Ycut + 1285*m2*Ycut - 308*m2^2*Ycut + 
            239*m2^3*Ycut - 54*m2^4*Ycut + 60*Ycut^2 - 210*m2*Ycut^2 + 
            22*m2^2*Ycut^2 - 79*m2^3*Ycut^2 + 27*m2^4*Ycut^2 + 20*Ycut^3 - 
            90*m2*Ycut^3 + 52*m2^2*Ycut^3 - 27*m2^3*Ycut^3 + 10*Ycut^4 - 
            55*m2*Ycut^4 + 27*m2^2*Ycut^4 + 78*Ycut^5 + 33*m2*Ycut^5 - 
            48*Ycut^6))/(60*(-1 + Ycut)^2) + Sqrt[m2]*(-2 + 5*m2 + 6*m2^2)*
          Log[m2] - Sqrt[m2]*(-2 + 5*m2 + 6*m2^2)*Log[1 - Ycut])) + 
     c[SL]*(c[SR]*(-1/8*(Sqrt[m2]*(-1 + m2 + Ycut)*(-80 - 115*m2 + 21*m2^2 - 
             7*m2^3 + m2^4 + 104*Ycut + 191*m2*Ycut - 36*m2^2*Ycut + 
             13*m2^3*Ycut - 2*m2^4*Ycut - 12*Ycut^2 - 54*m2*Ycut^2 + 
             10*m2^2*Ycut^2 - 5*m2^3*Ycut^2 + m2^4*Ycut^2 - 4*Ycut^3 - 
             14*m2*Ycut^3 + 4*m2^2*Ycut^3 - m2^3*Ycut^3 - 2*Ycut^4 - 
             5*m2*Ycut^4 + m2^2*Ycut^4 - 6*Ycut^5 + 3*m2*Ycut^5))/
           (-1 + Ycut)^2 - (3*Sqrt[m2]*(2 + 11*m2 + 2*m2^2)*Log[m2])/2 + 
         (3*Sqrt[m2]*(2 + 11*m2 + 2*m2^2)*Log[1 - Ycut])/2) + 
       c[T]*(((-1 + m2 + Ycut)*(2 + 2402*m2 + 3917*m2^2 - 403*m2^3 + 
            167*m2^4 - 25*m2^5 - 4*Ycut - 5522*m2*Ycut - 10290*m2^2*Ycut + 
            1067*m2^3*Ycut - 476*m2^4*Ycut + 75*m2^5*Ycut + 2*Ycut^2 + 
            3480*m2*Ycut^2 + 8085*m2^2*Ycut^2 - 808*m2^3*Ycut^2 + 
            426*m2^4*Ycut^2 - 75*m2^5*Ycut^2 - 240*m2*Ycut^3 - 
            1260*m2^2*Ycut^3 + 52*m2^3*Ycut^3 - 92*m2^4*Ycut^3 + 
            25*m2^5*Ycut^3 - 60*m2*Ycut^4 - 285*m2^2*Ycut^4 + 
            67*m2^3*Ycut^4 - 25*m2^4*Ycut^4 - 72*Ycut^5 - 312*m2*Ycut^5 + 
            258*m2^2*Ycut^5 + 25*m2^3*Ycut^5 + 244*Ycut^6 + 352*m2*Ycut^6 - 
            125*m2^2*Ycut^6 - 272*Ycut^7 - 100*m2*Ycut^7 + 100*Ycut^8))/
          (360*(-1 + Ycut)^3) + (m2*(12 + 69*m2 + 20*m2^2)*Log[m2])/6 - 
         (m2*(12 + 69*m2 + 20*m2^2)*Log[1 - Ycut])/6))))/mb^2 - 
 (-4 + 54*m2 + 195*m2^2 - 240*m2^3 - 6*m2^5 + m2^6 + 8*Ycut - 108*m2*Ycut - 
   570*m2^2*Ycut + 360*m2^3*Ycut + 12*m2^5*Ycut - 2*m2^6*Ycut - 4*Ycut^2 + 
   54*m2*Ycut^2 + 465*m2^2*Ycut^2 - 60*m2^3*Ycut^2 - 6*m2^5*Ycut^2 + 
   m2^6*Ycut^2 - 60*m2^2*Ycut^3 - 40*m2^3*Ycut^3 - 15*m2^2*Ycut^4 - 
   10*m2^3*Ycut^4 + 9*Ycut^5 - 9*m2*Ycut^5 - 15*m2^2*Ycut^5 + 5*m2^3*Ycut^5 - 
   23*Ycut^6 + 18*m2*Ycut^6 + 19*Ycut^7 - 9*m2*Ycut^7 - 5*Ycut^8 - 
   30*api*X1El[2, SM^2, Ycut, m2, mu2hat] + 
   60*api*Ycut*X1El[2, SM^2, Ycut, m2, mu2hat] - 
   30*api*Ycut^2*X1El[2, SM^2, Ycut, m2, mu2hat])/(30*(-1 + Ycut)^2) + 
 c[SL]^2*(-1/2*(m2^2*(3 + 2*m2)*Log[m2]) + (m2^2*(3 + 2*m2)*Log[1 - Ycut])/
    2 - (-4 + 54*m2 + 195*m2^2 - 240*m2^3 - 6*m2^5 + m2^6 + 8*Ycut - 
     108*m2*Ycut - 570*m2^2*Ycut + 360*m2^3*Ycut + 12*m2^5*Ycut - 
     2*m2^6*Ycut - 4*Ycut^2 + 54*m2*Ycut^2 + 465*m2^2*Ycut^2 - 
     60*m2^3*Ycut^2 - 6*m2^5*Ycut^2 + m2^6*Ycut^2 - 60*m2^2*Ycut^3 - 
     40*m2^3*Ycut^3 - 15*m2^2*Ycut^4 - 10*m2^3*Ycut^4 + 9*Ycut^5 - 
     9*m2*Ycut^5 - 15*m2^2*Ycut^5 + 5*m2^3*Ycut^5 - 23*Ycut^6 + 
     18*m2*Ycut^6 + 19*Ycut^7 - 9*m2*Ycut^7 - 5*Ycut^8 - 
     120*api*X1El[2, c[SL]^2, Ycut, m2, mu2hat] + 
     240*api*Ycut*X1El[2, c[SL]^2, Ycut, m2, mu2hat] - 
     120*api*Ycut^2*X1El[2, c[SL]^2, Ycut, m2, mu2hat])/
    (120*(-1 + Ycut)^2)) + c[SR]^2*(-1/2*(m2^2*(3 + 2*m2)*Log[m2]) + 
   (m2^2*(3 + 2*m2)*Log[1 - Ycut])/2 - 
   (-4 + 54*m2 + 195*m2^2 - 240*m2^3 - 6*m2^5 + m2^6 + 8*Ycut - 108*m2*Ycut - 
     570*m2^2*Ycut + 360*m2^3*Ycut + 12*m2^5*Ycut - 2*m2^6*Ycut - 4*Ycut^2 + 
     54*m2*Ycut^2 + 465*m2^2*Ycut^2 - 60*m2^3*Ycut^2 - 6*m2^5*Ycut^2 + 
     m2^6*Ycut^2 - 60*m2^2*Ycut^3 - 40*m2^3*Ycut^3 - 15*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 9*Ycut^5 - 9*m2*Ycut^5 - 15*m2^2*Ycut^5 + 
     5*m2^3*Ycut^5 - 23*Ycut^6 + 18*m2*Ycut^6 + 19*Ycut^7 - 9*m2*Ycut^7 - 
     5*Ycut^8 - 120*api*X1El[2, c[SR]^2, Ycut, m2, mu2hat] + 
     240*api*Ycut*X1El[2, c[SR]^2, Ycut, m2, mu2hat] - 
     120*api*Ycut^2*X1El[2, c[SR]^2, Ycut, m2, mu2hat])/
    (120*(-1 + Ycut)^2)) + 
 c[SL]*(c[SR]*(3*m2^(3/2)*(1 + 2*m2)*Log[m2] - 3*m2^(3/2)*(1 + 2*m2)*
      Log[1 - Ycut] - (6*Sqrt[m2] + 125*m2^(3/2) - 80*m2^(5/2) - 
       60*m2^(7/2) + 10*m2^(9/2) - m2^(11/2) - 6*Sqrt[m2]*Ycut - 
       185*m2^(3/2)*Ycut - 40*m2^(5/2)*Ycut + 60*m2^(7/2)*Ycut - 
       10*m2^(9/2)*Ycut + m2^(11/2)*Ycut + 30*m2^(3/2)*Ycut^2 + 
       60*m2^(5/2)*Ycut^2 + 10*m2^(3/2)*Ycut^3 + 20*m2^(5/2)*Ycut^3 + 
       5*m2^(3/2)*Ycut^4 + 10*m2^(5/2)*Ycut^4 - 6*Sqrt[m2]*Ycut^5 + 
       15*m2^(3/2)*Ycut^5 + 6*Sqrt[m2]*Ycut^6 + 
       20*api*X1El[2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       20*api*Ycut*X1El[2, c[SL]*c[SR], Ycut, m2, mu2hat])/
      (20*(-1 + Ycut))) + c[T]*(-(m2^2*(3 + 4*m2)*Log[m2]) + 
     m2^2*(3 + 4*m2)*Log[1 - Ycut] + (2 - 36*m2 - 285*m2^2 + 240*m2^3 + 
       90*m2^4 - 12*m2^5 + m2^6 - 4*Ycut + 72*m2*Ycut + 750*m2^2*Ycut - 
       240*m2^3*Ycut - 180*m2^4*Ycut + 24*m2^5*Ycut - 2*m2^6*Ycut + 
       2*Ycut^2 - 36*m2*Ycut^2 - 555*m2^2*Ycut^2 - 120*m2^3*Ycut^2 + 
       90*m2^4*Ycut^2 - 12*m2^5*Ycut^2 + m2^6*Ycut^2 + 60*m2^2*Ycut^3 + 
       80*m2^3*Ycut^3 + 15*m2^2*Ycut^4 + 20*m2^3*Ycut^4 + 18*Ycut^5 - 
       54*m2*Ycut^5 + 60*m2^2*Ycut^5 - 10*m2^3*Ycut^5 - 56*Ycut^6 + 
       108*m2*Ycut^6 - 45*m2^2*Ycut^6 + 58*Ycut^7 - 54*m2*Ycut^7 - 
       20*Ycut^8 + 60*api*X1El[2, c[SL]*c[T], Ycut, m2, mu2hat] - 
       120*api*Ycut*X1El[2, c[SL]*c[T], Ycut, m2, mu2hat] + 
       60*api*Ycut^2*X1El[2, c[SL]*c[T], Ycut, m2, mu2hat])/
      (60*(-1 + Ycut)^2))) + c[T]^2*(-16*m2^2*(3 + m2)*Log[m2] + 
   16*m2^2*(3 + m2)*Log[1 - Ycut] - (-20 + 252*m2 + 600*m2^2 - 960*m2^3 + 
     180*m2^4 - 60*m2^5 + 8*m2^6 + 40*Ycut - 504*m2*Ycut - 1920*m2^2*Ycut + 
     1680*m2^3*Ycut - 360*m2^4*Ycut + 120*m2^5*Ycut - 16*m2^6*Ycut - 
     20*Ycut^2 + 252*m2*Ycut^2 + 1680*m2^2*Ycut^2 - 600*m2^3*Ycut^2 + 
     180*m2^4*Ycut^2 - 60*m2^5*Ycut^2 + 8*m2^6*Ycut^2 - 240*m2^2*Ycut^3 - 
     80*m2^3*Ycut^3 - 60*m2^2*Ycut^4 - 20*m2^3*Ycut^4 + 90*Ycut^5 - 
     162*m2*Ycut^5 + 30*m2^2*Ycut^5 + 10*m2^3*Ycut^5 - 250*Ycut^6 + 
     324*m2*Ycut^6 - 90*m2^2*Ycut^6 + 230*Ycut^7 - 162*m2*Ycut^7 - 
     70*Ycut^8 - 15*api*X1El[2, c[T]^2, Ycut, m2, mu2hat] + 
     30*api*Ycut*X1El[2, c[T]^2, Ycut, m2, mu2hat] - 
     15*api*Ycut^2*X1El[2, c[T]^2, Ycut, m2, mu2hat])/(15*(-1 + Ycut)^2)) + 
 c[VL]^2*(-2*m2^2*(3 + 2*m2)*Log[m2] + 2*m2^2*(3 + 2*m2)*Log[1 - Ycut] - 
   (-4 + 54*m2 + 195*m2^2 - 240*m2^3 - 6*m2^5 + m2^6 + 8*Ycut - 108*m2*Ycut - 
     570*m2^2*Ycut + 360*m2^3*Ycut + 12*m2^5*Ycut - 2*m2^6*Ycut - 4*Ycut^2 + 
     54*m2*Ycut^2 + 465*m2^2*Ycut^2 - 60*m2^3*Ycut^2 - 6*m2^5*Ycut^2 + 
     m2^6*Ycut^2 - 60*m2^2*Ycut^3 - 40*m2^3*Ycut^3 - 15*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 9*Ycut^5 - 9*m2*Ycut^5 - 15*m2^2*Ycut^5 + 
     5*m2^3*Ycut^5 - 23*Ycut^6 + 18*m2*Ycut^6 + 19*Ycut^7 - 9*m2*Ycut^7 - 
     5*Ycut^8 - 30*api*X1El[2, c[VL]^2, Ycut, m2, mu2hat] + 
     60*api*Ycut*X1El[2, c[VL]^2, Ycut, m2, mu2hat] - 
     30*api*Ycut^2*X1El[2, c[VL]^2, Ycut, m2, mu2hat])/(30*(-1 + Ycut)^2)) + 
 c[VR]*(-6*m2^(3/2)*(1 + 2*m2)*Log[m2] + 6*m2^(3/2)*(1 + 2*m2)*
    Log[1 - Ycut] + (6*Sqrt[m2] + 125*m2^(3/2) - 80*m2^(5/2) - 60*m2^(7/2) + 
     10*m2^(9/2) - m2^(11/2) - 6*Sqrt[m2]*Ycut - 185*m2^(3/2)*Ycut - 
     40*m2^(5/2)*Ycut + 60*m2^(7/2)*Ycut - 10*m2^(9/2)*Ycut + 
     m2^(11/2)*Ycut + 30*m2^(3/2)*Ycut^2 + 60*m2^(5/2)*Ycut^2 + 
     10*m2^(3/2)*Ycut^3 + 20*m2^(5/2)*Ycut^3 + 5*m2^(3/2)*Ycut^4 + 
     10*m2^(5/2)*Ycut^4 - 6*Sqrt[m2]*Ycut^5 + 15*m2^(3/2)*Ycut^5 + 
     6*Sqrt[m2]*Ycut^6 - 10*api*X1El[2, SM*c[VR], Ycut, m2, mu2hat] + 
     10*api*Ycut*X1El[2, SM*c[VR], Ycut, m2, mu2hat])/(10*(-1 + Ycut))) + 
 c[VL]*(-4*m2^2*(3 + 2*m2)*Log[m2] + 4*m2^2*(3 + 2*m2)*Log[1 - Ycut] - 
   (-4 + 54*m2 + 195*m2^2 - 240*m2^3 - 6*m2^5 + m2^6 + 8*Ycut - 108*m2*Ycut - 
     570*m2^2*Ycut + 360*m2^3*Ycut + 12*m2^5*Ycut - 2*m2^6*Ycut - 4*Ycut^2 + 
     54*m2*Ycut^2 + 465*m2^2*Ycut^2 - 60*m2^3*Ycut^2 - 6*m2^5*Ycut^2 + 
     m2^6*Ycut^2 - 60*m2^2*Ycut^3 - 40*m2^3*Ycut^3 - 15*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 9*Ycut^5 - 9*m2*Ycut^5 - 15*m2^2*Ycut^5 + 
     5*m2^3*Ycut^5 - 23*Ycut^6 + 18*m2*Ycut^6 + 19*Ycut^7 - 9*m2*Ycut^7 - 
     5*Ycut^8 - 15*api*X1El[2, SM*c[VL], Ycut, m2, mu2hat] + 
     30*api*Ycut*X1El[2, SM*c[VL], Ycut, m2, mu2hat] - 
     15*api*Ycut^2*X1El[2, SM*c[VL], Ycut, m2, mu2hat])/(15*(-1 + Ycut)^2) + 
   c[VR]*(-6*m2^(3/2)*(1 + 2*m2)*Log[m2] + 6*m2^(3/2)*(1 + 2*m2)*
      Log[1 - Ycut] + (6*Sqrt[m2] + 125*m2^(3/2) - 80*m2^(5/2) - 
       60*m2^(7/2) + 10*m2^(9/2) - m2^(11/2) - 6*Sqrt[m2]*Ycut - 
       185*m2^(3/2)*Ycut - 40*m2^(5/2)*Ycut + 60*m2^(7/2)*Ycut - 
       10*m2^(9/2)*Ycut + m2^(11/2)*Ycut + 30*m2^(3/2)*Ycut^2 + 
       60*m2^(5/2)*Ycut^2 + 10*m2^(3/2)*Ycut^3 + 20*m2^(5/2)*Ycut^3 + 
       5*m2^(3/2)*Ycut^4 + 10*m2^(5/2)*Ycut^4 - 6*Sqrt[m2]*Ycut^5 + 
       15*m2^(3/2)*Ycut^5 + 6*Sqrt[m2]*Ycut^6 - 
       10*api*X1El[2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       10*api*Ycut*X1El[2, c[VL]*c[VR], Ycut, m2, mu2hat])/
      (10*(-1 + Ycut)))) + c[VR]^2*(-3*m2^2*Log[m2] + 3*m2^2*Log[1 - Ycut] + 
   (2 - 24*m2 - 35*m2^2 + 80*m2^3 - 30*m2^4 + 8*m2^5 - m2^6 + 60*m2^2*Ycut + 
     30*m2^2*Ycut^2 + 20*m2^2*Ycut^3 + 15*m2^2*Ycut^4 - 12*Ycut^5 + 
     24*m2*Ycut^5 + 10*Ycut^6 + 20*api*X1El[2, c[VR]^2, Ycut, m2, mu2hat])/20)
