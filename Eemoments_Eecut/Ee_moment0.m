-12*m2^2*Log[m2] + 12*m2^2*Log[1 - Ycut] + 
 (rhoD*(-1/6*(77 - 88*m2 + 24*m2^2 - 8*m2^3 - 5*m2^4 - 433*Ycut + 
        440*m2*Ycut - 156*m2^2*Ycut + 40*m2^3*Ycut + 25*m2^4*Ycut + 
        986*Ycut^2 - 880*m2*Ycut^2 + 402*m2^2*Ycut^2 - 80*m2^3*Ycut^2 - 
        50*m2^4*Ycut^2 - 1114*Ycut^3 + 848*m2*Ycut^3 - 490*m2^2*Ycut^3 + 
        48*m2^3*Ycut^3 + 50*m2^4*Ycut^3 + 546*Ycut^4 - 328*m2*Ycut^4 + 
        306*m2^2*Ycut^4 - 20*m2^3*Ycut^4 - 25*m2^4*Ycut^4 + 78*Ycut^5 - 
        56*m2*Ycut^5 - 102*m2^2*Ycut^5 + 4*m2^3*Ycut^5 + 5*m2^4*Ycut^5 - 
        222*Ycut^6 + 80*m2*Ycut^6 + 16*m2^2*Ycut^6 + 94*Ycut^7 - 
        16*m2*Ycut^7 - 11*Ycut^8 - Ycut^9)/(-1 + Ycut)^5 + 
     2*(4 + 3*m2^2)*Log[m2] - 2*(4 + 3*m2^2)*Log[1 - Ycut] + 
     c[VR]*((2*Sqrt[m2]*(-75 + 117*m2 - 45*m2^2 + 3*m2^3 + 336*Ycut - 
          450*m2*Ycut + 162*m2^2*Ycut - 12*m2^3*Ycut - 576*Ycut^2 + 
          639*m2*Ycut^2 - 207*m2^2*Ycut^2 + 18*m2^3*Ycut^2 + 448*Ycut^3 - 
          390*m2*Ycut^3 + 110*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 115*Ycut^4 + 
          53*m2*Ycut^4 - 2*m2^2*Ycut^4 + 3*m2^3*Ycut^4 - 48*Ycut^5 + 
          52*m2*Ycut^5 - 12*m2^2*Ycut^5 + 38*Ycut^6 - 21*m2*Ycut^6 - 
          8*Ycut^7))/(3*(-1 + Ycut)^4) + 12*(-2 + m2)*Sqrt[m2]*(1 + m2)*
        Log[m2] - 12*(-2 + m2)*Sqrt[m2]*(1 + m2)*Log[1 - Ycut]) + 
     c[SL]^2*(-1/24*(197 - 184*m2 - 8*m2^3 - 5*m2^4 - 1081*Ycut + 
          824*m2*Ycut - 36*m2^2*Ycut + 40*m2^3*Ycut + 25*m2^4*Ycut + 
          2402*Ycut^2 - 1408*m2*Ycut^2 + 162*m2^2*Ycut^2 - 80*m2^3*Ycut^2 - 
          50*m2^4*Ycut^2 - 2690*Ycut^3 + 1056*m2*Ycut^3 - 250*m2^2*Ycut^3 + 
          48*m2^3*Ycut^3 + 50*m2^4*Ycut^3 + 1466*Ycut^4 - 200*m2*Ycut^4 + 
          186*m2^2*Ycut^4 - 20*m2^3*Ycut^4 - 25*m2^4*Ycut^4 - 202*Ycut^5 - 
          152*m2*Ycut^5 - 78*m2^2*Ycut^5 + 4*m2^3*Ycut^5 + 5*m2^4*Ycut^5 - 
          134*Ycut^6 + 64*m2*Ycut^6 + 16*m2^2*Ycut^6 + 38*Ycut^7 + 5*Ycut^8 - 
          Ycut^9)/(-1 + Ycut)^5 + ((8 + 8*m2 + 3*m2^2)*Log[m2])/2 + 
       ((-8 - 8*m2 - 3*m2^2)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/24*(197 - 184*m2 - 8*m2^3 - 5*m2^4 - 1081*Ycut + 
          824*m2*Ycut - 36*m2^2*Ycut + 40*m2^3*Ycut + 25*m2^4*Ycut + 
          2402*Ycut^2 - 1408*m2*Ycut^2 + 162*m2^2*Ycut^2 - 80*m2^3*Ycut^2 - 
          50*m2^4*Ycut^2 - 2690*Ycut^3 + 1056*m2*Ycut^3 - 250*m2^2*Ycut^3 + 
          48*m2^3*Ycut^3 + 50*m2^4*Ycut^3 + 1466*Ycut^4 - 200*m2*Ycut^4 + 
          186*m2^2*Ycut^4 - 20*m2^3*Ycut^4 - 25*m2^4*Ycut^4 - 202*Ycut^5 - 
          152*m2*Ycut^5 - 78*m2^2*Ycut^5 + 4*m2^3*Ycut^5 + 5*m2^4*Ycut^5 - 
          134*Ycut^6 + 64*m2*Ycut^6 + 16*m2^2*Ycut^6 + 38*Ycut^7 + 5*Ycut^8 - 
          Ycut^9)/(-1 + Ycut)^5 + ((8 + 8*m2 + 3*m2^2)*Log[m2])/2 + 
       ((-8 - 8*m2 - 3*m2^2)*Log[1 - Ycut])/2) + 
     c[VR]^2*(-1/6*(77 - 88*m2 + 24*m2^2 - 8*m2^3 - 5*m2^4 - 279*Ycut + 
          264*m2*Ycut - 108*m2^2*Ycut + 24*m2^3*Ycut + 15*m2^4*Ycut + 
          351*Ycut^2 - 264*m2*Ycut^2 + 162*m2^2*Ycut^2 - 24*m2^3*Ycut^2 - 
          15*m2^4*Ycut^2 - 133*Ycut^3 + 88*m2*Ycut^3 - 122*m2^2*Ycut^3 + 
          8*m2^3*Ycut^3 + 5*m2^4*Ycut^3 - 69*Ycut^4 + 30*m2^2*Ycut^4 + 
          63*Ycut^5 + 6*m2^2*Ycut^5 - 7*Ycut^6 - 3*Ycut^7)/(-1 + Ycut)^3 + 
       2*(4 + 3*m2^2)*Log[m2] - 2*(4 + 3*m2^2)*Log[1 - Ycut]) + 
     c[VL]^2*(-1/6*(77 - 88*m2 + 24*m2^2 - 8*m2^3 - 5*m2^4 - 433*Ycut + 
          440*m2*Ycut - 156*m2^2*Ycut + 40*m2^3*Ycut + 25*m2^4*Ycut + 
          986*Ycut^2 - 880*m2*Ycut^2 + 402*m2^2*Ycut^2 - 80*m2^3*Ycut^2 - 
          50*m2^4*Ycut^2 - 1114*Ycut^3 + 848*m2*Ycut^3 - 490*m2^2*Ycut^3 + 
          48*m2^3*Ycut^3 + 50*m2^4*Ycut^3 + 546*Ycut^4 - 328*m2*Ycut^4 + 
          306*m2^2*Ycut^4 - 20*m2^3*Ycut^4 - 25*m2^4*Ycut^4 + 78*Ycut^5 - 
          56*m2*Ycut^5 - 102*m2^2*Ycut^5 + 4*m2^3*Ycut^5 + 5*m2^4*Ycut^5 - 
          222*Ycut^6 + 80*m2*Ycut^6 + 16*m2^2*Ycut^6 + 94*Ycut^7 - 
          16*m2*Ycut^7 - 11*Ycut^8 - Ycut^9)/(-1 + Ycut)^5 + 
       2*(4 + 3*m2^2)*Log[m2] - 2*(4 + 3*m2^2)*Log[1 - Ycut]) + 
     c[T]^2*((-2*(111 - 168*m2 + 96*m2^2 - 24*m2^3 - 15*m2^4 - 651*Ycut + 
          936*m2*Ycut - 588*m2^2*Ycut + 120*m2^3*Ycut + 75*m2^4*Ycut + 
          1542*Ycut^2 - 2112*m2*Ycut^2 + 1446*m2^2*Ycut^2 - 240*m2^3*Ycut^2 - 
          150*m2^4*Ycut^2 - 1766*Ycut^3 + 2400*m2*Ycut^3 - 1838*m2^2*Ycut^3 + 
          208*m2^3*Ycut^3 + 150*m2^4*Ycut^3 + 722*Ycut^4 - 1336*m2*Ycut^4 + 
          1298*m2^2*Ycut^4 - 100*m2^3*Ycut^4 - 75*m2^4*Ycut^4 + 494*Ycut^5 + 
          216*m2*Ycut^5 - 478*m2^2*Ycut^5 + 20*m2^3*Ycut^5 + 15*m2^4*Ycut^5 - 
          714*Ycut^6 + 96*m2*Ycut^6 + 52*m2^2*Ycut^6 + 298*Ycut^7 - 
          32*m2*Ycut^7 + 12*m2^2*Ycut^7 - 29*Ycut^8 - 7*Ycut^9))/
        (3*(-1 + Ycut)^5) + 8*(8 - 8*m2 + 9*m2^2)*Log[m2] - 
       8*(8 - 8*m2 + 9*m2^2)*Log[1 - Ycut]) + 
     c[VL]*(-1/3*(77 - 88*m2 + 24*m2^2 - 8*m2^3 - 5*m2^4 - 433*Ycut + 
          440*m2*Ycut - 156*m2^2*Ycut + 40*m2^3*Ycut + 25*m2^4*Ycut + 
          986*Ycut^2 - 880*m2*Ycut^2 + 402*m2^2*Ycut^2 - 80*m2^3*Ycut^2 - 
          50*m2^4*Ycut^2 - 1114*Ycut^3 + 848*m2*Ycut^3 - 490*m2^2*Ycut^3 + 
          48*m2^3*Ycut^3 + 50*m2^4*Ycut^3 + 546*Ycut^4 - 328*m2*Ycut^4 + 
          306*m2^2*Ycut^4 - 20*m2^3*Ycut^4 - 25*m2^4*Ycut^4 + 78*Ycut^5 - 
          56*m2*Ycut^5 - 102*m2^2*Ycut^5 + 4*m2^3*Ycut^5 + 5*m2^4*Ycut^5 - 
          222*Ycut^6 + 80*m2*Ycut^6 + 16*m2^2*Ycut^6 + 94*Ycut^7 - 
          16*m2*Ycut^7 - 11*Ycut^8 - Ycut^9)/(-1 + Ycut)^5 + 
       4*(4 + 3*m2^2)*Log[m2] - 4*(4 + 3*m2^2)*Log[1 - Ycut] + 
       c[VR]*((2*Sqrt[m2]*(-75 + 117*m2 - 45*m2^2 + 3*m2^3 + 336*Ycut - 
            450*m2*Ycut + 162*m2^2*Ycut - 12*m2^3*Ycut - 576*Ycut^2 + 
            639*m2*Ycut^2 - 207*m2^2*Ycut^2 + 18*m2^3*Ycut^2 + 448*Ycut^3 - 
            390*m2*Ycut^3 + 110*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 115*Ycut^4 + 
            53*m2*Ycut^4 - 2*m2^2*Ycut^4 + 3*m2^3*Ycut^4 - 48*Ycut^5 + 
            52*m2*Ycut^5 - 12*m2^2*Ycut^5 + 38*Ycut^6 - 21*m2*Ycut^6 - 
            8*Ycut^7))/(3*(-1 + Ycut)^4) + 12*(-2 + m2)*Sqrt[m2]*(1 + m2)*
          Log[m2] - 12*(-2 + m2)*Sqrt[m2]*(1 + m2)*Log[1 - Ycut])) + 
     c[SL]*((Ycut^3*(16*m2 - 32*m2^2 + 16*m2^3 + Ycut - 56*m2*Ycut + 
          65*m2^2*Ycut - 10*m2^3*Ycut - 5*Ycut^2 + 72*m2*Ycut^2 - 
          37*m2^2*Ycut^2 + 2*m2^3*Ycut^2 + 10*Ycut^3 - 40*m2*Ycut^3 + 
          m2^2*Ycut^3 - 10*Ycut^4 + 8*m2*Ycut^4 + 3*m2^2*Ycut^4 + 5*Ycut^5 - 
          Ycut^6)*c[T])/(3*(-1 + Ycut)^5) + 
       c[SR]*((Sqrt[m2]*(143 - 153*m2 + 9*m2^2 + m2^3 - 632*Ycut + 
            522*m2*Ycut - 18*m2^2*Ycut - 4*m2^3*Ycut + 1068*Ycut^2 - 
            603*m2*Ycut^2 - 9*m2^2*Ycut^2 + 6*m2^3*Ycut^2 - 824*Ycut^3 + 
            222*m2*Ycut^3 + 34*m2^2*Ycut^3 - 4*m2^3*Ycut^3 + 239*Ycut^4 + 
            55*m2*Ycut^4 - 34*m2^2*Ycut^4 + m2^3*Ycut^4 + 24*Ycut^5 - 
            52*m2*Ycut^5 + 12*m2^2*Ycut^5 - 18*Ycut^6 + 9*m2*Ycut^6))/
          (3*(-1 + Ycut)^4) - 2*Sqrt[m2]*(-10 - 15*m2 + 3*m2^2)*Log[m2] + 
         2*Sqrt[m2]*(-10 - 15*m2 + 3*m2^2)*Log[1 - Ycut]))) + 
   rhoLS*(((-1 + m2 + Ycut)*(-9 + 15*m2 - 57*m2^2 + 15*m2^3 + 27*Ycut - 
        54*m2*Ycut + 213*m2^2*Ycut - 60*m2^3*Ycut - 27*Ycut^2 + 
        63*m2*Ycut^2 - 282*m2^2*Ycut^2 + 90*m2^3*Ycut^2 + 9*Ycut^3 - 
        24*m2*Ycut^3 + 186*m2^2*Ycut^3 - 60*m2^3*Ycut^3 - Ycut^4 - 
        m2*Ycut^4 - 49*m2^2*Ycut^4 + 15*m2^3*Ycut^4 + 3*Ycut^5 + 
        2*m2*Ycut^5 + m2^2*Ycut^5 - 3*Ycut^6 - m2*Ycut^6 + Ycut^7))/
      (6*(-1 + Ycut)^4) + (8*Sqrt[m2]*Ycut^3*(-1 + m2 + Ycut)*c[SR]*c[T])/
      (-1 + Ycut)^2 + 6*m2^2*Log[m2] - 6*m2^2*Log[1 - Ycut] + 
     c[SL]^2*(((-1 + m2 + Ycut)*(39 + 135*m2 - 81*m2^2 + 15*m2^3 - 117*Ycut - 
          510*m2*Ycut + 309*m2^2*Ycut - 60*m2^3*Ycut + 117*Ycut^2 + 
          687*m2*Ycut^2 - 426*m2^2*Ycut^2 + 90*m2^3*Ycut^2 - 39*Ycut^3 - 
          408*m2*Ycut^3 + 282*m2^2*Ycut^3 - 60*m2^3*Ycut^3 - Ycut^4 + 
          95*m2*Ycut^4 - 73*m2^2*Ycut^4 + 15*m2^3*Ycut^4 + 3*Ycut^5 + 
          2*m2*Ycut^5 + m2^2*Ycut^5 - 3*Ycut^6 - m2*Ycut^6 + Ycut^7))/
        (24*(-1 + Ycut)^4) + (3*(-4 + m2)*m2*Log[m2])/2 - 
       (3*(-4 + m2)*m2*Log[1 - Ycut])/2) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(39 + 135*m2 - 81*m2^2 + 15*m2^3 - 117*Ycut - 
          510*m2*Ycut + 309*m2^2*Ycut - 60*m2^3*Ycut + 117*Ycut^2 + 
          687*m2*Ycut^2 - 426*m2^2*Ycut^2 + 90*m2^3*Ycut^2 - 39*Ycut^3 - 
          408*m2*Ycut^3 + 282*m2^2*Ycut^3 - 60*m2^3*Ycut^3 - Ycut^4 + 
          95*m2*Ycut^4 - 73*m2^2*Ycut^4 + 15*m2^3*Ycut^4 + 3*Ycut^5 + 
          2*m2*Ycut^5 + m2^2*Ycut^5 - 3*Ycut^6 - m2*Ycut^6 + Ycut^7))/
        (24*(-1 + Ycut)^4) + (3*(-4 + m2)*m2*Log[m2])/2 - 
       (3*(-4 + m2)*m2*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 + 3*Ycut - 
          8*m2*Ycut + 33*m2^2*Ycut - 10*m2^3*Ycut - 9*m2^2*Ycut^2 + 
          5*m2^3*Ycut^2 - 5*m2^2*Ycut^3 - Ycut^4 - m2*Ycut^4 + Ycut^5))/
        (2*(-1 + Ycut)^2) + 6*m2^2*Log[m2] - 6*m2^2*Log[1 - Ycut]) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(-9 + 15*m2 - 57*m2^2 + 15*m2^3 + 27*Ycut - 
          54*m2*Ycut + 213*m2^2*Ycut - 60*m2^3*Ycut - 27*Ycut^2 + 
          63*m2*Ycut^2 - 282*m2^2*Ycut^2 + 90*m2^3*Ycut^2 + 9*Ycut^3 - 
          24*m2*Ycut^3 + 186*m2^2*Ycut^3 - 60*m2^3*Ycut^3 - Ycut^4 - 
          m2*Ycut^4 - 49*m2^2*Ycut^4 + 15*m2^3*Ycut^4 + 3*Ycut^5 + 
          2*m2*Ycut^5 + m2^2*Ycut^5 - 3*Ycut^6 - m2*Ycut^6 + Ycut^7))/
        (6*(-1 + Ycut)^4) + 6*m2^2*Log[m2] - 6*m2^2*Log[1 - Ycut]) + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(-75 - 75*m2 - 147*m2^2 + 45*m2^3 + 
          225*Ycut + 294*m2*Ycut + 543*m2^2*Ycut - 180*m2^3*Ycut - 
          225*Ycut^2 - 435*m2*Ycut^2 - 702*m2^2*Ycut^2 + 270*m2^3*Ycut^2 + 
          75*Ycut^3 + 312*m2*Ycut^3 + 366*m2^2*Ycut^3 - 180*m2^3*Ycut^3 - 
          7*Ycut^4 - 103*m2*Ycut^4 - 19*m2^2*Ycut^4 + 45*m2^3*Ycut^4 + 
          21*Ycut^5 + 14*m2*Ycut^5 - 29*m2^2*Ycut^5 - 21*Ycut^6 - 
          7*m2*Ycut^6 + 7*Ycut^7))/(3*(-1 + Ycut)^4) + 
       24*m2*(4 + 3*m2)*Log[m2] - 24*m2*(4 + 3*m2)*Log[1 - Ycut]) + 
     c[VR]*((-2*Sqrt[m2]*(-1 + m2 + Ycut)*(13 - 14*m2 + 13*m2^2 - 25*Ycut + 
          33*m2*Ycut - 26*m2^2*Ycut + 6*Ycut^2 - 15*m2*Ycut^2 + 
          13*m2^2*Ycut^2 + 2*Ycut^3 + 5*m2*Ycut^3 + 4*Ycut^4))/
        (3*(-1 + Ycut)^2) + 4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[m2] - 
       4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[1 - Ycut]) + 
     c[SL]*((Ycut^3*(-1 + m2 + Ycut)*(24*m2^2 + Ycut + m2*Ycut - 
          26*m2^2*Ycut - 3*Ycut^2 - 2*m2*Ycut^2 + 8*m2^2*Ycut^2 + 3*Ycut^3 + 
          m2*Ycut^3 - Ycut^4)*c[T])/(3*(-1 + Ycut)^4) + 
       c[SR]*((3*Sqrt[m2]*(-1 + m2 + Ycut)*(-11 - 2*m2 + m2^2 + 15*Ycut + 
            5*m2*Ycut - 2*m2^2*Ycut - 2*Ycut^2 - 3*m2*Ycut^2 + m2^2*Ycut^2 - 
            2*Ycut^3 + m2*Ycut^3))/(-1 + Ycut)^2 - 6*Sqrt[m2]*
          (-2 - 5*m2 + m2^2)*Log[m2] + 6*Sqrt[m2]*(-2 - 5*m2 + m2^2)*
          Log[1 - Ycut])) + 
     c[VL]*(((-1 + m2 + Ycut)*(-9 + 15*m2 - 57*m2^2 + 15*m2^3 + 27*Ycut - 
          54*m2*Ycut + 213*m2^2*Ycut - 60*m2^3*Ycut - 27*Ycut^2 + 
          63*m2*Ycut^2 - 282*m2^2*Ycut^2 + 90*m2^3*Ycut^2 + 9*Ycut^3 - 
          24*m2*Ycut^3 + 186*m2^2*Ycut^3 - 60*m2^3*Ycut^3 - Ycut^4 - 
          m2*Ycut^4 - 49*m2^2*Ycut^4 + 15*m2^3*Ycut^4 + 3*Ycut^5 + 
          2*m2*Ycut^5 + m2^2*Ycut^5 - 3*Ycut^6 - m2*Ycut^6 + Ycut^7))/
        (3*(-1 + Ycut)^4) + 12*m2^2*Log[m2] - 12*m2^2*Log[1 - Ycut] + 
       c[VR]*((-2*Sqrt[m2]*(-1 + m2 + Ycut)*(13 - 14*m2 + 13*m2^2 - 25*Ycut + 
            33*m2*Ycut - 26*m2^2*Ycut + 6*Ycut^2 - 15*m2*Ycut^2 + 
            13*m2^2*Ycut^2 + 2*Ycut^3 + 5*m2*Ycut^3 + 4*Ycut^4))/
          (3*(-1 + Ycut)^2) + 4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[m2] - 
         4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[1 - Ycut]))))/mb^3 + 
 (mupi*(((-1 + m2 + Ycut)*(3 - 21*m2 - 21*m2^2 + 3*m2^3 - 9*Ycut + 
        66*m2*Ycut + 81*m2^2*Ycut - 12*m2^3*Ycut + 9*Ycut^2 - 69*m2*Ycut^2 - 
        114*m2^2*Ycut^2 + 18*m2^3*Ycut^2 - 3*Ycut^3 + 24*m2*Ycut^3 + 
        66*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 5*Ycut^4 - 5*m2*Ycut^4 - 
        29*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 15*Ycut^5 + 10*m2*Ycut^5 + 
        5*m2^2*Ycut^5 - 15*Ycut^6 - 5*m2*Ycut^6 + 5*Ycut^7))/
      (6*(-1 + Ycut)^4) + 6*m2^2*Log[m2] - 6*m2^2*Log[1 - Ycut] + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(9 - 63*m2 - 63*m2^2 + 9*m2^3 - 27*Ycut + 
          198*m2*Ycut + 243*m2^2*Ycut - 36*m2^3*Ycut + 27*Ycut^2 - 
          207*m2*Ycut^2 - 342*m2^2*Ycut^2 + 54*m2^3*Ycut^2 - 9*Ycut^3 + 
          72*m2*Ycut^3 + 198*m2^2*Ycut^3 - 36*m2^3*Ycut^3 - 35*Ycut^4 - 
          35*m2*Ycut^4 - 47*m2^2*Ycut^4 + 9*m2^3*Ycut^4 + 105*Ycut^5 + 
          70*m2*Ycut^5 - m2^2*Ycut^5 - 105*Ycut^6 - 35*m2*Ycut^6 + 
          35*Ycut^7))/(3*(-1 + Ycut)^4) + 72*m2^2*Log[m2] - 
       72*m2^2*Log[1 - Ycut]) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(1 - 7*m2 - 7*m2^2 + m2^3 - Ycut + 
          8*m2*Ycut + 13*m2^2*Ycut - 2*m2^3*Ycut - 5*m2^2*Ycut^2 + 
          m2^3*Ycut^2 - m2^2*Ycut^3 - 5*Ycut^4 - 5*m2*Ycut^4 + 5*Ycut^5))/
        (2*(-1 + Ycut)^2) + 6*m2^2*Log[m2] - 6*m2^2*Log[1 - Ycut]) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(3 - 21*m2 - 21*m2^2 + 3*m2^3 - 9*Ycut + 
          66*m2*Ycut + 81*m2^2*Ycut - 12*m2^3*Ycut + 9*Ycut^2 - 
          69*m2*Ycut^2 - 114*m2^2*Ycut^2 + 18*m2^3*Ycut^2 - 3*Ycut^3 + 
          24*m2*Ycut^3 + 66*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 5*Ycut^4 - 
          5*m2*Ycut^4 - 29*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 15*Ycut^5 + 
          10*m2*Ycut^5 + 5*m2^2*Ycut^5 - 15*Ycut^6 - 5*m2*Ycut^6 + 5*Ycut^7))/
        (6*(-1 + Ycut)^4) + 6*m2^2*Log[m2] - 6*m2^2*Log[1 - Ycut]) + 
     c[SL]^2*(((-1 + m2 + Ycut)*(3 - 21*m2 - 21*m2^2 + 3*m2^3 - 9*Ycut + 
          66*m2*Ycut + 81*m2^2*Ycut - 12*m2^3*Ycut + 9*Ycut^2 - 
          69*m2*Ycut^2 - 114*m2^2*Ycut^2 + 18*m2^3*Ycut^2 - 3*Ycut^3 + 
          24*m2*Ycut^3 + 66*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 5*Ycut^4 - 
          5*m2*Ycut^4 - 29*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 15*Ycut^5 + 
          10*m2*Ycut^5 + 5*m2^2*Ycut^5 - 15*Ycut^6 - 5*m2*Ycut^6 + 5*Ycut^7))/
        (24*(-1 + Ycut)^4) + (3*m2^2*Log[m2])/2 - (3*m2^2*Log[1 - Ycut])/2) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(3 - 21*m2 - 21*m2^2 + 3*m2^3 - 9*Ycut + 
          66*m2*Ycut + 81*m2^2*Ycut - 12*m2^3*Ycut + 9*Ycut^2 - 
          69*m2*Ycut^2 - 114*m2^2*Ycut^2 + 18*m2^3*Ycut^2 - 3*Ycut^3 + 
          24*m2*Ycut^3 + 66*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 5*Ycut^4 - 
          5*m2*Ycut^4 - 29*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 15*Ycut^5 + 
          10*m2*Ycut^5 + 5*m2^2*Ycut^5 - 15*Ycut^6 - 5*m2*Ycut^6 + 5*Ycut^7))/
        (24*(-1 + Ycut)^4) + (3*m2^2*Log[m2])/2 - (3*m2^2*Log[1 - Ycut])/2) + 
     c[VR]*((-2*Sqrt[m2]*(-1 + m2 + Ycut)*(-1 - 10*m2 - m2^2 + 2*Ycut + 
          25*m2*Ycut + 3*m2^2*Ycut - Ycut^2 - 18*m2*Ycut^2 - 3*m2^2*Ycut^2 + 
          2*m2*Ycut^3 + m2^2*Ycut^3 + 3*m2*Ycut^4))/(-1 + Ycut)^3 + 
       12*m2^(3/2)*(1 + m2)*Log[m2] - 12*m2^(3/2)*(1 + m2)*Log[1 - Ycut]) + 
     c[VL]*(((-1 + m2 + Ycut)*(3 - 21*m2 - 21*m2^2 + 3*m2^3 - 9*Ycut + 
          66*m2*Ycut + 81*m2^2*Ycut - 12*m2^3*Ycut + 9*Ycut^2 - 
          69*m2*Ycut^2 - 114*m2^2*Ycut^2 + 18*m2^3*Ycut^2 - 3*Ycut^3 + 
          24*m2*Ycut^3 + 66*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 5*Ycut^4 - 
          5*m2*Ycut^4 - 29*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 15*Ycut^5 + 
          10*m2*Ycut^5 + 5*m2^2*Ycut^5 - 15*Ycut^6 - 5*m2*Ycut^6 + 5*Ycut^7))/
        (3*(-1 + Ycut)^4) + 12*m2^2*Log[m2] - 12*m2^2*Log[1 - Ycut] + 
       c[VR]*((-2*Sqrt[m2]*(-1 + m2 + Ycut)*(-1 - 10*m2 - m2^2 + 2*Ycut + 
            25*m2*Ycut + 3*m2^2*Ycut - Ycut^2 - 18*m2*Ycut^2 - 
            3*m2^2*Ycut^2 + 2*m2*Ycut^3 + m2^2*Ycut^3 + 3*m2*Ycut^4))/
          (-1 + Ycut)^3 + 12*m2^(3/2)*(1 + m2)*Log[m2] - 
         12*m2^(3/2)*(1 + m2)*Log[1 - Ycut])) + 
     c[SL]*((Ycut^4*(-1 + m2 + Ycut)*(5 + 5*m2 - 10*m2^2 - 15*Ycut - 
          10*m2*Ycut + 4*m2^2*Ycut + 15*Ycut^2 + 5*m2*Ycut^2 - 5*Ycut^3)*
         c[T])/(3*(-1 + Ycut)^4) + 
       c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-1 - 10*m2 - m2^2 + 2*Ycut + 
            25*m2*Ycut + 3*m2^2*Ycut - Ycut^2 - 18*m2*Ycut^2 - 
            3*m2^2*Ycut^2 + 2*m2*Ycut^3 + m2^2*Ycut^3 + 3*m2*Ycut^4))/
          (-1 + Ycut)^3 - 6*m2^(3/2)*(1 + m2)*Log[m2] + 6*m2^(3/2)*(1 + m2)*
          Log[1 - Ycut]))) + 
   muG*(-1/6*((-1 + m2 + Ycut)*(9 - 15*m2 + 57*m2^2 - 15*m2^3 - 18*Ycut + 
         39*m2*Ycut - 156*m2^2*Ycut + 45*m2^3*Ycut + 9*Ycut^2 - 
         24*m2*Ycut^2 + 126*m2^2*Ycut^2 - 45*m2^3*Ycut^2 + 8*Ycut^3 - 
         16*m2*Ycut^3 - 52*m2^2*Ycut^3 + 15*m2^3*Ycut^3 - 11*Ycut^4 + 
         21*m2*Ycut^4 + 5*m2^2*Ycut^4 - 2*Ycut^5 - 5*m2*Ycut^5 + 5*Ycut^6))/
       (-1 + Ycut)^3 - 6*m2^2*Log[m2] + 6*m2^2*Log[1 - Ycut] + 
     c[SL]^2*(-1/24*((-1 + m2 + Ycut)*(-39 - 135*m2 + 81*m2^2 - 15*m2^3 + 
           78*Ycut + 375*m2*Ycut - 228*m2^2*Ycut + 45*m2^3*Ycut - 39*Ycut^2 - 
           312*m2*Ycut^2 + 198*m2^2*Ycut^2 - 45*m2^3*Ycut^2 + 8*Ycut^3 + 
           80*m2*Ycut^3 - 76*m2^2*Ycut^3 + 15*m2^3*Ycut^3 - 11*Ycut^4 - 
           3*m2*Ycut^4 + 5*m2^2*Ycut^4 - 2*Ycut^5 - 5*m2*Ycut^5 + 5*Ycut^6))/
         (-1 + Ycut)^3 - (3*(-4 + m2)*m2*Log[m2])/2 + 
       (3*(-4 + m2)*m2*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/24*((-1 + m2 + Ycut)*(-39 - 135*m2 + 81*m2^2 - 15*m2^3 + 
           78*Ycut + 375*m2*Ycut - 228*m2^2*Ycut + 45*m2^3*Ycut - 39*Ycut^2 - 
           312*m2*Ycut^2 + 198*m2^2*Ycut^2 - 45*m2^3*Ycut^2 + 8*Ycut^3 + 
           80*m2*Ycut^3 - 76*m2^2*Ycut^3 + 15*m2^3*Ycut^3 - 11*Ycut^4 - 
           3*m2*Ycut^4 + 5*m2^2*Ycut^4 - 2*Ycut^5 - 5*m2*Ycut^5 + 5*Ycut^6))/
         (-1 + Ycut)^3 - (3*(-4 + m2)*m2*Log[m2])/2 + 
       (3*(-4 + m2)*m2*Log[1 - Ycut])/2) + 
     c[VR]^2*(-1/2*((-1 + m2 + Ycut)*(3 - 5*m2 + 19*m2^2 - 5*m2^3 + 
           3*m2*Ycut - 14*m2^2*Ycut + 5*m2^3*Ycut + 3*m2*Ycut^2 - 
           5*m2^2*Ycut^2 - 8*Ycut^3 - 5*m2*Ycut^3 + 5*Ycut^4))/(-1 + Ycut) - 
       6*m2^2*Log[m2] + 6*m2^2*Log[1 - Ycut]) + 
     c[VL]^2*(-1/6*((-1 + m2 + Ycut)*(9 - 15*m2 + 57*m2^2 - 15*m2^3 - 
           18*Ycut + 39*m2*Ycut - 156*m2^2*Ycut + 45*m2^3*Ycut + 9*Ycut^2 - 
           24*m2*Ycut^2 + 126*m2^2*Ycut^2 - 45*m2^3*Ycut^2 + 8*Ycut^3 - 
           16*m2*Ycut^3 - 52*m2^2*Ycut^3 + 15*m2^3*Ycut^3 - 11*Ycut^4 + 
           21*m2*Ycut^4 + 5*m2^2*Ycut^4 - 2*Ycut^5 - 5*m2*Ycut^5 + 5*Ycut^6))/
         (-1 + Ycut)^3 - 6*m2^2*Log[m2] + 6*m2^2*Log[1 - Ycut]) + 
     c[T]^2*((-2*(-1 + m2 + Ycut)*(75 + 75*m2 + 147*m2^2 - 45*m2^3 - 
          150*Ycut - 219*m2*Ycut - 396*m2^2*Ycut + 135*m2^3*Ycut + 
          75*Ycut^2 + 216*m2*Ycut^2 + 306*m2^2*Ycut^2 - 135*m2^3*Ycut^2 - 
          88*Ycut^3 - 112*m2*Ycut^3 - 52*m2^2*Ycut^3 + 45*m2^3*Ycut^3 + 
          211*Ycut^4 + 75*m2*Ycut^4 - 25*m2^2*Ycut^4 - 158*Ycut^5 - 
          35*m2*Ycut^5 + 35*Ycut^6))/(3*(-1 + Ycut)^3) - 
       24*m2*(4 + 3*m2)*Log[m2] + 24*m2*(4 + 3*m2)*Log[1 - Ycut]) + 
     c[VR]*((2*Sqrt[m2]*(-1 + m2 + Ycut)*(13 - 14*m2 + 13*m2^2 - 25*Ycut + 
          33*m2*Ycut - 26*m2^2*Ycut + 6*Ycut^2 - 15*m2*Ycut^2 + 
          13*m2^2*Ycut^2 + 14*Ycut^3 + 5*m2*Ycut^3 - 8*Ycut^4))/
        (3*(-1 + Ycut)^2) - 4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[m2] + 
       4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[1 - Ycut]) + 
     c[SL]*(-1/3*(Ycut^3*(-1 + m2 + Ycut)^2*(-4 - 20*m2 + 9*Ycut + 
           10*m2*Ycut - 5*Ycut^2)*c[T])/(-1 + Ycut)^3 + 
       c[SR]*((-3*Sqrt[m2]*(-1 + m2 + Ycut)*(-11 - 2*m2 + m2^2 + 15*Ycut + 
            5*m2*Ycut - 2*m2^2*Ycut - 2*Ycut^2 - 3*m2*Ycut^2 + m2^2*Ycut^2 - 
            2*Ycut^3 + m2*Ycut^3))/(-1 + Ycut)^2 + 6*Sqrt[m2]*
          (-2 - 5*m2 + m2^2)*Log[m2] - 6*Sqrt[m2]*(-2 - 5*m2 + m2^2)*
          Log[1 - Ycut])) + 
     c[VL]*(-1/3*((-1 + m2 + Ycut)*(9 - 15*m2 + 57*m2^2 - 15*m2^3 - 18*Ycut + 
           39*m2*Ycut - 156*m2^2*Ycut + 45*m2^3*Ycut + 9*Ycut^2 - 
           24*m2*Ycut^2 + 126*m2^2*Ycut^2 - 45*m2^3*Ycut^2 + 8*Ycut^3 - 
           16*m2*Ycut^3 - 52*m2^2*Ycut^3 + 15*m2^3*Ycut^3 - 11*Ycut^4 + 
           21*m2*Ycut^4 + 5*m2^2*Ycut^4 - 2*Ycut^5 - 5*m2*Ycut^5 + 5*Ycut^6))/
         (-1 + Ycut)^3 - 12*m2^2*Log[m2] + 12*m2^2*Log[1 - Ycut] + 
       c[VR]*((2*Sqrt[m2]*(-1 + m2 + Ycut)*(13 - 14*m2 + 13*m2^2 - 25*Ycut + 
            33*m2*Ycut - 26*m2^2*Ycut + 6*Ycut^2 - 15*m2*Ycut^2 + 
            13*m2^2*Ycut^2 + 14*Ycut^3 + 5*m2*Ycut^3 - 8*Ycut^4))/
          (3*(-1 + Ycut)^2) - 4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[m2] + 
         4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*Log[1 - Ycut]))))/mb^2 - 
 (-1 + 8*m2 - 8*m2^3 + m2^4 + 2*Ycut - 16*m2*Ycut - 12*m2^2*Ycut + 
   16*m2^3*Ycut - 2*m2^4*Ycut - Ycut^2 + 8*m2*Ycut^2 + 18*m2^2*Ycut^2 - 
   8*m2^3*Ycut^2 + m2^4*Ycut^2 + 2*Ycut^3 - 2*m2*Ycut^3 - 6*m2^2*Ycut^3 + 
   2*m2^3*Ycut^3 - 5*Ycut^4 + 4*m2*Ycut^4 + 4*Ycut^5 - 2*m2*Ycut^5 - Ycut^6 - 
   api*X1El[0, SM^2, Ycut, m2, mu2hat] + 
   2*api*Ycut*X1El[0, SM^2, Ycut, m2, mu2hat] - 
   api*Ycut^2*X1El[0, SM^2, Ycut, m2, mu2hat])/(-1 + Ycut)^2 + 
 c[SL]^2*(-3*m2^2*Log[m2] + 3*m2^2*Log[1 - Ycut] - 
   (-1 + 8*m2 - 8*m2^3 + m2^4 + 2*Ycut - 16*m2*Ycut - 12*m2^2*Ycut + 
     16*m2^3*Ycut - 2*m2^4*Ycut - Ycut^2 + 8*m2*Ycut^2 + 18*m2^2*Ycut^2 - 
     8*m2^3*Ycut^2 + m2^4*Ycut^2 + 2*Ycut^3 - 2*m2*Ycut^3 - 6*m2^2*Ycut^3 + 
     2*m2^3*Ycut^3 - 5*Ycut^4 + 4*m2*Ycut^4 + 4*Ycut^5 - 2*m2*Ycut^5 - 
     Ycut^6 - 4*api*X1El[0, c[SL]^2, Ycut, m2, mu2hat] + 
     8*api*Ycut*X1El[0, c[SL]^2, Ycut, m2, mu2hat] - 
     4*api*Ycut^2*X1El[0, c[SL]^2, Ycut, m2, mu2hat])/(4*(-1 + Ycut)^2)) + 
 c[SR]^2*(-3*m2^2*Log[m2] + 3*m2^2*Log[1 - Ycut] - 
   (-1 + 8*m2 - 8*m2^3 + m2^4 + 2*Ycut - 16*m2*Ycut - 12*m2^2*Ycut + 
     16*m2^3*Ycut - 2*m2^4*Ycut - Ycut^2 + 8*m2*Ycut^2 + 18*m2^2*Ycut^2 - 
     8*m2^3*Ycut^2 + m2^4*Ycut^2 + 2*Ycut^3 - 2*m2*Ycut^3 - 6*m2^2*Ycut^3 + 
     2*m2^3*Ycut^3 - 5*Ycut^4 + 4*m2*Ycut^4 + 4*Ycut^5 - 2*m2*Ycut^5 - 
     Ycut^6 - 4*api*X1El[0, c[SR]^2, Ycut, m2, mu2hat] + 
     8*api*Ycut*X1El[0, c[SR]^2, Ycut, m2, mu2hat] - 
     4*api*Ycut^2*X1El[0, c[SR]^2, Ycut, m2, mu2hat])/(4*(-1 + Ycut)^2)) + 
 c[SL]*(c[SR]*(12*m2^(3/2)*(1 + m2)*Log[m2] - 12*m2^(3/2)*(1 + m2)*
      Log[1 - Ycut] - (2*Sqrt[m2] + 18*m2^(3/2) - 18*m2^(5/2) - 2*m2^(7/2) - 
       2*Sqrt[m2]*Ycut - 30*m2^(3/2)*Ycut + 6*m2^(5/2)*Ycut + 
       2*m2^(7/2)*Ycut + 6*m2^(3/2)*Ycut^2 + 6*m2^(5/2)*Ycut^2 - 
       2*Sqrt[m2]*Ycut^3 + 6*m2^(3/2)*Ycut^3 + 2*Sqrt[m2]*Ycut^4 + 
       api*X1El[0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       api*Ycut*X1El[0, c[SL]*c[SR], Ycut, m2, mu2hat])/(-1 + Ycut)) - 
   (c[T]*(-2*Ycut^3 + 6*m2*Ycut^3 - 6*m2^2*Ycut^3 + 2*m2^3*Ycut^3 + 
      6*Ycut^4 - 12*m2*Ycut^4 + 6*m2^2*Ycut^4 - 6*Ycut^5 + 6*m2*Ycut^5 + 
      2*Ycut^6 - api*X1El[0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      2*api*Ycut*X1El[0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      api*Ycut^2*X1El[0, c[SL]*c[T], Ycut, m2, mu2hat]))/(-1 + Ycut)^2) + 
 c[T]^2*(-144*m2^2*Log[m2] + 144*m2^2*Log[1 - Ycut] - 
   (-12 + 96*m2 - 96*m2^3 + 12*m2^4 + 24*Ycut - 192*m2*Ycut - 144*m2^2*Ycut + 
     192*m2^3*Ycut - 24*m2^4*Ycut - 12*Ycut^2 + 96*m2*Ycut^2 + 
     216*m2^2*Ycut^2 - 96*m2^3*Ycut^2 + 12*m2^4*Ycut^2 + 40*Ycut^3 - 
     72*m2*Ycut^3 - 24*m2^2*Ycut^3 + 8*m2^3*Ycut^3 - 108*Ycut^4 + 
     144*m2*Ycut^4 - 48*m2^2*Ycut^4 + 96*Ycut^5 - 72*m2*Ycut^5 - 28*Ycut^6 - 
     api*X1El[0, c[T]^2, Ycut, m2, mu2hat] + 
     2*api*Ycut*X1El[0, c[T]^2, Ycut, m2, mu2hat] - 
     api*Ycut^2*X1El[0, c[T]^2, Ycut, m2, mu2hat])/(-1 + Ycut)^2) + 
 c[VL]^2*(-12*m2^2*Log[m2] + 12*m2^2*Log[1 - Ycut] - 
   (-1 + 8*m2 - 8*m2^3 + m2^4 + 2*Ycut - 16*m2*Ycut - 12*m2^2*Ycut + 
     16*m2^3*Ycut - 2*m2^4*Ycut - Ycut^2 + 8*m2*Ycut^2 + 18*m2^2*Ycut^2 - 
     8*m2^3*Ycut^2 + m2^4*Ycut^2 + 2*Ycut^3 - 2*m2*Ycut^3 - 6*m2^2*Ycut^3 + 
     2*m2^3*Ycut^3 - 5*Ycut^4 + 4*m2*Ycut^4 + 4*Ycut^5 - 2*m2*Ycut^5 - 
     Ycut^6 - api*X1El[0, c[VL]^2, Ycut, m2, mu2hat] + 
     2*api*Ycut*X1El[0, c[VL]^2, Ycut, m2, mu2hat] - 
     api*Ycut^2*X1El[0, c[VL]^2, Ycut, m2, mu2hat])/(-1 + Ycut)^2) + 
 c[VR]*(-24*m2^(3/2)*(1 + m2)*Log[m2] + 24*m2^(3/2)*(1 + m2)*Log[1 - Ycut] + 
   (4*Sqrt[m2] + 36*m2^(3/2) - 36*m2^(5/2) - 4*m2^(7/2) - 4*Sqrt[m2]*Ycut - 
     60*m2^(3/2)*Ycut + 12*m2^(5/2)*Ycut + 4*m2^(7/2)*Ycut + 
     12*m2^(3/2)*Ycut^2 + 12*m2^(5/2)*Ycut^2 - 4*Sqrt[m2]*Ycut^3 + 
     12*m2^(3/2)*Ycut^3 + 4*Sqrt[m2]*Ycut^4 - 
     api*X1El[0, SM*c[VR], Ycut, m2, mu2hat] + 
     api*Ycut*X1El[0, SM*c[VR], Ycut, m2, mu2hat])/(-1 + Ycut)) + 
 c[VL]*(-24*m2^2*Log[m2] + 24*m2^2*Log[1 - Ycut] - 
   (-2 + 16*m2 - 16*m2^3 + 2*m2^4 + 4*Ycut - 32*m2*Ycut - 24*m2^2*Ycut + 
     32*m2^3*Ycut - 4*m2^4*Ycut - 2*Ycut^2 + 16*m2*Ycut^2 + 36*m2^2*Ycut^2 - 
     16*m2^3*Ycut^2 + 2*m2^4*Ycut^2 + 4*Ycut^3 - 4*m2*Ycut^3 - 
     12*m2^2*Ycut^3 + 4*m2^3*Ycut^3 - 10*Ycut^4 + 8*m2*Ycut^4 + 8*Ycut^5 - 
     4*m2*Ycut^5 - 2*Ycut^6 - api*X1El[0, SM*c[VL], Ycut, m2, mu2hat] + 
     2*api*Ycut*X1El[0, SM*c[VL], Ycut, m2, mu2hat] - 
     api*Ycut^2*X1El[0, SM*c[VL], Ycut, m2, mu2hat])/(-1 + Ycut)^2 + 
   c[VR]*(-24*m2^(3/2)*(1 + m2)*Log[m2] + 24*m2^(3/2)*(1 + m2)*
      Log[1 - Ycut] + (4*Sqrt[m2] + 36*m2^(3/2) - 36*m2^(5/2) - 4*m2^(7/2) - 
       4*Sqrt[m2]*Ycut - 60*m2^(3/2)*Ycut + 12*m2^(5/2)*Ycut + 
       4*m2^(7/2)*Ycut + 12*m2^(3/2)*Ycut^2 + 12*m2^(5/2)*Ycut^2 - 
       4*Sqrt[m2]*Ycut^3 + 12*m2^(3/2)*Ycut^3 + 4*Sqrt[m2]*Ycut^4 - 
       api*X1El[0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       api*Ycut*X1El[0, c[VL]*c[VR], Ycut, m2, mu2hat])/(-1 + Ycut))) + 
 c[VR]^2*(1 - 8*m2 + 8*m2^3 - m2^4 + 12*m2^2*Ycut + 6*m2^2*Ycut^2 - 
   4*Ycut^3 + 8*m2*Ycut^3 + 3*Ycut^4 - 12*m2^2*Log[m2] + 
   12*m2^2*Log[1 - Ycut] + api*X1El[0, c[VR]^2, Ycut, m2, mu2hat])
