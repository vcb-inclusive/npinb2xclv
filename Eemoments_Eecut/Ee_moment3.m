(-15*m2^2*(1 + m2)*Log[m2])/4 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/4 + 
 (rhoD*((-779 + 5236*m2 - 5285*m2^2 + 1400*m2^3 - 525*m2^4 - 196*m2^5 + 
       189*m2^6 - 40*m2^7 + 4315*Ycut - 27860*m2*Ycut + 24325*m2^2*Ycut - 
       7000*m2^3*Ycut + 2625*m2^4*Ycut + 980*m2^5*Ycut - 945*m2^6*Ycut + 
       200*m2^7*Ycut - 9680*Ycut^2 + 59920*m2*Ycut^2 - 43400*m2^2*Ycut^2 + 
       14000*m2^3*Ycut^2 - 5250*m2^4*Ycut^2 - 1960*m2^5*Ycut^2 + 
       1890*m2^6*Ycut^2 - 400*m2^7*Ycut^2 + 11080*Ycut^3 - 65520*m2*Ycut^3 + 
       36400*m2^2*Ycut^3 - 14000*m2^3*Ycut^3 + 5250*m2^4*Ycut^3 + 
       1960*m2^5*Ycut^3 - 1890*m2^6*Ycut^3 + 400*m2^7*Ycut^3 - 6590*Ycut^4 + 
       36960*m2*Ycut^4 - 12950*m2^2*Ycut^4 + 7000*m2^3*Ycut^4 - 
       2625*m2^4*Ycut^4 - 980*m2^5*Ycut^4 + 945*m2^6*Ycut^4 - 
       200*m2^7*Ycut^4 + 1738*Ycut^5 - 9072*m2*Ycut^5 + 490*m2^2*Ycut^5 - 
       1400*m2^3*Ycut^5 + 525*m2^4*Ycut^5 + 196*m2^5*Ycut^5 - 
       189*m2^6*Ycut^5 + 40*m2^7*Ycut^5 - 210*Ycut^6 + 420*m2*Ycut^6 + 
       210*m2^2*Ycut^6 + 140*m2^3*Ycut^6 + 625*Ycut^7 - 420*m2*Ycut^7 + 
       175*m2^2*Ycut^7 - 1130*Ycut^8 + 560*m2*Ycut^8 + 70*m2^2*Ycut^8 + 
       965*Ycut^9 - 280*m2*Ycut^9 - 35*m2^2*Ycut^9 - 384*Ycut^10 + 
       56*m2*Ycut^10 + 45*Ycut^11 + 5*Ycut^12)/(420*(-1 + Ycut)^5) - 
     (1 + m2)*(-1 + 5*m2)*Log[m2] + (1 + m2)*(-1 + 5*m2)*Log[1 - Ycut] + 
     c[VR]^2*((-979 + 2240*m2 - 1561*m2^2 + 875*m2^4 - 896*m2^5 + 385*m2^6 - 
         64*m2^7 + 3357*Ycut - 6720*m2*Ycut + 4263*m2^2*Ycut - 
         2625*m2^4*Ycut + 2688*m2^5*Ycut - 1155*m2^6*Ycut + 192*m2^7*Ycut - 
         3987*Ycut^2 + 6720*m2*Ycut^2 - 3633*m2^2*Ycut^2 + 2625*m2^4*Ycut^2 - 
         2688*m2^5*Ycut^2 + 1155*m2^6*Ycut^2 - 192*m2^7*Ycut^2 + 
         1749*Ycut^3 - 2240*m2*Ycut^3 + 791*m2^2*Ycut^3 - 875*m2^4*Ycut^3 + 
         896*m2^5*Ycut^3 - 385*m2^6*Ycut^3 + 64*m2^7*Ycut^3 - 105*Ycut^4 + 
         105*m2^2*Ycut^4 - 21*Ycut^5 + 21*m2^2*Ycut^5 - 147*Ycut^6 + 
         147*m2^2*Ycut^6 + 342*Ycut^7 - 42*m2^2*Ycut^7 - 249*Ycut^8 - 
         21*m2^2*Ycut^8 + 25*Ycut^9 + 15*Ycut^10)/(420*(-1 + Ycut)^3) - 
       (-1 + m2)*(1 + m2)*Log[m2] + (-1 + m2)*(1 + m2)*Log[1 - Ycut]) + 
     c[VL]^2*((-779 + 5236*m2 - 5285*m2^2 + 1400*m2^3 - 525*m2^4 - 196*m2^5 + 
         189*m2^6 - 40*m2^7 + 4315*Ycut - 27860*m2*Ycut + 24325*m2^2*Ycut - 
         7000*m2^3*Ycut + 2625*m2^4*Ycut + 980*m2^5*Ycut - 945*m2^6*Ycut + 
         200*m2^7*Ycut - 9680*Ycut^2 + 59920*m2*Ycut^2 - 43400*m2^2*Ycut^2 + 
         14000*m2^3*Ycut^2 - 5250*m2^4*Ycut^2 - 1960*m2^5*Ycut^2 + 
         1890*m2^6*Ycut^2 - 400*m2^7*Ycut^2 + 11080*Ycut^3 - 
         65520*m2*Ycut^3 + 36400*m2^2*Ycut^3 - 14000*m2^3*Ycut^3 + 
         5250*m2^4*Ycut^3 + 1960*m2^5*Ycut^3 - 1890*m2^6*Ycut^3 + 
         400*m2^7*Ycut^3 - 6590*Ycut^4 + 36960*m2*Ycut^4 - 
         12950*m2^2*Ycut^4 + 7000*m2^3*Ycut^4 - 2625*m2^4*Ycut^4 - 
         980*m2^5*Ycut^4 + 945*m2^6*Ycut^4 - 200*m2^7*Ycut^4 + 1738*Ycut^5 - 
         9072*m2*Ycut^5 + 490*m2^2*Ycut^5 - 1400*m2^3*Ycut^5 + 
         525*m2^4*Ycut^5 + 196*m2^5*Ycut^5 - 189*m2^6*Ycut^5 + 
         40*m2^7*Ycut^5 - 210*Ycut^6 + 420*m2*Ycut^6 + 210*m2^2*Ycut^6 + 
         140*m2^3*Ycut^6 + 625*Ycut^7 - 420*m2*Ycut^7 + 175*m2^2*Ycut^7 - 
         1130*Ycut^8 + 560*m2*Ycut^8 + 70*m2^2*Ycut^8 + 965*Ycut^9 - 
         280*m2*Ycut^9 - 35*m2^2*Ycut^9 - 384*Ycut^10 + 56*m2*Ycut^10 + 
         45*Ycut^11 + 5*Ycut^12)/(420*(-1 + Ycut)^5) - 
       (1 + m2)*(-1 + 5*m2)*Log[m2] + (1 + m2)*(-1 + 5*m2)*Log[1 - Ycut]) + 
     c[SL]^2*((-2088 + 5600*m2 - 4235*m2^2 + 1400*m2^3 - 700*m2^4 - 
         112*m2^5 + 175*m2^6 - 40*m2^7 + 11280*Ycut - 28000*m2*Ycut + 
         19075*m2^2*Ycut - 7000*m2^3*Ycut + 3500*m2^4*Ycut + 560*m2^5*Ycut - 
         875*m2^6*Ycut + 200*m2^7*Ycut - 24660*Ycut^2 + 56000*m2*Ycut^2 - 
         32900*m2^2*Ycut^2 + 14000*m2^3*Ycut^2 - 7000*m2^4*Ycut^2 - 
         1120*m2^5*Ycut^2 + 1750*m2^6*Ycut^2 - 400*m2^7*Ycut^2 + 
         27460*Ycut^3 - 56000*m2*Ycut^3 + 25900*m2^2*Ycut^3 - 
         14000*m2^3*Ycut^3 + 7000*m2^4*Ycut^3 + 1120*m2^5*Ycut^3 - 
         1750*m2^6*Ycut^3 + 400*m2^7*Ycut^3 - 15830*Ycut^4 + 
         28000*m2*Ycut^4 - 7700*m2^2*Ycut^4 + 7000*m2^3*Ycut^4 - 
         3500*m2^4*Ycut^4 - 560*m2^5*Ycut^4 + 875*m2^6*Ycut^4 - 
         200*m2^7*Ycut^4 + 4006*Ycut^5 - 5600*m2*Ycut^5 - 560*m2^2*Ycut^5 - 
         1400*m2^3*Ycut^5 + 700*m2^4*Ycut^5 + 112*m2^5*Ycut^5 - 
         175*m2^6*Ycut^5 + 40*m2^7*Ycut^5 - 280*Ycut^6 + 140*m2*Ycut^6 + 
         210*m2^2*Ycut^6 + 140*m2^3*Ycut^6 + 555*Ycut^7 - 420*m2*Ycut^7 + 
         175*m2^2*Ycut^7 - 885*Ycut^8 + 420*m2*Ycut^8 + 70*m2^2*Ycut^8 + 
         580*Ycut^9 - 140*m2*Ycut^9 - 35*m2^2*Ycut^9 - 118*Ycut^10 - 
         25*Ycut^11 + 5*Ycut^12)/(1680*(-1 + Ycut)^5) + 
       ((2 - 5*m2^2)*Log[m2])/4 + ((-2 + 5*m2^2)*Log[1 - Ycut])/4) + 
     c[SR]^2*((-2088 + 5600*m2 - 4235*m2^2 + 1400*m2^3 - 700*m2^4 - 
         112*m2^5 + 175*m2^6 - 40*m2^7 + 11280*Ycut - 28000*m2*Ycut + 
         19075*m2^2*Ycut - 7000*m2^3*Ycut + 3500*m2^4*Ycut + 560*m2^5*Ycut - 
         875*m2^6*Ycut + 200*m2^7*Ycut - 24660*Ycut^2 + 56000*m2*Ycut^2 - 
         32900*m2^2*Ycut^2 + 14000*m2^3*Ycut^2 - 7000*m2^4*Ycut^2 - 
         1120*m2^5*Ycut^2 + 1750*m2^6*Ycut^2 - 400*m2^7*Ycut^2 + 
         27460*Ycut^3 - 56000*m2*Ycut^3 + 25900*m2^2*Ycut^3 - 
         14000*m2^3*Ycut^3 + 7000*m2^4*Ycut^3 + 1120*m2^5*Ycut^3 - 
         1750*m2^6*Ycut^3 + 400*m2^7*Ycut^3 - 15830*Ycut^4 + 
         28000*m2*Ycut^4 - 7700*m2^2*Ycut^4 + 7000*m2^3*Ycut^4 - 
         3500*m2^4*Ycut^4 - 560*m2^5*Ycut^4 + 875*m2^6*Ycut^4 - 
         200*m2^7*Ycut^4 + 4006*Ycut^5 - 5600*m2*Ycut^5 - 560*m2^2*Ycut^5 - 
         1400*m2^3*Ycut^5 + 700*m2^4*Ycut^5 + 112*m2^5*Ycut^5 - 
         175*m2^6*Ycut^5 + 40*m2^7*Ycut^5 - 280*Ycut^6 + 140*m2*Ycut^6 + 
         210*m2^2*Ycut^6 + 140*m2^3*Ycut^6 + 555*Ycut^7 - 420*m2*Ycut^7 + 
         175*m2^2*Ycut^7 - 885*Ycut^8 + 420*m2*Ycut^8 + 70*m2^2*Ycut^8 + 
         580*Ycut^9 - 140*m2*Ycut^9 - 35*m2^2*Ycut^9 - 118*Ycut^10 - 
         25*Ycut^11 + 5*Ycut^12)/(1680*(-1 + Ycut)^5) + 
       ((2 - 5*m2^2)*Log[m2])/4 + ((-2 + 5*m2^2)*Log[1 - Ycut])/4) + 
     c[VR]*(-1/60*(Sqrt[m2]*(1112 - 192*m2 - 725*m2^2 - 400*m2^3 + 300*m2^4 - 
           112*m2^5 + 17*m2^6 - 4808*Ycut - 672*m2*Ycut + 2600*m2^2*Ycut + 
           1600*m2^3*Ycut - 1200*m2^4*Ycut + 448*m2^5*Ycut - 68*m2^6*Ycut + 
           7932*Ycut^2 + 3888*m2*Ycut^2 - 3300*m2^2*Ycut^2 - 
           2400*m2^3*Ycut^2 + 1800*m2^4*Ycut^2 - 672*m2^5*Ycut^2 + 
           102*m2^6*Ycut^2 - 6008*Ycut^3 - 5472*m2*Ycut^3 + 
           1600*m2^2*Ycut^3 + 1600*m2^3*Ycut^3 - 1200*m2^4*Ycut^3 + 
           448*m2^5*Ycut^3 - 68*m2^6*Ycut^3 + 1862*Ycut^4 + 2808*m2*Ycut^4 - 
           100*m2^2*Ycut^4 - 400*m2^3*Ycut^4 + 300*m2^4*Ycut^4 - 
           112*m2^5*Ycut^4 + 17*m2^6*Ycut^4 - 72*Ycut^5 - 288*m2*Ycut^5 - 
           60*m2^2*Ycut^5 + 8*Ycut^6 - 48*m2*Ycut^6 - 30*m2^2*Ycut^6 - 
           92*Ycut^7 + 62*m2*Ycut^7 - 30*m2^2*Ycut^7 + 138*Ycut^8 - 
           128*m2*Ycut^8 + 15*m2^2*Ycut^8 - 92*Ycut^9 + 42*m2*Ycut^9 + 
           20*Ycut^10))/(-1 + Ycut)^4 - Sqrt[m2]*(6 + 24*m2 + 5*m2^2)*
        Log[m2] + Sqrt[m2]*(6 + 24*m2 + 5*m2^2)*Log[1 - Ycut]) + 
     c[T]^2*((-204 + 1336*m2 - 1351*m2^2 + 200*m2^3 + 200*m2^4 - 296*m2^5 + 
         139*m2^6 - 24*m2^7 + 1140*Ycut - 7160*m2*Ycut + 6335*m2^2*Ycut - 
         1000*m2^3*Ycut - 1000*m2^4*Ycut + 1480*m2^5*Ycut - 695*m2^6*Ycut + 
         120*m2^7*Ycut - 2580*Ycut^2 + 15520*m2*Ycut^2 - 11620*m2^2*Ycut^2 + 
         2000*m2^3*Ycut^2 + 2000*m2^4*Ycut^2 - 2960*m2^5*Ycut^2 + 
         1390*m2^6*Ycut^2 - 240*m2^7*Ycut^2 + 2980*Ycut^3 - 17120*m2*Ycut^3 + 
         10220*m2^2*Ycut^3 - 2000*m2^3*Ycut^3 - 2000*m2^4*Ycut^3 + 
         2960*m2^5*Ycut^3 - 1390*m2^6*Ycut^3 + 240*m2^7*Ycut^3 - 
         1790*Ycut^4 + 9760*m2*Ycut^4 - 4060*m2^2*Ycut^4 + 1000*m2^3*Ycut^4 + 
         1000*m2^4*Ycut^4 - 1480*m2^5*Ycut^4 + 695*m2^6*Ycut^4 - 
         120*m2^7*Ycut^4 + 478*Ycut^5 - 2432*m2*Ycut^5 + 392*m2^2*Ycut^5 - 
         200*m2^3*Ycut^5 - 200*m2^4*Ycut^5 + 296*m2^5*Ycut^5 - 
         139*m2^6*Ycut^5 + 24*m2^7*Ycut^5 - 80*Ycut^6 + 100*m2*Ycut^6 + 
         90*m2^2*Ycut^6 + 20*m2^3*Ycut^6 + 275*Ycut^7 - 60*m2*Ycut^7 - 
         65*m2^2*Ycut^7 - 505*Ycut^8 + 100*m2*Ycut^8 + 70*m2^2*Ycut^8 + 
         440*Ycut^9 - 60*m2*Ycut^9 - 5*m2^2*Ycut^9 - 174*Ycut^10 + 
         16*m2*Ycut^10 - 6*m2^2*Ycut^10 + 15*Ycut^11 + 5*Ycut^12)/
        (15*(-1 + Ycut)^5) - 4*(-2 + 8*m2 + 7*m2^2)*Log[m2] + 
       4*(-2 + 8*m2 + 7*m2^2)*Log[1 - Ycut]) + 
     c[VL]*((-779 + 5236*m2 - 5285*m2^2 + 1400*m2^3 - 525*m2^4 - 196*m2^5 + 
         189*m2^6 - 40*m2^7 + 4315*Ycut - 27860*m2*Ycut + 24325*m2^2*Ycut - 
         7000*m2^3*Ycut + 2625*m2^4*Ycut + 980*m2^5*Ycut - 945*m2^6*Ycut + 
         200*m2^7*Ycut - 9680*Ycut^2 + 59920*m2*Ycut^2 - 43400*m2^2*Ycut^2 + 
         14000*m2^3*Ycut^2 - 5250*m2^4*Ycut^2 - 1960*m2^5*Ycut^2 + 
         1890*m2^6*Ycut^2 - 400*m2^7*Ycut^2 + 11080*Ycut^3 - 
         65520*m2*Ycut^3 + 36400*m2^2*Ycut^3 - 14000*m2^3*Ycut^3 + 
         5250*m2^4*Ycut^3 + 1960*m2^5*Ycut^3 - 1890*m2^6*Ycut^3 + 
         400*m2^7*Ycut^3 - 6590*Ycut^4 + 36960*m2*Ycut^4 - 
         12950*m2^2*Ycut^4 + 7000*m2^3*Ycut^4 - 2625*m2^4*Ycut^4 - 
         980*m2^5*Ycut^4 + 945*m2^6*Ycut^4 - 200*m2^7*Ycut^4 + 1738*Ycut^5 - 
         9072*m2*Ycut^5 + 490*m2^2*Ycut^5 - 1400*m2^3*Ycut^5 + 
         525*m2^4*Ycut^5 + 196*m2^5*Ycut^5 - 189*m2^6*Ycut^5 + 
         40*m2^7*Ycut^5 - 210*Ycut^6 + 420*m2*Ycut^6 + 210*m2^2*Ycut^6 + 
         140*m2^3*Ycut^6 + 625*Ycut^7 - 420*m2*Ycut^7 + 175*m2^2*Ycut^7 - 
         1130*Ycut^8 + 560*m2*Ycut^8 + 70*m2^2*Ycut^8 + 965*Ycut^9 - 
         280*m2*Ycut^9 - 35*m2^2*Ycut^9 - 384*Ycut^10 + 56*m2*Ycut^10 + 
         45*Ycut^11 + 5*Ycut^12)/(210*(-1 + Ycut)^5) - 
       2*(1 + m2)*(-1 + 5*m2)*Log[m2] + 2*(1 + m2)*(-1 + 5*m2)*
        Log[1 - Ycut] + c[VR]*(-1/60*(Sqrt[m2]*(1112 - 192*m2 - 725*m2^2 - 
             400*m2^3 + 300*m2^4 - 112*m2^5 + 17*m2^6 - 4808*Ycut - 
             672*m2*Ycut + 2600*m2^2*Ycut + 1600*m2^3*Ycut - 1200*m2^4*Ycut + 
             448*m2^5*Ycut - 68*m2^6*Ycut + 7932*Ycut^2 + 3888*m2*Ycut^2 - 
             3300*m2^2*Ycut^2 - 2400*m2^3*Ycut^2 + 1800*m2^4*Ycut^2 - 
             672*m2^5*Ycut^2 + 102*m2^6*Ycut^2 - 6008*Ycut^3 - 
             5472*m2*Ycut^3 + 1600*m2^2*Ycut^3 + 1600*m2^3*Ycut^3 - 
             1200*m2^4*Ycut^3 + 448*m2^5*Ycut^3 - 68*m2^6*Ycut^3 + 
             1862*Ycut^4 + 2808*m2*Ycut^4 - 100*m2^2*Ycut^4 - 
             400*m2^3*Ycut^4 + 300*m2^4*Ycut^4 - 112*m2^5*Ycut^4 + 
             17*m2^6*Ycut^4 - 72*Ycut^5 - 288*m2*Ycut^5 - 60*m2^2*Ycut^5 + 
             8*Ycut^6 - 48*m2*Ycut^6 - 30*m2^2*Ycut^6 - 92*Ycut^7 + 
             62*m2*Ycut^7 - 30*m2^2*Ycut^7 + 138*Ycut^8 - 128*m2*Ycut^8 + 
             15*m2^2*Ycut^8 - 92*Ycut^9 + 42*m2*Ycut^9 + 20*Ycut^10))/
           (-1 + Ycut)^4 - Sqrt[m2]*(6 + 24*m2 + 5*m2^2)*Log[m2] + 
         Sqrt[m2]*(6 + 24*m2 + 5*m2^2)*Log[1 - Ycut])) + 
     c[SL]*(c[T]*(-1/420*(-200 - 2996*m2 + 3724*m2^2 - 1400*m2^3 + 
            1400*m2^4 - 700*m2^5 + 196*m2^6 - 24*m2^7 + 1000*Ycut + 
            16660*m2*Ycut - 16940*m2^2*Ycut + 7000*m2^3*Ycut - 
            7000*m2^4*Ycut + 3500*m2^5*Ycut - 980*m2^6*Ycut + 120*m2^7*Ycut - 
            2000*Ycut^2 - 37520*m2*Ycut^2 + 29680*m2^2*Ycut^2 - 
            14000*m2^3*Ycut^2 + 14000*m2^4*Ycut^2 - 7000*m2^5*Ycut^2 + 
            1960*m2^6*Ycut^2 - 240*m2^7*Ycut^2 + 2000*Ycut^3 + 
            43120*m2*Ycut^3 - 24080*m2^2*Ycut^3 + 14000*m2^3*Ycut^3 - 
            14000*m2^4*Ycut^3 + 7000*m2^5*Ycut^3 - 1960*m2^6*Ycut^3 + 
            240*m2^7*Ycut^3 - 1000*Ycut^4 - 25760*m2*Ycut^4 + 
            7840*m2^2*Ycut^4 - 7000*m2^3*Ycut^4 + 7000*m2^4*Ycut^4 - 
            3500*m2^5*Ycut^4 + 980*m2^6*Ycut^4 - 120*m2^7*Ycut^4 + 
            200*Ycut^5 + 6832*m2*Ycut^5 + 112*m2^2*Ycut^5 + 
            1400*m2^3*Ycut^5 - 1400*m2^4*Ycut^5 + 700*m2^5*Ycut^5 - 
            196*m2^6*Ycut^5 + 24*m2^7*Ycut^5 - 420*m2*Ycut^6 - 
            140*m2^3*Ycut^6 - 10*Ycut^7 + 420*m2*Ycut^7 - 490*m2^2*Ycut^7 + 
            50*Ycut^8 - 560*m2*Ycut^8 + 140*m2^2*Ycut^8 - 100*Ycut^9 + 
            280*m2*Ycut^9 + 35*m2^2*Ycut^9 + 100*Ycut^10 - 56*m2*Ycut^10 - 
            21*m2^2*Ycut^10 - 50*Ycut^11 + 10*Ycut^12)/(-1 + Ycut)^5 - 
         4*m2*(1 + m2)*Log[m2] + 4*m2*(1 + m2)*Log[1 - Ycut]) + 
       c[SR]*((Sqrt[m2]*(1526 + 12*m2 - 1625*m2^2 + 150*m2^4 - 76*m2^5 + 
            13*m2^6 - 6584*Ycut - 2208*m2*Ycut + 6200*m2^2*Ycut - 
            600*m2^4*Ycut + 304*m2^5*Ycut - 52*m2^6*Ycut + 10836*Ycut^2 + 
            7632*m2*Ycut^2 - 8700*m2^2*Ycut^2 + 900*m2^4*Ycut^2 - 
            456*m2^5*Ycut^2 + 78*m2^6*Ycut^2 - 8184*Ycut^3 - 9408*m2*Ycut^3 + 
            5200*m2^2*Ycut^3 - 600*m2^4*Ycut^3 + 304*m2^5*Ycut^3 - 
            52*m2^6*Ycut^3 + 2526*Ycut^4 + 4512*m2*Ycut^4 - 
            1000*m2^2*Ycut^4 + 150*m2^4*Ycut^4 - 76*m2^5*Ycut^4 + 
            13*m2^6*Ycut^4 - 96*Ycut^5 - 432*m2*Ycut^5 - 60*m2^2*Ycut^5 + 
            4*Ycut^6 - 72*m2*Ycut^6 - 30*m2^2*Ycut^6 - 76*Ycut^7 + 
            38*m2*Ycut^7 - 30*m2^2*Ycut^7 + 84*Ycut^8 - 92*m2*Ycut^8 + 
            15*m2^2*Ycut^8 - 36*Ycut^9 + 18*m2*Ycut^9))/(120*(-1 + Ycut)^4) + 
         (Sqrt[m2]*(8 + 36*m2 + 5*m2^2)*Log[m2])/2 - 
         (Sqrt[m2]*(8 + 36*m2 + 5*m2^2)*Log[1 - Ycut])/2))) + 
   rhoLS*(-1/84*((-1 + m2 + Ycut)*(6 - 36*m2 + 90*m2^2 - 120*m2^3 + 90*m2^4 - 
         36*m2^5 + 6*m2^6 - 18*Ycut + 114*m2*Ycut - 300*m2^2*Ycut + 
         420*m2^3*Ycut - 330*m2^4*Ycut + 138*m2^5*Ycut - 24*m2^6*Ycut + 
         18*Ycut^2 - 120*m2*Ycut^2 + 336*m2^2*Ycut^2 - 504*m2^3*Ycut^2 + 
         426*m2^4*Ycut^2 - 192*m2^5*Ycut^2 + 36*m2^6*Ycut^2 - 6*Ycut^3 + 
         42*m2*Ycut^3 - 126*m2^2*Ycut^3 + 210*m2^3*Ycut^3 - 204*m2^4*Ycut^3 + 
         108*m2^5*Ycut^3 - 24*m2^6*Ycut^3 + 6*m2^4*Ycut^4 - 12*m2^5*Ycut^4 + 
         6*m2^6*Ycut^4 + 6*m2^4*Ycut^5 - 6*m2^5*Ycut^5 - 42*m2^2*Ycut^6 + 
         6*m2^4*Ycut^6 + Ycut^7 + m2*Ycut^7 + 22*m2^2*Ycut^7 - 
         6*m2^3*Ycut^7 - 3*Ycut^8 - 2*m2*Ycut^8 - m2^2*Ycut^8 + 3*Ycut^9 + 
         m2*Ycut^9 - Ycut^10))/(-1 + Ycut)^4 - 
     ((-1 + m2 + Ycut)*(6 - 36*m2 + 90*m2^2 - 120*m2^3 + 90*m2^4 - 36*m2^5 + 
        6*m2^6 - 18*Ycut + 114*m2*Ycut - 300*m2^2*Ycut + 420*m2^3*Ycut - 
        330*m2^4*Ycut + 138*m2^5*Ycut - 24*m2^6*Ycut + 18*Ycut^2 - 
        120*m2*Ycut^2 + 336*m2^2*Ycut^2 - 504*m2^3*Ycut^2 + 426*m2^4*Ycut^2 - 
        192*m2^5*Ycut^2 + 36*m2^6*Ycut^2 - 6*Ycut^3 + 42*m2*Ycut^3 - 
        126*m2^2*Ycut^3 + 210*m2^3*Ycut^3 - 204*m2^4*Ycut^3 + 
        108*m2^5*Ycut^3 - 24*m2^6*Ycut^3 + 6*m2^4*Ycut^4 - 12*m2^5*Ycut^4 + 
        6*m2^6*Ycut^4 + 6*m2^4*Ycut^5 - 6*m2^5*Ycut^5 - 42*m2^2*Ycut^6 + 
        6*m2^4*Ycut^6 + Ycut^7 + m2*Ycut^7 + 22*m2^2*Ycut^7 - 6*m2^3*Ycut^7 - 
        3*Ycut^8 - 2*m2*Ycut^8 - m2^2*Ycut^8 + 3*Ycut^9 + m2*Ycut^9 - 
        Ycut^10)*c[VL]^2)/(84*(-1 + Ycut)^4) + 
     c[SL]^2*(-1/672*((-1 + m2 + Ycut)*(-30 + 390*m2 + 1377*m2^2 - 723*m2^3 + 
           327*m2^4 - 93*m2^5 + 12*m2^6 + 90*Ycut - 1200*m2*Ycut - 
           5031*m2^2*Ycut + 2646*m2^3*Ycut - 1227*m2^4*Ycut + 360*m2^5*Ycut - 
           48*m2^6*Ycut - 90*Ycut^2 + 1230*m2*Ycut^2 + 6531*m2^2*Ycut^2 - 
           3423*m2^3*Ycut^2 + 1650*m2^4*Ycut^2 - 510*m2^5*Ycut^2 + 
           72*m2^6*Ycut^2 + 30*Ycut^3 - 420*m2*Ycut^3 - 3297*m2^2*Ycut^3 + 
           1680*m2^3*Ycut^3 - 870*m2^4*Ycut^3 + 300*m2^5*Ycut^3 - 
           48*m2^6*Ycut^3 + 315*m2^2*Ycut^4 - 105*m2^3*Ycut^4 + 
           75*m2^4*Ycut^4 - 45*m2^5*Ycut^4 + 12*m2^6*Ycut^4 + 
           63*m2^2*Ycut^5 - 42*m2^3*Ycut^5 + 33*m2^4*Ycut^5 - 
           12*m2^5*Ycut^5 + 84*m2*Ycut^6 - 63*m2^2*Ycut^6 - 21*m2^3*Ycut^6 + 
           12*m2^4*Ycut^6 + 2*Ycut^7 - 82*m2*Ycut^7 + 65*m2^2*Ycut^7 - 
           12*m2^3*Ycut^7 - 6*Ycut^8 - 4*m2*Ycut^8 - 2*m2^2*Ycut^8 + 
           6*Ycut^9 + 2*m2*Ycut^9 - 2*Ycut^10))/(-1 + Ycut)^4 + 
       (15*m2^2*Log[m2])/8 - (15*m2^2*Log[1 - Ycut])/8) + 
     c[SR]^2*(-1/672*((-1 + m2 + Ycut)*(-30 + 390*m2 + 1377*m2^2 - 723*m2^3 + 
           327*m2^4 - 93*m2^5 + 12*m2^6 + 90*Ycut - 1200*m2*Ycut - 
           5031*m2^2*Ycut + 2646*m2^3*Ycut - 1227*m2^4*Ycut + 360*m2^5*Ycut - 
           48*m2^6*Ycut - 90*Ycut^2 + 1230*m2*Ycut^2 + 6531*m2^2*Ycut^2 - 
           3423*m2^3*Ycut^2 + 1650*m2^4*Ycut^2 - 510*m2^5*Ycut^2 + 
           72*m2^6*Ycut^2 + 30*Ycut^3 - 420*m2*Ycut^3 - 3297*m2^2*Ycut^3 + 
           1680*m2^3*Ycut^3 - 870*m2^4*Ycut^3 + 300*m2^5*Ycut^3 - 
           48*m2^6*Ycut^3 + 315*m2^2*Ycut^4 - 105*m2^3*Ycut^4 + 
           75*m2^4*Ycut^4 - 45*m2^5*Ycut^4 + 12*m2^6*Ycut^4 + 
           63*m2^2*Ycut^5 - 42*m2^3*Ycut^5 + 33*m2^4*Ycut^5 - 
           12*m2^5*Ycut^5 + 84*m2*Ycut^6 - 63*m2^2*Ycut^6 - 21*m2^3*Ycut^6 + 
           12*m2^4*Ycut^6 + 2*Ycut^7 - 82*m2*Ycut^7 + 65*m2^2*Ycut^7 - 
           12*m2^3*Ycut^7 - 6*Ycut^8 - 4*m2*Ycut^8 - 2*m2^2*Ycut^8 + 
           6*Ycut^9 + 2*m2*Ycut^9 - 2*Ycut^10))/(-1 + Ycut)^4 + 
       (15*m2^2*Log[m2])/8 - (15*m2^2*Log[1 - Ycut])/8) + 
     c[VR]^2*(-1/140*((-1 + m2 + Ycut)*(30 - 250*m2 - 159*m2^2 - 159*m2^3 + 
           191*m2^4 - 89*m2^5 + 16*m2^6 - 30*Ycut + 280*m2*Ycut + 
           359*m2^2*Ycut + 200*m2^3*Ycut - 309*m2^4*Ycut + 162*m2^5*Ycut - 
           32*m2^6*Ycut - 180*m2^2*Ycut^2 + 20*m2^3*Ycut^2 + 61*m2^4*Ycut^2 - 
           57*m2^5*Ycut^2 + 16*m2^6*Ycut^2 - 40*m2^2*Ycut^3 - 
           20*m2^3*Ycut^3 + 41*m2^4*Ycut^3 - 16*m2^5*Ycut^3 - 5*m2^2*Ycut^4 - 
           25*m2^3*Ycut^4 + 16*m2^4*Ycut^4 + 9*m2^2*Ycut^5 - 16*m2^3*Ycut^5 + 
           16*m2^2*Ycut^6 + 5*Ycut^7 + 5*m2*Ycut^7 - 5*Ycut^8))/
         (-1 + Ycut)^2 - 3*m2^2*Log[m2] + 3*m2^2*Log[1 - Ycut]) + 
     c[T]^2*(-1/30*((-1 + m2 + Ycut)*(90 - 810*m2 - 999*m2^2 - 99*m2^3 + 
           351*m2^4 - 189*m2^5 + 36*m2^6 - 270*Ycut + 2520*m2*Ycut + 
           3897*m2^2*Ycut + 198*m2^3*Ycut - 1251*m2^4*Ycut + 720*m2^5*Ycut - 
           144*m2^6*Ycut + 270*Ycut^2 - 2610*m2*Ycut^2 - 5517*m2^2*Ycut^2 + 
           81*m2^3*Ycut^2 + 1530*m2^4*Ycut^2 - 990*m2^5*Ycut^2 + 
           216*m2^6*Ycut^2 - 90*Ycut^3 + 900*m2*Ycut^3 + 3159*m2^2*Ycut^3 - 
           360*m2^3*Ycut^3 - 630*m2^4*Ycut^3 + 540*m2^5*Ycut^3 - 
           144*m2^6*Ycut^3 - 405*m2^2*Ycut^4 + 135*m2^3*Ycut^4 - 
           45*m2^4*Ycut^4 - 45*m2^5*Ycut^4 + 36*m2^6*Ycut^4 - 
           81*m2^2*Ycut^5 + 54*m2^3*Ycut^5 + 9*m2^4*Ycut^5 - 36*m2^5*Ycut^5 - 
           60*m2*Ycut^6 - 87*m2^2*Ycut^6 + 27*m2^3*Ycut^6 + 36*m2^4*Ycut^6 + 
           10*Ycut^7 + 70*m2*Ycut^7 - 23*m2^2*Ycut^7 - 36*m2^3*Ycut^7 - 
           30*Ycut^8 - 20*m2*Ycut^8 + 26*m2^2*Ycut^8 + 30*Ycut^9 + 
           10*m2*Ycut^9 - 10*Ycut^10))/(-1 + Ycut)^4 - 54*m2^2*Log[m2] + 
       54*m2^2*Log[1 - Ycut]) + c[SR]*c[T]*
      ((Sqrt[m2]*(-1 + m2 + Ycut)*(-197 - 222*m2 + 78*m2^2 - 22*m2^3 + 
          3*m2^4 + 257*Ycut + 385*m2*Ycut - 137*m2^2*Ycut + 41*m2^3*Ycut - 
          6*m2^4*Ycut - 30*Ycut^2 - 120*m2*Ycut^2 + 43*m2^2*Ycut^2 - 
          16*m2^3*Ycut^2 + 3*m2^4*Ycut^2 - 10*Ycut^3 - 30*m2*Ycut^3 + 
          13*m2^2*Ycut^3 - 3*m2^3*Ycut^3 - 5*Ycut^4 - 10*m2*Ycut^4 + 
          3*m2^2*Ycut^4 - 3*Ycut^5 - 3*m2*Ycut^5 + 8*Ycut^6))/
        (20*(-1 + Ycut)^2) + 3*Sqrt[m2]*(1 + 5*m2)*Log[m2] - 
       3*Sqrt[m2]*(1 + 5*m2)*Log[1 - Ycut]) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-204 + 1800*m2 - 375*m2^2 + 
          425*m2^3 - 175*m2^4 + 29*m2^5 + 324*Ycut - 2604*m2*Ycut + 
          471*m2^2*Ycut - 704*m2^3*Ycut + 321*m2^4*Ycut - 58*m2^5*Ycut - 
          60*Ycut^2 + 420*m2*Ycut^2 + 66*m2^2*Ycut^2 + 162*m2^3*Ycut^2 - 
          117*m2^4*Ycut^2 + 29*m2^5*Ycut^2 - 20*Ycut^3 + 160*m2*Ycut^3 - 
          74*m2^2*Ycut^3 + 88*m2^3*Ycut^3 - 29*m2^4*Ycut^3 - 10*Ycut^4 + 
          90*m2*Ycut^4 - 59*m2^2*Ycut^4 + 29*m2^3*Ycut^4 - 6*Ycut^5 + 
          60*m2*Ycut^5 - 29*m2^2*Ycut^5 - 4*Ycut^6 - 16*m2*Ycut^6 - 
          20*Ycut^7))/(120*(-1 + Ycut)^2) - (Sqrt[m2]*(-2 + 12*m2 + 15*m2^2)*
         Log[m2])/2 + (Sqrt[m2]*(-2 + 12*m2 + 15*m2^2)*Log[1 - Ycut])/2) + 
     c[SL]*(c[T]*(((-1 + m2 + Ycut)*(60 - 570*m2 - 927*m2^2 + 123*m2^3 + 
            123*m2^4 - 87*m2^5 + 18*m2^6 - 180*Ycut + 1770*m2*Ycut + 
            3531*m2^2*Ycut - 546*m2^3*Ycut - 423*m2^4*Ycut + 330*m2^5*Ycut - 
            72*m2^6*Ycut + 180*Ycut^2 - 1830*m2*Ycut^2 - 4851*m2^2*Ycut^2 + 
            903*m2^3*Ycut^2 + 480*m2^4*Ycut^2 - 450*m2^5*Ycut^2 + 
            108*m2^6*Ycut^2 - 60*Ycut^3 + 630*m2*Ycut^3 + 2667*m2^2*Ycut^3 - 
            630*m2^3*Ycut^3 - 150*m2^4*Ycut^3 + 240*m2^5*Ycut^3 - 
            72*m2^6*Ycut^3 - 315*m2^2*Ycut^4 + 105*m2^3*Ycut^4 - 
            45*m2^4*Ycut^4 - 15*m2^5*Ycut^4 + 18*m2^6*Ycut^4 - 
            63*m2^2*Ycut^5 + 42*m2^3*Ycut^5 - 3*m2^4*Ycut^5 - 
            18*m2^5*Ycut^5 + 189*m2^2*Ycut^6 + 21*m2^3*Ycut^6 + 
            18*m2^4*Ycut^6 + 10*Ycut^7 + 10*m2*Ycut^7 - 179*m2^2*Ycut^7 - 
            18*m2^3*Ycut^7 - 30*Ycut^8 - 20*m2*Ycut^8 + 53*m2^2*Ycut^8 + 
            30*Ycut^9 + 10*m2*Ycut^9 - 10*Ycut^10))/(420*(-1 + Ycut)^4) + 
         3*m2^2*Log[m2] - 3*m2^2*Log[1 - Ycut]) + 
       c[SR]*(-1/80*(Sqrt[m2]*(-1 + m2 + Ycut)*(424 + 902*m2 - 123*m2^2 + 
             77*m2^3 - 23*m2^4 + 3*m2^5 - 544*Ycut - 1438*m2*Ycut + 
             189*m2^2*Ycut - 134*m2^3*Ycut + 43*m2^4*Ycut - 6*m2^5*Ycut + 
             60*Ycut^2 + 360*m2*Ycut^2 - 26*m2^2*Ycut^2 + 40*m2^3*Ycut^2 - 
             17*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 20*Ycut^3 + 100*m2*Ycut^3 - 
             26*m2^2*Ycut^3 + 14*m2^3*Ycut^3 - 3*m2^4*Ycut^3 + 10*Ycut^4 + 
             40*m2*Ycut^4 - 11*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 6*Ycut^5 + 
             18*m2*Ycut^5 - 3*m2^2*Ycut^5 + 24*Ycut^6 - 12*m2*Ycut^6))/
           (-1 + Ycut)^2 + (3*Sqrt[m2]*(2 + 14*m2 + 5*m2^2)*Log[m2])/4 - 
         (3*Sqrt[m2]*(2 + 14*m2 + 5*m2^2)*Log[1 - Ycut])/4)) + 
     c[VL]*(-1/42*((-1 + m2 + Ycut)*(6 - 36*m2 + 90*m2^2 - 120*m2^3 + 
           90*m2^4 - 36*m2^5 + 6*m2^6 - 18*Ycut + 114*m2*Ycut - 
           300*m2^2*Ycut + 420*m2^3*Ycut - 330*m2^4*Ycut + 138*m2^5*Ycut - 
           24*m2^6*Ycut + 18*Ycut^2 - 120*m2*Ycut^2 + 336*m2^2*Ycut^2 - 
           504*m2^3*Ycut^2 + 426*m2^4*Ycut^2 - 192*m2^5*Ycut^2 + 
           36*m2^6*Ycut^2 - 6*Ycut^3 + 42*m2*Ycut^3 - 126*m2^2*Ycut^3 + 
           210*m2^3*Ycut^3 - 204*m2^4*Ycut^3 + 108*m2^5*Ycut^3 - 
           24*m2^6*Ycut^3 + 6*m2^4*Ycut^4 - 12*m2^5*Ycut^4 + 6*m2^6*Ycut^4 + 
           6*m2^4*Ycut^5 - 6*m2^5*Ycut^5 - 42*m2^2*Ycut^6 + 6*m2^4*Ycut^6 + 
           Ycut^7 + m2*Ycut^7 + 22*m2^2*Ycut^7 - 6*m2^3*Ycut^7 - 3*Ycut^8 - 
           2*m2*Ycut^8 - m2^2*Ycut^8 + 3*Ycut^9 + m2*Ycut^9 - Ycut^10))/
         (-1 + Ycut)^4 + c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*
           (-204 + 1800*m2 - 375*m2^2 + 425*m2^3 - 175*m2^4 + 29*m2^5 + 
            324*Ycut - 2604*m2*Ycut + 471*m2^2*Ycut - 704*m2^3*Ycut + 
            321*m2^4*Ycut - 58*m2^5*Ycut - 60*Ycut^2 + 420*m2*Ycut^2 + 
            66*m2^2*Ycut^2 + 162*m2^3*Ycut^2 - 117*m2^4*Ycut^2 + 
            29*m2^5*Ycut^2 - 20*Ycut^3 + 160*m2*Ycut^3 - 74*m2^2*Ycut^3 + 
            88*m2^3*Ycut^3 - 29*m2^4*Ycut^3 - 10*Ycut^4 + 90*m2*Ycut^4 - 
            59*m2^2*Ycut^4 + 29*m2^3*Ycut^4 - 6*Ycut^5 + 60*m2*Ycut^5 - 
            29*m2^2*Ycut^5 - 4*Ycut^6 - 16*m2*Ycut^6 - 20*Ycut^7))/
          (120*(-1 + Ycut)^2) - (Sqrt[m2]*(-2 + 12*m2 + 15*m2^2)*Log[m2])/2 + 
         (Sqrt[m2]*(-2 + 12*m2 + 15*m2^2)*Log[1 - Ycut])/2))))/mb^3 + 
 (mupi*(((-1 + m2 + Ycut)*(-18 + 276*m2 + 2019*m2^2 + 234*m2^3 + 24*m2^4 - 
        18*m2^5 + 3*m2^6 + 54*Ycut - 846*m2*Ycut - 7059*m2^2*Ycut - 
        945*m2^3*Ycut - 81*m2^4*Ycut + 69*m2^5*Ycut - 12*m2^6*Ycut - 
        54*Ycut^2 + 864*m2*Ycut^2 + 8673*m2^2*Ycut^2 + 1428*m2^3*Ycut^2 + 
        87*m2^4*Ycut^2 - 96*m2^5*Ycut^2 + 18*m2^6*Ycut^2 + 18*Ycut^3 - 
        294*m2*Ycut^3 - 4053*m2^2*Ycut^3 - 945*m2^3*Ycut^3 - 18*m2^4*Ycut^3 + 
        54*m2^5*Ycut^3 - 12*m2^6*Ycut^3 + 315*m2^2*Ycut^4 + 210*m2^3*Ycut^4 - 
        18*m2^4*Ycut^4 - 6*m2^5*Ycut^4 + 3*m2^6*Ycut^4 + 63*m2^2*Ycut^5 + 
        21*m2^3*Ycut^5 + 3*m2^4*Ycut^5 - 3*m2^5*Ycut^5 + 21*m2^2*Ycut^6 + 
        3*m2^4*Ycut^6 - 10*Ycut^7 - 10*m2*Ycut^7 - 31*m2^2*Ycut^7 - 
        3*m2^3*Ycut^7 + 30*Ycut^8 + 20*m2*Ycut^8 + 10*m2^2*Ycut^8 - 
        30*Ycut^9 - 10*m2*Ycut^9 + 10*Ycut^10))/(168*(-1 + Ycut)^4) - 
     (15*m2^2*(1 + m2)*Log[m2])/2 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/2 + 
     c[VR]^2*(((-1 + m2 + Ycut)*(-10 + 130*m2 + 459*m2^2 - 241*m2^3 + 
          109*m2^4 - 31*m2^5 + 4*m2^6 + 10*Ycut - 140*m2*Ycut - 
          759*m2^2*Ycut + 400*m2^3*Ycut - 191*m2^4*Ycut + 58*m2^5*Ycut - 
          8*m2^6*Ycut + 200*m2^2*Ycut^2 - 100*m2^3*Ycut^2 + 59*m2^4*Ycut^2 - 
          23*m2^5*Ycut^2 + 4*m2^6*Ycut^2 + 60*m2^2*Ycut^3 - 40*m2^3*Ycut^3 + 
          19*m2^4*Ycut^3 - 4*m2^5*Ycut^3 + 25*m2^2*Ycut^4 - 15*m2^3*Ycut^4 + 
          4*m2^4*Ycut^4 + 11*m2^2*Ycut^5 - 4*m2^3*Ycut^5 + 4*m2^2*Ycut^6 - 
          25*Ycut^7 - 25*m2*Ycut^7 + 25*Ycut^8))/(140*(-1 + Ycut)^2) - 
       3*m2^2*Log[m2] + 3*m2^2*Log[1 - Ycut]) + 
     c[SL]^2*(((-1 + m2 + Ycut)*(-18 + 276*m2 + 2019*m2^2 + 234*m2^3 + 
          24*m2^4 - 18*m2^5 + 3*m2^6 + 54*Ycut - 846*m2*Ycut - 
          7059*m2^2*Ycut - 945*m2^3*Ycut - 81*m2^4*Ycut + 69*m2^5*Ycut - 
          12*m2^6*Ycut - 54*Ycut^2 + 864*m2*Ycut^2 + 8673*m2^2*Ycut^2 + 
          1428*m2^3*Ycut^2 + 87*m2^4*Ycut^2 - 96*m2^5*Ycut^2 + 
          18*m2^6*Ycut^2 + 18*Ycut^3 - 294*m2*Ycut^3 - 4053*m2^2*Ycut^3 - 
          945*m2^3*Ycut^3 - 18*m2^4*Ycut^3 + 54*m2^5*Ycut^3 - 
          12*m2^6*Ycut^3 + 315*m2^2*Ycut^4 + 210*m2^3*Ycut^4 - 
          18*m2^4*Ycut^4 - 6*m2^5*Ycut^4 + 3*m2^6*Ycut^4 + 63*m2^2*Ycut^5 + 
          21*m2^3*Ycut^5 + 3*m2^4*Ycut^5 - 3*m2^5*Ycut^5 + 21*m2^2*Ycut^6 + 
          3*m2^4*Ycut^6 - 10*Ycut^7 - 10*m2*Ycut^7 - 31*m2^2*Ycut^7 - 
          3*m2^3*Ycut^7 + 30*Ycut^8 + 20*m2*Ycut^8 + 10*m2^2*Ycut^8 - 
          30*Ycut^9 - 10*m2*Ycut^9 + 10*Ycut^10))/(672*(-1 + Ycut)^4) - 
       (15*m2^2*(1 + m2)*Log[m2])/8 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/8) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(-18 + 276*m2 + 2019*m2^2 + 234*m2^3 + 
          24*m2^4 - 18*m2^5 + 3*m2^6 + 54*Ycut - 846*m2*Ycut - 
          7059*m2^2*Ycut - 945*m2^3*Ycut - 81*m2^4*Ycut + 69*m2^5*Ycut - 
          12*m2^6*Ycut - 54*Ycut^2 + 864*m2*Ycut^2 + 8673*m2^2*Ycut^2 + 
          1428*m2^3*Ycut^2 + 87*m2^4*Ycut^2 - 96*m2^5*Ycut^2 + 
          18*m2^6*Ycut^2 + 18*Ycut^3 - 294*m2*Ycut^3 - 4053*m2^2*Ycut^3 - 
          945*m2^3*Ycut^3 - 18*m2^4*Ycut^3 + 54*m2^5*Ycut^3 - 
          12*m2^6*Ycut^3 + 315*m2^2*Ycut^4 + 210*m2^3*Ycut^4 - 
          18*m2^4*Ycut^4 - 6*m2^5*Ycut^4 + 3*m2^6*Ycut^4 + 63*m2^2*Ycut^5 + 
          21*m2^3*Ycut^5 + 3*m2^4*Ycut^5 - 3*m2^5*Ycut^5 + 21*m2^2*Ycut^6 + 
          3*m2^4*Ycut^6 - 10*Ycut^7 - 10*m2*Ycut^7 - 31*m2^2*Ycut^7 - 
          3*m2^3*Ycut^7 + 30*Ycut^8 + 20*m2*Ycut^8 + 10*m2^2*Ycut^8 - 
          30*Ycut^9 - 10*m2*Ycut^9 + 10*Ycut^10))/(672*(-1 + Ycut)^4) - 
       (15*m2^2*(1 + m2)*Log[m2])/8 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/8) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(-18 + 276*m2 + 2019*m2^2 + 234*m2^3 + 
          24*m2^4 - 18*m2^5 + 3*m2^6 + 54*Ycut - 846*m2*Ycut - 
          7059*m2^2*Ycut - 945*m2^3*Ycut - 81*m2^4*Ycut + 69*m2^5*Ycut - 
          12*m2^6*Ycut - 54*Ycut^2 + 864*m2*Ycut^2 + 8673*m2^2*Ycut^2 + 
          1428*m2^3*Ycut^2 + 87*m2^4*Ycut^2 - 96*m2^5*Ycut^2 + 
          18*m2^6*Ycut^2 + 18*Ycut^3 - 294*m2*Ycut^3 - 4053*m2^2*Ycut^3 - 
          945*m2^3*Ycut^3 - 18*m2^4*Ycut^3 + 54*m2^5*Ycut^3 - 
          12*m2^6*Ycut^3 + 315*m2^2*Ycut^4 + 210*m2^3*Ycut^4 - 
          18*m2^4*Ycut^4 - 6*m2^5*Ycut^4 + 3*m2^6*Ycut^4 + 63*m2^2*Ycut^5 + 
          21*m2^3*Ycut^5 + 3*m2^4*Ycut^5 - 3*m2^5*Ycut^5 + 21*m2^2*Ycut^6 + 
          3*m2^4*Ycut^6 - 10*Ycut^7 - 10*m2*Ycut^7 - 31*m2^2*Ycut^7 - 
          3*m2^3*Ycut^7 + 30*Ycut^8 + 20*m2*Ycut^8 + 10*m2^2*Ycut^8 - 
          30*Ycut^9 - 10*m2*Ycut^9 + 10*Ycut^10))/(168*(-1 + Ycut)^4) - 
       (15*m2^2*(1 + m2)*Log[m2])/2 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/2) + 
     c[VR]*(-1/20*(Sqrt[m2]*(-1 + m2 + Ycut)*(10 + 284*m2 + 159*m2^2 - 
           41*m2^3 + 9*m2^4 - m2^5 - 20*Ycut - 678*m2*Ycut - 444*m2^2*Ycut + 
           115*m2^3*Ycut - 26*m2^4*Ycut + 3*m2^5*Ycut + 10*Ycut^2 + 
           454*m2*Ycut^2 + 385*m2^2*Ycut^2 - 100*m2^3*Ycut^2 + 
           24*m2^4*Ycut^2 - 3*m2^5*Ycut^2 - 40*m2*Ycut^3 - 80*m2^2*Ycut^3 + 
           20*m2^3*Ycut^3 - 6*m2^4*Ycut^3 + m2^5*Ycut^3 - 10*m2*Ycut^4 - 
           15*m2^2*Ycut^4 + 5*m2^3*Ycut^4 - m2^4*Ycut^4 - 4*m2*Ycut^5 - 
           4*m2^2*Ycut^5 + m2^3*Ycut^5 - 2*m2*Ycut^6 - m2^2*Ycut^6 + 
           6*m2*Ycut^7))/(-1 + Ycut)^3 - 3*m2^(3/2)*(2 + 5*m2)*Log[m2] + 
       3*m2^(3/2)*(2 + 5*m2)*Log[1 - Ycut]) + 
     c[T]^2*(((-1 + m2 + Ycut)*(-30 + 420*m2 + 2229*m2^2 - 246*m2^3 + 
          204*m2^4 - 66*m2^5 + 9*m2^6 + 90*Ycut - 1290*m2*Ycut - 
          7917*m2^2*Ycut + 837*m2^3*Ycut - 759*m2^4*Ycut + 255*m2^5*Ycut - 
          36*m2^6*Ycut - 90*Ycut^2 + 1320*m2*Ycut^2 + 9927*m2^2*Ycut^2 - 
          936*m2^3*Ycut^2 + 1005*m2^4*Ycut^2 - 360*m2^5*Ycut^2 + 
          54*m2^6*Ycut^2 + 30*Ycut^3 - 450*m2*Ycut^3 - 4779*m2^2*Ycut^3 + 
          285*m2^3*Ycut^3 - 510*m2^4*Ycut^3 + 210*m2^5*Ycut^3 - 
          36*m2^6*Ycut^3 + 405*m2^2*Ycut^4 + 90*m2^3*Ycut^4 + 
          30*m2^4*Ycut^4 - 30*m2^5*Ycut^4 + 9*m2^6*Ycut^4 + 81*m2^2*Ycut^5 - 
          9*m2^3*Ycut^5 + 21*m2^4*Ycut^5 - 9*m2^5*Ycut^5 + 27*m2^2*Ycut^6 - 
          12*m2^3*Ycut^6 + 9*m2^4*Ycut^6 - 50*Ycut^7 - 50*m2*Ycut^7 - 
          17*m2^2*Ycut^7 - 9*m2^3*Ycut^7 + 150*Ycut^8 + 100*m2*Ycut^8 + 
          14*m2^2*Ycut^8 - 150*Ycut^9 - 50*m2*Ycut^9 + 50*Ycut^10))/
        (30*(-1 + Ycut)^4) - 6*m2^2*(9 + 5*m2)*Log[m2] + 
       6*m2^2*(9 + 5*m2)*Log[1 - Ycut]) + 
     c[VL]*(((-1 + m2 + Ycut)*(-18 + 276*m2 + 2019*m2^2 + 234*m2^3 + 
          24*m2^4 - 18*m2^5 + 3*m2^6 + 54*Ycut - 846*m2*Ycut - 
          7059*m2^2*Ycut - 945*m2^3*Ycut - 81*m2^4*Ycut + 69*m2^5*Ycut - 
          12*m2^6*Ycut - 54*Ycut^2 + 864*m2*Ycut^2 + 8673*m2^2*Ycut^2 + 
          1428*m2^3*Ycut^2 + 87*m2^4*Ycut^2 - 96*m2^5*Ycut^2 + 
          18*m2^6*Ycut^2 + 18*Ycut^3 - 294*m2*Ycut^3 - 4053*m2^2*Ycut^3 - 
          945*m2^3*Ycut^3 - 18*m2^4*Ycut^3 + 54*m2^5*Ycut^3 - 
          12*m2^6*Ycut^3 + 315*m2^2*Ycut^4 + 210*m2^3*Ycut^4 - 
          18*m2^4*Ycut^4 - 6*m2^5*Ycut^4 + 3*m2^6*Ycut^4 + 63*m2^2*Ycut^5 + 
          21*m2^3*Ycut^5 + 3*m2^4*Ycut^5 - 3*m2^5*Ycut^5 + 21*m2^2*Ycut^6 + 
          3*m2^4*Ycut^6 - 10*Ycut^7 - 10*m2*Ycut^7 - 31*m2^2*Ycut^7 - 
          3*m2^3*Ycut^7 + 30*Ycut^8 + 20*m2*Ycut^8 + 10*m2^2*Ycut^8 - 
          30*Ycut^9 - 10*m2*Ycut^9 + 10*Ycut^10))/(84*(-1 + Ycut)^4) - 
       15*m2^2*(1 + m2)*Log[m2] + 15*m2^2*(1 + m2)*Log[1 - Ycut] + 
       c[VR]*(-1/20*(Sqrt[m2]*(-1 + m2 + Ycut)*(10 + 284*m2 + 159*m2^2 - 
             41*m2^3 + 9*m2^4 - m2^5 - 20*Ycut - 678*m2*Ycut - 
             444*m2^2*Ycut + 115*m2^3*Ycut - 26*m2^4*Ycut + 3*m2^5*Ycut + 
             10*Ycut^2 + 454*m2*Ycut^2 + 385*m2^2*Ycut^2 - 100*m2^3*Ycut^2 + 
             24*m2^4*Ycut^2 - 3*m2^5*Ycut^2 - 40*m2*Ycut^3 - 80*m2^2*Ycut^3 + 
             20*m2^3*Ycut^3 - 6*m2^4*Ycut^3 + m2^5*Ycut^3 - 10*m2*Ycut^4 - 
             15*m2^2*Ycut^4 + 5*m2^3*Ycut^4 - m2^4*Ycut^4 - 4*m2*Ycut^5 - 
             4*m2^2*Ycut^5 + m2^3*Ycut^5 - 2*m2*Ycut^6 - m2^2*Ycut^6 + 
             6*m2*Ycut^7))/(-1 + Ycut)^3 - 3*m2^(3/2)*(2 + 5*m2)*Log[m2] + 
         3*m2^(3/2)*(2 + 5*m2)*Log[1 - Ycut])) + 
     c[SL]*(c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(10 + 284*m2 + 159*m2^2 - 
            41*m2^3 + 9*m2^4 - m2^5 - 20*Ycut - 678*m2*Ycut - 444*m2^2*Ycut + 
            115*m2^3*Ycut - 26*m2^4*Ycut + 3*m2^5*Ycut + 10*Ycut^2 + 
            454*m2*Ycut^2 + 385*m2^2*Ycut^2 - 100*m2^3*Ycut^2 + 
            24*m2^4*Ycut^2 - 3*m2^5*Ycut^2 - 40*m2*Ycut^3 - 80*m2^2*Ycut^3 + 
            20*m2^3*Ycut^3 - 6*m2^4*Ycut^3 + m2^5*Ycut^3 - 10*m2*Ycut^4 - 
            15*m2^2*Ycut^4 + 5*m2^3*Ycut^4 - m2^4*Ycut^4 - 4*m2*Ycut^5 - 
            4*m2^2*Ycut^5 + m2^3*Ycut^5 - 2*m2*Ycut^6 - m2^2*Ycut^6 + 
            6*m2*Ycut^7))/(40*(-1 + Ycut)^3) + (3*m2^(3/2)*(2 + 5*m2)*
           Log[m2])/2 - (3*m2^(3/2)*(2 + 5*m2)*Log[1 - Ycut])/2) + 
       c[T]*(-1/840*((-1 + m2 + Ycut)*(30 - 600*m2 - 7341*m2^2 - 2616*m2^3 + 
             534*m2^4 - 96*m2^5 + 9*m2^6 - 90*Ycut + 1830*m2*Ycut + 
             25233*m2^2*Ycut + 10017*m2^3*Ycut - 2049*m2^4*Ycut + 
             375*m2^5*Ycut - 36*m2^6*Ycut + 90*Ycut^2 - 1860*m2*Ycut^2 - 
             30303*m2^2*Ycut^2 - 13986*m2^3*Ycut^2 + 2865*m2^4*Ycut^2 - 
             540*m2^5*Ycut^2 + 54*m2^6*Ycut^2 - 30*Ycut^3 + 630*m2*Ycut^3 + 
             13671*m2^2*Ycut^3 + 8085*m2^3*Ycut^3 - 1650*m2^4*Ycut^3 + 
             330*m2^5*Ycut^3 - 36*m2^6*Ycut^3 - 945*m2^2*Ycut^4 - 
             1260*m2^3*Ycut^4 + 240*m2^4*Ycut^4 - 60*m2^5*Ycut^4 + 
             9*m2^6*Ycut^4 - 189*m2^2*Ycut^5 - 189*m2^3*Ycut^5 + 
             51*m2^4*Ycut^5 - 9*m2^5*Ycut^5 - 63*m2^2*Ycut^6 - 
             42*m2^3*Ycut^6 + 9*m2^4*Ycut^6 - 100*Ycut^7 - 100*m2*Ycut^7 + 
             173*m2^2*Ycut^7 - 9*m2^3*Ycut^7 + 300*Ycut^8 + 200*m2*Ycut^8 - 
             26*m2^2*Ycut^8 - 300*Ycut^9 - 100*m2*Ycut^9 + 100*Ycut^10))/
           (-1 + Ycut)^4 - (3*m2^2*(3 + 5*m2)*Log[m2])/2 + 
         (3*m2^2*(3 + 5*m2)*Log[1 - Ycut])/2))) + 
   muG*(((-1 + m2 + Ycut)*(-530 - 2294*m2 + 13981*m2^2 + 4006*m2^3 - 
        1244*m2^4 + 436*m2^5 - 75*m2^6 + 1060*Ycut + 6578*m2*Ycut - 
        34566*m2^2*Ycut - 11135*m2^3*Ycut + 3371*m2^4*Ycut - 1233*m2^5*Ycut + 
        225*m2^6*Ycut - 530*Ycut^2 - 5544*m2*Ycut^2 + 24465*m2^2*Ycut^2 + 
        9655*m2^3*Ycut^2 - 2724*m2^4*Ycut^2 + 1083*m2^5*Ycut^2 - 
        225*m2^6*Ycut^2 + 840*m2*Ycut^3 - 2520*m2^2*Ycut^3 - 
        2140*m2^3*Ycut^3 + 386*m2^4*Ycut^3 - 211*m2^5*Ycut^3 + 
        75*m2^6*Ycut^3 + 210*m2*Ycut^4 - 735*m2^2*Ycut^4 - 250*m2^3*Ycut^4 + 
        136*m2^4*Ycut^4 - 75*m2^5*Ycut^4 + 84*m2*Ycut^5 - 336*m2^2*Ycut^5 - 
        61*m2^3*Ycut^5 + 75*m2^4*Ycut^5 - 140*Ycut^6 + 322*m2*Ycut^6 + 
        511*m2^2*Ycut^6 - 75*m2^3*Ycut^6 + 180*Ycut^7 - 296*m2*Ycut^7 - 
        100*m2^2*Ycut^7 + 60*Ycut^8 + 100*m2*Ycut^8 - 100*Ycut^9))/
      (1680*(-1 + Ycut)^3) + ((-1 + m2 + Ycut)^2*(2 - 10*m2 + 20*m2^2 - 
        20*m2^3 + 10*m2^4 - 2*m2^5 + 2*Ycut - 6*m2*Ycut + 4*m2^2*Ycut + 
        4*m2^3*Ycut - 6*m2^4*Ycut + 2*m2^5*Ycut + 2*Ycut^2 - 2*m2*Ycut^2 - 
        6*m2^2*Ycut^2 + 10*m2^3*Ycut^2 - 4*m2^4*Ycut^2 + 2*Ycut^3 + 
        2*m2*Ycut^3 - 10*m2^2*Ycut^3 + 6*m2^3*Ycut^3 + 2*Ycut^4 + 
        6*m2*Ycut^4 - 8*m2^2*Ycut^4 + 2*Ycut^5 + 10*m2*Ycut^5 - 5*Ycut^6)*
       c[VR]^2)/(28*(-1 + Ycut)) + (m2*(-6 + 15*m2 + 25*m2^2)*Log[m2])/4 - 
     (m2*(-6 + 15*m2 + 25*m2^2)*Log[1 - Ycut])/4 + 
     c[T]^2*(((-1 + m2 + Ycut)*(-230 - 1166*m2 + 2239*m2^2 + 814*m2^3 - 
          536*m2^4 + 244*m2^5 - 45*m2^6 + 460*Ycut + 3182*m2*Ycut - 
          5694*m2^2*Ycut - 2105*m2^3*Ycut + 1409*m2^4*Ycut - 687*m2^5*Ycut + 
          135*m2^6*Ycut - 230*Ycut^2 - 2556*m2*Ycut^2 + 4215*m2^2*Ycut^2 + 
          1585*m2^3*Ycut^2 - 1056*m2^4*Ycut^2 + 597*m2^5*Ycut^2 - 
          135*m2^6*Ycut^2 + 360*m2*Ycut^3 - 480*m2^2*Ycut^3 - 
          220*m2^3*Ycut^3 + 74*m2^4*Ycut^3 - 109*m2^5*Ycut^3 + 
          45*m2^6*Ycut^3 + 90*m2*Ycut^4 - 165*m2^2*Ycut^4 - 10*m2^3*Ycut^4 + 
          64*m2^4*Ycut^4 - 45*m2^5*Ycut^4 + 36*m2*Ycut^5 - 84*m2^2*Ycut^5 - 
          19*m2^3*Ycut^5 + 45*m2^4*Ycut^5 + 220*Ycut^6 + 178*m2*Ycut^6 + 
          49*m2^2*Ycut^6 - 45*m2^3*Ycut^6 - 540*Ycut^7 - 224*m2*Ycut^7 + 
          20*m2^2*Ycut^7 + 420*Ycut^8 + 100*m2*Ycut^8 - 100*Ycut^9))/
        (60*(-1 + Ycut)^3) + m2*(-3 + 5*m2)*(6 + 5*m2)*Log[m2] - 
       m2*(-3 + 5*m2)*(6 + 5*m2)*Log[1 - Ycut]) + 
     c[SL]^2*(((-1 + m2 + Ycut)*(230 + 5774*m2 + 4409*m2^2 + 734*m2^3 - 
          316*m2^4 + 104*m2^5 - 15*m2^6 - 460*Ycut - 13838*m2*Ycut - 
          11634*m2^2*Ycut - 1975*m2^3*Ycut + 859*m2^4*Ycut - 297*m2^5*Ycut + 
          45*m2^6*Ycut + 230*Ycut^2 + 9324*m2*Ycut^2 + 9345*m2^2*Ycut^2 + 
          1595*m2^3*Ycut^2 - 696*m2^4*Ycut^2 + 267*m2^5*Ycut^2 - 
          45*m2^6*Ycut^2 - 840*m2*Ycut^3 - 1680*m2^2*Ycut^3 - 
          260*m2^3*Ycut^3 + 94*m2^4*Ycut^3 - 59*m2^5*Ycut^3 + 
          15*m2^6*Ycut^3 - 210*m2*Ycut^4 - 315*m2^2*Ycut^4 - 50*m2^3*Ycut^4 + 
          44*m2^4*Ycut^4 - 15*m2^5*Ycut^4 - 84*m2*Ycut^5 - 84*m2^2*Ycut^5 - 
          29*m2^3*Ycut^5 + 15*m2^4*Ycut^5 - 28*Ycut^6 - 154*m2*Ycut^6 + 
          119*m2^2*Ycut^6 - 15*m2^3*Ycut^6 + 36*Ycut^7 + 8*m2*Ycut^7 - 
          20*m2^2*Ycut^7 + 12*Ycut^8 + 20*m2*Ycut^8 - 20*Ycut^9))/
        (1344*(-1 + Ycut)^3) + (5*m2*(6 + 15*m2 + 5*m2^2)*Log[m2])/16 - 
       (5*m2*(6 + 15*m2 + 5*m2^2)*Log[1 - Ycut])/16) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(230 + 5774*m2 + 4409*m2^2 + 734*m2^3 - 
          316*m2^4 + 104*m2^5 - 15*m2^6 - 460*Ycut - 13838*m2*Ycut - 
          11634*m2^2*Ycut - 1975*m2^3*Ycut + 859*m2^4*Ycut - 297*m2^5*Ycut + 
          45*m2^6*Ycut + 230*Ycut^2 + 9324*m2*Ycut^2 + 9345*m2^2*Ycut^2 + 
          1595*m2^3*Ycut^2 - 696*m2^4*Ycut^2 + 267*m2^5*Ycut^2 - 
          45*m2^6*Ycut^2 - 840*m2*Ycut^3 - 1680*m2^2*Ycut^3 - 
          260*m2^3*Ycut^3 + 94*m2^4*Ycut^3 - 59*m2^5*Ycut^3 + 
          15*m2^6*Ycut^3 - 210*m2*Ycut^4 - 315*m2^2*Ycut^4 - 50*m2^3*Ycut^4 + 
          44*m2^4*Ycut^4 - 15*m2^5*Ycut^4 - 84*m2*Ycut^5 - 84*m2^2*Ycut^5 - 
          29*m2^3*Ycut^5 + 15*m2^4*Ycut^5 - 28*Ycut^6 - 154*m2*Ycut^6 + 
          119*m2^2*Ycut^6 - 15*m2^3*Ycut^6 + 36*Ycut^7 + 8*m2*Ycut^7 - 
          20*m2^2*Ycut^7 + 12*Ycut^8 + 20*m2*Ycut^8 - 20*Ycut^9))/
        (1344*(-1 + Ycut)^3) + (5*m2*(6 + 15*m2 + 5*m2^2)*Log[m2])/16 - 
       (5*m2*(6 + 15*m2 + 5*m2^2)*Log[1 - Ycut])/16) + 
     c[VR]*(-1/120*(Sqrt[m2]*(-1 + m2 + Ycut)*(-264 + 1278*m2 + 3*m2^2 + 
           203*m2^3 - 97*m2^4 + 17*m2^5 + 384*Ycut - 1782*m2*Ycut - 
           129*m2^2*Ycut - 326*m2^3*Ycut + 177*m2^4*Ycut - 34*m2^5*Ycut - 
           60*Ycut^2 + 240*m2*Ycut^2 + 186*m2^2*Ycut^2 + 60*m2^3*Ycut^2 - 
           63*m2^4*Ycut^2 + 17*m2^5*Ycut^2 - 20*Ycut^3 + 100*m2*Ycut^3 - 
           14*m2^2*Ycut^3 + 46*m2^3*Ycut^3 - 17*m2^4*Ycut^3 - 10*Ycut^4 + 
           60*m2*Ycut^4 - 29*m2^2*Ycut^4 + 17*m2^3*Ycut^4 - 6*Ycut^5 + 
           42*m2*Ycut^5 - 17*m2^2*Ycut^5 - 64*Ycut^6 - 28*m2*Ycut^6 + 
           40*Ycut^7))/(-1 + Ycut)^2 + (Sqrt[m2]*(-2 + 6*m2 + 15*m2^2)*
         Log[m2])/2 - (Sqrt[m2]*(-2 + 6*m2 + 15*m2^2)*Log[1 - Ycut])/2) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(-530 - 2294*m2 + 13981*m2^2 + 4006*m2^3 - 
          1244*m2^4 + 436*m2^5 - 75*m2^6 + 1060*Ycut + 6578*m2*Ycut - 
          34566*m2^2*Ycut - 11135*m2^3*Ycut + 3371*m2^4*Ycut - 
          1233*m2^5*Ycut + 225*m2^6*Ycut - 530*Ycut^2 - 5544*m2*Ycut^2 + 
          24465*m2^2*Ycut^2 + 9655*m2^3*Ycut^2 - 2724*m2^4*Ycut^2 + 
          1083*m2^5*Ycut^2 - 225*m2^6*Ycut^2 + 840*m2*Ycut^3 - 
          2520*m2^2*Ycut^3 - 2140*m2^3*Ycut^3 + 386*m2^4*Ycut^3 - 
          211*m2^5*Ycut^3 + 75*m2^6*Ycut^3 + 210*m2*Ycut^4 - 
          735*m2^2*Ycut^4 - 250*m2^3*Ycut^4 + 136*m2^4*Ycut^4 - 
          75*m2^5*Ycut^4 + 84*m2*Ycut^5 - 336*m2^2*Ycut^5 - 61*m2^3*Ycut^5 + 
          75*m2^4*Ycut^5 - 140*Ycut^6 + 322*m2*Ycut^6 + 511*m2^2*Ycut^6 - 
          75*m2^3*Ycut^6 + 180*Ycut^7 - 296*m2*Ycut^7 - 100*m2^2*Ycut^7 + 
          60*Ycut^8 + 100*m2*Ycut^8 - 100*Ycut^9))/(1680*(-1 + Ycut)^3) + 
       (m2*(-6 + 15*m2 + 25*m2^2)*Log[m2])/4 - 
       (m2*(-6 + 15*m2 + 25*m2^2)*Log[1 - Ycut])/4) + 
     c[VL]*(((-1 + m2 + Ycut)*(-530 - 2294*m2 + 13981*m2^2 + 4006*m2^3 - 
          1244*m2^4 + 436*m2^5 - 75*m2^6 + 1060*Ycut + 6578*m2*Ycut - 
          34566*m2^2*Ycut - 11135*m2^3*Ycut + 3371*m2^4*Ycut - 
          1233*m2^5*Ycut + 225*m2^6*Ycut - 530*Ycut^2 - 5544*m2*Ycut^2 + 
          24465*m2^2*Ycut^2 + 9655*m2^3*Ycut^2 - 2724*m2^4*Ycut^2 + 
          1083*m2^5*Ycut^2 - 225*m2^6*Ycut^2 + 840*m2*Ycut^3 - 
          2520*m2^2*Ycut^3 - 2140*m2^3*Ycut^3 + 386*m2^4*Ycut^3 - 
          211*m2^5*Ycut^3 + 75*m2^6*Ycut^3 + 210*m2*Ycut^4 - 
          735*m2^2*Ycut^4 - 250*m2^3*Ycut^4 + 136*m2^4*Ycut^4 - 
          75*m2^5*Ycut^4 + 84*m2*Ycut^5 - 336*m2^2*Ycut^5 - 61*m2^3*Ycut^5 + 
          75*m2^4*Ycut^5 - 140*Ycut^6 + 322*m2*Ycut^6 + 511*m2^2*Ycut^6 - 
          75*m2^3*Ycut^6 + 180*Ycut^7 - 296*m2*Ycut^7 - 100*m2^2*Ycut^7 + 
          60*Ycut^8 + 100*m2*Ycut^8 - 100*Ycut^9))/(840*(-1 + Ycut)^3) + 
       (m2*(-6 + 15*m2 + 25*m2^2)*Log[m2])/2 - 
       (m2*(-6 + 15*m2 + 25*m2^2)*Log[1 - Ycut])/2 + 
       c[VR]*(-1/120*(Sqrt[m2]*(-1 + m2 + Ycut)*(-264 + 1278*m2 + 3*m2^2 + 
             203*m2^3 - 97*m2^4 + 17*m2^5 + 384*Ycut - 1782*m2*Ycut - 
             129*m2^2*Ycut - 326*m2^3*Ycut + 177*m2^4*Ycut - 34*m2^5*Ycut - 
             60*Ycut^2 + 240*m2*Ycut^2 + 186*m2^2*Ycut^2 + 60*m2^3*Ycut^2 - 
             63*m2^4*Ycut^2 + 17*m2^5*Ycut^2 - 20*Ycut^3 + 100*m2*Ycut^3 - 
             14*m2^2*Ycut^3 + 46*m2^3*Ycut^3 - 17*m2^4*Ycut^3 - 10*Ycut^4 + 
             60*m2*Ycut^4 - 29*m2^2*Ycut^4 + 17*m2^3*Ycut^4 - 6*Ycut^5 + 
             42*m2*Ycut^5 - 17*m2^2*Ycut^5 - 64*Ycut^6 - 28*m2*Ycut^6 + 
             40*Ycut^7))/(-1 + Ycut)^2 + (Sqrt[m2]*(-2 + 6*m2 + 15*m2^2)*
           Log[m2])/2 - (Sqrt[m2]*(-2 + 6*m2 + 15*m2^2)*Log[1 - Ycut])/2)) + 
     c[SL]*(c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(424 + 902*m2 - 123*m2^2 + 
            77*m2^3 - 23*m2^4 + 3*m2^5 - 544*Ycut - 1438*m2*Ycut + 
            189*m2^2*Ycut - 134*m2^3*Ycut + 43*m2^4*Ycut - 6*m2^5*Ycut + 
            60*Ycut^2 + 360*m2*Ycut^2 - 26*m2^2*Ycut^2 + 40*m2^3*Ycut^2 - 
            17*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 20*Ycut^3 + 100*m2*Ycut^3 - 
            26*m2^2*Ycut^3 + 14*m2^3*Ycut^3 - 3*m2^4*Ycut^3 + 10*Ycut^4 + 
            40*m2*Ycut^4 - 11*m2^2*Ycut^4 + 3*m2^3*Ycut^4 + 6*Ycut^5 + 
            18*m2*Ycut^5 - 3*m2^2*Ycut^5 + 24*Ycut^6 - 12*m2*Ycut^6))/
          (80*(-1 + Ycut)^2) - (3*Sqrt[m2]*(2 + 14*m2 + 5*m2^2)*Log[m2])/4 + 
         (3*Sqrt[m2]*(2 + 14*m2 + 5*m2^2)*Log[1 - Ycut])/4) + 
       c[T]*(-1/1680*((-1 + m2 + Ycut)*(-10 - 8914*m2 - 22459*m2^2 + 
             116*m2^3 - 934*m2^4 + 326*m2^5 - 45*m2^6 + 20*Ycut + 
             20338*m2*Ycut + 57414*m2^2*Ycut + 305*m2^3*Ycut + 
             2521*m2^4*Ycut - 933*m2^5*Ycut + 135*m2^6*Ycut - 10*Ycut^2 - 
             12684*m2*Ycut^2 - 43155*m2^2*Ycut^2 - 1375*m2^3*Ycut^2 - 
             2004*m2^4*Ycut^2 + 843*m2^5*Ycut^2 - 135*m2^6*Ycut^2 + 
             840*m2*Ycut^3 + 5880*m2^2*Ycut^3 + 1180*m2^3*Ycut^3 + 
             226*m2^4*Ycut^3 - 191*m2^5*Ycut^3 + 45*m2^6*Ycut^3 + 
             210*m2*Ycut^4 + 1365*m2^2*Ycut^4 - 80*m2^3*Ycut^4 + 
             146*m2^4*Ycut^4 - 45*m2^5*Ycut^4 + 84*m2*Ycut^5 + 
             504*m2^2*Ycut^5 - 101*m2^3*Ycut^5 + 45*m2^4*Ycut^5 + 
             140*Ycut^6 + 602*m2*Ycut^6 - 469*m2^2*Ycut^6 - 45*m2^3*Ycut^6 - 
             480*Ycut^7 - 676*m2*Ycut^7 + 220*m2^2*Ycut^7 + 540*Ycut^8 + 
             200*m2*Ycut^8 - 200*Ycut^9))/(-1 + Ycut)^3 + 
         (m2*(6 + 45*m2 + 25*m2^2)*Log[m2])/4 - 
         (m2*(6 + 45*m2 + 25*m2^2)*Log[1 - Ycut])/4))))/mb^2 + 
 (6 - 98*m2 - 581*m2^2 + 595*m2^3 + 70*m2^4 + 14*m2^5 - 7*m2^6 + m2^7 - 
   12*Ycut + 196*m2*Ycut + 1582*m2^2*Ycut - 770*m2^3*Ycut - 140*m2^4*Ycut - 
   28*m2^5*Ycut + 14*m2^6*Ycut - 2*m2^7*Ycut + 6*Ycut^2 - 98*m2*Ycut^2 - 
   1211*m2^2*Ycut^2 - 35*m2^3*Ycut^2 + 70*m2^4*Ycut^2 + 14*m2^5*Ycut^2 - 
   7*m2^6*Ycut^2 + m2^7*Ycut^2 + 140*m2^2*Ycut^3 + 140*m2^3*Ycut^3 + 
   35*m2^2*Ycut^4 + 35*m2^3*Ycut^4 + 14*m2^2*Ycut^5 + 14*m2^3*Ycut^5 - 
   14*Ycut^6 + 14*m2*Ycut^6 + 21*m2^2*Ycut^6 - 7*m2^3*Ycut^6 + 36*Ycut^7 - 
   28*m2*Ycut^7 - 30*Ycut^8 + 14*m2*Ycut^8 + 8*Ycut^9 + 
   112*api*X1El[3, SM^2, Ycut, m2, mu2hat] - 
   224*api*Ycut*X1El[3, SM^2, Ycut, m2, mu2hat] + 
   112*api*Ycut^2*X1El[3, SM^2, Ycut, m2, mu2hat])/(112*(-1 + Ycut)^2) + 
 c[SL]^2*((-15*m2^2*(1 + m2)*Log[m2])/16 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/
    16 + (6 - 98*m2 - 581*m2^2 + 595*m2^3 + 70*m2^4 + 14*m2^5 - 7*m2^6 + 
     m2^7 - 12*Ycut + 196*m2*Ycut + 1582*m2^2*Ycut - 770*m2^3*Ycut - 
     140*m2^4*Ycut - 28*m2^5*Ycut + 14*m2^6*Ycut - 2*m2^7*Ycut + 6*Ycut^2 - 
     98*m2*Ycut^2 - 1211*m2^2*Ycut^2 - 35*m2^3*Ycut^2 + 70*m2^4*Ycut^2 + 
     14*m2^5*Ycut^2 - 7*m2^6*Ycut^2 + m2^7*Ycut^2 + 140*m2^2*Ycut^3 + 
     140*m2^3*Ycut^3 + 35*m2^2*Ycut^4 + 35*m2^3*Ycut^4 + 14*m2^2*Ycut^5 + 
     14*m2^3*Ycut^5 - 14*Ycut^6 + 14*m2*Ycut^6 + 21*m2^2*Ycut^6 - 
     7*m2^3*Ycut^6 + 36*Ycut^7 - 28*m2*Ycut^7 - 30*Ycut^8 + 14*m2*Ycut^8 + 
     8*Ycut^9 + 448*api*X1El[3, c[SL]^2, Ycut, m2, mu2hat] - 
     896*api*Ycut*X1El[3, c[SL]^2, Ycut, m2, mu2hat] + 
     448*api*Ycut^2*X1El[3, c[SL]^2, Ycut, m2, mu2hat])/
    (448*(-1 + Ycut)^2)) + c[SR]^2*((-15*m2^2*(1 + m2)*Log[m2])/16 + 
   (15*m2^2*(1 + m2)*Log[1 - Ycut])/16 + 
   (6 - 98*m2 - 581*m2^2 + 595*m2^3 + 70*m2^4 + 14*m2^5 - 7*m2^6 + m2^7 - 
     12*Ycut + 196*m2*Ycut + 1582*m2^2*Ycut - 770*m2^3*Ycut - 140*m2^4*Ycut - 
     28*m2^5*Ycut + 14*m2^6*Ycut - 2*m2^7*Ycut + 6*Ycut^2 - 98*m2*Ycut^2 - 
     1211*m2^2*Ycut^2 - 35*m2^3*Ycut^2 + 70*m2^4*Ycut^2 + 14*m2^5*Ycut^2 - 
     7*m2^6*Ycut^2 + m2^7*Ycut^2 + 140*m2^2*Ycut^3 + 140*m2^3*Ycut^3 + 
     35*m2^2*Ycut^4 + 35*m2^3*Ycut^4 + 14*m2^2*Ycut^5 + 14*m2^3*Ycut^5 - 
     14*Ycut^6 + 14*m2*Ycut^6 + 21*m2^2*Ycut^6 - 7*m2^3*Ycut^6 + 36*Ycut^7 - 
     28*m2*Ycut^7 - 30*Ycut^8 + 14*m2*Ycut^8 + 8*Ycut^9 + 
     448*api*X1El[3, c[SR]^2, Ycut, m2, mu2hat] - 
     896*api*Ycut*X1El[3, c[SR]^2, Ycut, m2, mu2hat] + 
     448*api*Ycut^2*X1El[3, c[SR]^2, Ycut, m2, mu2hat])/
    (448*(-1 + Ycut)^2)) + 
 c[SL]*(c[SR]*((3*m2^(3/2)*(2 + 5*m2)*Log[m2])/4 - 
     (3*m2^(3/2)*(2 + 5*m2)*Log[1 - Ycut])/4 + 
     (-10*Sqrt[m2] - 274*m2^(3/2) + 125*m2^(5/2) + 200*m2^(7/2) - 
       50*m2^(9/2) + 10*m2^(11/2) - m2^(13/2) + 10*Sqrt[m2]*Ycut + 
       394*m2^(3/2)*Ycut + 175*m2^(5/2)*Ycut - 200*m2^(7/2)*Ycut + 
       50*m2^(9/2)*Ycut - 10*m2^(11/2)*Ycut + m2^(13/2)*Ycut - 
       60*m2^(3/2)*Ycut^2 - 150*m2^(5/2)*Ycut^2 - 20*m2^(3/2)*Ycut^3 - 
       50*m2^(5/2)*Ycut^3 - 10*m2^(3/2)*Ycut^4 - 25*m2^(5/2)*Ycut^4 - 
       6*m2^(3/2)*Ycut^5 - 15*m2^(5/2)*Ycut^5 + 10*Sqrt[m2]*Ycut^6 - 
       24*m2^(3/2)*Ycut^6 - 10*Sqrt[m2]*Ycut^7 - 
       80*api*X1El[3, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       80*api*Ycut*X1El[3, c[SL]*c[SR], Ycut, m2, mu2hat])/
      (80*(-1 + Ycut))) + c[T]*((-3*m2^2*(3 + 5*m2)*Log[m2])/4 + 
     (3*m2^2*(3 + 5*m2)*Log[1 - Ycut])/4 - 
     (-10 + 210*m2 + 2247*m2^2 - 1575*m2^3 - 1050*m2^4 + 210*m2^5 - 35*m2^6 + 
       3*m2^7 + 20*Ycut - 420*m2*Ycut - 5754*m2^2*Ycut + 1050*m2^3*Ycut + 
       2100*m2^4*Ycut - 420*m2^5*Ycut + 70*m2^6*Ycut - 6*m2^7*Ycut - 
       10*Ycut^2 + 210*m2*Ycut^2 + 4137*m2^2*Ycut^2 + 1575*m2^3*Ycut^2 - 
       1050*m2^4*Ycut^2 + 210*m2^5*Ycut^2 - 35*m2^6*Ycut^2 + 3*m2^7*Ycut^2 - 
       420*m2^2*Ycut^3 - 700*m2^3*Ycut^3 - 105*m2^2*Ycut^4 - 
       175*m2^3*Ycut^4 - 42*m2^2*Ycut^5 - 70*m2^3*Ycut^5 - 70*Ycut^6 + 
       210*m2*Ycut^6 - 231*m2^2*Ycut^6 + 35*m2^3*Ycut^6 + 220*Ycut^7 - 
       420*m2*Ycut^7 + 168*m2^2*Ycut^7 - 230*Ycut^8 + 210*m2*Ycut^8 + 
       80*Ycut^9 - 560*api*X1El[3, c[SL]*c[T], Ycut, m2, mu2hat] + 
       1120*api*Ycut*X1El[3, c[SL]*c[T], Ycut, m2, mu2hat] - 
       560*api*Ycut^2*X1El[3, c[SL]*c[T], Ycut, m2, mu2hat])/
      (560*(-1 + Ycut)^2))) + c[T]^2*(-3*m2^2*(9 + 5*m2)*Log[m2] + 
   3*m2^2*(9 + 5*m2)*Log[1 - Ycut] + (10 - 150*m2 - 603*m2^2 + 825*m2^3 - 
     150*m2^4 + 90*m2^5 - 25*m2^6 + 3*m2^7 - 20*Ycut + 300*m2*Ycut + 
     1746*m2^2*Ycut - 1350*m2^3*Ycut + 300*m2^4*Ycut - 180*m2^5*Ycut + 
     50*m2^6*Ycut - 6*m2^7*Ycut + 10*Ycut^2 - 150*m2*Ycut^2 - 
     1413*m2^2*Ycut^2 + 375*m2^3*Ycut^2 - 150*m2^4*Ycut^2 + 90*m2^5*Ycut^2 - 
     25*m2^6*Ycut^2 + 3*m2^7*Ycut^2 + 180*m2^2*Ycut^3 + 100*m2^3*Ycut^3 + 
     45*m2^2*Ycut^4 + 25*m2^3*Ycut^4 + 18*m2^2*Ycut^5 + 10*m2^3*Ycut^5 - 
     50*Ycut^6 + 90*m2*Ycut^6 - 21*m2^2*Ycut^6 - 5*m2^3*Ycut^6 + 140*Ycut^7 - 
     180*m2*Ycut^7 + 48*m2^2*Ycut^7 - 130*Ycut^8 + 90*m2*Ycut^8 + 40*Ycut^9 + 
     20*api*X1El[3, c[T]^2, Ycut, m2, mu2hat] - 
     40*api*Ycut*X1El[3, c[T]^2, Ycut, m2, mu2hat] + 
     20*api*Ycut^2*X1El[3, c[T]^2, Ycut, m2, mu2hat])/(20*(-1 + Ycut)^2)) + 
 c[VL]^2*((-15*m2^2*(1 + m2)*Log[m2])/4 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/
    4 + (6 - 98*m2 - 581*m2^2 + 595*m2^3 + 70*m2^4 + 14*m2^5 - 7*m2^6 + 
     m2^7 - 12*Ycut + 196*m2*Ycut + 1582*m2^2*Ycut - 770*m2^3*Ycut - 
     140*m2^4*Ycut - 28*m2^5*Ycut + 14*m2^6*Ycut - 2*m2^7*Ycut + 6*Ycut^2 - 
     98*m2*Ycut^2 - 1211*m2^2*Ycut^2 - 35*m2^3*Ycut^2 + 70*m2^4*Ycut^2 + 
     14*m2^5*Ycut^2 - 7*m2^6*Ycut^2 + m2^7*Ycut^2 + 140*m2^2*Ycut^3 + 
     140*m2^3*Ycut^3 + 35*m2^2*Ycut^4 + 35*m2^3*Ycut^4 + 14*m2^2*Ycut^5 + 
     14*m2^3*Ycut^5 - 14*Ycut^6 + 14*m2*Ycut^6 + 21*m2^2*Ycut^6 - 
     7*m2^3*Ycut^6 + 36*Ycut^7 - 28*m2*Ycut^7 - 30*Ycut^8 + 14*m2*Ycut^8 + 
     8*Ycut^9 + 112*api*X1El[3, c[VL]^2, Ycut, m2, mu2hat] - 
     224*api*Ycut*X1El[3, c[VL]^2, Ycut, m2, mu2hat] + 
     112*api*Ycut^2*X1El[3, c[VL]^2, Ycut, m2, mu2hat])/
    (112*(-1 + Ycut)^2)) + c[VR]*((-3*m2^(3/2)*(2 + 5*m2)*Log[m2])/2 + 
   (3*m2^(3/2)*(2 + 5*m2)*Log[1 - Ycut])/2 - 
   (-10*Sqrt[m2] - 274*m2^(3/2) + 125*m2^(5/2) + 200*m2^(7/2) - 50*m2^(9/2) + 
     10*m2^(11/2) - m2^(13/2) + 10*Sqrt[m2]*Ycut + 394*m2^(3/2)*Ycut + 
     175*m2^(5/2)*Ycut - 200*m2^(7/2)*Ycut + 50*m2^(9/2)*Ycut - 
     10*m2^(11/2)*Ycut + m2^(13/2)*Ycut - 60*m2^(3/2)*Ycut^2 - 
     150*m2^(5/2)*Ycut^2 - 20*m2^(3/2)*Ycut^3 - 50*m2^(5/2)*Ycut^3 - 
     10*m2^(3/2)*Ycut^4 - 25*m2^(5/2)*Ycut^4 - 6*m2^(3/2)*Ycut^5 - 
     15*m2^(5/2)*Ycut^5 + 10*Sqrt[m2]*Ycut^6 - 24*m2^(3/2)*Ycut^6 - 
     10*Sqrt[m2]*Ycut^7 + 40*api*X1El[3, SM*c[VR], Ycut, m2, mu2hat] - 
     40*api*Ycut*X1El[3, SM*c[VR], Ycut, m2, mu2hat])/(40*(-1 + Ycut))) + 
 c[VL]*((-15*m2^2*(1 + m2)*Log[m2])/2 + (15*m2^2*(1 + m2)*Log[1 - Ycut])/2 + 
   (6 - 98*m2 - 581*m2^2 + 595*m2^3 + 70*m2^4 + 14*m2^5 - 7*m2^6 + m2^7 - 
     12*Ycut + 196*m2*Ycut + 1582*m2^2*Ycut - 770*m2^3*Ycut - 140*m2^4*Ycut - 
     28*m2^5*Ycut + 14*m2^6*Ycut - 2*m2^7*Ycut + 6*Ycut^2 - 98*m2*Ycut^2 - 
     1211*m2^2*Ycut^2 - 35*m2^3*Ycut^2 + 70*m2^4*Ycut^2 + 14*m2^5*Ycut^2 - 
     7*m2^6*Ycut^2 + m2^7*Ycut^2 + 140*m2^2*Ycut^3 + 140*m2^3*Ycut^3 + 
     35*m2^2*Ycut^4 + 35*m2^3*Ycut^4 + 14*m2^2*Ycut^5 + 14*m2^3*Ycut^5 - 
     14*Ycut^6 + 14*m2*Ycut^6 + 21*m2^2*Ycut^6 - 7*m2^3*Ycut^6 + 36*Ycut^7 - 
     28*m2*Ycut^7 - 30*Ycut^8 + 14*m2*Ycut^8 + 8*Ycut^9 + 
     56*api*X1El[3, SM*c[VL], Ycut, m2, mu2hat] - 
     112*api*Ycut*X1El[3, SM*c[VL], Ycut, m2, mu2hat] + 
     56*api*Ycut^2*X1El[3, SM*c[VL], Ycut, m2, mu2hat])/(56*(-1 + Ycut)^2) + 
   c[VR]*((-3*m2^(3/2)*(2 + 5*m2)*Log[m2])/2 + 
     (3*m2^(3/2)*(2 + 5*m2)*Log[1 - Ycut])/2 - 
     (-10*Sqrt[m2] - 274*m2^(3/2) + 125*m2^(5/2) + 200*m2^(7/2) - 
       50*m2^(9/2) + 10*m2^(11/2) - m2^(13/2) + 10*Sqrt[m2]*Ycut + 
       394*m2^(3/2)*Ycut + 175*m2^(5/2)*Ycut - 200*m2^(7/2)*Ycut + 
       50*m2^(9/2)*Ycut - 10*m2^(11/2)*Ycut + m2^(13/2)*Ycut - 
       60*m2^(3/2)*Ycut^2 - 150*m2^(5/2)*Ycut^2 - 20*m2^(3/2)*Ycut^3 - 
       50*m2^(5/2)*Ycut^3 - 10*m2^(3/2)*Ycut^4 - 25*m2^(5/2)*Ycut^4 - 
       6*m2^(3/2)*Ycut^5 - 15*m2^(5/2)*Ycut^5 + 10*Sqrt[m2]*Ycut^6 - 
       24*m2^(3/2)*Ycut^6 - 10*Sqrt[m2]*Ycut^7 + 
       40*api*X1El[3, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       40*api*Ycut*X1El[3, c[VL]*c[VR], Ycut, m2, mu2hat])/
      (40*(-1 + Ycut)))) + c[VR]^2*((-3*m2^2*Log[m2])/2 + 
   (3*m2^2*Log[1 - Ycut])/2 + (10 - 140*m2 - 329*m2^2 + 700*m2^3 - 350*m2^4 + 
     140*m2^5 - 35*m2^6 + 4*m2^7 + 420*m2^2*Ycut + 210*m2^2*Ycut^2 + 
     140*m2^2*Ycut^3 + 105*m2^2*Ycut^4 + 84*m2^2*Ycut^5 - 70*Ycut^6 + 
     140*m2*Ycut^6 + 60*Ycut^7 + 280*api*X1El[3, c[VR]^2, Ycut, m2, mu2hat])/
    280)
