-3*m2^2*(3 + m2)*Log[m2] + 3*m2^2*(3 + m2)*Log[1 - Ycut] + 
 (mupi*((Ycut^5*(-1 + m2 + Ycut)*(-1 - m2 - 4*m2^2 + 3*Ycut + 2*m2*Ycut + 
        m2^2*Ycut - 3*Ycut^2 - m2*Ycut^2 + Ycut^3))/(3*(-1 + Ycut)^4) + 
     (Ycut^5*(-1 + m2 + Ycut)*(-1 - m2 - 4*m2^2 + 3*Ycut + 2*m2*Ycut + 
        m2^2*Ycut - 3*Ycut^2 - m2*Ycut^2 + Ycut^3)*c[SL]^2)/
      (12*(-1 + Ycut)^4) + (Ycut^5*(-1 + m2 + Ycut)*(-1 - m2 - 4*m2^2 + 
        3*Ycut + 2*m2*Ycut + m2^2*Ycut - 3*Ycut^2 - m2*Ycut^2 + Ycut^3)*
       c[SR]^2)/(12*(-1 + Ycut)^4) + (4*Ycut^5*(-1 + m2 + Ycut)*
       (-7 - 7*m2 - 4*m2^2 + 21*Ycut + 14*m2*Ycut + m2^2*Ycut - 21*Ycut^2 - 
        7*m2*Ycut^2 + 7*Ycut^3)*c[T]^2)/(3*(-1 + Ycut)^4) + 
     c[SL]*((m2^(3/2)*Ycut^5*(-1 + m2 + Ycut)*c[SR])/(-1 + Ycut)^3 + 
       (Ycut^5*(-1 + m2 + Ycut)*(2 + 2*m2 - 4*m2^2 - 6*Ycut - 4*m2*Ycut + 
          m2^2*Ycut + 6*Ycut^2 + 2*m2*Ycut^2 - 2*Ycut^3)*c[T])/
        (3*(-1 + Ycut)^4)) + (Ycut^5*(-1 + m2 + Ycut)*
       (-1 - m2 - 4*m2^2 + 3*Ycut + 2*m2*Ycut + m2^2*Ycut - 3*Ycut^2 - 
        m2*Ycut^2 + Ycut^3)*c[VL]^2)/(3*(-1 + Ycut)^4) - 
     (2*m2^(3/2)*Ycut^5*(-1 + m2 + Ycut)*c[VR])/(-1 + Ycut)^3 - 
     ((1 + m2 - Ycut)*Ycut^5*(-1 + m2 + Ycut)*c[VR]^2)/(-1 + Ycut)^2 + 
     c[VL]*((2*Ycut^5*(-1 + m2 + Ycut)*(-1 - m2 - 4*m2^2 + 3*Ycut + 
          2*m2*Ycut + m2^2*Ycut - 3*Ycut^2 - m2*Ycut^2 + Ycut^3))/
        (3*(-1 + Ycut)^4) - (2*m2^(3/2)*Ycut^5*(-1 + m2 + Ycut)*c[VR])/
        (-1 + Ycut)^3)) + 
   muG*(((-1 + m2 + Ycut)*(-6 - m2 - 13*m2^2 + 11*m2^3 - 3*m2^4 + 12*Ycut + 
        8*m2*Ycut + 31*m2^2*Ycut - 30*m2^3*Ycut + 9*m2^4*Ycut - 6*Ycut^2 - 
        13*m2*Ycut^2 - 18*m2^2*Ycut^2 + 24*m2^3*Ycut^2 - 9*m2^4*Ycut^2 + 
        4*m2*Ycut^3 - 2*m2^2*Ycut^3 - 2*m2^3*Ycut^3 + 3*m2^4*Ycut^3 - 
        3*Ycut^4 + 7*m2*Ycut^4 + 14*m2^2*Ycut^4 - 3*m2^3*Ycut^4 + 4*Ycut^5 - 
        7*m2*Ycut^5 - 2*m2^2*Ycut^5 + Ycut^6 + 2*m2*Ycut^6 - 2*Ycut^7))/
      (6*(-1 + Ycut)^3) - 2*m2*Log[m2] + 2*m2*Log[1 - Ycut] + 
     c[VL]^2*(((-1 + m2 + Ycut)*(-6 - m2 - 13*m2^2 + 11*m2^3 - 3*m2^4 + 
          12*Ycut + 8*m2*Ycut + 31*m2^2*Ycut - 30*m2^3*Ycut + 9*m2^4*Ycut - 
          6*Ycut^2 - 13*m2*Ycut^2 - 18*m2^2*Ycut^2 + 24*m2^3*Ycut^2 - 
          9*m2^4*Ycut^2 + 4*m2*Ycut^3 - 2*m2^2*Ycut^3 - 2*m2^3*Ycut^3 + 
          3*m2^4*Ycut^3 - 3*Ycut^4 + 7*m2*Ycut^4 + 14*m2^2*Ycut^4 - 
          3*m2^3*Ycut^4 + 4*Ycut^5 - 7*m2*Ycut^5 - 2*m2^2*Ycut^5 + Ycut^6 + 
          2*m2*Ycut^6 - 2*Ycut^7))/(6*(-1 + Ycut)^3) - 2*m2*Log[m2] + 
       2*m2*Log[1 - Ycut]) + c[VR]^2*
      (((-1 + m2 + Ycut)*(-3 + 9*m2 - 31*m2^2 + 17*m2^3 - 4*m2^4 - 
          3*m2*Ycut + 18*m2^2*Ycut - 13*m2^3*Ycut + 4*m2^4*Ycut - 
          3*m2*Ycut^2 + 9*m2^2*Ycut^2 - 4*m2^3*Ycut^2 - 3*m2*Ycut^3 + 
          4*m2^2*Ycut^3 + 9*Ycut^4 + 6*m2*Ycut^4 - 6*Ycut^5))/
        (6*(-1 + Ycut)) - 2*m2^2*Log[m2] + 2*m2^2*Log[1 - Ycut]) + 
     c[VR]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*(-19 + 49*m2 - 23*m2^2 + 5*m2^3 + 
           31*Ycut - 80*m2*Ycut + 41*m2^2*Ycut - 10*m2^3*Ycut - 6*Ycut^2 + 
           18*m2*Ycut^2 - 13*m2^2*Ycut^2 + 5*m2^3*Ycut^2 - 2*Ycut^3 + 
           8*m2*Ycut^3 - 5*m2^2*Ycut^3 - 10*Ycut^4 - 4*m2*Ycut^4 + 6*Ycut^5))/
         (-1 + Ycut)^2 + 4*Sqrt[m2]*(-1 + 2*m2)*Log[m2] - 
       4*Sqrt[m2]*(-1 + 2*m2)*Log[1 - Ycut]) + 
     c[SL]^2*(((-1 + m2 + Ycut)*(18 + 171*m2 - 21*m2^2 + 15*m2^3 - 3*m2^4 - 
          36*Ycut - 432*m2*Ycut + 51*m2^2*Ycut - 42*m2^3*Ycut + 9*m2^4*Ycut + 
          18*Ycut^2 + 315*m2*Ycut^2 - 30*m2^2*Ycut^2 + 36*m2^3*Ycut^2 - 
          9*m2^4*Ycut^2 - 36*m2*Ycut^3 - 6*m2^2*Ycut^3 - 6*m2^3*Ycut^3 + 
          3*m2^4*Ycut^3 - 3*Ycut^4 - 21*m2*Ycut^4 + 18*m2^2*Ycut^4 - 
          3*m2^3*Ycut^4 + 4*Ycut^5 + m2*Ycut^5 - 2*m2^2*Ycut^5 + Ycut^6 + 
          2*m2*Ycut^6 - 2*Ycut^7))/(24*(-1 + Ycut)^3) + 
       (3*m2*(3 + 2*m2)*Log[m2])/2 - (3*m2*(3 + 2*m2)*Log[1 - Ycut])/2) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(18 + 171*m2 - 21*m2^2 + 15*m2^3 - 3*m2^4 - 
          36*Ycut - 432*m2*Ycut + 51*m2^2*Ycut - 42*m2^3*Ycut + 9*m2^4*Ycut + 
          18*Ycut^2 + 315*m2*Ycut^2 - 30*m2^2*Ycut^2 + 36*m2^3*Ycut^2 - 
          9*m2^4*Ycut^2 - 36*m2*Ycut^3 - 6*m2^2*Ycut^3 - 6*m2^3*Ycut^3 + 
          3*m2^4*Ycut^3 - 3*Ycut^4 - 21*m2*Ycut^4 + 18*m2^2*Ycut^4 - 
          3*m2^3*Ycut^4 + 4*Ycut^5 + m2*Ycut^5 - 2*m2^2*Ycut^5 + Ycut^6 + 
          2*m2*Ycut^6 - 2*Ycut^7))/(24*(-1 + Ycut)^3) + 
       (3*m2*(3 + 2*m2)*Log[m2])/2 - (3*m2*(3 + 2*m2)*Log[1 - Ycut])/2) + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(-30 - 61*m2 - 45*m2^2 + 39*m2^3 - 11*m2^4 + 
          60*Ycut + 176*m2*Ycut + 107*m2^2*Ycut - 106*m2^3*Ycut + 
          33*m2^4*Ycut - 30*Ycut^2 - 157*m2*Ycut^2 - 62*m2^2*Ycut^2 + 
          84*m2^3*Ycut^2 - 33*m2^4*Ycut^2 + 28*m2*Ycut^3 - 6*m2^2*Ycut^3 - 
          6*m2^3*Ycut^3 + 11*m2^4*Ycut^3 + 33*Ycut^4 + 31*m2*Ycut^4 + 
          10*m2^2*Ycut^4 - 11*m2^3*Ycut^4 - 80*Ycut^5 - 31*m2*Ycut^5 + 
          6*m2^2*Ycut^5 + 61*Ycut^6 + 14*m2*Ycut^6 - 14*Ycut^7))/
        (3*(-1 + Ycut)^3) - 8*m2*(7 + 2*m2)*Log[m2] + 
       8*m2*(7 + 2*m2)*Log[1 - Ycut]) + 
     c[VL]*(((-1 + m2 + Ycut)*(-6 - m2 - 13*m2^2 + 11*m2^3 - 3*m2^4 + 
          12*Ycut + 8*m2*Ycut + 31*m2^2*Ycut - 30*m2^3*Ycut + 9*m2^4*Ycut - 
          6*Ycut^2 - 13*m2*Ycut^2 - 18*m2^2*Ycut^2 + 24*m2^3*Ycut^2 - 
          9*m2^4*Ycut^2 + 4*m2*Ycut^3 - 2*m2^2*Ycut^3 - 2*m2^3*Ycut^3 + 
          3*m2^4*Ycut^3 - 3*Ycut^4 + 7*m2*Ycut^4 + 14*m2^2*Ycut^4 - 
          3*m2^3*Ycut^4 + 4*Ycut^5 - 7*m2*Ycut^5 - 2*m2^2*Ycut^5 + Ycut^6 + 
          2*m2*Ycut^6 - 2*Ycut^7))/(3*(-1 + Ycut)^3) - 4*m2*Log[m2] + 
       4*m2*Log[1 - Ycut] + c[VR]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*
            (-19 + 49*m2 - 23*m2^2 + 5*m2^3 + 31*Ycut - 80*m2*Ycut + 
             41*m2^2*Ycut - 10*m2^3*Ycut - 6*Ycut^2 + 18*m2*Ycut^2 - 
             13*m2^2*Ycut^2 + 5*m2^3*Ycut^2 - 2*Ycut^3 + 8*m2*Ycut^3 - 
             5*m2^2*Ycut^3 - 10*Ycut^4 - 4*m2*Ycut^4 + 6*Ycut^5))/
           (-1 + Ycut)^2 + 4*Sqrt[m2]*(-1 + 2*m2)*Log[m2] - 
         4*Sqrt[m2]*(-1 + 2*m2)*Log[1 - Ycut])) + 
     c[SL]*(c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(37 + 29*m2 - 7*m2^2 + m2^3 - 
            49*Ycut - 52*m2*Ycut + 13*m2^2*Ycut - 2*m2^3*Ycut + 6*Ycut^2 + 
            18*m2*Ycut^2 - 5*m2^2*Ycut^2 + m2^3*Ycut^2 + 2*Ycut^3 + 
            4*m2*Ycut^3 - m2^2*Ycut^3 + 4*Ycut^4 - 2*m2*Ycut^4))/
          (2*(-1 + Ycut)^2) - 6*Sqrt[m2]*(1 + 4*m2)*Log[m2] + 
         6*Sqrt[m2]*(1 + 4*m2)*Log[1 - Ycut]) + 
       c[T]*(-1/6*((-1 + m2 + Ycut)*(-37*m2 - 29*m2^2 + 7*m2^3 - m2^4 + 
             86*m2*Ycut + 81*m2^2*Ycut - 20*m2^3*Ycut + 3*m2^4*Ycut - 
             55*m2*Ycut^2 - 70*m2^2*Ycut^2 + 18*m2^3*Ycut^2 - 3*m2^4*Ycut^2 + 
             4*m2*Ycut^3 + 14*m2^2*Ycut^3 - 4*m2^3*Ycut^3 + m2^4*Ycut^3 + 
             3*Ycut^4 + 13*m2*Ycut^4 - 12*m2^2*Ycut^4 - m2^3*Ycut^4 - 
             10*Ycut^5 - 15*m2*Ycut^5 + 6*m2^2*Ycut^5 + 11*Ycut^6 + 
             4*m2*Ycut^6 - 4*Ycut^7))/(-1 + Ycut)^3 + 2*m2*(1 + 4*m2)*
          Log[m2] - 2*m2*(1 + 4*m2)*Log[1 - Ycut]))))/mb^2 + 
 (rhoLS*(-1/15*((-1 + m2 + Ycut)*(9 - 36*m2 + 54*m2^2 - 36*m2^3 + 9*m2^4 - 
         27*Ycut + 117*m2*Ycut - 189*m2^2*Ycut + 135*m2^3*Ycut - 
         36*m2^4*Ycut + 27*Ycut^2 - 126*m2*Ycut^2 + 225*m2^2*Ycut^2 - 
         180*m2^3*Ycut^2 + 54*m2^4*Ycut^2 - 9*Ycut^3 + 45*m2*Ycut^3 - 
         90*m2^2*Ycut^3 + 90*m2^3*Ycut^3 - 36*m2^4*Ycut^3 - 45*m2^2*Ycut^4 + 
         9*m2^4*Ycut^4 + Ycut^5 + m2*Ycut^5 + 31*m2^2*Ycut^5 - 
         9*m2^3*Ycut^5 - 3*Ycut^6 - 2*m2*Ycut^6 - m2^2*Ycut^6 + 3*Ycut^7 + 
         m2*Ycut^7 - Ycut^8))/(-1 + Ycut)^4 - 
     ((-1 + m2 + Ycut)*(9 - 36*m2 + 54*m2^2 - 36*m2^3 + 9*m2^4 - 27*Ycut + 
        117*m2*Ycut - 189*m2^2*Ycut + 135*m2^3*Ycut - 36*m2^4*Ycut + 
        27*Ycut^2 - 126*m2*Ycut^2 + 225*m2^2*Ycut^2 - 180*m2^3*Ycut^2 + 
        54*m2^4*Ycut^2 - 9*Ycut^3 + 45*m2*Ycut^3 - 90*m2^2*Ycut^3 + 
        90*m2^3*Ycut^3 - 36*m2^4*Ycut^3 - 45*m2^2*Ycut^4 + 9*m2^4*Ycut^4 + 
        Ycut^5 + m2*Ycut^5 + 31*m2^2*Ycut^5 - 9*m2^3*Ycut^5 - 3*Ycut^6 - 
        2*m2*Ycut^6 - m2^2*Ycut^6 + 3*Ycut^7 + m2*Ycut^7 - Ycut^8)*c[VL]^2)/
      (15*(-1 + Ycut)^4) - ((-1 + m2 + Ycut)*(4 - 16*m2 + 24*m2^2 - 16*m2^3 + 
        4*m2^4 - 4*Ycut + 20*m2*Ycut - 36*m2^2*Ycut + 28*m2^3*Ycut - 
        8*m2^4*Ycut + 4*m2^2*Ycut^2 - 8*m2^3*Ycut^2 + 4*m2^4*Ycut^2 + 
        4*m2^2*Ycut^3 - 4*m2^3*Ycut^3 + 4*m2^2*Ycut^4 + Ycut^5 + m2*Ycut^5 - 
        Ycut^6)*c[VR]^2)/(5*(-1 + Ycut)^2) + 
     c[T]^2*((-4*(-1 + m2 + Ycut)*(78 + 63*m2 + 123*m2^2 - 117*m2^3 + 
          33*m2^4 - 234*Ycut - 291*m2*Ycut - 408*m2^2*Ycut + 435*m2^3*Ycut - 
          132*m2^4*Ycut + 234*Ycut^2 + 483*m2*Ycut^2 + 435*m2^2*Ycut^2 - 
          570*m2^3*Ycut^2 + 198*m2^4*Ycut^2 - 78*Ycut^3 - 315*m2*Ycut^3 - 
          120*m2^2*Ycut^3 + 270*m2^3*Ycut^3 - 132*m2^4*Ycut^3 - 
          60*m2^2*Ycut^4 + 15*m2^3*Ycut^4 + 33*m2^4*Ycut^4 + 7*Ycut^5 + 
          67*m2*Ycut^5 - 8*m2^2*Ycut^5 - 33*m2^3*Ycut^5 - 21*Ycut^6 - 
          14*m2*Ycut^6 + 23*m2^2*Ycut^6 + 21*Ycut^7 + 7*m2*Ycut^7 - 
          7*Ycut^8))/(15*(-1 + Ycut)^4) + 48*m2*Log[m2] - 
       48*m2*Log[1 - Ycut]) + c[SL]^2*
      (-1/60*((-1 + m2 + Ycut)*(-36 - 231*m2 + 129*m2^2 - 51*m2^3 + 9*m2^4 + 
           108*Ycut + 837*m2*Ycut - 474*m2^2*Ycut + 195*m2^3*Ycut - 
           36*m2^4*Ycut - 108*Ycut^2 - 1071*m2*Ycut^2 + 615*m2^2*Ycut^2 - 
           270*m2^3*Ycut^2 + 54*m2^4*Ycut^2 + 36*Ycut^3 + 525*m2*Ycut^3 - 
           300*m2^2*Ycut^3 + 150*m2^3*Ycut^3 - 36*m2^4*Ycut^3 - 
           30*m2^2*Ycut^4 - 15*m2^3*Ycut^4 + 9*m2^4*Ycut^4 + Ycut^5 - 
           59*m2*Ycut^5 + 46*m2^2*Ycut^5 - 9*m2^3*Ycut^5 - 3*Ycut^6 - 
           2*m2*Ycut^6 - m2^2*Ycut^6 + 3*Ycut^7 + m2*Ycut^7 - Ycut^8))/
         (-1 + Ycut)^4 - 3*m2*Log[m2] + 3*m2*Log[1 - Ycut]) + 
     c[SR]^2*(-1/60*((-1 + m2 + Ycut)*(-36 - 231*m2 + 129*m2^2 - 51*m2^3 + 
           9*m2^4 + 108*Ycut + 837*m2*Ycut - 474*m2^2*Ycut + 195*m2^3*Ycut - 
           36*m2^4*Ycut - 108*Ycut^2 - 1071*m2*Ycut^2 + 615*m2^2*Ycut^2 - 
           270*m2^3*Ycut^2 + 54*m2^4*Ycut^2 + 36*Ycut^3 + 525*m2*Ycut^3 - 
           300*m2^2*Ycut^3 + 150*m2^3*Ycut^3 - 36*m2^4*Ycut^3 - 
           30*m2^2*Ycut^4 - 15*m2^3*Ycut^4 + 9*m2^4*Ycut^4 + Ycut^5 - 
           59*m2*Ycut^5 + 46*m2^2*Ycut^5 - 9*m2^3*Ycut^5 - 3*Ycut^6 - 
           2*m2*Ycut^6 - m2^2*Ycut^6 + 3*Ycut^7 + m2*Ycut^7 - Ycut^8))/
         (-1 + Ycut)^4 - 3*m2*Log[m2] + 3*m2*Log[1 - Ycut]) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-16 + 62*m2 - 28*m2^2 + 6*m2^3 + 
          28*Ycut - 102*m2*Ycut + 50*m2^2*Ycut - 12*m2^3*Ycut - 6*Ycut^2 + 
          24*m2*Ycut^2 - 16*m2^2*Ycut^2 + 6*m2^3*Ycut^2 - 2*Ycut^3 + 
          10*m2*Ycut^3 - 6*m2^2*Ycut^3 - Ycut^4 - 3*m2*Ycut^4 - 3*Ycut^5))/
        (3*(-1 + Ycut)^2) - 4*Sqrt[m2]*(-1 + 3*m2)*Log[m2] + 
       4*Sqrt[m2]*(-1 + 3*m2)*Log[1 - Ycut]) + 
     c[SR]*c[T]*((2*Sqrt[m2]*(-1 + m2 + Ycut)*(-17 - 8*m2 + m2^2 + 23*Ycut + 
          15*m2*Ycut - 2*m2^2*Ycut - 3*Ycut^2 - 6*m2*Ycut^2 + m2^2*Ycut^2 - 
          Ycut^3 - m2*Ycut^3 + 4*Ycut^4))/(3*(-1 + Ycut)^2) + 
       4*Sqrt[m2]*(1 + 3*m2)*Log[m2] - 4*Sqrt[m2]*(1 + 3*m2)*Log[1 - Ycut]) + 
     c[VL]*((-2*(-1 + m2 + Ycut)*(9 - 36*m2 + 54*m2^2 - 36*m2^3 + 9*m2^4 - 
          27*Ycut + 117*m2*Ycut - 189*m2^2*Ycut + 135*m2^3*Ycut - 
          36*m2^4*Ycut + 27*Ycut^2 - 126*m2*Ycut^2 + 225*m2^2*Ycut^2 - 
          180*m2^3*Ycut^2 + 54*m2^4*Ycut^2 - 9*Ycut^3 + 45*m2*Ycut^3 - 
          90*m2^2*Ycut^3 + 90*m2^3*Ycut^3 - 36*m2^4*Ycut^3 - 45*m2^2*Ycut^4 + 
          9*m2^4*Ycut^4 + Ycut^5 + m2*Ycut^5 + 31*m2^2*Ycut^5 - 
          9*m2^3*Ycut^5 - 3*Ycut^6 - 2*m2*Ycut^6 - m2^2*Ycut^6 + 3*Ycut^7 + 
          m2*Ycut^7 - Ycut^8))/(15*(-1 + Ycut)^4) + 
       c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-16 + 62*m2 - 28*m2^2 + 6*m2^3 + 
            28*Ycut - 102*m2*Ycut + 50*m2^2*Ycut - 12*m2^3*Ycut - 6*Ycut^2 + 
            24*m2*Ycut^2 - 16*m2^2*Ycut^2 + 6*m2^3*Ycut^2 - 2*Ycut^3 + 
            10*m2*Ycut^3 - 6*m2^2*Ycut^3 - Ycut^4 - 3*m2*Ycut^4 - 3*Ycut^5))/
          (3*(-1 + Ycut)^2) - 4*Sqrt[m2]*(-1 + 3*m2)*Log[m2] + 
         4*Sqrt[m2]*(-1 + 3*m2)*Log[1 - Ycut])) + 
     c[SL]*(((-1 + m2 + Ycut)*(3 - 12*m2 + 18*m2^2 - 12*m2^3 + 3*m2^4 - 
          9*Ycut + 39*m2*Ycut - 63*m2^2*Ycut + 45*m2^3*Ycut - 12*m2^4*Ycut + 
          9*Ycut^2 - 42*m2*Ycut^2 + 75*m2^2*Ycut^2 - 60*m2^3*Ycut^2 + 
          18*m2^4*Ycut^2 - 3*Ycut^3 + 15*m2*Ycut^3 - 30*m2^2*Ycut^3 + 
          30*m2^3*Ycut^3 - 12*m2^4*Ycut^3 + 45*m2^2*Ycut^4 + 3*m2^4*Ycut^4 + 
          2*Ycut^5 + 2*m2*Ycut^5 - 43*m2^2*Ycut^5 - 3*m2^3*Ycut^5 - 
          6*Ycut^6 - 4*m2*Ycut^6 + 13*m2^2*Ycut^6 + 6*Ycut^7 + 2*m2*Ycut^7 - 
          2*Ycut^8)*c[T])/(15*(-1 + Ycut)^4) + 
       c[SR]*(-1/2*(Sqrt[m2]*(-1 + m2 + Ycut)*(37 + 29*m2 - 7*m2^2 + m2^3 - 
             49*Ycut - 52*m2*Ycut + 13*m2^2*Ycut - 2*m2^3*Ycut + 6*Ycut^2 + 
             18*m2*Ycut^2 - 5*m2^2*Ycut^2 + m2^3*Ycut^2 + 2*Ycut^3 + 
             4*m2*Ycut^3 - m2^2*Ycut^3 + 4*Ycut^4 - 2*m2*Ycut^4))/
           (-1 + Ycut)^2 + 6*Sqrt[m2]*(1 + 4*m2)*Log[m2] - 
         6*Sqrt[m2]*(1 + 4*m2)*Log[1 - Ycut]))) + 
   rhoD*((-327 + 730*m2 - 540*m2^2 + 120*m2^3 + 35*m2^4 - 18*m2^5 + 
       1815*Ycut - 3770*m2*Ycut + 2700*m2^2*Ycut - 600*m2^3*Ycut - 
       175*m2^4*Ycut + 90*m2^5*Ycut - 4080*Ycut^2 + 7840*m2*Ycut^2 - 
       5400*m2^2*Ycut^2 + 1200*m2^3*Ycut^2 + 350*m2^4*Ycut^2 - 
       180*m2^5*Ycut^2 + 4680*Ycut^3 - 8240*m2*Ycut^3 + 5400*m2^2*Ycut^3 - 
       1200*m2^3*Ycut^3 - 350*m2^4*Ycut^3 + 180*m2^5*Ycut^3 - 2880*Ycut^4 + 
       4510*m2*Ycut^4 - 2790*m2^2*Ycut^4 + 690*m2^3*Ycut^4 + 
       175*m2^4*Ycut^4 - 90*m2^5*Ycut^4 + 1149*Ycut^5 - 1310*m2*Ycut^5 + 
       645*m2^2*Ycut^5 - 150*m2^3*Ycut^5 - 35*m2^4*Ycut^5 + 18*m2^5*Ycut^5 - 
       765*Ycut^6 + 400*m2*Ycut^6 + 15*m2^2*Ycut^6 + 630*Ycut^7 - 
       200*m2*Ycut^7 - 30*m2^2*Ycut^7 - 255*Ycut^8 + 40*m2*Ycut^8 + 
       30*Ycut^9 + 3*Ycut^10)/(45*(-1 + Ycut)^5) - (4*(-3 + 2*m2)*Log[m2])/
      3 + c[VR]^2*((-117 + 200*m2 - 100*m2^2 + 25*m2^4 - 8*m2^5 + 411*Ycut - 
         600*m2*Ycut + 300*m2^2*Ycut - 75*m2^4*Ycut + 24*m2^5*Ycut - 
         501*Ycut^2 + 600*m2*Ycut^2 - 300*m2^2*Ycut^2 + 75*m2^4*Ycut^2 - 
         24*m2^5*Ycut^2 + 227*Ycut^3 - 200*m2*Ycut^3 + 100*m2^2*Ycut^3 - 
         25*m2^4*Ycut^3 + 8*m2^5*Ycut^3 - 45*Ycut^4 + 30*m2^2*Ycut^4 + 
         72*Ycut^5 - 15*m2^2*Ycut^5 - 56*Ycut^6 - 5*m2^2*Ycut^6 + 6*Ycut^7 + 
         3*Ycut^8)/(15*(-1 + Ycut)^3) + 4*Log[m2] - 4*Log[1 - Ycut]) + 
     (4*(-3 + 2*m2)*Log[1 - Ycut])/3 + 
     c[SL]^2*((-274 + 350*m2 - 120*m2^2 + 40*m2^3 + 10*m2^4 - 6*m2^5 + 
         1490*Ycut - 1630*m2*Ycut + 600*m2^2*Ycut - 200*m2^3*Ycut - 
         50*m2^4*Ycut + 30*m2^5*Ycut - 3280*Ycut^2 + 2960*m2*Ycut^2 - 
         1200*m2^2*Ycut^2 + 400*m2^3*Ycut^2 + 100*m2^4*Ycut^2 - 
         60*m2^5*Ycut^2 + 3680*Ycut^3 - 2560*m2*Ycut^3 + 1200*m2^2*Ycut^3 - 
         400*m2^3*Ycut^3 - 100*m2^4*Ycut^3 + 60*m2^5*Ycut^3 - 2170*Ycut^4 + 
         1010*m2*Ycut^4 - 630*m2^2*Ycut^4 + 230*m2^3*Ycut^4 + 
         50*m2^4*Ycut^4 - 30*m2^5*Ycut^4 + 673*Ycut^5 - 170*m2*Ycut^5 + 
         155*m2^2*Ycut^5 - 50*m2^3*Ycut^5 - 10*m2^4*Ycut^5 + 6*m2^5*Ycut^5 - 
         215*Ycut^6 + 80*m2*Ycut^6 + 5*m2^2*Ycut^6 + 130*Ycut^7 - 
         40*m2*Ycut^7 - 10*m2^2*Ycut^7 - 30*Ycut^8 - 5*Ycut^9 + Ycut^10)/
        (60*(-1 + Ycut)^5) + 2*(1 + m2)*Log[m2] - 2*(1 + m2)*Log[1 - Ycut]) + 
     c[SR]^2*((-274 + 350*m2 - 120*m2^2 + 40*m2^3 + 10*m2^4 - 6*m2^5 + 
         1490*Ycut - 1630*m2*Ycut + 600*m2^2*Ycut - 200*m2^3*Ycut - 
         50*m2^4*Ycut + 30*m2^5*Ycut - 3280*Ycut^2 + 2960*m2*Ycut^2 - 
         1200*m2^2*Ycut^2 + 400*m2^3*Ycut^2 + 100*m2^4*Ycut^2 - 
         60*m2^5*Ycut^2 + 3680*Ycut^3 - 2560*m2*Ycut^3 + 1200*m2^2*Ycut^3 - 
         400*m2^3*Ycut^3 - 100*m2^4*Ycut^3 + 60*m2^5*Ycut^3 - 2170*Ycut^4 + 
         1010*m2*Ycut^4 - 630*m2^2*Ycut^4 + 230*m2^3*Ycut^4 + 
         50*m2^4*Ycut^4 - 30*m2^5*Ycut^4 + 673*Ycut^5 - 170*m2*Ycut^5 + 
         155*m2^2*Ycut^5 - 50*m2^3*Ycut^5 - 10*m2^4*Ycut^5 + 6*m2^5*Ycut^5 - 
         215*Ycut^6 + 80*m2*Ycut^6 + 5*m2^2*Ycut^6 + 130*Ycut^7 - 
         40*m2*Ycut^7 - 10*m2^2*Ycut^7 - 30*Ycut^8 - 5*Ycut^9 + Ycut^10)/
        (60*(-1 + Ycut)^5) + 2*(1 + m2)*Log[m2] - 2*(1 + m2)*Log[1 - Ycut]) + 
     c[VL]^2*((-327 + 730*m2 - 540*m2^2 + 120*m2^3 + 35*m2^4 - 18*m2^5 + 
         1815*Ycut - 3770*m2*Ycut + 2700*m2^2*Ycut - 600*m2^3*Ycut - 
         175*m2^4*Ycut + 90*m2^5*Ycut - 4080*Ycut^2 + 7840*m2*Ycut^2 - 
         5400*m2^2*Ycut^2 + 1200*m2^3*Ycut^2 + 350*m2^4*Ycut^2 - 
         180*m2^5*Ycut^2 + 4680*Ycut^3 - 8240*m2*Ycut^3 + 5400*m2^2*Ycut^3 - 
         1200*m2^3*Ycut^3 - 350*m2^4*Ycut^3 + 180*m2^5*Ycut^3 - 2880*Ycut^4 + 
         4510*m2*Ycut^4 - 2790*m2^2*Ycut^4 + 690*m2^3*Ycut^4 + 
         175*m2^4*Ycut^4 - 90*m2^5*Ycut^4 + 1149*Ycut^5 - 1310*m2*Ycut^5 + 
         645*m2^2*Ycut^5 - 150*m2^3*Ycut^5 - 35*m2^4*Ycut^5 + 
         18*m2^5*Ycut^5 - 765*Ycut^6 + 400*m2*Ycut^6 + 15*m2^2*Ycut^6 + 
         630*Ycut^7 - 200*m2*Ycut^7 - 30*m2^2*Ycut^7 - 255*Ycut^8 + 
         40*m2*Ycut^8 + 30*Ycut^9 + 3*Ycut^10)/(45*(-1 + Ycut)^5) - 
       (4*(-3 + 2*m2)*Log[m2])/3 + (4*(-3 + 2*m2)*Log[1 - Ycut])/3) + 
     c[VR]*((-2*Sqrt[m2]*(62 - 56*m2 - 8*m2^3 + 2*m2^4 - 272*Ycut + 
          176*m2*Ycut + 32*m2^3*Ycut - 8*m2^4*Ycut + 456*Ycut^2 - 
          168*m2*Ycut^2 - 48*m2^3*Ycut^2 + 12*m2^4*Ycut^2 - 352*Ycut^3 + 
          16*m2*Ycut^3 + 32*m2^3*Ycut^3 - 8*m2^4*Ycut^3 + 115*Ycut^4 + 
          44*m2*Ycut^4 - 3*m2^2*Ycut^4 - 8*m2^3*Ycut^4 + 2*m2^4*Ycut^4 - 
          18*Ycut^5 + m2*Ycut^5 - 3*m2^2*Ycut^5 + 20*Ycut^6 - 20*m2*Ycut^6 + 
          3*m2^2*Ycut^6 - 14*Ycut^7 + 7*m2*Ycut^7 + 3*Ycut^8))/
        (3*(-1 + Ycut)^4) - 16*Sqrt[m2]*(1 + 2*m2)*Log[m2] + 
       16*Sqrt[m2]*(1 + 2*m2)*Log[1 - Ycut]) + 
     c[T]^2*((4*(-534 + 1610*m2 - 1320*m2^2 + 120*m2^3 + 190*m2^4 - 66*m2^5 + 
          3030*Ycut - 8650*m2*Ycut + 6600*m2^2*Ycut - 600*m2^3*Ycut - 
          950*m2^4*Ycut + 330*m2^5*Ycut - 6960*Ycut^2 + 18800*m2*Ycut^2 - 
          13200*m2^2*Ycut^2 + 1200*m2^3*Ycut^2 + 1900*m2^4*Ycut^2 - 
          660*m2^5*Ycut^2 + 8160*Ycut^3 - 20800*m2*Ycut^3 + 
          13200*m2^2*Ycut^3 - 1200*m2^3*Ycut^3 - 1900*m2^4*Ycut^3 + 
          660*m2^5*Ycut^3 - 5250*Ycut^4 + 11990*m2*Ycut^4 - 
          6510*m2^2*Ycut^4 + 690*m2^3*Ycut^4 + 950*m2^4*Ycut^4 - 
          330*m2^5*Ycut^4 + 2613*Ycut^5 - 3310*m2*Ycut^5 + 975*m2^2*Ycut^5 - 
          150*m2^3*Ycut^5 - 190*m2^4*Ycut^5 + 66*m2^5*Ycut^5 - 2355*Ycut^6 + 
          560*m2*Ycut^6 + 345*m2^2*Ycut^6 + 2010*Ycut^7 - 280*m2*Ycut^7 - 
          60*m2^2*Ycut^7 - 810*Ycut^8 + 80*m2*Ycut^8 - 30*m2^2*Ycut^8 + 
          75*Ycut^9 + 21*Ycut^10))/(45*(-1 + Ycut)^5) - 
       (32*(-3 + 5*m2)*Log[m2])/3 + (32*(-3 + 5*m2)*Log[1 - Ycut])/3) + 
     c[VL]*((2*(-327 + 730*m2 - 540*m2^2 + 120*m2^3 + 35*m2^4 - 18*m2^5 + 
          1815*Ycut - 3770*m2*Ycut + 2700*m2^2*Ycut - 600*m2^3*Ycut - 
          175*m2^4*Ycut + 90*m2^5*Ycut - 4080*Ycut^2 + 7840*m2*Ycut^2 - 
          5400*m2^2*Ycut^2 + 1200*m2^3*Ycut^2 + 350*m2^4*Ycut^2 - 
          180*m2^5*Ycut^2 + 4680*Ycut^3 - 8240*m2*Ycut^3 + 5400*m2^2*Ycut^3 - 
          1200*m2^3*Ycut^3 - 350*m2^4*Ycut^3 + 180*m2^5*Ycut^3 - 
          2880*Ycut^4 + 4510*m2*Ycut^4 - 2790*m2^2*Ycut^4 + 690*m2^3*Ycut^4 + 
          175*m2^4*Ycut^4 - 90*m2^5*Ycut^4 + 1149*Ycut^5 - 1310*m2*Ycut^5 + 
          645*m2^2*Ycut^5 - 150*m2^3*Ycut^5 - 35*m2^4*Ycut^5 + 
          18*m2^5*Ycut^5 - 765*Ycut^6 + 400*m2*Ycut^6 + 15*m2^2*Ycut^6 + 
          630*Ycut^7 - 200*m2*Ycut^7 - 30*m2^2*Ycut^7 - 255*Ycut^8 + 
          40*m2*Ycut^8 + 30*Ycut^9 + 3*Ycut^10))/(45*(-1 + Ycut)^5) - 
       (8*(-3 + 2*m2)*Log[m2])/3 + (8*(-3 + 2*m2)*Log[1 - Ycut])/3 + 
       c[VR]*((-2*Sqrt[m2]*(62 - 56*m2 - 8*m2^3 + 2*m2^4 - 272*Ycut + 
            176*m2*Ycut + 32*m2^3*Ycut - 8*m2^4*Ycut + 456*Ycut^2 - 
            168*m2*Ycut^2 - 48*m2^3*Ycut^2 + 12*m2^4*Ycut^2 - 352*Ycut^3 + 
            16*m2*Ycut^3 + 32*m2^3*Ycut^3 - 8*m2^4*Ycut^3 + 115*Ycut^4 + 
            44*m2*Ycut^4 - 3*m2^2*Ycut^4 - 8*m2^3*Ycut^4 + 2*m2^4*Ycut^4 - 
            18*Ycut^5 + m2*Ycut^5 - 3*m2^2*Ycut^5 + 20*Ycut^6 - 
            20*m2*Ycut^6 + 3*m2^2*Ycut^6 - 14*Ycut^7 + 7*m2*Ycut^7 + 
            3*Ycut^8))/(3*(-1 + Ycut)^4) - 16*Sqrt[m2]*(1 + 2*m2)*Log[m2] + 
         16*Sqrt[m2]*(1 + 2*m2)*Log[1 - Ycut])) + 
     c[SL]*(c[T]*(-1/45*(-24 - 130*m2 + 240*m2^2 - 120*m2^3 + 40*m2^4 - 
            6*m2^5 + 120*Ycut + 770*m2*Ycut - 1200*m2^2*Ycut + 
            600*m2^3*Ycut - 200*m2^4*Ycut + 30*m2^5*Ycut - 240*Ycut^2 - 
            1840*m2*Ycut^2 + 2400*m2^2*Ycut^2 - 1200*m2^3*Ycut^2 + 
            400*m2^4*Ycut^2 - 60*m2^5*Ycut^2 + 240*Ycut^3 + 2240*m2*Ycut^3 - 
            2400*m2^2*Ycut^3 + 1200*m2^3*Ycut^3 - 400*m2^4*Ycut^3 + 
            60*m2^5*Ycut^3 - 120*Ycut^4 - 1510*m2*Ycut^4 + 1380*m2^2*Ycut^4 - 
            690*m2^3*Ycut^4 + 200*m2^4*Ycut^4 - 30*m2^5*Ycut^4 + 18*Ycut^5 + 
            710*m2*Ycut^5 - 570*m2^2*Ycut^5 + 150*m2^3*Ycut^5 - 
            40*m2^4*Ycut^5 + 6*m2^5*Ycut^5 + 30*Ycut^6 - 400*m2*Ycut^6 + 
            150*m2^2*Ycut^6 - 60*Ycut^7 + 200*m2*Ycut^7 + 15*m2^2*Ycut^7 + 
            60*Ycut^8 - 40*m2*Ycut^8 - 15*m2^2*Ycut^8 - 30*Ycut^9 + 
            6*Ycut^10)/(-1 + Ycut)^5 - (8*m2*Log[m2])/3 + 
         (8*m2*Log[1 - Ycut])/3) + 
       c[SR]*((Sqrt[m2]*(99 - 64*m2 - 36*m2^2 + m2^4 - 432*Ycut + 
            160*m2*Ycut + 144*m2^2*Ycut - 4*m2^4*Ycut + 720*Ycut^2 - 
            48*m2*Ycut^2 - 216*m2^2*Ycut^2 + 6*m2^4*Ycut^2 - 552*Ycut^3 - 
            160*m2*Ycut^3 + 144*m2^2*Ycut^3 - 4*m2^4*Ycut^3 + 177*Ycut^4 + 
            136*m2*Ycut^4 - 39*m2^2*Ycut^4 + m2^4*Ycut^4 - 18*Ycut^5 - 
            11*m2*Ycut^5 - 3*m2^2*Ycut^5 + 12*Ycut^6 - 16*m2*Ycut^6 + 
            3*m2^2*Ycut^6 - 6*Ycut^7 + 3*m2*Ycut^7))/(3*(-1 + Ycut)^4) + 
         4*Sqrt[m2]*(3 + 8*m2)*Log[m2] - 4*Sqrt[m2]*(3 + 8*m2)*
          Log[1 - Ycut]))))/mb^3 + 
 (7 - 75*m2 - 120*m2^2 + 200*m2^3 - 15*m2^4 + 3*m2^5 - 14*Ycut + 
   150*m2*Ycut + 420*m2^2*Ycut - 340*m2^3*Ycut + 30*m2^4*Ycut - 6*m2^5*Ycut + 
   7*Ycut^2 - 75*m2*Ycut^2 - 390*m2^2*Ycut^2 + 110*m2^3*Ycut^2 - 
   15*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 60*m2^2*Ycut^3 + 20*m2^3*Ycut^3 - 
   15*Ycut^4 + 15*m2*Ycut^4 + 30*m2^2*Ycut^4 - 10*m2^3*Ycut^4 + 38*Ycut^5 - 
   30*m2*Ycut^5 - 31*Ycut^6 + 15*m2*Ycut^6 + 8*Ycut^7 + 
   20*api*X1El[1, SM^2, Ycut, m2, mu2hat] - 
   40*api*Ycut*X1El[1, SM^2, Ycut, m2, mu2hat] + 
   20*api*Ycut^2*X1El[1, SM^2, Ycut, m2, mu2hat])/(20*(-1 + Ycut)^2) + 
 c[SL]^2*((-3*m2^2*(3 + m2)*Log[m2])/4 + (3*m2^2*(3 + m2)*Log[1 - Ycut])/4 + 
   (7 - 75*m2 - 120*m2^2 + 200*m2^3 - 15*m2^4 + 3*m2^5 - 14*Ycut + 
     150*m2*Ycut + 420*m2^2*Ycut - 340*m2^3*Ycut + 30*m2^4*Ycut - 
     6*m2^5*Ycut + 7*Ycut^2 - 75*m2*Ycut^2 - 390*m2^2*Ycut^2 + 
     110*m2^3*Ycut^2 - 15*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 60*m2^2*Ycut^3 + 
     20*m2^3*Ycut^3 - 15*Ycut^4 + 15*m2*Ycut^4 + 30*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 38*Ycut^5 - 30*m2*Ycut^5 - 31*Ycut^6 + 15*m2*Ycut^6 + 
     8*Ycut^7 + 80*api*X1El[1, c[SL]^2, Ycut, m2, mu2hat] - 
     160*api*Ycut*X1El[1, c[SL]^2, Ycut, m2, mu2hat] + 
     80*api*Ycut^2*X1El[1, c[SL]^2, Ycut, m2, mu2hat])/(80*(-1 + Ycut)^2)) + 
 c[SR]^2*((-3*m2^2*(3 + m2)*Log[m2])/4 + (3*m2^2*(3 + m2)*Log[1 - Ycut])/4 + 
   (7 - 75*m2 - 120*m2^2 + 200*m2^3 - 15*m2^4 + 3*m2^5 - 14*Ycut + 
     150*m2*Ycut + 420*m2^2*Ycut - 340*m2^3*Ycut + 30*m2^4*Ycut - 
     6*m2^5*Ycut + 7*Ycut^2 - 75*m2*Ycut^2 - 390*m2^2*Ycut^2 + 
     110*m2^3*Ycut^2 - 15*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 60*m2^2*Ycut^3 + 
     20*m2^3*Ycut^3 - 15*Ycut^4 + 15*m2*Ycut^4 + 30*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 38*Ycut^5 - 30*m2*Ycut^5 - 31*Ycut^6 + 15*m2*Ycut^6 + 
     8*Ycut^7 + 80*api*X1El[1, c[SR]^2, Ycut, m2, mu2hat] - 
     160*api*Ycut*X1El[1, c[SR]^2, Ycut, m2, mu2hat] + 
     80*api*Ycut^2*X1El[1, c[SR]^2, Ycut, m2, mu2hat])/(80*(-1 + Ycut)^2)) + 
 c[SL]*(c[SR]*(3*m2^(3/2)*(2 + 3*m2)*Log[m2] - 3*m2^(3/2)*(2 + 3*m2)*
      Log[1 - Ycut] + (-3*Sqrt[m2] - 44*m2^(3/2) + 36*m2^(5/2) + 
       12*m2^(7/2) - m2^(9/2) + 3*Sqrt[m2]*Ycut + 68*m2^(3/2)*Ycut - 
       12*m2^(7/2)*Ycut + m2^(9/2)*Ycut - 12*m2^(3/2)*Ycut^2 - 
       18*m2^(5/2)*Ycut^2 - 4*m2^(3/2)*Ycut^3 - 6*m2^(5/2)*Ycut^3 + 
       3*Sqrt[m2]*Ycut^4 - 8*m2^(3/2)*Ycut^4 - 3*Sqrt[m2]*Ycut^5 - 
       4*api*X1El[1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       4*api*Ycut*X1El[1, c[SL]*c[SR], Ycut, m2, mu2hat])/(4*(-1 + Ycut))) + 
   c[T]*(-3*m2^2*(1 + m2)*Log[m2] + 3*m2^2*(1 + m2)*Log[1 - Ycut] - 
     (-1 + 15*m2 + 80*m2^2 - 80*m2^3 - 15*m2^4 + m2^5 + 2*Ycut - 30*m2*Ycut - 
       220*m2^2*Ycut + 100*m2^3*Ycut + 30*m2^4*Ycut - 2*m2^5*Ycut - Ycut^2 + 
       15*m2*Ycut^2 + 170*m2^2*Ycut^2 + 10*m2^3*Ycut^2 - 15*m2^4*Ycut^2 + 
       m2^5*Ycut^2 - 20*m2^2*Ycut^3 - 20*m2^3*Ycut^3 - 15*Ycut^4 + 
       45*m2*Ycut^4 - 50*m2^2*Ycut^4 + 10*m2^3*Ycut^4 + 46*Ycut^5 - 
       90*m2*Ycut^5 + 40*m2^2*Ycut^5 - 47*Ycut^6 + 45*m2*Ycut^6 + 16*Ycut^7 - 
       20*api*X1El[1, c[SL]*c[T], Ycut, m2, mu2hat] + 
       40*api*Ycut*X1El[1, c[SL]*c[T], Ycut, m2, mu2hat] - 
       20*api*Ycut^2*X1El[1, c[SL]*c[T], Ycut, m2, mu2hat])/
      (20*(-1 + Ycut)^2))) + c[T]^2*(-12*m2^2*(7 + m2)*Log[m2] + 
   12*m2^2*(7 + m2)*Log[1 - Ycut] + (19 - 195*m2 - 200*m2^2 + 440*m2^3 - 
     75*m2^4 + 11*m2^5 - 38*Ycut + 390*m2*Ycut + 820*m2^2*Ycut - 
     820*m2^3*Ycut + 150*m2^4*Ycut - 22*m2^5*Ycut + 19*Ycut^2 - 
     195*m2*Ycut^2 - 830*m2^2*Ycut^2 + 350*m2^3*Ycut^2 - 75*m2^4*Ycut^2 + 
     11*m2^5*Ycut^2 + 140*m2^2*Ycut^3 + 20*m2^3*Ycut^3 - 75*Ycut^4 + 
     135*m2*Ycut^4 - 10*m2^2*Ycut^4 - 10*m2^3*Ycut^4 + 206*Ycut^5 - 
     270*m2*Ycut^5 + 80*m2^2*Ycut^5 - 187*Ycut^6 + 135*m2*Ycut^6 + 
     56*Ycut^7 + 5*api*X1El[1, c[T]^2, Ycut, m2, mu2hat] - 
     10*api*Ycut*X1El[1, c[T]^2, Ycut, m2, mu2hat] + 
     5*api*Ycut^2*X1El[1, c[T]^2, Ycut, m2, mu2hat])/(5*(-1 + Ycut)^2)) + 
 c[VL]^2*(-3*m2^2*(3 + m2)*Log[m2] + 3*m2^2*(3 + m2)*Log[1 - Ycut] + 
   (7 - 75*m2 - 120*m2^2 + 200*m2^3 - 15*m2^4 + 3*m2^5 - 14*Ycut + 
     150*m2*Ycut + 420*m2^2*Ycut - 340*m2^3*Ycut + 30*m2^4*Ycut - 
     6*m2^5*Ycut + 7*Ycut^2 - 75*m2*Ycut^2 - 390*m2^2*Ycut^2 + 
     110*m2^3*Ycut^2 - 15*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 60*m2^2*Ycut^3 + 
     20*m2^3*Ycut^3 - 15*Ycut^4 + 15*m2*Ycut^4 + 30*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 38*Ycut^5 - 30*m2*Ycut^5 - 31*Ycut^6 + 15*m2*Ycut^6 + 
     8*Ycut^7 + 20*api*X1El[1, c[VL]^2, Ycut, m2, mu2hat] - 
     40*api*Ycut*X1El[1, c[VL]^2, Ycut, m2, mu2hat] + 
     20*api*Ycut^2*X1El[1, c[VL]^2, Ycut, m2, mu2hat])/(20*(-1 + Ycut)^2)) + 
 c[VR]*(-6*m2^(3/2)*(2 + 3*m2)*Log[m2] + 6*m2^(3/2)*(2 + 3*m2)*
    Log[1 - Ycut] - (-3*Sqrt[m2] - 44*m2^(3/2) + 36*m2^(5/2) + 12*m2^(7/2) - 
     m2^(9/2) + 3*Sqrt[m2]*Ycut + 68*m2^(3/2)*Ycut - 12*m2^(7/2)*Ycut + 
     m2^(9/2)*Ycut - 12*m2^(3/2)*Ycut^2 - 18*m2^(5/2)*Ycut^2 - 
     4*m2^(3/2)*Ycut^3 - 6*m2^(5/2)*Ycut^3 + 3*Sqrt[m2]*Ycut^4 - 
     8*m2^(3/2)*Ycut^4 - 3*Sqrt[m2]*Ycut^5 + 
     2*api*X1El[1, SM*c[VR], Ycut, m2, mu2hat] - 
     2*api*Ycut*X1El[1, SM*c[VR], Ycut, m2, mu2hat])/(2*(-1 + Ycut))) + 
 c[VL]*(-6*m2^2*(3 + m2)*Log[m2] + 6*m2^2*(3 + m2)*Log[1 - Ycut] + 
   (7 - 75*m2 - 120*m2^2 + 200*m2^3 - 15*m2^4 + 3*m2^5 - 14*Ycut + 
     150*m2*Ycut + 420*m2^2*Ycut - 340*m2^3*Ycut + 30*m2^4*Ycut - 
     6*m2^5*Ycut + 7*Ycut^2 - 75*m2*Ycut^2 - 390*m2^2*Ycut^2 + 
     110*m2^3*Ycut^2 - 15*m2^4*Ycut^2 + 3*m2^5*Ycut^2 + 60*m2^2*Ycut^3 + 
     20*m2^3*Ycut^3 - 15*Ycut^4 + 15*m2*Ycut^4 + 30*m2^2*Ycut^4 - 
     10*m2^3*Ycut^4 + 38*Ycut^5 - 30*m2*Ycut^5 - 31*Ycut^6 + 15*m2*Ycut^6 + 
     8*Ycut^7 + 10*api*X1El[1, SM*c[VL], Ycut, m2, mu2hat] - 
     20*api*Ycut*X1El[1, SM*c[VL], Ycut, m2, mu2hat] + 
     10*api*Ycut^2*X1El[1, SM*c[VL], Ycut, m2, mu2hat])/(10*(-1 + Ycut)^2) + 
   c[VR]*(-6*m2^(3/2)*(2 + 3*m2)*Log[m2] + 6*m2^(3/2)*(2 + 3*m2)*
      Log[1 - Ycut] - (-3*Sqrt[m2] - 44*m2^(3/2) + 36*m2^(5/2) + 
       12*m2^(7/2) - m2^(9/2) + 3*Sqrt[m2]*Ycut + 68*m2^(3/2)*Ycut - 
       12*m2^(7/2)*Ycut + m2^(9/2)*Ycut - 12*m2^(3/2)*Ycut^2 - 
       18*m2^(5/2)*Ycut^2 - 4*m2^(3/2)*Ycut^3 - 6*m2^(5/2)*Ycut^3 + 
       3*Sqrt[m2]*Ycut^4 - 8*m2^(3/2)*Ycut^4 - 3*Sqrt[m2]*Ycut^5 + 
       2*api*X1El[1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       2*api*Ycut*X1El[1, c[VL]*c[VR], Ycut, m2, mu2hat])/(2*(-1 + Ycut)))) + 
 c[VR]^2*(-6*m2^2*Log[m2] + 6*m2^2*Log[1 - Ycut] + 
   (3 - 30*m2 - 20*m2^2 + 60*m2^3 - 15*m2^4 + 2*m2^5 + 60*m2^2*Ycut + 
     30*m2^2*Ycut^2 + 20*m2^2*Ycut^3 - 15*Ycut^4 + 30*m2*Ycut^4 + 12*Ycut^5 + 
     10*api*X1El[1, c[VR]^2, Ycut, m2, mu2hat])/10)
