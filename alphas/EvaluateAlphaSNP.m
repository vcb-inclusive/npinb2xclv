(* ::Package:: *)

BeginPackage["EvaluateAlphaSNP`"];


X1Q2::usage="X1Q2[n,cNP,q2hatcut,m2,mu2hat] calculates the NLO correction to the q^2 moment in the on-shell scheme. 

n: order of the moment. Currently 0<= n <=3 implemented.

cNP: product of NP coefficients appearing in the decay rate. 
Possible options are cNP = {SM^2,SM c[VL],SM c[VR],c[SL]c[SL],c[SR]c[SR],c[VL]^2,c[SL]c[SR],c[VR]^2,c[VL]c[VR],c[SL]c[T],c[T]^2}.
SM^2 stands for the SM prediction.

q2hatcut: value of the lower cut in q^2 normalized to bottom mass.

m2: the ratio mc^2/mb^2.

mu2hat: value of the renormalization scale of the Wilson coefficients in the MS scheme, normalized to the bottom mass.	 
"


X1El::usage="X1El[n,cNP,Ycut,m2,mu2hat] calculates the NLO correction to the moments of the electron energy in the on-shell scheme.

n: order of the moment. Currently 0<= n <=3 implemented.

cNP: product of NP coefficients appearing in the decay rate. 
Possible options are cNP = {SM^2,SM c[VL],SM c[VR],c[SL]c[SL],c[SR]c[SR],c[VL]^2,c[SL]c[SR],c[VR]^2,c[VL]c[VR],c[SL]c[T],c[T]^2}.
SM^2 stands for the SM prediction.

Ycut: value of the lower cut in the elctron energy normalized to bottom mass: Ycut = 2 Ecut/mb;

m2: the ratio mc^2/mb^2.

mu2hat: value of the renormalization scale of the Wilson coefficients in the MS scheme, normalized to the bottom mass.	 
"


X1mix::usage="X1El[nq2,nq0,cNP,Ycut,m2,mu2hat] calculates the NLO correction to the mixed moments.

nq2,nq0: order of the mix moment q2^nq2 q0^nq0. Currently 0<= nq2+nq0 <=3 implemented.

cNP: product of NP coefficients appearing in the decay rate. 
Possible options are cNP = {SM^2,SM c[VL],SM c[VR],c[SL]c[SL],c[SR]c[SR],c[VL]^2,c[SL]c[SR],c[VR]^2,c[VL]c[VR],c[SL]c[T],c[T]^2}.
SM^2 stands for the SM prediction.

Ycut: value of the lower cut in the elctron energy normalized to bottom mass: Ycut = 2 Ecut/mb;

m2: the ratio mc^2/mb^2.

mu2hat: value of the renormalization scale of the Wilson coefficients in the MS scheme, normalized to the bottom mass.	 
"


X1AFBForward::usage="X1AFBForward[cNP,q2hatcut,m2,mu2hat] calculates the NLO correction to the decay rate in forward direction in the on-shell scheme. 

cNP: product of NP coefficients appearing in the decay rate. 
Possible options are cNP = {SM^2,SM c[VL],SM c[VR],c[SL]c[SL],c[SR]c[SR],c[VL]^2,c[SL]c[SR],c[VR]^2,c[VL]c[VR],c[SL]c[T],c[T]^2}.
SM^2 stands for the SM prediction.

q2hatcut: value of the lower cut in q^2 normalized to bottom mass.

m2: the ratio mc^2/mb^2.

mu2hat: value of the renormalization scale of the Wilson coefficients in the MS scheme, normalized to the bottom mass.	 
"


X1AFBBackward::usage="X1AFBBackward[cNP,q2hatcut,m2,mu2hat] calculates the NLO correction to the decay rate in backward direction in the on-shell scheme. 

cNP: product of NP coefficients appearing in the decay rate. 
Possible options are cNP = {SM^2,SM c[VL],SM c[VR],c[SL]c[SL],c[SR]c[SR],c[VL]^2,c[SL]c[SR],c[VR]^2,c[VL]c[VR],c[SL]c[T],c[T]^2}.
SM^2 stands for the SM prediction.

q2hatcut: value of the lower cut in q^2 normalized to bottom mass.

m2: the ratio mc^2/mb^2.

mu2hat: value of the renormalization scale of the Wilson coefficients in the MS scheme, normalized to the bottom mass.	 
"


c;
SM;
SL;
SR;
VL;
VR;
T;


$EvaluateAlphaSNPDirectory=DirectoryName[$InputFileName];


Print["New physics contributions to moments of inclusive b\[RightArrow]c semileptonic decays"];
Print["by M. Fael, M. Rahimi, K. K. Vos"];
Print["hep-ph/2208.04282"];
Print["EvaluateAlphaSNP allows to numerically evaluate the functions"];
Print["X1Q2, X1El, X1mix, X1AFBForward, X1AFBBackward appearing in the"];
Print["NLO prediction of the moments of inclusive B decays."]


Begin["`Private`"];


rewritecNP:={c[SLSL]->c[SL]c[SL] ,
c[SLSR]-> c[SL]c[SR],
c[SM]->(SM+c[VL])^2,
c[SRSR]->c[SR]c[SR],
c[SL T]->c[SL]c[T],
c[T^2]->c[T]^2,
c[VLVR]->(SM+c[VL])c[VR],
c[VRVR]->c[VR]^2};


IntegrandDeltaCUT=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandDeltaCUT.m"}]]/.rewritecNP;
IntegrandDeltaFULL=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandDeltaFULL.m"}]]/.rewritecNP;
IntegrandPlusCUT=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandPlusCUT.m"}]]/.rewritecNP;
IntegrandPlusFULL=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandPlusFULL.m"}]]/.rewritecNP;
IntegrandDeltaForward=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandDeltaForward.m"}]]/.rewritecNP;
IntegrandDeltaBackward=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandDeltaBackward.m"}]]/.rewritecNP;
IntegrandPlusForward=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandPlusForward.m"}]]/.rewritecNP;
IntegrandPlusBackward=Get[FileNameJoin[{$EvaluateAlphaSNPDirectory,"IntegrandPlusBackward.m"}]]/.rewritecNP;


TakePlus[expr_]:=(expr-(expr/.u->0))/u;


SetOptions[NIntegrate,MinRecursion->4,MaxRecursion->20];


X1Q2[n_?NumericQ,cNP_,
     q2hatcut_?NumericQ,
     r2_?NumericQ,
     mu2hat_?NumericQ]:=Block[{integrand1full,integrand2full,res1full,res2full,
     mu2=SetPrecision[mu2hat,Infinity],
     mb=1,
     m2=SetPrecision[r2,Infinity],
     q2max=(1-Sqrt[m2])^2,
     CF=4/3,
     umax=(1-Sqrt[q2])^2-m2,
     Q2cut=SetPrecision[q2hatcut,Infinity]},
	integrand1full=Coefficient[IntegrandDeltaFULL[[1]],cNP];
	integrand2full=Coefficient[IntegrandPlusFULL[[1]],cNP];
    res1full=If[ integrand2full===0,0,NIntegrate[q2 ^n integrand1full,{q2,Q2cut,q2max}]];
	res2full=If[ integrand2full===0,0,NIntegrate[q2^n TakePlus[integrand2full],{q2,Q2cut,q2max},{u,0,umax}]];
	Chop[Plus@@{res1full,res2full}]
]


X1El[n_?NumericQ,cNP_,Ycut_?NumericQ,r2_?NumericQ,mu2hat_?NumericQ]:=Block[{integrand1cut,integrand1full,integrand2cut,integrand2full,
	res1cut,res1full,res2cut1,res2cut2,res2full1,res2full2,
	mu2=SetPrecision[mu2hat,Infinity],
	mb=1,
	m2=SetPrecision[r2,Infinity],
	y=SetPrecision[Ycut,Infinity],
	A0=y (1-m2-y)/(1-y),
	gamma=1+q2(1-1/y)-m2-y,
	q2max=(1-Sqrt[m2])^2,CF=4/3,
	umax=(1-Sqrt[q2])^2-m2},
	integrand1cut=Coefficient[IntegrandDeltaCUT[[n+1]],cNP];
	integrand1full=Coefficient[IntegrandDeltaFULL[[n+1]],cNP];
	integrand2cut=Coefficient[IntegrandPlusCUT[[n+1]],cNP];
	integrand2full=Coefficient[IntegrandPlusFULL[[n+1]],cNP];
	res1cut=NIntegrate[integrand1cut,{q2,0,A0}];
	res1full=If[ integrand2full===0,0,NIntegrate[integrand1full,{q2,A0,q2max}]];
	res2cut1=-((integrand2cut/.u->0)/u)//NIntegrate[#,{q2,0,A0},{u,gamma,umax}]&;
	res2cut2=   NIntegrate[TakePlus[integrand2cut],{q2,0,A0},{u,0,gamma}];
	res2full1=If[ integrand2full===0,0,NIntegrate[integrand2full/u,{q2,y^2,A0},{u,gamma,umax}]];
	res2full2=If[ integrand2full===0,0,NIntegrate[TakePlus[integrand2full],{q2,A0,q2max},{u,0,umax}]];
	Chop[Plus@@{res1cut,res1full,res2cut2,res2cut1, res2full1,res2full2}]
]


X1mix[n_?NumericQ,n2_?NumericQ,cNP_,Ycut_?NumericQ,r2_?NumericQ,mu2hat_?NumericQ]:=Block[{integrand1cut,integrand1full,integrand2cut,integrand2full,
	res1cut,res1full,res2cut1,res2cut2,res2full1,res2full2,weight,
	mu2=SetPrecision[mu2hat,Infinity],
	mb=1,
	m2=SetPrecision[r2,Infinity],
	y=SetPrecision[Ycut,Infinity],
	A0=y (1-m2-y)/(1-y),
	gamma=1+q2(1-1/y)-m2-y,
	q2max=(1-Sqrt[m2])^2,CF=4/3,
	umax=(1-Sqrt[q2])^2-m2,
	q0= (1+q2-m2-u)/2},
	weight= q2^n q0^n2;
	integrand1cut=(weight /.u->0)Coefficient[IntegrandDeltaCUT[[1]],cNP];
	integrand1full=(weight /.u->0)Coefficient[IntegrandDeltaFULL[[1]],cNP];
	integrand2cut=weight Coefficient[IntegrandPlusCUT[[1]],cNP];
	integrand2full=weight Coefficient[IntegrandPlusFULL[[1]],cNP];
	res1cut=NIntegrate[integrand1cut,{q2,0,A0}];
	res1full=If[ integrand2full===0,0,NIntegrate[integrand1full,{q2,A0,q2max}]];
	res2cut1=-((  integrand2cut/.u->0)/u)//NIntegrate[#,{q2,0,A0},{u,gamma,umax}]&;
	res2cut2=   NIntegrate[TakePlus[ integrand2cut],{q2,0,A0},{u,0,gamma}];
	res2full1=If[ integrand2full===0,0,NIntegrate[integrand2full/u,{q2,y^2,A0},{u,gamma,umax}]];
	res2full2=If[ integrand2full===0,0,NIntegrate[TakePlus[integrand2full],{q2,A0,q2max},{u,0,umax}]];
	Chop[Plus@@{res1cut,res1full,res2cut2,res2cut1, res2full1,res2full2}]
]


X1AFBForward[cNP_,q2hatcut_?NumericQ,r2_?NumericQ,mu2hat_?NumericQ]:=Block[{integrand1full,integrand2full,res1full,res2full,
	mu2=SetPrecision[mu2hat,Infinity],
	mb=1,
	m2=SetPrecision[r2,Infinity],
	q2max=(1-Sqrt[m2])^2,CF=4/3,
	umax=(1-Sqrt[q2])^2-m2,
	Q2cut=SetPrecision[q2hatcut,Infinity]},
	integrand1full=Coefficient[IntegrandDeltaForward,cNP];
	integrand2full=Coefficient[IntegrandPlusForward ,cNP];
	res1full=If[ integrand1full===0,0,NIntegrate[ integrand1full,{q2,Q2cut,q2max}]];
	res2full=If[ integrand2full===0,0,NIntegrate[ TakePlus[integrand2full],{q2,Q2cut,q2max},{u,0,umax},Exclusions->{u==0,q2==q2max},WorkingPrecision->20,PrecisionGoal->8]];
	Chop[Plus@@{res1full,res2full}]
]


X1AFBBackward[cNP_,q2hatcut_?NumericQ,r2_?NumericQ,mu2hat_?NumericQ]:=Block[{integrand1full,integrand2full,res1full,res2full,
	mu2=SetPrecision[mu2hat,Infinity],
	mb=1,
	m2=SetPrecision[r2,Infinity],
	q2max=(1-Sqrt[m2])^2,CF=4/3,
	umax=(1-Sqrt[q2])^2-m2,
	Q2cut=SetPrecision[q2hatcut,Infinity]},
	integrand1full=Coefficient[IntegrandDeltaBackward,cNP];
	integrand2full=Coefficient[IntegrandPlusBackward ,cNP];
	res1full=If[ integrand1full===0,0,NIntegrate[ integrand1full,{q2,Q2cut,q2max}]];
	res2full=If[ integrand2full===0,0,NIntegrate[ TakePlus[integrand2full],{q2,Q2cut,q2max},{u,0,umax},Exclusions->{u==0,q2==q2max},WorkingPrecision->20,PrecisionGoal->8]];
	Chop[Plus@@{res1full,res2full}]
]


End[];


EndPackage[];
