# New physics contributions to moments of inclusive b→c semileptonic decays
by M. Fael, M. Rahimi, K. K. Vos,
hep-ph/2208.04282

**NEW 2024**: you can find an implementation of the NP contributions also in Kolya [here](https://gitlab.com/vcb-inclusive/kolya)

## Content
This repository contains the analytic expressions for the evaluation of the various kinematic moments in the presence of NP operators. All expressions are given in the on-shell scheme. 
We include power corrections up to $1/m_b^3$ and NLO corrections at LO in $1/m_b$.  Content of the directories:
- `Eemoments_Eecut`: the directory contains expressions for the electron energy moments up to the third moment, with a lower cut on the electron energy. 
- `MXmoments_Eecut`: the directory contains expressions for the hadronic invariant mass moments up to the third moment, with a lower cut on the electron energy. 
- `Q2moments_q2cut`: the directory contains expressions for the dilepton invariant mass ($`q^2`$) moments up to the third moment, with a lower cut on $`q^2`$.
- `AFB`: the directory contains expressions for the total rate, with emission in the forward and backward direction (in the rest frame of the two leptons). They corresponds to the two terms at numerator in Eq. (13).
- `alphas`: expressions of the NLO corrections. The NLO corrections can be evaluated with the Mathematica package `EvaluateAlphaSNP.m`. 

## Notation
- `mb`: the mass of the bottom quark in the on-shell scheme.
- `mc`: the mass of the charm quark in the on-shell scheme.
- `m2 = mc^2/mb^2`
- `api` $`=\alpha_s/\pi`$.
- `mupi,muG,rhoD,rhoSL`: the HQE parameters in the "perp" basis.
- `c[SL], c[SR], c[T], c[VL], c[VR]`: the Wilson coefficients of the effective Hamiltonian.
- `Q2hatcut`: the value of the lower q2 cut in unit of bottom mass, i.e. `Q2hatcut = Q2cut/mb^2`.
- `Eecut`: the value of the lower cut on the charged lepton energy.
- `Ycut = 2 Eecut/mb`: the normalized value of the lower cut on the charged lepton energy.
- `MB`: mass of the B meson
- `MBhat = MB/mb`: mass of the B meson normalized.

## QCD NLO Corrections

The files contained in the directories `AFB`,`Eemoments_Eecut`, `MXmoments_Eecut` and `Q2moments_q2cut` give analytic expressions for the moments at LO. However the coefficients of order $`\alpha_s`$ is represented in this files by the functions:
```
X1AFBForward[cNP,q2cuthat,m2,mu2hat]
X1AFBBackward[cNP,q2cuthat,m2,mu2hat]
X1mix[nq2,nq0,cNP,Ycut,m2,mu2hat]
X1El[n,cNP,Ycut,m2,mu2hat]
X1Q2[n,cNP,q2cuthat,m2,mu2hat]
```
The numerical evaluation of the NLO corrections requires non-trivial numerical integration over the phase-space. The package `EvaluateAlphaSNP.m` provides the necessary subroutines for the integration. One needs to load the package in Mathematica:
```
In[]:=<< "alphas/EvaluateAlphaSNP.m"
```
Afterwards, calling these functions with numerical arguments will execute the numerical integration.
For example, the NLO correction the the first $`q^2`$ moment, with a cut $`q^2_\mathrm{cut}= 4 \, \mathrm{GeV}^2`$ is evaluated in the following way:
```
In[] := mb = 4.6; mc = 1.15; mus = mb; q2cut = 1; q2hatcut = q2cut/mb^2;
In[] := X1Q2[1, SM^2, q2hatcut, (mc/mb)^2, (mus/mb)^2]
Out[]:= -0.215785
```
For the syntax, please refer to Mathematica
```
?X1Q2
```
and the example notebook `example.nb`.


