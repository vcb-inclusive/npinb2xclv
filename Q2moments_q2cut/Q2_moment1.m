-18*m2^2*(1 + m2)*
  Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
       2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
     Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
 (mupi*(-1/20*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(3 - 42*m2 - 282*m2^2 - 42*m2^3 + 3*m2^4 + 3*Q2hatcut - 
        33*m2*Q2hatcut - 33*m2^2*Q2hatcut + 3*m2^3*Q2hatcut - 7*Q2hatcut^2 + 
        2*m2*Q2hatcut^2 - 7*m2^2*Q2hatcut^2 - 7*Q2hatcut^3 - 
        7*m2*Q2hatcut^3 + 8*Q2hatcut^4)) + 9*m2^2*(1 + m2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*(-1/20*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 14*m2 - 94*m2^2 - 14*m2^3 + m2^4 + Q2hatcut - 
          11*m2*Q2hatcut - 11*m2^2*Q2hatcut + m2^3*Q2hatcut + Q2hatcut^2 - 
          6*m2*Q2hatcut^2 + m2^2*Q2hatcut^2 - 9*Q2hatcut^3 - 
          9*m2*Q2hatcut^3 + 6*Q2hatcut^4)) + 3*m2^2*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*(-1/20*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 14*m2 - 94*m2^2 - 14*m2^3 + m2^4 + Q2hatcut - 
          11*m2*Q2hatcut - 11*m2^2*Q2hatcut + m2^3*Q2hatcut + Q2hatcut^2 - 
          6*m2*Q2hatcut^2 + m2^2*Q2hatcut^2 - 9*Q2hatcut^3 - 
          9*m2*Q2hatcut^3 + 6*Q2hatcut^4)) + 3*m2^2*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*(-1/20*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(3 - 42*m2 - 282*m2^2 - 42*m2^3 + 3*m2^4 + 
          3*Q2hatcut - 33*m2*Q2hatcut - 33*m2^2*Q2hatcut + 3*m2^3*Q2hatcut - 
          7*Q2hatcut^2 + 2*m2*Q2hatcut^2 - 7*m2^2*Q2hatcut^2 - 7*Q2hatcut^3 - 
          7*m2*Q2hatcut^3 + 8*Q2hatcut^4)) + 9*m2^2*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/20*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(3 - 42*m2 - 282*m2^2 - 42*m2^3 + 3*m2^4 + 
          3*Q2hatcut - 33*m2*Q2hatcut - 33*m2^2*Q2hatcut + 3*m2^3*Q2hatcut - 
          7*Q2hatcut^2 + 2*m2*Q2hatcut^2 - 7*m2^2*Q2hatcut^2 - 7*Q2hatcut^3 - 
          7*m2*Q2hatcut^3 + 8*Q2hatcut^4)) + 9*m2^2*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((-8*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 14*m2 - 94*m2^2 - 14*m2^3 + m2^4 + Q2hatcut - 
          11*m2*Q2hatcut - 11*m2^2*Q2hatcut + m2^3*Q2hatcut - 4*Q2hatcut^2 + 
          4*m2*Q2hatcut^2 - 4*m2^2*Q2hatcut^2 + Q2hatcut^3 + m2*Q2hatcut^3 + 
          Q2hatcut^4))/5 + 96*m2^2*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]*c[SR]*(-1/2*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)*(1 + 29*m2 + 29*m2^2 + m2^3 + Q2hatcut + 
           8*m2*Q2hatcut + m2^2*Q2hatcut + Q2hatcut^2 + m2*Q2hatcut^2 - 
           3*Q2hatcut^3))/Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2)/m2] - 6*m2^(3/2)*(1 + 3*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
         (1 + 29*m2 + 29*m2^2 + m2^3 + Q2hatcut + 8*m2*Q2hatcut + 
          m2^2*Q2hatcut + Q2hatcut^2 + m2*Q2hatcut^2 - 3*Q2hatcut^3))/
        Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
          m2] + 12*m2^(3/2)*(1 + 3*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(-1/10*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(3 - 42*m2 - 282*m2^2 - 42*m2^3 + 3*m2^4 + 
          3*Q2hatcut - 33*m2*Q2hatcut - 33*m2^2*Q2hatcut + 3*m2^3*Q2hatcut - 
          7*Q2hatcut^2 + 2*m2*Q2hatcut^2 - 7*m2^2*Q2hatcut^2 - 7*Q2hatcut^3 - 
          7*m2*Q2hatcut^3 + 8*Q2hatcut^4)) + 18*m2^2*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
           (1 + 29*m2 + 29*m2^2 + m2^3 + Q2hatcut + 8*m2*Q2hatcut + 
            m2^2*Q2hatcut + Q2hatcut^2 + m2*Q2hatcut^2 - 3*Q2hatcut^3))/
          Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
            m2] + 12*m2^(3/2)*(1 + 3*m2 + m2^2)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 15*Q2hatcut + 
        29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 21*Q2hatcut^2 + 
        38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 21*Q2hatcut^3 - 
        21*m2*Q2hatcut^3 + 24*Q2hatcut^4))/12 - m2*(4 - 3*m2 + 9*m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(5 + 102*m2 + 22*m2^2 - 10*m2^3 + m2^4 + 5*Q2hatcut + 
          21*m2*Q2hatcut - 7*m2^2*Q2hatcut + m2^3*Q2hatcut + 5*Q2hatcut^2 - 
          2*m2*Q2hatcut^2 + m2^2*Q2hatcut^2 + 3*Q2hatcut^3 - 
          9*m2*Q2hatcut^3 + 6*Q2hatcut^4))/4 - 3*m2*(-4 - 7*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(5 + 102*m2 + 22*m2^2 - 10*m2^3 + m2^4 + 5*Q2hatcut + 
          21*m2*Q2hatcut - 7*m2^2*Q2hatcut + m2^3*Q2hatcut + 5*Q2hatcut^2 - 
          2*m2*Q2hatcut^2 + m2^2*Q2hatcut^2 + 3*Q2hatcut^3 - 
          9*m2*Q2hatcut^3 + 6*Q2hatcut^4))/4 - 3*m2*(-4 - 7*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((8*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-9 - 70*m2 - 22*m2^2 - 22*m2^3 + 3*m2^4 - 
          9*Q2hatcut - m2*Q2hatcut - 13*m2^2*Q2hatcut + 3*m2^3*Q2hatcut + 
          8*m2*Q2hatcut^2 - 12*m2^2*Q2hatcut^2 + 3*Q2hatcut^3 + 
          3*m2*Q2hatcut^3 + 3*Q2hatcut^4))/3 - 32*m2*(4 + 3*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 
          15*Q2hatcut + 29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
          21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4))/12 - 
       m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 
          15*Q2hatcut + 29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
          21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4))/12 - 
       m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
         (-85 - 217*m2 - m2^2 + 3*m2^3 - 37*Q2hatcut - 16*m2*Q2hatcut + 
          3*m2^2*Q2hatcut - 13*Q2hatcut^2 + 3*m2*Q2hatcut^2 - 9*Q2hatcut^3))/
        (2*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2]) + 6*Sqrt[m2]*(-2 - 15*m2 - 9*m2^2 + m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(-1/3*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
          (59 - 137*m2 + 7*m2^2 + 11*m2^3 + 11*Q2hatcut - 32*m2*Q2hatcut + 
           11*m2^2*Q2hatcut - Q2hatcut^2 - m2*Q2hatcut^2 - 21*Q2hatcut^3))/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
         (-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 15*Q2hatcut + 
          29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
          21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4))/6 - 
       2*m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])] + 
       c[VR]*(-1/3*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
             Q2hatcut^2)*(59 - 137*m2 + 7*m2^2 + 11*m2^3 + 11*Q2hatcut - 
             32*m2*Q2hatcut + 11*m2^2*Q2hatcut - Q2hatcut^2 - m2*Q2hatcut^2 - 
             21*Q2hatcut^3))/Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2)/m2] - 4*Sqrt[m2]*
          (2 - m2 - 9*m2^2 + 3*m2^3)*Log[(1 + m2 - Q2hatcut - 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
            (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])]))))/mb^2 + 
 (rhoD*((729 - 2560*m2 + 3127*m2^2 - 1440*m2^3 + 103*m2^4 + 32*m2^5 + 
       9*m2^6 - 1305*Q2hatcut + 181*m2*Q2hatcut + 2564*m2^2*Q2hatcut - 
       892*m2^3*Q2hatcut - 59*m2^4*Q2hatcut - 9*m2^5*Q2hatcut + 
       354*Q2hatcut^2 - 666*m2*Q2hatcut^2 + 296*m2^2*Q2hatcut^2 + 
       6*m2^3*Q2hatcut^2 - 30*m2^4*Q2hatcut^2 + 126*Q2hatcut^3 - 
       272*m2*Q2hatcut^3 + 112*m2^2*Q2hatcut^3 + 30*m2^3*Q2hatcut^3 - 
       3*Q2hatcut^4 - 166*m2*Q2hatcut^4 + 45*m2^2*Q2hatcut^4 + 
       75*Q2hatcut^5 - 69*m2*Q2hatcut^5 + 24*Q2hatcut^6)/
      (36*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
     ((24 + 8*m2 - 69*m2^2 + 27*m2^3)*
       Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
            2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
          Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3 + 
     c[SL]*c[SR]*(-1/6*(-551 + 219*m2 + 1268*m2^2 - 988*m2^3 + 51*m2^4 + 
          m2^5 + 887*Q2hatcut + 3282*m2*Q2hatcut + 1478*m2^2*Q2hatcut - 
          126*m2^3*Q2hatcut - m2^4*Q2hatcut - 168*Q2hatcut^2 - 
          326*m2*Q2hatcut^2 + 34*m2^2*Q2hatcut^2 - 76*Q2hatcut^3 + 
          34*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 + 7*Q2hatcut^4 + 
          7*m2*Q2hatcut^4 - 3*Q2hatcut^5)/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 2*Sqrt[m2]*(-14 - 77*m2 - 27*m2^2 + 3*m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(-1/3*(385 - 285*m2 - 676*m2^2 + 668*m2^3 - 93*m2^4 + m2^5 - 
          625*Q2hatcut - 2046*m2*Q2hatcut - 610*m2^2*Q2hatcut + 
          162*m2^3*Q2hatcut - m2^4*Q2hatcut + 132*Q2hatcut^2 + 
          142*m2*Q2hatcut^2 - 2*m2^2*Q2hatcut^2 - 12*m2^3*Q2hatcut^2 + 
          32*Q2hatcut^3 - 38*m2*Q2hatcut^3 + 32*m2^2*Q2hatcut^3 - 
          29*Q2hatcut^4 - 29*m2*Q2hatcut^4 + 9*Q2hatcut^5)/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] + 4*Sqrt[m2]*(-10 - 49*m2 - 9*m2^2 + 3*m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]^2*((133 - 308*m2 + 239*m2^2 - 80*m2^3 + 11*m2^4 + 4*m2^5 + m2^6 - 
         229*Q2hatcut - 283*m2*Q2hatcut + 140*m2^2*Q2hatcut - 
         100*m2^3*Q2hatcut - 7*m2^4*Q2hatcut - m2^5*Q2hatcut + 
         48*Q2hatcut^2 - 26*m2*Q2hatcut^2 + 20*m2^2*Q2hatcut^2 - 
         2*m2^3*Q2hatcut^2 + 38*Q2hatcut^3 - 48*m2*Q2hatcut^3 + 
         24*m2^2*Q2hatcut^3 - 10*m2^3*Q2hatcut^3 - 11*Q2hatcut^4 - 
         34*m2*Q2hatcut^4 + 25*m2^2*Q2hatcut^4 + 15*Q2hatcut^5 - 
         21*m2*Q2hatcut^5 + 6*Q2hatcut^6)/
        (12*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + (4 + 8*m2 - 5*m2^2 + 3*m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*((133 - 308*m2 + 239*m2^2 - 80*m2^3 + 11*m2^4 + 4*m2^5 + m2^6 - 
         229*Q2hatcut - 283*m2*Q2hatcut + 140*m2^2*Q2hatcut - 
         100*m2^3*Q2hatcut - 7*m2^4*Q2hatcut - m2^5*Q2hatcut + 
         48*Q2hatcut^2 - 26*m2*Q2hatcut^2 + 20*m2^2*Q2hatcut^2 - 
         2*m2^3*Q2hatcut^2 + 38*Q2hatcut^3 - 48*m2*Q2hatcut^3 + 
         24*m2^2*Q2hatcut^3 - 10*m2^3*Q2hatcut^3 - 11*Q2hatcut^4 - 
         34*m2*Q2hatcut^4 + 25*m2^2*Q2hatcut^4 + 15*Q2hatcut^5 - 
         21*m2*Q2hatcut^5 + 6*Q2hatcut^6)/
        (12*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + (4 + 8*m2 - 5*m2^2 + 3*m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((8*(165 - 818*m2 + 1205*m2^2 - 600*m2^3 + 35*m2^4 + 10*m2^5 + 
          3*m2^6 - 309*Q2hatcut + 515*m2*Q2hatcut + 1072*m2^2*Q2hatcut - 
          296*m2^3*Q2hatcut - 19*m2^4*Q2hatcut - 3*m2^5*Q2hatcut + 
          105*Q2hatcut^2 - 294*m2*Q2hatcut^2 + 118*m2^2*Q2hatcut^2 + 
          6*m2^3*Q2hatcut^2 - 15*m2^4*Q2hatcut^2 + 6*Q2hatcut^3 - 
          64*m2*Q2hatcut^3 + 20*m2^2*Q2hatcut^3 + 30*m2^3*Q2hatcut^3 + 
          15*Q2hatcut^4 - 32*m2*Q2hatcut^4 - 15*m2^2*Q2hatcut^4 + 
          15*Q2hatcut^5 - 3*m2*Q2hatcut^5 + 3*Q2hatcut^6))/
        (9*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
       (32*(6 - 8*m2 - 27*m2^2 + 9*m2^3)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3) + 
     c[VL]^2*((729 - 2560*m2 + 3127*m2^2 - 1440*m2^3 + 103*m2^4 + 32*m2^5 + 
         9*m2^6 - 1305*Q2hatcut + 181*m2*Q2hatcut + 2564*m2^2*Q2hatcut - 
         892*m2^3*Q2hatcut - 59*m2^4*Q2hatcut - 9*m2^5*Q2hatcut + 
         354*Q2hatcut^2 - 666*m2*Q2hatcut^2 + 296*m2^2*Q2hatcut^2 + 
         6*m2^3*Q2hatcut^2 - 30*m2^4*Q2hatcut^2 + 126*Q2hatcut^3 - 
         272*m2*Q2hatcut^3 + 112*m2^2*Q2hatcut^3 + 30*m2^3*Q2hatcut^3 - 
         3*Q2hatcut^4 - 166*m2*Q2hatcut^4 + 45*m2^2*Q2hatcut^4 + 
         75*Q2hatcut^5 - 69*m2*Q2hatcut^5 + 24*Q2hatcut^6)/
        (36*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + ((24 + 8*m2 - 69*m2^2 + 27*m2^3)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3) + 
     c[VR]^2*((729 - 2560*m2 + 3127*m2^2 - 1440*m2^3 + 103*m2^4 + 32*m2^5 + 
         9*m2^6 - 1305*Q2hatcut + 181*m2*Q2hatcut + 2564*m2^2*Q2hatcut - 
         892*m2^3*Q2hatcut - 59*m2^4*Q2hatcut - 9*m2^5*Q2hatcut + 
         354*Q2hatcut^2 - 666*m2*Q2hatcut^2 + 296*m2^2*Q2hatcut^2 + 
         6*m2^3*Q2hatcut^2 - 30*m2^4*Q2hatcut^2 + 126*Q2hatcut^3 - 
         272*m2*Q2hatcut^3 + 112*m2^2*Q2hatcut^3 + 30*m2^3*Q2hatcut^3 - 
         3*Q2hatcut^4 - 166*m2*Q2hatcut^4 + 45*m2^2*Q2hatcut^4 + 
         75*Q2hatcut^5 - 69*m2*Q2hatcut^5 + 24*Q2hatcut^6)/
        (36*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + ((24 + 8*m2 - 69*m2^2 + 27*m2^3)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3) + 
     c[VL]*((729 - 2560*m2 + 3127*m2^2 - 1440*m2^3 + 103*m2^4 + 32*m2^5 + 
         9*m2^6 - 1305*Q2hatcut + 181*m2*Q2hatcut + 2564*m2^2*Q2hatcut - 
         892*m2^3*Q2hatcut - 59*m2^4*Q2hatcut - 9*m2^5*Q2hatcut + 
         354*Q2hatcut^2 - 666*m2*Q2hatcut^2 + 296*m2^2*Q2hatcut^2 + 
         6*m2^3*Q2hatcut^2 - 30*m2^4*Q2hatcut^2 + 126*Q2hatcut^3 - 
         272*m2*Q2hatcut^3 + 112*m2^2*Q2hatcut^3 + 30*m2^3*Q2hatcut^3 - 
         3*Q2hatcut^4 - 166*m2*Q2hatcut^4 + 45*m2^2*Q2hatcut^4 + 
         75*Q2hatcut^5 - 69*m2*Q2hatcut^5 + 24*Q2hatcut^6)/
        (18*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + (2*(24 + 8*m2 - 69*m2^2 + 27*m2^3)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3 + 
       c[VR]*(-1/3*(385 - 285*m2 - 676*m2^2 + 668*m2^3 - 93*m2^4 + m2^5 - 
            625*Q2hatcut - 2046*m2*Q2hatcut - 610*m2^2*Q2hatcut + 
            162*m2^3*Q2hatcut - m2^4*Q2hatcut + 132*Q2hatcut^2 + 
            142*m2*Q2hatcut^2 - 2*m2^2*Q2hatcut^2 - 12*m2^3*Q2hatcut^2 + 
            32*Q2hatcut^3 - 38*m2*Q2hatcut^3 + 32*m2^2*Q2hatcut^3 - 
            29*Q2hatcut^4 - 29*m2*Q2hatcut^4 + 9*Q2hatcut^5)/
           Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
             m2] + 4*Sqrt[m2]*(-10 - 49*m2 - 9*m2^2 + 3*m2^3)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   rhoLS*(-1/12*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 
        15*Q2hatcut + 29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
        21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
        21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4)) + 
     m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
        (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(5 + 102*m2 + 22*m2^2 - 10*m2^3 + m2^4 + 5*Q2hatcut + 
          21*m2*Q2hatcut - 7*m2^2*Q2hatcut + m2^3*Q2hatcut + 5*Q2hatcut^2 - 
          2*m2*Q2hatcut^2 + m2^2*Q2hatcut^2 + 3*Q2hatcut^3 - 
          9*m2*Q2hatcut^3 + 6*Q2hatcut^4)) + 3*m2*(-4 - 7*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(5 + 102*m2 + 22*m2^2 - 10*m2^3 + m2^4 + 5*Q2hatcut + 
          21*m2*Q2hatcut - 7*m2^2*Q2hatcut + m2^3*Q2hatcut + 5*Q2hatcut^2 - 
          2*m2*Q2hatcut^2 + m2^2*Q2hatcut^2 + 3*Q2hatcut^3 - 
          9*m2*Q2hatcut^3 + 6*Q2hatcut^4)) + 3*m2*(-4 - 7*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((-8*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-9 - 70*m2 - 22*m2^2 - 22*m2^3 + 3*m2^4 - 
          9*Q2hatcut - m2*Q2hatcut - 13*m2^2*Q2hatcut + 3*m2^3*Q2hatcut + 
          8*m2*Q2hatcut^2 - 12*m2^2*Q2hatcut^2 + 3*Q2hatcut^3 + 
          3*m2*Q2hatcut^3 + 3*Q2hatcut^4))/3 + 32*m2*(4 + 3*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*(-1/12*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 
          15*Q2hatcut + 29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
          21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4)) + 
       m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/12*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 
          15*Q2hatcut + 29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
          21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4)) + 
       m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (-1/2*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
          (-85 - 217*m2 - m2^2 + 3*m2^3 - 37*Q2hatcut - 16*m2*Q2hatcut + 
           3*m2^2*Q2hatcut - 13*Q2hatcut^2 + 3*m2*Q2hatcut^2 - 9*Q2hatcut^3))/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 6*Sqrt[m2]*(-2 - 15*m2 - 9*m2^2 + m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
         (59 - 137*m2 + 7*m2^2 + 11*m2^3 + 11*Q2hatcut - 32*m2*Q2hatcut + 
          11*m2^2*Q2hatcut - Q2hatcut^2 - m2*Q2hatcut^2 - 21*Q2hatcut^3))/
        (3*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2]) + 4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(-1/6*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-15 - 22*m2 - 22*m2^2 - 70*m2^3 + 9*m2^4 - 
          15*Q2hatcut + 29*m2*Q2hatcut - 43*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          21*Q2hatcut^2 + 38*m2*Q2hatcut^2 - 21*m2^2*Q2hatcut^2 - 
          21*Q2hatcut^3 - 21*m2*Q2hatcut^3 + 24*Q2hatcut^4)) + 
       2*m2*(4 - 3*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])] + 
       c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
           (59 - 137*m2 + 7*m2^2 + 11*m2^3 + 11*Q2hatcut - 32*m2*Q2hatcut + 
            11*m2^2*Q2hatcut - Q2hatcut^2 - m2*Q2hatcut^2 - 21*Q2hatcut^3))/
          (3*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
             m2]) + 4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))))/
  mb^3 + (3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   42*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   282*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   42*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   3*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 33*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 33*m2^2*Q2hatcut*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   3*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 7*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 2*m2*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   7*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 7*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 7*m2*Q2hatcut^3*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   8*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 10*api*X1Q2[1, SM^2, Q2hatcut, m2, mu2hat])/10 + 
 c[SL]^2*(-6*m2^2*(1 + m2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     14*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     94*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 14*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 11*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 11*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 6*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 9*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 9*m2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     6*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 10*api*X1Q2[1, c[SL]^2, Q2hatcut, m2, mu2hat])/10) + 
 c[SR]^2*(-6*m2^2*(1 + m2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     14*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     94*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 14*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 11*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 11*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 6*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 9*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 9*m2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     6*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 10*api*X1Q2[1, c[SR]^2, Q2hatcut, m2, mu2hat])/10) + 
 c[SL]*(c[SR]*(Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2)] + 29*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] + 
     29*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] + 
     Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + 8*m2*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] + 
     m2^2*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] + m2*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     3*Q2hatcut^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + 12*m2^(3/2)*(1 + 3*m2 + m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     api*X1Q2[1, c[SL]*c[SR], Q2hatcut, m2, mu2hat]) + 
   api*c[T]*X1Q2[1, c[SL]*c[T], Q2hatcut, m2, mu2hat]) + 
 c[T]^2*(-192*m2^2*(1 + m2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (16*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     224*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1504*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 224*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     16*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 16*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 176*m2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     176*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 16*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 64*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     64*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 64*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 16*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     16*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 16*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 5*api*X1Q2[1, c[T]^2, Q2hatcut, m2, 
       mu2hat])/5) + 
 c[VL]^2*(-18*m2^2*(1 + m2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     42*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     282*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 42*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     3*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 33*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 33*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     3*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 7*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 2*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 7*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 7*m2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     8*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 10*api*X1Q2[1, c[VL]^2, Q2hatcut, m2, mu2hat])/10) + 
 c[VR]*(-2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2)] - 58*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
       2*m2*Q2hatcut + Q2hatcut^2)] - 
   58*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2)] - 2*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
       2*m2*Q2hatcut + Q2hatcut^2)] - 
   2*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2)] - 16*m2*Q2hatcut*
    Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
   2*m2^2*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2)] - 2*Q2hatcut^2*
    Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
   2*m2*Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2)] + 6*Q2hatcut^3*
    Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
   24*m2^(3/2)*(1 + 3*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   api*X1Q2[1, SM*c[VR], Q2hatcut, m2, mu2hat]) + 
 c[VL]*(-36*m2^2*(1 + m2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     42*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     282*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 42*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     3*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 33*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 33*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     3*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 7*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 2*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 7*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 7*m2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     8*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 5*api*X1Q2[1, SM*c[VL], Q2hatcut, m2, mu2hat])/5 + 
   c[VR]*(-2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 58*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 
     58*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 2*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 2*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     16*m2*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 2*m2^2*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     2*Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 2*m2*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] + 
     6*Q2hatcut^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 24*m2^(3/2)*(1 + 3*m2 + m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     api*X1Q2[1, c[VL]*c[VR], Q2hatcut, m2, mu2hat])) + 
 c[VR]^2*(-18*m2^2*(1 + m2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     42*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     282*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 42*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     3*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 33*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 33*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     3*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 7*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 2*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 7*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 7*m2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     8*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 10*api*X1Q2[1, c[VR]^2, Q2hatcut, m2, mu2hat])/10)
