-30*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
  Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
       2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
     Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
 (mupi*(-1/28*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(1 - 34*m2 - 1133*m2^2 - 2708*m2^3 - 1133*m2^4 - 
        34*m2^5 + m2^6 + Q2hatcut - 31*m2*Q2hatcut - 390*m2^2*Q2hatcut - 
        390*m2^3*Q2hatcut - 31*m2^4*Q2hatcut + m2^5*Q2hatcut + Q2hatcut^2 - 
        26*m2*Q2hatcut^2 - 118*m2^2*Q2hatcut^2 - 26*m2^3*Q2hatcut^2 + 
        m2^4*Q2hatcut^2 + Q2hatcut^3 - 19*m2*Q2hatcut^3 - 
        19*m2^2*Q2hatcut^3 + m2^3*Q2hatcut^3 - 6*Q2hatcut^4 + 
        4*m2*Q2hatcut^4 - 6*m2^2*Q2hatcut^4 - 6*Q2hatcut^5 - 
        6*m2*Q2hatcut^5 + 8*Q2hatcut^6)) + 15*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*(-1/70*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 34*m2 - 1133*m2^2 - 2708*m2^3 - 1133*m2^4 - 
          34*m2^5 + m2^6 + Q2hatcut - 31*m2*Q2hatcut - 390*m2^2*Q2hatcut - 
          390*m2^3*Q2hatcut - 31*m2^4*Q2hatcut + m2^5*Q2hatcut + Q2hatcut^2 - 
          26*m2*Q2hatcut^2 - 118*m2^2*Q2hatcut^2 - 26*m2^3*Q2hatcut^2 + 
          m2^4*Q2hatcut^2 + Q2hatcut^3 - 19*m2*Q2hatcut^3 - 
          19*m2^2*Q2hatcut^3 + m2^3*Q2hatcut^3 + Q2hatcut^4 - 
          10*m2*Q2hatcut^4 + m2^2*Q2hatcut^4 - 20*Q2hatcut^5 - 
          20*m2*Q2hatcut^5 + 15*Q2hatcut^6)) + 6*m2^2*(1 + m2)*
        (1 + 4*m2 + m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*(-1/70*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 34*m2 - 1133*m2^2 - 2708*m2^3 - 1133*m2^4 - 
          34*m2^5 + m2^6 + Q2hatcut - 31*m2*Q2hatcut - 390*m2^2*Q2hatcut - 
          390*m2^3*Q2hatcut - 31*m2^4*Q2hatcut + m2^5*Q2hatcut + Q2hatcut^2 - 
          26*m2*Q2hatcut^2 - 118*m2^2*Q2hatcut^2 - 26*m2^3*Q2hatcut^2 + 
          m2^4*Q2hatcut^2 + Q2hatcut^3 - 19*m2*Q2hatcut^3 - 
          19*m2^2*Q2hatcut^3 + m2^3*Q2hatcut^3 + Q2hatcut^4 - 
          10*m2*Q2hatcut^4 + m2^2*Q2hatcut^4 - 20*Q2hatcut^5 - 
          20*m2*Q2hatcut^5 + 15*Q2hatcut^6)) + 6*m2^2*(1 + m2)*
        (1 + 4*m2 + m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*(-1/28*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 34*m2 - 1133*m2^2 - 2708*m2^3 - 1133*m2^4 - 
          34*m2^5 + m2^6 + Q2hatcut - 31*m2*Q2hatcut - 390*m2^2*Q2hatcut - 
          390*m2^3*Q2hatcut - 31*m2^4*Q2hatcut + m2^5*Q2hatcut + Q2hatcut^2 - 
          26*m2*Q2hatcut^2 - 118*m2^2*Q2hatcut^2 - 26*m2^3*Q2hatcut^2 + 
          m2^4*Q2hatcut^2 + Q2hatcut^3 - 19*m2*Q2hatcut^3 - 
          19*m2^2*Q2hatcut^3 + m2^3*Q2hatcut^3 - 6*Q2hatcut^4 + 
          4*m2*Q2hatcut^4 - 6*m2^2*Q2hatcut^4 - 6*Q2hatcut^5 - 
          6*m2*Q2hatcut^5 + 8*Q2hatcut^6)) + 15*m2^2*(1 + m2)*
        (1 + 4*m2 + m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/28*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 34*m2 - 1133*m2^2 - 2708*m2^3 - 1133*m2^4 - 
          34*m2^5 + m2^6 + Q2hatcut - 31*m2*Q2hatcut - 390*m2^2*Q2hatcut - 
          390*m2^3*Q2hatcut - 31*m2^4*Q2hatcut + m2^5*Q2hatcut + Q2hatcut^2 - 
          26*m2*Q2hatcut^2 - 118*m2^2*Q2hatcut^2 - 26*m2^3*Q2hatcut^2 + 
          m2^4*Q2hatcut^2 + Q2hatcut^3 - 19*m2*Q2hatcut^3 - 
          19*m2^2*Q2hatcut^3 + m2^3*Q2hatcut^3 - 6*Q2hatcut^4 + 
          4*m2*Q2hatcut^4 - 6*m2^2*Q2hatcut^4 - 6*Q2hatcut^5 - 
          6*m2*Q2hatcut^5 + 8*Q2hatcut^6)) + 15*m2^2*(1 + m2)*
        (1 + 4*m2 + m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((-4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(3 - 102*m2 - 3399*m2^2 - 8124*m2^3 - 3399*m2^4 - 
          102*m2^5 + 3*m2^6 + 3*Q2hatcut - 93*m2*Q2hatcut - 
          1170*m2^2*Q2hatcut - 1170*m2^3*Q2hatcut - 93*m2^4*Q2hatcut + 
          3*m2^5*Q2hatcut + 3*Q2hatcut^2 - 78*m2*Q2hatcut^2 - 
          354*m2^2*Q2hatcut^2 - 78*m2^3*Q2hatcut^2 + 3*m2^4*Q2hatcut^2 + 
          3*Q2hatcut^3 - 57*m2*Q2hatcut^3 - 57*m2^2*Q2hatcut^3 + 
          3*m2^3*Q2hatcut^3 - 32*Q2hatcut^4 + 40*m2*Q2hatcut^4 - 
          32*m2^2*Q2hatcut^4 + 10*Q2hatcut^5 + 10*m2*Q2hatcut^5 + 
          10*Q2hatcut^6))/35 + 144*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]*c[SR]*(-1/5*(Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2)]*(1 + 102*m2 + 527*m2^2 + 527*m2^3 + 
          102*m2^4 + m2^5 + Q2hatcut + 45*m2*Q2hatcut + 118*m2^2*Q2hatcut + 
          45*m2^3*Q2hatcut + m2^4*Q2hatcut + Q2hatcut^2 + 20*m2*Q2hatcut^2 + 
          20*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 + Q2hatcut^3 + 
          7*m2*Q2hatcut^3 + m2^2*Q2hatcut^3 + Q2hatcut^4 + m2*Q2hatcut^4 - 
          5*Q2hatcut^5)) - 6*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*((2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2)]*(1 + 102*m2 + 527*m2^2 + 527*m2^3 + 102*m2^4 + 
          m2^5 + Q2hatcut + 45*m2*Q2hatcut + 118*m2^2*Q2hatcut + 
          45*m2^3*Q2hatcut + m2^4*Q2hatcut + Q2hatcut^2 + 20*m2*Q2hatcut^2 + 
          20*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 + Q2hatcut^3 + 
          7*m2*Q2hatcut^3 + m2^2*Q2hatcut^3 + Q2hatcut^4 + m2*Q2hatcut^4 - 
          5*Q2hatcut^5))/5 + 12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + 
         m2^4)*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(-1/14*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 34*m2 - 1133*m2^2 - 2708*m2^3 - 1133*m2^4 - 
          34*m2^5 + m2^6 + Q2hatcut - 31*m2*Q2hatcut - 390*m2^2*Q2hatcut - 
          390*m2^3*Q2hatcut - 31*m2^4*Q2hatcut + m2^5*Q2hatcut + Q2hatcut^2 - 
          26*m2*Q2hatcut^2 - 118*m2^2*Q2hatcut^2 - 26*m2^3*Q2hatcut^2 + 
          m2^4*Q2hatcut^2 + Q2hatcut^3 - 19*m2*Q2hatcut^3 - 
          19*m2^2*Q2hatcut^3 + m2^3*Q2hatcut^3 - 6*Q2hatcut^4 + 
          4*m2*Q2hatcut^4 - 6*m2^2*Q2hatcut^4 - 6*Q2hatcut^5 - 
          6*m2*Q2hatcut^5 + 8*Q2hatcut^6)) + 30*m2^2*(1 + m2)*
        (1 + 4*m2 + m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2)]*(1 + 102*m2 + 527*m2^2 + 527*m2^3 + 102*m2^4 + 
            m2^5 + Q2hatcut + 45*m2*Q2hatcut + 118*m2^2*Q2hatcut + 
            45*m2^3*Q2hatcut + m2^4*Q2hatcut + Q2hatcut^2 + 
            20*m2*Q2hatcut^2 + 20*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 + 
            Q2hatcut^3 + 7*m2*Q2hatcut^3 + m2^2*Q2hatcut^3 + Q2hatcut^4 + 
            m2*Q2hatcut^4 - 5*Q2hatcut^5))/5 + 12*m2^(3/2)*
          (1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 486*m2^5 + 
        25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 3466*m2^2*Q2hatcut + 
        666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 25*m2^5*Q2hatcut - 
        115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 886*m2^2*Q2hatcut^2 - 
        286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 115*Q2hatcut^3 + 
        309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 25*m2^3*Q2hatcut^3 - 
        150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 150*m2^2*Q2hatcut^4 - 
        150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 200*Q2hatcut^6))/140 - 
     3*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(61 + 5542*m2 + 23847*m2^2 + 15972*m2^3 + 47*m2^4 - 
          114*m2^5 + 5*m2^6 + 61*Q2hatcut + 2365*m2*Q2hatcut + 
          4658*m2^2*Q2hatcut + 570*m2^3*Q2hatcut - 99*m2^4*Q2hatcut + 
          5*m2^5*Q2hatcut + 61*Q2hatcut^2 + 990*m2*Q2hatcut^2 + 
          530*m2^2*Q2hatcut^2 - 74*m2^3*Q2hatcut^2 + 5*m2^4*Q2hatcut^2 + 
          61*Q2hatcut^3 + 297*m2*Q2hatcut^3 - 39*m2^2*Q2hatcut^3 + 
          5*m2^3*Q2hatcut^3 + 61*Q2hatcut^4 + 6*m2*Q2hatcut^4 + 
          5*m2^2*Q2hatcut^4 + 40*Q2hatcut^5 - 100*m2*Q2hatcut^5 + 
          75*Q2hatcut^6))/70 - 6*m2*(-4 - 35*m2 - 55*m2^2 - 15*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(61 + 5542*m2 + 23847*m2^2 + 15972*m2^3 + 47*m2^4 - 
          114*m2^5 + 5*m2^6 + 61*Q2hatcut + 2365*m2*Q2hatcut + 
          4658*m2^2*Q2hatcut + 570*m2^3*Q2hatcut - 99*m2^4*Q2hatcut + 
          5*m2^5*Q2hatcut + 61*Q2hatcut^2 + 990*m2*Q2hatcut^2 + 
          530*m2^2*Q2hatcut^2 - 74*m2^3*Q2hatcut^2 + 5*m2^4*Q2hatcut^2 + 
          61*Q2hatcut^3 + 297*m2*Q2hatcut^3 - 39*m2^2*Q2hatcut^3 + 
          5*m2^3*Q2hatcut^3 + 61*Q2hatcut^4 + 6*m2*Q2hatcut^4 + 
          5*m2^2*Q2hatcut^4 + 40*Q2hatcut^5 - 100*m2*Q2hatcut^5 + 
          75*Q2hatcut^6))/70 - 6*m2*(-4 - 35*m2 - 55*m2^2 - 15*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-97 - 4598*m2 - 4283*m2^2 + 5692*m2^3 - 1483*m2^4 - 
          286*m2^5 + 15*m2^6 - 97*Q2hatcut - 1529*m2*Q2hatcut + 
          758*m2^2*Q2hatcut + 254*m2^3*Q2hatcut - 241*m2^4*Q2hatcut + 
          15*m2^5*Q2hatcut - 97*Q2hatcut^2 - 334*m2*Q2hatcut^2 + 
          414*m2^2*Q2hatcut^2 - 166*m2^3*Q2hatcut^2 + 15*m2^4*Q2hatcut^2 - 
          97*Q2hatcut^3 + 107*m2*Q2hatcut^3 - 61*m2^2*Q2hatcut^3 + 
          15*m2^3*Q2hatcut^3 + 8*Q2hatcut^4 + 144*m2*Q2hatcut^4 - 
          160*m2^2*Q2hatcut^4 + 50*Q2hatcut^5 + 50*m2*Q2hatcut^5 + 
          50*Q2hatcut^6))/35 - 48*m2*(4 + 15*m2 - 5*m2^2 - 5*m2^3 + 3*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 
          486*m2^5 + 25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 
          3466*m2^2*Q2hatcut + 666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 
          25*m2^5*Q2hatcut - 115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 
          886*m2^2*Q2hatcut^2 - 286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 
          115*Q2hatcut^3 + 309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 
          25*m2^3*Q2hatcut^3 - 150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 
          150*m2^2*Q2hatcut^4 - 150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 
          200*Q2hatcut^6))/140 - 3*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 
          486*m2^5 + 25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 
          3466*m2^2*Q2hatcut + 666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 
          25*m2^5*Q2hatcut - 115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 
          886*m2^2*Q2hatcut^2 - 286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 
          115*Q2hatcut^3 + 309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 
          25*m2^3*Q2hatcut^3 - 150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 
          150*m2^2*Q2hatcut^4 - 150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 
          200*Q2hatcut^6))/140 - 3*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]*c[SR]*((Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2)]*(-271 - 3218*m2 - 5943*m2^2 - 1943*m2^3 + 32*m2^4 + 
          3*m2^5 - 151*Q2hatcut - 971*m2*Q2hatcut - 752*m2^2*Q2hatcut - 
          19*m2^3*Q2hatcut + 3*m2^4*Q2hatcut - 91*Q2hatcut^2 - 
          256*m2*Q2hatcut^2 - 34*m2^2*Q2hatcut^2 + 3*m2^3*Q2hatcut^2 - 
          51*Q2hatcut^3 - 33*m2*Q2hatcut^3 + 3*m2^2*Q2hatcut^3 - 
          21*Q2hatcut^4 + 3*m2*Q2hatcut^4 - 15*Q2hatcut^5))/5 + 
       6*Sqrt[m2]*(-2 - 47*m2 - 170*m2^2 - 140*m2^3 - 20*m2^4 + m2^5)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*((-2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2)]*(46 + 43*m2 - 827*m2^2 - 547*m2^3 + 23*m2^4 + 
          2*m2^5 + 22*Q2hatcut - 47*m2*Q2hatcut - 180*m2^2*Q2hatcut - 
          7*m2^3*Q2hatcut + 2*m2^4*Q2hatcut + 10*Q2hatcut^2 - 
          39*m2*Q2hatcut^2 - 15*m2^2*Q2hatcut^2 + 2*m2^3*Q2hatcut^2 + 
          2*Q2hatcut^3 - 13*m2*Q2hatcut^3 + 2*m2^2*Q2hatcut^3 - Q2hatcut^4 - 
          m2*Q2hatcut^4 - 7*Q2hatcut^5))/3 - 4*Sqrt[m2]*
        (2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
         (-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 486*m2^5 + 
          25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 3466*m2^2*Q2hatcut + 
          666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 25*m2^5*Q2hatcut - 
          115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 886*m2^2*Q2hatcut^2 - 
          286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 115*Q2hatcut^3 + 
          309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 25*m2^3*Q2hatcut^3 - 
          150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 150*m2^2*Q2hatcut^4 - 
          150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 200*Q2hatcut^6))/70 - 
       6*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((-2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2)]*(46 + 43*m2 - 827*m2^2 - 547*m2^3 + 23*m2^4 + 
            2*m2^5 + 22*Q2hatcut - 47*m2*Q2hatcut - 180*m2^2*Q2hatcut - 
            7*m2^3*Q2hatcut + 2*m2^4*Q2hatcut + 10*Q2hatcut^2 - 
            39*m2*Q2hatcut^2 - 15*m2^2*Q2hatcut^2 + 2*m2^3*Q2hatcut^2 + 
            2*Q2hatcut^3 - 13*m2*Q2hatcut^3 + 2*m2^2*Q2hatcut^3 - 
            Q2hatcut^4 - m2*Q2hatcut^4 - 7*Q2hatcut^5))/3 - 
         4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))))/
  mb^2 + 
 (rhoD*((11897 - 78740*m2 - 310632*m2^2 + 584500*m2^3 + 28350*m2^4 - 
       244972*m2^5 + 9240*m2^6 + 332*m2^7 + 25*m2^8 - 18617*Q2hatcut + 
       36329*m2*Q2hatcut + 978767*m2^2*Q2hatcut + 1434817*m2^3*Q2hatcut + 
       354017*m2^4*Q2hatcut - 22961*m2^5*Q2hatcut - 407*m2^6*Q2hatcut - 
       25*m2^7*Q2hatcut + 3360*Q2hatcut^2 - 10354*m2*Q2hatcut^2 - 
       156516*m2^2*Q2hatcut^2 - 72036*m2^3*Q2hatcut^2 + 
       5436*m2^4*Q2hatcut^2 - 50*m2^5*Q2hatcut^2 + 1120*Q2hatcut^3 - 
       6994*m2*Q2hatcut^3 - 18178*m2^2*Q2hatcut^3 + 1086*m2^3*Q2hatcut^3 - 
       50*m2^4*Q2hatcut^3 + 945*Q2hatcut^4 - 4614*m2*Q2hatcut^4 + 
       466*m2^2*Q2hatcut^4 + 90*m2^3*Q2hatcut^4 - 175*m2^4*Q2hatcut^4 + 
       623*Q2hatcut^5 - 2129*m2*Q2hatcut^5 + 783*m2^2*Q2hatcut^5 + 
       175*m2^3*Q2hatcut^5 - 98*Q2hatcut^6 - 1268*m2*Q2hatcut^6 + 
       350*m2^2*Q2hatcut^6 + 570*Q2hatcut^7 - 550*m2*Q2hatcut^7 + 
       200*Q2hatcut^8)/(420*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2]) + (8 + 8*m2 - 585*m2^2 - 925*m2^3 - 
       165*m2^4 + 15*m2^5)*Log[(1 + m2 - Q2hatcut - 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
        (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])] + 
     c[VR]*((2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2)]*(-156 - 1129*m2 + 845*m2^2 + 2000*m2^3 - 1220*m2^4 - 
          359*m2^5 + 19*m2^6 + 228*Q2hatcut + 2977*m2*Q2hatcut + 
          7162*m2^2*Q2hatcut + 4362*m2^3*Q2hatcut + 422*m2^4*Q2hatcut - 
          31*m2^5*Q2hatcut - 36*Q2hatcut^2 - 486*m2*Q2hatcut^2 - 
          664*m2^2*Q2hatcut^2 - 80*m2^3*Q2hatcut^2 + 6*m2^4*Q2hatcut^2 - 
          12*Q2hatcut^3 - 86*m2*Q2hatcut^3 - 30*m2^2*Q2hatcut^3 + 
          2*m2^3*Q2hatcut^3 - 7*Q2hatcut^4 - 10*m2*Q2hatcut^4 - 
          2*m2^2*Q2hatcut^4 + m2^3*Q2hatcut^4 - 3*Q2hatcut^5 + 
          3*m2*Q2hatcut^5 - 3*m2^2*Q2hatcut^5 + 3*Q2hatcut^6 + 
          3*m2*Q2hatcut^6 - Q2hatcut^7))/(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2) + 12*Sqrt[m2]*(-6 - 115*m2 - 320*m2^2 - 
         180*m2^3 - 10*m2^4 + m2^5)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (-1/15*(Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
             Q2hatcut^2)]*(-2869 - 21380*m2 + 14274*m2^2 + 39575*m2^3 - 
           21875*m2^4 - 7956*m2^5 + 230*m2^6 + m2^7 + 4189*Q2hatcut + 
           55727*m2*Q2hatcut + 138011*m2^2*Q2hatcut + 88986*m2^3*Q2hatcut + 
           10861*m2^4*Q2hatcut - 413*m2^5*Q2hatcut - m2^6*Q2hatcut - 
           660*Q2hatcut^2 - 9112*m2*Q2hatcut^2 - 13098*m2^2*Q2hatcut^2 - 
           1998*m2^3*Q2hatcut^2 + 88*m2^4*Q2hatcut^2 - 220*Q2hatcut^3 - 
           1632*m2*Q2hatcut^3 - 654*m2^2*Q2hatcut^3 + 28*m2^3*Q2hatcut^3 - 
           110*Q2hatcut^4 - 257*m2*Q2hatcut^4 + 13*m2^2*Q2hatcut^4 - 
           96*Q2hatcut^5 + 43*m2*Q2hatcut^5 - 6*m2^2*Q2hatcut^5 + 
           11*Q2hatcut^6 + 11*m2*Q2hatcut^6 - 5*Q2hatcut^7))/
         (1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2) - 
       2*Sqrt[m2]*(-22 - 429*m2 - 1230*m2^2 - 740*m2^3 - 60*m2^4 + 3*m2^5)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]^2*((3085 - 12108*m2 - 68572*m2^2 + 114520*m2^3 + 11550*m2^4 - 
         50260*m2^5 + 1708*m2^6 + 72*m2^7 + 5*m2^8 - 4765*Q2hatcut - 
         3867*m2*Q2hatcut + 177671*m2^2*Q2hatcut + 285541*m2^3*Q2hatcut + 
         74141*m2^4*Q2hatcut - 4469*m2^5*Q2hatcut - 87*m2^6*Q2hatcut - 
         5*m2^7*Q2hatcut + 840*Q2hatcut^2 - 290*m2*Q2hatcut^2 - 
         30284*m2^2*Q2hatcut^2 - 15012*m2^3*Q2hatcut^2 + 
         1076*m2^4*Q2hatcut^2 - 10*m2^5*Q2hatcut^2 + 280*Q2hatcut^3 - 
         1130*m2*Q2hatcut^3 - 3714*m2^2*Q2hatcut^3 + 206*m2^3*Q2hatcut^3 - 
         10*m2^4*Q2hatcut^3 + 140*Q2hatcut^4 - 710*m2*Q2hatcut^4 - 
         44*m2^2*Q2hatcut^4 - 10*m2^3*Q2hatcut^4 + 315*Q2hatcut^5 - 
         577*m2*Q2hatcut^5 + 263*m2^2*Q2hatcut^5 - 105*m2^3*Q2hatcut^5 - 
         140*Q2hatcut^6 - 388*m2*Q2hatcut^6 + 280*m2^2*Q2hatcut^6 + 
         170*Q2hatcut^7 - 250*m2*Q2hatcut^7 + 75*Q2hatcut^8)/
        (210*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + 2*(2 + 8*m2 - 105*m2^2 - 185*m2^3 - 35*m2^4 + 
         3*m2^5)*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*((3085 - 12108*m2 - 68572*m2^2 + 114520*m2^3 + 11550*m2^4 - 
         50260*m2^5 + 1708*m2^6 + 72*m2^7 + 5*m2^8 - 4765*Q2hatcut - 
         3867*m2*Q2hatcut + 177671*m2^2*Q2hatcut + 285541*m2^3*Q2hatcut + 
         74141*m2^4*Q2hatcut - 4469*m2^5*Q2hatcut - 87*m2^6*Q2hatcut - 
         5*m2^7*Q2hatcut + 840*Q2hatcut^2 - 290*m2*Q2hatcut^2 - 
         30284*m2^2*Q2hatcut^2 - 15012*m2^3*Q2hatcut^2 + 
         1076*m2^4*Q2hatcut^2 - 10*m2^5*Q2hatcut^2 + 280*Q2hatcut^3 - 
         1130*m2*Q2hatcut^3 - 3714*m2^2*Q2hatcut^3 + 206*m2^3*Q2hatcut^3 - 
         10*m2^4*Q2hatcut^3 + 140*Q2hatcut^4 - 710*m2*Q2hatcut^4 - 
         44*m2^2*Q2hatcut^4 - 10*m2^3*Q2hatcut^4 + 315*Q2hatcut^5 - 
         577*m2*Q2hatcut^5 + 263*m2^2*Q2hatcut^5 - 105*m2^3*Q2hatcut^5 - 
         140*Q2hatcut^6 - 388*m2*Q2hatcut^6 + 280*m2^2*Q2hatcut^6 + 
         170*Q2hatcut^7 - 250*m2*Q2hatcut^7 + 75*Q2hatcut^8)/
        (210*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + 2*(2 + 8*m2 - 105*m2^2 - 185*m2^3 - 35*m2^4 + 
         3*m2^5)*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((4*(5727 - 54524*m2 - 173488*m2^2 + 355460*m2^3 + 5250*m2^4 - 
          144452*m2^5 + 5824*m2^6 + 188*m2^7 + 15*m2^8 - 9087*Q2hatcut + 
          44063*m2*Q2hatcut + 623425*m2^2*Q2hatcut + 863735*m2^3*Q2hatcut + 
          205735*m2^4*Q2hatcut - 14023*m2^5*Q2hatcut - 233*m2^6*Q2hatcut - 
          15*m2^7*Q2hatcut + 1680*Q2hatcut^2 - 9774*m2*Q2hatcut^2 - 
          95948*m2^2*Q2hatcut^2 - 42012*m2^3*Q2hatcut^2 + 
          3284*m2^4*Q2hatcut^2 - 30*m2^5*Q2hatcut^2 + 560*Q2hatcut^3 - 
          4734*m2*Q2hatcut^3 - 10750*m2^2*Q2hatcut^3 + 674*m2^3*Q2hatcut^3 - 
          30*m2^4*Q2hatcut^3 + 665*Q2hatcut^4 - 3194*m2*Q2hatcut^4 + 
          554*m2^2*Q2hatcut^4 + 110*m2^3*Q2hatcut^4 - 175*m2^4*Q2hatcut^4 - 
          7*Q2hatcut^5 - 975*m2*Q2hatcut^5 + 257*m2^2*Q2hatcut^5 + 
          385*m2^3*Q2hatcut^5 + 182*Q2hatcut^6 - 492*m2*Q2hatcut^6 - 
          210*m2^2*Q2hatcut^6 + 230*Q2hatcut^7 - 50*m2*Q2hatcut^7 + 
          50*Q2hatcut^8))/(105*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + 16*(4 - 8*m2 - 375*m2^2 - 
         555*m2^3 - 95*m2^4 + 9*m2^5)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*((11897 - 78740*m2 - 310632*m2^2 + 584500*m2^3 + 28350*m2^4 - 
         244972*m2^5 + 9240*m2^6 + 332*m2^7 + 25*m2^8 - 18617*Q2hatcut + 
         36329*m2*Q2hatcut + 978767*m2^2*Q2hatcut + 1434817*m2^3*Q2hatcut + 
         354017*m2^4*Q2hatcut - 22961*m2^5*Q2hatcut - 407*m2^6*Q2hatcut - 
         25*m2^7*Q2hatcut + 3360*Q2hatcut^2 - 10354*m2*Q2hatcut^2 - 
         156516*m2^2*Q2hatcut^2 - 72036*m2^3*Q2hatcut^2 + 
         5436*m2^4*Q2hatcut^2 - 50*m2^5*Q2hatcut^2 + 1120*Q2hatcut^3 - 
         6994*m2*Q2hatcut^3 - 18178*m2^2*Q2hatcut^3 + 1086*m2^3*Q2hatcut^3 - 
         50*m2^4*Q2hatcut^3 + 945*Q2hatcut^4 - 4614*m2*Q2hatcut^4 + 
         466*m2^2*Q2hatcut^4 + 90*m2^3*Q2hatcut^4 - 175*m2^4*Q2hatcut^4 + 
         623*Q2hatcut^5 - 2129*m2*Q2hatcut^5 + 783*m2^2*Q2hatcut^5 + 
         175*m2^3*Q2hatcut^5 - 98*Q2hatcut^6 - 1268*m2*Q2hatcut^6 + 
         350*m2^2*Q2hatcut^6 + 570*Q2hatcut^7 - 550*m2*Q2hatcut^7 + 
         200*Q2hatcut^8)/(420*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + (8 + 8*m2 - 585*m2^2 - 925*m2^3 - 
         165*m2^4 + 15*m2^5)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*((11897 - 78740*m2 - 310632*m2^2 + 584500*m2^3 + 28350*m2^4 - 
         244972*m2^5 + 9240*m2^6 + 332*m2^7 + 25*m2^8 - 18617*Q2hatcut + 
         36329*m2*Q2hatcut + 978767*m2^2*Q2hatcut + 1434817*m2^3*Q2hatcut + 
         354017*m2^4*Q2hatcut - 22961*m2^5*Q2hatcut - 407*m2^6*Q2hatcut - 
         25*m2^7*Q2hatcut + 3360*Q2hatcut^2 - 10354*m2*Q2hatcut^2 - 
         156516*m2^2*Q2hatcut^2 - 72036*m2^3*Q2hatcut^2 + 
         5436*m2^4*Q2hatcut^2 - 50*m2^5*Q2hatcut^2 + 1120*Q2hatcut^3 - 
         6994*m2*Q2hatcut^3 - 18178*m2^2*Q2hatcut^3 + 1086*m2^3*Q2hatcut^3 - 
         50*m2^4*Q2hatcut^3 + 945*Q2hatcut^4 - 4614*m2*Q2hatcut^4 + 
         466*m2^2*Q2hatcut^4 + 90*m2^3*Q2hatcut^4 - 175*m2^4*Q2hatcut^4 + 
         623*Q2hatcut^5 - 2129*m2*Q2hatcut^5 + 783*m2^2*Q2hatcut^5 + 
         175*m2^3*Q2hatcut^5 - 98*Q2hatcut^6 - 1268*m2*Q2hatcut^6 + 
         350*m2^2*Q2hatcut^6 + 570*Q2hatcut^7 - 550*m2*Q2hatcut^7 + 
         200*Q2hatcut^8)/(420*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + (8 + 8*m2 - 585*m2^2 - 925*m2^3 - 
         165*m2^4 + 15*m2^5)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((11897 - 78740*m2 - 310632*m2^2 + 584500*m2^3 + 28350*m2^4 - 
         244972*m2^5 + 9240*m2^6 + 332*m2^7 + 25*m2^8 - 18617*Q2hatcut + 
         36329*m2*Q2hatcut + 978767*m2^2*Q2hatcut + 1434817*m2^3*Q2hatcut + 
         354017*m2^4*Q2hatcut - 22961*m2^5*Q2hatcut - 407*m2^6*Q2hatcut - 
         25*m2^7*Q2hatcut + 3360*Q2hatcut^2 - 10354*m2*Q2hatcut^2 - 
         156516*m2^2*Q2hatcut^2 - 72036*m2^3*Q2hatcut^2 + 
         5436*m2^4*Q2hatcut^2 - 50*m2^5*Q2hatcut^2 + 1120*Q2hatcut^3 - 
         6994*m2*Q2hatcut^3 - 18178*m2^2*Q2hatcut^3 + 1086*m2^3*Q2hatcut^3 - 
         50*m2^4*Q2hatcut^3 + 945*Q2hatcut^4 - 4614*m2*Q2hatcut^4 + 
         466*m2^2*Q2hatcut^4 + 90*m2^3*Q2hatcut^4 - 175*m2^4*Q2hatcut^4 + 
         623*Q2hatcut^5 - 2129*m2*Q2hatcut^5 + 783*m2^2*Q2hatcut^5 + 
         175*m2^3*Q2hatcut^5 - 98*Q2hatcut^6 - 1268*m2*Q2hatcut^6 + 
         350*m2^2*Q2hatcut^6 + 570*Q2hatcut^7 - 550*m2*Q2hatcut^7 + 
         200*Q2hatcut^8)/(210*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + 2*(8 + 8*m2 - 585*m2^2 - 925*m2^3 - 
         165*m2^4 + 15*m2^5)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2)]*(-156 - 1129*m2 + 845*m2^2 + 2000*m2^3 - 
            1220*m2^4 - 359*m2^5 + 19*m2^6 + 228*Q2hatcut + 
            2977*m2*Q2hatcut + 7162*m2^2*Q2hatcut + 4362*m2^3*Q2hatcut + 
            422*m2^4*Q2hatcut - 31*m2^5*Q2hatcut - 36*Q2hatcut^2 - 
            486*m2*Q2hatcut^2 - 664*m2^2*Q2hatcut^2 - 80*m2^3*Q2hatcut^2 + 
            6*m2^4*Q2hatcut^2 - 12*Q2hatcut^3 - 86*m2*Q2hatcut^3 - 
            30*m2^2*Q2hatcut^3 + 2*m2^3*Q2hatcut^3 - 7*Q2hatcut^4 - 
            10*m2*Q2hatcut^4 - 2*m2^2*Q2hatcut^4 + m2^3*Q2hatcut^4 - 
            3*Q2hatcut^5 + 3*m2*Q2hatcut^5 - 3*m2^2*Q2hatcut^5 + 
            3*Q2hatcut^6 + 3*m2*Q2hatcut^6 - Q2hatcut^7))/
          (1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2) + 
         12*Sqrt[m2]*(-6 - 115*m2 - 320*m2^2 - 180*m2^3 - 10*m2^4 + m2^5)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   rhoLS*(-1/140*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 
        486*m2^5 + 25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 
        3466*m2^2*Q2hatcut + 666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 
        25*m2^5*Q2hatcut - 115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 
        886*m2^2*Q2hatcut^2 - 286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 
        115*Q2hatcut^3 + 309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 
        25*m2^3*Q2hatcut^3 - 150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 
        150*m2^2*Q2hatcut^4 - 150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 
        200*Q2hatcut^6)) + 3*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*(-1/70*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(61 + 5542*m2 + 23847*m2^2 + 15972*m2^3 + 47*m2^4 - 
          114*m2^5 + 5*m2^6 + 61*Q2hatcut + 2365*m2*Q2hatcut + 
          4658*m2^2*Q2hatcut + 570*m2^3*Q2hatcut - 99*m2^4*Q2hatcut + 
          5*m2^5*Q2hatcut + 61*Q2hatcut^2 + 990*m2*Q2hatcut^2 + 
          530*m2^2*Q2hatcut^2 - 74*m2^3*Q2hatcut^2 + 5*m2^4*Q2hatcut^2 + 
          61*Q2hatcut^3 + 297*m2*Q2hatcut^3 - 39*m2^2*Q2hatcut^3 + 
          5*m2^3*Q2hatcut^3 + 61*Q2hatcut^4 + 6*m2*Q2hatcut^4 + 
          5*m2^2*Q2hatcut^4 + 40*Q2hatcut^5 - 100*m2*Q2hatcut^5 + 
          75*Q2hatcut^6)) + 6*m2*(-4 - 35*m2 - 55*m2^2 - 15*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SR]^2*(-1/70*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(61 + 5542*m2 + 23847*m2^2 + 15972*m2^3 + 47*m2^4 - 
          114*m2^5 + 5*m2^6 + 61*Q2hatcut + 2365*m2*Q2hatcut + 
          4658*m2^2*Q2hatcut + 570*m2^3*Q2hatcut - 99*m2^4*Q2hatcut + 
          5*m2^5*Q2hatcut + 61*Q2hatcut^2 + 990*m2*Q2hatcut^2 + 
          530*m2^2*Q2hatcut^2 - 74*m2^3*Q2hatcut^2 + 5*m2^4*Q2hatcut^2 + 
          61*Q2hatcut^3 + 297*m2*Q2hatcut^3 - 39*m2^2*Q2hatcut^3 + 
          5*m2^3*Q2hatcut^3 + 61*Q2hatcut^4 + 6*m2*Q2hatcut^4 + 
          5*m2^2*Q2hatcut^4 + 40*Q2hatcut^5 - 100*m2*Q2hatcut^5 + 
          75*Q2hatcut^6)) + 6*m2*(-4 - 35*m2 - 55*m2^2 - 15*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((-4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-97 - 4598*m2 - 4283*m2^2 + 5692*m2^3 - 1483*m2^4 - 
          286*m2^5 + 15*m2^6 - 97*Q2hatcut - 1529*m2*Q2hatcut + 
          758*m2^2*Q2hatcut + 254*m2^3*Q2hatcut - 241*m2^4*Q2hatcut + 
          15*m2^5*Q2hatcut - 97*Q2hatcut^2 - 334*m2*Q2hatcut^2 + 
          414*m2^2*Q2hatcut^2 - 166*m2^3*Q2hatcut^2 + 15*m2^4*Q2hatcut^2 - 
          97*Q2hatcut^3 + 107*m2*Q2hatcut^3 - 61*m2^2*Q2hatcut^3 + 
          15*m2^3*Q2hatcut^3 + 8*Q2hatcut^4 + 144*m2*Q2hatcut^4 - 
          160*m2^2*Q2hatcut^4 + 50*Q2hatcut^5 + 50*m2*Q2hatcut^5 + 
          50*Q2hatcut^6))/35 + 48*m2*(4 + 15*m2 - 5*m2^2 - 5*m2^3 + 3*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*(-1/140*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 
          486*m2^5 + 25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 
          3466*m2^2*Q2hatcut + 666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 
          25*m2^5*Q2hatcut - 115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 
          886*m2^2*Q2hatcut^2 - 286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 
          115*Q2hatcut^3 + 309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 
          25*m2^3*Q2hatcut^3 - 150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 
          150*m2^2*Q2hatcut^4 - 150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 
          200*Q2hatcut^6)) + 3*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/140*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 
          486*m2^5 + 25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 
          3466*m2^2*Q2hatcut + 666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 
          25*m2^5*Q2hatcut - 115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 
          886*m2^2*Q2hatcut^2 - 286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 
          115*Q2hatcut^3 + 309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 
          25*m2^3*Q2hatcut^3 - 150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 
          150*m2^2*Q2hatcut^4 - 150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 
          200*Q2hatcut^6)) + 3*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]*c[SR]*(-1/5*(Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2)]*(-271 - 3218*m2 - 5943*m2^2 - 
          1943*m2^3 + 32*m2^4 + 3*m2^5 - 151*Q2hatcut - 971*m2*Q2hatcut - 
          752*m2^2*Q2hatcut - 19*m2^3*Q2hatcut + 3*m2^4*Q2hatcut - 
          91*Q2hatcut^2 - 256*m2*Q2hatcut^2 - 34*m2^2*Q2hatcut^2 + 
          3*m2^3*Q2hatcut^2 - 51*Q2hatcut^3 - 33*m2*Q2hatcut^3 + 
          3*m2^2*Q2hatcut^3 - 21*Q2hatcut^4 + 3*m2*Q2hatcut^4 - 
          15*Q2hatcut^5)) - 6*Sqrt[m2]*(-2 - 47*m2 - 170*m2^2 - 140*m2^3 - 
         20*m2^4 + m2^5)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*((2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2)]*(46 + 43*m2 - 827*m2^2 - 547*m2^3 + 23*m2^4 + 
          2*m2^5 + 22*Q2hatcut - 47*m2*Q2hatcut - 180*m2^2*Q2hatcut - 
          7*m2^3*Q2hatcut + 2*m2^4*Q2hatcut + 10*Q2hatcut^2 - 
          39*m2*Q2hatcut^2 - 15*m2^2*Q2hatcut^2 + 2*m2^3*Q2hatcut^2 + 
          2*Q2hatcut^3 - 13*m2*Q2hatcut^3 + 2*m2^2*Q2hatcut^3 - Q2hatcut^4 - 
          m2*Q2hatcut^4 - 7*Q2hatcut^5))/3 + 4*Sqrt[m2]*
        (2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(-1/70*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-115 - 4126*m2 + 5499*m2^2 + 16524*m2^3 - 2201*m2^4 - 
          486*m2^5 + 25*m2^6 - 115*Q2hatcut - 1111*m2*Q2hatcut + 
          3466*m2^2*Q2hatcut + 666*m2^3*Q2hatcut - 411*m2^4*Q2hatcut + 
          25*m2^5*Q2hatcut - 115*Q2hatcut^2 - 6*m2*Q2hatcut^2 + 
          886*m2^2*Q2hatcut^2 - 286*m2^3*Q2hatcut^2 + 25*m2^4*Q2hatcut^2 - 
          115*Q2hatcut^3 + 309*m2*Q2hatcut^3 - 111*m2^2*Q2hatcut^3 + 
          25*m2^3*Q2hatcut^3 - 150*Q2hatcut^4 + 324*m2*Q2hatcut^4 - 
          150*m2^2*Q2hatcut^4 - 150*Q2hatcut^5 - 150*m2*Q2hatcut^5 + 
          200*Q2hatcut^6)) + 6*m2*(4 + 5*m2 - 35*m2^2 - 15*m2^3 + 5*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2)]*(46 + 43*m2 - 827*m2^2 - 547*m2^3 + 23*m2^4 + 
            2*m2^5 + 22*Q2hatcut - 47*m2*Q2hatcut - 180*m2^2*Q2hatcut - 
            7*m2^3*Q2hatcut + 2*m2^4*Q2hatcut + 10*Q2hatcut^2 - 
            39*m2*Q2hatcut^2 - 15*m2^2*Q2hatcut^2 + 2*m2^3*Q2hatcut^2 + 
            2*Q2hatcut^3 - 13*m2*Q2hatcut^3 + 2*m2^2*Q2hatcut^3 - 
            Q2hatcut^4 - m2*Q2hatcut^4 - 7*Q2hatcut^5))/3 + 
         4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))))/
  mb^3 + (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   34*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   1133*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 2708*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 
   1133*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 34*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 
   m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   31*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 390*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 390*m2^3*Q2hatcut*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   31*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + m2^5*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 
   Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 26*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 118*m2^2*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   26*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 
   Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 19*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 19*m2^2*Q2hatcut^3*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   m2^3*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 6*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 4*m2*Q2hatcut^4*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   6*m2^2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 6*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 6*m2*Q2hatcut^5*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   8*Q2hatcut^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 14*api*X1Q2[3, SM^2, Q2hatcut, m2, mu2hat])/14 + 
 c[SL]^2*(-12*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1133*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 2708*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 1133*m2^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 390*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     390*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^5*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 26*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 118*m2^2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     26*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     19*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 19*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 10*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^2*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     20*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 20*m2*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 15*Q2hatcut^6*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     35*api*X1Q2[3, c[SL]^2, Q2hatcut, m2, mu2hat])/35) + 
 c[SR]^2*(-12*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1133*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 2708*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 1133*m2^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 390*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     390*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^5*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 26*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 118*m2^2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     26*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     19*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 19*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 10*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^2*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     20*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 20*m2*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 15*Q2hatcut^6*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     35*api*X1Q2[3, c[SR]^2, Q2hatcut, m2, mu2hat])/35) + 
 c[SL]*(c[SR]*(12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     (2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 204*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] + 1054*m2^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 1054*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] + 
       204*m2^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*m2^5*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] + 2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 90*m2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 236*m2^2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 90*m2^3*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*m2^4*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 40*m2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 40*m2^2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*m2^3*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 14*m2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*m2^2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*Q2hatcut^4*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 2*m2*Q2hatcut^4*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 10*Q2hatcut^5*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 5*api*X1Q2[3, c[SL]*c[SR], Q2hatcut, m2, mu2hat])/
      5) + api*c[T]*X1Q2[3, c[SL]*c[T], Q2hatcut, m2, mu2hat]) + 
 c[T]^2*(-288*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (24*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     816*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     27192*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 64992*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 27192*m2^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     816*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 24*m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 24*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     744*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 9360*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 9360*m2^3*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     744*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 24*m2^5*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 24*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     624*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 2832*m2^2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     624*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 24*m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 24*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     456*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 456*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 24*m2^3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     256*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 320*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 256*m2^2*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     80*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 80*m2*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 80*Q2hatcut^6*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     35*api*X1Q2[3, c[T]^2, Q2hatcut, m2, mu2hat])/35) + 
 c[VL]^2*(-30*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1133*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 2708*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 1133*m2^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 390*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     390*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^5*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 26*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 118*m2^2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     26*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     19*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 19*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 6*m2^2*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 6*m2*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 8*Q2hatcut^6*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     14*api*X1Q2[3, c[VL]^2, Q2hatcut, m2, mu2hat])/14) + 
 c[VR]*(-24*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (-4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     408*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 2108*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 
     2108*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 408*m2^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 
     4*m2^5*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 4*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 180*m2*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     472*m2^2*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 180*m2^3*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     4*m2^4*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 4*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     80*m2*Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 80*m2^2*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     4*m2^3*Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 4*Q2hatcut^3*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     28*m2*Q2hatcut^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 4*m2^2*Q2hatcut^3*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     4*Q2hatcut^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 4*m2*Q2hatcut^4*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] + 
     20*Q2hatcut^5*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + 5*api*X1Q2[3, SM*c[VR], Q2hatcut, m2, mu2hat])/5) + 
 c[VL]*(-60*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1133*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 2708*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 1133*m2^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 390*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     390*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^5*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 26*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 118*m2^2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     26*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     19*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 19*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 6*m2^2*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 6*m2*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 8*Q2hatcut^6*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     7*api*X1Q2[3, SM*c[VL], Q2hatcut, m2, mu2hat])/7 + 
   c[VR]*(-24*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     (-4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 408*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] - 2108*m2^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 2108*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] - 
       408*m2^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*m2^5*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] - 4*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 180*m2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 472*m2^2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 180*m2^3*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*m2^4*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 80*m2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 80*m2^2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*m2^3*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 28*m2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*m2^2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*Q2hatcut^4*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 4*m2*Q2hatcut^4*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 20*Q2hatcut^5*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 5*api*X1Q2[3, c[VL]*c[VR], Q2hatcut, m2, mu2hat])/
      5)) + c[VR]^2*(-30*m2^2*(1 + m2)*(1 + 4*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1133*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 2708*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 1133*m2^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     34*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 390*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     390*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 31*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^5*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 26*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 118*m2^2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     26*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     19*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 19*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 6*m2^2*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 6*m2*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 8*Q2hatcut^6*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     14*api*X1Q2[3, c[VR]^2, Q2hatcut, m2, mu2hat])/14)
