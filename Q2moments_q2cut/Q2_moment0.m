Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
 7*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
 7*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
 m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
 Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
 m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
    Q2hatcut^2] - Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
    2*m2*Q2hatcut + Q2hatcut^2] - m2*Q2hatcut^2*
  Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
 Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
 12*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
       2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
     Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
 (mupi*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - Q2hatcut^2 - 
        m2*Q2hatcut^2 + Q2hatcut^3)) + 
     6*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*(-1/8*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 + Q2hatcut - 4*m2*Q2hatcut + 
          m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 3*Q2hatcut^3)) + 
       (3*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*(-1/8*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 + Q2hatcut - 4*m2*Q2hatcut + 
          m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 3*Q2hatcut^3)) + 
       (3*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[VL]^2*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
          Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3)) + 
       6*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
          Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3)) + 
       6*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*(-2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(3 - 21*m2 - 21*m2^2 + 3*m2^3 - 5*Q2hatcut + 
         4*m2*Q2hatcut - 5*m2^2*Q2hatcut + Q2hatcut^2 + m2*Q2hatcut^2 + 
         Q2hatcut^3) + 72*m2^2*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (-(((1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 2*Q2hatcut^2)*
          (1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2))/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2]) - 6*m2^(3/2)*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*((2*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 2*Q2hatcut^2)*
         (1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2))/
        Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
          m2] + 12*m2^(3/2)*(1 + m2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(-(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
         (1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - Q2hatcut^2 - 
          m2*Q2hatcut^2 + Q2hatcut^3)) + 
       12*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((2*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 2*Q2hatcut^2)*
           (1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2))/
          Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
            m2] + 12*m2^(3/2)*(1 + m2)*Log[(1 + m2 - Q2hatcut - 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
            (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])]))) + 
   muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 8*m2*Q2hatcut - 
        5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 5*Q2hatcut^3))/2 - 
     6*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 
          12*m2*Q2hatcut + 5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 
          25*m2*Q2hatcut^2 + 15*Q2hatcut^3))/8 - 
       (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut - 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
           (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 
          12*m2*Q2hatcut + 5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 
          25*m2*Q2hatcut^2 + 15*Q2hatcut^3))/8 - 
       (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut - 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
           (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])])/2) + 
     c[VL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
          8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
          5*Q2hatcut^3))/2 - 6*m2^2*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
          8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
          5*Q2hatcut^3))/2 - 6*m2^2*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*(2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(-25 - 25*m2 - 49*m2^2 + 15*m2^3 - Q2hatcut + 
         12*m2*Q2hatcut - 25*m2^2*Q2hatcut + 5*Q2hatcut^2 + 5*m2*Q2hatcut^2 + 
         5*Q2hatcut^3) - 24*m2*(4 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]*c[SR]*((3*(-11 - 2*m2 + m2^2 - 3*Q2hatcut + m2*Q2hatcut - 
          2*Q2hatcut^2)*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2))/Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)/m2] + 6*Sqrt[m2]*(-2 - 5*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*((-2*(13 - 14*m2 + 13*m2^2 + Q2hatcut + m2*Q2hatcut - 
          14*Q2hatcut^2)*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2))/(3*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2)/m2]) - 4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
        (-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 8*m2*Q2hatcut - 
         5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 5*Q2hatcut^3) - 
       12*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((-2*(13 - 14*m2 + 13*m2^2 + Q2hatcut + m2*Q2hatcut - 
            14*Q2hatcut^2)*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2))/(3*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2)/m2]) - 4*Sqrt[m2]*
          (2 - 3*m2 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
            (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])]))))/mb^2 + 
 (rhoD*((77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 151*Q2hatcut - 
       122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 10*m2^3*Q2hatcut - 
       15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 60*m2*Q2hatcut^2 + 
       28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 2*Q2hatcut^3 - 
       38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 17*Q2hatcut^4 - 
       15*m2*Q2hatcut^4 + 5*Q2hatcut^5)/
      (6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
     2*(4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[VR]*((-2*(25 - 64*m2 + 54*m2^2 - 16*m2^3 + m2^4 - 45*Q2hatcut - 
          35*m2*Q2hatcut + 37*m2^2*Q2hatcut - 5*m2^3*Q2hatcut + 
          9*Q2hatcut^2 - 14*m2*Q2hatcut^2 + 9*m2^2*Q2hatcut^2 - 
          7*Q2hatcut^3 - 7*m2*Q2hatcut^3 + 2*Q2hatcut^4))/
        Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
          m2] + 12*(-2 + m2)*Sqrt[m2]*(1 + m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*((77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 
         151*Q2hatcut - 122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 
         10*m2^3*Q2hatcut - 15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 
         60*m2*Q2hatcut^2 + 28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 
         2*Q2hatcut^3 - 38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 
         17*Q2hatcut^4 - 15*m2*Q2hatcut^4 + 5*Q2hatcut^5)/
        (6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
       2*(4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*((77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 
         151*Q2hatcut - 122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 
         10*m2^3*Q2hatcut - 15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 
         60*m2*Q2hatcut^2 + 28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 
         2*Q2hatcut^3 - 38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 
         17*Q2hatcut^4 - 15*m2*Q2hatcut^4 + 5*Q2hatcut^5)/
        (6*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
       2*(4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (-1/3*(-143 + 296*m2 - 162*m2^2 + 8*m2^3 + m2^4 + 263*Q2hatcut + 
          313*m2*Q2hatcut - 47*m2^2*Q2hatcut - m2^3*Q2hatcut - 
          75*Q2hatcut^2 + 34*m2*Q2hatcut^2 - 3*m2^2*Q2hatcut^2 + 
          5*Q2hatcut^3 + 5*m2*Q2hatcut^3 - 2*Q2hatcut^4)/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 2*Sqrt[m2]*(-10 - 15*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]^2*((197 - 381*m2 + 184*m2^2 - 8*m2^3 + 3*m2^4 + 5*m2^5 - 
         389*Q2hatcut - 402*m2*Q2hatcut - 98*m2^2*Q2hatcut - 
         18*m2^3*Q2hatcut - 5*m2^4*Q2hatcut + 162*Q2hatcut^2 - 
         124*m2*Q2hatcut^2 + 68*m2^2*Q2hatcut^2 - 30*m2^3*Q2hatcut^2 - 
         26*Q2hatcut^3 - 94*m2*Q2hatcut^3 + 70*m2^2*Q2hatcut^3 + 
         41*Q2hatcut^4 - 55*m2*Q2hatcut^4 + 15*Q2hatcut^5)/
        (24*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + ((8 + 8*m2 + 3*m2^2)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*((197 - 381*m2 + 184*m2^2 - 8*m2^3 + 3*m2^4 + 5*m2^5 - 
         389*Q2hatcut - 402*m2*Q2hatcut - 98*m2^2*Q2hatcut - 
         18*m2^3*Q2hatcut - 5*m2^4*Q2hatcut + 162*Q2hatcut^2 - 
         124*m2*Q2hatcut^2 + 68*m2^2*Q2hatcut^2 - 30*m2^3*Q2hatcut^2 - 
         26*Q2hatcut^3 - 94*m2*Q2hatcut^3 + 70*m2^2*Q2hatcut^3 + 
         41*Q2hatcut^4 - 55*m2*Q2hatcut^4 + 15*Q2hatcut^5)/
        (24*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + ((8 + 8*m2 + 3*m2^2)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[T]^2*((2*(111 - 279*m2 + 264*m2^2 - 120*m2^3 + 9*m2^4 + 15*m2^5 - 
          215*Q2hatcut - 86*m2*Q2hatcut - 54*m2^2*Q2hatcut - 
          22*m2^3*Q2hatcut - 55*m2^4*Q2hatcut + 38*Q2hatcut^2 - 
          116*m2*Q2hatcut^2 + 44*m2^2*Q2hatcut^2 + 70*m2^3*Q2hatcut^2 + 
          34*Q2hatcut^3 - 58*m2*Q2hatcut^3 - 30*m2^2*Q2hatcut^3 + 
          27*Q2hatcut^4 - 5*m2*Q2hatcut^4 + 5*Q2hatcut^5))/
        (3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
       8*(8 - 8*m2 + 9*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 
         151*Q2hatcut - 122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 
         10*m2^3*Q2hatcut - 15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 
         60*m2*Q2hatcut^2 + 28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 
         2*Q2hatcut^3 - 38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 
         17*Q2hatcut^4 - 15*m2*Q2hatcut^4 + 5*Q2hatcut^5)/
        (3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
       4*(4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((-2*(25 - 64*m2 + 54*m2^2 - 16*m2^3 + m2^4 - 45*Q2hatcut - 
            35*m2*Q2hatcut + 37*m2^2*Q2hatcut - 5*m2^3*Q2hatcut + 
            9*Q2hatcut^2 - 14*m2*Q2hatcut^2 + 9*m2^2*Q2hatcut^2 - 
            7*Q2hatcut^3 - 7*m2*Q2hatcut^3 + 2*Q2hatcut^4))/
          Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
            m2] + 12*(-2 + m2)*Sqrt[m2]*(1 + m2)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   rhoLS*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
        8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
        5*Q2hatcut^3)) + 6*m2^2*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]^2*(-1/8*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 
          12*m2*Q2hatcut + 5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 
          25*m2*Q2hatcut^2 + 15*Q2hatcut^3)) + 
       (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut - 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
           (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*(-1/8*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 
          12*m2*Q2hatcut + 5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 
          25*m2*Q2hatcut^2 + 15*Q2hatcut^3)) + 
       (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut - 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
           (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])])/2) + 
     c[VL]^2*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
          8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
          5*Q2hatcut^3)) + 6*m2^2*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
          8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
          5*Q2hatcut^3)) + 6*m2^2*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*(-2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(-25 - 25*m2 - 49*m2^2 + 15*m2^3 - Q2hatcut + 
         12*m2*Q2hatcut - 25*m2^2*Q2hatcut + 5*Q2hatcut^2 + 5*m2*Q2hatcut^2 + 
         5*Q2hatcut^3) + 24*m2*(4 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]*c[SR]*((-3*(-11 - 2*m2 + m2^2 - 3*Q2hatcut + m2*Q2hatcut - 
          2*Q2hatcut^2)*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2))/Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)/m2] - 6*Sqrt[m2]*(-2 - 5*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*((2*(13 - 14*m2 + 13*m2^2 + Q2hatcut + m2*Q2hatcut - 
          14*Q2hatcut^2)*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2))/(3*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2)/m2]) + 4*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*(-(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
         (-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 8*m2*Q2hatcut - 
          5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 5*Q2hatcut^3)) + 
       12*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*((2*(13 - 14*m2 + 13*m2^2 + Q2hatcut + m2*Q2hatcut - 
            14*Q2hatcut^2)*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2))/(3*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2)/m2]) + 4*Sqrt[m2]*
          (2 - 3*m2 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
            (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])]))))/mb^3 + 
 api*X1Q2[0, SM^2, Q2hatcut, m2, mu2hat] + 
 c[SL]^2*
  (-3*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 4*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     5*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 5*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     4*api*X1Q2[0, c[SL]^2, Q2hatcut, m2, mu2hat])/4) + 
 c[SR]^2*
  (-3*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     7*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 4*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     5*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 5*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 3*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     4*api*X1Q2[0, c[SR]^2, Q2hatcut, m2, mu2hat])/4) + 
 c[SL]*(c[SR]*(20*m2^(3/2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 2*m2^(5/2)*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     2*m2^(3/2)*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] + 2*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     4*Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + 12*m2^(3/2)*(1 + m2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     api*X1Q2[0, c[SL]*c[SR], Q2hatcut, m2, mu2hat]) + 
   api*c[T]*X1Q2[0, c[SL]*c[T], Q2hatcut, m2, mu2hat]) + 
 c[T]^2*(12*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   84*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   84*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   12*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   20*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 16*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 20*m2^2*Q2hatcut*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 4*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 4*Q2hatcut^3*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   144*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   api*X1Q2[0, c[T]^2, Q2hatcut, m2, mu2hat]) + 
 c[VL]^2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   7*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   7*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - m2*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 12*m2^2*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   api*X1Q2[0, c[VL]^2, Q2hatcut, m2, mu2hat]) + 
 c[VR]*(-40*m2^(3/2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 4*m2^(5/2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 4*m2^(3/2)*Q2hatcut*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
   4*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2)] + 8*Q2hatcut^2*
    Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
   24*m2^(3/2)*(1 + m2)*Log[(1 + m2 - Q2hatcut - 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
      (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])] + api*X1Q2[0, SM*c[VR], Q2hatcut, m2, 
     mu2hat]) + 
 c[VL]*(2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   14*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   14*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   2*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 2*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 2*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   2*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 
   24*m2^2*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   api*X1Q2[0, SM*c[VL], Q2hatcut, m2, mu2hat] + 
   c[VR]*(-40*m2^(3/2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 4*m2^(5/2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 4*m2^(3/2)*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     4*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + 8*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     24*m2^(3/2)*(1 + m2)*Log[(1 + m2 - Q2hatcut - 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
        (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])] + api*X1Q2[0, c[VL]*c[VR], Q2hatcut, m2, 
       mu2hat])) + 
 c[VR]^2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   7*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   7*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - m2*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 12*m2^2*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   api*X1Q2[0, c[VR]^2, Q2hatcut, m2, mu2hat])
