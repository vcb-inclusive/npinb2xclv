-8*m2^2*(3 + 8*m2 + 3*m2^2)*
  Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
       2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
     Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
 (mupi*(-1/15*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
        Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
        m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 15*m2^2*Q2hatcut^2 + 
        m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 2*m2*Q2hatcut^3 - 
        4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 4*m2*Q2hatcut^4 + 5*Q2hatcut^5)) + 
     4*m2^2*(3 + 8*m2 + 3*m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[SL]*c[SR]*(-1/10*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)*(3 + 178*m2 + 478*m2^2 + 178*m2^3 + 3*m2^4 + 
           3*Q2hatcut + 67*m2*Q2hatcut + 67*m2^2*Q2hatcut + 3*m2^3*Q2hatcut + 
           3*Q2hatcut^2 + 22*m2*Q2hatcut^2 + 3*m2^2*Q2hatcut^2 + 
           3*Q2hatcut^3 + 3*m2*Q2hatcut^3 - 12*Q2hatcut^4))/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 6*m2^(3/2)*(1 + m2)*(1 + 5*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
         (3 + 178*m2 + 478*m2^2 + 178*m2^3 + 3*m2^4 + 3*Q2hatcut + 
          67*m2*Q2hatcut + 67*m2^2*Q2hatcut + 3*m2^3*Q2hatcut + 
          3*Q2hatcut^2 + 22*m2*Q2hatcut^2 + 3*m2^2*Q2hatcut^2 + 
          3*Q2hatcut^3 + 3*m2*Q2hatcut^3 - 12*Q2hatcut^4))/
        (5*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2]) + 12*m2^(3/2)*(1 + m2)*(1 + 5*m2 + m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]^2*(-1/40*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
          Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
          m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 
          15*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 + Q2hatcut^3 - 
          8*m2*Q2hatcut^3 + m2^2*Q2hatcut^3 - 14*Q2hatcut^4 - 
          14*m2*Q2hatcut^4 + 10*Q2hatcut^5)) + 
       (3*m2^2*(3 + 8*m2 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
           (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*(-1/40*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
          Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
          m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 
          15*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 + Q2hatcut^3 - 
          8*m2*Q2hatcut^3 + m2^2*Q2hatcut^3 - 14*Q2hatcut^4 - 
          14*m2*Q2hatcut^4 + 10*Q2hatcut^5)) + 
       (3*m2^2*(3 + 8*m2 + 3*m2^2)*Log[(1 + m2 - Q2hatcut - 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
           (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])])/2) + 
     c[VL]^2*(-1/15*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
          Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
          m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 
          15*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          2*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5)) + 4*m2^2*(3 + 8*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/15*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
          Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
          m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 
          15*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          2*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5)) + 4*m2^2*(3 + 8*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[T]^2*((-2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
          Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
          m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 
          15*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 7*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 7*m2^2*Q2hatcut^3 + 2*Q2hatcut^4 + 
          2*m2*Q2hatcut^4 + 2*Q2hatcut^5))/3 + 40*m2^2*(3 + 8*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((-2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(1 - 23*m2 - 398*m2^2 - 398*m2^3 - 23*m2^4 + m2^5 + 
          Q2hatcut - 20*m2*Q2hatcut - 102*m2^2*Q2hatcut - 20*m2^3*Q2hatcut + 
          m2^4*Q2hatcut + Q2hatcut^2 - 15*m2*Q2hatcut^2 - 
          15*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          2*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5))/15 + 8*m2^2*(3 + 8*m2 + 3*m2^2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
           (3 + 178*m2 + 478*m2^2 + 178*m2^3 + 3*m2^4 + 3*Q2hatcut + 
            67*m2*Q2hatcut + 67*m2^2*Q2hatcut + 3*m2^3*Q2hatcut + 
            3*Q2hatcut^2 + 22*m2*Q2hatcut^2 + 3*m2^2*Q2hatcut^2 + 
            3*Q2hatcut^3 + 3*m2*Q2hatcut^3 - 12*Q2hatcut^4))/
          (5*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
             m2]) + 12*m2^(3/2)*(1 + m2)*(1 + 5*m2 + m2^2)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 3*Q2hatcut - 
        2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + m2^4*Q2hatcut - 
        3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 5*m2^2*Q2hatcut^2 + 
        m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 8*m2*Q2hatcut^3 - 
        4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 4*m2*Q2hatcut^4 + 5*Q2hatcut^5))/
      3 - 4*(-1 + m2)^2*m2*(2 + 3*m2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[VL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
          3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
          m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 
          5*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5))/3 - 4*(-1 + m2)^2*m2*(2 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
          3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
          m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 
          5*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5))/3 - 4*(-1 + m2)^2*m2*(2 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(41 + 2021*m2 + 3746*m2^2 + 146*m2^3 - 79*m2^4 + 
          5*m2^5 + 41*Q2hatcut + 704*m2*Q2hatcut + 294*m2^2*Q2hatcut - 
          64*m2^3*Q2hatcut + 5*m2^4*Q2hatcut + 41*Q2hatcut^2 + 
          189*m2*Q2hatcut^2 - 39*m2^2*Q2hatcut^2 + 5*m2^3*Q2hatcut^2 + 
          41*Q2hatcut^3 - 4*m2*Q2hatcut^3 + 5*m2^2*Q2hatcut^3 + 
          26*Q2hatcut^4 - 70*m2*Q2hatcut^4 + 50*Q2hatcut^5))/40 - 
       (3*m2*(4 + 3*m2)*(-3 - 12*m2 + m2^2)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(41 + 2021*m2 + 3746*m2^2 + 146*m2^3 - 79*m2^4 + 
          5*m2^5 + 41*Q2hatcut + 704*m2*Q2hatcut + 294*m2^2*Q2hatcut - 
          64*m2^3*Q2hatcut + 5*m2^4*Q2hatcut + 41*Q2hatcut^2 + 
          189*m2*Q2hatcut^2 - 39*m2^2*Q2hatcut^2 + 5*m2^3*Q2hatcut^2 + 
          41*Q2hatcut^3 - 4*m2*Q2hatcut^3 + 5*m2^2*Q2hatcut^3 + 
          26*Q2hatcut^4 - 70*m2*Q2hatcut^4 + 50*Q2hatcut^5))/40 - 
       (3*m2*(4 + 3*m2)*(-3 - 12*m2 + m2^2)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[T]^2*((2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-23 - 523*m2 - 78*m2^2 - 158*m2^3 - 63*m2^4 + 
          5*m2^5 - 23*Q2hatcut - 112*m2*Q2hatcut + 38*m2^2*Q2hatcut - 
          48*m2^3*Q2hatcut + 5*m2^4*Q2hatcut - 23*Q2hatcut^2 + 
          13*m2*Q2hatcut^2 - 23*m2^2*Q2hatcut^2 + 5*m2^3*Q2hatcut^2 + 
          Q2hatcut^3 + 28*m2*Q2hatcut^3 - 35*m2^2*Q2hatcut^3 + 
          10*Q2hatcut^4 + 10*m2*Q2hatcut^4 + 10*Q2hatcut^5))/3 - 
       40*m2*(4 + 7*m2 + 3*m2^3)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
         (-491 - 3166*m2 - 2266*m2^2 + 34*m2^3 + 9*m2^4 - 251*Q2hatcut - 
          679*m2*Q2hatcut - 59*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
          131*Q2hatcut^2 - 74*m2*Q2hatcut^2 + 9*m2^2*Q2hatcut^2 - 
          51*Q2hatcut^3 + 9*m2*Q2hatcut^3 - 36*Q2hatcut^4))/
        (10*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2]) + 6*Sqrt[m2]*(-2 - 29*m2 - 54*m2^2 - 14*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(-1/15*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)*(391 - 574*m2 - 2514*m2^2 + 146*m2^3 + 31*m2^4 + 
           151*Q2hatcut - 481*m2*Q2hatcut - 121*m2^2*Q2hatcut + 
           31*m2^3*Q2hatcut + 31*Q2hatcut^2 - 146*m2*Q2hatcut^2 + 
           31*m2^2*Q2hatcut^2 - 9*Q2hatcut^3 - 9*m2*Q2hatcut^3 - 
           84*Q2hatcut^4))/Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2)/m2] - 4*Sqrt[m2]*
        (2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
          3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
          m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 
          5*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5))/3 - 8*(-1 + m2)^2*m2*(2 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*(-1/15*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
             Q2hatcut^2)*(391 - 574*m2 - 2514*m2^2 + 146*m2^3 + 31*m2^4 + 
             151*Q2hatcut - 481*m2*Q2hatcut - 121*m2^2*Q2hatcut + 
             31*m2^3*Q2hatcut + 31*Q2hatcut^2 - 146*m2*Q2hatcut^2 + 
             31*m2^2*Q2hatcut^2 - 9*Q2hatcut^3 - 9*m2*Q2hatcut^3 - 
             84*Q2hatcut^4))/Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2)/m2] - 4*Sqrt[m2]*
          (2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))))/
  mb^2 + 
 (rhoD*((1119 - 5539*m2 - 855*m2^2 + 14475*m2^3 - 9775*m2^4 + 531*m2^5 + 
       39*m2^6 + 5*m2^7 - 1839*Q2hatcut + 1702*m2*Q2hatcut + 
       27597*m2^2*Q2hatcut + 14672*m2^3*Q2hatcut - 1753*m2^4*Q2hatcut - 
       54*m2^5*Q2hatcut - 5*m2^6*Q2hatcut + 360*Q2hatcut^2 - 
       918*m2*Q2hatcut^2 - 3214*m2^2*Q2hatcut^2 + 422*m2^3*Q2hatcut^2 - 
       10*m2^4*Q2hatcut^2 + 175*Q2hatcut^3 - 618*m2*Q2hatcut^3 + 
       122*m2^2*Q2hatcut^3 + 10*m2^3*Q2hatcut^3 - 25*m2^4*Q2hatcut^3 + 
       95*Q2hatcut^4 - 273*m2*Q2hatcut^4 + 105*m2^2*Q2hatcut^4 + 
       25*m2^3*Q2hatcut^4 - 9*Q2hatcut^5 - 164*m2*Q2hatcut^5 + 
       45*m2^2*Q2hatcut^5 + 74*Q2hatcut^6 - 70*m2*Q2hatcut^6 + 25*Q2hatcut^7)/
      (45*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]) + 
     (4*(6 + 4*m2 - 131*m2^2 - 56*m2^3 + 9*m2^4)*
       Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
            2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
          Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3 + 
     c[SL]*c[SR]*(-1/10*(-1399 - 3876*m2 + 8975*m2^2 - 600*m2^3 - 3225*m2^4 + 
          124*m2^5 + m2^6 + 2119*Q2hatcut + 16353*m2*Q2hatcut + 
          20928*m2^2*Q2hatcut + 4528*m2^3*Q2hatcut - 247*m2^4*Q2hatcut - 
          m2^5*Q2hatcut - 360*Q2hatcut^2 - 2422*m2*Q2hatcut^2 - 
          916*m2^2*Q2hatcut^2 + 58*m2^3*Q2hatcut^2 - 120*Q2hatcut^3 - 
          262*m2*Q2hatcut^3 + 18*m2^2*Q2hatcut^3 - 85*Q2hatcut^4 + 
          38*m2*Q2hatcut^4 - 5*m2^2*Q2hatcut^4 + 9*Q2hatcut^5 + 
          9*m2*Q2hatcut^5 - 4*Q2hatcut^6)/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 6*Sqrt[m2]*(-6 - 69*m2 - 94*m2^2 - 14*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(-1/15*(3241 + 8124*m2 - 20625*m2^2 + 3400*m2^3 + 6375*m2^4 - 
          516*m2^5 + m2^6 - 4921*Q2hatcut - 36447*m2*Q2hatcut - 
          43232*m2^2*Q2hatcut - 6992*m2^3*Q2hatcut + 873*m2^4*Q2hatcut - 
          m2^5*Q2hatcut + 840*Q2hatcut^2 + 5338*m2*Q2hatcut^2 + 
          1564*m2^2*Q2hatcut^2 - 182*m2^3*Q2hatcut^2 + 320*Q2hatcut^3 + 
          418*m2*Q2hatcut^3 + 58*m2^2*Q2hatcut^3 - 40*m2^3*Q2hatcut^3 + 
          115*Q2hatcut^4 - 122*m2*Q2hatcut^4 + 115*m2^2*Q2hatcut^4 - 
          111*Q2hatcut^5 - 111*m2*Q2hatcut^5 + 36*Q2hatcut^6)/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] + 4*Sqrt[m2]*(-14 - 155*m2 - 194*m2^2 - 18*m2^3 + 3*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]^2*((1119 - 5539*m2 - 855*m2^2 + 14475*m2^3 - 9775*m2^4 + 
         531*m2^5 + 39*m2^6 + 5*m2^7 - 1839*Q2hatcut + 1702*m2*Q2hatcut + 
         27597*m2^2*Q2hatcut + 14672*m2^3*Q2hatcut - 1753*m2^4*Q2hatcut - 
         54*m2^5*Q2hatcut - 5*m2^6*Q2hatcut + 360*Q2hatcut^2 - 
         918*m2*Q2hatcut^2 - 3214*m2^2*Q2hatcut^2 + 422*m2^3*Q2hatcut^2 - 
         10*m2^4*Q2hatcut^2 + 175*Q2hatcut^3 - 618*m2*Q2hatcut^3 + 
         122*m2^2*Q2hatcut^3 + 10*m2^3*Q2hatcut^3 - 25*m2^4*Q2hatcut^3 + 
         95*Q2hatcut^4 - 273*m2*Q2hatcut^4 + 105*m2^2*Q2hatcut^4 + 
         25*m2^3*Q2hatcut^4 - 9*Q2hatcut^5 - 164*m2*Q2hatcut^5 + 
         45*m2^2*Q2hatcut^5 + 74*Q2hatcut^6 - 70*m2*Q2hatcut^6 + 
         25*Q2hatcut^7)/(45*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + 
       (4*(6 + 4*m2 - 131*m2^2 - 56*m2^3 + 9*m2^4)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3) + 
     c[VR]^2*((1119 - 5539*m2 - 855*m2^2 + 14475*m2^3 - 9775*m2^4 + 
         531*m2^5 + 39*m2^6 + 5*m2^7 - 1839*Q2hatcut + 1702*m2*Q2hatcut + 
         27597*m2^2*Q2hatcut + 14672*m2^3*Q2hatcut - 1753*m2^4*Q2hatcut - 
         54*m2^5*Q2hatcut - 5*m2^6*Q2hatcut + 360*Q2hatcut^2 - 
         918*m2*Q2hatcut^2 - 3214*m2^2*Q2hatcut^2 + 422*m2^3*Q2hatcut^2 - 
         10*m2^4*Q2hatcut^2 + 175*Q2hatcut^3 - 618*m2*Q2hatcut^3 + 
         122*m2^2*Q2hatcut^3 + 10*m2^3*Q2hatcut^3 - 25*m2^4*Q2hatcut^3 + 
         95*Q2hatcut^4 - 273*m2*Q2hatcut^4 + 105*m2^2*Q2hatcut^4 + 
         25*m2^3*Q2hatcut^4 - 9*Q2hatcut^5 - 164*m2*Q2hatcut^5 + 
         45*m2^2*Q2hatcut^5 + 74*Q2hatcut^6 - 70*m2*Q2hatcut^6 + 
         25*Q2hatcut^7)/(45*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + 
       (4*(6 + 4*m2 - 131*m2^2 - 56*m2^3 + 9*m2^4)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3) + 
     c[SL]^2*((1573 - 4693*m2 - 3555*m2^2 + 15475*m2^3 - 9325*m2^4 + 
         477*m2^5 + 43*m2^6 + 5*m2^7 - 2533*Q2hatcut - 2906*m2*Q2hatcut + 
         23089*m2^2*Q2hatcut + 14364*m2^3*Q2hatcut - 1711*m2^4*Q2hatcut - 
         58*m2^5*Q2hatcut - 5*m2^6*Q2hatcut + 480*Q2hatcut^2 - 
         266*m2*Q2hatcut^2 - 3138*m2^2*Q2hatcut^2 + 414*m2^3*Q2hatcut^2 - 
         10*m2^4*Q2hatcut^2 + 160*Q2hatcut^3 - 426*m2*Q2hatcut^3 + 
         24*m2^2*Q2hatcut^3 - 10*m2^3*Q2hatcut^3 + 245*Q2hatcut^4 - 
         391*m2*Q2hatcut^4 + 185*m2^2*Q2hatcut^4 - 75*m2^3*Q2hatcut^4 - 
         93*Q2hatcut^5 - 268*m2*Q2hatcut^5 + 195*m2^2*Q2hatcut^5 + 
         118*Q2hatcut^6 - 170*m2*Q2hatcut^6 + 50*Q2hatcut^7)/
        (120*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + ((8 + 24*m2 - 111*m2^2 - 56*m2^3 + 9*m2^4)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*((1573 - 4693*m2 - 3555*m2^2 + 15475*m2^3 - 9325*m2^4 + 
         477*m2^5 + 43*m2^6 + 5*m2^7 - 2533*Q2hatcut - 2906*m2*Q2hatcut + 
         23089*m2^2*Q2hatcut + 14364*m2^3*Q2hatcut - 1711*m2^4*Q2hatcut - 
         58*m2^5*Q2hatcut - 5*m2^6*Q2hatcut + 480*Q2hatcut^2 - 
         266*m2*Q2hatcut^2 - 3138*m2^2*Q2hatcut^2 + 414*m2^3*Q2hatcut^2 - 
         10*m2^4*Q2hatcut^2 + 160*Q2hatcut^3 - 426*m2*Q2hatcut^3 + 
         24*m2^2*Q2hatcut^3 - 10*m2^3*Q2hatcut^3 + 245*Q2hatcut^4 - 
         391*m2*Q2hatcut^4 + 185*m2^2*Q2hatcut^4 - 75*m2^3*Q2hatcut^4 - 
         93*Q2hatcut^5 - 268*m2*Q2hatcut^5 + 195*m2^2*Q2hatcut^5 + 
         118*Q2hatcut^6 - 170*m2*Q2hatcut^6 + 50*Q2hatcut^7)/
        (120*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + ((8 + 24*m2 - 111*m2^2 - 56*m2^3 + 9*m2^4)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[T]^2*((2*(4233 - 30233*m2 + 3825*m2^2 + 69375*m2^3 - 50225*m2^4 + 
          2817*m2^5 + 183*m2^6 + 25*m2^7 - 7113*Q2hatcut + 
          22334*m2*Q2hatcut + 151509*m2^2*Q2hatcut + 74284*m2^3*Q2hatcut - 
          8891*m2^4*Q2hatcut - 258*m2^5*Q2hatcut - 25*m2^6*Q2hatcut + 
          1440*Q2hatcut^2 - 6546*m2*Q2hatcut^2 - 16298*m2^2*Q2hatcut^2 + 
          2134*m2^3*Q2hatcut^2 - 50*m2^4*Q2hatcut^2 + 920*Q2hatcut^3 - 
          3666*m2*Q2hatcut^3 + 904*m2^2*Q2hatcut^3 + 110*m2^3*Q2hatcut^3 - 
          200*m2^4*Q2hatcut^3 + 25*Q2hatcut^4 - 1011*m2*Q2hatcut^4 + 
          285*m2^2*Q2hatcut^4 + 425*m2^3*Q2hatcut^4 + 207*Q2hatcut^5 - 
          508*m2*Q2hatcut^5 - 225*m2^2*Q2hatcut^5 + 238*Q2hatcut^6 - 
          50*m2*Q2hatcut^6 + 50*Q2hatcut^7))/
        (45*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]) + (8*(24 - 40*m2 - 715*m2^2 - 280*m2^3 + 45*m2^4)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3) + 
     c[VL]*((2*(1119 - 5539*m2 - 855*m2^2 + 14475*m2^3 - 9775*m2^4 + 
          531*m2^5 + 39*m2^6 + 5*m2^7 - 1839*Q2hatcut + 1702*m2*Q2hatcut + 
          27597*m2^2*Q2hatcut + 14672*m2^3*Q2hatcut - 1753*m2^4*Q2hatcut - 
          54*m2^5*Q2hatcut - 5*m2^6*Q2hatcut + 360*Q2hatcut^2 - 
          918*m2*Q2hatcut^2 - 3214*m2^2*Q2hatcut^2 + 422*m2^3*Q2hatcut^2 - 
          10*m2^4*Q2hatcut^2 + 175*Q2hatcut^3 - 618*m2*Q2hatcut^3 + 
          122*m2^2*Q2hatcut^3 + 10*m2^3*Q2hatcut^3 - 25*m2^4*Q2hatcut^3 + 
          95*Q2hatcut^4 - 273*m2*Q2hatcut^4 + 105*m2^2*Q2hatcut^4 + 
          25*m2^3*Q2hatcut^4 - 9*Q2hatcut^5 - 164*m2*Q2hatcut^5 + 
          45*m2^2*Q2hatcut^5 + 74*Q2hatcut^6 - 70*m2*Q2hatcut^6 + 
          25*Q2hatcut^7))/(45*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2]) + 
       (8*(6 + 4*m2 - 131*m2^2 - 56*m2^3 + 9*m2^4)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/3 + 
       c[VR]*(-1/15*(3241 + 8124*m2 - 20625*m2^2 + 3400*m2^3 + 6375*m2^4 - 
            516*m2^5 + m2^6 - 4921*Q2hatcut - 36447*m2*Q2hatcut - 
            43232*m2^2*Q2hatcut - 6992*m2^3*Q2hatcut + 873*m2^4*Q2hatcut - 
            m2^5*Q2hatcut + 840*Q2hatcut^2 + 5338*m2*Q2hatcut^2 + 
            1564*m2^2*Q2hatcut^2 - 182*m2^3*Q2hatcut^2 + 320*Q2hatcut^3 + 
            418*m2*Q2hatcut^3 + 58*m2^2*Q2hatcut^3 - 40*m2^3*Q2hatcut^3 + 
            115*Q2hatcut^4 - 122*m2*Q2hatcut^4 + 115*m2^2*Q2hatcut^4 - 
            111*Q2hatcut^5 - 111*m2*Q2hatcut^5 + 36*Q2hatcut^6)/
           Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
             m2] + 4*Sqrt[m2]*(-14 - 155*m2 - 194*m2^2 - 18*m2^3 + 3*m2^4)*
          Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*
                (1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))) + 
   rhoLS*(-1/3*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
        3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
        m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 5*m2^2*Q2hatcut^2 + 
        m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 8*m2*Q2hatcut^3 - 
        4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 4*m2*Q2hatcut^4 + 5*Q2hatcut^5)) + 
     4*(-1 + m2)^2*m2*(2 + 3*m2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     c[VL]^2*(-1/3*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
          3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
          m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 
          5*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5)) + 4*(-1 + m2)^2*m2*(2 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]^2*(-1/3*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
          3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
          m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 
          5*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5)) + 4*(-1 + m2)^2*m2*(2 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[SL]^2*(-1/40*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(41 + 2021*m2 + 3746*m2^2 + 146*m2^3 - 79*m2^4 + 
          5*m2^5 + 41*Q2hatcut + 704*m2*Q2hatcut + 294*m2^2*Q2hatcut - 
          64*m2^3*Q2hatcut + 5*m2^4*Q2hatcut + 41*Q2hatcut^2 + 
          189*m2*Q2hatcut^2 - 39*m2^2*Q2hatcut^2 + 5*m2^3*Q2hatcut^2 + 
          41*Q2hatcut^3 - 4*m2*Q2hatcut^3 + 5*m2^2*Q2hatcut^3 + 
          26*Q2hatcut^4 - 70*m2*Q2hatcut^4 + 50*Q2hatcut^5)) + 
       (3*m2*(4 + 3*m2)*(-3 - 12*m2 + m2^2)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[SR]^2*(-1/40*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(41 + 2021*m2 + 3746*m2^2 + 146*m2^3 - 79*m2^4 + 
          5*m2^5 + 41*Q2hatcut + 704*m2*Q2hatcut + 294*m2^2*Q2hatcut - 
          64*m2^3*Q2hatcut + 5*m2^4*Q2hatcut + 41*Q2hatcut^2 + 
          189*m2*Q2hatcut^2 - 39*m2^2*Q2hatcut^2 + 5*m2^3*Q2hatcut^2 + 
          41*Q2hatcut^3 - 4*m2*Q2hatcut^3 + 5*m2^2*Q2hatcut^3 + 
          26*Q2hatcut^4 - 70*m2*Q2hatcut^4 + 50*Q2hatcut^5)) + 
       (3*m2*(4 + 3*m2)*(-3 - 12*m2 + m2^2)*
         Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
              2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
            Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])])/2) + 
     c[T]^2*((-2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-23 - 523*m2 - 78*m2^2 - 158*m2^3 - 63*m2^4 + 
          5*m2^5 - 23*Q2hatcut - 112*m2*Q2hatcut + 38*m2^2*Q2hatcut - 
          48*m2^3*Q2hatcut + 5*m2^4*Q2hatcut - 23*Q2hatcut^2 + 
          13*m2*Q2hatcut^2 - 23*m2^2*Q2hatcut^2 + 5*m2^3*Q2hatcut^2 + 
          Q2hatcut^3 + 28*m2*Q2hatcut^3 - 35*m2^2*Q2hatcut^3 + 
          10*Q2hatcut^4 + 10*m2*Q2hatcut^4 + 10*Q2hatcut^5))/3 + 
       40*m2*(4 + 7*m2 + 3*m2^3)*Log[(1 + m2 - Q2hatcut - 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])/
          (1 + m2 - Q2hatcut + Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])]) + c[SL]*c[SR]*
      (-1/10*((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
          (-491 - 3166*m2 - 2266*m2^2 + 34*m2^3 + 9*m2^4 - 251*Q2hatcut - 
           679*m2*Q2hatcut - 59*m2^2*Q2hatcut + 9*m2^3*Q2hatcut - 
           131*Q2hatcut^2 - 74*m2*Q2hatcut^2 + 9*m2^2*Q2hatcut^2 - 
           51*Q2hatcut^3 + 9*m2*Q2hatcut^3 - 36*Q2hatcut^4))/
         Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2] - 6*Sqrt[m2]*(-2 - 29*m2 - 54*m2^2 - 14*m2^3 + m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
         (391 - 574*m2 - 2514*m2^2 + 146*m2^3 + 31*m2^4 + 151*Q2hatcut - 
          481*m2*Q2hatcut - 121*m2^2*Q2hatcut + 31*m2^3*Q2hatcut + 
          31*Q2hatcut^2 - 146*m2*Q2hatcut^2 + 31*m2^2*Q2hatcut^2 - 
          9*Q2hatcut^3 - 9*m2*Q2hatcut^3 - 84*Q2hatcut^4))/
        (15*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/
           m2]) + 4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]) + 
     c[VL]*((-2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2]*(-3 - 41*m2 + 80*m2^2 - 24*m2^3 - 13*m2^4 + m2^5 - 
          3*Q2hatcut - 2*m2*Q2hatcut + 14*m2^2*Q2hatcut - 10*m2^3*Q2hatcut + 
          m2^4*Q2hatcut - 3*Q2hatcut^2 + 7*m2*Q2hatcut^2 - 
          5*m2^2*Q2hatcut^2 + m2^3*Q2hatcut^2 - 4*Q2hatcut^3 + 
          8*m2*Q2hatcut^3 - 4*m2^2*Q2hatcut^3 - 4*Q2hatcut^4 - 
          4*m2*Q2hatcut^4 + 5*Q2hatcut^5))/3 + 8*(-1 + m2)^2*m2*(2 + 3*m2)*
        Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
             2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
           Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
       c[VR]*(((1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)*
           (391 - 574*m2 - 2514*m2^2 + 146*m2^3 + 31*m2^4 + 151*Q2hatcut - 
            481*m2*Q2hatcut - 121*m2^2*Q2hatcut + 31*m2^3*Q2hatcut + 
            31*Q2hatcut^2 - 146*m2*Q2hatcut^2 + 31*m2^2*Q2hatcut^2 - 
            9*Q2hatcut^3 - 9*m2*Q2hatcut^3 - 84*Q2hatcut^4))/
          (15*Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2)/m2]) + 4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 
           3*m2^4)*Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*
                m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
             Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])]))))/
  mb^3 + (2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   46*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   796*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   796*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   46*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   2*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 40*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 204*m2^2*Q2hatcut*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   40*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 2*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 2*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   30*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 30*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 2*m2^3*Q2hatcut^2*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   8*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] + 4*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] - 8*m2^2*Q2hatcut^3*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
   8*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2] - 8*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 10*Q2hatcut^5*
    Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
   15*api*X1Q2[2, SM^2, Q2hatcut, m2, mu2hat])/15 + 
 c[SL]^2*(-3*m2^2*(3 + 8*m2 + 3*m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     23*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     398*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 398*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 
     23*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 20*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 102*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     20*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     15*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 15*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 8*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     14*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 14*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 10*Q2hatcut^5*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     20*api*X1Q2[2, c[SL]^2, Q2hatcut, m2, mu2hat])/20) + 
 c[SR]^2*(-3*m2^2*(3 + 8*m2 + 3*m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     23*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     398*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 398*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 
     23*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 20*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 102*m2^2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     20*m2^3*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     15*m2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 15*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^3*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 8*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + m2^2*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     14*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 14*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 10*Q2hatcut^5*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     20*api*X1Q2[2, c[SR]^2, Q2hatcut, m2, mu2hat])/20) + 
 c[SL]*(c[SR]*(12*m2^(3/2)*(1 + m2)*(1 + 5*m2 + m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     (3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 178*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] + 
       478*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 178*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] + 
       3*m2^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 3*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] + 67*m2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 67*m2^2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 3*m2^3*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 3*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 22*m2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 3*m2^2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 3*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 3*m2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 12*Q2hatcut^4*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 5*api*X1Q2[2, c[SL]*c[SR], Q2hatcut, m2, mu2hat])/
      5) + api*c[T]*X1Q2[2, c[SL]*c[T], Q2hatcut, m2, mu2hat]) + 
 c[T]^2*(-80*m2^2*(3 + 8*m2 + 3*m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     92*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1592*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 1592*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 
     92*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 4*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     80*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 408*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 80*m2^3*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     4*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 60*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     60*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 28*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     32*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 28*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 8*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     8*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 8*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 3*api*X1Q2[2, c[T]^2, Q2hatcut, m2, 
       mu2hat])/3) + c[VL]^2*(-8*m2^2*(3 + 8*m2 + 3*m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     46*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     796*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 796*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 
     46*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     40*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 204*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 40*m2^3*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     2*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 30*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     30*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 8*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     4*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 8*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 8*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     8*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 10*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 15*api*X1Q2[2, c[VL]^2, Q2hatcut, m2, 
       mu2hat])/15) + c[VR]*(-24*m2^(3/2)*(1 + m2)*(1 + 5*m2 + m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (-6*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     356*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 956*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 
     356*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 6*m2^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2)] - 6*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     134*m2*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 134*m2^2*Q2hatcut*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     6*m2^3*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 6*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     44*m2*Q2hatcut^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 6*m2^2*Q2hatcut^2*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] - 
     6*Q2hatcut^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] - 6*m2*Q2hatcut^3*
      Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)] + 
     24*Q2hatcut^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2)] + 5*api*X1Q2[2, SM*c[VR], Q2hatcut, m2, mu2hat])/5) + 
 c[VL]*(-16*m2^2*(3 + 8*m2 + 3*m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     92*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     1592*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 1592*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 
     92*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 4*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     80*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 408*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 80*m2^3*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     4*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 60*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     60*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 4*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 16*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     8*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 16*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 16*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     16*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 20*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 15*api*X1Q2[2, SM*c[VL], Q2hatcut, m2, 
       mu2hat])/15 + c[VR]*(-24*m2^(3/2)*(1 + m2)*(1 + 5*m2 + m2^2)*
      Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
           2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
         Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
     (-6*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 356*m2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] - 
       956*m2^2*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 356*m2^3*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] - 
       6*m2^4*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 6*Q2hatcut*Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2)] - 134*m2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 134*m2^2*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 6*m2^3*Q2hatcut*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 6*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 44*m2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 6*m2^2*Q2hatcut^2*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 6*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] - 6*m2*Q2hatcut^3*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 24*Q2hatcut^4*
        Sqrt[m2*(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2)] + 5*api*X1Q2[2, c[VL]*c[VR], Q2hatcut, m2, mu2hat])/
      5)) + c[VR]^2*(-8*m2^2*(3 + 8*m2 + 3*m2^2)*
    Log[(1 + m2 - Q2hatcut - Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 
         2*m2*(1 + Q2hatcut)])/(1 + m2 - Q2hatcut + 
       Sqrt[m2^2 + (-1 + Q2hatcut)^2 - 2*m2*(1 + Q2hatcut)])] + 
   (2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     46*m2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     796*m2^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 796*m2^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 
     46*m2^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*m2^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 2*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     40*m2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 204*m2^2*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 40*m2^3*Q2hatcut*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     2*m2^4*Q2hatcut*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 30*m2*Q2hatcut^2*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     30*m2^2*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 2*m2^3*Q2hatcut^2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 8*Q2hatcut^3*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] + 
     4*m2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] - 8*m2^2*Q2hatcut^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] - 8*Q2hatcut^4*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     8*m2*Q2hatcut^4*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 10*Q2hatcut^5*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
        2*m2*Q2hatcut + Q2hatcut^2] + 15*api*X1Q2[2, c[VR]^2, Q2hatcut, m2, 
       mu2hat])/15)
