4*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 32*m2*MBhat + 3*m2^2*MBhat - 
   27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 3*m2*MBhat^3 - 3*MBhat^4)*
  Log[m2] - 4*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 32*m2*MBhat + 
   3*m2^2*MBhat - 27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 3*m2*MBhat^3 - 
   3*MBhat^4)*Log[1 - Ycut] + 
 (mupi*(((-1 + m2 + Ycut)*(6 - 138*m2 - 2388*m2^2 - 2388*m2^3 - 138*m2^4 + 
        6*m2^5 - 67*MBhat^2 + 698*m2*MBhat^2 + 1058*m2^2*MBhat^2 - 
        1102*m2^3*MBhat^2 + 293*m2^4*MBhat^2 - 40*m2^5*MBhat^2 + 45*MBhat^4 - 
        315*m2*MBhat^4 - 315*m2^2*MBhat^4 + 45*m2^3*MBhat^4 - 30*Ycut + 
        696*m2*Ycut + 12888*m2^2*Ycut + 13380*m2^3*Ycut + 822*m2^4*Ycut - 
        36*m2^5*Ycut + 335*MBhat^2*Ycut - 3557*m2*MBhat^2*Ycut - 
        6279*m2^2*MBhat^2*Ycut + 6359*m2^3*MBhat^2*Ycut - 
        1718*m2^4*MBhat^2*Ycut + 240*m2^5*MBhat^2*Ycut - 225*MBhat^4*Ycut + 
        1620*m2*MBhat^4*Ycut + 1845*m2^2*MBhat^4*Ycut - 
        270*m2^3*MBhat^4*Ycut + 60*Ycut^2 - 1404*m2*Ycut^2 - 
        28206*m2^2*Ycut^2 - 30666*m2^3*Ycut^2 - 2034*m2^4*Ycut^2 + 
        90*m2^5*Ycut^2 - 670*MBhat^2*Ycut^2 + 7248*m2*MBhat^2*Ycut^2 + 
        15279*m2^2*MBhat^2*Ycut^2 - 15052*m2^3*MBhat^2*Ycut^2 + 
        4155*m2^4*MBhat^2*Ycut^2 - 600*m2^5*MBhat^2*Ycut^2 + 
        450*MBhat^4*Ycut^2 - 3330*m2*MBhat^4*Ycut^2 - 4455*m2^2*MBhat^4*
         Ycut^2 + 675*m2^3*MBhat^4*Ycut^2 - 60*Ycut^3 + 1416*m2*Ycut^3 + 
        31530*m2^2*Ycut^3 + 36384*m2^3*Ycut^3 + 2670*m2^4*Ycut^3 - 
        120*m2^5*Ycut^3 + 790*MBhat^2*Ycut^3 - 7382*m2*MBhat^2*Ycut^3 - 
        20003*m2^2*MBhat^2*Ycut^3 + 19445*m2^3*MBhat^2*Ycut^3 - 
        5620*m2^4*MBhat^2*Ycut^3 + 800*m2^5*MBhat^2*Ycut^3 - 
        60*MBhat^3*Ycut^3 - 180*m2*MBhat^3*Ycut^3 + 540*m2^2*MBhat^3*Ycut^3 - 
        300*m2^3*MBhat^3*Ycut^3 - 450*MBhat^4*Ycut^3 + 
        3420*m2*MBhat^4*Ycut^3 + 5625*m2^2*MBhat^4*Ycut^3 - 
        900*m2^3*MBhat^4*Ycut^3 + 30*Ycut^4 - 714*m2*Ycut^4 - 
        18324*m2^2*Ycut^4 - 22980*m2^3*Ycut^4 - 1950*m2^4*Ycut^4 + 
        90*m2^5*Ycut^4 - 120*MBhat*Ycut^4 + 720*m2^2*MBhat*Ycut^4 - 
        960*m2^3*MBhat*Ycut^4 + 360*m2^4*MBhat*Ycut^4 - 920*MBhat^2*Ycut^4 + 
        3878*m2*MBhat^2*Ycut^4 + 14745*m2^2*MBhat^2*Ycut^4 - 
        13660*m2^3*MBhat^2*Ycut^4 + 3865*m2^4*MBhat^2*Ycut^4 - 
        600*m2^5*MBhat^2*Ycut^4 + 420*MBhat^3*Ycut^4 + 
        960*m2*MBhat^3*Ycut^4 - 1380*m2^2*MBhat^3*Ycut^4 + 
        150*MBhat^4*Ycut^4 - 1830*m2*MBhat^4*Ycut^4 - 4125*m2^2*MBhat^4*
         Ycut^4 + 675*m2^3*MBhat^4*Ycut^4 + 30*Ycut^5 + 144*m2*Ycut^5 + 
        4500*m2^2*Ycut^5 + 7080*m2^3*Ycut^5 + 630*m2^4*Ycut^5 - 
        36*m2^5*Ycut^5 + 648*MBhat*Ycut^5 - 72*m2*MBhat*Ycut^5 - 
        1692*m2^2*MBhat*Ycut^5 + 1008*m2^3*MBhat*Ycut^5 + 
        108*m2^4*MBhat*Ycut^5 + 1096*MBhat^2*Ycut^5 - 
        1326*m2*MBhat^2*Ycut^5 - 6201*m2^2*MBhat^2*Ycut^5 + 
        5399*m2^3*MBhat^2*Ycut^5 - 1266*m2^4*MBhat^2*Ycut^5 + 
        240*m2^5*MBhat^2*Ycut^5 - 1170*MBhat^3*Ycut^5 - 
        2010*m2*MBhat^3*Ycut^5 + 1830*m2^2*MBhat^3*Ycut^5 + 
        630*m2^3*MBhat^3*Ycut^5 + 330*MBhat^4*Ycut^5 + 
        660*m2*MBhat^4*Ycut^5 + 1935*m2^2*MBhat^4*Ycut^5 - 
        270*m2^3*MBhat^4*Ycut^5 - 201*Ycut^6 + 15*m2*Ycut^6 + 
        285*m2^2*Ycut^6 - 615*m2^3*Ycut^6 - 210*m2^4*Ycut^6 + 6*m2^5*Ycut^6 - 
        1416*MBhat*Ycut^6 + 312*m2*MBhat*Ycut^6 + 1500*m2^2*MBhat*Ycut^6 - 
        252*m2^3*MBhat*Ycut^6 - 144*m2^4*MBhat*Ycut^6 - 575*MBhat^2*Ycut^6 + 
        1039*m2*MBhat^2*Ycut^6 + 2128*m2^2*MBhat^2*Ycut^6 - 
        1783*m2^3*MBhat^2*Ycut^6 + 71*m2^4*MBhat^2*Ycut^6 - 
        40*m2^5*MBhat^2*Ycut^6 + 1650*MBhat^3*Ycut^6 + 
        2040*m2*MBhat^3*Ycut^6 - 1890*m2^2*MBhat^3*Ycut^6 - 
        420*m2^3*MBhat^3*Ycut^6 - 750*MBhat^4*Ycut^6 - 
        450*m2*MBhat^4*Ycut^6 - 585*m2^2*MBhat^4*Ycut^6 + 
        45*m2^3*MBhat^4*Ycut^6 + 465*Ycut^7 - 60*m2*Ycut^7 - 
        315*m2^2*Ycut^7 - 210*m2^3*Ycut^7 + 30*m2^4*Ycut^7 + 
        1560*MBhat*Ycut^7 - 528*m2*MBhat*Ycut^7 - 828*m2^2*MBhat*Ycut^7 + 
        240*m2^3*MBhat*Ycut^7 + 36*m2^4*MBhat*Ycut^7 - 485*MBhat^2*Ycut^7 - 
        946*m2*MBhat^2*Ycut^7 - 798*m2^2*MBhat^2*Ycut^7 + 
        479*m2^3*MBhat^2*Ycut^7 + 40*m2^4*MBhat^2*Ycut^7 - 
        1200*MBhat^3*Ycut^7 - 960*m2*MBhat^3*Ycut^7 + 1110*m2^2*MBhat^3*
         Ycut^7 + 90*m2^3*MBhat^3*Ycut^7 + 750*MBhat^4*Ycut^7 + 
        300*m2*MBhat^4*Ycut^7 + 75*m2^2*MBhat^4*Ycut^7 - 570*Ycut^8 + 
        90*m2*Ycut^8 + 45*m2^2*Ycut^8 + 15*m2^3*Ycut^8 - 840*MBhat*Ycut^8 + 
        432*m2*MBhat*Ycut^8 + 324*m2^2*MBhat*Ycut^8 - 36*m2^3*MBhat*Ycut^8 + 
        865*MBhat^2*Ycut^8 + 414*m2*MBhat^2*Ycut^8 + 66*m2^2*MBhat^2*Ycut^8 - 
        85*m2^3*MBhat^2*Ycut^8 + 360*MBhat^3*Ycut^8 + 120*m2*MBhat^3*Ycut^8 - 
        210*m2^2*MBhat^3*Ycut^8 - 375*MBhat^4*Ycut^8 - 75*m2*MBhat^4*Ycut^8 + 
        390*Ycut^9 - 60*m2*Ycut^9 - 15*m2^2*Ycut^9 + 120*MBhat*Ycut^9 - 
        168*m2*MBhat*Ycut^9 - 24*m2^2*MBhat*Ycut^9 - 445*MBhat^2*Ycut^9 - 
        61*m2*MBhat^2*Ycut^9 + 5*m2^2*MBhat^2*Ycut^9 + 30*MBhat^3*Ycut^9 + 
        30*m2*MBhat^3*Ycut^9 + 75*MBhat^4*Ycut^9 - 141*Ycut^10 + 
        15*m2*Ycut^10 + 72*MBhat*Ycut^10 + 24*m2*MBhat*Ycut^10 + 
        71*MBhat^2*Ycut^10 - 5*m2*MBhat^2*Ycut^10 - 30*MBhat^3*Ycut^10 + 
        21*Ycut^11 - 24*MBhat*Ycut^11 + 5*MBhat^2*Ycut^11))/
      (90*(-1 + Ycut)^6) + (2*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 
        13*m2*MBhat^2 + 9*MBhat^4)*Log[m2])/3 - 
     (2*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 13*m2*MBhat^2 + 9*MBhat^4)*
       Log[1 - Ycut])/3 + c[SL]^2*
      (((-1 + m2 + Ycut)*(3 - 69*m2 - 1194*m2^2 - 1194*m2^3 - 69*m2^4 + 
          3*m2^5 - 22*MBhat^2 + 248*m2*MBhat^2 + 758*m2^2*MBhat^2 - 
          202*m2^3*MBhat^2 + 68*m2^4*MBhat^2 - 10*m2^5*MBhat^2 + 15*MBhat^4 - 
          105*m2*MBhat^4 - 105*m2^2*MBhat^4 + 15*m2^3*MBhat^4 - 15*Ycut + 
          348*m2*Ycut + 6444*m2^2*Ycut + 6690*m2^3*Ycut + 411*m2^4*Ycut - 
          18*m2^5*Ycut + 110*MBhat^2*Ycut - 1262*m2*MBhat^2*Ycut - 
          4284*m2^2*MBhat^2*Ycut + 1154*m2^3*MBhat^2*Ycut - 
          398*m2^4*MBhat^2*Ycut + 60*m2^5*MBhat^2*Ycut - 75*MBhat^4*Ycut + 
          540*m2*MBhat^4*Ycut + 615*m2^2*MBhat^4*Ycut - 90*m2^3*MBhat^4*
           Ycut + 30*Ycut^2 - 702*m2*Ycut^2 - 14103*m2^2*Ycut^2 - 
          15333*m2^3*Ycut^2 - 1017*m2^4*Ycut^2 + 45*m2^5*Ycut^2 - 
          220*MBhat^2*Ycut^2 + 2568*m2*MBhat^2*Ycut^2 + 9894*m2^2*MBhat^2*
           Ycut^2 - 2692*m2^3*MBhat^2*Ycut^2 + 960*m2^4*MBhat^2*Ycut^2 - 
          150*m2^5*MBhat^2*Ycut^2 + 150*MBhat^4*Ycut^2 - 
          1110*m2*MBhat^4*Ycut^2 - 1485*m2^2*MBhat^4*Ycut^2 + 
          225*m2^3*MBhat^4*Ycut^2 - 30*Ycut^3 + 708*m2*Ycut^3 + 
          15765*m2^2*Ycut^3 + 18192*m2^3*Ycut^3 + 1335*m2^4*Ycut^3 - 
          60*m2^5*Ycut^3 + 340*MBhat^2*Ycut^3 - 2852*m2*MBhat^2*Ycut^3 - 
          11798*m2^2*MBhat^2*Ycut^3 + 3470*m2^3*MBhat^2*Ycut^3 - 
          1330*m2^4*MBhat^2*Ycut^3 + 200*m2^5*MBhat^2*Ycut^3 - 
          100*MBhat^3*Ycut^3 + 100*m2*MBhat^3*Ycut^3 + 100*m2^2*MBhat^3*
           Ycut^3 - 100*m2^3*MBhat^3*Ycut^3 - 150*MBhat^4*Ycut^3 + 
          1140*m2*MBhat^4*Ycut^3 + 1875*m2^2*MBhat^4*Ycut^3 - 
          300*m2^3*MBhat^4*Ycut^3 + 15*Ycut^4 - 357*m2*Ycut^4 - 
          9162*m2^2*Ycut^4 - 11490*m2^3*Ycut^4 - 975*m2^4*Ycut^4 + 
          45*m2^5*Ycut^4 - 120*MBhat*Ycut^4 + 240*m2*MBhat*Ycut^4 - 
          240*m2^3*MBhat*Ycut^4 + 120*m2^4*MBhat*Ycut^4 - 
          715*MBhat^2*Ycut^4 + 2443*m2*MBhat^2*Ycut^4 + 7115*m2^2*MBhat^2*
           Ycut^4 - 2295*m2^3*MBhat^2*Ycut^4 + 910*m2^4*MBhat^2*Ycut^4 - 
          150*m2^5*MBhat^2*Ycut^4 + 620*MBhat^3*Ycut^4 - 
          480*m2*MBhat^3*Ycut^4 - 140*m2^2*MBhat^3*Ycut^4 + 
          50*MBhat^4*Ycut^4 - 610*m2*MBhat^4*Ycut^4 - 1375*m2^2*MBhat^4*
           Ycut^4 + 225*m2^3*MBhat^4*Ycut^4 + 33*Ycut^5 + 2358*m2^2*Ycut^5 + 
          3468*m2^3*Ycut^5 + 333*m2^4*Ycut^5 - 18*m2^5*Ycut^5 + 
          696*MBhat*Ycut^5 - 1164*m2*MBhat*Ycut^5 + 276*m2^2*MBhat*Ycut^5 + 
          156*m2^3*MBhat*Ycut^5 + 36*m2^4*MBhat*Ycut^5 + 
          1103*MBhat^2*Ycut^5 - 2274*m2*MBhat^2*Ycut^5 - 1459*m2^2*MBhat^2*
           Ycut^5 + 806*m2^3*MBhat^2*Ycut^5 - 294*m2^4*MBhat^2*Ycut^5 + 
          60*m2^5*MBhat^2*Ycut^5 - 1570*MBhat^3*Ycut^5 + 
          950*m2*MBhat^3*Ycut^5 + 90*m2^2*MBhat^3*Ycut^5 + 
          210*m2^3*MBhat^3*Ycut^5 + 110*MBhat^4*Ycut^5 + 
          220*m2*MBhat^4*Ycut^5 + 645*m2^2*MBhat^4*Ycut^5 - 
          90*m2^3*MBhat^4*Ycut^5 - 222*Ycut^6 + 354*m2*Ycut^6 - 
          168*m2^2*Ycut^6 - 240*m2^3*Ycut^6 - 87*m2^4*Ycut^6 + 
          3*m2^5*Ycut^6 - 1632*MBhat*Ycut^6 + 2244*m2*MBhat*Ycut^6 - 
          600*m2^2*MBhat*Ycut^6 + 36*m2^3*MBhat*Ycut^6 - 
          48*m2^4*MBhat*Ycut^6 - 540*MBhat^2*Ycut^6 + 1656*m2*MBhat^2*
           Ycut^6 - 373*m2^2*MBhat^2*Ycut^6 - 307*m2^3*MBhat^2*Ycut^6 + 
          14*m2^4*MBhat^2*Ycut^6 - 10*m2^5*MBhat^2*Ycut^6 + 
          2050*MBhat^3*Ycut^6 - 1000*m2*MBhat^3*Ycut^6 - 
          190*m2^2*MBhat^3*Ycut^6 - 140*m2^3*MBhat^3*Ycut^6 - 
          250*MBhat^4*Ycut^6 - 150*m2*MBhat^4*Ycut^6 - 195*m2^2*MBhat^4*
           Ycut^6 + 15*m2^3*MBhat^4*Ycut^6 + 570*Ycut^7 - 696*m2*Ycut^7 + 
          156*m2^2*Ycut^7 - 84*m2^3*Ycut^7 + 9*m2^4*Ycut^7 + 
          1920*MBhat*Ycut^7 - 2136*m2*MBhat*Ycut^7 + 384*m2^2*MBhat*Ycut^7 + 
          60*m2^3*MBhat*Ycut^7 + 12*m2^4*MBhat*Ycut^7 - 740*MBhat^2*Ycut^7 - 
          484*m2*MBhat^2*Ycut^7 + 223*m2^2*MBhat^2*Ycut^7 + 
          76*m2^3*MBhat^2*Ycut^7 + 10*m2^4*MBhat^2*Ycut^7 - 
          1400*MBhat^3*Ycut^7 + 600*m2*MBhat^3*Ycut^7 + 170*m2^2*MBhat^3*
           Ycut^7 + 30*m2^3*MBhat^3*Ycut^7 + 250*MBhat^4*Ycut^7 + 
          100*m2*MBhat^4*Ycut^7 + 25*m2^2*MBhat^4*Ycut^7 - 780*Ycut^8 + 
          684*m2*Ycut^8 - 105*m2^2*Ycut^8 - 9*m2^3*Ycut^8 - 
          1080*MBhat*Ycut^8 + 984*m2*MBhat*Ycut^8 - 72*m2^2*MBhat*Ycut^8 - 
          12*m2^3*MBhat*Ycut^8 + 1195*MBhat^2*Ycut^8 - 129*m2*MBhat^2*
           Ycut^8 - 86*m2^2*MBhat^2*Ycut^8 - 10*m2^3*MBhat^2*Ycut^8 + 
          400*MBhat^3*Ycut^8 - 200*m2*MBhat^3*Ycut^8 - 30*m2^2*MBhat^3*
           Ycut^8 - 125*MBhat^4*Ycut^8 - 25*m2*MBhat^4*Ycut^8 + 600*Ycut^9 - 
          336*m2*Ycut^9 + 9*m2^2*Ycut^9 + 120*MBhat*Ycut^9 - 
          156*m2*MBhat*Ycut^9 + 12*m2^2*MBhat*Ycut^9 - 615*MBhat^2*Ycut^9 + 
          96*m2*MBhat^2*Ycut^9 + 10*m2^2*MBhat^2*Ycut^9 + 30*MBhat^3*Ycut^9 + 
          30*m2*MBhat^3*Ycut^9 + 25*MBhat^4*Ycut^9 - 246*Ycut^10 + 
          66*m2*Ycut^10 + 144*MBhat*Ycut^10 - 12*m2*MBhat*Ycut^10 + 
          94*MBhat^2*Ycut^10 - 10*m2*MBhat^2*Ycut^10 - 30*MBhat^3*Ycut^10 + 
          42*Ycut^11 - 48*MBhat*Ycut^11 + 10*MBhat^2*Ycut^11))/
        (120*(-1 + Ycut)^6) + (m2^2*(9 + 24*m2 + 9*m2^2 - 12*MBhat^2 - 
          2*m2*MBhat^2 + 3*MBhat^4)*Log[m2])/2 - 
       (m2^2*(9 + 24*m2 + 9*m2^2 - 12*MBhat^2 - 2*m2*MBhat^2 + 3*MBhat^4)*
         Log[1 - Ycut])/2) + c[SR]^2*
      (((-1 + m2 + Ycut)*(3 - 69*m2 - 1194*m2^2 - 1194*m2^3 - 69*m2^4 + 
          3*m2^5 - 22*MBhat^2 + 248*m2*MBhat^2 + 758*m2^2*MBhat^2 - 
          202*m2^3*MBhat^2 + 68*m2^4*MBhat^2 - 10*m2^5*MBhat^2 + 15*MBhat^4 - 
          105*m2*MBhat^4 - 105*m2^2*MBhat^4 + 15*m2^3*MBhat^4 - 15*Ycut + 
          348*m2*Ycut + 6444*m2^2*Ycut + 6690*m2^3*Ycut + 411*m2^4*Ycut - 
          18*m2^5*Ycut + 110*MBhat^2*Ycut - 1262*m2*MBhat^2*Ycut - 
          4284*m2^2*MBhat^2*Ycut + 1154*m2^3*MBhat^2*Ycut - 
          398*m2^4*MBhat^2*Ycut + 60*m2^5*MBhat^2*Ycut - 75*MBhat^4*Ycut + 
          540*m2*MBhat^4*Ycut + 615*m2^2*MBhat^4*Ycut - 90*m2^3*MBhat^4*
           Ycut + 30*Ycut^2 - 702*m2*Ycut^2 - 14103*m2^2*Ycut^2 - 
          15333*m2^3*Ycut^2 - 1017*m2^4*Ycut^2 + 45*m2^5*Ycut^2 - 
          220*MBhat^2*Ycut^2 + 2568*m2*MBhat^2*Ycut^2 + 9894*m2^2*MBhat^2*
           Ycut^2 - 2692*m2^3*MBhat^2*Ycut^2 + 960*m2^4*MBhat^2*Ycut^2 - 
          150*m2^5*MBhat^2*Ycut^2 + 150*MBhat^4*Ycut^2 - 
          1110*m2*MBhat^4*Ycut^2 - 1485*m2^2*MBhat^4*Ycut^2 + 
          225*m2^3*MBhat^4*Ycut^2 - 30*Ycut^3 + 708*m2*Ycut^3 + 
          15765*m2^2*Ycut^3 + 18192*m2^3*Ycut^3 + 1335*m2^4*Ycut^3 - 
          60*m2^5*Ycut^3 + 340*MBhat^2*Ycut^3 - 2852*m2*MBhat^2*Ycut^3 - 
          11798*m2^2*MBhat^2*Ycut^3 + 3470*m2^3*MBhat^2*Ycut^3 - 
          1330*m2^4*MBhat^2*Ycut^3 + 200*m2^5*MBhat^2*Ycut^3 - 
          100*MBhat^3*Ycut^3 + 100*m2*MBhat^3*Ycut^3 + 100*m2^2*MBhat^3*
           Ycut^3 - 100*m2^3*MBhat^3*Ycut^3 - 150*MBhat^4*Ycut^3 + 
          1140*m2*MBhat^4*Ycut^3 + 1875*m2^2*MBhat^4*Ycut^3 - 
          300*m2^3*MBhat^4*Ycut^3 + 15*Ycut^4 - 357*m2*Ycut^4 - 
          9162*m2^2*Ycut^4 - 11490*m2^3*Ycut^4 - 975*m2^4*Ycut^4 + 
          45*m2^5*Ycut^4 - 120*MBhat*Ycut^4 + 240*m2*MBhat*Ycut^4 - 
          240*m2^3*MBhat*Ycut^4 + 120*m2^4*MBhat*Ycut^4 - 
          715*MBhat^2*Ycut^4 + 2443*m2*MBhat^2*Ycut^4 + 7115*m2^2*MBhat^2*
           Ycut^4 - 2295*m2^3*MBhat^2*Ycut^4 + 910*m2^4*MBhat^2*Ycut^4 - 
          150*m2^5*MBhat^2*Ycut^4 + 620*MBhat^3*Ycut^4 - 
          480*m2*MBhat^3*Ycut^4 - 140*m2^2*MBhat^3*Ycut^4 + 
          50*MBhat^4*Ycut^4 - 610*m2*MBhat^4*Ycut^4 - 1375*m2^2*MBhat^4*
           Ycut^4 + 225*m2^3*MBhat^4*Ycut^4 + 33*Ycut^5 + 2358*m2^2*Ycut^5 + 
          3468*m2^3*Ycut^5 + 333*m2^4*Ycut^5 - 18*m2^5*Ycut^5 + 
          696*MBhat*Ycut^5 - 1164*m2*MBhat*Ycut^5 + 276*m2^2*MBhat*Ycut^5 + 
          156*m2^3*MBhat*Ycut^5 + 36*m2^4*MBhat*Ycut^5 + 
          1103*MBhat^2*Ycut^5 - 2274*m2*MBhat^2*Ycut^5 - 1459*m2^2*MBhat^2*
           Ycut^5 + 806*m2^3*MBhat^2*Ycut^5 - 294*m2^4*MBhat^2*Ycut^5 + 
          60*m2^5*MBhat^2*Ycut^5 - 1570*MBhat^3*Ycut^5 + 
          950*m2*MBhat^3*Ycut^5 + 90*m2^2*MBhat^3*Ycut^5 + 
          210*m2^3*MBhat^3*Ycut^5 + 110*MBhat^4*Ycut^5 + 
          220*m2*MBhat^4*Ycut^5 + 645*m2^2*MBhat^4*Ycut^5 - 
          90*m2^3*MBhat^4*Ycut^5 - 222*Ycut^6 + 354*m2*Ycut^6 - 
          168*m2^2*Ycut^6 - 240*m2^3*Ycut^6 - 87*m2^4*Ycut^6 + 
          3*m2^5*Ycut^6 - 1632*MBhat*Ycut^6 + 2244*m2*MBhat*Ycut^6 - 
          600*m2^2*MBhat*Ycut^6 + 36*m2^3*MBhat*Ycut^6 - 
          48*m2^4*MBhat*Ycut^6 - 540*MBhat^2*Ycut^6 + 1656*m2*MBhat^2*
           Ycut^6 - 373*m2^2*MBhat^2*Ycut^6 - 307*m2^3*MBhat^2*Ycut^6 + 
          14*m2^4*MBhat^2*Ycut^6 - 10*m2^5*MBhat^2*Ycut^6 + 
          2050*MBhat^3*Ycut^6 - 1000*m2*MBhat^3*Ycut^6 - 
          190*m2^2*MBhat^3*Ycut^6 - 140*m2^3*MBhat^3*Ycut^6 - 
          250*MBhat^4*Ycut^6 - 150*m2*MBhat^4*Ycut^6 - 195*m2^2*MBhat^4*
           Ycut^6 + 15*m2^3*MBhat^4*Ycut^6 + 570*Ycut^7 - 696*m2*Ycut^7 + 
          156*m2^2*Ycut^7 - 84*m2^3*Ycut^7 + 9*m2^4*Ycut^7 + 
          1920*MBhat*Ycut^7 - 2136*m2*MBhat*Ycut^7 + 384*m2^2*MBhat*Ycut^7 + 
          60*m2^3*MBhat*Ycut^7 + 12*m2^4*MBhat*Ycut^7 - 740*MBhat^2*Ycut^7 - 
          484*m2*MBhat^2*Ycut^7 + 223*m2^2*MBhat^2*Ycut^7 + 
          76*m2^3*MBhat^2*Ycut^7 + 10*m2^4*MBhat^2*Ycut^7 - 
          1400*MBhat^3*Ycut^7 + 600*m2*MBhat^3*Ycut^7 + 170*m2^2*MBhat^3*
           Ycut^7 + 30*m2^3*MBhat^3*Ycut^7 + 250*MBhat^4*Ycut^7 + 
          100*m2*MBhat^4*Ycut^7 + 25*m2^2*MBhat^4*Ycut^7 - 780*Ycut^8 + 
          684*m2*Ycut^8 - 105*m2^2*Ycut^8 - 9*m2^3*Ycut^8 - 
          1080*MBhat*Ycut^8 + 984*m2*MBhat*Ycut^8 - 72*m2^2*MBhat*Ycut^8 - 
          12*m2^3*MBhat*Ycut^8 + 1195*MBhat^2*Ycut^8 - 129*m2*MBhat^2*
           Ycut^8 - 86*m2^2*MBhat^2*Ycut^8 - 10*m2^3*MBhat^2*Ycut^8 + 
          400*MBhat^3*Ycut^8 - 200*m2*MBhat^3*Ycut^8 - 30*m2^2*MBhat^3*
           Ycut^8 - 125*MBhat^4*Ycut^8 - 25*m2*MBhat^4*Ycut^8 + 600*Ycut^9 - 
          336*m2*Ycut^9 + 9*m2^2*Ycut^9 + 120*MBhat*Ycut^9 - 
          156*m2*MBhat*Ycut^9 + 12*m2^2*MBhat*Ycut^9 - 615*MBhat^2*Ycut^9 + 
          96*m2*MBhat^2*Ycut^9 + 10*m2^2*MBhat^2*Ycut^9 + 30*MBhat^3*Ycut^9 + 
          30*m2*MBhat^3*Ycut^9 + 25*MBhat^4*Ycut^9 - 246*Ycut^10 + 
          66*m2*Ycut^10 + 144*MBhat*Ycut^10 - 12*m2*MBhat*Ycut^10 + 
          94*MBhat^2*Ycut^10 - 10*m2*MBhat^2*Ycut^10 - 30*MBhat^3*Ycut^10 + 
          42*Ycut^11 - 48*MBhat*Ycut^11 + 10*MBhat^2*Ycut^11))/
        (120*(-1 + Ycut)^6) + (m2^2*(9 + 24*m2 + 9*m2^2 - 12*MBhat^2 - 
          2*m2*MBhat^2 + 3*MBhat^4)*Log[m2])/2 - 
       (m2^2*(9 + 24*m2 + 9*m2^2 - 12*MBhat^2 - 2*m2*MBhat^2 + 3*MBhat^4)*
         Log[1 - Ycut])/2) + c[VR]^2*
      (((-1 + m2 + Ycut)*(6 - 138*m2 - 2388*m2^2 - 2388*m2^3 - 138*m2^4 + 
          6*m2^5 - 67*MBhat^2 + 698*m2*MBhat^2 + 1058*m2^2*MBhat^2 - 
          1102*m2^3*MBhat^2 + 293*m2^4*MBhat^2 - 40*m2^5*MBhat^2 + 
          45*MBhat^4 - 315*m2*MBhat^4 - 315*m2^2*MBhat^4 + 45*m2^3*MBhat^4 - 
          18*Ycut + 420*m2*Ycut + 8112*m2^2*Ycut + 8604*m2^3*Ycut + 
          546*m2^4*Ycut - 24*m2^5*Ycut + 201*MBhat^2*Ycut - 
          2161*m2*MBhat^2*Ycut - 4163*m2^2*MBhat^2*Ycut + 
          4155*m2^3*MBhat^2*Ycut - 1132*m2^4*MBhat^2*Ycut + 
          160*m2^5*MBhat^2*Ycut - 135*MBhat^4*Ycut + 990*m2*MBhat^4*Ycut + 
          1215*m2^2*MBhat^4*Ycut - 180*m2^3*MBhat^4*Ycut + 18*Ycut^2 - 
          426*m2*Ycut^2 - 9594*m2^2*Ycut^2 - 11070*m2^3*Ycut^2 - 
          804*m2^4*Ycut^2 + 36*m2^5*Ycut^2 - 201*MBhat^2*Ycut^2 + 
          2228*m2*MBhat^2*Ycut^2 + 5895*m2^2*MBhat^2*Ycut^2 - 
          5640*m2^3*MBhat^2*Ycut^2 + 1598*m2^4*MBhat^2*Ycut^2 - 
          240*m2^5*MBhat^2*Ycut^2 + 135*MBhat^4*Ycut^2 - 
          1035*m2*MBhat^4*Ycut^2 - 1710*m2^2*MBhat^4*Ycut^2 + 
          270*m2^3*MBhat^4*Ycut^2 - 6*Ycut^3 + 144*m2*Ycut^3 + 
          4230*m2^2*Ycut^3 + 5640*m2^3*Ycut^3 + 516*m2^4*Ycut^3 - 
          24*m2^5*Ycut^3 + 547*MBhat^2*Ycut^3 - 2205*m2*MBhat^2*Ycut^3 - 
          1890*m2^2*MBhat^2*Ycut^3 + 2570*m2^3*MBhat^2*Ycut^3 - 
          932*m2^4*MBhat^2*Ycut^3 + 160*m2^5*MBhat^2*Ycut^3 - 
          360*MBhat^3*Ycut^3 + 720*m2*MBhat^3*Ycut^3 - 360*m2^2*MBhat^3*
           Ycut^3 - 45*MBhat^4*Ycut^3 + 360*m2*MBhat^4*Ycut^3 + 
          990*m2^2*MBhat^4*Ycut^3 - 180*m2^3*MBhat^4*Ycut^3 - 
          270*m2^2*Ycut^4 - 630*m2^3*Ycut^4 - 114*m2^4*Ycut^4 + 
          6*m2^5*Ycut^4 - 480*MBhat*Ycut^4 + 1440*m2*MBhat*Ycut^4 - 
          1440*m2^2*MBhat*Ycut^4 + 480*m2^3*MBhat*Ycut^4 - 
          1635*MBhat^2*Ycut^4 + 3540*m2*MBhat^2*Ycut^4 - 1770*m2^2*MBhat^2*
           Ycut^4 - 60*m2^3*MBhat^2*Ycut^4 + 133*m2^4*MBhat^2*Ycut^4 - 
          40*m2^5*MBhat^2*Ycut^4 + 1800*MBhat^3*Ycut^4 - 
          1800*m2*MBhat^3*Ycut^4 - 225*MBhat^4*Ycut^4 - 225*m2*MBhat^4*
           Ycut^4 - 135*m2^2*MBhat^4*Ycut^4 + 45*m2^3*MBhat^4*Ycut^4 + 
          144*Ycut^5 - 432*m2*Ycut^5 + 378*m2^2*Ycut^5 - 252*m2^3*Ycut^5 - 
          6*m2^4*Ycut^5 + 1980*MBhat*Ycut^5 - 3960*m2*MBhat*Ycut^5 + 
          1980*m2^2*MBhat*Ycut^5 + 1257*MBhat^2*Ycut^5 - 
          2463*m2*MBhat^2*Ycut^5 + 1167*m2^2*MBhat^2*Ycut^5 + 
          27*m2^3*MBhat^2*Ycut^5 + 40*m2^4*MBhat^2*Ycut^5 - 
          3060*MBhat^3*Ycut^5 + 1620*m2*MBhat^3*Ycut^5 + 
          720*m2^2*MBhat^3*Ycut^5 + 675*MBhat^4*Ycut^5 + 
          450*m2*MBhat^4*Ycut^5 - 45*m2^2*MBhat^4*Ycut^5 - 642*Ycut^6 + 
          1230*m2*Ycut^6 - 552*m2^2*Ycut^6 - 84*m2^3*Ycut^6 - 
          2820*MBhat*Ycut^6 + 3540*m2*MBhat*Ycut^6 - 600*m2^2*MBhat*Ycut^6 - 
          120*m2^3*MBhat*Ycut^6 + 1189*MBhat^2*Ycut^6 + 
          16*m2*MBhat^2*Ycut^6 - 347*m2^2*MBhat^2*Ycut^6 - 
          130*m2^3*MBhat^2*Ycut^6 + 1980*MBhat^3*Ycut^6 - 
          720*m2*MBhat^3*Ycut^6 - 360*m2^2*MBhat^3*Ycut^6 - 
          675*MBhat^4*Ycut^6 - 225*m2*MBhat^4*Ycut^6 + 1062*Ycut^7 - 
          1164*m2*Ycut^7 + 84*m2^2*Ycut^7 + 1380*MBhat*Ycut^7 - 
          960*m2*MBhat*Ycut^7 + 60*m2^2*MBhat*Ycut^7 - 1959*MBhat^2*Ycut^7 + 
          397*m2*MBhat^2*Ycut^7 + 50*m2^2*MBhat^2*Ycut^7 - 
          180*MBhat^3*Ycut^7 + 180*m2*MBhat^3*Ycut^7 + 225*MBhat^4*Ycut^7 - 
          774*Ycut^8 + 366*m2*Ycut^8 + 180*MBhat*Ycut^8 - 
          60*m2*MBhat*Ycut^8 + 618*MBhat^2*Ycut^8 - 50*m2*MBhat^2*Ycut^8 - 
          180*MBhat^3*Ycut^8 + 210*Ycut^9 - 240*MBhat*Ycut^9 + 
          50*MBhat^2*Ycut^9))/(90*(-1 + Ycut)^4) + 
       (2*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 13*m2*MBhat^2 + 
          9*MBhat^4)*Log[m2])/3 - (2*m2^2*(18 + 48*m2 + 18*m2^2 - 
          27*MBhat^2 + 13*m2*MBhat^2 + 9*MBhat^4)*Log[1 - Ycut])/3) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(6 - 138*m2 - 2388*m2^2 - 2388*m2^3 - 
          138*m2^4 + 6*m2^5 - 67*MBhat^2 + 698*m2*MBhat^2 + 
          1058*m2^2*MBhat^2 - 1102*m2^3*MBhat^2 + 293*m2^4*MBhat^2 - 
          40*m2^5*MBhat^2 + 45*MBhat^4 - 315*m2*MBhat^4 - 315*m2^2*MBhat^4 + 
          45*m2^3*MBhat^4 - 30*Ycut + 696*m2*Ycut + 12888*m2^2*Ycut + 
          13380*m2^3*Ycut + 822*m2^4*Ycut - 36*m2^5*Ycut + 335*MBhat^2*Ycut - 
          3557*m2*MBhat^2*Ycut - 6279*m2^2*MBhat^2*Ycut + 
          6359*m2^3*MBhat^2*Ycut - 1718*m2^4*MBhat^2*Ycut + 
          240*m2^5*MBhat^2*Ycut - 225*MBhat^4*Ycut + 1620*m2*MBhat^4*Ycut + 
          1845*m2^2*MBhat^4*Ycut - 270*m2^3*MBhat^4*Ycut + 60*Ycut^2 - 
          1404*m2*Ycut^2 - 28206*m2^2*Ycut^2 - 30666*m2^3*Ycut^2 - 
          2034*m2^4*Ycut^2 + 90*m2^5*Ycut^2 - 670*MBhat^2*Ycut^2 + 
          7248*m2*MBhat^2*Ycut^2 + 15279*m2^2*MBhat^2*Ycut^2 - 
          15052*m2^3*MBhat^2*Ycut^2 + 4155*m2^4*MBhat^2*Ycut^2 - 
          600*m2^5*MBhat^2*Ycut^2 + 450*MBhat^4*Ycut^2 - 
          3330*m2*MBhat^4*Ycut^2 - 4455*m2^2*MBhat^4*Ycut^2 + 
          675*m2^3*MBhat^4*Ycut^2 - 60*Ycut^3 + 1416*m2*Ycut^3 + 
          31530*m2^2*Ycut^3 + 36384*m2^3*Ycut^3 + 2670*m2^4*Ycut^3 - 
          120*m2^5*Ycut^3 + 790*MBhat^2*Ycut^3 - 7382*m2*MBhat^2*Ycut^3 - 
          20003*m2^2*MBhat^2*Ycut^3 + 19445*m2^3*MBhat^2*Ycut^3 - 
          5620*m2^4*MBhat^2*Ycut^3 + 800*m2^5*MBhat^2*Ycut^3 - 
          60*MBhat^3*Ycut^3 - 180*m2*MBhat^3*Ycut^3 + 540*m2^2*MBhat^3*
           Ycut^3 - 300*m2^3*MBhat^3*Ycut^3 - 450*MBhat^4*Ycut^3 + 
          3420*m2*MBhat^4*Ycut^3 + 5625*m2^2*MBhat^4*Ycut^3 - 
          900*m2^3*MBhat^4*Ycut^3 + 30*Ycut^4 - 714*m2*Ycut^4 - 
          18324*m2^2*Ycut^4 - 22980*m2^3*Ycut^4 - 1950*m2^4*Ycut^4 + 
          90*m2^5*Ycut^4 - 120*MBhat*Ycut^4 + 720*m2^2*MBhat*Ycut^4 - 
          960*m2^3*MBhat*Ycut^4 + 360*m2^4*MBhat*Ycut^4 - 
          920*MBhat^2*Ycut^4 + 3878*m2*MBhat^2*Ycut^4 + 14745*m2^2*MBhat^2*
           Ycut^4 - 13660*m2^3*MBhat^2*Ycut^4 + 3865*m2^4*MBhat^2*Ycut^4 - 
          600*m2^5*MBhat^2*Ycut^4 + 420*MBhat^3*Ycut^4 + 
          960*m2*MBhat^3*Ycut^4 - 1380*m2^2*MBhat^3*Ycut^4 + 
          150*MBhat^4*Ycut^4 - 1830*m2*MBhat^4*Ycut^4 - 4125*m2^2*MBhat^4*
           Ycut^4 + 675*m2^3*MBhat^4*Ycut^4 + 30*Ycut^5 + 144*m2*Ycut^5 + 
          4500*m2^2*Ycut^5 + 7080*m2^3*Ycut^5 + 630*m2^4*Ycut^5 - 
          36*m2^5*Ycut^5 + 648*MBhat*Ycut^5 - 72*m2*MBhat*Ycut^5 - 
          1692*m2^2*MBhat*Ycut^5 + 1008*m2^3*MBhat*Ycut^5 + 
          108*m2^4*MBhat*Ycut^5 + 1096*MBhat^2*Ycut^5 - 1326*m2*MBhat^2*
           Ycut^5 - 6201*m2^2*MBhat^2*Ycut^5 + 5399*m2^3*MBhat^2*Ycut^5 - 
          1266*m2^4*MBhat^2*Ycut^5 + 240*m2^5*MBhat^2*Ycut^5 - 
          1170*MBhat^3*Ycut^5 - 2010*m2*MBhat^3*Ycut^5 + 1830*m2^2*MBhat^3*
           Ycut^5 + 630*m2^3*MBhat^3*Ycut^5 + 330*MBhat^4*Ycut^5 + 
          660*m2*MBhat^4*Ycut^5 + 1935*m2^2*MBhat^4*Ycut^5 - 
          270*m2^3*MBhat^4*Ycut^5 - 201*Ycut^6 + 15*m2*Ycut^6 + 
          285*m2^2*Ycut^6 - 615*m2^3*Ycut^6 - 210*m2^4*Ycut^6 + 
          6*m2^5*Ycut^6 - 1416*MBhat*Ycut^6 + 312*m2*MBhat*Ycut^6 + 
          1500*m2^2*MBhat*Ycut^6 - 252*m2^3*MBhat*Ycut^6 - 
          144*m2^4*MBhat*Ycut^6 - 575*MBhat^2*Ycut^6 + 1039*m2*MBhat^2*
           Ycut^6 + 2128*m2^2*MBhat^2*Ycut^6 - 1783*m2^3*MBhat^2*Ycut^6 + 
          71*m2^4*MBhat^2*Ycut^6 - 40*m2^5*MBhat^2*Ycut^6 + 
          1650*MBhat^3*Ycut^6 + 2040*m2*MBhat^3*Ycut^6 - 1890*m2^2*MBhat^3*
           Ycut^6 - 420*m2^3*MBhat^3*Ycut^6 - 750*MBhat^4*Ycut^6 - 
          450*m2*MBhat^4*Ycut^6 - 585*m2^2*MBhat^4*Ycut^6 + 
          45*m2^3*MBhat^4*Ycut^6 + 465*Ycut^7 - 60*m2*Ycut^7 - 
          315*m2^2*Ycut^7 - 210*m2^3*Ycut^7 + 30*m2^4*Ycut^7 + 
          1560*MBhat*Ycut^7 - 528*m2*MBhat*Ycut^7 - 828*m2^2*MBhat*Ycut^7 + 
          240*m2^3*MBhat*Ycut^7 + 36*m2^4*MBhat*Ycut^7 - 485*MBhat^2*Ycut^7 - 
          946*m2*MBhat^2*Ycut^7 - 798*m2^2*MBhat^2*Ycut^7 + 
          479*m2^3*MBhat^2*Ycut^7 + 40*m2^4*MBhat^2*Ycut^7 - 
          1200*MBhat^3*Ycut^7 - 960*m2*MBhat^3*Ycut^7 + 1110*m2^2*MBhat^3*
           Ycut^7 + 90*m2^3*MBhat^3*Ycut^7 + 750*MBhat^4*Ycut^7 + 
          300*m2*MBhat^4*Ycut^7 + 75*m2^2*MBhat^4*Ycut^7 - 570*Ycut^8 + 
          90*m2*Ycut^8 + 45*m2^2*Ycut^8 + 15*m2^3*Ycut^8 - 840*MBhat*Ycut^8 + 
          432*m2*MBhat*Ycut^8 + 324*m2^2*MBhat*Ycut^8 - 
          36*m2^3*MBhat*Ycut^8 + 865*MBhat^2*Ycut^8 + 414*m2*MBhat^2*Ycut^8 + 
          66*m2^2*MBhat^2*Ycut^8 - 85*m2^3*MBhat^2*Ycut^8 + 
          360*MBhat^3*Ycut^8 + 120*m2*MBhat^3*Ycut^8 - 210*m2^2*MBhat^3*
           Ycut^8 - 375*MBhat^4*Ycut^8 - 75*m2*MBhat^4*Ycut^8 + 390*Ycut^9 - 
          60*m2*Ycut^9 - 15*m2^2*Ycut^9 + 120*MBhat*Ycut^9 - 
          168*m2*MBhat*Ycut^9 - 24*m2^2*MBhat*Ycut^9 - 445*MBhat^2*Ycut^9 - 
          61*m2*MBhat^2*Ycut^9 + 5*m2^2*MBhat^2*Ycut^9 + 30*MBhat^3*Ycut^9 + 
          30*m2*MBhat^3*Ycut^9 + 75*MBhat^4*Ycut^9 - 141*Ycut^10 + 
          15*m2*Ycut^10 + 72*MBhat*Ycut^10 + 24*m2*MBhat*Ycut^10 + 
          71*MBhat^2*Ycut^10 - 5*m2*MBhat^2*Ycut^10 - 30*MBhat^3*Ycut^10 + 
          21*Ycut^11 - 24*MBhat*Ycut^11 + 5*MBhat^2*Ycut^11))/
        (90*(-1 + Ycut)^6) + (2*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 
          13*m2*MBhat^2 + 9*MBhat^4)*Log[m2])/3 - 
       (2*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 13*m2*MBhat^2 + 
          9*MBhat^4)*Log[1 - Ycut])/3) + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(15 - 345*m2 - 5970*m2^2 - 5970*m2^3 - 
          345*m2^4 + 15*m2^5 - 202*MBhat^2 + 2048*m2*MBhat^2 + 
          1958*m2^2*MBhat^2 - 3802*m2^3*MBhat^2 + 968*m2^4*MBhat^2 - 
          130*m2^5*MBhat^2 + 135*MBhat^4 - 945*m2*MBhat^4 - 
          945*m2^2*MBhat^4 + 135*m2^3*MBhat^4 - 75*Ycut + 1740*m2*Ycut + 
          32220*m2^2*Ycut + 33450*m2^3*Ycut + 2055*m2^4*Ycut - 90*m2^5*Ycut + 
          1010*MBhat^2*Ycut - 10442*m2*MBhat^2*Ycut - 12264*m2^2*MBhat^2*
           Ycut + 21974*m2^3*MBhat^2*Ycut - 5678*m2^4*MBhat^2*Ycut + 
          780*m2^5*MBhat^2*Ycut - 675*MBhat^4*Ycut + 4860*m2*MBhat^4*Ycut + 
          5535*m2^2*MBhat^4*Ycut - 810*m2^3*MBhat^4*Ycut + 150*Ycut^2 - 
          3510*m2*Ycut^2 - 70515*m2^2*Ycut^2 - 76665*m2^3*Ycut^2 - 
          5085*m2^4*Ycut^2 + 225*m2^5*Ycut^2 - 2020*MBhat^2*Ycut^2 + 
          21288*m2*MBhat^2*Ycut^2 + 31434*m2^2*MBhat^2*Ycut^2 - 
          52132*m2^3*MBhat^2*Ycut^2 + 13740*m2^4*MBhat^2*Ycut^2 - 
          1950*m2^5*MBhat^2*Ycut^2 + 1350*MBhat^4*Ycut^2 - 
          9990*m2*MBhat^4*Ycut^2 - 13365*m2^2*MBhat^4*Ycut^2 + 
          2025*m2^3*MBhat^4*Ycut^2 - 150*Ycut^3 + 3540*m2*Ycut^3 + 
          78825*m2^2*Ycut^3 + 90960*m2^3*Ycut^3 + 6675*m2^4*Ycut^3 - 
          300*m2^5*Ycut^3 + 2860*MBhat^2*Ycut^3 - 23852*m2*MBhat^2*Ycut^3 - 
          40298*m2^2*MBhat^2*Ycut^3 + 64490*m2^3*MBhat^2*Ycut^3 - 
          17770*m2^4*MBhat^2*Ycut^3 + 2600*m2^5*MBhat^2*Ycut^3 - 
          540*MBhat^3*Ycut^3 + 780*m2*MBhat^3*Ycut^3 + 60*m2^2*MBhat^3*
           Ycut^3 - 300*m2^3*MBhat^3*Ycut^3 - 1350*MBhat^4*Ycut^3 + 
          10260*m2*MBhat^4*Ycut^3 + 16875*m2^2*MBhat^4*Ycut^3 - 
          2700*m2^3*MBhat^4*Ycut^3 + 75*Ycut^4 - 1785*m2*Ycut^4 - 
          45810*m2^2*Ycut^4 - 57450*m2^3*Ycut^4 - 4875*m2^4*Ycut^4 + 
          225*m2^5*Ycut^4 - 840*MBhat*Ycut^4 + 2160*m2*MBhat*Ycut^4 - 
          1440*m2^2*MBhat*Ycut^4 - 240*m2^3*MBhat*Ycut^4 + 
          360*m2^4*MBhat*Ycut^4 - 5555*MBhat^2*Ycut^4 + 20783*m2*MBhat^2*
           Ycut^4 + 23955*m2^2*MBhat^2*Ycut^4 - 42115*m2^3*MBhat^2*Ycut^4 + 
          12190*m2^4*MBhat^2*Ycut^4 - 1950*m2^5*MBhat^2*Ycut^4 + 
          4020*MBhat^3*Ycut^4 - 3120*m2*MBhat^3*Ycut^4 - 
          900*m2^2*MBhat^3*Ycut^4 + 150*MBhat^4*Ycut^4 - 
          5790*m2*MBhat^4*Ycut^4 - 11775*m2^2*MBhat^4*Ycut^4 + 
          2025*m2^3*MBhat^4*Ycut^4 + 237*Ycut^5 - 288*m2*Ycut^5 + 
          12222*m2^2*Ycut^5 + 17052*m2^3*Ycut^5 + 1737*m2^4*Ycut^5 - 
          90*m2^5*Ycut^5 + 5088*MBhat*Ycut^5 - 10332*m2*MBhat*Ycut^5 + 
          5508*m2^2*MBhat*Ycut^5 - 372*m2^3*MBhat*Ycut^5 + 
          108*m2^4*MBhat*Ycut^5 + 9031*MBhat^2*Ycut^5 - 19326*m2*MBhat^2*
           Ycut^5 - 2391*m2^2*MBhat^2*Ycut^5 + 13814*m2^3*MBhat^2*Ycut^5 - 
          3966*m2^4*MBhat^2*Ycut^5 + 780*m2^5*MBhat^2*Ycut^5 - 
          11670*MBhat^3*Ycut^5 + 5010*m2*MBhat^3*Ycut^5 + 
          4110*m2^2*MBhat^3*Ycut^5 + 630*m2^3*MBhat^3*Ycut^5 + 
          2490*MBhat^4*Ycut^5 + 3180*m2*MBhat^4*Ycut^5 + 4365*m2^2*MBhat^4*
           Ycut^5 - 810*m2^3*MBhat^4*Ycut^5 - 1596*Ycut^6 + 3156*m2*Ycut^6 - 
          2082*m2^2*Ycut^6 - 930*m2^3*Ycut^6 - 363*m2^4*Ycut^6 + 
          15*m2^5*Ycut^6 - 12456*MBhat*Ycut^6 + 19692*m2*MBhat*Ycut^6 - 
          7200*m2^2*MBhat*Ycut^6 + 108*m2^3*MBhat*Ycut^6 - 
          144*m2^4*MBhat*Ycut^6 - 5450*MBhat^2*Ycut^6 + 14074*m2*MBhat^2*
           Ycut^6 - 3527*m2^2*MBhat^2*Ycut^6 - 3133*m2^3*MBhat^2*Ycut^6 + 
          206*m2^4*MBhat^2*Ycut^6 - 130*m2^5*MBhat^2*Ycut^6 + 
          16950*MBhat^3*Ycut^6 - 4440*m2*MBhat^3*Ycut^6 - 
          6810*m2^2*MBhat^3*Ycut^6 - 420*m2^3*MBhat^3*Ycut^6 - 
          5250*MBhat^4*Ycut^6 - 3150*m2*MBhat^4*Ycut^6 - 
          675*m2^2*MBhat^4*Ycut^6 + 135*m2^3*MBhat^4*Ycut^6 + 4200*Ycut^7 - 
          6144*m2*Ycut^7 + 2034*m2^2*Ycut^7 - 336*m2^3*Ycut^7 + 
          21*m2^4*Ycut^7 + 15360*MBhat*Ycut^7 - 18648*m2*MBhat*Ycut^7 + 
          3672*m2^2*MBhat*Ycut^7 + 780*m2^3*MBhat*Ycut^7 + 
          36*m2^4*MBhat*Ycut^7 - 4910*MBhat^2*Ycut^7 - 4636*m2*MBhat^2*
           Ycut^7 + 1557*m2^2*MBhat^2*Ycut^7 + 1304*m2^3*MBhat^2*Ycut^7 + 
          130*m2^4*MBhat^2*Ycut^7 - 12600*MBhat^3*Ycut^7 + 
          2760*m2*MBhat^3*Ycut^7 + 4590*m2^2*MBhat^3*Ycut^7 + 
          90*m2^3*MBhat^3*Ycut^7 + 5250*MBhat^4*Ycut^7 + 
          2100*m2*MBhat^4*Ycut^7 - 15*m2^2*MBhat^4*Ycut^7 - 5880*Ycut^8 + 
          5976*m2*Ycut^8 - 1035*m2^2*Ycut^8 - 111*m2^3*Ycut^8 - 
          9240*MBhat*Ycut^8 + 8712*m2*MBhat*Ycut^8 - 576*m2^2*MBhat*Ycut^8 - 
          276*m2^3*MBhat*Ycut^8 + 9595*MBhat^2*Ycut^8 - 441*m2*MBhat^2*
           Ycut^8 - 504*m2^2*MBhat^2*Ycut^8 - 400*m2^3*MBhat^2*Ycut^8 + 
          3840*MBhat^3*Ycut^8 - 1320*m2*MBhat^3*Ycut^8 - 1050*m2^2*MBhat^3*
           Ycut^8 - 2625*MBhat^4*Ycut^8 - 525*m2*MBhat^4*Ycut^8 + 
          4620*Ycut^9 - 2904*m2*Ycut^9 + 111*m2^2*Ycut^9 + 
          1440*MBhat*Ycut^9 - 1548*m2*MBhat*Ycut^9 + 36*m2^2*MBhat*Ycut^9 - 
          5335*MBhat^2*Ycut^9 + 584*m2*MBhat^2*Ycut^9 + 80*m2^2*MBhat^2*
           Ycut^9 + 330*MBhat^3*Ycut^9 + 330*m2*MBhat^3*Ycut^9 + 
          525*MBhat^4*Ycut^9 - 1932*Ycut^10 + 564*m2*Ycut^10 + 
          1032*MBhat*Ycut^10 - 36*m2*MBhat*Ycut^10 + 896*MBhat^2*Ycut^10 - 
          80*m2*MBhat^2*Ycut^10 - 330*MBhat^3*Ycut^10 + 336*Ycut^11 - 
          384*MBhat*Ycut^11 + 80*MBhat^2*Ycut^11))/(45*(-1 + Ycut)^6) + 
       (8*m2^2*(45 + 120*m2 + 45*m2^2 - 72*MBhat^2 + 58*m2*MBhat^2 + 
          27*MBhat^4)*Log[m2])/3 - (8*m2^2*(45 + 120*m2 + 45*m2^2 - 
          72*MBhat^2 + 58*m2*MBhat^2 + 27*MBhat^4)*Log[1 - Ycut])/3) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(9 + 534*m2 + 1434*m2^2 + 534*m2^3 + 
          9*m2^4 - 45*MBhat^2 - 660*m2*MBhat^2 + 540*m2^2*MBhat^2 + 
          180*m2^3*MBhat^2 - 15*m2^4*MBhat^2 + 30*MBhat^4 + 300*m2*MBhat^4 + 
          30*m2^2*MBhat^4 - 36*Ycut - 2307*m2*Ycut - 6453*m2^2*Ycut - 
          2499*m2^3*Ycut - 45*m2^4*Ycut + 180*MBhat^2*Ycut + 
          2955*m2*MBhat^2*Ycut - 2325*m2^2*MBhat^2*Ycut - 
          885*m2^3*MBhat^2*Ycut + 75*m2^4*MBhat^2*Ycut - 120*MBhat^4*Ycut - 
          1350*m2*MBhat^4*Ycut - 150*m2^2*MBhat^4*Ycut + 54*Ycut^2 + 
          3807*m2*Ycut^2 + 11214*m2^2*Ycut^2 + 4575*m2^3*Ycut^2 + 
          90*m2^4*Ycut^2 - 270*MBhat^2*Ycut^2 - 5085*m2*MBhat^2*Ycut^2 + 
          3780*m2^2*MBhat^2*Ycut^2 + 1725*m2^3*MBhat^2*Ycut^2 - 
          150*m2^4*MBhat^2*Ycut^2 + 180*MBhat^4*Ycut^2 + 
          2340*m2*MBhat^4*Ycut^2 + 300*m2^2*MBhat^4*Ycut^2 - 36*Ycut^3 - 
          2889*m2*Ycut^3 - 9135*m2^2*Ycut^3 - 4020*m2^3*Ycut^3 - 
          90*m2^4*Ycut^3 + 300*MBhat^2*Ycut^3 + 3705*m2*MBhat^2*Ycut^3 - 
          2385*m2^2*MBhat^2*Ycut^3 - 1770*m2^3*MBhat^2*Ycut^3 + 
          150*m2^4*MBhat^2*Ycut^3 - 100*MBhat^3*Ycut^3 + 
          200*m2*MBhat^3*Ycut^3 - 100*m2^2*MBhat^3*Ycut^3 - 
          120*MBhat^4*Ycut^3 - 1890*m2*MBhat^4*Ycut^3 - 300*m2^2*MBhat^4*
           Ycut^3 + 9*Ycut^4 + 900*m2*Ycut^4 + 3195*m2^2*Ycut^4 + 
          1605*m2^3*Ycut^4 + 45*m2^4*Ycut^4 - 120*MBhat*Ycut^4 + 
          360*m2*MBhat*Ycut^4 - 360*m2^2*MBhat*Ycut^4 + 120*m2^3*MBhat*
           Ycut^4 - 425*MBhat^2*Ycut^4 - 530*m2*MBhat^2*Ycut^4 + 
          205*m2^2*MBhat^2*Ycut^4 + 825*m2^3*MBhat^2*Ycut^4 - 
          75*m2^4*MBhat^2*Ycut^4 + 400*MBhat^3*Ycut^4 - 420*m2*MBhat^3*
           Ycut^4 + 20*m2^2*MBhat^3*Ycut^4 + 30*MBhat^4*Ycut^4 + 
          570*m2*MBhat^4*Ycut^4 + 150*m2^2*MBhat^4*Ycut^4 + 36*Ycut^5 - 
          144*m2*Ycut^5 - 99*m2^2*Ycut^5 - 204*m2^3*Ycut^5 - 9*m2^4*Ycut^5 + 
          450*MBhat*Ycut^5 - 870*m2*MBhat*Ycut^5 + 390*m2^2*MBhat*Ycut^5 + 
          30*m2^3*MBhat*Ycut^5 + 320*MBhat^2*Ycut^5 - 600*m2*MBhat^2*Ycut^5 + 
          385*m2^2*MBhat^2*Ycut^5 - 120*m2^3*MBhat^2*Ycut^5 + 
          15*m2^4*MBhat^2*Ycut^5 - 600*MBhat^3*Ycut^5 + 300*m2*MBhat^3*
           Ycut^5 + 140*m2^2*MBhat^3*Ycut^5 + 120*m2*MBhat^4*Ycut^5 - 
          30*m2^2*MBhat^4*Ycut^5 - 144*Ycut^6 + 252*m2*Ycut^6 - 
          117*m2^2*Ycut^6 - 51*m2^3*Ycut^6 - 600*MBhat*Ycut^6 + 
          690*m2*MBhat*Ycut^6 - 60*m2^2*MBhat*Ycut^6 - 30*m2^3*MBhat*Ycut^6 + 
          120*MBhat^2*Ycut^6 + 270*m2*MBhat^2*Ycut^6 - 215*m2^2*MBhat^2*
           Ycut^6 - 15*m2^3*MBhat^2*Ycut^6 + 400*MBhat^3*Ycut^6 - 
          140*m2*MBhat^3*Ycut^6 - 60*m2^2*MBhat^3*Ycut^6 - 
          90*m2*MBhat^4*Ycut^6 + 216*Ycut^7 - 192*m2*Ycut^7 - 
          39*m2^2*Ycut^7 + 300*MBhat*Ycut^7 - 210*m2*MBhat*Ycut^7 + 
          30*m2^2*MBhat*Ycut^7 - 280*MBhat^2*Ycut^7 - 40*m2*MBhat^2*Ycut^7 + 
          15*m2^2*MBhat^2*Ycut^7 - 100*MBhat^3*Ycut^7 + 
          60*m2*MBhat^3*Ycut^7 - 144*Ycut^8 + 39*m2*Ycut^8 + 
          30*m2*MBhat*Ycut^8 + 100*MBhat^2*Ycut^8 - 15*m2*MBhat^2*Ycut^8 + 
          36*Ycut^9 - 30*MBhat*Ycut^9))/(15*(-1 + Ycut)^5) + 
       12*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 2*MBhat^2 - m2*MBhat^2 + 
         3*m2^2*MBhat^2 + MBhat^4 + m2*MBhat^4)*Log[m2] - 
       12*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 2*MBhat^2 - m2*MBhat^2 + 
         3*m2^2*MBhat^2 + MBhat^4 + m2*MBhat^4)*Log[1 - Ycut]) + 
     c[VL]*(((-1 + m2 + Ycut)*(6 - 138*m2 - 2388*m2^2 - 2388*m2^3 - 
          138*m2^4 + 6*m2^5 - 67*MBhat^2 + 698*m2*MBhat^2 + 
          1058*m2^2*MBhat^2 - 1102*m2^3*MBhat^2 + 293*m2^4*MBhat^2 - 
          40*m2^5*MBhat^2 + 45*MBhat^4 - 315*m2*MBhat^4 - 315*m2^2*MBhat^4 + 
          45*m2^3*MBhat^4 - 30*Ycut + 696*m2*Ycut + 12888*m2^2*Ycut + 
          13380*m2^3*Ycut + 822*m2^4*Ycut - 36*m2^5*Ycut + 335*MBhat^2*Ycut - 
          3557*m2*MBhat^2*Ycut - 6279*m2^2*MBhat^2*Ycut + 
          6359*m2^3*MBhat^2*Ycut - 1718*m2^4*MBhat^2*Ycut + 
          240*m2^5*MBhat^2*Ycut - 225*MBhat^4*Ycut + 1620*m2*MBhat^4*Ycut + 
          1845*m2^2*MBhat^4*Ycut - 270*m2^3*MBhat^4*Ycut + 60*Ycut^2 - 
          1404*m2*Ycut^2 - 28206*m2^2*Ycut^2 - 30666*m2^3*Ycut^2 - 
          2034*m2^4*Ycut^2 + 90*m2^5*Ycut^2 - 670*MBhat^2*Ycut^2 + 
          7248*m2*MBhat^2*Ycut^2 + 15279*m2^2*MBhat^2*Ycut^2 - 
          15052*m2^3*MBhat^2*Ycut^2 + 4155*m2^4*MBhat^2*Ycut^2 - 
          600*m2^5*MBhat^2*Ycut^2 + 450*MBhat^4*Ycut^2 - 
          3330*m2*MBhat^4*Ycut^2 - 4455*m2^2*MBhat^4*Ycut^2 + 
          675*m2^3*MBhat^4*Ycut^2 - 60*Ycut^3 + 1416*m2*Ycut^3 + 
          31530*m2^2*Ycut^3 + 36384*m2^3*Ycut^3 + 2670*m2^4*Ycut^3 - 
          120*m2^5*Ycut^3 + 790*MBhat^2*Ycut^3 - 7382*m2*MBhat^2*Ycut^3 - 
          20003*m2^2*MBhat^2*Ycut^3 + 19445*m2^3*MBhat^2*Ycut^3 - 
          5620*m2^4*MBhat^2*Ycut^3 + 800*m2^5*MBhat^2*Ycut^3 - 
          60*MBhat^3*Ycut^3 - 180*m2*MBhat^3*Ycut^3 + 540*m2^2*MBhat^3*
           Ycut^3 - 300*m2^3*MBhat^3*Ycut^3 - 450*MBhat^4*Ycut^3 + 
          3420*m2*MBhat^4*Ycut^3 + 5625*m2^2*MBhat^4*Ycut^3 - 
          900*m2^3*MBhat^4*Ycut^3 + 30*Ycut^4 - 714*m2*Ycut^4 - 
          18324*m2^2*Ycut^4 - 22980*m2^3*Ycut^4 - 1950*m2^4*Ycut^4 + 
          90*m2^5*Ycut^4 - 120*MBhat*Ycut^4 + 720*m2^2*MBhat*Ycut^4 - 
          960*m2^3*MBhat*Ycut^4 + 360*m2^4*MBhat*Ycut^4 - 
          920*MBhat^2*Ycut^4 + 3878*m2*MBhat^2*Ycut^4 + 14745*m2^2*MBhat^2*
           Ycut^4 - 13660*m2^3*MBhat^2*Ycut^4 + 3865*m2^4*MBhat^2*Ycut^4 - 
          600*m2^5*MBhat^2*Ycut^4 + 420*MBhat^3*Ycut^4 + 
          960*m2*MBhat^3*Ycut^4 - 1380*m2^2*MBhat^3*Ycut^4 + 
          150*MBhat^4*Ycut^4 - 1830*m2*MBhat^4*Ycut^4 - 4125*m2^2*MBhat^4*
           Ycut^4 + 675*m2^3*MBhat^4*Ycut^4 + 30*Ycut^5 + 144*m2*Ycut^5 + 
          4500*m2^2*Ycut^5 + 7080*m2^3*Ycut^5 + 630*m2^4*Ycut^5 - 
          36*m2^5*Ycut^5 + 648*MBhat*Ycut^5 - 72*m2*MBhat*Ycut^5 - 
          1692*m2^2*MBhat*Ycut^5 + 1008*m2^3*MBhat*Ycut^5 + 
          108*m2^4*MBhat*Ycut^5 + 1096*MBhat^2*Ycut^5 - 1326*m2*MBhat^2*
           Ycut^5 - 6201*m2^2*MBhat^2*Ycut^5 + 5399*m2^3*MBhat^2*Ycut^5 - 
          1266*m2^4*MBhat^2*Ycut^5 + 240*m2^5*MBhat^2*Ycut^5 - 
          1170*MBhat^3*Ycut^5 - 2010*m2*MBhat^3*Ycut^5 + 1830*m2^2*MBhat^3*
           Ycut^5 + 630*m2^3*MBhat^3*Ycut^5 + 330*MBhat^4*Ycut^5 + 
          660*m2*MBhat^4*Ycut^5 + 1935*m2^2*MBhat^4*Ycut^5 - 
          270*m2^3*MBhat^4*Ycut^5 - 201*Ycut^6 + 15*m2*Ycut^6 + 
          285*m2^2*Ycut^6 - 615*m2^3*Ycut^6 - 210*m2^4*Ycut^6 + 
          6*m2^5*Ycut^6 - 1416*MBhat*Ycut^6 + 312*m2*MBhat*Ycut^6 + 
          1500*m2^2*MBhat*Ycut^6 - 252*m2^3*MBhat*Ycut^6 - 
          144*m2^4*MBhat*Ycut^6 - 575*MBhat^2*Ycut^6 + 1039*m2*MBhat^2*
           Ycut^6 + 2128*m2^2*MBhat^2*Ycut^6 - 1783*m2^3*MBhat^2*Ycut^6 + 
          71*m2^4*MBhat^2*Ycut^6 - 40*m2^5*MBhat^2*Ycut^6 + 
          1650*MBhat^3*Ycut^6 + 2040*m2*MBhat^3*Ycut^6 - 1890*m2^2*MBhat^3*
           Ycut^6 - 420*m2^3*MBhat^3*Ycut^6 - 750*MBhat^4*Ycut^6 - 
          450*m2*MBhat^4*Ycut^6 - 585*m2^2*MBhat^4*Ycut^6 + 
          45*m2^3*MBhat^4*Ycut^6 + 465*Ycut^7 - 60*m2*Ycut^7 - 
          315*m2^2*Ycut^7 - 210*m2^3*Ycut^7 + 30*m2^4*Ycut^7 + 
          1560*MBhat*Ycut^7 - 528*m2*MBhat*Ycut^7 - 828*m2^2*MBhat*Ycut^7 + 
          240*m2^3*MBhat*Ycut^7 + 36*m2^4*MBhat*Ycut^7 - 485*MBhat^2*Ycut^7 - 
          946*m2*MBhat^2*Ycut^7 - 798*m2^2*MBhat^2*Ycut^7 + 
          479*m2^3*MBhat^2*Ycut^7 + 40*m2^4*MBhat^2*Ycut^7 - 
          1200*MBhat^3*Ycut^7 - 960*m2*MBhat^3*Ycut^7 + 1110*m2^2*MBhat^3*
           Ycut^7 + 90*m2^3*MBhat^3*Ycut^7 + 750*MBhat^4*Ycut^7 + 
          300*m2*MBhat^4*Ycut^7 + 75*m2^2*MBhat^4*Ycut^7 - 570*Ycut^8 + 
          90*m2*Ycut^8 + 45*m2^2*Ycut^8 + 15*m2^3*Ycut^8 - 840*MBhat*Ycut^8 + 
          432*m2*MBhat*Ycut^8 + 324*m2^2*MBhat*Ycut^8 - 
          36*m2^3*MBhat*Ycut^8 + 865*MBhat^2*Ycut^8 + 414*m2*MBhat^2*Ycut^8 + 
          66*m2^2*MBhat^2*Ycut^8 - 85*m2^3*MBhat^2*Ycut^8 + 
          360*MBhat^3*Ycut^8 + 120*m2*MBhat^3*Ycut^8 - 210*m2^2*MBhat^3*
           Ycut^8 - 375*MBhat^4*Ycut^8 - 75*m2*MBhat^4*Ycut^8 + 390*Ycut^9 - 
          60*m2*Ycut^9 - 15*m2^2*Ycut^9 + 120*MBhat*Ycut^9 - 
          168*m2*MBhat*Ycut^9 - 24*m2^2*MBhat*Ycut^9 - 445*MBhat^2*Ycut^9 - 
          61*m2*MBhat^2*Ycut^9 + 5*m2^2*MBhat^2*Ycut^9 + 30*MBhat^3*Ycut^9 + 
          30*m2*MBhat^3*Ycut^9 + 75*MBhat^4*Ycut^9 - 141*Ycut^10 + 
          15*m2*Ycut^10 + 72*MBhat*Ycut^10 + 24*m2*MBhat*Ycut^10 + 
          71*MBhat^2*Ycut^10 - 5*m2*MBhat^2*Ycut^10 - 30*MBhat^3*Ycut^10 + 
          21*Ycut^11 - 24*MBhat*Ycut^11 + 5*MBhat^2*Ycut^11))/
        (45*(-1 + Ycut)^6) + (4*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 
          13*m2*MBhat^2 + 9*MBhat^4)*Log[m2])/3 - 
       (4*m2^2*(18 + 48*m2 + 18*m2^2 - 27*MBhat^2 + 13*m2*MBhat^2 + 
          9*MBhat^4)*Log[1 - Ycut])/3 + 
       c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(9 + 534*m2 + 1434*m2^2 + 534*m2^3 + 
            9*m2^4 - 45*MBhat^2 - 660*m2*MBhat^2 + 540*m2^2*MBhat^2 + 
            180*m2^3*MBhat^2 - 15*m2^4*MBhat^2 + 30*MBhat^4 + 
            300*m2*MBhat^4 + 30*m2^2*MBhat^4 - 36*Ycut - 2307*m2*Ycut - 
            6453*m2^2*Ycut - 2499*m2^3*Ycut - 45*m2^4*Ycut + 
            180*MBhat^2*Ycut + 2955*m2*MBhat^2*Ycut - 2325*m2^2*MBhat^2*
             Ycut - 885*m2^3*MBhat^2*Ycut + 75*m2^4*MBhat^2*Ycut - 
            120*MBhat^4*Ycut - 1350*m2*MBhat^4*Ycut - 150*m2^2*MBhat^4*Ycut + 
            54*Ycut^2 + 3807*m2*Ycut^2 + 11214*m2^2*Ycut^2 + 
            4575*m2^3*Ycut^2 + 90*m2^4*Ycut^2 - 270*MBhat^2*Ycut^2 - 
            5085*m2*MBhat^2*Ycut^2 + 3780*m2^2*MBhat^2*Ycut^2 + 
            1725*m2^3*MBhat^2*Ycut^2 - 150*m2^4*MBhat^2*Ycut^2 + 
            180*MBhat^4*Ycut^2 + 2340*m2*MBhat^4*Ycut^2 + 300*m2^2*MBhat^4*
             Ycut^2 - 36*Ycut^3 - 2889*m2*Ycut^3 - 9135*m2^2*Ycut^3 - 
            4020*m2^3*Ycut^3 - 90*m2^4*Ycut^3 + 300*MBhat^2*Ycut^3 + 
            3705*m2*MBhat^2*Ycut^3 - 2385*m2^2*MBhat^2*Ycut^3 - 
            1770*m2^3*MBhat^2*Ycut^3 + 150*m2^4*MBhat^2*Ycut^3 - 
            100*MBhat^3*Ycut^3 + 200*m2*MBhat^3*Ycut^3 - 100*m2^2*MBhat^3*
             Ycut^3 - 120*MBhat^4*Ycut^3 - 1890*m2*MBhat^4*Ycut^3 - 
            300*m2^2*MBhat^4*Ycut^3 + 9*Ycut^4 + 900*m2*Ycut^4 + 
            3195*m2^2*Ycut^4 + 1605*m2^3*Ycut^4 + 45*m2^4*Ycut^4 - 
            120*MBhat*Ycut^4 + 360*m2*MBhat*Ycut^4 - 360*m2^2*MBhat*Ycut^4 + 
            120*m2^3*MBhat*Ycut^4 - 425*MBhat^2*Ycut^4 - 530*m2*MBhat^2*
             Ycut^4 + 205*m2^2*MBhat^2*Ycut^4 + 825*m2^3*MBhat^2*Ycut^4 - 
            75*m2^4*MBhat^2*Ycut^4 + 400*MBhat^3*Ycut^4 - 420*m2*MBhat^3*
             Ycut^4 + 20*m2^2*MBhat^3*Ycut^4 + 30*MBhat^4*Ycut^4 + 
            570*m2*MBhat^4*Ycut^4 + 150*m2^2*MBhat^4*Ycut^4 + 36*Ycut^5 - 
            144*m2*Ycut^5 - 99*m2^2*Ycut^5 - 204*m2^3*Ycut^5 - 
            9*m2^4*Ycut^5 + 450*MBhat*Ycut^5 - 870*m2*MBhat*Ycut^5 + 
            390*m2^2*MBhat*Ycut^5 + 30*m2^3*MBhat*Ycut^5 + 
            320*MBhat^2*Ycut^5 - 600*m2*MBhat^2*Ycut^5 + 385*m2^2*MBhat^2*
             Ycut^5 - 120*m2^3*MBhat^2*Ycut^5 + 15*m2^4*MBhat^2*Ycut^5 - 
            600*MBhat^3*Ycut^5 + 300*m2*MBhat^3*Ycut^5 + 140*m2^2*MBhat^3*
             Ycut^5 + 120*m2*MBhat^4*Ycut^5 - 30*m2^2*MBhat^4*Ycut^5 - 
            144*Ycut^6 + 252*m2*Ycut^6 - 117*m2^2*Ycut^6 - 51*m2^3*Ycut^6 - 
            600*MBhat*Ycut^6 + 690*m2*MBhat*Ycut^6 - 60*m2^2*MBhat*Ycut^6 - 
            30*m2^3*MBhat*Ycut^6 + 120*MBhat^2*Ycut^6 + 270*m2*MBhat^2*
             Ycut^6 - 215*m2^2*MBhat^2*Ycut^6 - 15*m2^3*MBhat^2*Ycut^6 + 
            400*MBhat^3*Ycut^6 - 140*m2*MBhat^3*Ycut^6 - 60*m2^2*MBhat^3*
             Ycut^6 - 90*m2*MBhat^4*Ycut^6 + 216*Ycut^7 - 192*m2*Ycut^7 - 
            39*m2^2*Ycut^7 + 300*MBhat*Ycut^7 - 210*m2*MBhat*Ycut^7 + 
            30*m2^2*MBhat*Ycut^7 - 280*MBhat^2*Ycut^7 - 40*m2*MBhat^2*
             Ycut^7 + 15*m2^2*MBhat^2*Ycut^7 - 100*MBhat^3*Ycut^7 + 
            60*m2*MBhat^3*Ycut^7 - 144*Ycut^8 + 39*m2*Ycut^8 + 
            30*m2*MBhat*Ycut^8 + 100*MBhat^2*Ycut^8 - 15*m2*MBhat^2*Ycut^8 + 
            36*Ycut^9 - 30*MBhat*Ycut^9))/(15*(-1 + Ycut)^5) + 
         12*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 2*MBhat^2 - m2*MBhat^2 + 
           3*m2^2*MBhat^2 + MBhat^4 + m2*MBhat^4)*Log[m2] - 
         12*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 2*MBhat^2 - m2*MBhat^2 + 
           3*m2^2*MBhat^2 + MBhat^4 + m2*MBhat^4)*Log[1 - Ycut])) + 
     c[SL]*(-1/30*(Ycut^3*(-1 + m2 + Ycut)*(120*MBhat^2 - 480*m2*MBhat^2 + 
           720*m2^2*MBhat^2 - 480*m2^3*MBhat^2 + 120*m2^4*MBhat^2 - 
           100*MBhat^3 + 300*m2*MBhat^3 - 300*m2^2*MBhat^3 + 
           100*m2^3*MBhat^3 - 120*MBhat*Ycut + 480*m2*MBhat*Ycut - 
           720*m2^2*MBhat*Ycut + 480*m2^3*MBhat*Ycut - 120*m2^4*MBhat*Ycut - 
           670*MBhat^2*Ycut + 2100*m2*MBhat^2*Ycut - 2280*m2^2*MBhat^2*Ycut + 
           940*m2^3*MBhat^2*Ycut - 90*m2^4*MBhat^2*Ycut + 700*MBhat^3*Ycut - 
           1400*m2*MBhat^3*Ycut + 700*m2^2*MBhat^3*Ycut - 50*MBhat^4*Ycut - 
           50*m2*MBhat^4*Ycut + 100*m2^2*MBhat^4*Ycut + 36*Ycut^2 - 
           144*m2*Ycut^2 + 216*m2^2*Ycut^2 - 144*m2^3*Ycut^2 + 
           36*m2^4*Ycut^2 + 764*MBhat*Ycut^2 - 2256*m2*MBhat*Ycut^2 + 
           2184*m2^2*MBhat*Ycut^2 - 656*m2^3*MBhat*Ycut^2 - 
           36*m2^4*MBhat*Ycut^2 + 1326*MBhat^2*Ycut^2 - 3474*m2*MBhat^2*
            Ycut^2 + 3006*m2^2*MBhat^2*Ycut^2 - 894*m2^3*MBhat^2*Ycut^2 + 
           36*m2^4*MBhat^2*Ycut^2 - 1950*MBhat^3*Ycut^2 + 
           2650*m2*MBhat^3*Ycut^2 - 490*m2^2*MBhat^3*Ycut^2 - 
           210*m2^3*MBhat^3*Ycut^2 + 250*MBhat^4*Ycut^2 + 
           200*m2*MBhat^4*Ycut^2 - 240*m2^2*MBhat^4*Ycut^2 - 243*Ycut^3 + 
           693*m2*Ycut^3 - 621*m2^2*Ycut^3 + 135*m2^3*Ycut^3 + 
           36*m2^4*Ycut^3 - 1948*MBhat*Ycut^3 + 4196*m2*MBhat*Ycut^3 - 
           2500*m2^2*MBhat*Ycut^3 + 204*m2^3*MBhat*Ycut^3 + 
           48*m2^4*MBhat*Ycut^3 - 795*MBhat^2*Ycut^3 + 2481*m2*MBhat^2*
            Ycut^3 - 2193*m2^2*MBhat^2*Ycut^3 + 513*m2^3*MBhat^2*Ycut^3 - 
           6*m2^4*MBhat^2*Ycut^3 + 2750*MBhat^3*Ycut^3 - 2600*m2*MBhat^3*
            Ycut^3 + 30*m2^2*MBhat^3*Ycut^3 + 140*m2^3*MBhat^3*Ycut^3 - 
           500*MBhat^4*Ycut^3 - 300*m2*MBhat^4*Ycut^3 + 180*m2^2*MBhat^4*
            Ycut^3 + 675*Ycut^4 - 1332*m2*Ycut^4 + 627*m2^2*Ycut^4 + 
           42*m2^3*Ycut^4 - 12*m2^4*Ycut^4 + 2480*MBhat*Ycut^4 - 
           3824*m2*MBhat*Ycut^4 + 1356*m2^2*MBhat*Ycut^4 - 
           12*m2^4*MBhat*Ycut^4 - 865*MBhat^2*Ycut^4 - 384*m2*MBhat^2*
            Ycut^4 + 903*m2^2*MBhat^2*Ycut^4 - 64*m2^3*MBhat^2*Ycut^4 - 
           2000*MBhat^3*Ycut^4 + 1400*m2*MBhat^3*Ycut^4 + 110*m2^2*MBhat^3*
            Ycut^4 - 30*m2^3*MBhat^3*Ycut^4 + 500*MBhat^4*Ycut^4 + 
           200*m2*MBhat^4*Ycut^4 - 40*m2^2*MBhat^4*Ycut^4 - 990*Ycut^5 + 
           1278*m2*Ycut^5 - 255*m2^2*Ycut^5 - 33*m2^3*Ycut^5 - 
           1520*MBhat*Ycut^5 + 1656*m2*MBhat*Ycut^5 - 348*m2^2*MBhat*Ycut^5 - 
           28*m2^3*MBhat*Ycut^5 + 1620*MBhat^2*Ycut^5 - 414*m2*MBhat^2*
            Ycut^5 - 171*m2^2*MBhat^2*Ycut^5 - 15*m2^3*MBhat^2*Ycut^5 + 
           600*MBhat^3*Ycut^5 - 400*m2*MBhat^3*Ycut^5 - 50*m2^2*MBhat^3*
            Ycut^5 - 250*MBhat^4*Ycut^5 - 50*m2*MBhat^4*Ycut^5 + 810*Ycut^6 - 
           612*m2*Ycut^6 + 33*m2^2*Ycut^6 + 220*MBhat*Ycut^6 - 
           224*m2*MBhat*Ycut^6 + 28*m2^2*MBhat*Ycut^6 - 900*MBhat^2*Ycut^6 + 
           186*m2*MBhat^2*Ycut^6 + 15*m2^2*MBhat^2*Ycut^6 + 
           50*MBhat^3*Ycut^6 + 50*m2*MBhat^3*Ycut^6 + 50*MBhat^4*Ycut^6 - 
           351*Ycut^7 + 117*m2*Ycut^7 + 196*MBhat*Ycut^7 - 
           28*m2*MBhat*Ycut^7 + 149*MBhat^2*Ycut^7 - 15*m2*MBhat^2*Ycut^7 - 
           50*MBhat^3*Ycut^7 + 63*Ycut^8 - 72*MBhat*Ycut^8 + 
           15*MBhat^2*Ycut^8)*c[T])/(-1 + Ycut)^6 + 
       c[SR]*(-1/30*(Sqrt[m2]*(-1 + m2 + Ycut)*(9 + 534*m2 + 1434*m2^2 + 
             534*m2^3 + 9*m2^4 - 45*MBhat^2 - 660*m2*MBhat^2 + 
             540*m2^2*MBhat^2 + 180*m2^3*MBhat^2 - 15*m2^4*MBhat^2 + 
             30*MBhat^4 + 300*m2*MBhat^4 + 30*m2^2*MBhat^4 - 36*Ycut - 
             2307*m2*Ycut - 6453*m2^2*Ycut - 2499*m2^3*Ycut - 45*m2^4*Ycut + 
             180*MBhat^2*Ycut + 2955*m2*MBhat^2*Ycut - 2325*m2^2*MBhat^2*
              Ycut - 885*m2^3*MBhat^2*Ycut + 75*m2^4*MBhat^2*Ycut - 
             120*MBhat^4*Ycut - 1350*m2*MBhat^4*Ycut - 150*m2^2*MBhat^4*
              Ycut + 54*Ycut^2 + 3807*m2*Ycut^2 + 11214*m2^2*Ycut^2 + 
             4575*m2^3*Ycut^2 + 90*m2^4*Ycut^2 - 270*MBhat^2*Ycut^2 - 
             5085*m2*MBhat^2*Ycut^2 + 3780*m2^2*MBhat^2*Ycut^2 + 
             1725*m2^3*MBhat^2*Ycut^2 - 150*m2^4*MBhat^2*Ycut^2 + 
             180*MBhat^4*Ycut^2 + 2340*m2*MBhat^4*Ycut^2 + 300*m2^2*MBhat^4*
              Ycut^2 - 36*Ycut^3 - 2889*m2*Ycut^3 - 9135*m2^2*Ycut^3 - 
             4020*m2^3*Ycut^3 - 90*m2^4*Ycut^3 + 300*MBhat^2*Ycut^3 + 
             3705*m2*MBhat^2*Ycut^3 - 2385*m2^2*MBhat^2*Ycut^3 - 
             1770*m2^3*MBhat^2*Ycut^3 + 150*m2^4*MBhat^2*Ycut^3 - 
             100*MBhat^3*Ycut^3 + 200*m2*MBhat^3*Ycut^3 - 100*m2^2*MBhat^3*
              Ycut^3 - 120*MBhat^4*Ycut^3 - 1890*m2*MBhat^4*Ycut^3 - 
             300*m2^2*MBhat^4*Ycut^3 + 9*Ycut^4 + 900*m2*Ycut^4 + 
             3195*m2^2*Ycut^4 + 1605*m2^3*Ycut^4 + 45*m2^4*Ycut^4 - 
             120*MBhat*Ycut^4 + 360*m2*MBhat*Ycut^4 - 360*m2^2*MBhat*Ycut^4 + 
             120*m2^3*MBhat*Ycut^4 - 425*MBhat^2*Ycut^4 - 530*m2*MBhat^2*
              Ycut^4 + 205*m2^2*MBhat^2*Ycut^4 + 825*m2^3*MBhat^2*Ycut^4 - 
             75*m2^4*MBhat^2*Ycut^4 + 400*MBhat^3*Ycut^4 - 420*m2*MBhat^3*
              Ycut^4 + 20*m2^2*MBhat^3*Ycut^4 + 30*MBhat^4*Ycut^4 + 
             570*m2*MBhat^4*Ycut^4 + 150*m2^2*MBhat^4*Ycut^4 + 36*Ycut^5 - 
             144*m2*Ycut^5 - 99*m2^2*Ycut^5 - 204*m2^3*Ycut^5 - 
             9*m2^4*Ycut^5 + 450*MBhat*Ycut^5 - 870*m2*MBhat*Ycut^5 + 
             390*m2^2*MBhat*Ycut^5 + 30*m2^3*MBhat*Ycut^5 + 
             320*MBhat^2*Ycut^5 - 600*m2*MBhat^2*Ycut^5 + 385*m2^2*MBhat^2*
              Ycut^5 - 120*m2^3*MBhat^2*Ycut^5 + 15*m2^4*MBhat^2*Ycut^5 - 
             600*MBhat^3*Ycut^5 + 300*m2*MBhat^3*Ycut^5 + 140*m2^2*MBhat^3*
              Ycut^5 + 120*m2*MBhat^4*Ycut^5 - 30*m2^2*MBhat^4*Ycut^5 - 
             144*Ycut^6 + 252*m2*Ycut^6 - 117*m2^2*Ycut^6 - 51*m2^3*Ycut^6 - 
             600*MBhat*Ycut^6 + 690*m2*MBhat*Ycut^6 - 60*m2^2*MBhat*Ycut^6 - 
             30*m2^3*MBhat*Ycut^6 + 120*MBhat^2*Ycut^6 + 270*m2*MBhat^2*
              Ycut^6 - 215*m2^2*MBhat^2*Ycut^6 - 15*m2^3*MBhat^2*Ycut^6 + 
             400*MBhat^3*Ycut^6 - 140*m2*MBhat^3*Ycut^6 - 60*m2^2*MBhat^3*
              Ycut^6 - 90*m2*MBhat^4*Ycut^6 + 216*Ycut^7 - 192*m2*Ycut^7 - 
             39*m2^2*Ycut^7 + 300*MBhat*Ycut^7 - 210*m2*MBhat*Ycut^7 + 
             30*m2^2*MBhat*Ycut^7 - 280*MBhat^2*Ycut^7 - 40*m2*MBhat^2*
              Ycut^7 + 15*m2^2*MBhat^2*Ycut^7 - 100*MBhat^3*Ycut^7 + 
             60*m2*MBhat^3*Ycut^7 - 144*Ycut^8 + 39*m2*Ycut^8 + 
             30*m2*MBhat*Ycut^8 + 100*MBhat^2*Ycut^8 - 15*m2*MBhat^2*Ycut^8 + 
             36*Ycut^9 - 30*MBhat*Ycut^9))/(-1 + Ycut)^5 - 
         6*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 2*MBhat^2 - m2*MBhat^2 + 
           3*m2^2*MBhat^2 + MBhat^4 + m2*MBhat^4)*Log[m2] + 
         6*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 2*MBhat^2 - m2*MBhat^2 + 
           3*m2^2*MBhat^2 + MBhat^4 + m2*MBhat^4)*Log[1 - Ycut]))) + 
   muG*(-1/90*((-1 + m2 + Ycut)*(90 + 1230*m2 - 2400*m2^2 + 720*m2^3 + 
         390*m2^4 - 30*m2^5 - 424*MBhat - 2404*m2*MBhat + 4616*m2^2*MBhat - 
         1864*m2^3*MBhat + 656*m2^4*MBhat - 100*m2^5*MBhat + 739*MBhat^2 + 
         874*m2*MBhat^2 - 206*m2^2*MBhat^2 - 686*m2^3*MBhat^2 + 
         799*m2^4*MBhat^2 - 200*m2^5*MBhat^2 - 540*MBhat^3 + 480*m2*MBhat^3 - 
         2640*m2^2*MBhat^3 + 1680*m2^3*MBhat^3 - 420*m2^4*MBhat^3 + 
         135*MBhat^4 - 225*m2*MBhat^4 + 855*m2^2*MBhat^4 - 225*m2^3*MBhat^4 - 
         360*Ycut - 5550*m2*Ycut + 10560*m2^2*Ycut - 2880*m2^3*Ycut - 
         1920*m2^4*Ycut + 150*m2^5*Ycut + 1696*MBhat*Ycut + 
         11352*m2*MBhat*Ycut - 21292*m2^2*MBhat*Ycut + 8764*m2^3*MBhat*Ycut - 
         3180*m2^4*MBhat*Ycut + 500*m2^5*MBhat*Ycut - 2956*MBhat^2*Ycut - 
         4917*m2*MBhat^2*Ycut + 1897*m2^2*MBhat^2*Ycut + 
         2831*m2^3*MBhat^2*Ycut - 3795*m2^4*MBhat^2*Ycut + 
         1000*m2^5*MBhat^2*Ycut + 2160*MBhat^3*Ycut - 1740*m2*MBhat^3*Ycut + 
         11940*m2^2*MBhat^3*Ycut - 7980*m2^3*MBhat^3*Ycut + 
         2100*m2^4*MBhat^3*Ycut - 540*MBhat^4*Ycut + 1035*m2*MBhat^4*Ycut - 
         4050*m2^2*MBhat^4*Ycut + 1125*m2^3*MBhat^4*Ycut + 540*Ycut^2 + 
         9630*m2*Ycut^2 - 17730*m2^2*Ycut^2 + 4110*m2^3*Ycut^2 + 
         3750*m2^4*Ycut^2 - 300*m2^5*Ycut^2 - 2544*MBhat*Ycut^2 - 
         20712*m2*MBhat*Ycut^2 + 37916*m2^2*MBhat*Ycut^2 - 
         15960*m2^3*MBhat*Ycut^2 + 6060*m2^4*MBhat*Ycut^2 - 
         1000*m2^5*MBhat*Ycut^2 + 4434*MBhat^2*Ycut^2 + 
         10587*m2*MBhat^2*Ycut^2 - 5606*m2^2*MBhat^2*Ycut^2 - 
         4065*m2^3*MBhat^2*Ycut^2 + 6990*m2^4*MBhat^2*Ycut^2 - 
         2000*m2^5*MBhat^2*Ycut^2 - 3240*MBhat^3*Ycut^2 + 
         1980*m2*MBhat^3*Ycut^2 - 20520*m2^2*MBhat^3*Ycut^2 + 
         14700*m2^3*MBhat^3*Ycut^2 - 4200*m2^4*MBhat^3*Ycut^2 + 
         810*MBhat^4*Ycut^2 - 1755*m2*MBhat^4*Ycut^2 + 7425*m2^2*MBhat^4*
          Ycut^2 - 2250*m2^3*MBhat^4*Ycut^2 - 360*Ycut^3 - 7770*m2*Ycut^3 + 
         13620*m2^2*Ycut^3 - 2190*m2^3*Ycut^3 - 3600*m2^4*Ycut^3 + 
         300*m2^5*Ycut^3 + 1696*MBhat*Ycut^3 + 17704*m2*MBhat*Ycut^3 - 
         31500*m2^2*MBhat*Ycut^3 + 13580*m2^3*MBhat*Ycut^3 - 
         5560*m2^4*MBhat*Ycut^3 + 1000*m2^5*MBhat*Ycut^3 - 
         2776*MBhat^2*Ycut^3 - 11719*m2*MBhat^2*Ycut^3 + 
         7965*m2^2*MBhat^2*Ycut^3 + 2950*m2^3*MBhat^2*Ycut^3 - 
         6890*m2^4*MBhat^2*Ycut^3 + 2000*m2^5*MBhat^2*Ycut^3 + 
         1860*MBhat^3*Ycut^3 + 840*m2*MBhat^3*Ycut^3 + 16440*m2^2*MBhat^3*
          Ycut^3 - 14100*m2^3*MBhat^3*Ycut^3 + 4200*m2^4*MBhat^3*Ycut^3 - 
         420*MBhat^4*Ycut^3 + 1065*m2*MBhat^4*Ycut^3 - 6900*m2^2*MBhat^4*
          Ycut^3 + 2250*m2^3*MBhat^4*Ycut^3 + 90*Ycut^4 + 2640*m2*Ycut^4 - 
         4200*m2^2*Ycut^4 - 30*m2^3*Ycut^4 + 1650*m2^4*Ycut^4 - 
         150*m2^5*Ycut^4 - 544*MBhat*Ycut^4 - 5820*m2*MBhat*Ycut^4 + 
         10920*m2^2*MBhat*Ycut^4 - 6140*m2^3*MBhat*Ycut^4 + 
         3180*m2^4*MBhat*Ycut^4 - 500*m2^5*MBhat*Ycut^4 + 
         244*MBhat^2*Ycut^4 + 7755*m2*MBhat^2*Ycut^4 - 8040*m2^2*MBhat^2*
          Ycut^4 + 710*m2^3*MBhat^2*Ycut^4 + 3345*m2^4*MBhat^2*Ycut^4 - 
         1000*m2^5*MBhat^2*Ycut^4 + 480*MBhat^3*Ycut^4 - 
         4740*m2*MBhat^3*Ycut^4 - 4560*m2^2*MBhat^3*Ycut^4 + 
         7200*m2^3*MBhat^3*Ycut^4 - 2100*m2^4*MBhat^3*Ycut^4 - 
         270*MBhat^4*Ycut^4 + 435*m2*MBhat^4*Ycut^4 + 3525*m2^2*MBhat^4*
          Ycut^4 - 1125*m2^3*MBhat^4*Ycut^4 + 30*Ycut^5 - 300*m2*Ycut^5 + 
         60*m2^2*Ycut^5 + 690*m2^3*Ycut^5 - 510*m2^4*Ycut^5 + 
         30*m2^5*Ycut^5 + 384*MBhat*Ycut^5 - 1416*m2*MBhat*Ycut^5 + 
         324*m2^2*MBhat*Ycut^5 + 1404*m2^3*MBhat*Ycut^5 - 
         876*m2^4*MBhat*Ycut^5 + 100*m2^5*MBhat*Ycut^5 + 276*MBhat^2*Ycut^5 - 
         3339*m2*MBhat^2*Ycut^5 + 6801*m2^2*MBhat^2*Ycut^5 - 
         2949*m2^3*MBhat^2*Ycut^5 - 609*m2^4*MBhat^2*Ycut^5 + 
         200*m2^5*MBhat^2*Ycut^5 - 1110*MBhat^3*Ycut^5 + 
         5370*m2*MBhat^3*Ycut^5 - 2310*m2^2*MBhat^3*Ycut^5 - 
         1530*m2^3*MBhat^3*Ycut^5 + 420*m2^4*MBhat^3*Ycut^5 + 
         420*MBhat^4*Ycut^5 - 945*m2*MBhat^4*Ycut^5 - 930*m2^2*MBhat^4*
          Ycut^5 + 225*m2^3*MBhat^4*Ycut^5 - 105*Ycut^6 + 405*m2*Ycut^6 + 
         75*m2^2*Ycut^6 - 435*m2^3*Ycut^6 + 60*m2^4*Ycut^6 - 
         356*MBhat*Ycut^6 + 1708*m2*MBhat*Ycut^6 - 1568*m2^2*MBhat*Ycut^6 + 
         296*m2^3*MBhat*Ycut^6 + 80*m2^4*MBhat*Ycut^6 + 251*MBhat^2*Ycut^6 + 
         662*m2*MBhat^2*Ycut^6 - 3247*m2^2*MBhat^2*Ycut^6 + 
         1414*m2^3*MBhat^2*Ycut^6 - 20*m2^4*MBhat^2*Ycut^6 + 
         240*MBhat^3*Ycut^6 - 2670*m2*MBhat^3*Ycut^6 + 1980*m2^2*MBhat^3*
          Ycut^6 + 30*m2^3*MBhat^3*Ycut^6 - 30*MBhat^4*Ycut^6 + 
         465*m2*MBhat^4*Ycut^6 + 75*m2^2*MBhat^4*Ycut^6 + 120*Ycut^7 - 
         375*m2*Ycut^7 + 30*m2^2*Ycut^7 + 15*m2^3*Ycut^7 - 16*MBhat*Ycut^7 - 
         348*m2*MBhat*Ycut^7 + 604*m2^2*MBhat*Ycut^7 - 80*m2^3*MBhat*Ycut^7 - 
         164*MBhat^2*Ycut^7 + 168*m2*MBhat^2*Ycut^7 + 431*m2^2*MBhat^2*
          Ycut^7 - 205*m2^3*MBhat^2*Ycut^7 + 240*MBhat^3*Ycut^7 + 
         450*m2*MBhat^3*Ycut^7 - 330*m2^2*MBhat^3*Ycut^7 - 
         180*MBhat^4*Ycut^7 - 75*m2*MBhat^4*Ycut^7 - 30*Ycut^8 + 
         75*m2*Ycut^8 - 15*m2^2*Ycut^8 + 144*MBhat*Ycut^8 - 
         84*m2*MBhat*Ycut^8 - 20*m2^2*MBhat*Ycut^8 - 129*MBhat^2*Ycut^8 - 
         66*m2*MBhat^2*Ycut^8 + 5*m2^2*MBhat^2*Ycut^8 - 60*MBhat^3*Ycut^8 + 
         30*m2*MBhat^3*Ycut^8 + 75*MBhat^4*Ycut^8 - 30*Ycut^9 + 
         15*m2*Ycut^9 - 16*MBhat*Ycut^9 + 20*m2*MBhat*Ycut^9 + 
         76*MBhat^2*Ycut^9 - 5*m2*MBhat^2*Ycut^9 - 30*MBhat^3*Ycut^9 + 
         15*Ycut^10 - 20*MBhat*Ycut^10 + 5*MBhat^2*Ycut^10))/(-1 + Ycut)^5 - 
     (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 36*m2*MBhat + 
        8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 13*m2^2*MBhat^2 - 
        12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*Log[m2])/3 + 
     (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 36*m2*MBhat + 
        8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 13*m2^2*MBhat^2 - 
        12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*Log[1 - Ycut])/3 + 
     c[SL]^2*(-1/120*((-1 + m2 + Ycut)*(-123 - 6063*m2 - 11238*m2^2 - 
           438*m2^3 + 237*m2^4 - 15*m2^5 + 560*MBhat + 17720*m2*MBhat + 
           15920*m2^2*MBhat - 880*m2^3*MBhat + 320*m2^4*MBhat - 
           40*m2^5*MBhat - 962*MBhat^2 - 17792*m2*MBhat^2 - 
           4682*m2^2*MBhat^2 + 118*m2^3*MBhat^2 + 208*m2^4*MBhat^2 - 
           50*m2^5*MBhat^2 + 720*MBhat^3 + 6840*m2*MBhat^3 - 
           840*m2^2*MBhat^3 + 600*m2^3*MBhat^3 - 120*m2^4*MBhat^3 - 
           195*MBhat^4 - 675*m2*MBhat^4 + 405*m2^2*MBhat^4 - 
           75*m2^3*MBhat^4 + 492*Ycut + 26289*m2*Ycut + 51186*m2^2*Ycut + 
           2508*m2^3*Ycut - 1170*m2^4*Ycut + 75*m2^5*Ycut - 2240*MBhat*Ycut - 
           77520*m2*MBhat*Ycut - 74200*m2^2*MBhat*Ycut + 4120*m2^3*MBhat*
            Ycut - 1560*m2^4*MBhat*Ycut + 200*m2^5*MBhat*Ycut + 
           3848*MBhat^2*Ycut + 78846*m2*MBhat^2*Ycut + 23014*m2^2*MBhat^2*
            Ycut - 748*m2^3*MBhat^2*Ycut - 990*m2^4*MBhat^2*Ycut + 
           250*m2^5*MBhat^2*Ycut - 2880*MBhat^3*Ycut - 30960*m2*MBhat^3*
            Ycut + 3720*m2^2*MBhat^3*Ycut - 2880*m2^3*MBhat^3*Ycut + 
           600*m2^4*MBhat^3*Ycut + 780*MBhat^4*Ycut + 3225*m2*MBhat^4*Ycut - 
           1950*m2^2*MBhat^4*Ycut + 375*m2^3*MBhat^4*Ycut - 738*Ycut^2 - 
           43569*m2*Ycut^2 - 90303*m2^2*Ycut^2 - 5715*m2^3*Ycut^2 + 
           2295*m2^4*Ycut^2 - 150*m2^5*Ycut^2 + 3360*MBhat*Ycut^2 + 
           129840*m2*MBhat*Ycut^2 + 134840*m2^2*MBhat*Ycut^2 - 
           7440*m2^3*MBhat*Ycut^2 + 3000*m2^4*MBhat*Ycut^2 - 
           400*m2^5*MBhat*Ycut^2 - 5772*MBhat^2*Ycut^2 - 134106*m2*MBhat^2*
            Ycut^2 - 44792*m2^2*MBhat^2*Ycut^2 + 1920*m2^3*MBhat^2*Ycut^2 + 
           1830*m2^4*MBhat^2*Ycut^2 - 500*m2^5*MBhat^2*Ycut^2 + 
           4320*MBhat^3*Ycut^2 + 54000*m2*MBhat^3*Ycut^2 - 
           6120*m2^2*MBhat^3*Ycut^2 + 5400*m2^3*MBhat^3*Ycut^2 - 
           1200*m2^4*MBhat^3*Ycut^2 - 1170*MBhat^4*Ycut^2 - 
           5985*m2*MBhat^4*Ycut^2 + 3675*m2^2*MBhat^4*Ycut^2 - 
           750*m2^3*MBhat^4*Ycut^2 + 492*Ycut^3 + 33243*m2*Ycut^3 + 
           75060*m2^2*Ycut^3 + 6465*m2^3*Ycut^3 - 2220*m2^4*Ycut^3 + 
           150*m2^5*Ycut^3 - 2240*MBhat*Ycut^3 - 100400*m2*MBhat*Ycut^3 - 
           116760*m2^2*MBhat*Ycut^3 + 6200*m2^3*MBhat*Ycut^3 - 
           2800*m2^4*MBhat*Ycut^3 + 400*m2^5*MBhat*Ycut^3 + 
           3908*MBhat^2*Ycut^3 + 105842*m2*MBhat^2*Ycut^3 + 
           41910*m2^2*MBhat^2*Ycut^3 - 1670*m2^3*MBhat^2*Ycut^3 - 
           1880*m2^4*MBhat^2*Ycut^3 + 500*m2^5*MBhat^2*Ycut^3 - 
           2980*MBhat^3*Ycut^3 - 44220*m2*MBhat^3*Ycut^3 + 
           5100*m2^2*MBhat^3*Ycut^3 - 5300*m2^3*MBhat^3*Ycut^3 + 
           1200*m2^4*MBhat^3*Ycut^3 + 820*MBhat^4*Ycut^3 + 
           5395*m2*MBhat^4*Ycut^3 - 3500*m2^2*MBhat^4*Ycut^3 + 
           750*m2^3*MBhat^4*Ycut^3 - 123*Ycut^4 - 10440*m2*Ycut^4 - 
           27090*m2^2*Ycut^4 - 3585*m2^3*Ycut^4 + 1035*m2^4*Ycut^4 - 
           75*m2^5*Ycut^4 + 500*MBhat*Ycut^4 + 32040*m2*MBhat*Ycut^4 + 
           45720*m2^2*MBhat*Ycut^4 - 2840*m2^3*MBhat*Ycut^4 + 
           1500*m2^4*MBhat*Ycut^4 - 200*m2^5*MBhat*Ycut^4 - 
           1027*MBhat^2*Ycut^4 - 34875*m2*MBhat^2*Ycut^4 - 
           18315*m2^2*MBhat^2*Ycut^4 + 595*m2^3*MBhat^2*Ycut^4 + 
           990*m2^4*MBhat^2*Ycut^4 - 250*m2^5*MBhat^2*Ycut^4 + 
           980*MBhat^3*Ycut^4 + 15720*m2*MBhat^3*Ycut^4 - 2460*m2^2*MBhat^3*
            Ycut^4 + 2800*m2^3*MBhat^3*Ycut^4 - 600*m2^4*MBhat^3*Ycut^4 - 
           330*MBhat^4*Ycut^4 - 2375*m2*MBhat^4*Ycut^4 + 1775*m2^2*MBhat^4*
            Ycut^4 - 375*m2^3*MBhat^4*Ycut^4 + 18*Ycut^5 + 468*m2*Ycut^5 + 
           1728*m2^2*Ycut^5 + 963*m2^3*Ycut^5 - 252*m2^4*Ycut^5 + 
           15*m2^5*Ycut^5 + 120*MBhat*Ycut^5 - 1200*m2*MBhat*Ycut^5 - 
           5040*m2^2*MBhat*Ycut^5 + 840*m2^3*MBhat*Ycut^5 - 
           360*m2^4*MBhat*Ycut^5 + 40*m2^5*MBhat*Ycut^5 - 
           208*MBhat^2*Ycut^5 + 1507*m2*MBhat^2*Ycut^5 + 3022*m2^2*MBhat^2*
            Ycut^5 - 283*m2^3*MBhat^2*Ycut^5 - 228*m2^4*MBhat^2*Ycut^5 + 
           50*m2^5*MBhat^2*Ycut^5 - 70*MBhat^3*Ycut^5 - 1110*m2*MBhat^3*
            Ycut^5 + 510*m2^2*MBhat^3*Ycut^5 - 650*m2^3*MBhat^3*Ycut^5 + 
           120*m2^4*MBhat^3*Ycut^5 + 140*MBhat^4*Ycut^5 + 
           405*m2*MBhat^4*Ycut^5 - 430*m2^2*MBhat^4*Ycut^5 + 
           75*m2^3*MBhat^4*Ycut^5 - 42*Ycut^6 - 24*m2*Ycut^6 + 
           654*m2^2*Ycut^6 - 183*m2^3*Ycut^6 + 15*m2^4*Ycut^6 + 
           80*MBhat*Ycut^6 - 460*m2*MBhat*Ycut^6 - 460*m2^2*MBhat*Ycut^6 + 
           20*m2^3*MBhat*Ycut^6 + 20*m2^4*MBhat*Ycut^6 + 292*MBhat^2*Ycut^6 + 
           319*m2*MBhat^2*Ycut^6 - 79*m2^2*MBhat^2*Ycut^6 + 
           78*m2^3*MBhat^2*Ycut^6 + 10*m2^4*MBhat^2*Ycut^6 - 
           320*MBhat^3*Ycut^6 - 150*m2*MBhat^3*Ycut^6 + 120*m2^2*MBhat^3*
            Ycut^6 + 30*m2^3*MBhat^3*Ycut^6 - 10*MBhat^4*Ycut^6 + 
           35*m2*MBhat^4*Ycut^6 + 25*m2^2*MBhat^4*Ycut^6 - 12*Ycut^7 + 
           144*m2*Ycut^7 - 12*m2^2*Ycut^7 - 15*m2^3*Ycut^7 - 
           320*MBhat*Ycut^7 + 180*m2*MBhat*Ycut^7 - 40*m2^2*MBhat*Ycut^7 - 
           20*m2^3*MBhat*Ycut^7 + 112*MBhat^2*Ycut^7 + 171*m2*MBhat^2*
            Ycut^7 - 88*m2^2*MBhat^2*Ycut^7 - 10*m2^3*MBhat^2*Ycut^7 + 
           280*MBhat^3*Ycut^7 - 150*m2*MBhat^3*Ycut^7 - 30*m2^2*MBhat^3*
            Ycut^7 - 60*MBhat^4*Ycut^7 - 25*m2*MBhat^4*Ycut^7 + 108*Ycut^8 - 
           108*m2*Ycut^8 + 15*m2^2*Ycut^8 + 180*MBhat*Ycut^8 - 
           180*m2*MBhat*Ycut^8 + 20*m2^2*MBhat*Ycut^8 - 293*MBhat^2*Ycut^8 + 
           98*m2*MBhat^2*Ycut^8 + 10*m2^2*MBhat^2*Ycut^8 - 
           20*MBhat^3*Ycut^8 + 30*m2*MBhat^3*Ycut^8 + 25*MBhat^4*Ycut^8 - 
           102*Ycut^9 + 60*m2*Ycut^9 + 40*MBhat*Ycut^9 - 20*m2*MBhat*Ycut^9 + 
           92*MBhat^2*Ycut^9 - 10*m2*MBhat^2*Ycut^9 - 30*MBhat^3*Ycut^9 + 
           30*Ycut^10 - 40*MBhat*Ycut^10 + 10*MBhat^2*Ycut^10))/
         (-1 + Ycut)^5 - (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 120*MBhat + 
          360*m2*MBhat + 80*m2^2*MBhat - 144*MBhat^2 - 240*m2*MBhat^2 - 
          2*m2^2*MBhat^2 + 72*MBhat^3 + 48*m2*MBhat^3 - 12*MBhat^4 + 
          3*m2*MBhat^4)*Log[m2])/2 + (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 
          120*MBhat + 360*m2*MBhat + 80*m2^2*MBhat - 144*MBhat^2 - 
          240*m2*MBhat^2 - 2*m2^2*MBhat^2 + 72*MBhat^3 + 48*m2*MBhat^3 - 
          12*MBhat^4 + 3*m2*MBhat^4)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/120*((-1 + m2 + Ycut)*(-123 - 6063*m2 - 11238*m2^2 - 
           438*m2^3 + 237*m2^4 - 15*m2^5 + 560*MBhat + 17720*m2*MBhat + 
           15920*m2^2*MBhat - 880*m2^3*MBhat + 320*m2^4*MBhat - 
           40*m2^5*MBhat - 962*MBhat^2 - 17792*m2*MBhat^2 - 
           4682*m2^2*MBhat^2 + 118*m2^3*MBhat^2 + 208*m2^4*MBhat^2 - 
           50*m2^5*MBhat^2 + 720*MBhat^3 + 6840*m2*MBhat^3 - 
           840*m2^2*MBhat^3 + 600*m2^3*MBhat^3 - 120*m2^4*MBhat^3 - 
           195*MBhat^4 - 675*m2*MBhat^4 + 405*m2^2*MBhat^4 - 
           75*m2^3*MBhat^4 + 492*Ycut + 26289*m2*Ycut + 51186*m2^2*Ycut + 
           2508*m2^3*Ycut - 1170*m2^4*Ycut + 75*m2^5*Ycut - 2240*MBhat*Ycut - 
           77520*m2*MBhat*Ycut - 74200*m2^2*MBhat*Ycut + 4120*m2^3*MBhat*
            Ycut - 1560*m2^4*MBhat*Ycut + 200*m2^5*MBhat*Ycut + 
           3848*MBhat^2*Ycut + 78846*m2*MBhat^2*Ycut + 23014*m2^2*MBhat^2*
            Ycut - 748*m2^3*MBhat^2*Ycut - 990*m2^4*MBhat^2*Ycut + 
           250*m2^5*MBhat^2*Ycut - 2880*MBhat^3*Ycut - 30960*m2*MBhat^3*
            Ycut + 3720*m2^2*MBhat^3*Ycut - 2880*m2^3*MBhat^3*Ycut + 
           600*m2^4*MBhat^3*Ycut + 780*MBhat^4*Ycut + 3225*m2*MBhat^4*Ycut - 
           1950*m2^2*MBhat^4*Ycut + 375*m2^3*MBhat^4*Ycut - 738*Ycut^2 - 
           43569*m2*Ycut^2 - 90303*m2^2*Ycut^2 - 5715*m2^3*Ycut^2 + 
           2295*m2^4*Ycut^2 - 150*m2^5*Ycut^2 + 3360*MBhat*Ycut^2 + 
           129840*m2*MBhat*Ycut^2 + 134840*m2^2*MBhat*Ycut^2 - 
           7440*m2^3*MBhat*Ycut^2 + 3000*m2^4*MBhat*Ycut^2 - 
           400*m2^5*MBhat*Ycut^2 - 5772*MBhat^2*Ycut^2 - 134106*m2*MBhat^2*
            Ycut^2 - 44792*m2^2*MBhat^2*Ycut^2 + 1920*m2^3*MBhat^2*Ycut^2 + 
           1830*m2^4*MBhat^2*Ycut^2 - 500*m2^5*MBhat^2*Ycut^2 + 
           4320*MBhat^3*Ycut^2 + 54000*m2*MBhat^3*Ycut^2 - 
           6120*m2^2*MBhat^3*Ycut^2 + 5400*m2^3*MBhat^3*Ycut^2 - 
           1200*m2^4*MBhat^3*Ycut^2 - 1170*MBhat^4*Ycut^2 - 
           5985*m2*MBhat^4*Ycut^2 + 3675*m2^2*MBhat^4*Ycut^2 - 
           750*m2^3*MBhat^4*Ycut^2 + 492*Ycut^3 + 33243*m2*Ycut^3 + 
           75060*m2^2*Ycut^3 + 6465*m2^3*Ycut^3 - 2220*m2^4*Ycut^3 + 
           150*m2^5*Ycut^3 - 2240*MBhat*Ycut^3 - 100400*m2*MBhat*Ycut^3 - 
           116760*m2^2*MBhat*Ycut^3 + 6200*m2^3*MBhat*Ycut^3 - 
           2800*m2^4*MBhat*Ycut^3 + 400*m2^5*MBhat*Ycut^3 + 
           3908*MBhat^2*Ycut^3 + 105842*m2*MBhat^2*Ycut^3 + 
           41910*m2^2*MBhat^2*Ycut^3 - 1670*m2^3*MBhat^2*Ycut^3 - 
           1880*m2^4*MBhat^2*Ycut^3 + 500*m2^5*MBhat^2*Ycut^3 - 
           2980*MBhat^3*Ycut^3 - 44220*m2*MBhat^3*Ycut^3 + 
           5100*m2^2*MBhat^3*Ycut^3 - 5300*m2^3*MBhat^3*Ycut^3 + 
           1200*m2^4*MBhat^3*Ycut^3 + 820*MBhat^4*Ycut^3 + 
           5395*m2*MBhat^4*Ycut^3 - 3500*m2^2*MBhat^4*Ycut^3 + 
           750*m2^3*MBhat^4*Ycut^3 - 123*Ycut^4 - 10440*m2*Ycut^4 - 
           27090*m2^2*Ycut^4 - 3585*m2^3*Ycut^4 + 1035*m2^4*Ycut^4 - 
           75*m2^5*Ycut^4 + 500*MBhat*Ycut^4 + 32040*m2*MBhat*Ycut^4 + 
           45720*m2^2*MBhat*Ycut^4 - 2840*m2^3*MBhat*Ycut^4 + 
           1500*m2^4*MBhat*Ycut^4 - 200*m2^5*MBhat*Ycut^4 - 
           1027*MBhat^2*Ycut^4 - 34875*m2*MBhat^2*Ycut^4 - 
           18315*m2^2*MBhat^2*Ycut^4 + 595*m2^3*MBhat^2*Ycut^4 + 
           990*m2^4*MBhat^2*Ycut^4 - 250*m2^5*MBhat^2*Ycut^4 + 
           980*MBhat^3*Ycut^4 + 15720*m2*MBhat^3*Ycut^4 - 2460*m2^2*MBhat^3*
            Ycut^4 + 2800*m2^3*MBhat^3*Ycut^4 - 600*m2^4*MBhat^3*Ycut^4 - 
           330*MBhat^4*Ycut^4 - 2375*m2*MBhat^4*Ycut^4 + 1775*m2^2*MBhat^4*
            Ycut^4 - 375*m2^3*MBhat^4*Ycut^4 + 18*Ycut^5 + 468*m2*Ycut^5 + 
           1728*m2^2*Ycut^5 + 963*m2^3*Ycut^5 - 252*m2^4*Ycut^5 + 
           15*m2^5*Ycut^5 + 120*MBhat*Ycut^5 - 1200*m2*MBhat*Ycut^5 - 
           5040*m2^2*MBhat*Ycut^5 + 840*m2^3*MBhat*Ycut^5 - 
           360*m2^4*MBhat*Ycut^5 + 40*m2^5*MBhat*Ycut^5 - 
           208*MBhat^2*Ycut^5 + 1507*m2*MBhat^2*Ycut^5 + 3022*m2^2*MBhat^2*
            Ycut^5 - 283*m2^3*MBhat^2*Ycut^5 - 228*m2^4*MBhat^2*Ycut^5 + 
           50*m2^5*MBhat^2*Ycut^5 - 70*MBhat^3*Ycut^5 - 1110*m2*MBhat^3*
            Ycut^5 + 510*m2^2*MBhat^3*Ycut^5 - 650*m2^3*MBhat^3*Ycut^5 + 
           120*m2^4*MBhat^3*Ycut^5 + 140*MBhat^4*Ycut^5 + 
           405*m2*MBhat^4*Ycut^5 - 430*m2^2*MBhat^4*Ycut^5 + 
           75*m2^3*MBhat^4*Ycut^5 - 42*Ycut^6 - 24*m2*Ycut^6 + 
           654*m2^2*Ycut^6 - 183*m2^3*Ycut^6 + 15*m2^4*Ycut^6 + 
           80*MBhat*Ycut^6 - 460*m2*MBhat*Ycut^6 - 460*m2^2*MBhat*Ycut^6 + 
           20*m2^3*MBhat*Ycut^6 + 20*m2^4*MBhat*Ycut^6 + 292*MBhat^2*Ycut^6 + 
           319*m2*MBhat^2*Ycut^6 - 79*m2^2*MBhat^2*Ycut^6 + 
           78*m2^3*MBhat^2*Ycut^6 + 10*m2^4*MBhat^2*Ycut^6 - 
           320*MBhat^3*Ycut^6 - 150*m2*MBhat^3*Ycut^6 + 120*m2^2*MBhat^3*
            Ycut^6 + 30*m2^3*MBhat^3*Ycut^6 - 10*MBhat^4*Ycut^6 + 
           35*m2*MBhat^4*Ycut^6 + 25*m2^2*MBhat^4*Ycut^6 - 12*Ycut^7 + 
           144*m2*Ycut^7 - 12*m2^2*Ycut^7 - 15*m2^3*Ycut^7 - 
           320*MBhat*Ycut^7 + 180*m2*MBhat*Ycut^7 - 40*m2^2*MBhat*Ycut^7 - 
           20*m2^3*MBhat*Ycut^7 + 112*MBhat^2*Ycut^7 + 171*m2*MBhat^2*
            Ycut^7 - 88*m2^2*MBhat^2*Ycut^7 - 10*m2^3*MBhat^2*Ycut^7 + 
           280*MBhat^3*Ycut^7 - 150*m2*MBhat^3*Ycut^7 - 30*m2^2*MBhat^3*
            Ycut^7 - 60*MBhat^4*Ycut^7 - 25*m2*MBhat^4*Ycut^7 + 108*Ycut^8 - 
           108*m2*Ycut^8 + 15*m2^2*Ycut^8 + 180*MBhat*Ycut^8 - 
           180*m2*MBhat*Ycut^8 + 20*m2^2*MBhat*Ycut^8 - 293*MBhat^2*Ycut^8 + 
           98*m2*MBhat^2*Ycut^8 + 10*m2^2*MBhat^2*Ycut^8 - 
           20*MBhat^3*Ycut^8 + 30*m2*MBhat^3*Ycut^8 + 25*MBhat^4*Ycut^8 - 
           102*Ycut^9 + 60*m2*Ycut^9 + 40*MBhat*Ycut^9 - 20*m2*MBhat*Ycut^9 + 
           92*MBhat^2*Ycut^9 - 10*m2*MBhat^2*Ycut^9 - 30*MBhat^3*Ycut^9 + 
           30*Ycut^10 - 40*MBhat*Ycut^10 + 10*MBhat^2*Ycut^10))/
         (-1 + Ycut)^5 - (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 120*MBhat + 
          360*m2*MBhat + 80*m2^2*MBhat - 144*MBhat^2 - 240*m2*MBhat^2 - 
          2*m2^2*MBhat^2 + 72*MBhat^3 + 48*m2*MBhat^3 - 12*MBhat^4 + 
          3*m2*MBhat^4)*Log[m2])/2 + (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 
          120*MBhat + 360*m2*MBhat + 80*m2^2*MBhat - 144*MBhat^2 - 
          240*m2*MBhat^2 - 2*m2^2*MBhat^2 + 72*MBhat^3 + 48*m2*MBhat^3 - 
          12*MBhat^4 + 3*m2*MBhat^4)*Log[1 - Ycut])/2) + 
     c[VR]^2*(-1/90*((-1 + m2 + Ycut)*(90 + 1230*m2 - 2400*m2^2 + 720*m2^3 + 
           390*m2^4 - 30*m2^5 - 424*MBhat - 2404*m2*MBhat + 4616*m2^2*MBhat - 
           1864*m2^3*MBhat + 656*m2^4*MBhat - 100*m2^5*MBhat + 739*MBhat^2 + 
           874*m2*MBhat^2 - 206*m2^2*MBhat^2 - 686*m2^3*MBhat^2 + 
           799*m2^4*MBhat^2 - 200*m2^5*MBhat^2 - 540*MBhat^3 + 
           480*m2*MBhat^3 - 2640*m2^2*MBhat^3 + 1680*m2^3*MBhat^3 - 
           420*m2^4*MBhat^3 + 135*MBhat^4 - 225*m2*MBhat^4 + 
           855*m2^2*MBhat^4 - 225*m2^3*MBhat^4 - 180*Ycut - 3090*m2*Ycut + 
           5760*m2^2*Ycut - 1440*m2^3*Ycut - 1140*m2^4*Ycut + 90*m2^5*Ycut + 
           848*MBhat*Ycut + 6544*m2*MBhat*Ycut - 12060*m2^2*MBhat*Ycut + 
           5036*m2^3*MBhat*Ycut - 1868*m2^4*MBhat*Ycut + 300*m2^5*MBhat*
            Ycut - 1478*MBhat^2*Ycut - 3169*m2*MBhat^2*Ycut + 
           1485*m2^2*MBhat^2*Ycut + 1459*m2^3*MBhat^2*Ycut - 
           2197*m2^4*MBhat^2*Ycut + 600*m2^5*MBhat^2*Ycut + 
           1080*MBhat^3*Ycut - 780*m2*MBhat^3*Ycut + 6660*m2^2*MBhat^3*Ycut - 
           4620*m2^3*MBhat^3*Ycut + 1260*m2^4*MBhat^3*Ycut - 
           270*MBhat^4*Ycut + 585*m2*MBhat^4*Ycut - 2340*m2^2*MBhat^4*Ycut + 
           675*m2^3*MBhat^4*Ycut + 90*Ycut^2 + 2220*m2*Ycut^2 - 
           3810*m2^2*Ycut^2 + 510*m2^3*Ycut^2 + 1080*m2^4*Ycut^2 - 
           90*m2^5*Ycut^2 - 424*MBhat*Ycut^2 - 5220*m2*MBhat*Ycut^2 + 
           9180*m2^2*MBhat*Ycut^2 - 4024*m2^3*MBhat*Ycut^2 + 
           1668*m2^4*MBhat*Ycut^2 - 300*m2^5*MBhat*Ycut^2 + 
           739*MBhat^2*Ycut^2 + 3375*m2*MBhat^2*Ycut^2 - 2430*m2^2*MBhat^2*
            Ycut^2 - 461*m2^3*MBhat^2*Ycut^2 + 1797*m2^4*MBhat^2*Ycut^2 - 
           600*m2^5*MBhat^2*Ycut^2 - 540*MBhat^3*Ycut^2 - 
           60*m2*MBhat^3*Ycut^2 - 4560*m2^2*MBhat^3*Ycut^2 + 
           3780*m2^3*MBhat^3*Ycut^2 - 1260*m2^4*MBhat^3*Ycut^2 + 
           135*MBhat^4*Ycut^2 - 360*m2*MBhat^4*Ycut^2 + 1890*m2^2*MBhat^4*
            Ycut^2 - 675*m2^3*MBhat^4*Ycut^2 - 240*m2*Ycut^3 + 
           240*m2^2*Ycut^3 + 270*m2^3*Ycut^3 - 300*m2^4*Ycut^3 + 
           30*m2^5*Ycut^3 + 720*m2*MBhat*Ycut^3 - 1080*m2^2*MBhat*Ycut^3 + 
           496*m2^3*MBhat*Ycut^3 - 356*m2^4*MBhat*Ycut^3 + 
           100*m2^5*MBhat*Ycut^3 - 720*MBhat^2*Ycut^3 + 1620*m2^2*MBhat^2*
            Ycut^3 - 1231*m2^3*MBhat^2*Ycut^3 - 199*m2^4*MBhat^2*Ycut^3 + 
           200*m2^5*MBhat^2*Ycut^3 + 1080*MBhat^3*Ycut^3 + 
           240*m2*MBhat^3*Ycut^3 - 960*m2^2*MBhat^3*Ycut^3 - 
           420*m2^3*MBhat^3*Ycut^3 + 420*m2^4*MBhat^3*Ycut^3 - 
           360*MBhat^4*Ycut^3 - 360*m2*MBhat^4*Ycut^3 - 180*m2^2*MBhat^4*
            Ycut^3 + 225*m2^3*MBhat^4*Ycut^3 - 60*m2*Ycut^4 + 
           90*m2^2*Ycut^4 - 30*m2^4*Ycut^4 + 780*MBhat*Ycut^4 - 
           960*m2*MBhat*Ycut^4 - 420*m2^2*MBhat*Ycut^4 + 676*m2^3*MBhat*
            Ycut^4 - 100*m2^4*MBhat*Ycut^4 + 795*MBhat^2*Ycut^4 - 
           1380*m2*MBhat^2*Ycut^4 - 390*m2^2*MBhat^2*Ycut^4 + 
           1109*m2^3*MBhat^2*Ycut^4 - 200*m2^4*MBhat^2*Ycut^4 - 
           2520*MBhat^3*Ycut^4 + 600*m2*MBhat^3*Ycut^4 + 1980*m2^2*MBhat^3*
            Ycut^4 - 420*m2^3*MBhat^3*Ycut^4 + 945*MBhat^4*Ycut^4 + 
           585*m2*MBhat^4*Ycut^4 - 225*m2^2*MBhat^4*Ycut^4 - 240*Ycut^5 + 
           360*m2*Ycut^5 - 120*m2^3*Ycut^5 - 1716*MBhat*Ycut^5 + 
           2244*m2*MBhat*Ycut^5 - 336*m2^2*MBhat*Ycut^5 - 
           200*m2^3*MBhat*Ycut^5 + 1146*MBhat^2*Ycut^5 - 189*m2*MBhat^2*
            Ycut^5 - 129*m2^2*MBhat^2*Ycut^5 - 250*m2^3*MBhat^2*Ycut^5 + 
           1620*MBhat^3*Ycut^5 - 660*m2*MBhat^3*Ycut^5 - 480*m2^2*MBhat^3*
            Ycut^5 - 810*MBhat^4*Ycut^5 - 225*m2*MBhat^4*Ycut^5 + 
           630*Ycut^6 - 750*m2*Ycut^6 + 120*m2^2*Ycut^6 + 892*MBhat*Ycut^6 - 
           824*m2*MBhat*Ycut^6 + 100*m2^2*MBhat*Ycut^6 - 
           1747*MBhat^2*Ycut^6 + 539*m2*MBhat^2*Ycut^6 + 50*m2^2*MBhat^2*
            Ycut^6 + 180*m2*MBhat^3*Ycut^6 + 225*MBhat^4*Ycut^6 - 
           540*Ycut^7 + 330*m2*Ycut^7 + 244*MBhat*Ycut^7 - 
           100*m2*MBhat*Ycut^7 + 476*MBhat^2*Ycut^7 - 50*m2*MBhat^2*Ycut^7 - 
           180*MBhat^3*Ycut^7 + 150*Ycut^8 - 200*MBhat*Ycut^8 + 
           50*MBhat^2*Ycut^8))/(-1 + Ycut)^3 - 
       (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 36*m2*MBhat + 
          8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 13*m2^2*MBhat^2 - 
          12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*Log[m2])/3 + 
       (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 36*m2*MBhat + 
          8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 13*m2^2*MBhat^2 - 
          12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*Log[1 - Ycut])/3) + 
     c[VL]^2*(-1/90*((-1 + m2 + Ycut)*(90 + 1230*m2 - 2400*m2^2 + 720*m2^3 + 
           390*m2^4 - 30*m2^5 - 424*MBhat - 2404*m2*MBhat + 4616*m2^2*MBhat - 
           1864*m2^3*MBhat + 656*m2^4*MBhat - 100*m2^5*MBhat + 739*MBhat^2 + 
           874*m2*MBhat^2 - 206*m2^2*MBhat^2 - 686*m2^3*MBhat^2 + 
           799*m2^4*MBhat^2 - 200*m2^5*MBhat^2 - 540*MBhat^3 + 
           480*m2*MBhat^3 - 2640*m2^2*MBhat^3 + 1680*m2^3*MBhat^3 - 
           420*m2^4*MBhat^3 + 135*MBhat^4 - 225*m2*MBhat^4 + 
           855*m2^2*MBhat^4 - 225*m2^3*MBhat^4 - 360*Ycut - 5550*m2*Ycut + 
           10560*m2^2*Ycut - 2880*m2^3*Ycut - 1920*m2^4*Ycut + 
           150*m2^5*Ycut + 1696*MBhat*Ycut + 11352*m2*MBhat*Ycut - 
           21292*m2^2*MBhat*Ycut + 8764*m2^3*MBhat*Ycut - 
           3180*m2^4*MBhat*Ycut + 500*m2^5*MBhat*Ycut - 2956*MBhat^2*Ycut - 
           4917*m2*MBhat^2*Ycut + 1897*m2^2*MBhat^2*Ycut + 
           2831*m2^3*MBhat^2*Ycut - 3795*m2^4*MBhat^2*Ycut + 
           1000*m2^5*MBhat^2*Ycut + 2160*MBhat^3*Ycut - 1740*m2*MBhat^3*
            Ycut + 11940*m2^2*MBhat^3*Ycut - 7980*m2^3*MBhat^3*Ycut + 
           2100*m2^4*MBhat^3*Ycut - 540*MBhat^4*Ycut + 1035*m2*MBhat^4*Ycut - 
           4050*m2^2*MBhat^4*Ycut + 1125*m2^3*MBhat^4*Ycut + 540*Ycut^2 + 
           9630*m2*Ycut^2 - 17730*m2^2*Ycut^2 + 4110*m2^3*Ycut^2 + 
           3750*m2^4*Ycut^2 - 300*m2^5*Ycut^2 - 2544*MBhat*Ycut^2 - 
           20712*m2*MBhat*Ycut^2 + 37916*m2^2*MBhat*Ycut^2 - 
           15960*m2^3*MBhat*Ycut^2 + 6060*m2^4*MBhat*Ycut^2 - 
           1000*m2^5*MBhat*Ycut^2 + 4434*MBhat^2*Ycut^2 + 10587*m2*MBhat^2*
            Ycut^2 - 5606*m2^2*MBhat^2*Ycut^2 - 4065*m2^3*MBhat^2*Ycut^2 + 
           6990*m2^4*MBhat^2*Ycut^2 - 2000*m2^5*MBhat^2*Ycut^2 - 
           3240*MBhat^3*Ycut^2 + 1980*m2*MBhat^3*Ycut^2 - 20520*m2^2*MBhat^3*
            Ycut^2 + 14700*m2^3*MBhat^3*Ycut^2 - 4200*m2^4*MBhat^3*Ycut^2 + 
           810*MBhat^4*Ycut^2 - 1755*m2*MBhat^4*Ycut^2 + 7425*m2^2*MBhat^4*
            Ycut^2 - 2250*m2^3*MBhat^4*Ycut^2 - 360*Ycut^3 - 7770*m2*Ycut^3 + 
           13620*m2^2*Ycut^3 - 2190*m2^3*Ycut^3 - 3600*m2^4*Ycut^3 + 
           300*m2^5*Ycut^3 + 1696*MBhat*Ycut^3 + 17704*m2*MBhat*Ycut^3 - 
           31500*m2^2*MBhat*Ycut^3 + 13580*m2^3*MBhat*Ycut^3 - 
           5560*m2^4*MBhat*Ycut^3 + 1000*m2^5*MBhat*Ycut^3 - 
           2776*MBhat^2*Ycut^3 - 11719*m2*MBhat^2*Ycut^3 + 
           7965*m2^2*MBhat^2*Ycut^3 + 2950*m2^3*MBhat^2*Ycut^3 - 
           6890*m2^4*MBhat^2*Ycut^3 + 2000*m2^5*MBhat^2*Ycut^3 + 
           1860*MBhat^3*Ycut^3 + 840*m2*MBhat^3*Ycut^3 + 16440*m2^2*MBhat^3*
            Ycut^3 - 14100*m2^3*MBhat^3*Ycut^3 + 4200*m2^4*MBhat^3*Ycut^3 - 
           420*MBhat^4*Ycut^3 + 1065*m2*MBhat^4*Ycut^3 - 6900*m2^2*MBhat^4*
            Ycut^3 + 2250*m2^3*MBhat^4*Ycut^3 + 90*Ycut^4 + 2640*m2*Ycut^4 - 
           4200*m2^2*Ycut^4 - 30*m2^3*Ycut^4 + 1650*m2^4*Ycut^4 - 
           150*m2^5*Ycut^4 - 544*MBhat*Ycut^4 - 5820*m2*MBhat*Ycut^4 + 
           10920*m2^2*MBhat*Ycut^4 - 6140*m2^3*MBhat*Ycut^4 + 
           3180*m2^4*MBhat*Ycut^4 - 500*m2^5*MBhat*Ycut^4 + 
           244*MBhat^2*Ycut^4 + 7755*m2*MBhat^2*Ycut^4 - 8040*m2^2*MBhat^2*
            Ycut^4 + 710*m2^3*MBhat^2*Ycut^4 + 3345*m2^4*MBhat^2*Ycut^4 - 
           1000*m2^5*MBhat^2*Ycut^4 + 480*MBhat^3*Ycut^4 - 
           4740*m2*MBhat^3*Ycut^4 - 4560*m2^2*MBhat^3*Ycut^4 + 
           7200*m2^3*MBhat^3*Ycut^4 - 2100*m2^4*MBhat^3*Ycut^4 - 
           270*MBhat^4*Ycut^4 + 435*m2*MBhat^4*Ycut^4 + 3525*m2^2*MBhat^4*
            Ycut^4 - 1125*m2^3*MBhat^4*Ycut^4 + 30*Ycut^5 - 300*m2*Ycut^5 + 
           60*m2^2*Ycut^5 + 690*m2^3*Ycut^5 - 510*m2^4*Ycut^5 + 
           30*m2^5*Ycut^5 + 384*MBhat*Ycut^5 - 1416*m2*MBhat*Ycut^5 + 
           324*m2^2*MBhat*Ycut^5 + 1404*m2^3*MBhat*Ycut^5 - 
           876*m2^4*MBhat*Ycut^5 + 100*m2^5*MBhat*Ycut^5 + 
           276*MBhat^2*Ycut^5 - 3339*m2*MBhat^2*Ycut^5 + 6801*m2^2*MBhat^2*
            Ycut^5 - 2949*m2^3*MBhat^2*Ycut^5 - 609*m2^4*MBhat^2*Ycut^5 + 
           200*m2^5*MBhat^2*Ycut^5 - 1110*MBhat^3*Ycut^5 + 
           5370*m2*MBhat^3*Ycut^5 - 2310*m2^2*MBhat^3*Ycut^5 - 
           1530*m2^3*MBhat^3*Ycut^5 + 420*m2^4*MBhat^3*Ycut^5 + 
           420*MBhat^4*Ycut^5 - 945*m2*MBhat^4*Ycut^5 - 930*m2^2*MBhat^4*
            Ycut^5 + 225*m2^3*MBhat^4*Ycut^5 - 105*Ycut^6 + 405*m2*Ycut^6 + 
           75*m2^2*Ycut^6 - 435*m2^3*Ycut^6 + 60*m2^4*Ycut^6 - 
           356*MBhat*Ycut^6 + 1708*m2*MBhat*Ycut^6 - 1568*m2^2*MBhat*Ycut^6 + 
           296*m2^3*MBhat*Ycut^6 + 80*m2^4*MBhat*Ycut^6 + 
           251*MBhat^2*Ycut^6 + 662*m2*MBhat^2*Ycut^6 - 3247*m2^2*MBhat^2*
            Ycut^6 + 1414*m2^3*MBhat^2*Ycut^6 - 20*m2^4*MBhat^2*Ycut^6 + 
           240*MBhat^3*Ycut^6 - 2670*m2*MBhat^3*Ycut^6 + 1980*m2^2*MBhat^3*
            Ycut^6 + 30*m2^3*MBhat^3*Ycut^6 - 30*MBhat^4*Ycut^6 + 
           465*m2*MBhat^4*Ycut^6 + 75*m2^2*MBhat^4*Ycut^6 + 120*Ycut^7 - 
           375*m2*Ycut^7 + 30*m2^2*Ycut^7 + 15*m2^3*Ycut^7 - 
           16*MBhat*Ycut^7 - 348*m2*MBhat*Ycut^7 + 604*m2^2*MBhat*Ycut^7 - 
           80*m2^3*MBhat*Ycut^7 - 164*MBhat^2*Ycut^7 + 168*m2*MBhat^2*
            Ycut^7 + 431*m2^2*MBhat^2*Ycut^7 - 205*m2^3*MBhat^2*Ycut^7 + 
           240*MBhat^3*Ycut^7 + 450*m2*MBhat^3*Ycut^7 - 330*m2^2*MBhat^3*
            Ycut^7 - 180*MBhat^4*Ycut^7 - 75*m2*MBhat^4*Ycut^7 - 30*Ycut^8 + 
           75*m2*Ycut^8 - 15*m2^2*Ycut^8 + 144*MBhat*Ycut^8 - 
           84*m2*MBhat*Ycut^8 - 20*m2^2*MBhat*Ycut^8 - 129*MBhat^2*Ycut^8 - 
           66*m2*MBhat^2*Ycut^8 + 5*m2^2*MBhat^2*Ycut^8 - 60*MBhat^3*Ycut^8 + 
           30*m2*MBhat^3*Ycut^8 + 75*MBhat^4*Ycut^8 - 30*Ycut^9 + 
           15*m2*Ycut^9 - 16*MBhat*Ycut^9 + 20*m2*MBhat*Ycut^9 + 
           76*MBhat^2*Ycut^9 - 5*m2*MBhat^2*Ycut^9 - 30*MBhat^3*Ycut^9 + 
           15*Ycut^10 - 20*MBhat*Ycut^10 + 5*MBhat^2*Ycut^10))/
         (-1 + Ycut)^5 - (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 
          36*m2*MBhat + 8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 
          13*m2^2*MBhat^2 - 12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*
         Log[m2])/3 + (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 
          36*m2*MBhat + 8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 
          13*m2^2*MBhat^2 - 12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*
         Log[1 - Ycut])/3) + 
     c[T]^2*((-2*(-1 + m2 + Ycut)*(69 + 1569*m2 + 234*m2^2 + 474*m2^3 + 
          189*m2^4 - 15*m2^5 - 368*MBhat - 4472*m2*MBhat + 688*m2^2*MBhat - 
          944*m2^3*MBhat + 352*m2^4*MBhat - 56*m2^5*MBhat + 794*MBhat^2 + 
          4088*m2*MBhat^2 + 158*m2^2*MBhat^2 - 226*m2^3*MBhat^2 + 
          476*m2^4*MBhat^2 - 130*m2^5*MBhat^2 - 720*MBhat^3 - 
          1464*m2*MBhat^3 - 1080*m2^2*MBhat^3 + 936*m2^3*MBhat^3 - 
          264*m2^4*MBhat^3 + 225*MBhat^4 + 225*m2*MBhat^4 + 
          441*m2^2*MBhat^4 - 135*m2^3*MBhat^4 - 276*Ycut - 6927*m2*Ycut - 
          1278*m2^2*Ycut - 2004*m2^3*Ycut - 930*m2^4*Ycut + 75*m2^5*Ycut + 
          1472*MBhat*Ycut + 20112*m2*MBhat*Ycut - 2984*m2^2*MBhat*Ycut + 
          4424*m2^3*MBhat*Ycut - 1704*m2^4*MBhat*Ycut + 280*m2^5*MBhat*Ycut - 
          3176*MBhat^2*Ycut - 19014*m2*MBhat^2*Ycut - 214*m2^2*MBhat^2*Ycut + 
          784*m2^3*MBhat^2*Ycut - 2250*m2^4*MBhat^2*Ycut + 
          650*m2^5*MBhat^2*Ycut + 2880*MBhat^3*Ycut + 7152*m2*MBhat^3*Ycut + 
          4728*m2^2*MBhat^3*Ycut - 4416*m2^3*MBhat^3*Ycut + 
          1320*m2^4*MBhat^3*Ycut - 900*MBhat^4*Ycut - 1107*m2*MBhat^4*Ycut - 
          2070*m2^2*MBhat^4*Ycut + 675*m2^3*MBhat^4*Ycut + 414*Ycut^2 + 
          11727*m2*Ycut^2 + 2769*m2^2*Ycut^2 + 3165*m2^3*Ycut^2 + 
          1815*m2^4*Ycut^2 - 150*m2^5*Ycut^2 - 2208*MBhat*Ycut^2 - 
          34800*m2*MBhat*Ycut^2 + 4744*m2^2*MBhat*Ycut^2 - 
          8016*m2^3*MBhat*Ycut^2 + 3240*m2^4*MBhat*Ycut^2 - 
          560*m2^5*MBhat*Ycut^2 + 4764*MBhat^2*Ycut^2 + 34242*m2*MBhat^2*
           Ycut^2 - 736*m2^2*MBhat^2*Ycut^2 - 660*m2^3*MBhat^2*Ycut^2 + 
          4110*m2^4*MBhat^2*Ycut^2 - 1300*m2^5*MBhat^2*Ycut^2 - 
          4320*MBhat^3*Ycut^2 - 13680*m2*MBhat^3*Ycut^2 - 
          7704*m2^2*MBhat^3*Ycut^2 + 8040*m2^3*MBhat^3*Ycut^2 - 
          2640*m2^4*MBhat^3*Ycut^2 + 1350*MBhat^4*Ycut^2 + 
          2187*m2*MBhat^4*Ycut^2 + 3735*m2^2*MBhat^4*Ycut^2 - 
          1350*m2^3*MBhat^4*Ycut^2 - 276*Ycut^3 - 9189*m2*Ycut^3 - 
          2940*m2^2*Ycut^3 - 2175*m2^3*Ycut^3 - 1740*m2^4*Ycut^3 + 
          150*m2^5*Ycut^3 + 1472*MBhat*Ycut^3 + 28016*m2*MBhat*Ycut^3 - 
          3048*m2^2*MBhat*Ycut^3 + 6760*m2^3*MBhat*Ycut^3 - 
          2960*m2^4*MBhat*Ycut^3 + 560*m2^5*MBhat*Ycut^3 - 
          3668*MBhat^2*Ycut^3 - 28442*m2*MBhat^2*Ycut^3 + 
          2010*m2^2*MBhat^2*Ycut^3 - 670*m2^3*MBhat^2*Ycut^3 - 
          3640*m2^4*MBhat^2*Ycut^3 + 1300*m2^5*MBhat^2*Ycut^3 + 
          3636*MBhat^3*Ycut^3 + 12348*m2*MBhat^3*Ycut^3 + 
          5028*m2^2*MBhat^3*Ycut^3 - 7020*m2^3*MBhat^3*Ycut^3 + 
          2640*m2^4*MBhat^3*Ycut^3 - 1164*MBhat^4*Ycut^3 - 
          2289*m2*MBhat^4*Ycut^3 - 3180*m2^2*MBhat^4*Ycut^3 + 
          1350*m2^3*MBhat^4*Ycut^3 + 69*Ycut^4 + 3000*m2*Ycut^4 + 
          1470*m2^2*Ycut^4 + 495*m2^3*Ycut^4 + 795*m2^4*Ycut^4 - 
          75*m2^5*Ycut^4 - 20*MBhat*Ycut^4 - 9960*m2*MBhat*Ycut^4 + 
          360*m2^2*MBhat*Ycut^4 - 2440*m2^3*MBhat*Ycut^4 + 
          1380*m2^4*MBhat*Ycut^4 - 280*m2^5*MBhat*Ycut^4 + 
          2489*MBhat^2*Ycut^4 + 8457*m2*MBhat^2*Ycut^4 - 2139*m2^2*MBhat^2*
           Ycut^4 + 2275*m2^3*MBhat^2*Ycut^4 + 1350*m2^4*MBhat^2*Ycut^4 - 
          650*m2^5*MBhat^2*Ycut^4 - 3924*MBhat^3*Ycut^4 - 
          4248*m2*MBhat^3*Ycut^4 + 588*m2^2*MBhat^3*Ycut^4 + 
          2640*m2^3*MBhat^3*Ycut^4 - 1320*m2^4*MBhat^3*Ycut^4 + 
          1386*MBhat^4*Ycut^4 + 1545*m2*MBhat^4*Ycut^4 + 1155*m2^2*MBhat^4*
           Ycut^4 - 675*m2^3*MBhat^4*Ycut^4 - 90*Ycut^5 - 12*m2*Ycut^5 - 
          264*m2^2*Ycut^5 + 99*m2^3*Ycut^5 - 168*m2^4*Ycut^5 + 
          15*m2^5*Ycut^5 - 1440*MBhat*Ycut^5 + 2328*m2*MBhat*Ycut^5 + 
          216*m2^2*MBhat*Ycut^5 - 144*m2^3*MBhat*Ycut^5 - 
          216*m2^4*MBhat*Ycut^5 + 56*m2^5*MBhat*Ycut^5 - 
          1656*MBhat^2*Ycut^5 + 2115*m2*MBhat^2*Ycut^5 + 1302*m2^2*MBhat^2*
           Ycut^5 - 2379*m2^3*MBhat^2*Ycut^5 + 12*m2^4*MBhat^2*Ycut^5 + 
          130*m2^5*MBhat^2*Ycut^5 + 5190*MBhat^3*Ycut^5 - 
          1146*m2*MBhat^3*Ycut^5 - 2910*m2^2*MBhat^3*Ycut^5 - 
          6*m2^3*MBhat^3*Ycut^5 + 264*m2^4*MBhat^3*Ycut^5 - 
          2004*MBhat^4*Ycut^5 - 891*m2*MBhat^4*Ycut^5 - 6*m2^2*MBhat^4*
           Ycut^5 + 135*m2^3*MBhat^4*Ycut^5 + 408*Ycut^6 - 546*m2*Ycut^6 + 
          96*m2^2*Ycut^6 - 21*m2^3*Ycut^6 + 3*m2^4*Ycut^6 + 
          2216*MBhat*Ycut^6 - 2500*m2*MBhat*Ycut^6 + 68*m2^2*MBhat*Ycut^6 + 
          460*m2^3*MBhat*Ycut^6 - 20*m2^4*MBhat*Ycut^6 - 470*MBhat^2*Ycut^6 - 
          1451*m2*MBhat^2*Ycut^6 - 353*m2^2*MBhat^2*Ycut^6 + 
          1052*m2^3*MBhat^2*Ycut^6 - 94*m2^4*MBhat^2*Ycut^6 - 
          3840*MBhat^3*Ycut^6 + 1446*m2*MBhat^3*Ycut^6 + 1656*m2^2*MBhat^3*
           Ycut^6 - 174*m2^3*MBhat^3*Ycut^6 + 1686*MBhat^4*Ycut^6 + 
          435*m2*MBhat^4*Ycut^6 - 75*m2^2*MBhat^4*Ycut^6 - 732*Ycut^7 + 
          750*m2*Ycut^7 - 120*m2^2*Ycut^7 - 33*m2^3*Ycut^7 - 
          1424*MBhat*Ycut^7 + 1548*m2*MBhat*Ycut^7 - 64*m2^2*MBhat*Ycut^7 - 
          100*m2^3*MBhat*Ycut^7 + 1760*MBhat^2*Ycut^7 - 135*m2*MBhat^2*
           Ycut^7 - 44*m2^2*MBhat^2*Ycut^7 - 176*m2^3*MBhat^2*Ycut^7 + 
          1080*MBhat^3*Ycut^7 - 474*m2*MBhat^3*Ycut^7 - 306*m2^2*MBhat^3*
           Ycut^7 - 684*MBhat^4*Ycut^7 - 105*m2*MBhat^4*Ycut^7 + 648*Ycut^8 - 
          474*m2*Ycut^8 + 33*m2^2*Ycut^8 + 156*MBhat*Ycut^8 - 
          252*m2*MBhat*Ycut^8 + 20*m2^2*MBhat*Ycut^8 - 993*MBhat^2*Ycut^8 + 
          156*m2*MBhat^2*Ycut^8 + 16*m2^2*MBhat^2*Ycut^8 + 
          84*MBhat^3*Ycut^8 + 66*m2*MBhat^3*Ycut^8 + 105*MBhat^4*Ycut^8 - 
          282*Ycut^9 + 102*m2*Ycut^9 + 208*MBhat*Ycut^9 - 
          20*m2*MBhat*Ycut^9 + 140*MBhat^2*Ycut^9 - 16*m2*MBhat^2*Ycut^9 - 
          66*MBhat^3*Ycut^9 + 48*Ycut^10 - 64*MBhat*Ycut^10 + 
          16*MBhat^2*Ycut^10))/(9*(-1 + Ycut)^5) - 
       (8*m2*(60 + 105*m2 + 45*m2^3 - 216*MBhat - 168*m2*MBhat - 
          16*m2^2*MBhat + 288*MBhat^2 + 84*m2*MBhat^2 + 58*m2^2*MBhat^2 - 
          168*MBhat^3 - 48*m2*MBhat^3 + 36*MBhat^4 + 27*m2*MBhat^4)*Log[m2])/
        3 + (8*m2*(60 + 105*m2 + 45*m2^3 - 216*MBhat - 168*m2*MBhat - 
          16*m2^2*MBhat + 288*MBhat^2 + 84*m2*MBhat^2 + 58*m2^2*MBhat^2 - 
          168*MBhat^3 - 48*m2*MBhat^3 + 36*MBhat^4 + 27*m2*MBhat^4)*
         Log[1 - Ycut])/3) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(391 - 574*m2 - 2514*m2^2 + 146*m2^3 + 
          31*m2^4 - 1336*MBhat + 3464*m2*MBhat + 2664*m2^2*MBhat - 
          536*m2^3*MBhat + 64*m2^4*MBhat + 1575*MBhat^2 - 4620*m2*MBhat^2 + 
          820*m2^2*MBhat^2 - 260*m2^3*MBhat^2 + 85*m2^4*MBhat^2 - 
          760*MBhat^3 + 1960*m2*MBhat^3 - 920*m2^2*MBhat^3 + 
          200*m2^3*MBhat^3 + 130*MBhat^4 - 140*m2*MBhat^4 + 
          130*m2^2*MBhat^4 - 1293*Ycut + 1693*m2*Ycut + 8979*m2^2*Ycut - 
          435*m2^3*Ycut - 124*m2^4*Ycut + 4488*MBhat*Ycut - 
          11248*m2*MBhat*Ycut - 10184*m2^2*MBhat*Ycut + 2080*m2^3*MBhat*
           Ycut - 256*m2^4*MBhat*Ycut - 5445*MBhat^2*Ycut + 
          15795*m2*MBhat^2*Ycut - 2565*m2^2*MBhat^2*Ycut + 
          955*m2^3*MBhat^2*Ycut - 340*m2^4*MBhat^2*Ycut + 2760*MBhat^3*Ycut - 
          7120*m2*MBhat^3*Ycut + 3480*m2^2*MBhat^3*Ycut - 
          800*m2^3*MBhat^3*Ycut - 510*MBhat^4*Ycut + 610*m2*MBhat^4*Ycut - 
          520*m2^2*MBhat^4*Ycut + 1473*Ycut^2 - 1574*m2*Ycut^2 - 
          11375*m2^2*Ycut^2 + 370*m2^3*Ycut^2 + 186*m2^4*Ycut^2 - 
          5208*MBhat*Ycut^2 + 12344*m2*MBhat*Ycut^2 + 14160*m2^2*MBhat*
           Ycut^2 - 2960*m2^3*MBhat*Ycut^2 + 384*m2^4*MBhat*Ycut^2 + 
          6525*MBhat^2*Ycut^2 - 18630*m2*MBhat^2*Ycut^2 + 
          2415*m2^2*MBhat^2*Ycut^2 - 1220*m2^3*MBhat^2*Ycut^2 + 
          510*m2^4*MBhat^2*Ycut^2 - 3480*MBhat^3*Ycut^2 + 
          9080*m2*MBhat^3*Ycut^2 - 4720*m2^2*MBhat^3*Ycut^2 + 
          1200*m2^3*MBhat^3*Ycut^2 + 690*MBhat^4*Ycut^2 - 
          950*m2*MBhat^4*Ycut^2 + 780*m2^2*MBhat^4*Ycut^2 - 611*Ycut^3 + 
          375*m2*Ycut^3 + 5600*m2^2*Ycut^3 + 10*m2^3*Ycut^3 - 
          124*m2^4*Ycut^3 + 2216*MBhat*Ycut^3 - 4640*m2*MBhat*Ycut^3 - 
          8080*m2^2*MBhat*Ycut^3 + 1760*m2^3*MBhat*Ycut^3 - 
          256*m2^4*MBhat*Ycut^3 - 2595*MBhat^2*Ycut^3 + 7515*m2*MBhat^2*
           Ycut^3 - 290*m2^2*MBhat^2*Ycut^3 + 710*m2^3*MBhat^2*Ycut^3 - 
          340*m2^4*MBhat^2*Ycut^3 + 1220*MBhat^3*Ycut^3 - 
          4200*m2*MBhat^3*Ycut^3 + 2780*m2^2*MBhat^3*Ycut^3 - 
          800*m2^3*MBhat^3*Ycut^3 - 230*MBhat^4*Ycut^3 + 
          680*m2*MBhat^4*Ycut^3 - 520*m2^2*MBhat^4*Ycut^3 + 30*Ycut^4 + 
          65*m2*Ycut^4 - 525*m2^2*Ycut^4 - 105*m2^3*Ycut^4 + 31*m2^4*Ycut^4 - 
          340*MBhat*Ycut^4 + 300*m2*MBhat*Ycut^4 + 1300*m2^2*MBhat*Ycut^4 - 
          460*m2^3*MBhat*Ycut^4 + 64*m2^4*MBhat*Ycut^4 - 410*MBhat^2*Ycut^4 + 
          895*m2*MBhat^2*Ycut^4 - 935*m2^2*MBhat^2*Ycut^4 - 
          115*m2^3*MBhat^2*Ycut^4 + 85*m2^4*MBhat^2*Ycut^4 + 
          1020*MBhat^3*Ycut^4 - 380*m2*MBhat^3*Ycut^4 - 600*m2^2*MBhat^3*
           Ycut^4 + 200*m2^3*MBhat^3*Ycut^4 - 300*MBhat^4*Ycut^4 - 
          250*m2*MBhat^4*Ycut^4 + 130*m2^2*MBhat^4*Ycut^4 + 64*Ycut^5 - 
          51*m2*Ycut^5 - 156*m2^2*Ycut^5 + 59*m2^3*Ycut^5 + 
          506*MBhat*Ycut^5 - 634*m2*MBhat*Ycut^5 + 246*m2^2*MBhat*Ycut^5 + 
          26*m2^3*MBhat*Ycut^5 + 150*MBhat^2*Ycut^5 - 1055*m2*MBhat^2*
           Ycut^5 + 650*m2^2*MBhat^2*Ycut^5 - 25*m2^3*MBhat^2*Ycut^5 - 
          1020*MBhat^3*Ycut^5 + 880*m2*MBhat^3*Ycut^5 - 20*m2^2*MBhat^3*
           Ycut^5 + 300*MBhat^4*Ycut^5 + 50*m2*MBhat^4*Ycut^5 - 152*Ycut^6 + 
          137*m2*Ycut^6 - 9*m2^2*Ycut^6 - 358*MBhat*Ycut^6 + 
          448*m2*MBhat*Ycut^6 - 106*m2^2*MBhat*Ycut^6 + 330*MBhat^2*Ycut^6 + 
          85*m2*MBhat^2*Ycut^6 - 95*m2^2*MBhat^2*Ycut^6 + 
          260*MBhat^3*Ycut^6 - 220*m2*MBhat^3*Ycut^6 - 80*MBhat^4*Ycut^6 + 
          132*Ycut^7 - 71*m2*Ycut^7 - 2*MBhat*Ycut^7 - 34*m2*MBhat*Ycut^7 - 
          130*MBhat^2*Ycut^7 + 15*m2*MBhat^2*Ycut^7 - 34*Ycut^8 + 
          34*MBhat*Ycut^8))/(15*(-1 + Ycut)^4) - 
       4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4 - 8*MBhat + 
         80*m2^2*MBhat + 12*MBhat^2 - 18*m2*MBhat^2 - 43*m2^2*MBhat^2 + 
         9*m2^3*MBhat^2 - 8*MBhat^3 + 16*m2*MBhat^3 + 2*MBhat^4 - 
         3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2] + 
       4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4 - 8*MBhat + 
         80*m2^2*MBhat + 12*MBhat^2 - 18*m2*MBhat^2 - 43*m2^2*MBhat^2 + 
         9*m2^3*MBhat^2 - 8*MBhat^3 + 16*m2*MBhat^3 + 2*MBhat^4 - 
         3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[1 - Ycut]) + 
     c[SL]*((Ycut^3*(-1 + m2 + Ycut)^2*(20*MBhat^2 + 20*m2*MBhat^2 - 
          100*m2^2*MBhat^2 + 60*m2^3*MBhat^2 - 28*MBhat^3 - 72*m2*MBhat^3 + 
          100*m2^2*MBhat^3 + 8*MBhat^4 + 40*m2*MBhat^4 - 20*MBhat*Ycut - 
          20*m2*MBhat*Ycut + 100*m2^2*MBhat*Ycut - 60*m2^3*MBhat*Ycut - 
          58*MBhat^2*Ycut + 58*m2*MBhat^2*Ycut + 90*m2^2*MBhat^2*Ycut - 
          90*m2^3*MBhat^2*Ycut + 112*MBhat^3*Ycut + 120*m2*MBhat^3*Ycut - 
          200*m2^2*MBhat^3*Ycut - 34*MBhat^4*Ycut - 100*m2*MBhat^4*Ycut + 
          6*Ycut^2 + 6*m2*Ycut^2 - 30*m2^2*Ycut^2 + 18*m2^3*Ycut^2 + 
          80*MBhat*Ycut^2 - 16*m2*MBhat*Ycut^2 - 112*m2^2*MBhat*Ycut^2 + 
          48*m2^3*MBhat*Ycut^2 + 18*MBhat^2*Ycut^2 - 178*m2*MBhat^2*Ycut^2 + 
          42*m2^2*MBhat^2*Ycut^2 + 54*m2^3*MBhat^2*Ycut^2 - 
          158*MBhat^3*Ycut^2 - 4*m2*MBhat^3*Ycut^2 + 130*m2^2*MBhat^3*
           Ycut^2 + 54*MBhat^4*Ycut^2 + 80*m2*MBhat^4*Ycut^2 - 27*Ycut^3 + 
          33*m2^2*Ycut^3 - 6*m2^3*Ycut^3 - 108*MBhat*Ycut^3 + 
          96*m2*MBhat*Ycut^3 + 8*m2^2*MBhat*Ycut^3 - 12*m2^3*MBhat*Ycut^3 + 
          91*MBhat^2*Ycut^3 + 96*m2*MBhat^2*Ycut^3 - 41*m2^2*MBhat^2*Ycut^3 - 
          12*m2^3*MBhat^2*Ycut^3 + 82*MBhat^3*Ycut^3 - 64*m2*MBhat^3*Ycut^3 - 
          30*m2^2*MBhat^3*Ycut^3 - 38*MBhat^4*Ycut^3 - 20*m2*MBhat^4*Ycut^3 + 
          45*Ycut^4 - 18*m2*Ycut^4 - 3*m2^2*Ycut^4 + 44*MBhat*Ycut^4 - 
          64*m2*MBhat*Ycut^4 + 4*m2^2*MBhat*Ycut^4 - 101*MBhat^2*Ycut^4 + 
          10*m2*MBhat^2*Ycut^4 + 9*m2^2*MBhat^2*Ycut^4 + 2*MBhat^3*Ycut^4 + 
          20*m2*MBhat^3*Ycut^4 + 10*MBhat^4*Ycut^4 - 33*Ycut^5 + 
          12*m2*Ycut^5 + 16*MBhat*Ycut^5 + 4*m2*MBhat*Ycut^5 + 
          27*MBhat^2*Ycut^5 - 6*m2*MBhat^2*Ycut^5 - 10*MBhat^3*Ycut^5 + 
          9*Ycut^6 - 12*MBhat*Ycut^6 + 3*MBhat^2*Ycut^6)*c[T])/
        (6*(-1 + Ycut)^5) + c[SR]*(-1/10*(Sqrt[m2]*(-1 + m2 + Ycut)*
            (-491 - 3166*m2 - 2266*m2^2 + 34*m2^3 + 9*m2^4 + 1816*MBhat + 
             7016*m2*MBhat + 1416*m2^2*MBhat - 184*m2^3*MBhat + 
             16*m2^4*MBhat - 2475*MBhat^2 - 4980*m2*MBhat^2 + 
             300*m2^2*MBhat^2 - 60*m2^3*MBhat^2 + 15*m2^4*MBhat^2 + 
             1480*MBhat^3 + 1160*m2*MBhat^3 - 280*m2^2*MBhat^3 + 
             40*m2^3*MBhat^3 - 330*MBhat^4 - 60*m2*MBhat^4 + 
             30*m2^2*MBhat^4 + 1593*Ycut + 10867*m2*Ycut + 8241*m2^2*Ycut - 
             85*m2^3*Ycut - 36*m2^4*Ycut - 5928*MBhat*Ycut - 
             24512*m2*MBhat*Ycut - 5496*m2^2*MBhat*Ycut + 720*m2^3*MBhat*
              Ycut - 64*m2^4*MBhat*Ycut + 8145*MBhat^2*Ycut + 
             17865*m2*MBhat^2*Ycut - 975*m2^2*MBhat^2*Ycut + 
             225*m2^3*MBhat^2*Ycut - 60*m2^4*MBhat^2*Ycut - 
             4920*MBhat^3*Ycut - 4400*m2*MBhat^3*Ycut + 1080*m2^2*MBhat^3*
              Ycut - 160*m2^3*MBhat^3*Ycut + 1110*MBhat^4*Ycut + 
             270*m2*MBhat^4*Ycut - 120*m2^2*MBhat^4*Ycut - 1773*Ycut^2 - 
             13046*m2*Ycut^2 - 10745*m2^2*Ycut^2 + 30*m2^3*Ycut^2 + 
             54*m2^4*Ycut^2 + 6648*MBhat*Ycut^2 + 30136*m2*MBhat*Ycut^2 + 
             7840*m2^2*MBhat*Ycut^2 - 1040*m2^3*MBhat*Ycut^2 + 
             96*m2^4*MBhat*Ycut^2 - 9225*MBhat^2*Ycut^2 - 22770*m2*MBhat^2*
              Ycut^2 + 1005*m2^2*MBhat^2*Ycut^2 - 300*m2^3*MBhat^2*Ycut^2 + 
             90*m2^4*MBhat^2*Ycut^2 + 5640*MBhat^3*Ycut^2 + 
             6040*m2*MBhat^3*Ycut^2 - 1520*m2^2*MBhat^3*Ycut^2 + 
             240*m2^3*MBhat^3*Ycut^2 - 1290*MBhat^4*Ycut^2 - 
             450*m2*MBhat^4*Ycut^2 + 180*m2^2*MBhat^4*Ycut^2 + 711*Ycut^3 + 
             5905*m2*Ycut^3 + 5600*m2^2*Ycut^3 + 70*m2^3*Ycut^3 - 
             36*m2^4*Ycut^3 - 2696*MBhat*Ycut^3 - 14160*m2*MBhat*Ycut^3 - 
             4720*m2^2*MBhat*Ycut^3 + 640*m2^3*MBhat*Ycut^3 - 
             64*m2^4*MBhat*Ycut^3 + 3735*MBhat^2*Ycut^3 + 11505*m2*MBhat^2*
              Ycut^3 - 390*m2^2*MBhat^2*Ycut^3 + 210*m2^3*MBhat^2*Ycut^3 - 
             60*m2^4*MBhat^2*Ycut^3 - 2260*MBhat^3*Ycut^3 - 
             3560*m2*MBhat^3*Ycut^3 + 980*m2^2*MBhat^3*Ycut^3 - 
             160*m2^3*MBhat^3*Ycut^3 + 510*MBhat^4*Ycut^3 + 
             360*m2*MBhat^4*Ycut^3 - 120*m2^2*MBhat^4*Ycut^3 - 30*Ycut^4 - 
             425*m2*Ycut^4 - 675*m2^2*Ycut^4 - 55*m2^3*Ycut^4 + 
             9*m2^4*Ycut^4 + 180*MBhat*Ycut^4 + 980*m2*MBhat*Ycut^4 + 
             1020*m2^2*MBhat*Ycut^4 - 180*m2^3*MBhat*Ycut^4 + 
             16*m2^4*MBhat*Ycut^4 - 150*MBhat^2*Ycut^4 - 1235*m2*MBhat^2*
              Ycut^4 - 5*m2^2*MBhat^2*Ycut^4 - 65*m2^3*MBhat^2*Ycut^4 + 
             15*m2^4*MBhat^2*Ycut^4 - 60*MBhat^3*Ycut^4 + 780*m2*MBhat^3*
              Ycut^4 - 280*m2^2*MBhat^3*Ycut^4 + 40*m2^3*MBhat^3*Ycut^4 + 
             60*MBhat^4*Ycut^4 - 150*m2*MBhat^4*Ycut^4 + 30*m2^2*MBhat^4*
              Ycut^4 - 24*Ycut^5 - 29*m2*Ycut^5 - 164*m2^2*Ycut^5 + 
             21*m2^3*Ycut^5 - 66*MBhat*Ycut^5 + 434*m2*MBhat*Ycut^5 - 
             46*m2^2*MBhat*Ycut^5 + 14*m2^3*MBhat*Ycut^5 + 
             90*MBhat^2*Ycut^5 - 405*m2*MBhat^2*Ycut^5 + 70*m2^2*MBhat^2*
              Ycut^5 + 5*m2^3*MBhat^2*Ycut^5 + 60*MBhat^3*Ycut^5 + 
             20*m2^2*MBhat^3*Ycut^5 - 60*MBhat^4*Ycut^5 + 30*m2*MBhat^4*
              Ycut^5 + 32*Ycut^6 - 97*m2*Ycut^6 + 9*m2^2*Ycut^6 - 
             2*MBhat*Ycut^6 + 112*m2*MBhat*Ycut^6 - 14*m2^2*MBhat*Ycut^6 - 
             90*MBhat^2*Ycut^6 + 15*m2*MBhat^2*Ycut^6 - 5*m2^2*MBhat^2*
              Ycut^6 + 60*MBhat^3*Ycut^6 - 20*m2*MBhat^3*Ycut^6 - 12*Ycut^7 - 
             9*m2*Ycut^7 + 42*MBhat*Ycut^7 - 6*m2*MBhat*Ycut^7 - 
             30*MBhat^2*Ycut^7 + 5*m2*MBhat^2*Ycut^7 - 6*Ycut^8 + 
             6*MBhat*Ycut^8))/(-1 + Ycut)^4 + 6*Sqrt[m2]*
          (-2 - 29*m2 - 54*m2^2 - 14*m2^3 + m2^4 + 8*MBhat + 80*m2*MBhat + 
           80*m2^2*MBhat - 12*MBhat^2 - 78*m2*MBhat^2 - 33*m2^2*MBhat^2 + 
           3*m2^3*MBhat^2 + 8*MBhat^3 + 32*m2*MBhat^3 - 2*MBhat^4 - 
           5*m2*MBhat^4 + m2^2*MBhat^4)*Log[m2] - 6*Sqrt[m2]*
          (-2 - 29*m2 - 54*m2^2 - 14*m2^3 + m2^4 + 8*MBhat + 80*m2*MBhat + 
           80*m2^2*MBhat - 12*MBhat^2 - 78*m2*MBhat^2 - 33*m2^2*MBhat^2 + 
           3*m2^3*MBhat^2 + 8*MBhat^3 + 32*m2*MBhat^3 - 2*MBhat^4 - 
           5*m2*MBhat^4 + m2^2*MBhat^4)*Log[1 - Ycut])) + 
     c[VL]*(-1/45*((-1 + m2 + Ycut)*(90 + 1230*m2 - 2400*m2^2 + 720*m2^3 + 
           390*m2^4 - 30*m2^5 - 424*MBhat - 2404*m2*MBhat + 4616*m2^2*MBhat - 
           1864*m2^3*MBhat + 656*m2^4*MBhat - 100*m2^5*MBhat + 739*MBhat^2 + 
           874*m2*MBhat^2 - 206*m2^2*MBhat^2 - 686*m2^3*MBhat^2 + 
           799*m2^4*MBhat^2 - 200*m2^5*MBhat^2 - 540*MBhat^3 + 
           480*m2*MBhat^3 - 2640*m2^2*MBhat^3 + 1680*m2^3*MBhat^3 - 
           420*m2^4*MBhat^3 + 135*MBhat^4 - 225*m2*MBhat^4 + 
           855*m2^2*MBhat^4 - 225*m2^3*MBhat^4 - 360*Ycut - 5550*m2*Ycut + 
           10560*m2^2*Ycut - 2880*m2^3*Ycut - 1920*m2^4*Ycut + 
           150*m2^5*Ycut + 1696*MBhat*Ycut + 11352*m2*MBhat*Ycut - 
           21292*m2^2*MBhat*Ycut + 8764*m2^3*MBhat*Ycut - 
           3180*m2^4*MBhat*Ycut + 500*m2^5*MBhat*Ycut - 2956*MBhat^2*Ycut - 
           4917*m2*MBhat^2*Ycut + 1897*m2^2*MBhat^2*Ycut + 
           2831*m2^3*MBhat^2*Ycut - 3795*m2^4*MBhat^2*Ycut + 
           1000*m2^5*MBhat^2*Ycut + 2160*MBhat^3*Ycut - 1740*m2*MBhat^3*
            Ycut + 11940*m2^2*MBhat^3*Ycut - 7980*m2^3*MBhat^3*Ycut + 
           2100*m2^4*MBhat^3*Ycut - 540*MBhat^4*Ycut + 1035*m2*MBhat^4*Ycut - 
           4050*m2^2*MBhat^4*Ycut + 1125*m2^3*MBhat^4*Ycut + 540*Ycut^2 + 
           9630*m2*Ycut^2 - 17730*m2^2*Ycut^2 + 4110*m2^3*Ycut^2 + 
           3750*m2^4*Ycut^2 - 300*m2^5*Ycut^2 - 2544*MBhat*Ycut^2 - 
           20712*m2*MBhat*Ycut^2 + 37916*m2^2*MBhat*Ycut^2 - 
           15960*m2^3*MBhat*Ycut^2 + 6060*m2^4*MBhat*Ycut^2 - 
           1000*m2^5*MBhat*Ycut^2 + 4434*MBhat^2*Ycut^2 + 10587*m2*MBhat^2*
            Ycut^2 - 5606*m2^2*MBhat^2*Ycut^2 - 4065*m2^3*MBhat^2*Ycut^2 + 
           6990*m2^4*MBhat^2*Ycut^2 - 2000*m2^5*MBhat^2*Ycut^2 - 
           3240*MBhat^3*Ycut^2 + 1980*m2*MBhat^3*Ycut^2 - 20520*m2^2*MBhat^3*
            Ycut^2 + 14700*m2^3*MBhat^3*Ycut^2 - 4200*m2^4*MBhat^3*Ycut^2 + 
           810*MBhat^4*Ycut^2 - 1755*m2*MBhat^4*Ycut^2 + 7425*m2^2*MBhat^4*
            Ycut^2 - 2250*m2^3*MBhat^4*Ycut^2 - 360*Ycut^3 - 7770*m2*Ycut^3 + 
           13620*m2^2*Ycut^3 - 2190*m2^3*Ycut^3 - 3600*m2^4*Ycut^3 + 
           300*m2^5*Ycut^3 + 1696*MBhat*Ycut^3 + 17704*m2*MBhat*Ycut^3 - 
           31500*m2^2*MBhat*Ycut^3 + 13580*m2^3*MBhat*Ycut^3 - 
           5560*m2^4*MBhat*Ycut^3 + 1000*m2^5*MBhat*Ycut^3 - 
           2776*MBhat^2*Ycut^3 - 11719*m2*MBhat^2*Ycut^3 + 
           7965*m2^2*MBhat^2*Ycut^3 + 2950*m2^3*MBhat^2*Ycut^3 - 
           6890*m2^4*MBhat^2*Ycut^3 + 2000*m2^5*MBhat^2*Ycut^3 + 
           1860*MBhat^3*Ycut^3 + 840*m2*MBhat^3*Ycut^3 + 16440*m2^2*MBhat^3*
            Ycut^3 - 14100*m2^3*MBhat^3*Ycut^3 + 4200*m2^4*MBhat^3*Ycut^3 - 
           420*MBhat^4*Ycut^3 + 1065*m2*MBhat^4*Ycut^3 - 6900*m2^2*MBhat^4*
            Ycut^3 + 2250*m2^3*MBhat^4*Ycut^3 + 90*Ycut^4 + 2640*m2*Ycut^4 - 
           4200*m2^2*Ycut^4 - 30*m2^3*Ycut^4 + 1650*m2^4*Ycut^4 - 
           150*m2^5*Ycut^4 - 544*MBhat*Ycut^4 - 5820*m2*MBhat*Ycut^4 + 
           10920*m2^2*MBhat*Ycut^4 - 6140*m2^3*MBhat*Ycut^4 + 
           3180*m2^4*MBhat*Ycut^4 - 500*m2^5*MBhat*Ycut^4 + 
           244*MBhat^2*Ycut^4 + 7755*m2*MBhat^2*Ycut^4 - 8040*m2^2*MBhat^2*
            Ycut^4 + 710*m2^3*MBhat^2*Ycut^4 + 3345*m2^4*MBhat^2*Ycut^4 - 
           1000*m2^5*MBhat^2*Ycut^4 + 480*MBhat^3*Ycut^4 - 
           4740*m2*MBhat^3*Ycut^4 - 4560*m2^2*MBhat^3*Ycut^4 + 
           7200*m2^3*MBhat^3*Ycut^4 - 2100*m2^4*MBhat^3*Ycut^4 - 
           270*MBhat^4*Ycut^4 + 435*m2*MBhat^4*Ycut^4 + 3525*m2^2*MBhat^4*
            Ycut^4 - 1125*m2^3*MBhat^4*Ycut^4 + 30*Ycut^5 - 300*m2*Ycut^5 + 
           60*m2^2*Ycut^5 + 690*m2^3*Ycut^5 - 510*m2^4*Ycut^5 + 
           30*m2^5*Ycut^5 + 384*MBhat*Ycut^5 - 1416*m2*MBhat*Ycut^5 + 
           324*m2^2*MBhat*Ycut^5 + 1404*m2^3*MBhat*Ycut^5 - 
           876*m2^4*MBhat*Ycut^5 + 100*m2^5*MBhat*Ycut^5 + 
           276*MBhat^2*Ycut^5 - 3339*m2*MBhat^2*Ycut^5 + 6801*m2^2*MBhat^2*
            Ycut^5 - 2949*m2^3*MBhat^2*Ycut^5 - 609*m2^4*MBhat^2*Ycut^5 + 
           200*m2^5*MBhat^2*Ycut^5 - 1110*MBhat^3*Ycut^5 + 
           5370*m2*MBhat^3*Ycut^5 - 2310*m2^2*MBhat^3*Ycut^5 - 
           1530*m2^3*MBhat^3*Ycut^5 + 420*m2^4*MBhat^3*Ycut^5 + 
           420*MBhat^4*Ycut^5 - 945*m2*MBhat^4*Ycut^5 - 930*m2^2*MBhat^4*
            Ycut^5 + 225*m2^3*MBhat^4*Ycut^5 - 105*Ycut^6 + 405*m2*Ycut^6 + 
           75*m2^2*Ycut^6 - 435*m2^3*Ycut^6 + 60*m2^4*Ycut^6 - 
           356*MBhat*Ycut^6 + 1708*m2*MBhat*Ycut^6 - 1568*m2^2*MBhat*Ycut^6 + 
           296*m2^3*MBhat*Ycut^6 + 80*m2^4*MBhat*Ycut^6 + 
           251*MBhat^2*Ycut^6 + 662*m2*MBhat^2*Ycut^6 - 3247*m2^2*MBhat^2*
            Ycut^6 + 1414*m2^3*MBhat^2*Ycut^6 - 20*m2^4*MBhat^2*Ycut^6 + 
           240*MBhat^3*Ycut^6 - 2670*m2*MBhat^3*Ycut^6 + 1980*m2^2*MBhat^3*
            Ycut^6 + 30*m2^3*MBhat^3*Ycut^6 - 30*MBhat^4*Ycut^6 + 
           465*m2*MBhat^4*Ycut^6 + 75*m2^2*MBhat^4*Ycut^6 + 120*Ycut^7 - 
           375*m2*Ycut^7 + 30*m2^2*Ycut^7 + 15*m2^3*Ycut^7 - 
           16*MBhat*Ycut^7 - 348*m2*MBhat*Ycut^7 + 604*m2^2*MBhat*Ycut^7 - 
           80*m2^3*MBhat*Ycut^7 - 164*MBhat^2*Ycut^7 + 168*m2*MBhat^2*
            Ycut^7 + 431*m2^2*MBhat^2*Ycut^7 - 205*m2^3*MBhat^2*Ycut^7 + 
           240*MBhat^3*Ycut^7 + 450*m2*MBhat^3*Ycut^7 - 330*m2^2*MBhat^3*
            Ycut^7 - 180*MBhat^4*Ycut^7 - 75*m2*MBhat^4*Ycut^7 - 30*Ycut^8 + 
           75*m2*Ycut^8 - 15*m2^2*Ycut^8 + 144*MBhat*Ycut^8 - 
           84*m2*MBhat*Ycut^8 - 20*m2^2*MBhat*Ycut^8 - 129*MBhat^2*Ycut^8 - 
           66*m2*MBhat^2*Ycut^8 + 5*m2^2*MBhat^2*Ycut^8 - 60*MBhat^3*Ycut^8 + 
           30*m2*MBhat^3*Ycut^8 + 75*MBhat^4*Ycut^8 - 30*Ycut^9 + 
           15*m2*Ycut^9 - 16*MBhat*Ycut^9 + 20*m2*MBhat*Ycut^9 + 
           76*MBhat^2*Ycut^9 - 5*m2*MBhat^2*Ycut^9 - 30*MBhat^3*Ycut^9 + 
           15*Ycut^10 - 20*MBhat*Ycut^10 + 5*MBhat^2*Ycut^10))/
         (-1 + Ycut)^5 - (4*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 
          36*m2*MBhat + 8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 
          13*m2^2*MBhat^2 - 12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*
         Log[m2])/3 + (4*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 36*MBhat + 
          36*m2*MBhat + 8*m2^2*MBhat + 36*MBhat^2 - 27*m2*MBhat^2 + 
          13*m2^2*MBhat^2 - 12*MBhat^3 - 12*m2*MBhat^3 + 9*m2*MBhat^4)*
         Log[1 - Ycut])/3 + c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*
           (391 - 574*m2 - 2514*m2^2 + 146*m2^3 + 31*m2^4 - 1336*MBhat + 
            3464*m2*MBhat + 2664*m2^2*MBhat - 536*m2^3*MBhat + 
            64*m2^4*MBhat + 1575*MBhat^2 - 4620*m2*MBhat^2 + 
            820*m2^2*MBhat^2 - 260*m2^3*MBhat^2 + 85*m2^4*MBhat^2 - 
            760*MBhat^3 + 1960*m2*MBhat^3 - 920*m2^2*MBhat^3 + 
            200*m2^3*MBhat^3 + 130*MBhat^4 - 140*m2*MBhat^4 + 
            130*m2^2*MBhat^4 - 1293*Ycut + 1693*m2*Ycut + 8979*m2^2*Ycut - 
            435*m2^3*Ycut - 124*m2^4*Ycut + 4488*MBhat*Ycut - 
            11248*m2*MBhat*Ycut - 10184*m2^2*MBhat*Ycut + 2080*m2^3*MBhat*
             Ycut - 256*m2^4*MBhat*Ycut - 5445*MBhat^2*Ycut + 
            15795*m2*MBhat^2*Ycut - 2565*m2^2*MBhat^2*Ycut + 
            955*m2^3*MBhat^2*Ycut - 340*m2^4*MBhat^2*Ycut + 
            2760*MBhat^3*Ycut - 7120*m2*MBhat^3*Ycut + 3480*m2^2*MBhat^3*
             Ycut - 800*m2^3*MBhat^3*Ycut - 510*MBhat^4*Ycut + 
            610*m2*MBhat^4*Ycut - 520*m2^2*MBhat^4*Ycut + 1473*Ycut^2 - 
            1574*m2*Ycut^2 - 11375*m2^2*Ycut^2 + 370*m2^3*Ycut^2 + 
            186*m2^4*Ycut^2 - 5208*MBhat*Ycut^2 + 12344*m2*MBhat*Ycut^2 + 
            14160*m2^2*MBhat*Ycut^2 - 2960*m2^3*MBhat*Ycut^2 + 
            384*m2^4*MBhat*Ycut^2 + 6525*MBhat^2*Ycut^2 - 18630*m2*MBhat^2*
             Ycut^2 + 2415*m2^2*MBhat^2*Ycut^2 - 1220*m2^3*MBhat^2*Ycut^2 + 
            510*m2^4*MBhat^2*Ycut^2 - 3480*MBhat^3*Ycut^2 + 
            9080*m2*MBhat^3*Ycut^2 - 4720*m2^2*MBhat^3*Ycut^2 + 
            1200*m2^3*MBhat^3*Ycut^2 + 690*MBhat^4*Ycut^2 - 
            950*m2*MBhat^4*Ycut^2 + 780*m2^2*MBhat^4*Ycut^2 - 611*Ycut^3 + 
            375*m2*Ycut^3 + 5600*m2^2*Ycut^3 + 10*m2^3*Ycut^3 - 
            124*m2^4*Ycut^3 + 2216*MBhat*Ycut^3 - 4640*m2*MBhat*Ycut^3 - 
            8080*m2^2*MBhat*Ycut^3 + 1760*m2^3*MBhat*Ycut^3 - 
            256*m2^4*MBhat*Ycut^3 - 2595*MBhat^2*Ycut^3 + 7515*m2*MBhat^2*
             Ycut^3 - 290*m2^2*MBhat^2*Ycut^3 + 710*m2^3*MBhat^2*Ycut^3 - 
            340*m2^4*MBhat^2*Ycut^3 + 1220*MBhat^3*Ycut^3 - 
            4200*m2*MBhat^3*Ycut^3 + 2780*m2^2*MBhat^3*Ycut^3 - 
            800*m2^3*MBhat^3*Ycut^3 - 230*MBhat^4*Ycut^3 + 
            680*m2*MBhat^4*Ycut^3 - 520*m2^2*MBhat^4*Ycut^3 + 30*Ycut^4 + 
            65*m2*Ycut^4 - 525*m2^2*Ycut^4 - 105*m2^3*Ycut^4 + 
            31*m2^4*Ycut^4 - 340*MBhat*Ycut^4 + 300*m2*MBhat*Ycut^4 + 
            1300*m2^2*MBhat*Ycut^4 - 460*m2^3*MBhat*Ycut^4 + 
            64*m2^4*MBhat*Ycut^4 - 410*MBhat^2*Ycut^4 + 895*m2*MBhat^2*
             Ycut^4 - 935*m2^2*MBhat^2*Ycut^4 - 115*m2^3*MBhat^2*Ycut^4 + 
            85*m2^4*MBhat^2*Ycut^4 + 1020*MBhat^3*Ycut^4 - 
            380*m2*MBhat^3*Ycut^4 - 600*m2^2*MBhat^3*Ycut^4 + 
            200*m2^3*MBhat^3*Ycut^4 - 300*MBhat^4*Ycut^4 - 
            250*m2*MBhat^4*Ycut^4 + 130*m2^2*MBhat^4*Ycut^4 + 64*Ycut^5 - 
            51*m2*Ycut^5 - 156*m2^2*Ycut^5 + 59*m2^3*Ycut^5 + 
            506*MBhat*Ycut^5 - 634*m2*MBhat*Ycut^5 + 246*m2^2*MBhat*Ycut^5 + 
            26*m2^3*MBhat*Ycut^5 + 150*MBhat^2*Ycut^5 - 1055*m2*MBhat^2*
             Ycut^5 + 650*m2^2*MBhat^2*Ycut^5 - 25*m2^3*MBhat^2*Ycut^5 - 
            1020*MBhat^3*Ycut^5 + 880*m2*MBhat^3*Ycut^5 - 20*m2^2*MBhat^3*
             Ycut^5 + 300*MBhat^4*Ycut^5 + 50*m2*MBhat^4*Ycut^5 - 
            152*Ycut^6 + 137*m2*Ycut^6 - 9*m2^2*Ycut^6 - 358*MBhat*Ycut^6 + 
            448*m2*MBhat*Ycut^6 - 106*m2^2*MBhat*Ycut^6 + 
            330*MBhat^2*Ycut^6 + 85*m2*MBhat^2*Ycut^6 - 95*m2^2*MBhat^2*
             Ycut^6 + 260*MBhat^3*Ycut^6 - 220*m2*MBhat^3*Ycut^6 - 
            80*MBhat^4*Ycut^6 + 132*Ycut^7 - 71*m2*Ycut^7 - 2*MBhat*Ycut^7 - 
            34*m2*MBhat*Ycut^7 - 130*MBhat^2*Ycut^7 + 15*m2*MBhat^2*Ycut^7 - 
            34*Ycut^8 + 34*MBhat*Ycut^8))/(15*(-1 + Ycut)^4) - 
         4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4 - 8*MBhat + 
           80*m2^2*MBhat + 12*MBhat^2 - 18*m2*MBhat^2 - 43*m2^2*MBhat^2 + 
           9*m2^3*MBhat^2 - 8*MBhat^3 + 16*m2*MBhat^3 + 2*MBhat^4 - 
           3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2] + 
         4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4 - 8*MBhat + 
           80*m2^2*MBhat + 12*MBhat^2 - 18*m2*MBhat^2 - 43*m2^2*MBhat^2 + 
           9*m2^3*MBhat^2 - 8*MBhat^3 + 16*m2*MBhat^3 + 2*MBhat^4 - 
           3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[1 - Ycut]))))/mb^2 + 
 (rhoD*((-2238 + 8840*m2 + 10550*m2^2 - 18400*m2^3 + 1150*m2^4 + 88*m2^5 + 
       10*m2^6 + 7856*MBhat - 26880*m2*MBhat + 7600*m2^2*MBhat + 
       12800*m2^3*MBhat - 1200*m2^4*MBhat - 256*m2^5*MBhat + 80*m2^6*MBhat - 
       9825*MBhat^2 + 26595*m2*MBhat^2 - 19320*m2^2*MBhat^2 + 
       1760*m2^3*MBhat^2 + 1665*m2^4*MBhat^2 - 1155*m2^5*MBhat^2 + 
       280*m2^6*MBhat^2 + 5424*MBhat^3 - 10640*m2*MBhat^3 + 
       6720*m2^2*MBhat^3 - 960*m2^3*MBhat^3 - 880*m2^4*MBhat^3 + 
       336*m2^5*MBhat^3 - 1155*MBhat^4 + 1320*m2*MBhat^4 - 360*m2^2*MBhat^4 + 
       120*m2^3*MBhat^4 + 75*m2^4*MBhat^4 + 16386*Ycut - 61400*m2*Ycut - 
       89570*m2^2*Ycut + 122080*m2^3*Ycut - 6970*m2^4*Ycut - 616*m2^5*Ycut - 
       70*m2^6*Ycut - 57872*MBhat*Ycut + 188160*m2*MBhat*Ycut - 
       29200*m2^2*MBhat*Ycut - 89600*m2^3*MBhat*Ycut + 8400*m2^4*MBhat*Ycut + 
       1792*m2^5*MBhat*Ycut - 560*m2^6*MBhat*Ycut + 73095*MBhat^2*Ycut - 
       187605*m2*MBhat^2*Ycut + 126060*m2^2*MBhat^2*Ycut - 
       10220*m2^3*MBhat^2*Ycut - 11655*m2^4*MBhat^2*Ycut + 
       8085*m2^5*MBhat^2*Ycut - 1960*m2^6*MBhat^2*Ycut - 40848*MBhat^3*Ycut + 
       75440*m2*MBhat^3*Ycut - 47040*m2^2*MBhat^3*Ycut + 
       6720*m2^3*MBhat^3*Ycut + 6160*m2^4*MBhat^3*Ycut - 
       2352*m2^5*MBhat^3*Ycut + 8805*MBhat^4*Ycut - 9240*m2*MBhat^4*Ycut + 
       3060*m2^2*MBhat^4*Ycut - 840*m2^3*MBhat^4*Ycut - 
       525*m2^4*MBhat^4*Ycut - 51678*Ycut^2 + 182520*m2*Ycut^2 + 
       323730*m2^2*Ycut^2 - 342720*m2^3*Ycut^2 + 17130*m2^4*Ycut^2 + 
       1848*m2^5*Ycut^2 + 210*m2^6*Ycut^2 + 183696*MBhat*Ycut^2 - 
       564480*m2*MBhat*Ycut^2 + 3600*m2^2*MBhat*Ycut^2 + 
       268800*m2^3*MBhat*Ycut^2 - 25200*m2^4*MBhat*Ycut^2 - 
       5376*m2^5*MBhat*Ycut^2 + 1680*m2^6*MBhat*Ycut^2 - 
       234405*MBhat^2*Ycut^2 + 567855*m2*MBhat^2*Ycut^2 - 
       346050*m2^2*MBhat^2*Ycut^2 + 23310*m2^3*MBhat^2*Ycut^2 + 
       34965*m2^4*MBhat^2*Ycut^2 - 24255*m2^5*MBhat^2*Ycut^2 + 
       5880*m2^6*MBhat^2*Ycut^2 + 132624*MBhat^3*Ycut^2 - 
       229680*m2*MBhat^3*Ycut^2 + 141120*m2^2*MBhat^3*Ycut^2 - 
       20160*m2^3*MBhat^3*Ycut^2 - 18480*m2^4*MBhat^3*Ycut^2 + 
       7056*m2^5*MBhat^3*Ycut^2 - 28935*MBhat^4*Ycut^2 + 
       27720*m2*MBhat^4*Ycut^2 - 11070*m2^2*MBhat^4*Ycut^2 + 
       2520*m2^3*MBhat^4*Ycut^2 + 1575*m2^4*MBhat^4*Ycut^2 + 91170*Ycut^3 - 
       300840*m2*Ycut^3 - 649590*m2^2*Ycut^3 + 524160*m2^3*Ycut^3 - 
       20990*m2^4*Ycut^3 - 3080*m2^5*Ycut^3 - 350*m2^6*Ycut^3 - 
       326320*MBhat*Ycut^3 + 940800*m2*MBhat*Ycut^3 + 
       162000*m2^2*MBhat*Ycut^3 - 448000*m2^3*MBhat*Ycut^3 + 
       42000*m2^4*MBhat*Ycut^3 + 8960*m2^5*MBhat*Ycut^3 - 
       2800*m2^6*MBhat*Ycut^3 + 419955*MBhat^2*Ycut^3 - 
       954585*m2*MBhat^2*Ycut^3 + 510570*m2^2*MBhat^2*Ycut^3 - 
       20310*m2^3*MBhat^2*Ycut^3 - 63075*m2^4*MBhat^2*Ycut^3 + 
       42345*m2^5*MBhat^2*Ycut^3 - 9800*m2^6*MBhat^2*Ycut^3 - 
       239820*MBhat^3*Ycut^3 + 387360*m2*MBhat^3*Ycut^3 - 
       232920*m2^2*MBhat^3*Ycut^3 + 30000*m2^3*MBhat^3*Ycut^3 + 
       32900*m2^4*MBhat^3*Ycut^3 - 11760*m2^5*MBhat^3*Ycut^3 + 
       52785*MBhat^4*Ycut^3 - 45720*m2*MBhat^4*Ycut^3 + 
       21750*m2^2*MBhat^4*Ycut^3 - 3720*m2^3*MBhat^4*Ycut^3 - 
       2625*m2^4*MBhat^4*Ycut^3 - 97470*Ycut^4 + 296640*m2*Ycut^4 + 
       787140*m2^2*Ycut^4 - 465360*m2^3*Ycut^4 + 11540*m2^4*Ycut^4 + 
       3080*m2^5*Ycut^4 + 350*m2^6*Ycut^4 + 352240*MBhat*Ycut^4 - 
       942240*m2*MBhat*Ycut^4 - 370080*m2^2*MBhat*Ycut^4 + 
       443680*m2^3*MBhat*Ycut^4 - 36960*m2^4*MBhat*Ycut^4 - 
       10880*m2^5*MBhat*Ycut^4 + 2800*m2^6*MBhat*Ycut^4 - 
       453210*MBhat^2*Ycut^4 + 958920*m2*MBhat^2*Ycut^4 - 
       425790*m2^2*MBhat^2*Ycut^4 - 2430*m2^3*MBhat^2*Ycut^4 + 
       68775*m2^4*MBhat^2*Ycut^4 - 44415*m2^5*MBhat^2*Ycut^4 + 
       9800*m2^6*MBhat^2*Ycut^4 + 257460*MBhat^3*Ycut^4 - 
       385440*m2*MBhat^3*Ycut^4 + 226200*m2^2*MBhat^3*Ycut^4 - 
       23040*m2^3*MBhat^3*Ycut^4 - 35900*m2^4*MBhat^3*Ycut^4 + 
       11760*m2^5*MBhat^3*Ycut^4 - 56400*MBhat^4*Ycut^4 + 
       43560*m2*MBhat^4*Ycut^4 - 25320*m2^2*MBhat^4*Ycut^4 + 
       2940*m2^3*MBhat^4*Ycut^4 + 2625*m2^4*MBhat^4*Ycut^4 + 63330*Ycut^5 - 
       174240*m2*Ycut^5 - 582900*m2^2*Ycut^5 + 233520*m2^3*Ycut^5 - 
       900*m2^4*Ycut^5 - 1272*m2^5*Ycut^5 - 210*m2^6*Ycut^5 - 
       235512*MBhat*Ycut^5 + 572160*m2*MBhat*Ycut^5 + 
       384540*m2^2*MBhat*Ycut^5 - 258660*m2^3*MBhat*Ycut^5 + 
       16380*m2^4*MBhat*Ycut^5 + 7476*m2^5*MBhat*Ycut^5 - 
       1680*m2^6*MBhat*Ycut^5 + 292950*MBhat^2*Ycut^5 - 
       569400*m2*MBhat^2*Ycut^5 + 186510*m2^2*MBhat^2*Ycut^5 + 
       15870*m2^3*MBhat^2*Ycut^5 - 45105*m2^4*MBhat^2*Ycut^5 + 
       28665*m2^5*MBhat^2*Ycut^5 - 5880*m2^6*MBhat^2*Ycut^5 - 
       155478*MBhat^3*Ycut^5 + 215040*m2*MBhat^3*Ycut^5 - 
       126540*m2^2*MBhat^3*Ycut^5 + 8400*m2^3*MBhat^3*Ycut^5 + 
       23730*m2^4*MBhat^3*Ycut^5 - 7056*m2^5*MBhat^3*Ycut^5 + 
       31920*MBhat^4*Ycut^5 - 21720*m2*MBhat^4*Ycut^5 + 
       18060*m2^2*MBhat^4*Ycut^5 - 1380*m2^3*MBhat^4*Ycut^5 - 
       1575*m2^4*MBhat^4*Ycut^5 - 22465*Ycut^6 + 54480*m2*Ycut^6 + 
       251070*m2^2*Ycut^6 - 56780*m2^3*Ycut^6 - 1955*m2^4*Ycut^6 + 
       364*m2^5*Ycut^6 + 70*m2^6*Ycut^6 + 98504*MBhat*Ycut^6 - 
       204960*m2*MBhat*Ycut^6 - 205620*m2^2*MBhat*Ycut^6 + 
       82380*m2^3*MBhat*Ycut^6 - 3300*m2^4*MBhat*Ycut^6 - 
       3052*m2^5*MBhat*Ycut^6 + 560*m2^6*MBhat*Ycut^6 - 
       103531*MBhat^2*Ycut^6 + 177120*m2*MBhat^2*Ycut^6 - 
       25350*m2^2*MBhat^2*Ycut^6 - 11270*m2^3*MBhat^2*Ycut^6 + 
       17640*m2^4*MBhat^2*Ycut^6 - 10899*m2^5*MBhat^2*Ycut^6 + 
       1960*m2^6*MBhat^2*Ycut^6 + 33306*MBhat^3*Ycut^6 - 
       44640*m2*MBhat^3*Ycut^6 + 33780*m2^2*MBhat^3*Ycut^6 - 
       240*m2^3*MBhat^3*Ycut^6 - 9310*m2^4*MBhat^3*Ycut^6 + 
       2352*m2^5*MBhat^3*Ycut^6 - 2520*MBhat^4*Ycut^6 + 
       2040*m2*MBhat^4*Ycut^6 - 7890*m2^2*MBhat^4*Ycut^6 + 
       420*m2^3*MBhat^4*Ycut^6 + 525*m2^4*MBhat^4*Ycut^6 + 775*Ycut^7 - 
       3120*m2*Ycut^7 - 53610*m2^2*Ycut^7 + 2420*m2^3*Ycut^7 + 
       1325*m2^4*Ycut^7 - 52*m2^5*Ycut^7 - 10*m2^6*Ycut^7 - 
       30872*MBhat*Ycut^7 + 46080*m2*MBhat*Ycut^7 + 48600*m2^2*MBhat*Ycut^7 - 
       11160*m2^3*MBhat*Ycut^7 - 300*m2^4*MBhat*Ycut^7 + 
       676*m2^5*MBhat*Ycut^7 - 80*m2^6*MBhat*Ycut^7 + 16237*MBhat^2*Ycut^7 - 
       14880*m2*MBhat^2*Ycut^7 - 10770*m2^2*MBhat^2*Ycut^7 + 
       3710*m2^3*MBhat^2*Ycut^7 - 3240*m2^4*MBhat^2*Ycut^7 + 
       2133*m2^5*MBhat^2*Ycut^7 - 280*m2^6*MBhat^2*Ycut^7 + 
       19482*MBhat^3*Ycut^7 - 18720*m2*MBhat^3*Ycut^7 + 
       1440*m2^2*MBhat^3*Ycut^7 - 960*m2^3*MBhat^3*Ycut^7 + 
       1930*m2^4*MBhat^3*Ycut^7 - 336*m2^5*MBhat^3*Ycut^7 - 
       9240*MBhat^4*Ycut^7 + 3480*m2*MBhat^4*Ycut^7 + 
       2010*m2^2*MBhat^4*Ycut^7 - 60*m2^3*MBhat^4*Ycut^7 - 
       75*m2^4*MBhat^4*Ycut^7 + 4845*Ycut^8 - 5160*m2*Ycut^8 + 
       3030*m2^2*Ycut^8 + 1260*m2^3*Ycut^8 - 345*m2^4*Ycut^8 + 
       12120*MBhat*Ycut^8 - 12000*m2*MBhat*Ycut^8 + 120*m2^2*MBhat*Ycut^8 - 
       600*m2^3*MBhat*Ycut^8 + 180*m2^4*MBhat*Ycut^8 - 60*m2^5*MBhat*Ycut^8 - 
       4026*MBhat^2*Ycut^8 - 4515*m2*MBhat^2*Ycut^8 + 
       4830*m2^2*MBhat^2*Ycut^8 - 330*m2^3*MBhat^2*Ycut^8 - 
       45*m2^4*MBhat^2*Ycut^8 - 144*m2^5*MBhat^2*Ycut^8 - 
       16230*MBhat^3*Ycut^8 + 14640*m2*MBhat^3*Ycut^8 - 
       3600*m2^2*MBhat^3*Ycut^8 + 240*m2^3*MBhat^3*Ycut^8 - 
       150*m2^4*MBhat^3*Ycut^8 + 6315*MBhat^4*Ycut^8 - 
       1680*m2*MBhat^4*Ycut^8 - 240*m2^2*MBhat^4*Ycut^8 - 4315*Ycut^9 + 
       3160*m2*Ycut^9 + 70*m2^2*Ycut^9 - 180*m2^3*Ycut^9 + 15*m2^4*Ycut^9 - 
       4440*MBhat*Ycut^9 + 3840*m2*MBhat*Ycut^9 - 1780*m2^2*MBhat*Ycut^9 + 
       380*m2^3*MBhat*Ycut^9 + 4070*MBhat^2*Ycut^9 + 405*m2*MBhat^2*Ycut^9 - 
       690*m2^2*MBhat^2*Ycut^9 - 90*m2^3*MBhat^2*Ycut^9 + 
       75*m2^4*MBhat^2*Ycut^9 + 4590*MBhat^3*Ycut^9 - 
       3760*m2*MBhat^3*Ycut^9 + 900*m2^2*MBhat^3*Ycut^9 - 
       1725*MBhat^4*Ycut^9 + 240*m2*MBhat^4*Ycut^9 + 2203*Ycut^10 - 
       1000*m2*Ycut^10 + 80*m2^2*Ycut^10 + 456*MBhat*Ycut^10 - 
       480*m2*MBhat*Ycut^10 + 220*m2^2*MBhat*Ycut^10 - 
       20*m2^3*MBhat*Ycut^10 - 1460*MBhat^2*Ycut^10 + 
       105*m2*MBhat^2*Ycut^10 - 546*MBhat^3*Ycut^10 + 
       400*m2*MBhat^3*Ycut^10 - 60*m2^2*MBhat^3*Ycut^10 + 
       135*MBhat^4*Ycut^10 - 621*Ycut^11 + 120*m2*Ycut^11 + 
       168*MBhat*Ycut^11 + 156*MBhat^2*Ycut^11 - 15*m2*MBhat^2*Ycut^11 + 
       42*MBhat^3*Ycut^11 + 15*MBhat^4*Ycut^11 + 83*Ycut^12 - 
       24*MBhat*Ycut^12 - 7*MBhat^2*Ycut^12 - 6*MBhat^3*Ycut^12 - 5*Ycut^13 + 
       MBhat^2*Ycut^13)/(90*(-1 + Ycut)^7) + 
     (2*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 48*MBhat + 
        400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 153*m2^2*MBhat^2 + 
        35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 12*MBhat^4 + 
        9*m2^2*MBhat^4)*Log[m2])/3 - 
     (2*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 48*MBhat + 
        400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 153*m2^2*MBhat^2 + 
        35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 12*MBhat^4 + 
        9*m2^2*MBhat^4)*Log[1 - Ycut])/3 + 
     c[SL]^2*((-1573 + 3120*m2 + 6675*m2^2 - 8800*m2^3 + 525*m2^4 + 48*m2^5 + 
         5*m2^6 + 5664*MBhat - 10432*m2*MBhat - 800*m2^2*MBhat + 
         6400*m2^3*MBhat - 800*m2^4*MBhat - 64*m2^5*MBhat + 32*m2^6*MBhat - 
         7470*MBhat^2 + 11710*m2*MBhat^2 - 5390*m2^2*MBhat^2 + 
         1280*m2^3*MBhat^2 - 10*m2^4*MBhat^2 - 190*m2^5*MBhat^2 + 
         70*m2^6*MBhat^2 + 4384*MBhat^3 - 5600*m2*MBhat^3 + 
         1920*m2^2*MBhat^3 - 640*m2^3*MBhat^3 - 160*m2^4*MBhat^3 + 
         96*m2^5*MBhat^3 - 985*MBhat^4 + 920*m2*MBhat^4 + 40*m2^3*MBhat^4 + 
         25*m2^4*MBhat^4 + 11491*Ycut - 20400*m2*Ycut - 53385*m2^2*Ycut + 
         58240*m2^3*Ycut - 3135*m2^4*Ycut - 336*m2^5*Ycut - 35*m2^6*Ycut - 
         41568*MBhat*Ycut + 69184*m2*MBhat*Ycut + 15200*m2^2*MBhat*Ycut - 
         44800*m2^3*MBhat*Ycut + 5600*m2^4*MBhat*Ycut + 448*m2^5*MBhat*Ycut - 
         224*m2^6*MBhat*Ycut + 55170*MBhat^2*Ycut - 78130*m2*MBhat^2*Ycut + 
         34370*m2^2*MBhat^2*Ycut - 8120*m2^3*MBhat^2*Ycut + 
         70*m2^4*MBhat^2*Ycut + 1330*m2^5*MBhat^2*Ycut - 
         490*m2^6*MBhat^2*Ycut - 32608*MBhat^3*Ycut + 37280*m2*MBhat^3*Ycut - 
         13440*m2^2*MBhat^3*Ycut + 4480*m2^3*MBhat^3*Ycut + 
         1120*m2^4*MBhat^3*Ycut - 672*m2^5*MBhat^3*Ycut + 7375*MBhat^4*Ycut - 
         5960*m2*MBhat^4*Ycut + 180*m2^2*MBhat^4*Ycut - 
         280*m2^3*MBhat^4*Ycut - 175*m2^4*MBhat^4*Ycut - 36153*Ycut^2 + 
         56160*m2*Ycut^2 + 183465*m2^2*Ycut^2 - 162960*m2^3*Ycut^2 + 
         7515*m2^4*Ycut^2 + 1008*m2^5*Ycut^2 + 105*m2^6*Ycut^2 + 
         131424*MBhat*Ycut^2 - 194112*m2*MBhat*Ycut^2 - 
         79200*m2^2*MBhat*Ycut^2 + 134400*m2^3*MBhat*Ycut^2 - 
         16800*m2^4*MBhat*Ycut^2 - 1344*m2^5*MBhat*Ycut^2 + 
         672*m2^6*MBhat*Ycut^2 - 175590*MBhat^2*Ycut^2 + 
         220950*m2*MBhat^2*Ycut^2 - 91350*m2^2*MBhat^2*Ycut^2 + 
         21420*m2^3*MBhat^2*Ycut^2 - 210*m2^4*MBhat^2*Ycut^2 - 
         3990*m2^5*MBhat^2*Ycut^2 + 1470*m2^6*MBhat^2*Ycut^2 + 
         104544*MBhat^3*Ycut^2 - 105120*m2*MBhat^3*Ycut^2 + 
         40320*m2^2*MBhat^3*Ycut^2 - 13440*m2^3*MBhat^3*Ycut^2 - 
         3360*m2^4*MBhat^3*Ycut^2 + 2016*m2^5*MBhat^3*Ycut^2 - 
         23805*MBhat^4*Ycut^2 + 16200*m2*MBhat^4*Ycut^2 - 
         1170*m2^2*MBhat^4*Ycut^2 + 840*m2^3*MBhat^4*Ycut^2 + 
         525*m2^4*MBhat^4*Ycut^2 + 63615*Ycut^3 - 83520*m2*Ycut^3 - 
         352395*m2^2*Ycut^3 + 248080*m2^3*Ycut^3 - 8745*m2^4*Ycut^3 - 
         1680*m2^5*Ycut^3 - 175*m2^6*Ycut^3 - 232480*MBhat*Ycut^3 + 
         296640*m2*MBhat*Ycut^3 + 199200*m2^2*MBhat*Ycut^3 - 
         224000*m2^3*MBhat*Ycut^3 + 28000*m2^4*MBhat*Ycut^3 + 
         2240*m2^5*MBhat*Ycut^3 - 1120*m2^6*MBhat*Ycut^3 + 
         312410*MBhat^2*Ycut^3 - 340730*m2*MBhat^2*Ycut^3 + 
         128570*m2^2*MBhat^2*Ycut^3 - 29180*m2^3*MBhat^2*Ycut^3 - 
         1010*m2^4*MBhat^2*Ycut^3 + 7290*m2^5*MBhat^2*Ycut^3 - 
         2450*m2^6*MBhat^2*Ycut^3 - 187180*MBhat^3*Ycut^3 + 
         161160*m2*MBhat^3*Ycut^3 - 66800*m2^2*MBhat^3*Ycut^3 + 
         21400*m2^3*MBhat^3*Ycut^3 + 6300*m2^4*MBhat^3*Ycut^3 - 
         3360*m2^5*MBhat^3*Ycut^3 + 42875*MBhat^4*Ycut^3 - 
         23480*m2*MBhat^4*Ycut^3 + 3050*m2^2*MBhat^4*Ycut^3 - 
         1240*m2^3*MBhat^4*Ycut^3 - 875*m2^4*MBhat^4*Ycut^3 - 67815*Ycut^4 + 
         70920*m2*Ycut^4 + 410670*m2^2*Ycut^4 - 218680*m2^3*Ycut^4 + 
         4020*m2^4*Ycut^4 + 1680*m2^5*Ycut^4 + 175*m2^6*Ycut^4 + 
         249680*MBhat*Ycut^4 - 263680*m2*MBhat*Ycut^4 - 
         283040*m2^2*MBhat*Ycut^4 + 223360*m2^3*MBhat*Ycut^4 - 
         26640*m2^4*MBhat*Ycut^4 - 2880*m2^5*MBhat*Ycut^4 + 
         1120*m2^6*MBhat*Ycut^4 - 335885*MBhat^2*Ycut^4 + 
         304690*m2*MBhat^2*Ycut^4 - 99510*m2^2*MBhat^2*Ycut^4 + 
         22240*m2^3*MBhat^2*Ycut^4 + 2345*m2^4*MBhat^2*Ycut^4 - 
         7980*m2^5*MBhat^2*Ycut^4 + 2450*m2^6*MBhat^2*Ycut^4 + 
         201460*MBhat^3*Ycut^4 - 141880*m2*MBhat^3*Ycut^4 + 
         66160*m2^2*MBhat^3*Ycut^4 - 19720*m2^3*MBhat^3*Ycut^4 - 
         7300*m2^4*MBhat^3*Ycut^4 + 3360*m2^5*MBhat^3*Ycut^4 - 
         46240*MBhat^4*Ycut^4 + 18600*m2*MBhat^4*Ycut^4 - 
         4240*m2^2*MBhat^4*Ycut^4 + 980*m2^3*MBhat^4*Ycut^4 + 
         875*m2^4*MBhat^4*Ycut^4 + 43929*Ycut^5 - 32280*m2*Ycut^5 - 
         293070*m2^2*Ycut^5 + 107880*m2^3*Ycut^5 + 960*m2^4*Ycut^5 - 
         816*m2^5*Ycut^5 - 105*m2^6*Ycut^5 - 165448*MBhat*Ycut^5 + 
         134284*m2*MBhat*Ycut^5 + 237200*m2^2*MBhat*Ycut^5 - 
         134120*m2^3*MBhat*Ycut^5 + 14840*m2^4*MBhat*Ycut^5 + 
         2044*m2^5*MBhat*Ycut^5 - 672*m2^6*MBhat*Ycut^5 + 
         218715*MBhat^2*Ycut^5 - 152030*m2*MBhat^2*Ycut^5 + 
         37690*m2^2*MBhat^2*Ycut^5 - 9800*m2^3*MBhat^2*Ycut^5 - 
         2175*m2^4*MBhat^2*Ycut^5 + 5460*m2^5*MBhat^2*Ycut^5 - 
         1470*m2^6*MBhat^2*Ycut^5 - 128598*MBhat^3*Ycut^5 + 
         67240*m2*MBhat^3*Ycut^5 - 39920*m2^2*MBhat^3*Ycut^5 + 
         10920*m2^3*MBhat^3*Ycut^5 + 5110*m2^4*MBhat^3*Ycut^5 - 
         2016*m2^5*MBhat^3*Ycut^5 + 29120*MBhat^4*Ycut^5 - 
         6520*m2*MBhat^4*Ycut^5 + 3500*m2^2*MBhat^4*Ycut^5 - 
         460*m2^3*MBhat^4*Ycut^5 - 525*m2^4*MBhat^4*Ycut^5 - 15593*Ycut^6 + 
         4760*m2*Ycut^6 + 120960*m2^2*Ycut^6 - 24180*m2^3*Ycut^6 - 
         1940*m2^4*Ycut^6 + 252*m2^5*Ycut^6 + 35*m2^6*Ycut^6 + 
         67256*MBhat*Ycut^6 - 37268*m2*MBhat*Ycut^6 - 114160*m2^2*MBhat*
          Ycut^6 + 47000*m2^3*MBhat*Ycut^6 - 5000*m2^4*MBhat*Ycut^6 - 
         868*m2^5*MBhat*Ycut^6 + 224*m2^6*MBhat*Ycut^6 - 
         81027*MBhat^2*Ycut^6 + 34090*m2*MBhat^2*Ycut^6 - 
         2130*m2^2*MBhat^2*Ycut^6 + 2100*m2^3*MBhat^2*Ycut^6 + 
         1435*m2^4*MBhat^2*Ycut^6 - 2268*m2^5*MBhat^2*Ycut^6 + 
         490*m2^6*MBhat^2*Ycut^6 + 42266*MBhat^3*Ycut^6 - 
         11480*m2*MBhat^3*Ycut^6 + 14480*m2^2*MBhat^3*Ycut^6 - 
         3480*m2^3*MBhat^3*Ycut^6 - 2170*m2^4*MBhat^3*Ycut^6 + 
         672*m2^5*MBhat^3*Ycut^6 - 8680*MBhat^4*Ycut^6 - 
         840*m2*MBhat^4*Ycut^6 - 1790*m2^2*MBhat^4*Ycut^6 + 
         140*m2^3*MBhat^4*Ycut^6 + 175*m2^4*MBhat^4*Ycut^6 + 719*Ycut^7 + 
         2840*m2*Ycut^7 - 23460*m2^2*Ycut^7 - 660*m2^3*Ycut^7 + 
         1000*m2^4*Ycut^7 - 36*m2^5*Ycut^7 - 5*m2^6*Ycut^7 - 
         19048*MBhat*Ycut^7 + 8124*m2*MBhat*Ycut^7 + 28080*m2^2*MBhat*
          Ycut^7 - 9080*m2^3*MBhat*Ycut^7 + 840*m2^4*MBhat*Ycut^7 + 
         204*m2^5*MBhat*Ycut^7 - 32*m2^6*MBhat*Ycut^7 + 
         15429*MBhat^2*Ycut^7 + 250*m2*MBhat^2*Ycut^7 - 
         3170*m2^2*MBhat^2*Ycut^7 + 220*m2^3*MBhat^2*Ycut^7 - 
         525*m2^4*MBhat^2*Ycut^7 + 516*m2^5*MBhat^2*Ycut^7 - 
         70*m2^6*MBhat^2*Ycut^7 - 2438*MBhat^3*Ycut^7 - 
         2280*m2*MBhat^3*Ycut^7 - 3040*m2^2*MBhat^3*Ycut^7 + 
         480*m2^3*MBhat^3*Ycut^7 + 510*m2^4*MBhat^3*Ycut^7 - 
         96*m2^5*MBhat^3*Ycut^7 - 520*MBhat^4*Ycut^7 + 1400*m2*MBhat^4*
          Ycut^7 + 550*m2^2*MBhat^4*Ycut^7 - 20*m2^3*MBhat^4*Ycut^7 - 
         25*m2^4*MBhat^4*Ycut^7 + 3090*Ycut^8 - 2680*m2*Ycut^8 + 
         15*m2^2*Ycut^8 + 1260*m2^3*Ycut^8 - 200*m2^4*Ycut^8 + 
         6680*MBhat*Ycut^8 - 3940*m2*MBhat*Ycut^8 - 2480*m2^2*MBhat*Ycut^8 + 
         840*m2^3*MBhat*Ycut^8 - 40*m2^4*MBhat*Ycut^8 - 
         20*m2^5*MBhat*Ycut^8 - 3027*MBhat^2*Ycut^8 - 920*m2*MBhat^2*Ycut^8 + 
         1100*m2^2*MBhat^2*Ycut^8 - 160*m2^3*MBhat^2*Ycut^8 + 
         80*m2^4*MBhat^2*Ycut^8 - 48*m2^5*MBhat^2*Ycut^8 - 
         2070*MBhat^3*Ycut^8 + 440*m2*MBhat^3*Ycut^8 + 320*m2^2*MBhat^3*
          Ycut^8 - 50*m2^4*MBhat^3*Ycut^8 + 1025*MBhat^4*Ycut^8 - 
         320*m2*MBhat^4*Ycut^8 - 80*m2^2*MBhat^4*Ycut^8 - 2830*Ycut^9 + 
         1480*m2*Ycut^9 + 615*m2^2*Ycut^9 - 180*m2^3*Ycut^9 - 
         2680*MBhat*Ycut^9 + 1380*m2*MBhat*Ycut^9 + 1605*MBhat^2*Ycut^9 + 
         360*m2*MBhat^2*Ycut^9 - 180*m2^2*MBhat^2*Ycut^9 + 
         190*MBhat^3*Ycut^9 + 280*m2*MBhat^3*Ycut^9 - 135*MBhat^4*Ycut^9 + 
         1534*Ycut^10 - 440*m2*Ycut^10 - 105*m2^2*Ycut^10 + 
         584*MBhat*Ycut^10 - 252*m2*MBhat*Ycut^10 - 315*MBhat^2*Ycut^10 - 
         280*m2*MBhat^2*Ycut^10 + 14*MBhat^3*Ycut^10 - 
         40*m2*MBhat^3*Ycut^10 - 35*MBhat^4*Ycut^10 - 498*Ycut^11 + 
         40*m2*Ycut^11 + 15*m2^2*Ycut^11 - 88*MBhat*Ycut^11 + 
         84*m2*MBhat*Ycut^11 - 3*MBhat^2*Ycut^11 + 40*m2*MBhat^2*Ycut^11 + 
         42*MBhat^3*Ycut^11 + 5*MBhat^4*Ycut^11 + 94*Ycut^12 + 
         24*MBhat*Ycut^12 - 12*m2*MBhat*Ycut^12 - 14*MBhat^2*Ycut^12 - 
         6*MBhat^3*Ycut^12 - 10*Ycut^13 + 2*MBhat^2*Ycut^13)/
        (120*(-1 + Ycut)^7) + ((8 + 24*m2 - 111*m2^2 - 56*m2^3 + 9*m2^4 - 
          32*MBhat - 64*m2*MBhat + 160*m2^2*MBhat + 48*MBhat^2 + 
          64*m2*MBhat^2 - 56*m2^2*MBhat^2 + 14*m2^3*MBhat^2 - 32*MBhat^3 - 
          32*m2*MBhat^3 + 8*MBhat^4 + 8*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2])/
        2 + ((-8 - 24*m2 + 111*m2^2 + 56*m2^3 - 9*m2^4 + 32*MBhat + 
          64*m2*MBhat - 160*m2^2*MBhat - 48*MBhat^2 - 64*m2*MBhat^2 + 
          56*m2^2*MBhat^2 - 14*m2^3*MBhat^2 + 32*MBhat^3 + 32*m2*MBhat^3 - 
          8*MBhat^4 - 8*m2*MBhat^4 - 3*m2^2*MBhat^4)*Log[1 - Ycut])/2) + 
     c[SR]^2*((-1573 + 3120*m2 + 6675*m2^2 - 8800*m2^3 + 525*m2^4 + 48*m2^5 + 
         5*m2^6 + 5664*MBhat - 10432*m2*MBhat - 800*m2^2*MBhat + 
         6400*m2^3*MBhat - 800*m2^4*MBhat - 64*m2^5*MBhat + 32*m2^6*MBhat - 
         7470*MBhat^2 + 11710*m2*MBhat^2 - 5390*m2^2*MBhat^2 + 
         1280*m2^3*MBhat^2 - 10*m2^4*MBhat^2 - 190*m2^5*MBhat^2 + 
         70*m2^6*MBhat^2 + 4384*MBhat^3 - 5600*m2*MBhat^3 + 
         1920*m2^2*MBhat^3 - 640*m2^3*MBhat^3 - 160*m2^4*MBhat^3 + 
         96*m2^5*MBhat^3 - 985*MBhat^4 + 920*m2*MBhat^4 + 40*m2^3*MBhat^4 + 
         25*m2^4*MBhat^4 + 11491*Ycut - 20400*m2*Ycut - 53385*m2^2*Ycut + 
         58240*m2^3*Ycut - 3135*m2^4*Ycut - 336*m2^5*Ycut - 35*m2^6*Ycut - 
         41568*MBhat*Ycut + 69184*m2*MBhat*Ycut + 15200*m2^2*MBhat*Ycut - 
         44800*m2^3*MBhat*Ycut + 5600*m2^4*MBhat*Ycut + 448*m2^5*MBhat*Ycut - 
         224*m2^6*MBhat*Ycut + 55170*MBhat^2*Ycut - 78130*m2*MBhat^2*Ycut + 
         34370*m2^2*MBhat^2*Ycut - 8120*m2^3*MBhat^2*Ycut + 
         70*m2^4*MBhat^2*Ycut + 1330*m2^5*MBhat^2*Ycut - 
         490*m2^6*MBhat^2*Ycut - 32608*MBhat^3*Ycut + 37280*m2*MBhat^3*Ycut - 
         13440*m2^2*MBhat^3*Ycut + 4480*m2^3*MBhat^3*Ycut + 
         1120*m2^4*MBhat^3*Ycut - 672*m2^5*MBhat^3*Ycut + 7375*MBhat^4*Ycut - 
         5960*m2*MBhat^4*Ycut + 180*m2^2*MBhat^4*Ycut - 
         280*m2^3*MBhat^4*Ycut - 175*m2^4*MBhat^4*Ycut - 36153*Ycut^2 + 
         56160*m2*Ycut^2 + 183465*m2^2*Ycut^2 - 162960*m2^3*Ycut^2 + 
         7515*m2^4*Ycut^2 + 1008*m2^5*Ycut^2 + 105*m2^6*Ycut^2 + 
         131424*MBhat*Ycut^2 - 194112*m2*MBhat*Ycut^2 - 
         79200*m2^2*MBhat*Ycut^2 + 134400*m2^3*MBhat*Ycut^2 - 
         16800*m2^4*MBhat*Ycut^2 - 1344*m2^5*MBhat*Ycut^2 + 
         672*m2^6*MBhat*Ycut^2 - 175590*MBhat^2*Ycut^2 + 
         220950*m2*MBhat^2*Ycut^2 - 91350*m2^2*MBhat^2*Ycut^2 + 
         21420*m2^3*MBhat^2*Ycut^2 - 210*m2^4*MBhat^2*Ycut^2 - 
         3990*m2^5*MBhat^2*Ycut^2 + 1470*m2^6*MBhat^2*Ycut^2 + 
         104544*MBhat^3*Ycut^2 - 105120*m2*MBhat^3*Ycut^2 + 
         40320*m2^2*MBhat^3*Ycut^2 - 13440*m2^3*MBhat^3*Ycut^2 - 
         3360*m2^4*MBhat^3*Ycut^2 + 2016*m2^5*MBhat^3*Ycut^2 - 
         23805*MBhat^4*Ycut^2 + 16200*m2*MBhat^4*Ycut^2 - 
         1170*m2^2*MBhat^4*Ycut^2 + 840*m2^3*MBhat^4*Ycut^2 + 
         525*m2^4*MBhat^4*Ycut^2 + 63615*Ycut^3 - 83520*m2*Ycut^3 - 
         352395*m2^2*Ycut^3 + 248080*m2^3*Ycut^3 - 8745*m2^4*Ycut^3 - 
         1680*m2^5*Ycut^3 - 175*m2^6*Ycut^3 - 232480*MBhat*Ycut^3 + 
         296640*m2*MBhat*Ycut^3 + 199200*m2^2*MBhat*Ycut^3 - 
         224000*m2^3*MBhat*Ycut^3 + 28000*m2^4*MBhat*Ycut^3 + 
         2240*m2^5*MBhat*Ycut^3 - 1120*m2^6*MBhat*Ycut^3 + 
         312410*MBhat^2*Ycut^3 - 340730*m2*MBhat^2*Ycut^3 + 
         128570*m2^2*MBhat^2*Ycut^3 - 29180*m2^3*MBhat^2*Ycut^3 - 
         1010*m2^4*MBhat^2*Ycut^3 + 7290*m2^5*MBhat^2*Ycut^3 - 
         2450*m2^6*MBhat^2*Ycut^3 - 187180*MBhat^3*Ycut^3 + 
         161160*m2*MBhat^3*Ycut^3 - 66800*m2^2*MBhat^3*Ycut^3 + 
         21400*m2^3*MBhat^3*Ycut^3 + 6300*m2^4*MBhat^3*Ycut^3 - 
         3360*m2^5*MBhat^3*Ycut^3 + 42875*MBhat^4*Ycut^3 - 
         23480*m2*MBhat^4*Ycut^3 + 3050*m2^2*MBhat^4*Ycut^3 - 
         1240*m2^3*MBhat^4*Ycut^3 - 875*m2^4*MBhat^4*Ycut^3 - 67815*Ycut^4 + 
         70920*m2*Ycut^4 + 410670*m2^2*Ycut^4 - 218680*m2^3*Ycut^4 + 
         4020*m2^4*Ycut^4 + 1680*m2^5*Ycut^4 + 175*m2^6*Ycut^4 + 
         249680*MBhat*Ycut^4 - 263680*m2*MBhat*Ycut^4 - 
         283040*m2^2*MBhat*Ycut^4 + 223360*m2^3*MBhat*Ycut^4 - 
         26640*m2^4*MBhat*Ycut^4 - 2880*m2^5*MBhat*Ycut^4 + 
         1120*m2^6*MBhat*Ycut^4 - 335885*MBhat^2*Ycut^4 + 
         304690*m2*MBhat^2*Ycut^4 - 99510*m2^2*MBhat^2*Ycut^4 + 
         22240*m2^3*MBhat^2*Ycut^4 + 2345*m2^4*MBhat^2*Ycut^4 - 
         7980*m2^5*MBhat^2*Ycut^4 + 2450*m2^6*MBhat^2*Ycut^4 + 
         201460*MBhat^3*Ycut^4 - 141880*m2*MBhat^3*Ycut^4 + 
         66160*m2^2*MBhat^3*Ycut^4 - 19720*m2^3*MBhat^3*Ycut^4 - 
         7300*m2^4*MBhat^3*Ycut^4 + 3360*m2^5*MBhat^3*Ycut^4 - 
         46240*MBhat^4*Ycut^4 + 18600*m2*MBhat^4*Ycut^4 - 
         4240*m2^2*MBhat^4*Ycut^4 + 980*m2^3*MBhat^4*Ycut^4 + 
         875*m2^4*MBhat^4*Ycut^4 + 43929*Ycut^5 - 32280*m2*Ycut^5 - 
         293070*m2^2*Ycut^5 + 107880*m2^3*Ycut^5 + 960*m2^4*Ycut^5 - 
         816*m2^5*Ycut^5 - 105*m2^6*Ycut^5 - 165448*MBhat*Ycut^5 + 
         134284*m2*MBhat*Ycut^5 + 237200*m2^2*MBhat*Ycut^5 - 
         134120*m2^3*MBhat*Ycut^5 + 14840*m2^4*MBhat*Ycut^5 + 
         2044*m2^5*MBhat*Ycut^5 - 672*m2^6*MBhat*Ycut^5 + 
         218715*MBhat^2*Ycut^5 - 152030*m2*MBhat^2*Ycut^5 + 
         37690*m2^2*MBhat^2*Ycut^5 - 9800*m2^3*MBhat^2*Ycut^5 - 
         2175*m2^4*MBhat^2*Ycut^5 + 5460*m2^5*MBhat^2*Ycut^5 - 
         1470*m2^6*MBhat^2*Ycut^5 - 128598*MBhat^3*Ycut^5 + 
         67240*m2*MBhat^3*Ycut^5 - 39920*m2^2*MBhat^3*Ycut^5 + 
         10920*m2^3*MBhat^3*Ycut^5 + 5110*m2^4*MBhat^3*Ycut^5 - 
         2016*m2^5*MBhat^3*Ycut^5 + 29120*MBhat^4*Ycut^5 - 
         6520*m2*MBhat^4*Ycut^5 + 3500*m2^2*MBhat^4*Ycut^5 - 
         460*m2^3*MBhat^4*Ycut^5 - 525*m2^4*MBhat^4*Ycut^5 - 15593*Ycut^6 + 
         4760*m2*Ycut^6 + 120960*m2^2*Ycut^6 - 24180*m2^3*Ycut^6 - 
         1940*m2^4*Ycut^6 + 252*m2^5*Ycut^6 + 35*m2^6*Ycut^6 + 
         67256*MBhat*Ycut^6 - 37268*m2*MBhat*Ycut^6 - 114160*m2^2*MBhat*
          Ycut^6 + 47000*m2^3*MBhat*Ycut^6 - 5000*m2^4*MBhat*Ycut^6 - 
         868*m2^5*MBhat*Ycut^6 + 224*m2^6*MBhat*Ycut^6 - 
         81027*MBhat^2*Ycut^6 + 34090*m2*MBhat^2*Ycut^6 - 
         2130*m2^2*MBhat^2*Ycut^6 + 2100*m2^3*MBhat^2*Ycut^6 + 
         1435*m2^4*MBhat^2*Ycut^6 - 2268*m2^5*MBhat^2*Ycut^6 + 
         490*m2^6*MBhat^2*Ycut^6 + 42266*MBhat^3*Ycut^6 - 
         11480*m2*MBhat^3*Ycut^6 + 14480*m2^2*MBhat^3*Ycut^6 - 
         3480*m2^3*MBhat^3*Ycut^6 - 2170*m2^4*MBhat^3*Ycut^6 + 
         672*m2^5*MBhat^3*Ycut^6 - 8680*MBhat^4*Ycut^6 - 
         840*m2*MBhat^4*Ycut^6 - 1790*m2^2*MBhat^4*Ycut^6 + 
         140*m2^3*MBhat^4*Ycut^6 + 175*m2^4*MBhat^4*Ycut^6 + 719*Ycut^7 + 
         2840*m2*Ycut^7 - 23460*m2^2*Ycut^7 - 660*m2^3*Ycut^7 + 
         1000*m2^4*Ycut^7 - 36*m2^5*Ycut^7 - 5*m2^6*Ycut^7 - 
         19048*MBhat*Ycut^7 + 8124*m2*MBhat*Ycut^7 + 28080*m2^2*MBhat*
          Ycut^7 - 9080*m2^3*MBhat*Ycut^7 + 840*m2^4*MBhat*Ycut^7 + 
         204*m2^5*MBhat*Ycut^7 - 32*m2^6*MBhat*Ycut^7 + 
         15429*MBhat^2*Ycut^7 + 250*m2*MBhat^2*Ycut^7 - 
         3170*m2^2*MBhat^2*Ycut^7 + 220*m2^3*MBhat^2*Ycut^7 - 
         525*m2^4*MBhat^2*Ycut^7 + 516*m2^5*MBhat^2*Ycut^7 - 
         70*m2^6*MBhat^2*Ycut^7 - 2438*MBhat^3*Ycut^7 - 
         2280*m2*MBhat^3*Ycut^7 - 3040*m2^2*MBhat^3*Ycut^7 + 
         480*m2^3*MBhat^3*Ycut^7 + 510*m2^4*MBhat^3*Ycut^7 - 
         96*m2^5*MBhat^3*Ycut^7 - 520*MBhat^4*Ycut^7 + 1400*m2*MBhat^4*
          Ycut^7 + 550*m2^2*MBhat^4*Ycut^7 - 20*m2^3*MBhat^4*Ycut^7 - 
         25*m2^4*MBhat^4*Ycut^7 + 3090*Ycut^8 - 2680*m2*Ycut^8 + 
         15*m2^2*Ycut^8 + 1260*m2^3*Ycut^8 - 200*m2^4*Ycut^8 + 
         6680*MBhat*Ycut^8 - 3940*m2*MBhat*Ycut^8 - 2480*m2^2*MBhat*Ycut^8 + 
         840*m2^3*MBhat*Ycut^8 - 40*m2^4*MBhat*Ycut^8 - 
         20*m2^5*MBhat*Ycut^8 - 3027*MBhat^2*Ycut^8 - 920*m2*MBhat^2*Ycut^8 + 
         1100*m2^2*MBhat^2*Ycut^8 - 160*m2^3*MBhat^2*Ycut^8 + 
         80*m2^4*MBhat^2*Ycut^8 - 48*m2^5*MBhat^2*Ycut^8 - 
         2070*MBhat^3*Ycut^8 + 440*m2*MBhat^3*Ycut^8 + 320*m2^2*MBhat^3*
          Ycut^8 - 50*m2^4*MBhat^3*Ycut^8 + 1025*MBhat^4*Ycut^8 - 
         320*m2*MBhat^4*Ycut^8 - 80*m2^2*MBhat^4*Ycut^8 - 2830*Ycut^9 + 
         1480*m2*Ycut^9 + 615*m2^2*Ycut^9 - 180*m2^3*Ycut^9 - 
         2680*MBhat*Ycut^9 + 1380*m2*MBhat*Ycut^9 + 1605*MBhat^2*Ycut^9 + 
         360*m2*MBhat^2*Ycut^9 - 180*m2^2*MBhat^2*Ycut^9 + 
         190*MBhat^3*Ycut^9 + 280*m2*MBhat^3*Ycut^9 - 135*MBhat^4*Ycut^9 + 
         1534*Ycut^10 - 440*m2*Ycut^10 - 105*m2^2*Ycut^10 + 
         584*MBhat*Ycut^10 - 252*m2*MBhat*Ycut^10 - 315*MBhat^2*Ycut^10 - 
         280*m2*MBhat^2*Ycut^10 + 14*MBhat^3*Ycut^10 - 
         40*m2*MBhat^3*Ycut^10 - 35*MBhat^4*Ycut^10 - 498*Ycut^11 + 
         40*m2*Ycut^11 + 15*m2^2*Ycut^11 - 88*MBhat*Ycut^11 + 
         84*m2*MBhat*Ycut^11 - 3*MBhat^2*Ycut^11 + 40*m2*MBhat^2*Ycut^11 + 
         42*MBhat^3*Ycut^11 + 5*MBhat^4*Ycut^11 + 94*Ycut^12 + 
         24*MBhat*Ycut^12 - 12*m2*MBhat*Ycut^12 - 14*MBhat^2*Ycut^12 - 
         6*MBhat^3*Ycut^12 - 10*Ycut^13 + 2*MBhat^2*Ycut^13)/
        (120*(-1 + Ycut)^7) + ((8 + 24*m2 - 111*m2^2 - 56*m2^3 + 9*m2^4 - 
          32*MBhat - 64*m2*MBhat + 160*m2^2*MBhat + 48*MBhat^2 + 
          64*m2*MBhat^2 - 56*m2^2*MBhat^2 + 14*m2^3*MBhat^2 - 32*MBhat^3 - 
          32*m2*MBhat^3 + 8*MBhat^4 + 8*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2])/
        2 + ((-8 - 24*m2 + 111*m2^2 + 56*m2^3 - 9*m2^4 + 32*MBhat + 
          64*m2*MBhat - 160*m2^2*MBhat - 48*MBhat^2 - 64*m2*MBhat^2 + 
          56*m2^2*MBhat^2 - 14*m2^3*MBhat^2 + 32*MBhat^3 + 32*m2*MBhat^3 - 
          8*MBhat^4 - 8*m2*MBhat^4 - 3*m2^2*MBhat^4)*Log[1 - Ycut])/2) + 
     c[VR]*((Sqrt[m2]*(-3241 - 11365*m2 + 9260*m2^2 + 5860*m2^3 - 515*m2^4 + 
          m2^5 + 10032*MBhat + 13200*m2*MBhat - 22080*m2^2*MBhat - 
          960*m2^3*MBhat - 240*m2^4*MBhat + 48*m2^5*MBhat - 10965*MBhat^2 + 
          725*m2*MBhat^2 + 10640*m2^2*MBhat^2 + 120*m2^3*MBhat^2 - 
          635*m2^4*MBhat^2 + 115*m2^5*MBhat^2 + 4960*MBhat^3 - 
          4480*m2*MBhat^3 - 640*m2^3*MBhat^3 + 160*m2^4*MBhat^3 - 
          750*MBhat^4 + 1170*m2*MBhat^4 - 450*m2^2*MBhat^4 + 
          30*m2^3*MBhat^4 + 20286*Ycut + 77490*m2*Ycut - 43920*m2^2*Ycut - 
          34080*m2^3*Ycut + 2910*m2^4*Ycut - 6*m2^5*Ycut - 63072*MBhat*Ycut - 
          99360*m2*MBhat*Ycut + 120960*m2^2*MBhat*Ycut + 
          5760*m2^3*MBhat*Ycut + 1440*m2^4*MBhat*Ycut - 288*m2^5*MBhat*Ycut + 
          69390*MBhat^2*Ycut + 10170*m2*MBhat^2*Ycut - 61020*m2^2*MBhat^2*
           Ycut - 1260*m2^3*MBhat^2*Ycut + 3810*m2^4*MBhat^2*Ycut - 
          690*m2^5*MBhat^2*Ycut - 31680*MBhat^3*Ycut + 23040*m2*MBhat^3*
           Ycut + 3840*m2^3*MBhat^3*Ycut - 960*m2^4*MBhat^3*Ycut + 
          4860*MBhat^4*Ycut - 6840*m2*MBhat^4*Ycut + 2520*m2^2*MBhat^4*Ycut - 
          180*m2^3*MBhat^4*Ycut - 53235*Ycut^2 - 221625*m2*Ycut^2 + 
          74880*m2^2*Ycut^2 + 81960*m2^3*Ycut^2 - 6735*m2^4*Ycut^2 + 
          15*m2^5*Ycut^2 + 166320*MBhat*Ycut^2 + 308880*m2*MBhat*Ycut^2 - 
          267840*m2^2*MBhat*Ycut^2 - 14400*m2^3*MBhat*Ycut^2 - 
          3600*m2^4*MBhat*Ycut^2 + 720*m2^5*MBhat*Ycut^2 - 
          184275*MBhat^2*Ycut^2 - 68985*m2*MBhat^2*Ycut^2 + 
          144090*m2^2*MBhat^2*Ycut^2 + 4770*m2^3*MBhat^2*Ycut^2 - 
          9525*m2^4*MBhat^2*Ycut^2 + 1725*m2^5*MBhat^2*Ycut^2 + 
          84960*MBhat^3*Ycut^2 - 46080*m2*MBhat^3*Ycut^2 - 
          9600*m2^3*MBhat^3*Ycut^2 + 2400*m2^4*MBhat^3*Ycut^2 - 
          13230*MBhat^4*Ycut^2 + 16560*m2*MBhat^4*Ycut^2 - 
          5760*m2^2*MBhat^4*Ycut^2 + 450*m2^3*MBhat^4*Ycut^2 + 75180*Ycut^3 + 
          342000*m2*Ycut^3 - 41640*m2^2*Ycut^3 - 103880*m2^3*Ycut^3 + 
          8080*m2^4*Ycut^3 - 20*m2^5*Ycut^3 - 236160*MBhat*Ycut^3 - 
          512640*m2*MBhat*Ycut^3 + 299520*m2^2*MBhat*Ycut^3 + 
          19200*m2^3*MBhat*Ycut^3 + 4800*m2^4*MBhat*Ycut^3 - 
          960*m2^5*MBhat*Ycut^3 + 263420*MBhat^2*Ycut^3 + 
          164900*m2*MBhat^2*Ycut^3 - 177300*m2^2*MBhat^2*Ycut^3 - 
          10340*m2^3*MBhat^2*Ycut^3 + 13220*m2^4*MBhat^2*Ycut^3 - 
          2300*m2^5*MBhat^2*Ycut^3 - 122580*MBhat^3*Ycut^3 + 
          42140*m2*MBhat^3*Ycut^3 - 700*m2^2*MBhat^3*Ycut^3 + 
          13300*m2^3*MBhat^3*Ycut^3 - 3200*m2^4*MBhat^3*Ycut^3 + 
          19360*MBhat^4*Ycut^3 - 21180*m2*MBhat^4*Ycut^3 + 
          6860*m2^2*MBhat^4*Ycut^3 - 600*m2^3*MBhat^4*Ycut^3 - 60585*Ycut^4 - 
          303000*m2*Ycut^4 - 26970*m2^2*Ycut^4 + 72510*m2^3*Ycut^4 - 
          5160*m2^4*Ycut^4 + 15*m2^5*Ycut^4 + 191720*MBhat*Ycut^4 + 
          485200*m2*MBhat*Ycut^4 - 168000*m2^2*MBhat*Ycut^4 - 
          13040*m2^3*MBhat*Ycut^4 - 4120*m2^4*MBhat*Ycut^4 + 
          720*m2^5*MBhat*Ycut^4 - 214285*MBhat^2*Ycut^4 - 
          198600*m2*MBhat^2*Ycut^4 + 119230*m2^2*MBhat^2*Ycut^4 + 
          11600*m2^3*MBhat^2*Ycut^4 - 10370*m2^4*MBhat^2*Ycut^4 + 
          1725*m2^5*MBhat^2*Ycut^4 + 99960*MBhat^3*Ycut^4 - 
          10920*m2*MBhat^3*Ycut^4 + 1160*m2^2*MBhat^3*Ycut^4 - 
          10520*m2^3*MBhat^3*Ycut^4 + 2400*m2^4*MBhat^3*Ycut^4 - 
          15870*MBhat^4*Ycut^4 + 14720*m2*MBhat^4*Ycut^4 - 
          4290*m2^2*MBhat^4*Ycut^4 + 450*m2^3*MBhat^4*Ycut^4 + 26702*Ycut^5 + 
          149100*m2*Ycut^5 + 46020*m2^2*Ycut^5 - 26180*m2^3*Ycut^5 + 
          1680*m2^4*Ycut^5 - 6*m2^5*Ycut^5 - 86354*MBhat*Ycut^5 - 
          253440*m2*MBhat*Ycut^5 + 33660*m2^2*MBhat*Ycut^5 + 
          3920*m2^3*MBhat*Ycut^5 + 1830*m2^4*MBhat*Ycut^5 - 
          288*m2^5*MBhat*Ycut^5 + 94050*MBhat^2*Ycut^5 + 128340*m2*MBhat^2*
           Ycut^5 - 42420*m2^2*MBhat^2*Ycut^5 - 6240*m2^3*MBhat^2*Ycut^5 + 
          4440*m2^4*MBhat^2*Ycut^5 - 690*m2^5*MBhat^2*Ycut^5 - 
          42060*MBhat^3*Ycut^5 - 11580*m2*MBhat^3*Ycut^5 + 
          600*m2^2*MBhat^3*Ycut^5 + 4320*m2^3*MBhat^3*Ycut^5 - 
          960*m2^4*MBhat^3*Ycut^5 + 6300*MBhat^4*Ycut^5 - 
          4440*m2*MBhat^4*Ycut^5 + 1020*m2^2*MBhat^4*Ycut^5 - 
          180*m2^3*MBhat^4*Ycut^5 - 5007*Ycut^6 - 34360*m2*Ycut^6 - 
          19740*m2^2*Ycut^6 + 3640*m2^3*Ycut^6 - 100*m2^4*Ycut^6 + 
          m2^5*Ycut^6 + 19524*MBhat*Ycut^6 + 59280*m2*MBhat*Ycut^6 + 
          6920*m2^2*MBhat*Ycut^6 - 800*m2^3*MBhat*Ycut^6 - 
          300*m2^4*MBhat*Ycut^6 + 48*m2^5*MBhat*Ycut^6 - 
          16915*MBhat^2*Ycut^6 - 41560*m2*MBhat^2*Ycut^6 + 
          7830*m2^2*MBhat^2*Ycut^6 + 1230*m2^3*MBhat^2*Ycut^6 - 
          880*m2^4*MBhat^2*Ycut^6 + 115*m2^5*MBhat^2*Ycut^6 + 
          4080*MBhat^3*Ycut^6 + 11760*m2*MBhat^3*Ycut^6 - 
          2120*m2^2*MBhat^3*Ycut^6 - 680*m2^3*MBhat^3*Ycut^6 + 
          160*m2^4*MBhat^3*Ycut^6 + 190*MBhat^4*Ycut^6 - 
          720*m2*MBhat^4*Ycut^6 + 220*m2^2*MBhat^4*Ycut^6 + 
          30*m2^3*MBhat^4*Ycut^6 - 540*Ycut^7 + 2040*m2*Ycut^7 + 
          1500*m2^2*Ycut^7 + 300*m2^3*Ycut^7 - 60*m2^4*Ycut^7 - 
          3070*MBhat*Ycut^7 + 800*m2*MBhat*Ycut^7 - 3480*m2^2*MBhat*Ycut^7 + 
          400*m2^3*MBhat*Ycut^7 - 10*m2^4*MBhat*Ycut^7 - 400*MBhat^2*Ycut^7 + 
          4980*m2*MBhat^2*Ycut^7 - 1420*m2^2*MBhat^2*Ycut^7 + 
          240*m2^3*MBhat^2*Ycut^7 + 40*m2^4*MBhat^2*Ycut^7 + 
          3540*MBhat^3*Ycut^7 - 4860*m2*MBhat^3*Ycut^7 + 1300*m2^2*MBhat^3*
           Ycut^7 - 20*m2^3*MBhat^3*Ycut^7 - 1320*MBhat^4*Ycut^7 + 
          940*m2*MBhat^4*Ycut^7 - 120*m2^2*MBhat^4*Ycut^7 + 770*Ycut^8 - 
          675*m2*Ycut^8 + 690*m2^2*Ycut^8 - 130*m2^3*Ycut^8 + 
          1360*MBhat*Ycut^8 - 1920*m2*MBhat*Ycut^8 + 360*m2^2*MBhat*Ycut^8 - 
          80*m2^3*MBhat*Ycut^8 - 270*MBhat^2*Ycut^8 - 45*m2*MBhat^2*Ycut^8 + 
          450*m2^2*MBhat^2*Ycut^8 - 120*m2^3*MBhat^2*Ycut^8 - 
          1320*MBhat^3*Ycut^8 + 1080*m2*MBhat^3*Ycut^8 - 
          240*m2^2*MBhat^3*Ycut^8 + 540*MBhat^4*Ycut^8 - 
          210*m2*MBhat^4*Ycut^8 - 440*Ycut^9 + 410*m2*Ycut^9 - 
          80*m2^2*Ycut^9 - 270*MBhat*Ycut^9 - 20*m2^2*MBhat*Ycut^9 + 
          260*MBhat^2*Ycut^9 + 90*m2*MBhat^2*Ycut^9 - 80*m2^2*MBhat^2*
           Ycut^9 + 140*MBhat^3*Ycut^9 - 100*m2*MBhat^3*Ycut^9 - 
          80*MBhat^4*Ycut^9 + 114*Ycut^10 - 15*m2*Ycut^10 - 
          28*MBhat*Ycut^10 - 10*MBhat^2*Ycut^10 - 15*m2*MBhat^2*Ycut^10 - 
          4*Ycut^11 - 2*MBhat*Ycut^11))/(15*(-1 + Ycut)^6) + 
       4*Sqrt[m2]*(-14 - 155*m2 - 194*m2^2 - 18*m2^3 + 3*m2^4 + 48*MBhat + 
         336*m2*MBhat + 192*m2^2*MBhat - 60*MBhat^2 - 242*m2*MBhat^2 - 
         47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 + 32*MBhat^3 + 64*m2*MBhat^3 - 
         6*MBhat^4 - 3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2] - 
       4*Sqrt[m2]*(-14 - 155*m2 - 194*m2^2 - 18*m2^3 + 3*m2^4 + 48*MBhat + 
         336*m2*MBhat + 192*m2^2*MBhat - 60*MBhat^2 - 242*m2*MBhat^2 - 
         47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 + 32*MBhat^3 + 64*m2*MBhat^3 - 
         6*MBhat^4 - 3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[1 - Ycut]) + 
     c[VR]^2*(-1/90*(2238 - 8840*m2 - 10550*m2^2 + 18400*m2^3 - 1150*m2^4 - 
          88*m2^5 - 10*m2^6 - 7856*MBhat + 26880*m2*MBhat - 7600*m2^2*MBhat - 
          12800*m2^3*MBhat + 1200*m2^4*MBhat + 256*m2^5*MBhat - 
          80*m2^6*MBhat + 9825*MBhat^2 - 26595*m2*MBhat^2 + 
          19320*m2^2*MBhat^2 - 1760*m2^3*MBhat^2 - 1665*m2^4*MBhat^2 + 
          1155*m2^5*MBhat^2 - 280*m2^6*MBhat^2 - 5424*MBhat^3 + 
          10640*m2*MBhat^3 - 6720*m2^2*MBhat^3 + 960*m2^3*MBhat^3 + 
          880*m2^4*MBhat^3 - 336*m2^5*MBhat^3 + 1155*MBhat^4 - 
          1320*m2*MBhat^4 + 360*m2^2*MBhat^4 - 120*m2^3*MBhat^4 - 
          75*m2^4*MBhat^4 - 11910*Ycut + 43720*m2*Ycut + 68470*m2^2*Ycut - 
          85280*m2^3*Ycut + 4670*m2^4*Ycut + 440*m2^5*Ycut + 50*m2^6*Ycut + 
          42160*MBhat*Ycut - 134400*m2*MBhat*Ycut + 14000*m2^2*MBhat*Ycut + 
          64000*m2^3*MBhat*Ycut - 6000*m2^4*MBhat*Ycut - 
          1280*m2^5*MBhat*Ycut + 400*m2^6*MBhat*Ycut - 53445*MBhat^2*Ycut + 
          134415*m2*MBhat^2*Ycut - 87420*m2^2*MBhat^2*Ycut + 
          6700*m2^3*MBhat^2*Ycut + 8325*m2^4*MBhat^2*Ycut - 
          5775*m2^5*MBhat^2*Ycut + 1400*m2^6*MBhat^2*Ycut + 
          30000*MBhat^3*Ycut - 54160*m2*MBhat^3*Ycut + 33600*m2^2*MBhat^3*
           Ycut - 4800*m2^3*MBhat^3*Ycut - 4400*m2^4*MBhat^3*Ycut + 
          1680*m2^5*MBhat^3*Ycut - 6495*MBhat^4*Ycut + 6600*m2*MBhat^4*Ycut - 
          2340*m2^2*MBhat^4*Ycut + 600*m2^3*MBhat^4*Ycut + 
          375*m2^4*MBhat^4*Ycut + 25620*Ycut^2 - 86240*m2*Ycut^2 - 
          176240*m2^2*Ycut^2 + 153760*m2^3*Ycut^2 - 6640*m2^4*Ycut^2 - 
          880*m2^5*Ycut^2 - 100*m2^6*Ycut^2 - 91520*MBhat*Ycut^2 + 
          268800*m2*MBhat*Ycut^2 + 32000*m2^2*MBhat*Ycut^2 - 
          128000*m2^3*MBhat*Ycut^2 + 12000*m2^4*MBhat*Ycut^2 + 
          2560*m2^5*MBhat*Ycut^2 - 800*m2^6*MBhat*Ycut^2 + 
          117690*MBhat^2*Ycut^2 - 272430*m2*MBhat^2*Ycut^2 + 
          151890*m2^2*MBhat^2*Ycut^2 - 8150*m2^3*MBhat^2*Ycut^2 - 
          16650*m2^4*MBhat^2*Ycut^2 + 11550*m2^5*MBhat^2*Ycut^2 - 
          2800*m2^6*MBhat^2*Ycut^2 - 67200*MBhat^3*Ycut^2 + 
          110720*m2*MBhat^3*Ycut^2 - 67200*m2^2*MBhat^3*Ycut^2 + 
          9600*m2^3*MBhat^3*Ycut^2 + 8800*m2^4*MBhat^3*Ycut^2 - 
          3360*m2^5*MBhat^3*Ycut^2 + 14790*MBhat^4*Ycut^2 - 
          13200*m2*MBhat^4*Ycut^2 + 6030*m2^2*MBhat^4*Ycut^2 - 
          1200*m2^3*MBhat^4*Ycut^2 - 750*m2^4*MBhat^4*Ycut^2 - 28020*Ycut^3 + 
          84640*m2*Ycut^3 + 228640*m2^2*Ycut^3 - 131360*m2^3*Ycut^3 + 
          3040*m2^4*Ycut^3 + 880*m2^5*Ycut^3 + 100*m2^6*Ycut^3 + 
          101120*MBhat*Ycut^3 - 268800*m2*MBhat*Ycut^3 - 112000*m2^2*MBhat*
           Ycut^3 + 128000*m2^3*MBhat*Ycut^3 - 12000*m2^4*MBhat*Ycut^3 - 
          2560*m2^5*MBhat*Ycut^3 + 800*m2^6*MBhat*Ycut^3 - 
          130650*MBhat^2*Ycut^3 + 275310*m2*MBhat^2*Ycut^3 - 
          124170*m2^2*MBhat^2*Ycut^3 + 6910*m2^3*MBhat^2*Ycut^3 + 
          14250*m2^4*MBhat^2*Ycut^3 - 11550*m2^5*MBhat^2*Ycut^3 + 
          2800*m2^6*MBhat^2*Ycut^3 + 75120*MBhat^3*Ycut^3 - 
          112960*m2*MBhat^3*Ycut^3 + 70320*m2^2*MBhat^3*Ycut^3 - 
          12000*m2^3*MBhat^3*Ycut^3 - 8800*m2^4*MBhat^3*Ycut^3 + 
          3360*m2^5*MBhat^3*Ycut^3 - 16710*MBhat^4*Ycut^3 + 
          13200*m2*MBhat^4*Ycut^3 - 8310*m2^2*MBhat^4*Ycut^3 + 
          1200*m2^3*MBhat^4*Ycut^3 + 750*m2^4*MBhat^4*Ycut^3 + 15810*Ycut^4 - 
          41120*m2*Ycut^4 - 153620*m2^2*Ycut^4 + 48880*m2^3*Ycut^4 + 
          1180*m2^4*Ycut^4 - 440*m2^5*Ycut^4 - 50*m2^6*Ycut^4 - 
          58960*MBhat*Ycut^4 + 135840*m2*MBhat*Ycut^4 + 118880*m2^2*MBhat*
           Ycut^4 - 69280*m2^3*MBhat*Ycut^4 + 8160*m2^4*MBhat*Ycut^4 + 
          1280*m2^5*MBhat*Ycut^4 - 400*m2^6*MBhat*Ycut^4 + 
          71010*MBhat^2*Ycut^4 - 134220*m2*MBhat^2*Ycut^4 + 
          43860*m2^2*MBhat^2*Ycut^4 - 8300*m2^3*MBhat^2*Ycut^4 - 
          3675*m2^4*MBhat^2*Ycut^4 + 5775*m2^5*MBhat^2*Ycut^4 - 
          1400*m2^6*MBhat^2*Ycut^4 - 37920*MBhat^3*Ycut^4 + 
          55280*m2*MBhat^3*Ycut^4 - 42720*m2^2*MBhat^3*Ycut^4 + 
          10320*m2^3*MBhat^3*Ycut^4 + 4400*m2^4*MBhat^3*Ycut^4 - 
          1680*m2^5*MBhat^3*Ycut^4 + 8220*MBhat^4*Ycut^4 - 
          6600*m2*MBhat^4*Ycut^4 + 6540*m2^2*MBhat^4*Ycut^4 - 
          600*m2^3*MBhat^4*Ycut^4 - 375*m2^4*MBhat^4*Ycut^4 - 3546*Ycut^5 + 
          7360*m2*Ycut^5 + 45580*m2^2*Ycut^5 - 1520*m2^3*Ycut^5 - 
          1940*m2^4*Ycut^5 + 88*m2^5*Ycut^5 + 10*m2^6*Ycut^5 + 
          19892*MBhat*Ycut^5 - 32700*m2*MBhat*Ycut^5 - 54340*m2^2*MBhat*
           Ycut^5 + 22700*m2^3*MBhat*Ycut^5 - 3600*m2^4*MBhat*Ycut^5 - 
          256*m2^5*MBhat*Ycut^5 + 80*m2^6*MBhat*Ycut^5 - 
          11250*MBhat^2*Ycut^5 + 16980*m2*MBhat^2*Ycut^5 - 
          960*m2^2*MBhat^2*Ycut^5 + 7600*m2^3*MBhat^2*Ycut^5 - 
          1905*m2^4*MBhat^2*Ycut^5 - 1155*m2^5*MBhat^2*Ycut^5 + 
          280*m2^6*MBhat^2*Ycut^5 - 1812*MBhat^3*Ycut^5 - 
          6160*m2*MBhat^3*Ycut^5 + 15900*m2^2*MBhat^3*Ycut^5 - 
          5160*m2^3*MBhat^3*Ycut^5 - 880*m2^4*MBhat^3*Ycut^5 + 
          336*m2^5*MBhat^3*Ycut^5 + 1020*MBhat^4*Ycut^5 + 
          1320*m2*MBhat^4*Ycut^5 - 2640*m2^2*MBhat^4*Ycut^5 + 
          120*m2^3*MBhat^4*Ycut^5 + 75*m2^4*MBhat^4*Ycut^5 - 1490*Ycut^6 + 
          1600*m2*Ycut^6 - 440*m2^2*Ycut^6 - 3600*m2^3*Ycut^6 + 
          570*m2^4*Ycut^6 - 10260*MBhat*Ycut^6 + 8940*m2*MBhat*Ycut^6 + 
          9380*m2^2*MBhat*Ycut^6 - 5260*m2^3*MBhat*Ycut^6 + 
          720*m2^4*MBhat*Ycut^6 - 3620*MBhat^2*Ycut^6 + 9870*m2*MBhat^2*
           Ycut^6 - 3150*m2^2*MBhat^2*Ycut^6 - 3870*m2^3*MBhat^2*Ycut^6 + 
          1230*m2^4*MBhat^2*Ycut^6 + 11700*MBhat^3*Ycut^6 - 
          4960*m2*MBhat^3*Ycut^6 - 3420*m2^2*MBhat^3*Ycut^6 + 
          1080*m2^3*MBhat^3*Ycut^6 - 3030*MBhat^4*Ycut^6 + 
          270*m2^2*MBhat^4*Ycut^6 + 3130*Ycut^7 - 2240*m2*Ycut^7 - 
          2120*m2^2*Ycut^7 + 720*m2^3*Ycut^7 + 30*m2^4*Ycut^7 + 
          8520*MBhat*Ycut^7 - 6360*m2*MBhat*Ycut^7 - 460*m2^2*MBhat*Ycut^7 + 
          620*m2^3*MBhat*Ycut^7 - 740*MBhat^2*Ycut^7 - 4110*m2*MBhat^2*
           Ycut^7 + 630*m2^2*MBhat^2*Ycut^7 + 870*m2^3*MBhat^2*Ycut^7 - 
          150*m2^4*MBhat^2*Ycut^7 - 5400*MBhat^3*Ycut^7 + 
          1760*m2*MBhat^3*Ycut^7 + 180*m2^2*MBhat^3*Ycut^7 + 
          1110*MBhat^4*Ycut^7 + 90*m2^2*MBhat^4*Ycut^7 - 3080*Ycut^8 + 
          1480*m2*Ycut^8 + 370*m2^2*Ycut^8 - 3720*MBhat*Ycut^8 + 
          2040*m2*MBhat*Ycut^8 + 140*m2^2*MBhat*Ycut^8 + 
          20*m2^3*MBhat*Ycut^8 + 1525*MBhat^2*Ycut^8 + 975*m2*MBhat^2*
           Ycut^8 + 1080*MBhat^3*Ycut^8 - 160*m2*MBhat^3*Ycut^8 + 
          60*m2^2*MBhat^3*Ycut^8 - 15*MBhat^4*Ycut^8 + 1640*Ycut^9 - 
          360*m2*Ycut^9 - 90*m2^2*Ycut^9 + 660*MBhat*Ycut^9 - 
          300*m2*MBhat*Ycut^9 - 385*MBhat^2*Ycut^9 - 195*m2*MBhat^2*Ycut^9 - 
          180*MBhat^3*Ycut^9 - 45*MBhat^4*Ycut^9 - 442*Ycut^10 - 
          36*MBhat*Ycut^10 + 60*m2*MBhat*Ycut^10 + 50*MBhat^2*Ycut^10 + 
          36*MBhat^3*Ycut^10 + 50*Ycut^11 - 10*MBhat^2*Ycut^11)/
         (-1 + Ycut)^5 + (2*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 
          48*MBhat + 400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 
          153*m2^2*MBhat^2 + 35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 
          12*MBhat^4 + 9*m2^2*MBhat^4)*Log[m2])/3 - 
       (2*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 48*MBhat + 
          400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 153*m2^2*MBhat^2 + 
          35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 12*MBhat^4 + 
          9*m2^2*MBhat^4)*Log[1 - Ycut])/3) + 
     c[VL]^2*((-2238 + 8840*m2 + 10550*m2^2 - 18400*m2^3 + 1150*m2^4 + 
         88*m2^5 + 10*m2^6 + 7856*MBhat - 26880*m2*MBhat + 7600*m2^2*MBhat + 
         12800*m2^3*MBhat - 1200*m2^4*MBhat - 256*m2^5*MBhat + 
         80*m2^6*MBhat - 9825*MBhat^2 + 26595*m2*MBhat^2 - 
         19320*m2^2*MBhat^2 + 1760*m2^3*MBhat^2 + 1665*m2^4*MBhat^2 - 
         1155*m2^5*MBhat^2 + 280*m2^6*MBhat^2 + 5424*MBhat^3 - 
         10640*m2*MBhat^3 + 6720*m2^2*MBhat^3 - 960*m2^3*MBhat^3 - 
         880*m2^4*MBhat^3 + 336*m2^5*MBhat^3 - 1155*MBhat^4 + 
         1320*m2*MBhat^4 - 360*m2^2*MBhat^4 + 120*m2^3*MBhat^4 + 
         75*m2^4*MBhat^4 + 16386*Ycut - 61400*m2*Ycut - 89570*m2^2*Ycut + 
         122080*m2^3*Ycut - 6970*m2^4*Ycut - 616*m2^5*Ycut - 70*m2^6*Ycut - 
         57872*MBhat*Ycut + 188160*m2*MBhat*Ycut - 29200*m2^2*MBhat*Ycut - 
         89600*m2^3*MBhat*Ycut + 8400*m2^4*MBhat*Ycut + 
         1792*m2^5*MBhat*Ycut - 560*m2^6*MBhat*Ycut + 73095*MBhat^2*Ycut - 
         187605*m2*MBhat^2*Ycut + 126060*m2^2*MBhat^2*Ycut - 
         10220*m2^3*MBhat^2*Ycut - 11655*m2^4*MBhat^2*Ycut + 
         8085*m2^5*MBhat^2*Ycut - 1960*m2^6*MBhat^2*Ycut - 
         40848*MBhat^3*Ycut + 75440*m2*MBhat^3*Ycut - 47040*m2^2*MBhat^3*
          Ycut + 6720*m2^3*MBhat^3*Ycut + 6160*m2^4*MBhat^3*Ycut - 
         2352*m2^5*MBhat^3*Ycut + 8805*MBhat^4*Ycut - 9240*m2*MBhat^4*Ycut + 
         3060*m2^2*MBhat^4*Ycut - 840*m2^3*MBhat^4*Ycut - 
         525*m2^4*MBhat^4*Ycut - 51678*Ycut^2 + 182520*m2*Ycut^2 + 
         323730*m2^2*Ycut^2 - 342720*m2^3*Ycut^2 + 17130*m2^4*Ycut^2 + 
         1848*m2^5*Ycut^2 + 210*m2^6*Ycut^2 + 183696*MBhat*Ycut^2 - 
         564480*m2*MBhat*Ycut^2 + 3600*m2^2*MBhat*Ycut^2 + 
         268800*m2^3*MBhat*Ycut^2 - 25200*m2^4*MBhat*Ycut^2 - 
         5376*m2^5*MBhat*Ycut^2 + 1680*m2^6*MBhat*Ycut^2 - 
         234405*MBhat^2*Ycut^2 + 567855*m2*MBhat^2*Ycut^2 - 
         346050*m2^2*MBhat^2*Ycut^2 + 23310*m2^3*MBhat^2*Ycut^2 + 
         34965*m2^4*MBhat^2*Ycut^2 - 24255*m2^5*MBhat^2*Ycut^2 + 
         5880*m2^6*MBhat^2*Ycut^2 + 132624*MBhat^3*Ycut^2 - 
         229680*m2*MBhat^3*Ycut^2 + 141120*m2^2*MBhat^3*Ycut^2 - 
         20160*m2^3*MBhat^3*Ycut^2 - 18480*m2^4*MBhat^3*Ycut^2 + 
         7056*m2^5*MBhat^3*Ycut^2 - 28935*MBhat^4*Ycut^2 + 
         27720*m2*MBhat^4*Ycut^2 - 11070*m2^2*MBhat^4*Ycut^2 + 
         2520*m2^3*MBhat^4*Ycut^2 + 1575*m2^4*MBhat^4*Ycut^2 + 91170*Ycut^3 - 
         300840*m2*Ycut^3 - 649590*m2^2*Ycut^3 + 524160*m2^3*Ycut^3 - 
         20990*m2^4*Ycut^3 - 3080*m2^5*Ycut^3 - 350*m2^6*Ycut^3 - 
         326320*MBhat*Ycut^3 + 940800*m2*MBhat*Ycut^3 + 
         162000*m2^2*MBhat*Ycut^3 - 448000*m2^3*MBhat*Ycut^3 + 
         42000*m2^4*MBhat*Ycut^3 + 8960*m2^5*MBhat*Ycut^3 - 
         2800*m2^6*MBhat*Ycut^3 + 419955*MBhat^2*Ycut^3 - 
         954585*m2*MBhat^2*Ycut^3 + 510570*m2^2*MBhat^2*Ycut^3 - 
         20310*m2^3*MBhat^2*Ycut^3 - 63075*m2^4*MBhat^2*Ycut^3 + 
         42345*m2^5*MBhat^2*Ycut^3 - 9800*m2^6*MBhat^2*Ycut^3 - 
         239820*MBhat^3*Ycut^3 + 387360*m2*MBhat^3*Ycut^3 - 
         232920*m2^2*MBhat^3*Ycut^3 + 30000*m2^3*MBhat^3*Ycut^3 + 
         32900*m2^4*MBhat^3*Ycut^3 - 11760*m2^5*MBhat^3*Ycut^3 + 
         52785*MBhat^4*Ycut^3 - 45720*m2*MBhat^4*Ycut^3 + 
         21750*m2^2*MBhat^4*Ycut^3 - 3720*m2^3*MBhat^4*Ycut^3 - 
         2625*m2^4*MBhat^4*Ycut^3 - 97470*Ycut^4 + 296640*m2*Ycut^4 + 
         787140*m2^2*Ycut^4 - 465360*m2^3*Ycut^4 + 11540*m2^4*Ycut^4 + 
         3080*m2^5*Ycut^4 + 350*m2^6*Ycut^4 + 352240*MBhat*Ycut^4 - 
         942240*m2*MBhat*Ycut^4 - 370080*m2^2*MBhat*Ycut^4 + 
         443680*m2^3*MBhat*Ycut^4 - 36960*m2^4*MBhat*Ycut^4 - 
         10880*m2^5*MBhat*Ycut^4 + 2800*m2^6*MBhat*Ycut^4 - 
         453210*MBhat^2*Ycut^4 + 958920*m2*MBhat^2*Ycut^4 - 
         425790*m2^2*MBhat^2*Ycut^4 - 2430*m2^3*MBhat^2*Ycut^4 + 
         68775*m2^4*MBhat^2*Ycut^4 - 44415*m2^5*MBhat^2*Ycut^4 + 
         9800*m2^6*MBhat^2*Ycut^4 + 257460*MBhat^3*Ycut^4 - 
         385440*m2*MBhat^3*Ycut^4 + 226200*m2^2*MBhat^3*Ycut^4 - 
         23040*m2^3*MBhat^3*Ycut^4 - 35900*m2^4*MBhat^3*Ycut^4 + 
         11760*m2^5*MBhat^3*Ycut^4 - 56400*MBhat^4*Ycut^4 + 
         43560*m2*MBhat^4*Ycut^4 - 25320*m2^2*MBhat^4*Ycut^4 + 
         2940*m2^3*MBhat^4*Ycut^4 + 2625*m2^4*MBhat^4*Ycut^4 + 63330*Ycut^5 - 
         174240*m2*Ycut^5 - 582900*m2^2*Ycut^5 + 233520*m2^3*Ycut^5 - 
         900*m2^4*Ycut^5 - 1272*m2^5*Ycut^5 - 210*m2^6*Ycut^5 - 
         235512*MBhat*Ycut^5 + 572160*m2*MBhat*Ycut^5 + 
         384540*m2^2*MBhat*Ycut^5 - 258660*m2^3*MBhat*Ycut^5 + 
         16380*m2^4*MBhat*Ycut^5 + 7476*m2^5*MBhat*Ycut^5 - 
         1680*m2^6*MBhat*Ycut^5 + 292950*MBhat^2*Ycut^5 - 
         569400*m2*MBhat^2*Ycut^5 + 186510*m2^2*MBhat^2*Ycut^5 + 
         15870*m2^3*MBhat^2*Ycut^5 - 45105*m2^4*MBhat^2*Ycut^5 + 
         28665*m2^5*MBhat^2*Ycut^5 - 5880*m2^6*MBhat^2*Ycut^5 - 
         155478*MBhat^3*Ycut^5 + 215040*m2*MBhat^3*Ycut^5 - 
         126540*m2^2*MBhat^3*Ycut^5 + 8400*m2^3*MBhat^3*Ycut^5 + 
         23730*m2^4*MBhat^3*Ycut^5 - 7056*m2^5*MBhat^3*Ycut^5 + 
         31920*MBhat^4*Ycut^5 - 21720*m2*MBhat^4*Ycut^5 + 
         18060*m2^2*MBhat^4*Ycut^5 - 1380*m2^3*MBhat^4*Ycut^5 - 
         1575*m2^4*MBhat^4*Ycut^5 - 22465*Ycut^6 + 54480*m2*Ycut^6 + 
         251070*m2^2*Ycut^6 - 56780*m2^3*Ycut^6 - 1955*m2^4*Ycut^6 + 
         364*m2^5*Ycut^6 + 70*m2^6*Ycut^6 + 98504*MBhat*Ycut^6 - 
         204960*m2*MBhat*Ycut^6 - 205620*m2^2*MBhat*Ycut^6 + 
         82380*m2^3*MBhat*Ycut^6 - 3300*m2^4*MBhat*Ycut^6 - 
         3052*m2^5*MBhat*Ycut^6 + 560*m2^6*MBhat*Ycut^6 - 
         103531*MBhat^2*Ycut^6 + 177120*m2*MBhat^2*Ycut^6 - 
         25350*m2^2*MBhat^2*Ycut^6 - 11270*m2^3*MBhat^2*Ycut^6 + 
         17640*m2^4*MBhat^2*Ycut^6 - 10899*m2^5*MBhat^2*Ycut^6 + 
         1960*m2^6*MBhat^2*Ycut^6 + 33306*MBhat^3*Ycut^6 - 
         44640*m2*MBhat^3*Ycut^6 + 33780*m2^2*MBhat^3*Ycut^6 - 
         240*m2^3*MBhat^3*Ycut^6 - 9310*m2^4*MBhat^3*Ycut^6 + 
         2352*m2^5*MBhat^3*Ycut^6 - 2520*MBhat^4*Ycut^6 + 
         2040*m2*MBhat^4*Ycut^6 - 7890*m2^2*MBhat^4*Ycut^6 + 
         420*m2^3*MBhat^4*Ycut^6 + 525*m2^4*MBhat^4*Ycut^6 + 775*Ycut^7 - 
         3120*m2*Ycut^7 - 53610*m2^2*Ycut^7 + 2420*m2^3*Ycut^7 + 
         1325*m2^4*Ycut^7 - 52*m2^5*Ycut^7 - 10*m2^6*Ycut^7 - 
         30872*MBhat*Ycut^7 + 46080*m2*MBhat*Ycut^7 + 48600*m2^2*MBhat*
          Ycut^7 - 11160*m2^3*MBhat*Ycut^7 - 300*m2^4*MBhat*Ycut^7 + 
         676*m2^5*MBhat*Ycut^7 - 80*m2^6*MBhat*Ycut^7 + 
         16237*MBhat^2*Ycut^7 - 14880*m2*MBhat^2*Ycut^7 - 
         10770*m2^2*MBhat^2*Ycut^7 + 3710*m2^3*MBhat^2*Ycut^7 - 
         3240*m2^4*MBhat^2*Ycut^7 + 2133*m2^5*MBhat^2*Ycut^7 - 
         280*m2^6*MBhat^2*Ycut^7 + 19482*MBhat^3*Ycut^7 - 
         18720*m2*MBhat^3*Ycut^7 + 1440*m2^2*MBhat^3*Ycut^7 - 
         960*m2^3*MBhat^3*Ycut^7 + 1930*m2^4*MBhat^3*Ycut^7 - 
         336*m2^5*MBhat^3*Ycut^7 - 9240*MBhat^4*Ycut^7 + 
         3480*m2*MBhat^4*Ycut^7 + 2010*m2^2*MBhat^4*Ycut^7 - 
         60*m2^3*MBhat^4*Ycut^7 - 75*m2^4*MBhat^4*Ycut^7 + 4845*Ycut^8 - 
         5160*m2*Ycut^8 + 3030*m2^2*Ycut^8 + 1260*m2^3*Ycut^8 - 
         345*m2^4*Ycut^8 + 12120*MBhat*Ycut^8 - 12000*m2*MBhat*Ycut^8 + 
         120*m2^2*MBhat*Ycut^8 - 600*m2^3*MBhat*Ycut^8 + 
         180*m2^4*MBhat*Ycut^8 - 60*m2^5*MBhat*Ycut^8 - 4026*MBhat^2*Ycut^8 - 
         4515*m2*MBhat^2*Ycut^8 + 4830*m2^2*MBhat^2*Ycut^8 - 
         330*m2^3*MBhat^2*Ycut^8 - 45*m2^4*MBhat^2*Ycut^8 - 
         144*m2^5*MBhat^2*Ycut^8 - 16230*MBhat^3*Ycut^8 + 
         14640*m2*MBhat^3*Ycut^8 - 3600*m2^2*MBhat^3*Ycut^8 + 
         240*m2^3*MBhat^3*Ycut^8 - 150*m2^4*MBhat^3*Ycut^8 + 
         6315*MBhat^4*Ycut^8 - 1680*m2*MBhat^4*Ycut^8 - 
         240*m2^2*MBhat^4*Ycut^8 - 4315*Ycut^9 + 3160*m2*Ycut^9 + 
         70*m2^2*Ycut^9 - 180*m2^3*Ycut^9 + 15*m2^4*Ycut^9 - 
         4440*MBhat*Ycut^9 + 3840*m2*MBhat*Ycut^9 - 1780*m2^2*MBhat*Ycut^9 + 
         380*m2^3*MBhat*Ycut^9 + 4070*MBhat^2*Ycut^9 + 
         405*m2*MBhat^2*Ycut^9 - 690*m2^2*MBhat^2*Ycut^9 - 
         90*m2^3*MBhat^2*Ycut^9 + 75*m2^4*MBhat^2*Ycut^9 + 
         4590*MBhat^3*Ycut^9 - 3760*m2*MBhat^3*Ycut^9 + 
         900*m2^2*MBhat^3*Ycut^9 - 1725*MBhat^4*Ycut^9 + 
         240*m2*MBhat^4*Ycut^9 + 2203*Ycut^10 - 1000*m2*Ycut^10 + 
         80*m2^2*Ycut^10 + 456*MBhat*Ycut^10 - 480*m2*MBhat*Ycut^10 + 
         220*m2^2*MBhat*Ycut^10 - 20*m2^3*MBhat*Ycut^10 - 
         1460*MBhat^2*Ycut^10 + 105*m2*MBhat^2*Ycut^10 - 
         546*MBhat^3*Ycut^10 + 400*m2*MBhat^3*Ycut^10 - 
         60*m2^2*MBhat^3*Ycut^10 + 135*MBhat^4*Ycut^10 - 621*Ycut^11 + 
         120*m2*Ycut^11 + 168*MBhat*Ycut^11 + 156*MBhat^2*Ycut^11 - 
         15*m2*MBhat^2*Ycut^11 + 42*MBhat^3*Ycut^11 + 15*MBhat^4*Ycut^11 + 
         83*Ycut^12 - 24*MBhat*Ycut^12 - 7*MBhat^2*Ycut^12 - 
         6*MBhat^3*Ycut^12 - 5*Ycut^13 + MBhat^2*Ycut^13)/
        (90*(-1 + Ycut)^7) + (2*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 
          48*MBhat + 400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 
          153*m2^2*MBhat^2 + 35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 
          12*MBhat^4 + 9*m2^2*MBhat^4)*Log[m2])/3 - 
       (2*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 48*MBhat + 
          400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 153*m2^2*MBhat^2 + 
          35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 12*MBhat^4 + 
          9*m2^2*MBhat^4)*Log[1 - Ycut])/3) + 
     c[T]^2*((2*(-4233 + 26000*m2 + 22175*m2^2 - 47200*m2^3 + 3025*m2^4 + 
          208*m2^5 + 25*m2^6 + 14432*MBhat - 76224*m2*MBhat + 
          32800*m2^2*MBhat + 32000*m2^3*MBhat - 2400*m2^4*MBhat - 
          832*m2^5*MBhat + 224*m2^6*MBhat - 16890*MBhat^2 + 
          71250*m2*MBhat^2 - 61110*m2^2*MBhat^2 + 3200*m2^3*MBhat^2 + 
          6690*m2^4*MBhat^2 - 4050*m2^5*MBhat^2 + 910*m2^6*MBhat^2 + 
          8544*MBhat^3 - 25760*m2*MBhat^3 + 21120*m2^2*MBhat^3 - 
          1920*m2^3*MBhat^3 - 3040*m2^4*MBhat^3 + 1056*m2^5*MBhat^3 - 
          1665*MBhat^4 + 2520*m2*MBhat^4 - 1440*m2^2*MBhat^4 + 
          360*m2^3*MBhat^4 + 225*m2^4*MBhat^4 + 31071*Ycut - 184400*m2*Ycut - 
          198125*m2^2*Ycut + 313600*m2^3*Ycut - 18475*m2^4*Ycut - 
          1456*m2^5*Ycut - 175*m2^6*Ycut - 106784*MBhat*Ycut + 
          545088*m2*MBhat*Ycut - 162400*m2^2*MBhat*Ycut - 
          224000*m2^3*MBhat*Ycut + 16800*m2^4*MBhat*Ycut + 
          5824*m2^5*MBhat*Ycut - 1568*m2^6*MBhat*Ycut + 126870*MBhat^2*Ycut - 
          516030*m2*MBhat^2*Ycut + 401130*m2^2*MBhat^2*Ycut - 
          16520*m2^3*MBhat^2*Ycut - 46830*m2^4*MBhat^2*Ycut + 
          28350*m2^5*MBhat^2*Ycut - 6370*m2^6*MBhat^2*Ycut - 
          65568*MBhat^3*Ycut + 189920*m2*MBhat^3*Ycut - 147840*m2^2*MBhat^3*
           Ycut + 13440*m2^3*MBhat^3*Ycut + 21280*m2^4*MBhat^3*Ycut - 
          7392*m2^5*MBhat^3*Ycut + 13095*MBhat^4*Ycut - 19080*m2*MBhat^4*
           Ycut + 11700*m2^2*MBhat^4*Ycut - 2520*m2^3*MBhat^4*Ycut - 
          1575*m2^4*MBhat^4*Ycut - 98253*Ycut^2 + 561600*m2*Ycut^2 + 
          744525*m2^2*Ycut^2 - 882000*m2^3*Ycut^2 + 45975*m2^4*Ycut^2 + 
          4368*m2^5*Ycut^2 + 525*m2^6*Ycut^2 + 340512*MBhat*Ycut^2 - 
          1675584*m2*MBhat*Ycut^2 + 252000*m2^2*MBhat*Ycut^2 + 
          672000*m2^3*MBhat*Ycut^2 - 50400*m2^4*MBhat*Ycut^2 - 
          17472*m2^5*MBhat*Ycut^2 + 4704*m2^6*MBhat*Ycut^2 - 
          410850*MBhat^2*Ycut^2 + 1608570*m2*MBhat^2*Ycut^2 - 
          1110150*m2^2*MBhat^2*Ycut^2 + 28980*m2^3*MBhat^2*Ycut^2 + 
          140490*m2^4*MBhat^2*Ycut^2 - 85050*m2^5*MBhat^2*Ycut^2 + 
          19110*m2^6*MBhat^2*Ycut^2 + 216864*MBhat^3*Ycut^2 - 
          603360*m2*MBhat^3*Ycut^2 + 443520*m2^2*MBhat^3*Ycut^2 - 
          40320*m2^3*MBhat^3*Ycut^2 - 63840*m2^4*MBhat^3*Ycut^2 + 
          22176*m2^5*MBhat^3*Ycut^2 - 44325*MBhat^4*Ycut^2 + 
          62280*m2*MBhat^4*Ycut^2 - 40770*m2^2*MBhat^4*Ycut^2 + 
          7560*m2^3*MBhat^4*Ycut^2 + 4725*m2^4*MBhat^4*Ycut^2 + 
          173835*Ycut^3 - 952800*m2*Ycut^3 - 1541175*m2^2*Ycut^3 + 
          1352400*m2^3*Ycut^3 - 57725*m2^4*Ycut^3 - 7280*m2^5*Ycut^3 - 
          875*m2^6*Ycut^3 - 607840*MBhat*Ycut^3 + 2873280*m2*MBhat*Ycut^3 + 
          50400*m2^2*MBhat*Ycut^3 - 1120000*m2^3*MBhat*Ycut^3 + 
          84000*m2^4*MBhat*Ycut^3 + 29120*m2^5*MBhat*Ycut^3 - 
          7840*m2^6*MBhat*Ycut^3 + 741630*MBhat^2*Ycut^3 - 
          2796150*m2*MBhat^2*Ycut^3 + 1666170*m2^2*MBhat^2*Ycut^3 - 
          12900*m2^3*MBhat^2*Ycut^3 - 234870*m2^4*MBhat^2*Ycut^3 + 
          143670*m2^5*MBhat^2*Ycut^3 - 31850*m2^6*MBhat^2*Ycut^3 - 
          397140*MBhat^3*Ycut^3 + 1068360*m2*MBhat^3*Ycut^3 - 
          742080*m2^2*MBhat^3*Ycut^3 + 67800*m2^3*MBhat^3*Ycut^3 + 
          108500*m2^4*MBhat^3*Ycut^3 - 36960*m2^5*MBhat^3*Ycut^3 + 
          82515*MBhat^4*Ycut^3 - 113400*m2*MBhat^4*Ycut^3 + 
          79770*m2^2*MBhat^4*Ycut^3 - 12120*m2^3*MBhat^4*Ycut^3 - 
          7875*m2^4*MBhat^4*Ycut^3 - 186435*Ycut^4 + 973800*m2*Ycut^4 + 
          1916550*m2^2*Ycut^4 - 1205400*m2^3*Ycut^4 + 34100*m2^4*Ycut^4 + 
          7280*m2^5*Ycut^4 + 875*m2^6*Ycut^4 + 660880*MBhat*Ycut^4 - 
          2977920*m2*MBhat*Ycut^4 - 640800*m2^2*MBhat*Ycut^4 + 
          1123840*m2^3*MBhat*Ycut^4 - 82320*m2^4*MBhat*Ycut^4 - 
          31040*m2^5*MBhat*Ycut^4 + 7840*m2^6*MBhat*Ycut^4 - 
          798765*MBhat^2*Ycut^4 + 2918310*m2*MBhat^2*Ycut^4 - 
          1441230*m2^2*MBhat^2*Ycut^4 - 11040*m2^3*MBhat^2*Ycut^4 + 
          228165*m2^4*MBhat^2*Ycut^4 - 145740*m2^5*MBhat^2*Ycut^4 + 
          31850*m2^6*MBhat^2*Ycut^4 + 421260*MBhat^3*Ycut^4 - 
          1129080*m2*MBhat^3*Ycut^4 + 755040*m2^2*MBhat^3*Ycut^4 - 
          74760*m2^3*MBhat^3*Ycut^4 - 111500*m2^4*MBhat^3*Ycut^4 + 
          36960*m2^5*MBhat^3*Ycut^4 - 86940*MBhat^4*Ycut^4 + 
          123720*m2*MBhat^4*Ycut^4 - 96300*m2^2*MBhat^4*Ycut^4 + 
          11340*m2^3*MBhat^4*Ycut^4 + 7875*m2^4*MBhat^4*Ycut^4 + 
          121245*Ycut^5 - 600120*m2*Ycut^5 - 1449510*m2^2*Ycut^5 + 
          604680*m2^3*Ycut^5 - 2160*m2^4*Ycut^5 - 3792*m2^5*Ycut^5 - 
          525*m2^6*Ycut^5 - 452544*MBhat*Ycut^5 + 1887828*m2*MBhat*Ycut^5 + 
          865680*m2^2*MBhat*Ycut^5 - 693480*m2^3*MBhat*Ycut^5 + 
          52080*m2^4*MBhat*Ycut^5 + 19572*m2^5*MBhat*Ycut^5 - 
          4704*m2^6*MBhat*Ycut^5 + 497595*MBhat^2*Ycut^5 - 
          1804170*m2*MBhat^2*Ycut^5 + 685650*m2^2*MBhat^2*Ycut^5 - 
          1080*m2^3*MBhat^2*Ycut^5 - 123075*m2^4*MBhat^2*Ycut^5 + 
          89460*m2^5*MBhat^2*Ycut^5 - 19110*m2^6*MBhat^2*Ycut^5 - 
          223458*MBhat^3*Ycut^5 + 687720*m2*MBhat^3*Ycut^5 - 
          476640*m2^2*MBhat^3*Ycut^5 + 59640*m2^3*MBhat^3*Ycut^5 + 
          69090*m2^4*MBhat^3*Ycut^5 - 22176*m2^5*MBhat^3*Ycut^5 + 
          40740*MBhat^4*Ycut^5 - 79320*m2*MBhat^4*Ycut^5 + 
          73680*m2^2*MBhat^4*Ycut^5 - 6420*m2^3*MBhat^4*Ycut^5 - 
          4725*m2^4*MBhat^4*Ycut^5 - 40975*Ycut^6 + 203160*m2*Ycut^6 + 
          629700*m2^2*Ycut^6 - 137660*m2^3*Ycut^6 - 9350*m2^4*Ycut^6 + 
          1204*m2^5*Ycut^6 + 175*m2^6*Ycut^6 + 213248*MBhat*Ycut^6 - 
          718476*m2*MBhat*Ycut^6 - 542640*m2^2*MBhat*Ycut^6 + 
          263640*m2^3*MBhat*Ycut^6 - 23760*m2^4*MBhat*Ycut^6 - 
          7084*m2^5*MBhat*Ycut^6 + 1568*m2^6*MBhat*Ycut^6 - 
          143761*MBhat^2*Ycut^6 + 568590*m2*MBhat^2*Ycut^6 - 
          129570*m2^2*MBhat^2*Ycut^6 + 25900*m2^3*MBhat^2*Ycut^6 + 
          28245*m2^4*MBhat^2*Ycut^6 - 31164*m2^5*MBhat^2*Ycut^6 + 
          6370*m2^6*MBhat^2*Ycut^6 - 14994*MBhat^3*Ycut^6 - 
          180120*m2*MBhat^3*Ycut^6 + 180000*m2^2*MBhat^3*Ycut^6 - 
          33480*m2^3*MBhat^3*Ycut^6 - 24430*m2^4*MBhat^3*Ycut^6 + 
          7392*m2^5*MBhat^3*Ycut^6 + 14700*MBhat^4*Ycut^6 + 
          25080*m2*MBhat^4*Ycut^6 - 34590*m2^2*MBhat^4*Ycut^6 + 
          2100*m2^3*MBhat^4*Ycut^6 + 1575*m2^4*MBhat^4*Ycut^6 - 5735*Ycut^7 - 
          18600*m2*Ycut^7 - 125520*m2^2*Ycut^7 - 5980*m2^3*Ycut^7 + 
          5750*m2^4*Ycut^7 - 172*m2^5*Ycut^7 - 25*m2^6*Ycut^7 - 
          102464*MBhat*Ycut^7 + 181668*m2*MBhat*Ycut^7 + 160080*m2^2*MBhat*
           Ycut^7 - 62760*m2^3*MBhat*Ycut^7 + 6960*m2^4*MBhat*Ycut^7 + 
          1252*m2^5*MBhat*Ycut^7 - 224*m2^6*MBhat*Ycut^7 - 
          4313*MBhat^2*Ycut^7 - 16770*m2*MBhat^2*Ycut^7 - 
          23970*m2^2*MBhat^2*Ycut^7 - 25660*m2^3*MBhat^2*Ycut^7 + 
          4125*m2^4*MBhat^2*Ycut^7 + 5028*m2^5*MBhat^2*Ycut^7 - 
          910*m2^6*MBhat^2*Ycut^7 + 107502*MBhat^3*Ycut^7 - 
          41640*m2*MBhat^3*Ycut^7 - 33840*m2^2*MBhat^3*Ycut^7 + 
          11280*m2^3*MBhat^3*Ycut^7 + 4090*m2^4*MBhat^3*Ycut^7 - 
          1056*m2^5*MBhat^3*Ycut^7 - 33300*MBhat^4*Ycut^7 + 
          120*m2*MBhat^4*Ycut^7 + 8550*m2^2*MBhat^4*Ycut^7 - 
          300*m2^3*MBhat^4*Ycut^7 - 225*m2^4*MBhat^4*Ycut^7 + 22080*Ycut^8 - 
          17400*m2*Ycut^8 - 2325*m2^2*Ycut^8 + 8820*m2^3*Ycut^8 - 
          1110*m2^4*Ycut^8 + 66240*MBhat*Ycut^8 - 59580*m2*MBhat*Ycut^8 - 
          13200*m2^2*MBhat*Ycut^8 + 9240*m2^3*MBhat*Ycut^8 - 
          960*m2^4*MBhat*Ycut^8 - 60*m2^5*MBhat*Ycut^8 + 
          2259*MBhat^2*Ycut^8 - 44400*m2*MBhat^2*Ycut^8 + 
          15180*m2^2*MBhat^2*Ycut^8 + 11040*m2^3*MBhat^2*Ycut^8 - 
          3390*m2^4*MBhat^2*Ycut^8 - 144*m2^5*MBhat^2*Ycut^8 - 
          73410*MBhat^3*Ycut^8 + 45240*m2*MBhat^3*Ycut^8 - 
          720*m2^2*MBhat^3*Ycut^8 - 1680*m2^3*MBhat^3*Ycut^8 - 
          150*m2^4*MBhat^3*Ycut^8 + 20085*MBhat^4*Ycut^8 - 
          2400*m2*MBhat^4*Ycut^8 - 420*m2^2*MBhat^4*Ycut^8 - 22000*Ycut^9 + 
          13000*m2*Ycut^9 + 4195*m2^2*Ycut^9 - 1260*m2^3*Ycut^9 - 
          30*m2^4*Ycut^9 - 34080*MBhat*Ycut^9 + 25020*m2*MBhat*Ycut^9 - 
          2080*m2^2*MBhat*Ycut^9 - 400*m2^3*MBhat*Ycut^9 + 
          11675*MBhat^2*Ycut^9 + 12240*m2*MBhat^2*Ycut^9 - 
          2100*m2^2*MBhat^2*Ycut^9 - 1920*m2^3*MBhat^2*Ycut^9 + 
          450*m2^4*MBhat^2*Ycut^9 + 24090*MBhat^3*Ycut^9 - 
          12520*m2*MBhat^3*Ycut^9 + 1680*m2^2*MBhat^3*Ycut^9 - 
          5235*MBhat^4*Ycut^9 + 480*m2*MBhat^4*Ycut^9 - 180*m2^2*MBhat^4*
           Ycut^9 + 13408*Ycut^10 - 5080*m2*Ycut^10 - 625*m2^2*Ycut^10 + 
          9312*MBhat*Ycut^10 - 5604*m2*MBhat*Ycut^10 + 160*m2^2*MBhat*
           Ycut^10 - 80*m2^3*MBhat*Ycut^10 - 6665*MBhat^2*Ycut^10 - 
          1680*m2*MBhat^2*Ycut^10 - 4086*MBhat^3*Ycut^10 + 
          1240*m2*MBhat^3*Ycut^10 - 240*m2^2*MBhat^3*Ycut^10 + 
          225*MBhat^4*Ycut^10 - 4896*Ycut^11 + 840*m2*Ycut^11 + 
          135*m2^2*Ycut^11 - 864*MBhat*Ycut^11 + 588*m2*MBhat*Ycut^11 + 
          1311*MBhat^2*Ycut^11 + 240*m2*MBhat^2*Ycut^11 + 
          462*MBhat^3*Ycut^11 + 105*MBhat^4*Ycut^11 + 968*Ycut^12 - 
          48*MBhat*Ycut^12 - 84*m2*MBhat*Ycut^12 - 112*MBhat^2*Ycut^12 - 
          66*MBhat^3*Ycut^12 - 80*Ycut^13 + 16*MBhat^2*Ycut^13))/
        (45*(-1 + Ycut)^7) + (8*(24 - 40*m2 - 715*m2^2 - 280*m2^3 + 45*m2^4 - 
          96*MBhat + 192*m2*MBhat + 1120*m2^2*MBhat + 144*MBhat^2 - 
          288*m2*MBhat^2 - 444*m2^2*MBhat^2 + 98*m2^3*MBhat^2 - 96*MBhat^3 + 
          160*m2*MBhat^3 + 24*MBhat^4 - 24*m2*MBhat^4 + 27*m2^2*MBhat^4)*
         Log[m2])/3 - (8*(24 - 40*m2 - 715*m2^2 - 280*m2^3 + 45*m2^4 - 
          96*MBhat + 192*m2*MBhat + 1120*m2^2*MBhat + 144*MBhat^2 - 
          288*m2*MBhat^2 - 444*m2^2*MBhat^2 + 98*m2^3*MBhat^2 - 96*MBhat^3 + 
          160*m2*MBhat^3 + 24*MBhat^4 - 24*m2*MBhat^4 + 27*m2^2*MBhat^4)*
         Log[1 - Ycut])/3) + 
     c[SL]*(-1/30*(Ycut^3*(-160*MBhat^2 + 1600*m2^2*MBhat^2 - 
           3200*m2^3*MBhat^2 + 2400*m2^4*MBhat^2 - 640*m2^5*MBhat^2 + 
           100*MBhat^3 + 400*m2*MBhat^3 - 1800*m2^2*MBhat^3 + 
           2000*m2^3*MBhat^3 - 700*m2^4*MBhat^3 - 160*m2*MBhat^4 + 
           320*m2^2*MBhat^4 - 160*m2^3*MBhat^4 + 160*MBhat*Ycut - 
           1600*m2^2*MBhat*Ycut + 3200*m2^3*MBhat*Ycut - 2400*m2^4*MBhat*
            Ycut + 640*m2^5*MBhat*Ycut + 1070*MBhat^2*Ycut - 
           550*m2*MBhat^2*Ycut - 6100*m2^2*MBhat^2*Ycut + 10900*m2^3*MBhat^2*
            Ycut - 6650*m2^4*MBhat^2*Ycut + 1330*m2^5*MBhat^2*Ycut - 
           700*MBhat^3*Ycut - 2160*m2*MBhat^3*Ycut + 8120*m2^2*MBhat^3*Ycut - 
           6960*m2^3*MBhat^3*Ycut + 1700*m2^4*MBhat^3*Ycut - 
           10*MBhat^4*Ycut + 880*m2*MBhat^4*Ycut - 1290*m2^2*MBhat^4*Ycut + 
           420*m2^3*MBhat^4*Ycut - 48*Ycut^2 + 480*m2^2*Ycut^2 - 
           960*m2^3*Ycut^2 + 720*m2^4*Ycut^2 - 192*m2^5*Ycut^2 - 
           1140*MBhat*Ycut^2 + 340*m2*MBhat*Ycut^2 + 6520*m2^2*MBhat*Ycut^2 - 
           10200*m2^3*MBhat*Ycut^2 + 5180*m2^4*MBhat*Ycut^2 - 
           700*m2^5*MBhat*Ycut^2 - 3010*MBhat^2*Ycut^2 + 2890*m2*MBhat^2*
            Ycut^2 + 8780*m2^2*MBhat^2*Ycut^2 - 15660*m2^3*MBhat^2*Ycut^2 + 
           8470*m2^4*MBhat^2*Ycut^2 - 1470*m2^5*MBhat^2*Ycut^2 + 
           2110*MBhat^3*Ycut^2 + 4880*m2*MBhat^3*Ycut^2 - 15040*m2^2*MBhat^3*
            Ycut^2 + 9800*m2^3*MBhat^3*Ycut^2 - 1750*m2^4*MBhat^3*Ycut^2 + 
           70*MBhat^4*Ycut^2 - 2000*m2*MBhat^4*Ycut^2 + 1990*m2^2*MBhat^4*
            Ycut^2 - 380*m2^3*MBhat^4*Ycut^2 + 351*Ycut^3 - 80*m2*Ycut^3 - 
           1950*m2^2*Ycut^3 + 2820*m2^3*Ycut^3 - 1225*m2^4*Ycut^3 + 
           84*m2^5*Ycut^3 + 3500*MBhat*Ycut^3 - 1740*m2*MBhat*Ycut^3 - 
           10440*m2^2*MBhat*Ycut^3 + 12520*m2^3*MBhat*Ycut^3 - 
           4260*m2^4*MBhat*Ycut^3 + 420*m2^5*MBhat*Ycut^3 + 
           4547*MBhat^2*Ycut^3 - 6270*m2*MBhat^2*Ycut^3 - 5760*m2^2*MBhat^2*
            Ycut^3 + 12880*m2^3*MBhat^2*Ycut^3 - 6335*m2^4*MBhat^2*Ycut^3 + 
           938*m2^5*MBhat^2*Ycut^3 - 3570*MBhat^3*Ycut^3 - 
           6000*m2*MBhat^3*Ycut^3 + 14720*m2^2*MBhat^3*Ycut^3 - 
           7160*m2^3*MBhat^3*Ycut^3 + 1050*m2^4*MBhat^3*Ycut^3 - 
           210*MBhat^4*Ycut^3 + 2400*m2*MBhat^4*Ycut^3 - 1400*m2^2*MBhat^4*
            Ycut^3 + 140*m2^3*MBhat^4*Ycut^3 - 1113*Ycut^4 + 400*m2*Ycut^4 + 
           3090*m2^2*Ycut^4 - 2940*m2^3*Ycut^4 + 575*m2^4*Ycut^4 - 
           12*m2^5*Ycut^4 - 6020*MBhat*Ycut^4 + 3620*m2*MBhat*Ycut^4 + 
           8320*m2^2*MBhat*Ycut^4 - 7560*m2^3*MBhat*Ycut^4 + 
           1780*m2^4*MBhat*Ycut^4 - 140*m2^5*MBhat*Ycut^4 - 
           3829*MBhat^2*Ycut^4 + 7250*m2*MBhat^2*Ycut^4 + 1600*m2^2*MBhat^2*
            Ycut^4 - 6640*m2^3*MBhat^2*Ycut^4 + 2585*m2^4*MBhat^2*Ycut^4 - 
           326*m2^5*MBhat^2*Ycut^4 + 3710*MBhat^3*Ycut^4 + 
           4400*m2*MBhat^3*Ycut^4 - 8120*m2^2*MBhat^3*Ycut^4 + 
           2760*m2^3*MBhat^3*Ycut^4 - 350*m2^4*MBhat^3*Ycut^4 + 
           350*MBhat^4*Ycut^4 - 1600*m2*MBhat^4*Ycut^4 + 360*m2^2*MBhat^4*
            Ycut^4 - 20*m2^3*MBhat^4*Ycut^4 + 1995*Ycut^5 - 800*m2*Ycut^5 - 
           2400*m2^2*Ycut^5 + 1260*m2^3*Ycut^5 - 55*m2^4*Ycut^5 + 
           6300*MBhat*Ycut^5 - 3900*m2*MBhat*Ycut^5 - 3520*m2^2*MBhat*
            Ycut^5 + 2360*m2^3*MBhat*Ycut^5 - 300*m2^4*MBhat*Ycut^5 + 
           20*m2^5*MBhat*Ycut^5 + 1547*MBhat^2*Ycut^5 - 4850*m2*MBhat^2*
            Ycut^5 - 140*m2^2*MBhat^2*Ycut^5 + 1980*m2^3*MBhat^2*Ycut^5 - 
           495*m2^4*MBhat^2*Ycut^5 + 48*m2^5*MBhat^2*Ycut^5 - 
           2450*MBhat^3*Ycut^5 - 2000*m2*MBhat^3*Ycut^5 + 2440*m2^2*MBhat^3*
            Ycut^5 - 440*m2^3*MBhat^3*Ycut^5 + 50*m2^4*MBhat^3*Ycut^5 - 
           350*MBhat^4*Ycut^5 + 560*m2*MBhat^4*Ycut^5 + 50*m2^2*MBhat^4*
            Ycut^5 - 2205*Ycut^6 + 800*m2*Ycut^6 + 960*m2^2*Ycut^6 - 
           180*m2^3*Ycut^6 - 15*m2^4*Ycut^6 - 4060*MBhat*Ycut^6 + 
           2300*m2*MBhat*Ycut^6 + 840*m2^2*MBhat*Ycut^6 - 
           320*m2^3*MBhat*Ycut^6 + 35*MBhat^2*Ycut^6 + 1950*m2*MBhat^2*
            Ycut^6 + 20*m2^2*MBhat^2*Ycut^6 - 260*m2^3*MBhat^2*Ycut^6 + 
           25*m2^4*MBhat^2*Ycut^6 + 1050*MBhat^3*Ycut^6 + 
           560*m2*MBhat^3*Ycut^6 - 320*m2^2*MBhat^3*Ycut^6 + 
           210*MBhat^4*Ycut^6 - 80*m2*MBhat^4*Ycut^6 - 30*m2^2*MBhat^4*
            Ycut^6 + 1533*Ycut^7 - 400*m2*Ycut^7 - 210*m2^2*Ycut^7 + 
           1540*MBhat*Ycut^7 - 740*m2*MBhat*Ycut^7 - 120*m2^2*MBhat*Ycut^7 - 
           295*MBhat^2*Ycut^7 - 490*m2*MBhat^2*Ycut^7 - 310*MBhat^3*Ycut^7 - 
           80*m2*MBhat^3*Ycut^7 - 70*MBhat^4*Ycut^7 - 651*Ycut^8 + 
           80*m2*Ycut^8 + 30*m2^2*Ycut^8 - 300*MBhat*Ycut^8 + 
           140*m2*MBhat*Ycut^8 + 113*MBhat^2*Ycut^8 + 70*m2*MBhat^2*Ycut^8 + 
           70*MBhat^3*Ycut^8 + 10*MBhat^4*Ycut^8 + 153*Ycut^9 + 
           20*MBhat*Ycut^9 - 20*m2*MBhat*Ycut^9 - 21*MBhat^2*Ycut^9 - 
           10*MBhat^3*Ycut^9 - 15*Ycut^10 + 3*MBhat^2*Ycut^10)*c[T])/
         (-1 + Ycut)^7 + c[SR]*
        (-1/30*(Sqrt[m2]*(-4197 - 15825*m2 + 11100*m2^2 + 9300*m2^3 - 
             375*m2^4 - 3*m2^5 + 13568*MBhat + 21600*m2*MBhat - 
             32000*m2^2*MBhat - 3200*m2^3*MBhat + 32*m2^5*MBhat - 
             15825*MBhat^2 - 2995*m2*MBhat^2 + 19680*m2^2*MBhat^2 - 
             600*m2^3*MBhat^2 - 335*m2^4*MBhat^2 + 75*m2^5*MBhat^2 + 
             7920*MBhat^3 - 5120*m2*MBhat^3 - 2880*m2^2*MBhat^3 + 
             80*m2^4*MBhat^3 - 1430*MBhat^4 + 1530*m2*MBhat^4 - 
             90*m2^2*MBhat^4 - 10*m2^3*MBhat^4 + 26262*Ycut + 
             107370*m2*Ycut - 49680*m2^2*Ycut - 53280*m2^3*Ycut + 
             2070*m2^4*Ycut + 18*m2^5*Ycut - 85248*MBhat*Ycut - 
             158400*m2*MBhat*Ycut + 172800*m2^2*MBhat*Ycut + 
             19200*m2^3*MBhat*Ycut - 192*m2^5*MBhat*Ycut + 99990*MBhat^2*
              Ycut + 41130*m2*MBhat^2*Ycut - 112140*m2^2*MBhat^2*Ycut + 
             3060*m2^3*MBhat^2*Ycut + 2010*m2^4*MBhat^2*Ycut - 
             450*m2^5*MBhat^2*Ycut - 50400*MBhat^3*Ycut + 23040*m2*MBhat^3*
              Ycut + 17280*m2^2*MBhat^3*Ycut - 480*m2^4*MBhat^3*Ycut + 
             9180*MBhat^4*Ycut - 8280*m2*MBhat^4*Ycut + 360*m2^2*MBhat^4*
              Ycut + 60*m2^3*MBhat^4*Ycut - 68895*Ycut^2 - 305685*m2*Ycut^2 + 
             73440*m2^2*Ycut^2 + 125640*m2^3*Ycut^2 - 4635*m2^4*Ycut^2 - 
             45*m2^5*Ycut^2 + 224640*MBhat*Ycut^2 + 482400*m2*MBhat*Ycut^2 - 
             374400*m2^2*MBhat*Ycut^2 - 48000*m2^3*MBhat*Ycut^2 + 
             480*m2^5*MBhat*Ycut^2 - 265095*MBhat^2*Ycut^2 - 
             172305*m2*MBhat^2*Ycut^2 + 262530*m2^2*MBhat^2*Ycut^2 - 
             6030*m2^3*MBhat^2*Ycut^2 - 5025*m2^4*MBhat^2*Ycut^2 + 
             1125*m2^5*MBhat^2*Ycut^2 + 134640*MBhat^3*Ycut^2 - 
             34560*m2*MBhat^3*Ycut^2 - 43200*m2^2*MBhat^3*Ycut^2 + 
             1200*m2^4*MBhat^3*Ycut^2 - 24750*MBhat^4*Ycut^2 + 
             18000*m2*MBhat^4*Ycut^2 - 360*m2^2*MBhat^4*Ycut^2 - 
             150*m2^3*MBhat^4*Ycut^2 + 97260*Ycut^3 + 469680*m2*Ycut^3 - 
             13320*m2^2*Ycut^3 - 154920*m2^3*Ycut^3 + 5280*m2^4*Ycut^3 + 
             60*m2^5*Ycut^3 - 318720*MBhat*Ycut^3 - 787200*m2*MBhat*Ycut^3 + 
             403200*m2^2*MBhat*Ycut^3 + 64000*m2^3*MBhat*Ycut^3 - 
             640*m2^5*MBhat*Ycut^3 + 378380*MBhat^2*Ycut^3 + 
             345860*m2*MBhat^2*Ycut^3 - 319620*m2^2*MBhat^2*Ycut^3 + 
             4060*m2^3*MBhat^2*Ycut^3 + 7220*m2^4*MBhat^2*Ycut^3 - 
             1500*m2^5*MBhat^2*Ycut^3 - 193620*MBhat^3*Ycut^3 + 
             7580*m2*MBhat^3*Ycut^3 + 56900*m2^2*MBhat^3*Ycut^3 + 
             500*m2^3*MBhat^3*Ycut^3 - 1600*m2^4*MBhat^3*Ycut^3 + 
             35920*MBhat^4*Ycut^3 - 19500*m2*MBhat^4*Ycut^3 - 
             340*m2^2*MBhat^4*Ycut^3 + 200*m2^3*MBhat^4*Ycut^3 - 
             78345*Ycut^4 - 414360*m2*Ycut^4 - 74610*m2^2*Ycut^4 + 
             103590*m2^3*Ycut^4 - 3060*m2^4*Ycut^4 - 45*m2^5*Ycut^4 + 
             258520*MBhat*Ycut^4 + 734080*m2*MBhat*Ycut^4 - 207120*m2^2*MBhat*
              Ycut^4 - 46720*m2^3*MBhat*Ycut^4 - 520*m2^4*MBhat*Ycut^4 + 
             480*m2^5*MBhat*Ycut^4 - 307965*MBhat^2*Ycut^4 - 
             376800*m2*MBhat^2*Ycut^4 + 209710*m2^2*MBhat^2*Ycut^4 + 
             1000*m2^3*MBhat^2*Ycut^4 - 5870*m2^4*MBhat^2*Ycut^4 + 
             1125*m2^5*MBhat^2*Ycut^4 + 158280*MBhat^3*Ycut^4 + 
             33720*m2*MBhat^3*Ycut^4 - 41800*m2^2*MBhat^3*Ycut^4 - 
             920*m2^3*MBhat^3*Ycut^4 + 1200*m2^4*MBhat^3*Ycut^4 - 
             29550*MBhat^4*Ycut^4 + 9920*m2*MBhat^4*Ycut^4 + 
             1110*m2^2*MBhat^4*Ycut^4 - 150*m2^3*MBhat^4*Ycut^4 + 
             34494*Ycut^5 + 203100*m2*Ycut^5 + 80820*m2^2*Ycut^5 - 
             34260*m2^3*Ycut^5 + 840*m2^4*Ycut^5 + 18*m2^5*Ycut^5 - 
             116226*MBhat*Ycut^5 - 378240*m2*MBhat*Ycut^5 + 25740*m2^2*MBhat*
              Ycut^5 + 17520*m2^3*MBhat*Ycut^5 + 390*m2^4*MBhat*Ycut^5 - 
             192*m2^5*MBhat*Ycut^5 + 137010*MBhat^2*Ycut^5 + 
             222660*m2*MBhat^2*Ycut^5 - 66900*m2^2*MBhat^2*Ycut^5 - 
             2640*m2^3*MBhat^2*Ycut^5 + 2640*m2^4*MBhat^2*Ycut^5 - 
             450*m2^5*MBhat^2*Ycut^5 - 69420*MBhat^3*Ycut^5 - 
             38940*m2*MBhat^3*Ycut^5 + 16920*m2^2*MBhat^3*Ycut^5 + 
             480*m2^3*MBhat^3*Ycut^5 - 480*m2^4*MBhat^3*Ycut^5 + 
             12780*MBhat^4*Ycut^5 - 600*m2*MBhat^4*Ycut^5 - 1140*m2^2*MBhat^4*
              Ycut^5 + 60*m2^3*MBhat^4*Ycut^5 - 6399*Ycut^6 - 
             46840*m2*Ycut^6 - 30540*m2^2*Ycut^6 + 3480*m2^3*Ycut^6 + 
             40*m2^4*Ycut^6 - 3*m2^5*Ycut^6 + 25716*MBhat*Ycut^6 + 
             88320*m2*MBhat*Ycut^6 + 16040*m2^2*MBhat*Ycut^6 - 
             3040*m2^3*MBhat*Ycut^6 - 60*m2^4*MBhat*Ycut^6 + 
             32*m2^5*MBhat*Ycut^6 - 27715*MBhat^2*Ycut^6 - 61120*m2*MBhat^2*
              Ycut^6 + 5310*m2^2*MBhat^2*Ycut^6 + 1470*m2^3*MBhat^2*Ycut^6 - 
             580*m2^4*MBhat^2*Ycut^6 + 75*m2^5*MBhat^2*Ycut^6 + 
             12000*MBhat^3*Ycut^6 + 16560*m2*MBhat^3*Ycut^6 - 
             3560*m2^2*MBhat^3*Ycut^6 - 40*m2^3*MBhat^3*Ycut^6 + 
             80*m2^4*MBhat^3*Ycut^6 - 1730*MBhat^4*Ycut^6 - 
             1680*m2*MBhat^4*Ycut^6 + 580*m2^2*MBhat^4*Ycut^6 - 
             10*m2^3*MBhat^4*Ycut^6 - 780*Ycut^7 + 3000*m2*Ycut^7 + 
             1980*m2^2*Ycut^7 + 540*m2^3*Ycut^7 - 60*m2^4*Ycut^7 - 
             2990*MBhat*Ycut^7 - 1120*m2*MBhat*Ycut^7 - 3960*m2^2*MBhat*
              Ycut^7 + 240*m2^3*MBhat*Ycut^7 - 10*m2^4*MBhat*Ycut^7 + 
             1920*MBhat^2*Ycut^7 + 2100*m2*MBhat^2*Ycut^7 + 1700*m2^2*MBhat^2*
              Ycut^7 - 320*m2^3*MBhat^2*Ycut^7 + 40*m2^4*MBhat^2*Ycut^7 + 
             660*MBhat^3*Ycut^7 - 1980*m2*MBhat^3*Ycut^7 + 340*m2^2*MBhat^3*
              Ycut^7 - 20*m2^3*MBhat^3*Ycut^7 - 600*MBhat^4*Ycut^7 + 
             700*m2*MBhat^4*Ycut^7 - 120*m2^2*MBhat^4*Ycut^7 + 990*Ycut^8 - 
             855*m2*Ycut^8 + 810*m2^2*Ycut^8 - 90*m2^3*Ycut^8 + 
             720*MBhat*Ycut^8 - 960*m2*MBhat*Ycut^8 - 360*m2^2*MBhat*Ycut^8 - 
             930*MBhat^2*Ycut^8 + 1395*m2*MBhat^2*Ycut^8 - 270*m2^2*MBhat^2*
              Ycut^8 + 120*MBhat^3*Ycut^8 - 360*m2*MBhat^3*Ycut^8 + 
             180*MBhat^4*Ycut^8 - 90*m2*MBhat^4*Ycut^8 - 480*Ycut^9 + 
             370*m2*Ycut^9 + 130*MBhat*Ycut^9 - 480*m2*MBhat*Ycut^9 + 
             60*m2^2*MBhat*Ycut^9 + 140*MBhat^2*Ycut^9 + 90*m2*MBhat^2*
              Ycut^9 - 180*MBhat^3*Ycut^9 + 60*m2*MBhat^3*Ycut^9 + 
             78*Ycut^10 + 45*m2*Ycut^10 - 92*MBhat*Ycut^10 + 
             90*MBhat^2*Ycut^10 - 15*m2*MBhat^2*Ycut^10 + 12*Ycut^11 - 
             18*MBhat*Ycut^11))/(-1 + Ycut)^6 - 2*Sqrt[m2]*
          (-18 - 207*m2 - 282*m2^2 - 42*m2^3 + 3*m2^4 + 64*MBhat + 
           480*m2*MBhat + 320*m2^2*MBhat - 84*MBhat^2 - 386*m2*MBhat^2 - 
           99*m2^2*MBhat^2 + 9*m2^3*MBhat^2 + 48*MBhat^3 + 128*m2*MBhat^3 - 
           10*MBhat^4 - 15*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2] + 
         2*Sqrt[m2]*(-18 - 207*m2 - 282*m2^2 - 42*m2^3 + 3*m2^4 + 64*MBhat + 
           480*m2*MBhat + 320*m2^2*MBhat - 84*MBhat^2 - 386*m2*MBhat^2 - 
           99*m2^2*MBhat^2 + 9*m2^3*MBhat^2 + 48*MBhat^3 + 128*m2*MBhat^3 - 
           10*MBhat^4 - 15*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[1 - Ycut])) + 
     c[VL]*((-2238 + 8840*m2 + 10550*m2^2 - 18400*m2^3 + 1150*m2^4 + 
         88*m2^5 + 10*m2^6 + 7856*MBhat - 26880*m2*MBhat + 7600*m2^2*MBhat + 
         12800*m2^3*MBhat - 1200*m2^4*MBhat - 256*m2^5*MBhat + 
         80*m2^6*MBhat - 9825*MBhat^2 + 26595*m2*MBhat^2 - 
         19320*m2^2*MBhat^2 + 1760*m2^3*MBhat^2 + 1665*m2^4*MBhat^2 - 
         1155*m2^5*MBhat^2 + 280*m2^6*MBhat^2 + 5424*MBhat^3 - 
         10640*m2*MBhat^3 + 6720*m2^2*MBhat^3 - 960*m2^3*MBhat^3 - 
         880*m2^4*MBhat^3 + 336*m2^5*MBhat^3 - 1155*MBhat^4 + 
         1320*m2*MBhat^4 - 360*m2^2*MBhat^4 + 120*m2^3*MBhat^4 + 
         75*m2^4*MBhat^4 + 16386*Ycut - 61400*m2*Ycut - 89570*m2^2*Ycut + 
         122080*m2^3*Ycut - 6970*m2^4*Ycut - 616*m2^5*Ycut - 70*m2^6*Ycut - 
         57872*MBhat*Ycut + 188160*m2*MBhat*Ycut - 29200*m2^2*MBhat*Ycut - 
         89600*m2^3*MBhat*Ycut + 8400*m2^4*MBhat*Ycut + 
         1792*m2^5*MBhat*Ycut - 560*m2^6*MBhat*Ycut + 73095*MBhat^2*Ycut - 
         187605*m2*MBhat^2*Ycut + 126060*m2^2*MBhat^2*Ycut - 
         10220*m2^3*MBhat^2*Ycut - 11655*m2^4*MBhat^2*Ycut + 
         8085*m2^5*MBhat^2*Ycut - 1960*m2^6*MBhat^2*Ycut - 
         40848*MBhat^3*Ycut + 75440*m2*MBhat^3*Ycut - 47040*m2^2*MBhat^3*
          Ycut + 6720*m2^3*MBhat^3*Ycut + 6160*m2^4*MBhat^3*Ycut - 
         2352*m2^5*MBhat^3*Ycut + 8805*MBhat^4*Ycut - 9240*m2*MBhat^4*Ycut + 
         3060*m2^2*MBhat^4*Ycut - 840*m2^3*MBhat^4*Ycut - 
         525*m2^4*MBhat^4*Ycut - 51678*Ycut^2 + 182520*m2*Ycut^2 + 
         323730*m2^2*Ycut^2 - 342720*m2^3*Ycut^2 + 17130*m2^4*Ycut^2 + 
         1848*m2^5*Ycut^2 + 210*m2^6*Ycut^2 + 183696*MBhat*Ycut^2 - 
         564480*m2*MBhat*Ycut^2 + 3600*m2^2*MBhat*Ycut^2 + 
         268800*m2^3*MBhat*Ycut^2 - 25200*m2^4*MBhat*Ycut^2 - 
         5376*m2^5*MBhat*Ycut^2 + 1680*m2^6*MBhat*Ycut^2 - 
         234405*MBhat^2*Ycut^2 + 567855*m2*MBhat^2*Ycut^2 - 
         346050*m2^2*MBhat^2*Ycut^2 + 23310*m2^3*MBhat^2*Ycut^2 + 
         34965*m2^4*MBhat^2*Ycut^2 - 24255*m2^5*MBhat^2*Ycut^2 + 
         5880*m2^6*MBhat^2*Ycut^2 + 132624*MBhat^3*Ycut^2 - 
         229680*m2*MBhat^3*Ycut^2 + 141120*m2^2*MBhat^3*Ycut^2 - 
         20160*m2^3*MBhat^3*Ycut^2 - 18480*m2^4*MBhat^3*Ycut^2 + 
         7056*m2^5*MBhat^3*Ycut^2 - 28935*MBhat^4*Ycut^2 + 
         27720*m2*MBhat^4*Ycut^2 - 11070*m2^2*MBhat^4*Ycut^2 + 
         2520*m2^3*MBhat^4*Ycut^2 + 1575*m2^4*MBhat^4*Ycut^2 + 91170*Ycut^3 - 
         300840*m2*Ycut^3 - 649590*m2^2*Ycut^3 + 524160*m2^3*Ycut^3 - 
         20990*m2^4*Ycut^3 - 3080*m2^5*Ycut^3 - 350*m2^6*Ycut^3 - 
         326320*MBhat*Ycut^3 + 940800*m2*MBhat*Ycut^3 + 
         162000*m2^2*MBhat*Ycut^3 - 448000*m2^3*MBhat*Ycut^3 + 
         42000*m2^4*MBhat*Ycut^3 + 8960*m2^5*MBhat*Ycut^3 - 
         2800*m2^6*MBhat*Ycut^3 + 419955*MBhat^2*Ycut^3 - 
         954585*m2*MBhat^2*Ycut^3 + 510570*m2^2*MBhat^2*Ycut^3 - 
         20310*m2^3*MBhat^2*Ycut^3 - 63075*m2^4*MBhat^2*Ycut^3 + 
         42345*m2^5*MBhat^2*Ycut^3 - 9800*m2^6*MBhat^2*Ycut^3 - 
         239820*MBhat^3*Ycut^3 + 387360*m2*MBhat^3*Ycut^3 - 
         232920*m2^2*MBhat^3*Ycut^3 + 30000*m2^3*MBhat^3*Ycut^3 + 
         32900*m2^4*MBhat^3*Ycut^3 - 11760*m2^5*MBhat^3*Ycut^3 + 
         52785*MBhat^4*Ycut^3 - 45720*m2*MBhat^4*Ycut^3 + 
         21750*m2^2*MBhat^4*Ycut^3 - 3720*m2^3*MBhat^4*Ycut^3 - 
         2625*m2^4*MBhat^4*Ycut^3 - 97470*Ycut^4 + 296640*m2*Ycut^4 + 
         787140*m2^2*Ycut^4 - 465360*m2^3*Ycut^4 + 11540*m2^4*Ycut^4 + 
         3080*m2^5*Ycut^4 + 350*m2^6*Ycut^4 + 352240*MBhat*Ycut^4 - 
         942240*m2*MBhat*Ycut^4 - 370080*m2^2*MBhat*Ycut^4 + 
         443680*m2^3*MBhat*Ycut^4 - 36960*m2^4*MBhat*Ycut^4 - 
         10880*m2^5*MBhat*Ycut^4 + 2800*m2^6*MBhat*Ycut^4 - 
         453210*MBhat^2*Ycut^4 + 958920*m2*MBhat^2*Ycut^4 - 
         425790*m2^2*MBhat^2*Ycut^4 - 2430*m2^3*MBhat^2*Ycut^4 + 
         68775*m2^4*MBhat^2*Ycut^4 - 44415*m2^5*MBhat^2*Ycut^4 + 
         9800*m2^6*MBhat^2*Ycut^4 + 257460*MBhat^3*Ycut^4 - 
         385440*m2*MBhat^3*Ycut^4 + 226200*m2^2*MBhat^3*Ycut^4 - 
         23040*m2^3*MBhat^3*Ycut^4 - 35900*m2^4*MBhat^3*Ycut^4 + 
         11760*m2^5*MBhat^3*Ycut^4 - 56400*MBhat^4*Ycut^4 + 
         43560*m2*MBhat^4*Ycut^4 - 25320*m2^2*MBhat^4*Ycut^4 + 
         2940*m2^3*MBhat^4*Ycut^4 + 2625*m2^4*MBhat^4*Ycut^4 + 63330*Ycut^5 - 
         174240*m2*Ycut^5 - 582900*m2^2*Ycut^5 + 233520*m2^3*Ycut^5 - 
         900*m2^4*Ycut^5 - 1272*m2^5*Ycut^5 - 210*m2^6*Ycut^5 - 
         235512*MBhat*Ycut^5 + 572160*m2*MBhat*Ycut^5 + 
         384540*m2^2*MBhat*Ycut^5 - 258660*m2^3*MBhat*Ycut^5 + 
         16380*m2^4*MBhat*Ycut^5 + 7476*m2^5*MBhat*Ycut^5 - 
         1680*m2^6*MBhat*Ycut^5 + 292950*MBhat^2*Ycut^5 - 
         569400*m2*MBhat^2*Ycut^5 + 186510*m2^2*MBhat^2*Ycut^5 + 
         15870*m2^3*MBhat^2*Ycut^5 - 45105*m2^4*MBhat^2*Ycut^5 + 
         28665*m2^5*MBhat^2*Ycut^5 - 5880*m2^6*MBhat^2*Ycut^5 - 
         155478*MBhat^3*Ycut^5 + 215040*m2*MBhat^3*Ycut^5 - 
         126540*m2^2*MBhat^3*Ycut^5 + 8400*m2^3*MBhat^3*Ycut^5 + 
         23730*m2^4*MBhat^3*Ycut^5 - 7056*m2^5*MBhat^3*Ycut^5 + 
         31920*MBhat^4*Ycut^5 - 21720*m2*MBhat^4*Ycut^5 + 
         18060*m2^2*MBhat^4*Ycut^5 - 1380*m2^3*MBhat^4*Ycut^5 - 
         1575*m2^4*MBhat^4*Ycut^5 - 22465*Ycut^6 + 54480*m2*Ycut^6 + 
         251070*m2^2*Ycut^6 - 56780*m2^3*Ycut^6 - 1955*m2^4*Ycut^6 + 
         364*m2^5*Ycut^6 + 70*m2^6*Ycut^6 + 98504*MBhat*Ycut^6 - 
         204960*m2*MBhat*Ycut^6 - 205620*m2^2*MBhat*Ycut^6 + 
         82380*m2^3*MBhat*Ycut^6 - 3300*m2^4*MBhat*Ycut^6 - 
         3052*m2^5*MBhat*Ycut^6 + 560*m2^6*MBhat*Ycut^6 - 
         103531*MBhat^2*Ycut^6 + 177120*m2*MBhat^2*Ycut^6 - 
         25350*m2^2*MBhat^2*Ycut^6 - 11270*m2^3*MBhat^2*Ycut^6 + 
         17640*m2^4*MBhat^2*Ycut^6 - 10899*m2^5*MBhat^2*Ycut^6 + 
         1960*m2^6*MBhat^2*Ycut^6 + 33306*MBhat^3*Ycut^6 - 
         44640*m2*MBhat^3*Ycut^6 + 33780*m2^2*MBhat^3*Ycut^6 - 
         240*m2^3*MBhat^3*Ycut^6 - 9310*m2^4*MBhat^3*Ycut^6 + 
         2352*m2^5*MBhat^3*Ycut^6 - 2520*MBhat^4*Ycut^6 + 
         2040*m2*MBhat^4*Ycut^6 - 7890*m2^2*MBhat^4*Ycut^6 + 
         420*m2^3*MBhat^4*Ycut^6 + 525*m2^4*MBhat^4*Ycut^6 + 775*Ycut^7 - 
         3120*m2*Ycut^7 - 53610*m2^2*Ycut^7 + 2420*m2^3*Ycut^7 + 
         1325*m2^4*Ycut^7 - 52*m2^5*Ycut^7 - 10*m2^6*Ycut^7 - 
         30872*MBhat*Ycut^7 + 46080*m2*MBhat*Ycut^7 + 48600*m2^2*MBhat*
          Ycut^7 - 11160*m2^3*MBhat*Ycut^7 - 300*m2^4*MBhat*Ycut^7 + 
         676*m2^5*MBhat*Ycut^7 - 80*m2^6*MBhat*Ycut^7 + 
         16237*MBhat^2*Ycut^7 - 14880*m2*MBhat^2*Ycut^7 - 
         10770*m2^2*MBhat^2*Ycut^7 + 3710*m2^3*MBhat^2*Ycut^7 - 
         3240*m2^4*MBhat^2*Ycut^7 + 2133*m2^5*MBhat^2*Ycut^7 - 
         280*m2^6*MBhat^2*Ycut^7 + 19482*MBhat^3*Ycut^7 - 
         18720*m2*MBhat^3*Ycut^7 + 1440*m2^2*MBhat^3*Ycut^7 - 
         960*m2^3*MBhat^3*Ycut^7 + 1930*m2^4*MBhat^3*Ycut^7 - 
         336*m2^5*MBhat^3*Ycut^7 - 9240*MBhat^4*Ycut^7 + 
         3480*m2*MBhat^4*Ycut^7 + 2010*m2^2*MBhat^4*Ycut^7 - 
         60*m2^3*MBhat^4*Ycut^7 - 75*m2^4*MBhat^4*Ycut^7 + 4845*Ycut^8 - 
         5160*m2*Ycut^8 + 3030*m2^2*Ycut^8 + 1260*m2^3*Ycut^8 - 
         345*m2^4*Ycut^8 + 12120*MBhat*Ycut^8 - 12000*m2*MBhat*Ycut^8 + 
         120*m2^2*MBhat*Ycut^8 - 600*m2^3*MBhat*Ycut^8 + 
         180*m2^4*MBhat*Ycut^8 - 60*m2^5*MBhat*Ycut^8 - 4026*MBhat^2*Ycut^8 - 
         4515*m2*MBhat^2*Ycut^8 + 4830*m2^2*MBhat^2*Ycut^8 - 
         330*m2^3*MBhat^2*Ycut^8 - 45*m2^4*MBhat^2*Ycut^8 - 
         144*m2^5*MBhat^2*Ycut^8 - 16230*MBhat^3*Ycut^8 + 
         14640*m2*MBhat^3*Ycut^8 - 3600*m2^2*MBhat^3*Ycut^8 + 
         240*m2^3*MBhat^3*Ycut^8 - 150*m2^4*MBhat^3*Ycut^8 + 
         6315*MBhat^4*Ycut^8 - 1680*m2*MBhat^4*Ycut^8 - 
         240*m2^2*MBhat^4*Ycut^8 - 4315*Ycut^9 + 3160*m2*Ycut^9 + 
         70*m2^2*Ycut^9 - 180*m2^3*Ycut^9 + 15*m2^4*Ycut^9 - 
         4440*MBhat*Ycut^9 + 3840*m2*MBhat*Ycut^9 - 1780*m2^2*MBhat*Ycut^9 + 
         380*m2^3*MBhat*Ycut^9 + 4070*MBhat^2*Ycut^9 + 
         405*m2*MBhat^2*Ycut^9 - 690*m2^2*MBhat^2*Ycut^9 - 
         90*m2^3*MBhat^2*Ycut^9 + 75*m2^4*MBhat^2*Ycut^9 + 
         4590*MBhat^3*Ycut^9 - 3760*m2*MBhat^3*Ycut^9 + 
         900*m2^2*MBhat^3*Ycut^9 - 1725*MBhat^4*Ycut^9 + 
         240*m2*MBhat^4*Ycut^9 + 2203*Ycut^10 - 1000*m2*Ycut^10 + 
         80*m2^2*Ycut^10 + 456*MBhat*Ycut^10 - 480*m2*MBhat*Ycut^10 + 
         220*m2^2*MBhat*Ycut^10 - 20*m2^3*MBhat*Ycut^10 - 
         1460*MBhat^2*Ycut^10 + 105*m2*MBhat^2*Ycut^10 - 
         546*MBhat^3*Ycut^10 + 400*m2*MBhat^3*Ycut^10 - 
         60*m2^2*MBhat^3*Ycut^10 + 135*MBhat^4*Ycut^10 - 621*Ycut^11 + 
         120*m2*Ycut^11 + 168*MBhat*Ycut^11 + 156*MBhat^2*Ycut^11 - 
         15*m2*MBhat^2*Ycut^11 + 42*MBhat^3*Ycut^11 + 15*MBhat^4*Ycut^11 + 
         83*Ycut^12 - 24*MBhat*Ycut^12 - 7*MBhat^2*Ycut^12 - 
         6*MBhat^3*Ycut^12 - 5*Ycut^13 + MBhat^2*Ycut^13)/
        (45*(-1 + Ycut)^7) + (4*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 
          48*MBhat + 400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 
          153*m2^2*MBhat^2 + 35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 
          12*MBhat^4 + 9*m2^2*MBhat^4)*Log[m2])/3 - 
       (4*(12 + 8*m2 - 262*m2^2 - 112*m2^3 + 18*m2^4 - 48*MBhat + 
          400*m2^2*MBhat + 72*MBhat^2 - 24*m2*MBhat^2 - 153*m2^2*MBhat^2 + 
          35*m2^3*MBhat^2 - 48*MBhat^3 + 16*m2*MBhat^3 + 12*MBhat^4 + 
          9*m2^2*MBhat^4)*Log[1 - Ycut])/3 + 
       c[VR]*((Sqrt[m2]*(-3241 - 11365*m2 + 9260*m2^2 + 5860*m2^3 - 
            515*m2^4 + m2^5 + 10032*MBhat + 13200*m2*MBhat - 
            22080*m2^2*MBhat - 960*m2^3*MBhat - 240*m2^4*MBhat + 
            48*m2^5*MBhat - 10965*MBhat^2 + 725*m2*MBhat^2 + 
            10640*m2^2*MBhat^2 + 120*m2^3*MBhat^2 - 635*m2^4*MBhat^2 + 
            115*m2^5*MBhat^2 + 4960*MBhat^3 - 4480*m2*MBhat^3 - 
            640*m2^3*MBhat^3 + 160*m2^4*MBhat^3 - 750*MBhat^4 + 
            1170*m2*MBhat^4 - 450*m2^2*MBhat^4 + 30*m2^3*MBhat^4 + 
            20286*Ycut + 77490*m2*Ycut - 43920*m2^2*Ycut - 34080*m2^3*Ycut + 
            2910*m2^4*Ycut - 6*m2^5*Ycut - 63072*MBhat*Ycut - 
            99360*m2*MBhat*Ycut + 120960*m2^2*MBhat*Ycut + 
            5760*m2^3*MBhat*Ycut + 1440*m2^4*MBhat*Ycut - 288*m2^5*MBhat*
             Ycut + 69390*MBhat^2*Ycut + 10170*m2*MBhat^2*Ycut - 
            61020*m2^2*MBhat^2*Ycut - 1260*m2^3*MBhat^2*Ycut + 
            3810*m2^4*MBhat^2*Ycut - 690*m2^5*MBhat^2*Ycut - 
            31680*MBhat^3*Ycut + 23040*m2*MBhat^3*Ycut + 3840*m2^3*MBhat^3*
             Ycut - 960*m2^4*MBhat^3*Ycut + 4860*MBhat^4*Ycut - 
            6840*m2*MBhat^4*Ycut + 2520*m2^2*MBhat^4*Ycut - 
            180*m2^3*MBhat^4*Ycut - 53235*Ycut^2 - 221625*m2*Ycut^2 + 
            74880*m2^2*Ycut^2 + 81960*m2^3*Ycut^2 - 6735*m2^4*Ycut^2 + 
            15*m2^5*Ycut^2 + 166320*MBhat*Ycut^2 + 308880*m2*MBhat*Ycut^2 - 
            267840*m2^2*MBhat*Ycut^2 - 14400*m2^3*MBhat*Ycut^2 - 
            3600*m2^4*MBhat*Ycut^2 + 720*m2^5*MBhat*Ycut^2 - 
            184275*MBhat^2*Ycut^2 - 68985*m2*MBhat^2*Ycut^2 + 
            144090*m2^2*MBhat^2*Ycut^2 + 4770*m2^3*MBhat^2*Ycut^2 - 
            9525*m2^4*MBhat^2*Ycut^2 + 1725*m2^5*MBhat^2*Ycut^2 + 
            84960*MBhat^3*Ycut^2 - 46080*m2*MBhat^3*Ycut^2 - 
            9600*m2^3*MBhat^3*Ycut^2 + 2400*m2^4*MBhat^3*Ycut^2 - 
            13230*MBhat^4*Ycut^2 + 16560*m2*MBhat^4*Ycut^2 - 
            5760*m2^2*MBhat^4*Ycut^2 + 450*m2^3*MBhat^4*Ycut^2 + 
            75180*Ycut^3 + 342000*m2*Ycut^3 - 41640*m2^2*Ycut^3 - 
            103880*m2^3*Ycut^3 + 8080*m2^4*Ycut^3 - 20*m2^5*Ycut^3 - 
            236160*MBhat*Ycut^3 - 512640*m2*MBhat*Ycut^3 + 299520*m2^2*MBhat*
             Ycut^3 + 19200*m2^3*MBhat*Ycut^3 + 4800*m2^4*MBhat*Ycut^3 - 
            960*m2^5*MBhat*Ycut^3 + 263420*MBhat^2*Ycut^3 + 
            164900*m2*MBhat^2*Ycut^3 - 177300*m2^2*MBhat^2*Ycut^3 - 
            10340*m2^3*MBhat^2*Ycut^3 + 13220*m2^4*MBhat^2*Ycut^3 - 
            2300*m2^5*MBhat^2*Ycut^3 - 122580*MBhat^3*Ycut^3 + 
            42140*m2*MBhat^3*Ycut^3 - 700*m2^2*MBhat^3*Ycut^3 + 
            13300*m2^3*MBhat^3*Ycut^3 - 3200*m2^4*MBhat^3*Ycut^3 + 
            19360*MBhat^4*Ycut^3 - 21180*m2*MBhat^4*Ycut^3 + 
            6860*m2^2*MBhat^4*Ycut^3 - 600*m2^3*MBhat^4*Ycut^3 - 
            60585*Ycut^4 - 303000*m2*Ycut^4 - 26970*m2^2*Ycut^4 + 
            72510*m2^3*Ycut^4 - 5160*m2^4*Ycut^4 + 15*m2^5*Ycut^4 + 
            191720*MBhat*Ycut^4 + 485200*m2*MBhat*Ycut^4 - 168000*m2^2*MBhat*
             Ycut^4 - 13040*m2^3*MBhat*Ycut^4 - 4120*m2^4*MBhat*Ycut^4 + 
            720*m2^5*MBhat*Ycut^4 - 214285*MBhat^2*Ycut^4 - 
            198600*m2*MBhat^2*Ycut^4 + 119230*m2^2*MBhat^2*Ycut^4 + 
            11600*m2^3*MBhat^2*Ycut^4 - 10370*m2^4*MBhat^2*Ycut^4 + 
            1725*m2^5*MBhat^2*Ycut^4 + 99960*MBhat^3*Ycut^4 - 
            10920*m2*MBhat^3*Ycut^4 + 1160*m2^2*MBhat^3*Ycut^4 - 
            10520*m2^3*MBhat^3*Ycut^4 + 2400*m2^4*MBhat^3*Ycut^4 - 
            15870*MBhat^4*Ycut^4 + 14720*m2*MBhat^4*Ycut^4 - 
            4290*m2^2*MBhat^4*Ycut^4 + 450*m2^3*MBhat^4*Ycut^4 + 
            26702*Ycut^5 + 149100*m2*Ycut^5 + 46020*m2^2*Ycut^5 - 
            26180*m2^3*Ycut^5 + 1680*m2^4*Ycut^5 - 6*m2^5*Ycut^5 - 
            86354*MBhat*Ycut^5 - 253440*m2*MBhat*Ycut^5 + 33660*m2^2*MBhat*
             Ycut^5 + 3920*m2^3*MBhat*Ycut^5 + 1830*m2^4*MBhat*Ycut^5 - 
            288*m2^5*MBhat*Ycut^5 + 94050*MBhat^2*Ycut^5 + 128340*m2*MBhat^2*
             Ycut^5 - 42420*m2^2*MBhat^2*Ycut^5 - 6240*m2^3*MBhat^2*Ycut^5 + 
            4440*m2^4*MBhat^2*Ycut^5 - 690*m2^5*MBhat^2*Ycut^5 - 
            42060*MBhat^3*Ycut^5 - 11580*m2*MBhat^3*Ycut^5 + 
            600*m2^2*MBhat^3*Ycut^5 + 4320*m2^3*MBhat^3*Ycut^5 - 
            960*m2^4*MBhat^3*Ycut^5 + 6300*MBhat^4*Ycut^5 - 
            4440*m2*MBhat^4*Ycut^5 + 1020*m2^2*MBhat^4*Ycut^5 - 
            180*m2^3*MBhat^4*Ycut^5 - 5007*Ycut^6 - 34360*m2*Ycut^6 - 
            19740*m2^2*Ycut^6 + 3640*m2^3*Ycut^6 - 100*m2^4*Ycut^6 + 
            m2^5*Ycut^6 + 19524*MBhat*Ycut^6 + 59280*m2*MBhat*Ycut^6 + 
            6920*m2^2*MBhat*Ycut^6 - 800*m2^3*MBhat*Ycut^6 - 
            300*m2^4*MBhat*Ycut^6 + 48*m2^5*MBhat*Ycut^6 - 
            16915*MBhat^2*Ycut^6 - 41560*m2*MBhat^2*Ycut^6 + 
            7830*m2^2*MBhat^2*Ycut^6 + 1230*m2^3*MBhat^2*Ycut^6 - 
            880*m2^4*MBhat^2*Ycut^6 + 115*m2^5*MBhat^2*Ycut^6 + 
            4080*MBhat^3*Ycut^6 + 11760*m2*MBhat^3*Ycut^6 - 
            2120*m2^2*MBhat^3*Ycut^6 - 680*m2^3*MBhat^3*Ycut^6 + 
            160*m2^4*MBhat^3*Ycut^6 + 190*MBhat^4*Ycut^6 - 
            720*m2*MBhat^4*Ycut^6 + 220*m2^2*MBhat^4*Ycut^6 + 
            30*m2^3*MBhat^4*Ycut^6 - 540*Ycut^7 + 2040*m2*Ycut^7 + 
            1500*m2^2*Ycut^7 + 300*m2^3*Ycut^7 - 60*m2^4*Ycut^7 - 
            3070*MBhat*Ycut^7 + 800*m2*MBhat*Ycut^7 - 3480*m2^2*MBhat*
             Ycut^7 + 400*m2^3*MBhat*Ycut^7 - 10*m2^4*MBhat*Ycut^7 - 
            400*MBhat^2*Ycut^7 + 4980*m2*MBhat^2*Ycut^7 - 1420*m2^2*MBhat^2*
             Ycut^7 + 240*m2^3*MBhat^2*Ycut^7 + 40*m2^4*MBhat^2*Ycut^7 + 
            3540*MBhat^3*Ycut^7 - 4860*m2*MBhat^3*Ycut^7 + 1300*m2^2*MBhat^3*
             Ycut^7 - 20*m2^3*MBhat^3*Ycut^7 - 1320*MBhat^4*Ycut^7 + 
            940*m2*MBhat^4*Ycut^7 - 120*m2^2*MBhat^4*Ycut^7 + 770*Ycut^8 - 
            675*m2*Ycut^8 + 690*m2^2*Ycut^8 - 130*m2^3*Ycut^8 + 
            1360*MBhat*Ycut^8 - 1920*m2*MBhat*Ycut^8 + 360*m2^2*MBhat*
             Ycut^8 - 80*m2^3*MBhat*Ycut^8 - 270*MBhat^2*Ycut^8 - 
            45*m2*MBhat^2*Ycut^8 + 450*m2^2*MBhat^2*Ycut^8 - 
            120*m2^3*MBhat^2*Ycut^8 - 1320*MBhat^3*Ycut^8 + 
            1080*m2*MBhat^3*Ycut^8 - 240*m2^2*MBhat^3*Ycut^8 + 
            540*MBhat^4*Ycut^8 - 210*m2*MBhat^4*Ycut^8 - 440*Ycut^9 + 
            410*m2*Ycut^9 - 80*m2^2*Ycut^9 - 270*MBhat*Ycut^9 - 
            20*m2^2*MBhat*Ycut^9 + 260*MBhat^2*Ycut^9 + 90*m2*MBhat^2*
             Ycut^9 - 80*m2^2*MBhat^2*Ycut^9 + 140*MBhat^3*Ycut^9 - 
            100*m2*MBhat^3*Ycut^9 - 80*MBhat^4*Ycut^9 + 114*Ycut^10 - 
            15*m2*Ycut^10 - 28*MBhat*Ycut^10 - 10*MBhat^2*Ycut^10 - 
            15*m2*MBhat^2*Ycut^10 - 4*Ycut^11 - 2*MBhat*Ycut^11))/
          (15*(-1 + Ycut)^6) + 4*Sqrt[m2]*(-14 - 155*m2 - 194*m2^2 - 
           18*m2^3 + 3*m2^4 + 48*MBhat + 336*m2*MBhat + 192*m2^2*MBhat - 
           60*MBhat^2 - 242*m2*MBhat^2 - 47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 + 
           32*MBhat^3 + 64*m2*MBhat^3 - 6*MBhat^4 - 3*m2*MBhat^4 + 
           3*m2^2*MBhat^4)*Log[m2] - 4*Sqrt[m2]*(-14 - 155*m2 - 194*m2^2 - 
           18*m2^3 + 3*m2^4 + 48*MBhat + 336*m2*MBhat + 192*m2^2*MBhat - 
           60*MBhat^2 - 242*m2*MBhat^2 - 47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 + 
           32*MBhat^3 + 64*m2*MBhat^3 - 6*MBhat^4 - 3*m2*MBhat^4 + 
           3*m2^2*MBhat^4)*Log[1 - Ycut]))) + 
   rhoLS*(((-1 + m2 + Ycut)*(-90 - 1230*m2 + 2400*m2^2 - 720*m2^3 - 
        390*m2^4 + 30*m2^5 + 384*MBhat + 504*m2*MBhat - 5616*m2^2*MBhat + 
        2544*m2^3*MBhat - 816*m2^4*MBhat + 120*m2^5*MBhat - 665*MBhat^2 + 
        2470*m2*MBhat^2 - 1250*m2^2*MBhat^2 + 910*m2^3*MBhat^2 - 
        1145*m2^4*MBhat^2 + 280*m2^5*MBhat^2 + 504*MBhat^3 - 
        2016*m2*MBhat^3 + 3024*m2^2*MBhat^3 - 2016*m2^3*MBhat^3 + 
        504*m2^4*MBhat^3 - 135*MBhat^4 + 225*m2*MBhat^4 - 855*m2^2*MBhat^4 + 
        225*m2^3*MBhat^4 + 450*Ycut + 6780*m2*Ycut - 12960*m2^2*Ycut + 
        3600*m2^3*Ycut + 2310*m2^4*Ycut - 180*m2^5*Ycut - 1920*MBhat*Ycut - 
        3576*m2*MBhat*Ycut + 31848*m2^2*MBhat*Ycut - 14568*m2^3*MBhat*Ycut + 
        4776*m2^4*MBhat*Ycut - 720*m2^5*MBhat*Ycut + 3325*MBhat^2*Ycut - 
        12295*m2*MBhat^2*Ycut + 5355*m2^2*MBhat^2*Ycut - 
        4595*m2^3*MBhat^2*Ycut + 6590*m2^4*MBhat^2*Ycut - 
        1680*m2^5*MBhat^2*Ycut - 2520*MBhat^3*Ycut + 10584*m2*MBhat^3*Ycut - 
        16632*m2^2*MBhat^3*Ycut + 11592*m2^3*MBhat^3*Ycut - 
        3024*m2^4*MBhat^3*Ycut + 675*MBhat^4*Ycut - 1260*m2*MBhat^4*Ycut + 
        4905*m2^2*MBhat^4*Ycut - 1350*m2^3*MBhat^4*Ycut - 900*Ycut^2 - 
        15180*m2*Ycut^2 + 28290*m2^2*Ycut^2 - 6990*m2^3*Ycut^2 - 
        5670*m2^4*Ycut^2 + 450*m2^5*Ycut^2 + 3840*MBhat*Ycut^2 + 
        9984*m2*MBhat*Ycut^2 - 73728*m2^2*MBhat*Ycut^2 + 
        34104*m2^3*MBhat*Ycut^2 - 11520*m2^4*MBhat*Ycut^2 + 
        1800*m2^5*MBhat*Ycut^2 - 6650*MBhat^2*Ycut^2 + 
        24120*m2*MBhat^2*Ycut^2 - 7515*m2^2*MBhat^2*Ycut^2 + 
        8740*m2^3*MBhat^2*Ycut^2 - 15495*m2^4*MBhat^2*Ycut^2 + 
        4200*m2^5*MBhat^2*Ycut^2 + 5040*MBhat^3*Ycut^2 - 
        22176*m2*MBhat^3*Ycut^2 + 36792*m2^2*MBhat^3*Ycut^2 - 
        27216*m2^3*MBhat^3*Ycut^2 + 7560*m2^4*MBhat^3*Ycut^2 - 
        1350*MBhat^4*Ycut^2 + 2790*m2*MBhat^4*Ycut^2 - 
        11475*m2^2*MBhat^4*Ycut^2 + 3375*m2^3*MBhat^4*Ycut^2 + 900*Ycut^3 + 
        17400*m2*Ycut^3 - 31350*m2^2*Ycut^3 + 6300*m2^3*Ycut^3 + 
        7350*m2^4*Ycut^3 - 600*m2^5*Ycut^3 - 3840*MBhat*Ycut^3 - 
        14016*m2*MBhat*Ycut^3 + 87936*m2^2*MBhat*Ycut^3 - 
        41160*m2^3*MBhat*Ycut^3 + 14520*m2^4*MBhat*Ycut^3 - 
        2400*m2^5*MBhat*Ycut^3 + 6890*MBhat^2*Ycut^3 - 
        22810*m2*MBhat^2*Ycut^3 + 1895*m2^2*MBhat^2*Ycut^3 - 
        9065*m2^3*MBhat^2*Ycut^3 + 20140*m2^4*MBhat^2*Ycut^3 - 
        5600*m2^5*MBhat^2*Ycut^3 - 5220*MBhat^3*Ycut^3 + 
        22764*m2*MBhat^3*Ycut^3 - 42324*m2^2*MBhat^3*Ycut^3 + 
        34860*m2^3*MBhat^3*Ycut^3 - 10080*m2^4*MBhat^3*Ycut^3 + 
        1350*MBhat^4*Ycut^3 - 3060*m2*MBhat^4*Ycut^3 + 
        14445*m2^2*MBhat^4*Ycut^3 - 4500*m2^3*MBhat^4*Ycut^3 - 450*Ycut^4 - 
        10410*m2*Ycut^4 + 17820*m2^2*Ycut^4 - 2160*m2^3*Ycut^4 - 
        5250*m2^4*Ycut^4 + 450*m2^5*Ycut^4 + 1680*MBhat*Ycut^4 + 
        10344*m2*MBhat*Ycut^4 - 56160*m2^2*MBhat*Ycut^4 + 
        28560*m2^3*MBhat*Ycut^4 - 11280*m2^4*MBhat*Ycut^4 + 
        1800*m2^5*MBhat*Ycut^4 - 4360*MBhat^2*Ycut^4 + 
        9430*m2*MBhat^2*Ycut^4 + 7455*m2^2*MBhat^2*Ycut^4 + 
        3040*m2^3*MBhat^2*Ycut^4 - 14545*m2^4*MBhat^2*Ycut^4 + 
        4200*m2^5*MBhat^2*Ycut^4 + 3420*MBhat^3*Ycut^4 - 
        10176*m2*MBhat^3*Ycut^4 + 25620*m2^2*MBhat^3*Ycut^4 - 
        25560*m2^3*MBhat^3*Ycut^4 + 7560*m2^4*MBhat^3*Ycut^4 - 
        690*MBhat^4*Ycut^4 + 1650*m2*MBhat^4*Ycut^4 - 10545*m2^2*MBhat^4*
         Ycut^4 + 3375*m2^3*MBhat^4*Ycut^4 + 162*Ycut^5 + 2712*m2*Ycut^5 - 
        3948*m2^2*Ycut^5 - 1068*m2^3*Ycut^5 + 2322*m2^4*Ycut^5 - 
        180*m2^5*Ycut^5 + 768*MBhat*Ycut^5 - 4008*m2*MBhat*Ycut^5 + 
        17412*m2^2*MBhat*Ycut^5 - 11328*m2^3*MBhat*Ycut^5 + 
        4932*m2^4*MBhat*Ycut^5 - 720*m2^5*MBhat*Ycut^5 + 
        2240*MBhat^2*Ycut^5 - 30*m2*MBhat^2*Ycut^5 - 11835*m2^2*MBhat^2*
         Ycut^5 + 3625*m2^3*MBhat^2*Ycut^5 + 5370*m2^4*MBhat^2*Ycut^5 - 
        1680*m2^5*MBhat^2*Ycut^5 - 2298*MBhat^3*Ycut^5 - 
        954*m2*MBhat^3*Ycut^5 - 5394*m2^2*MBhat^3*Ycut^5 + 
        9846*m2^3*MBhat^3*Ycut^5 - 3024*m2^4*MBhat^3*Ycut^5 + 
        210*MBhat^4*Ycut^5 - 300*m2*MBhat^4*Ycut^5 + 4275*m2^2*MBhat^4*
         Ycut^5 - 1350*m2^3*MBhat^4*Ycut^5 - 357*Ycut^6 + 195*m2*Ycut^6 - 
        783*m2^2*Ycut^6 + 1449*m2^3*Ycut^6 - 534*m2^4*Ycut^6 + 
        30*m2^5*Ycut^6 - 2160*MBhat*Ycut^6 + 1392*m2*MBhat*Ycut^6 - 
        996*m2^2*MBhat*Ycut^6 + 1716*m2^3*MBhat*Ycut^6 - 
        1032*m2^4*MBhat*Ycut^6 + 120*m2^5*MBhat*Ycut^6 - 751*MBhat^2*Ycut^6 - 
        1141*m2*MBhat^2*Ycut^6 + 8174*m2^2*MBhat^2*Ycut^6 - 
        3851*m2^3*MBhat^2*Ycut^6 - 671*m2^4*MBhat^2*Ycut^6 + 
        280*m2^5*MBhat^2*Ycut^6 + 1770*MBhat^3*Ycut^6 + 
        3096*m2*MBhat^3*Ycut^6 - 2298*m2^2*MBhat^3*Ycut^6 - 
        1452*m2^3*MBhat^3*Ycut^6 + 504*m2^4*MBhat^3*Ycut^6 - 
        150*MBhat^4*Ycut^6 - 90*m2*MBhat^4*Ycut^6 - 765*m2^2*MBhat^4*Ycut^6 + 
        225*m2^3*MBhat^4*Ycut^6 + 705*Ycut^7 - 540*m2*Ycut^7 + 
        657*m2^2*Ycut^7 - 414*m2^3*Ycut^7 + 42*m2^4*Ycut^7 + 
        1920*MBhat*Ycut^7 - 1008*m2*MBhat*Ycut^7 - 924*m2^2*MBhat*Ycut^7 + 
        192*m2^3*MBhat*Ycut^7 + 60*m2^4*MBhat*Ycut^7 - 445*MBhat^2*Ycut^7 + 
        274*m2*MBhat^2*Ycut^7 - 2532*m2^2*MBhat^2*Ycut^7 + 
        1357*m2^3*MBhat^2*Ycut^7 - 64*m2^4*MBhat^2*Ycut^7 - 
        840*MBhat^3*Ycut^7 - 1344*m2*MBhat^3*Ycut^7 + 1398*m2^2*MBhat^3*
         Ycut^7 - 54*m2^3*MBhat^3*Ycut^7 + 150*MBhat^4*Ycut^7 + 
        60*m2*MBhat^4*Ycut^7 + 15*m2^2*MBhat^4*Ycut^7 - 690*Ycut^8 + 
        390*m2*Ycut^8 - 123*m2^2*Ycut^8 + 3*m2^3*Ycut^8 - 720*MBhat*Ycut^8 + 
        432*m2*MBhat*Ycut^8 + 228*m2^2*MBhat*Ycut^8 - 60*m2^3*MBhat*Ycut^8 + 
        575*MBhat^2*Ycut^8 - 6*m2*MBhat^2*Ycut^8 + 252*m2^2*MBhat^2*Ycut^8 - 
        161*m2^3*MBhat^2*Ycut^8 + 120*MBhat^3*Ycut^8 + 
        216*m2*MBhat^3*Ycut^8 - 186*m2^2*MBhat^3*Ycut^8 - 75*MBhat^4*Ycut^8 - 
        15*m2*MBhat^4*Ycut^8 + 330*Ycut^9 - 120*m2*Ycut^9 - 3*m2^2*Ycut^9 - 
        48*m2*MBhat*Ycut^9 - 155*MBhat^2*Ycut^9 - 11*m2*MBhat^2*Ycut^9 + 
        m2^2*MBhat^2*Ycut^9 + 30*MBhat^3*Ycut^9 + 6*m2*MBhat^3*Ycut^9 + 
        15*MBhat^4*Ycut^9 - 57*Ycut^10 + 3*m2*Ycut^10 + 48*MBhat*Ycut^10 - 
        5*MBhat^2*Ycut^10 - m2*MBhat^2*Ycut^10 - 6*MBhat^3*Ycut^10 - 
        3*Ycut^11 + MBhat^2*Ycut^11))/(90*(-1 + Ycut)^6) + 
     (8*Sqrt[m2]*Ycut^3*(-1 + m2 + Ycut)*(10*MBhat^2 - 20*m2*MBhat^2 + 
        10*m2^2*MBhat^2 - 12*MBhat^3 + 12*m2*MBhat^3 + 3*MBhat^4 - 
        10*MBhat*Ycut + 20*m2*MBhat*Ycut - 10*m2^2*MBhat*Ycut - 
        12*MBhat^2*Ycut + 22*m2*MBhat^2*Ycut - 10*m2^2*MBhat^2*Ycut + 
        24*MBhat^3*Ycut - 18*m2*MBhat^3*Ycut - 6*MBhat^4*Ycut + 3*Ycut^2 - 
        6*m2*Ycut^2 + 3*m2^2*Ycut^2 + 18*MBhat*Ycut^2 - 22*m2*MBhat*Ycut^2 + 
        4*m2^2*MBhat*Ycut^2 - 6*MBhat^2*Ycut^2 - 2*m2*MBhat^2*Ycut^2 + 
        3*m2^2*MBhat^2*Ycut^2 - 12*MBhat^3*Ycut^2 + 6*m2*MBhat^3*Ycut^2 + 
        3*MBhat^4*Ycut^2 - 6*Ycut^3 + 6*m2*Ycut^3 - 6*MBhat*Ycut^3 + 
        2*m2*MBhat*Ycut^3 + 8*MBhat^2*Ycut^3 + 3*Ycut^4 - 2*MBhat*Ycut^4)*
       c[SR]*c[T])/(3*(-1 + Ycut)^4) + 
     (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 72*m2*MBhat + 
        12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 9*m2*MBhat^4)*Log[m2])/
      3 - (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 72*m2*MBhat + 
        12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 9*m2*MBhat^4)*
       Log[1 - Ycut])/3 + c[SL]^2*
      (((-1 + m2 + Ycut)*(123 + 6063*m2 + 11238*m2^2 + 438*m2^3 - 237*m2^4 + 
          15*m2^5 - 480*MBhat - 13632*m2*MBhat - 7632*m2^2*MBhat + 
          1968*m2^3*MBhat - 432*m2^4*MBhat + 48*m2^5*MBhat + 730*MBhat^2 + 
          10840*m2*MBhat^2 - 410*m2^2*MBhat^2 + 550*m2^3*MBhat^2 - 
          380*m2^4*MBhat^2 + 70*m2^5*MBhat^2 - 576*MBhat^3 - 
          3696*m2*MBhat^3 + 2064*m2^2*MBhat^3 - 816*m2^3*MBhat^3 + 
          144*m2^4*MBhat^3 + 195*MBhat^4 + 675*m2*MBhat^4 - 
          405*m2^2*MBhat^4 + 75*m2^3*MBhat^4 - 615*Ycut - 32352*m2*Ycut - 
          62424*m2^2*Ycut - 2946*m2^3*Ycut + 1407*m2^4*Ycut - 90*m2^5*Ycut + 
          2400*MBhat*Ycut + 73440*m2*MBhat*Ycut + 44208*m2^2*MBhat*Ycut - 
          11424*m2^3*MBhat*Ycut + 2544*m2^4*MBhat*Ycut - 
          288*m2^5*MBhat*Ycut - 3650*MBhat^2*Ycut - 59230*m2*MBhat^2*Ycut + 
          1380*m2^2*MBhat^2*Ycut - 2990*m2^3*MBhat^2*Ycut + 
          2210*m2^4*MBhat^2*Ycut - 420*m2^5*MBhat^2*Ycut + 
          2880*MBhat^3*Ycut + 20784*m2*MBhat^3*Ycut - 11712*m2^2*MBhat^3*
           Ycut + 4752*m2^3*MBhat^3*Ycut - 864*m2^4*MBhat^3*Ycut - 
          975*MBhat^4*Ycut - 3900*m2*MBhat^4*Ycut + 2355*m2^2*MBhat^4*Ycut - 
          450*m2^3*MBhat^4*Ycut + 1230*Ycut^2 + 69858*m2*Ycut^2 + 
          141489*m2^2*Ycut^2 + 8223*m2^3*Ycut^2 - 3465*m2^4*Ycut^2 + 
          225*m2^5*Ycut^2 - 4800*MBhat*Ycut^2 - 160320*m2*MBhat*Ycut^2 - 
          105312*m2^2*MBhat*Ycut^2 + 27264*m2^3*MBhat*Ycut^2 - 
          6192*m2^4*MBhat*Ycut^2 + 720*m2^5*MBhat*Ycut^2 + 
          7300*MBhat^2*Ycut^2 + 131400*m2*MBhat^2*Ycut^2 - 
          330*m2^2*MBhat^2*Ycut^2 + 6460*m2^3*MBhat^2*Ycut^2 - 
          5280*m2^4*MBhat^2*Ycut^2 + 1050*m2^5*MBhat^2*Ycut^2 - 
          5760*MBhat^3*Ycut^2 - 47616*m2*MBhat^3*Ycut^2 + 
          27072*m2^2*MBhat^3*Ycut^2 - 11376*m2^3*MBhat^3*Ycut^2 + 
          2160*m2^4*MBhat^3*Ycut^2 + 1950*MBhat^4*Ycut^2 + 
          9210*m2*MBhat^4*Ycut^2 - 5625*m2^2*MBhat^4*Ycut^2 + 
          1125*m2^3*MBhat^4*Ycut^2 - 1230*Ycut^3 - 76812*m2*Ycut^3 - 
          165363*m2^2*Ycut^3 - 12180*m2^3*Ycut^3 + 4515*m2^4*Ycut^3 - 
          300*m2^5*Ycut^3 + 4800*MBhat*Ycut^3 + 178560*m2*MBhat*Ycut^3 + 
          130848*m2^2*MBhat*Ycut^3 - 33888*m2^3*MBhat*Ycut^3 + 
          7920*m2^4*MBhat*Ycut^3 - 960*m2^5*MBhat*Ycut^3 - 
          7180*MBhat^2*Ycut^3 - 149980*m2*MBhat^2*Ycut^3 - 
          2590*m2^2*MBhat^2*Ycut^3 - 8330*m2^3*MBhat^2*Ycut^3 + 
          7030*m2^4*MBhat^2*Ycut^3 - 1400*m2^5*MBhat^2*Ycut^3 + 
          5660*MBhat^3*Ycut^3 + 56964*m2*MBhat^3*Ycut^3 - 
          33564*m2^2*MBhat^3*Ycut^3 + 14860*m2^3*MBhat^3*Ycut^3 - 
          2880*m2^4*MBhat^3*Ycut^3 - 1950*MBhat^4*Ycut^3 - 
          11460*m2*MBhat^4*Ycut^3 + 7215*m2^2*MBhat^4*Ycut^3 - 
          1500*m2^3*MBhat^4*Ycut^3 + 615*Ycut^4 + 43683*m2*Ycut^4 + 
          102150*m2^2*Ycut^4 + 10050*m2^3*Ycut^4 - 3255*m2^4*Ycut^4 + 
          225*m2^5*Ycut^4 - 2520*MBhat*Ycut^4 - 102360*m2*MBhat*Ycut^4 - 
          89352*m2^2*MBhat*Ycut^4 + 24120*m2^3*MBhat*Ycut^4 - 
          6000*m2^4*MBhat*Ycut^4 + 720*m2^5*MBhat*Ycut^4 + 
          3165*MBhat^2*Ycut^4 + 90475*m2*MBhat^2*Ycut^4 + 
          3435*m2^2*MBhat^2*Ycut^4 + 6345*m2^3*MBhat^2*Ycut^4 - 
          5290*m2^4*MBhat^2*Ycut^4 + 1050*m2^5*MBhat^2*Ycut^4 - 
          2380*MBhat^3*Ycut^4 - 37576*m2*MBhat^3*Ycut^4 + 
          23900*m2^2*MBhat^3*Ycut^4 - 11160*m2^3*MBhat^3*Ycut^4 + 
          2160*m2^4*MBhat^3*Ycut^4 + 970*MBhat^4*Ycut^4 + 
          7990*m2*MBhat^4*Ycut^4 - 5315*m2^2*MBhat^4*Ycut^4 + 
          1125*m2^3*MBhat^4*Ycut^4 - 87*Ycut^5 - 11124*m2*Ycut^5 - 
          28494*m2^2*Ycut^5 - 4764*m2^3*Ycut^5 + 1341*m2^4*Ycut^5 - 
          90*m2^5*Ycut^5 + 1032*MBhat*Ycut^5 + 23676*m2*MBhat*Ycut^5 + 
          32244*m2^2*MBhat*Ycut^5 - 9756*m2^3*MBhat*Ycut^5 + 
          2484*m2^4*MBhat*Ycut^5 - 288*m2^5*MBhat*Ycut^5 - 
          105*MBhat^2*Ycut^5 - 24770*m2*MBhat^2*Ycut^5 - 2435*m2^2*MBhat^2*
           Ycut^5 - 2330*m2^3*MBhat^2*Ycut^5 + 2130*m2^4*MBhat^2*Ycut^5 - 
          420*m2^5*MBhat^2*Ycut^5 - 418*MBhat^3*Ycut^5 + 
          13006*m2*MBhat^3*Ycut^5 - 9174*m2^2*MBhat^3*Ycut^5 + 
          4506*m2^3*MBhat^3*Ycut^5 - 864*m2^4*MBhat^3*Ycut^5 - 
          170*MBhat^4*Ycut^5 - 2980*m2*MBhat^4*Ycut^5 + 2145*m2^2*MBhat^4*
           Ycut^5 - 450*m2^3*MBhat^4*Ycut^5 - 174*Ycut^6 + 1182*m2*Ycut^6 + 
          408*m2^2*Ycut^6 + 1344*m2^3*Ycut^6 - 255*m2^4*Ycut^6 + 
          15*m2^5*Ycut^6 - 960*MBhat*Ycut^6 + 1812*m2*MBhat*Ycut^6 - 
          5064*m2^2*MBhat*Ycut^6 + 1740*m2^3*MBhat*Ycut^6 - 
          456*m2^4*MBhat*Ycut^6 + 48*m2^5*MBhat*Ycut^6 - 52*MBhat^2*Ycut^6 + 
          528*m2*MBhat^2*Ycut^6 + 1123*m2^2*MBhat^2*Ycut^6 + 
          253*m2^3*MBhat^2*Ycut^6 - 362*m2^4*MBhat^2*Ycut^6 + 
          70*m2^5*MBhat^2*Ycut^6 + 970*MBhat^3*Ycut^6 - 1944*m2*MBhat^3*
           Ycut^6 + 1362*m2^2*MBhat^3*Ycut^6 - 772*m2^3*MBhat^3*Ycut^6 + 
          144*m2^4*MBhat^3*Ycut^6 - 50*MBhat^4*Ycut^6 + 450*m2*MBhat^4*
           Ycut^6 - 375*m2^2*MBhat^4*Ycut^6 + 75*m2^3*MBhat^4*Ycut^6 + 
          330*Ycut^7 - 888*m2*Ycut^7 + 1020*m2^2*Ycut^7 - 156*m2^3*Ycut^7 + 
          9*m2^4*Ycut^7 + 720*MBhat*Ycut^7 - 1128*m2*MBhat*Ycut^7 + 
          48*m2^2*MBhat*Ycut^7 - 12*m2^3*MBhat*Ycut^7 + 
          12*m2^4*MBhat*Ycut^7 - 540*MBhat^2*Ycut^7 + 788*m2*MBhat^2*Ycut^7 - 
          129*m2^2*MBhat^2*Ycut^7 + 44*m2^3*MBhat^2*Ycut^7 + 
          2*m2^4*MBhat^2*Ycut^7 - 440*MBhat^3*Ycut^7 + 136*m2*MBhat^3*
           Ycut^7 + 58*m2^2*MBhat^3*Ycut^7 + 6*m2^3*MBhat^3*Ycut^7 + 
          50*MBhat^4*Ycut^7 + 20*m2*MBhat^4*Ycut^7 + 5*m2^2*MBhat^4*Ycut^7 - 
          300*Ycut^8 + 432*m2*Ycut^8 - 33*m2^2*Ycut^8 - 9*m2^3*Ycut^8 - 
          120*MBhat*Ycut^8 - 48*m2*MBhat*Ycut^8 - 12*m2^3*MBhat*Ycut^8 + 
          435*MBhat^2*Ycut^8 - 97*m2*MBhat^2*Ycut^8 - 46*m2^2*MBhat^2*
           Ycut^8 - 2*m2^3*MBhat^2*Ycut^8 + 40*MBhat^3*Ycut^8 - 
          64*m2*MBhat^3*Ycut^8 - 6*m2^2*MBhat^3*Ycut^8 - 25*MBhat^4*Ycut^8 - 
          5*m2*MBhat^4*Ycut^8 + 120*Ycut^9 - 48*m2*Ycut^9 + 9*m2^2*Ycut^9 - 
          120*MBhat*Ycut^9 + 12*m2*MBhat*Ycut^9 + 12*m2^2*MBhat*Ycut^9 - 
          95*MBhat^2*Ycut^9 + 48*m2*MBhat^2*Ycut^9 + 2*m2^2*MBhat^2*Ycut^9 + 
          30*MBhat^3*Ycut^9 + 6*m2*MBhat^3*Ycut^9 + 5*MBhat^4*Ycut^9 - 
          6*Ycut^10 + 6*m2*Ycut^10 + 48*MBhat*Ycut^10 - 12*m2*MBhat*Ycut^10 - 
          10*MBhat^2*Ycut^10 - 2*m2*MBhat^2*Ycut^10 - 6*MBhat^3*Ycut^10 - 
          6*Ycut^11 + 2*MBhat^2*Ycut^11))/(120*(-1 + Ycut)^6) + 
       (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 96*MBhat + 240*m2*MBhat - 
          96*MBhat^2 - 108*m2*MBhat^2 + 14*m2^2*MBhat^2 + 48*MBhat^3 - 
          12*MBhat^4 + 3*m2*MBhat^4)*Log[m2])/2 - 
       (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 96*MBhat + 240*m2*MBhat - 
          96*MBhat^2 - 108*m2*MBhat^2 + 14*m2^2*MBhat^2 + 48*MBhat^3 - 
          12*MBhat^4 + 3*m2*MBhat^4)*Log[1 - Ycut])/2) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(123 + 6063*m2 + 11238*m2^2 + 438*m2^3 - 
          237*m2^4 + 15*m2^5 - 480*MBhat - 13632*m2*MBhat - 7632*m2^2*MBhat + 
          1968*m2^3*MBhat - 432*m2^4*MBhat + 48*m2^5*MBhat + 730*MBhat^2 + 
          10840*m2*MBhat^2 - 410*m2^2*MBhat^2 + 550*m2^3*MBhat^2 - 
          380*m2^4*MBhat^2 + 70*m2^5*MBhat^2 - 576*MBhat^3 - 
          3696*m2*MBhat^3 + 2064*m2^2*MBhat^3 - 816*m2^3*MBhat^3 + 
          144*m2^4*MBhat^3 + 195*MBhat^4 + 675*m2*MBhat^4 - 
          405*m2^2*MBhat^4 + 75*m2^3*MBhat^4 - 615*Ycut - 32352*m2*Ycut - 
          62424*m2^2*Ycut - 2946*m2^3*Ycut + 1407*m2^4*Ycut - 90*m2^5*Ycut + 
          2400*MBhat*Ycut + 73440*m2*MBhat*Ycut + 44208*m2^2*MBhat*Ycut - 
          11424*m2^3*MBhat*Ycut + 2544*m2^4*MBhat*Ycut - 
          288*m2^5*MBhat*Ycut - 3650*MBhat^2*Ycut - 59230*m2*MBhat^2*Ycut + 
          1380*m2^2*MBhat^2*Ycut - 2990*m2^3*MBhat^2*Ycut + 
          2210*m2^4*MBhat^2*Ycut - 420*m2^5*MBhat^2*Ycut + 
          2880*MBhat^3*Ycut + 20784*m2*MBhat^3*Ycut - 11712*m2^2*MBhat^3*
           Ycut + 4752*m2^3*MBhat^3*Ycut - 864*m2^4*MBhat^3*Ycut - 
          975*MBhat^4*Ycut - 3900*m2*MBhat^4*Ycut + 2355*m2^2*MBhat^4*Ycut - 
          450*m2^3*MBhat^4*Ycut + 1230*Ycut^2 + 69858*m2*Ycut^2 + 
          141489*m2^2*Ycut^2 + 8223*m2^3*Ycut^2 - 3465*m2^4*Ycut^2 + 
          225*m2^5*Ycut^2 - 4800*MBhat*Ycut^2 - 160320*m2*MBhat*Ycut^2 - 
          105312*m2^2*MBhat*Ycut^2 + 27264*m2^3*MBhat*Ycut^2 - 
          6192*m2^4*MBhat*Ycut^2 + 720*m2^5*MBhat*Ycut^2 + 
          7300*MBhat^2*Ycut^2 + 131400*m2*MBhat^2*Ycut^2 - 
          330*m2^2*MBhat^2*Ycut^2 + 6460*m2^3*MBhat^2*Ycut^2 - 
          5280*m2^4*MBhat^2*Ycut^2 + 1050*m2^5*MBhat^2*Ycut^2 - 
          5760*MBhat^3*Ycut^2 - 47616*m2*MBhat^3*Ycut^2 + 
          27072*m2^2*MBhat^3*Ycut^2 - 11376*m2^3*MBhat^3*Ycut^2 + 
          2160*m2^4*MBhat^3*Ycut^2 + 1950*MBhat^4*Ycut^2 + 
          9210*m2*MBhat^4*Ycut^2 - 5625*m2^2*MBhat^4*Ycut^2 + 
          1125*m2^3*MBhat^4*Ycut^2 - 1230*Ycut^3 - 76812*m2*Ycut^3 - 
          165363*m2^2*Ycut^3 - 12180*m2^3*Ycut^3 + 4515*m2^4*Ycut^3 - 
          300*m2^5*Ycut^3 + 4800*MBhat*Ycut^3 + 178560*m2*MBhat*Ycut^3 + 
          130848*m2^2*MBhat*Ycut^3 - 33888*m2^3*MBhat*Ycut^3 + 
          7920*m2^4*MBhat*Ycut^3 - 960*m2^5*MBhat*Ycut^3 - 
          7180*MBhat^2*Ycut^3 - 149980*m2*MBhat^2*Ycut^3 - 
          2590*m2^2*MBhat^2*Ycut^3 - 8330*m2^3*MBhat^2*Ycut^3 + 
          7030*m2^4*MBhat^2*Ycut^3 - 1400*m2^5*MBhat^2*Ycut^3 + 
          5660*MBhat^3*Ycut^3 + 56964*m2*MBhat^3*Ycut^3 - 
          33564*m2^2*MBhat^3*Ycut^3 + 14860*m2^3*MBhat^3*Ycut^3 - 
          2880*m2^4*MBhat^3*Ycut^3 - 1950*MBhat^4*Ycut^3 - 
          11460*m2*MBhat^4*Ycut^3 + 7215*m2^2*MBhat^4*Ycut^3 - 
          1500*m2^3*MBhat^4*Ycut^3 + 615*Ycut^4 + 43683*m2*Ycut^4 + 
          102150*m2^2*Ycut^4 + 10050*m2^3*Ycut^4 - 3255*m2^4*Ycut^4 + 
          225*m2^5*Ycut^4 - 2520*MBhat*Ycut^4 - 102360*m2*MBhat*Ycut^4 - 
          89352*m2^2*MBhat*Ycut^4 + 24120*m2^3*MBhat*Ycut^4 - 
          6000*m2^4*MBhat*Ycut^4 + 720*m2^5*MBhat*Ycut^4 + 
          3165*MBhat^2*Ycut^4 + 90475*m2*MBhat^2*Ycut^4 + 
          3435*m2^2*MBhat^2*Ycut^4 + 6345*m2^3*MBhat^2*Ycut^4 - 
          5290*m2^4*MBhat^2*Ycut^4 + 1050*m2^5*MBhat^2*Ycut^4 - 
          2380*MBhat^3*Ycut^4 - 37576*m2*MBhat^3*Ycut^4 + 
          23900*m2^2*MBhat^3*Ycut^4 - 11160*m2^3*MBhat^3*Ycut^4 + 
          2160*m2^4*MBhat^3*Ycut^4 + 970*MBhat^4*Ycut^4 + 
          7990*m2*MBhat^4*Ycut^4 - 5315*m2^2*MBhat^4*Ycut^4 + 
          1125*m2^3*MBhat^4*Ycut^4 - 87*Ycut^5 - 11124*m2*Ycut^5 - 
          28494*m2^2*Ycut^5 - 4764*m2^3*Ycut^5 + 1341*m2^4*Ycut^5 - 
          90*m2^5*Ycut^5 + 1032*MBhat*Ycut^5 + 23676*m2*MBhat*Ycut^5 + 
          32244*m2^2*MBhat*Ycut^5 - 9756*m2^3*MBhat*Ycut^5 + 
          2484*m2^4*MBhat*Ycut^5 - 288*m2^5*MBhat*Ycut^5 - 
          105*MBhat^2*Ycut^5 - 24770*m2*MBhat^2*Ycut^5 - 2435*m2^2*MBhat^2*
           Ycut^5 - 2330*m2^3*MBhat^2*Ycut^5 + 2130*m2^4*MBhat^2*Ycut^5 - 
          420*m2^5*MBhat^2*Ycut^5 - 418*MBhat^3*Ycut^5 + 
          13006*m2*MBhat^3*Ycut^5 - 9174*m2^2*MBhat^3*Ycut^5 + 
          4506*m2^3*MBhat^3*Ycut^5 - 864*m2^4*MBhat^3*Ycut^5 - 
          170*MBhat^4*Ycut^5 - 2980*m2*MBhat^4*Ycut^5 + 2145*m2^2*MBhat^4*
           Ycut^5 - 450*m2^3*MBhat^4*Ycut^5 - 174*Ycut^6 + 1182*m2*Ycut^6 + 
          408*m2^2*Ycut^6 + 1344*m2^3*Ycut^6 - 255*m2^4*Ycut^6 + 
          15*m2^5*Ycut^6 - 960*MBhat*Ycut^6 + 1812*m2*MBhat*Ycut^6 - 
          5064*m2^2*MBhat*Ycut^6 + 1740*m2^3*MBhat*Ycut^6 - 
          456*m2^4*MBhat*Ycut^6 + 48*m2^5*MBhat*Ycut^6 - 52*MBhat^2*Ycut^6 + 
          528*m2*MBhat^2*Ycut^6 + 1123*m2^2*MBhat^2*Ycut^6 + 
          253*m2^3*MBhat^2*Ycut^6 - 362*m2^4*MBhat^2*Ycut^6 + 
          70*m2^5*MBhat^2*Ycut^6 + 970*MBhat^3*Ycut^6 - 1944*m2*MBhat^3*
           Ycut^6 + 1362*m2^2*MBhat^3*Ycut^6 - 772*m2^3*MBhat^3*Ycut^6 + 
          144*m2^4*MBhat^3*Ycut^6 - 50*MBhat^4*Ycut^6 + 450*m2*MBhat^4*
           Ycut^6 - 375*m2^2*MBhat^4*Ycut^6 + 75*m2^3*MBhat^4*Ycut^6 + 
          330*Ycut^7 - 888*m2*Ycut^7 + 1020*m2^2*Ycut^7 - 156*m2^3*Ycut^7 + 
          9*m2^4*Ycut^7 + 720*MBhat*Ycut^7 - 1128*m2*MBhat*Ycut^7 + 
          48*m2^2*MBhat*Ycut^7 - 12*m2^3*MBhat*Ycut^7 + 
          12*m2^4*MBhat*Ycut^7 - 540*MBhat^2*Ycut^7 + 788*m2*MBhat^2*Ycut^7 - 
          129*m2^2*MBhat^2*Ycut^7 + 44*m2^3*MBhat^2*Ycut^7 + 
          2*m2^4*MBhat^2*Ycut^7 - 440*MBhat^3*Ycut^7 + 136*m2*MBhat^3*
           Ycut^7 + 58*m2^2*MBhat^3*Ycut^7 + 6*m2^3*MBhat^3*Ycut^7 + 
          50*MBhat^4*Ycut^7 + 20*m2*MBhat^4*Ycut^7 + 5*m2^2*MBhat^4*Ycut^7 - 
          300*Ycut^8 + 432*m2*Ycut^8 - 33*m2^2*Ycut^8 - 9*m2^3*Ycut^8 - 
          120*MBhat*Ycut^8 - 48*m2*MBhat*Ycut^8 - 12*m2^3*MBhat*Ycut^8 + 
          435*MBhat^2*Ycut^8 - 97*m2*MBhat^2*Ycut^8 - 46*m2^2*MBhat^2*
           Ycut^8 - 2*m2^3*MBhat^2*Ycut^8 + 40*MBhat^3*Ycut^8 - 
          64*m2*MBhat^3*Ycut^8 - 6*m2^2*MBhat^3*Ycut^8 - 25*MBhat^4*Ycut^8 - 
          5*m2*MBhat^4*Ycut^8 + 120*Ycut^9 - 48*m2*Ycut^9 + 9*m2^2*Ycut^9 - 
          120*MBhat*Ycut^9 + 12*m2*MBhat*Ycut^9 + 12*m2^2*MBhat*Ycut^9 - 
          95*MBhat^2*Ycut^9 + 48*m2*MBhat^2*Ycut^9 + 2*m2^2*MBhat^2*Ycut^9 + 
          30*MBhat^3*Ycut^9 + 6*m2*MBhat^3*Ycut^9 + 5*MBhat^4*Ycut^9 - 
          6*Ycut^10 + 6*m2*Ycut^10 + 48*MBhat*Ycut^10 - 12*m2*MBhat*Ycut^10 - 
          10*MBhat^2*Ycut^10 - 2*m2*MBhat^2*Ycut^10 - 6*MBhat^3*Ycut^10 - 
          6*Ycut^11 + 2*MBhat^2*Ycut^11))/(120*(-1 + Ycut)^6) + 
       (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 96*MBhat + 240*m2*MBhat - 
          96*MBhat^2 - 108*m2*MBhat^2 + 14*m2^2*MBhat^2 + 48*MBhat^3 - 
          12*MBhat^4 + 3*m2*MBhat^4)*Log[m2])/2 - 
       (m2*(-36 - 171*m2 - 96*m2^2 + 9*m2^3 + 96*MBhat + 240*m2*MBhat - 
          96*MBhat^2 - 108*m2*MBhat^2 + 14*m2^2*MBhat^2 + 48*MBhat^3 - 
          12*MBhat^4 + 3*m2*MBhat^4)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(-90 - 1230*m2 + 2400*m2^2 - 720*m2^3 - 
          390*m2^4 + 30*m2^5 + 384*MBhat + 504*m2*MBhat - 5616*m2^2*MBhat + 
          2544*m2^3*MBhat - 816*m2^4*MBhat + 120*m2^5*MBhat - 665*MBhat^2 + 
          2470*m2*MBhat^2 - 1250*m2^2*MBhat^2 + 910*m2^3*MBhat^2 - 
          1145*m2^4*MBhat^2 + 280*m2^5*MBhat^2 + 504*MBhat^3 - 
          2016*m2*MBhat^3 + 3024*m2^2*MBhat^3 - 2016*m2^3*MBhat^3 + 
          504*m2^4*MBhat^3 - 135*MBhat^4 + 225*m2*MBhat^4 - 
          855*m2^2*MBhat^4 + 225*m2^3*MBhat^4 + 270*Ycut + 4320*m2*Ycut - 
          8160*m2^2*Ycut + 2160*m2^3*Ycut + 1530*m2^4*Ycut - 120*m2^5*Ycut - 
          1152*MBhat*Ycut - 2568*m2*MBhat*Ycut + 20616*m2^2*MBhat*Ycut - 
          9480*m2^3*MBhat*Ycut + 3144*m2^4*MBhat*Ycut - 480*m2^5*MBhat*Ycut + 
          1995*MBhat^2*Ycut - 7355*m2*MBhat^2*Ycut + 2855*m2^2*MBhat^2*Ycut - 
          2775*m2^3*MBhat^2*Ycut + 4300*m2^4*MBhat^2*Ycut - 
          1120*m2^5*MBhat^2*Ycut - 1512*MBhat^3*Ycut + 6552*m2*MBhat^3*Ycut - 
          10584*m2^2*MBhat^3*Ycut + 7560*m2^3*MBhat^3*Ycut - 
          2016*m2^4*MBhat^3*Ycut + 405*MBhat^4*Ycut - 810*m2*MBhat^4*Ycut + 
          3195*m2^2*MBhat^4*Ycut - 900*m2^3*MBhat^4*Ycut - 270*Ycut^2 - 
          5310*m2*Ycut^2 + 9570*m2^2*Ycut^2 - 1950*m2^3*Ycut^2 - 
          2220*m2^4*Ycut^2 + 180*m2^5*Ycut^2 + 1152*MBhat*Ycut^2 + 
          4344*m2*MBhat*Ycut^2 - 26880*m2^2*MBhat*Ycut^2 + 
          12600*m2^3*MBhat*Ycut^2 - 4416*m2^4*MBhat*Ycut^2 + 
          720*m2^5*MBhat*Ycut^2 - 1995*MBhat^2*Ycut^2 + 6940*m2*MBhat^2*
           Ycut^2 - 555*m2^2*MBhat^2*Ycut^2 + 2280*m2^3*MBhat^2*Ycut^2 - 
          5750*m2^4*MBhat^2*Ycut^2 + 1680*m2^5*MBhat^2*Ycut^2 + 
          1512*MBhat^3*Ycut^2 - 7056*m2*MBhat^3*Ycut^2 + 12600*m2^2*MBhat^3*
           Ycut^2 - 10080*m2^3*MBhat^3*Ycut^2 + 3024*m2^4*MBhat^3*Ycut^2 - 
          405*MBhat^4*Ycut^2 + 945*m2*MBhat^4*Ycut^2 - 4230*m2^2*MBhat^4*
           Ycut^2 + 1350*m2^3*MBhat^4*Ycut^2 + 90*Ycut^3 + 2460*m2*Ycut^3 - 
          4050*m2^2*Ycut^3 + 240*m2^3*Ycut^3 + 1380*m2^4*Ycut^3 - 
          120*m2^5*Ycut^3 - 384*MBhat*Ycut^3 - 2760*m2*MBhat*Ycut^3 + 
          13560*m2^2*MBhat*Ycut^3 - 6480*m2^3*MBhat*Ycut^3 + 
          2544*m2^4*MBhat*Ycut^3 - 480*m2^5*MBhat*Ycut^3 + 
          665*MBhat^2*Ycut^3 - 1815*m2*MBhat^2*Ycut^3 - 2310*m2^2*MBhat^2*
           Ycut^3 + 430*m2^3*MBhat^2*Ycut^3 + 2900*m2^4*MBhat^2*Ycut^3 - 
          1120*m2^5*MBhat^2*Ycut^3 - 504*MBhat^3*Ycut^3 + 
          2520*m2*MBhat^3*Ycut^3 - 5040*m2^2*MBhat^3*Ycut^3 + 
          5040*m2^3*MBhat^3*Ycut^3 - 2016*m2^4*MBhat^3*Ycut^3 + 
          135*MBhat^4*Ycut^3 - 360*m2*MBhat^4*Ycut^3 + 2070*m2^2*MBhat^4*
           Ycut^3 - 900*m2^3*MBhat^4*Ycut^3 - 180*m2*Ycut^4 + 
          150*m2^2*Ycut^4 + 270*m2^3*Ycut^4 - 270*m2^4*Ycut^4 + 
          30*m2^5*Ycut^4 + 720*m2*MBhat*Ycut^4 - 1920*m2^2*MBhat*Ycut^4 + 
          840*m2^3*MBhat*Ycut^4 - 336*m2^4*MBhat*Ycut^4 + 
          120*m2^5*MBhat*Ycut^4 + 75*MBhat^2*Ycut^4 - 480*m2*MBhat^2*Ycut^4 + 
          2190*m2^2*MBhat^2*Ycut^4 - 1920*m2^3*MBhat^2*Ycut^4 - 
          25*m2^4*MBhat^2*Ycut^4 + 280*m2^5*MBhat^2*Ycut^4 - 
          1080*m2^2*MBhat^3*Ycut^4 + 504*m2^4*MBhat^3*Ycut^4 - 
          45*MBhat^4*Ycut^4 - 45*m2*MBhat^4*Ycut^4 + 45*m2^2*MBhat^4*Ycut^4 + 
          225*m2^3*MBhat^4*Ycut^4 - 180*m2*Ycut^5 + 330*m2^2*Ycut^5 - 
          120*m2^3*Ycut^5 - 30*m2^4*Ycut^5 - 84*MBhat*Ycut^5 - 
          744*m2*MBhat*Ycut^5 + 396*m2^2*MBhat*Ycut^5 + 456*m2^3*MBhat*
           Ycut^5 - 120*m2^4*MBhat*Ycut^5 - 225*MBhat^2*Ycut^5 + 
          795*m2*MBhat^2*Ycut^5 - 975*m2^2*MBhat^2*Ycut^5 + 
          1065*m2^3*MBhat^2*Ycut^5 - 280*m2^4*MBhat^2*Ycut^5 + 
          36*MBhat^3*Ycut^5 + 36*m2*MBhat^3*Ycut^5 + 1296*m2^2*MBhat^3*
           Ycut^5 - 504*m2^3*MBhat^3*Ycut^5 + 135*MBhat^4*Ycut^5 + 
          90*m2*MBhat^4*Ycut^5 - 225*m2^2*MBhat^4*Ycut^5 + 30*Ycut^6 + 
          330*m2*Ycut^6 - 300*m2^2*Ycut^6 - 60*m2^3*Ycut^6 + 
          252*MBhat*Ycut^6 + 468*m2*MBhat*Ycut^6 - 216*m2^2*MBhat*Ycut^6 - 
          120*m2^3*MBhat*Ycut^6 + 215*MBhat^2*Ycut^6 - 760*m2*MBhat^2*
           Ycut^6 + 35*m2^2*MBhat^2*Ycut^6 - 170*m2^3*MBhat^2*Ycut^6 - 
          108*MBhat^3*Ycut^6 - 72*m2*MBhat^3*Ycut^6 - 216*m2^2*MBhat^3*
           Ycut^6 - 135*MBhat^4*Ycut^6 - 45*m2*MBhat^4*Ycut^6 - 90*Ycut^7 - 
          240*m2*Ycut^7 + 60*m2^2*Ycut^7 - 252*MBhat*Ycut^7 + 
          96*m2*MBhat*Ycut^7 + 60*m2^2*MBhat*Ycut^7 - 45*MBhat^2*Ycut^7 + 
          215*m2*MBhat^2*Ycut^7 + 10*m2^2*MBhat^2*Ycut^7 + 
          108*MBhat^3*Ycut^7 + 36*m2*MBhat^3*Ycut^7 + 45*MBhat^4*Ycut^7 + 
          90*Ycut^8 + 30*m2*Ycut^8 + 84*MBhat*Ycut^8 - 60*m2*MBhat*Ycut^8 - 
          30*MBhat^2*Ycut^8 - 10*m2*MBhat^2*Ycut^8 - 36*MBhat^3*Ycut^8 - 
          30*Ycut^9 + 10*MBhat^2*Ycut^9))/(90*(-1 + Ycut)^4) + 
       (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 72*m2*MBhat + 
          12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 9*m2*MBhat^4)*
         Log[m2])/3 - (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 
          72*m2*MBhat + 12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 
          9*m2*MBhat^4)*Log[1 - Ycut])/3) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(-90 - 1230*m2 + 2400*m2^2 - 720*m2^3 - 
          390*m2^4 + 30*m2^5 + 384*MBhat + 504*m2*MBhat - 5616*m2^2*MBhat + 
          2544*m2^3*MBhat - 816*m2^4*MBhat + 120*m2^5*MBhat - 665*MBhat^2 + 
          2470*m2*MBhat^2 - 1250*m2^2*MBhat^2 + 910*m2^3*MBhat^2 - 
          1145*m2^4*MBhat^2 + 280*m2^5*MBhat^2 + 504*MBhat^3 - 
          2016*m2*MBhat^3 + 3024*m2^2*MBhat^3 - 2016*m2^3*MBhat^3 + 
          504*m2^4*MBhat^3 - 135*MBhat^4 + 225*m2*MBhat^4 - 
          855*m2^2*MBhat^4 + 225*m2^3*MBhat^4 + 450*Ycut + 6780*m2*Ycut - 
          12960*m2^2*Ycut + 3600*m2^3*Ycut + 2310*m2^4*Ycut - 180*m2^5*Ycut - 
          1920*MBhat*Ycut - 3576*m2*MBhat*Ycut + 31848*m2^2*MBhat*Ycut - 
          14568*m2^3*MBhat*Ycut + 4776*m2^4*MBhat*Ycut - 
          720*m2^5*MBhat*Ycut + 3325*MBhat^2*Ycut - 12295*m2*MBhat^2*Ycut + 
          5355*m2^2*MBhat^2*Ycut - 4595*m2^3*MBhat^2*Ycut + 
          6590*m2^4*MBhat^2*Ycut - 1680*m2^5*MBhat^2*Ycut - 
          2520*MBhat^3*Ycut + 10584*m2*MBhat^3*Ycut - 16632*m2^2*MBhat^3*
           Ycut + 11592*m2^3*MBhat^3*Ycut - 3024*m2^4*MBhat^3*Ycut + 
          675*MBhat^4*Ycut - 1260*m2*MBhat^4*Ycut + 4905*m2^2*MBhat^4*Ycut - 
          1350*m2^3*MBhat^4*Ycut - 900*Ycut^2 - 15180*m2*Ycut^2 + 
          28290*m2^2*Ycut^2 - 6990*m2^3*Ycut^2 - 5670*m2^4*Ycut^2 + 
          450*m2^5*Ycut^2 + 3840*MBhat*Ycut^2 + 9984*m2*MBhat*Ycut^2 - 
          73728*m2^2*MBhat*Ycut^2 + 34104*m2^3*MBhat*Ycut^2 - 
          11520*m2^4*MBhat*Ycut^2 + 1800*m2^5*MBhat*Ycut^2 - 
          6650*MBhat^2*Ycut^2 + 24120*m2*MBhat^2*Ycut^2 - 
          7515*m2^2*MBhat^2*Ycut^2 + 8740*m2^3*MBhat^2*Ycut^2 - 
          15495*m2^4*MBhat^2*Ycut^2 + 4200*m2^5*MBhat^2*Ycut^2 + 
          5040*MBhat^3*Ycut^2 - 22176*m2*MBhat^3*Ycut^2 + 
          36792*m2^2*MBhat^3*Ycut^2 - 27216*m2^3*MBhat^3*Ycut^2 + 
          7560*m2^4*MBhat^3*Ycut^2 - 1350*MBhat^4*Ycut^2 + 
          2790*m2*MBhat^4*Ycut^2 - 11475*m2^2*MBhat^4*Ycut^2 + 
          3375*m2^3*MBhat^4*Ycut^2 + 900*Ycut^3 + 17400*m2*Ycut^3 - 
          31350*m2^2*Ycut^3 + 6300*m2^3*Ycut^3 + 7350*m2^4*Ycut^3 - 
          600*m2^5*Ycut^3 - 3840*MBhat*Ycut^3 - 14016*m2*MBhat*Ycut^3 + 
          87936*m2^2*MBhat*Ycut^3 - 41160*m2^3*MBhat*Ycut^3 + 
          14520*m2^4*MBhat*Ycut^3 - 2400*m2^5*MBhat*Ycut^3 + 
          6890*MBhat^2*Ycut^3 - 22810*m2*MBhat^2*Ycut^3 + 
          1895*m2^2*MBhat^2*Ycut^3 - 9065*m2^3*MBhat^2*Ycut^3 + 
          20140*m2^4*MBhat^2*Ycut^3 - 5600*m2^5*MBhat^2*Ycut^3 - 
          5220*MBhat^3*Ycut^3 + 22764*m2*MBhat^3*Ycut^3 - 
          42324*m2^2*MBhat^3*Ycut^3 + 34860*m2^3*MBhat^3*Ycut^3 - 
          10080*m2^4*MBhat^3*Ycut^3 + 1350*MBhat^4*Ycut^3 - 
          3060*m2*MBhat^4*Ycut^3 + 14445*m2^2*MBhat^4*Ycut^3 - 
          4500*m2^3*MBhat^4*Ycut^3 - 450*Ycut^4 - 10410*m2*Ycut^4 + 
          17820*m2^2*Ycut^4 - 2160*m2^3*Ycut^4 - 5250*m2^4*Ycut^4 + 
          450*m2^5*Ycut^4 + 1680*MBhat*Ycut^4 + 10344*m2*MBhat*Ycut^4 - 
          56160*m2^2*MBhat*Ycut^4 + 28560*m2^3*MBhat*Ycut^4 - 
          11280*m2^4*MBhat*Ycut^4 + 1800*m2^5*MBhat*Ycut^4 - 
          4360*MBhat^2*Ycut^4 + 9430*m2*MBhat^2*Ycut^4 + 7455*m2^2*MBhat^2*
           Ycut^4 + 3040*m2^3*MBhat^2*Ycut^4 - 14545*m2^4*MBhat^2*Ycut^4 + 
          4200*m2^5*MBhat^2*Ycut^4 + 3420*MBhat^3*Ycut^4 - 
          10176*m2*MBhat^3*Ycut^4 + 25620*m2^2*MBhat^3*Ycut^4 - 
          25560*m2^3*MBhat^3*Ycut^4 + 7560*m2^4*MBhat^3*Ycut^4 - 
          690*MBhat^4*Ycut^4 + 1650*m2*MBhat^4*Ycut^4 - 10545*m2^2*MBhat^4*
           Ycut^4 + 3375*m2^3*MBhat^4*Ycut^4 + 162*Ycut^5 + 2712*m2*Ycut^5 - 
          3948*m2^2*Ycut^5 - 1068*m2^3*Ycut^5 + 2322*m2^4*Ycut^5 - 
          180*m2^5*Ycut^5 + 768*MBhat*Ycut^5 - 4008*m2*MBhat*Ycut^5 + 
          17412*m2^2*MBhat*Ycut^5 - 11328*m2^3*MBhat*Ycut^5 + 
          4932*m2^4*MBhat*Ycut^5 - 720*m2^5*MBhat*Ycut^5 + 
          2240*MBhat^2*Ycut^5 - 30*m2*MBhat^2*Ycut^5 - 11835*m2^2*MBhat^2*
           Ycut^5 + 3625*m2^3*MBhat^2*Ycut^5 + 5370*m2^4*MBhat^2*Ycut^5 - 
          1680*m2^5*MBhat^2*Ycut^5 - 2298*MBhat^3*Ycut^5 - 
          954*m2*MBhat^3*Ycut^5 - 5394*m2^2*MBhat^3*Ycut^5 + 
          9846*m2^3*MBhat^3*Ycut^5 - 3024*m2^4*MBhat^3*Ycut^5 + 
          210*MBhat^4*Ycut^5 - 300*m2*MBhat^4*Ycut^5 + 4275*m2^2*MBhat^4*
           Ycut^5 - 1350*m2^3*MBhat^4*Ycut^5 - 357*Ycut^6 + 195*m2*Ycut^6 - 
          783*m2^2*Ycut^6 + 1449*m2^3*Ycut^6 - 534*m2^4*Ycut^6 + 
          30*m2^5*Ycut^6 - 2160*MBhat*Ycut^6 + 1392*m2*MBhat*Ycut^6 - 
          996*m2^2*MBhat*Ycut^6 + 1716*m2^3*MBhat*Ycut^6 - 
          1032*m2^4*MBhat*Ycut^6 + 120*m2^5*MBhat*Ycut^6 - 
          751*MBhat^2*Ycut^6 - 1141*m2*MBhat^2*Ycut^6 + 8174*m2^2*MBhat^2*
           Ycut^6 - 3851*m2^3*MBhat^2*Ycut^6 - 671*m2^4*MBhat^2*Ycut^6 + 
          280*m2^5*MBhat^2*Ycut^6 + 1770*MBhat^3*Ycut^6 + 
          3096*m2*MBhat^3*Ycut^6 - 2298*m2^2*MBhat^3*Ycut^6 - 
          1452*m2^3*MBhat^3*Ycut^6 + 504*m2^4*MBhat^3*Ycut^6 - 
          150*MBhat^4*Ycut^6 - 90*m2*MBhat^4*Ycut^6 - 765*m2^2*MBhat^4*
           Ycut^6 + 225*m2^3*MBhat^4*Ycut^6 + 705*Ycut^7 - 540*m2*Ycut^7 + 
          657*m2^2*Ycut^7 - 414*m2^3*Ycut^7 + 42*m2^4*Ycut^7 + 
          1920*MBhat*Ycut^7 - 1008*m2*MBhat*Ycut^7 - 924*m2^2*MBhat*Ycut^7 + 
          192*m2^3*MBhat*Ycut^7 + 60*m2^4*MBhat*Ycut^7 - 445*MBhat^2*Ycut^7 + 
          274*m2*MBhat^2*Ycut^7 - 2532*m2^2*MBhat^2*Ycut^7 + 
          1357*m2^3*MBhat^2*Ycut^7 - 64*m2^4*MBhat^2*Ycut^7 - 
          840*MBhat^3*Ycut^7 - 1344*m2*MBhat^3*Ycut^7 + 1398*m2^2*MBhat^3*
           Ycut^7 - 54*m2^3*MBhat^3*Ycut^7 + 150*MBhat^4*Ycut^7 + 
          60*m2*MBhat^4*Ycut^7 + 15*m2^2*MBhat^4*Ycut^7 - 690*Ycut^8 + 
          390*m2*Ycut^8 - 123*m2^2*Ycut^8 + 3*m2^3*Ycut^8 - 
          720*MBhat*Ycut^8 + 432*m2*MBhat*Ycut^8 + 228*m2^2*MBhat*Ycut^8 - 
          60*m2^3*MBhat*Ycut^8 + 575*MBhat^2*Ycut^8 - 6*m2*MBhat^2*Ycut^8 + 
          252*m2^2*MBhat^2*Ycut^8 - 161*m2^3*MBhat^2*Ycut^8 + 
          120*MBhat^3*Ycut^8 + 216*m2*MBhat^3*Ycut^8 - 186*m2^2*MBhat^3*
           Ycut^8 - 75*MBhat^4*Ycut^8 - 15*m2*MBhat^4*Ycut^8 + 330*Ycut^9 - 
          120*m2*Ycut^9 - 3*m2^2*Ycut^9 - 48*m2*MBhat*Ycut^9 - 
          155*MBhat^2*Ycut^9 - 11*m2*MBhat^2*Ycut^9 + m2^2*MBhat^2*Ycut^9 + 
          30*MBhat^3*Ycut^9 + 6*m2*MBhat^3*Ycut^9 + 15*MBhat^4*Ycut^9 - 
          57*Ycut^10 + 3*m2*Ycut^10 + 48*MBhat*Ycut^10 - 5*MBhat^2*Ycut^10 - 
          m2*MBhat^2*Ycut^10 - 6*MBhat^3*Ycut^10 - 3*Ycut^11 + 
          MBhat^2*Ycut^11))/(90*(-1 + Ycut)^6) + 
       (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 72*m2*MBhat + 
          12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 9*m2*MBhat^4)*
         Log[m2])/3 - (2*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 
          72*m2*MBhat + 12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 
          9*m2*MBhat^4)*Log[1 - Ycut])/3) + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(-345 - 7845*m2 - 1170*m2^2 - 2370*m2^3 - 
          945*m2^4 + 75*m2^5 + 1824*MBhat + 17760*m2*MBhat - 
          9360*m2^2*MBhat + 6000*m2^3*MBhat - 2160*m2^4*MBhat + 
          336*m2^5*MBhat - 4130*MBhat^2 - 11360*m2*MBhat^2 - 
          1130*m2^2*MBhat^2 + 1750*m2^3*MBhat^2 - 3440*m2^4*MBhat^2 + 
          910*m2^5*MBhat^2 + 3744*MBhat^3 + 3024*m2*MBhat^3 + 
          5904*m2^2*MBhat^3 - 5616*m2^3*MBhat^3 + 1584*m2^4*MBhat^3 - 
          1125*MBhat^4 - 1125*m2*MBhat^4 - 2205*m2^2*MBhat^4 + 
          675*m2^3*MBhat^4 + 1725*Ycut + 42480*m2*Ycut + 7560*m2^2*Ycut + 
          12390*m2^3*Ycut + 5595*m2^4*Ycut - 450*m2^5*Ycut - 
          9120*MBhat*Ycut - 98496*m2*MBhat*Ycut + 51984*m2^2*MBhat*Ycut - 
          34176*m2^3*MBhat*Ycut + 12624*m2^4*MBhat*Ycut - 
          2016*m2^5*MBhat*Ycut + 20650*MBhat^2*Ycut + 67070*m2*MBhat^2*Ycut + 
          1680*m2^2*MBhat^2*Ycut - 7970*m2^3*MBhat^2*Ycut + 
          19730*m2^4*MBhat^2*Ycut - 5460*m2^5*MBhat^2*Ycut - 
          18720*MBhat^3*Ycut - 20016*m2*MBhat^3*Ycut - 31392*m2^2*MBhat^3*
           Ycut + 32112*m2^3*MBhat^3*Ycut - 9504*m2^4*MBhat^3*Ycut + 
          5625*MBhat^4*Ycut + 6660*m2*MBhat^4*Ycut + 12555*m2^2*MBhat^4*
           Ycut - 4050*m2^3*MBhat^4*Ycut - 3450*Ycut^2 - 93270*m2*Ycut^2 - 
          20235*m2^2*Ycut^2 - 25845*m2^3*Ycut^2 - 13725*m2^4*Ycut^2 + 
          1125*m2^5*Ycut^2 + 18240*MBhat*Ycut^2 + 222144*m2*MBhat*Ycut^2 - 
          116832*m2^2*MBhat*Ycut^2 + 79392*m2^3*MBhat*Ycut^2 - 
          30384*m2^4*MBhat*Ycut^2 + 5040*m2^5*MBhat*Ycut^2 - 
          41300*MBhat^2*Ycut^2 - 161880*m2*MBhat^2*Ycut^2 + 
          9090*m2^2*MBhat^2*Ycut^2 + 11980*m2^3*MBhat^2*Ycut^2 - 
          46140*m2^4*MBhat^2*Ycut^2 + 13650*m2^5*MBhat^2*Ycut^2 + 
          37440*MBhat^3*Ycut^2 + 54144*m2*MBhat^3*Ycut^2 + 
          65952*m2^2*MBhat^3*Ycut^2 - 74736*m2^3*MBhat^3*Ycut^2 + 
          23760*m2^4*MBhat^3*Ycut^2 - 11250*MBhat^4*Ycut^2 - 
          16470*m2*MBhat^4*Ycut^2 - 29025*m2^2*MBhat^4*Ycut^2 + 
          10125*m2^3*MBhat^4*Ycut^2 + 3450*Ycut^3 + 104580*m2*Ycut^3 + 
          28545*m2^2*Ycut^3 + 26700*m2^3*Ycut^3 + 17775*m2^4*Ycut^3 - 
          1500*m2^5*Ycut^3 - 18240*MBhat*Ycut^3 - 256896*m2*MBhat*Ycut^3 + 
          133152*m2^2*MBhat*Ycut^3 - 94656*m2^3*MBhat*Ycut^3 + 
          38160*m2^4*MBhat*Ycut^3 - 6720*m2^5*MBhat*Ycut^3 + 
          41420*MBhat^2*Ycut^3 + 204620*m2*MBhat^2*Ycut^3 - 
          34330*m2^2*MBhat^2*Ycut^3 - 2150*m2^3*MBhat^2*Ycut^3 + 
          56590*m2^4*MBhat^2*Ycut^3 - 18200*m2^5*MBhat^2*Ycut^3 - 
          37500*MBhat^3*Ycut^3 - 78996*m2*MBhat^3*Ycut^3 - 
          65604*m2^2*MBhat^3*Ycut^3 + 90660*m2^3*MBhat^3*Ycut^3 - 
          31680*m2^4*MBhat^3*Ycut^3 + 11250*MBhat^4*Ycut^3 + 
          22140*m2*MBhat^4*Ycut^3 + 34695*m2^2*MBhat^4*Ycut^3 - 
          13500*m2^3*MBhat^4*Ycut^3 - 1725*Ycut^4 - 60945*m2*Ycut^4 - 
          22050*m2^2*Ycut^4 - 13350*m2^3*Ycut^4 - 12675*m2^4*Ycut^4 + 
          1125*m2^5*Ycut^4 + 9960*MBhat*Ycut^4 + 151944*m2*MBhat*Ycut^4 - 
          74184*m2^2*MBhat*Ycut^4 + 59640*m2^3*MBhat*Ycut^4 - 
          27120*m2^4*MBhat*Ycut^4 + 5040*m2^5*MBhat*Ycut^4 - 
          21835*MBhat^2*Ycut^4 - 141185*m2*MBhat^2*Ycut^4 + 
          51915*m2^2*MBhat^2*Ycut^4 - 17555*m2^3*MBhat^2*Ycut^4 - 
          36370*m2^4*MBhat^2*Ycut^4 + 13650*m2^5*MBhat^2*Ycut^4 + 
          19020*MBhat^3*Ycut^4 + 68184*m2*MBhat^3*Ycut^4 + 
          22740*m2^2*MBhat^3*Ycut^4 - 57960*m2^3*MBhat^3*Ycut^4 + 
          23760*m2^4*MBhat^3*Ycut^4 - 5730*MBhat^4*Ycut^4 - 
          17430*m2*MBhat^4*Ycut^4 - 21795*m2^2*MBhat^4*Ycut^4 + 
          10125*m2^3*MBhat^4*Ycut^4 - 3*Ycut^5 + 16812*m2*Ycut^5 + 
          7722*m2^2*Ycut^5 + 1812*m2^3*Ycut^5 + 4977*m2^4*Ycut^5 - 
          450*m2^5*Ycut^5 - 5952*MBhat*Ycut^5 - 30180*m2*MBhat*Ycut^5 + 
          9396*m2^2*MBhat*Ycut^5 - 16284*m2^3*MBhat*Ycut^5 + 
          9756*m2^4*MBhat*Ycut^5 - 2016*m2^5*MBhat*Ycut^5 + 
          8255*MBhat^2*Ycut^5 + 47250*m2*MBhat^2*Ycut^5 - 
          43935*m2^2*MBhat^2*Ycut^5 + 26350*m2^3*MBhat^2*Ycut^5 + 
          9690*m2^4*MBhat^2*Ycut^5 - 5460*m2^5*MBhat^2*Ycut^5 - 
          4278*MBhat^3*Ycut^5 - 35814*m2*MBhat^3*Ycut^5 + 
          13566*m2^2*MBhat^3*Ycut^5 + 15246*m2^3*MBhat^3*Ycut^5 - 
          9504*m2^4*MBhat^3*Ycut^5 + 1650*MBhat^4*Ycut^5 + 
          7980*m2*MBhat^4*Ycut^5 + 5625*m2^2*MBhat^4*Ycut^5 - 
          4050*m2^3*MBhat^4*Ycut^5 + 1788*Ycut^6 - 4440*m2*Ycut^6 + 
          582*m2^2*Ycut^6 + 1014*m2^3*Ycut^6 - 819*m2^4*Ycut^6 + 
          75*m2^5*Ycut^6 + 8040*MBhat*Ycut^6 - 13428*m2*MBhat*Ycut^6 + 
          9648*m2^2*MBhat*Ycut^6 - 1596*m2^3*MBhat*Ycut^6 - 
          1080*m2^4*MBhat*Ycut^6 + 336*m2^5*MBhat*Ycut^6 - 
          7066*MBhat^2*Ycut^6 - 3286*m2*MBhat^2*Ycut^6 + 22529*m2^2*MBhat^2*
           Ycut^6 - 17141*m2^3*MBhat^2*Ycut^6 + 814*m2^4*MBhat^2*Ycut^6 + 
          910*m2^5*MBhat^2*Ycut^6 + 270*MBhat^3*Ycut^6 + 
          11736*m2*MBhat^3*Ycut^6 - 16458*m2^2*MBhat^3*Ycut^6 + 
          1428*m2^3*MBhat^3*Ycut^6 + 1584*m2^4*MBhat^3*Ycut^6 - 
          1050*MBhat^4*Ycut^6 - 2070*m2*MBhat^4*Ycut^6 + 
          585*m2^2*MBhat^4*Ycut^6 + 675*m2^3*MBhat^4*Ycut^6 - 3720*Ycut^7 + 
          4800*m2*Ycut^7 - 918*m2^2*Ycut^7 - 264*m2^3*Ycut^7 - 
          3*m2^4*Ycut^7 - 7680*MBhat*Ycut^7 + 8952*m2*MBhat*Ycut^7 - 
          4440*m2^2*MBhat*Ycut^7 + 2004*m2^3*MBhat*Ycut^7 - 
          156*m2^4*MBhat*Ycut^7 + 6530*MBhat^2*Ycut^7 - 596*m2*MBhat^2*
           Ycut^7 - 6507*m2^2*MBhat^2*Ycut^7 + 5392*m2^3*MBhat^2*Ycut^7 - 
          694*m2^4*MBhat^2*Ycut^7 + 360*MBhat^3*Ycut^7 - 
          2664*m2*MBhat^3*Ycut^7 + 6078*m2^2*MBhat^3*Ycut^7 - 
          1134*m2^3*MBhat^3*Ycut^7 + 1050*MBhat^4*Ycut^7 + 
          420*m2*MBhat^4*Ycut^7 - 435*m2^2*MBhat^4*Ycut^7 + 3960*Ycut^8 - 
          2580*m2*Ycut^8 - 123*m2^2*Ycut^8 - 87*m2^3*Ycut^8 + 
          3480*MBhat*Ycut^8 - 2208*m2*MBhat*Ycut^8 + 552*m2^2*MBhat*Ycut^8 - 
          324*m2^3*MBhat*Ycut^8 - 3205*MBhat^2*Ycut^8 - 921*m2*MBhat^2*
           Ycut^8 + 672*m2^2*MBhat^2*Ycut^8 - 656*m2^3*MBhat^2*Ycut^8 - 
          600*MBhat^3*Ycut^8 + 336*m2*MBhat^3*Ycut^8 - 786*m2^2*MBhat^3*
           Ycut^8 - 525*MBhat^4*Ycut^8 - 105*m2*MBhat^4*Ycut^8 - 
          2220*Ycut^9 + 360*m2*Ycut^9 + 87*m2^2*Ycut^9 - 480*MBhat*Ycut^9 + 
          492*m2*MBhat*Ycut^9 + 84*m2^2*MBhat*Ycut^9 + 745*MBhat^2*Ycut^9 + 
          304*m2*MBhat^2*Ycut^9 + 16*m2^2*MBhat^2*Ycut^9 + 
          330*MBhat^3*Ycut^9 + 66*m2*MBhat^3*Ycut^9 + 105*MBhat^4*Ycut^9 + 
          588*Ycut^10 + 48*m2*Ycut^10 - 72*MBhat*Ycut^10 - 
          84*m2*MBhat*Ycut^10 - 80*MBhat^2*Ycut^10 - 16*m2*MBhat^2*Ycut^10 - 
          66*MBhat^3*Ycut^10 - 48*Ycut^11 + 16*MBhat^2*Ycut^11))/
        (45*(-1 + Ycut)^6) + (8*m2*(60 + 105*m2 + 45*m2^3 - 192*MBhat - 
          48*m2*MBhat + 240*MBhat^2 - 48*m2*MBhat^2 + 98*m2^2*MBhat^2 - 
          144*MBhat^3 + 36*MBhat^4 + 27*m2*MBhat^4)*Log[m2])/3 - 
       (8*m2*(60 + 105*m2 + 45*m2^3 - 192*MBhat - 48*m2*MBhat + 240*MBhat^2 - 
          48*m2*MBhat^2 + 98*m2^2*MBhat^2 - 144*MBhat^3 + 36*MBhat^4 + 
          27*m2*MBhat^4)*Log[1 - Ycut])/3) + 
     c[VR]*(-1/15*(Sqrt[m2]*(-1 + m2 + Ycut)*(391 - 574*m2 - 2514*m2^2 + 
           146*m2^3 + 31*m2^4 - 1288*MBhat + 4512*m2*MBhat + 
           3072*m2^2*MBhat - 608*m2^3*MBhat + 72*m2^4*MBhat + 1395*MBhat^2 - 
           5960*m2*MBhat^2 + 1320*m2^2*MBhat^2 - 480*m2^3*MBhat^2 + 
           125*m2^4*MBhat^2 - 640*MBhat^3 + 2480*m2*MBhat^3 - 
           1120*m2^2*MBhat^3 + 240*m2^3*MBhat^3 + 130*MBhat^4 - 
           140*m2*MBhat^4 + 130*m2^2*MBhat^4 - 1293*Ycut + 1693*m2*Ycut + 
           8979*m2^2*Ycut - 435*m2^3*Ycut - 124*m2^4*Ycut + 4344*MBhat*Ycut - 
           14824*m2*MBhat*Ycut - 11752*m2^2*MBhat*Ycut + 2360*m2^3*MBhat*
            Ycut - 288*m2^4*MBhat*Ycut - 4905*MBhat^2*Ycut + 
           20595*m2*MBhat^2*Ycut - 4385*m2^2*MBhat^2*Ycut + 
           1795*m2^3*MBhat^2*Ycut - 500*m2^4*MBhat^2*Ycut + 
           2400*MBhat^3*Ycut - 9040*m2*MBhat^3*Ycut + 4240*m2^2*MBhat^3*
            Ycut - 960*m2^3*MBhat^3*Ycut - 510*MBhat^4*Ycut + 
           610*m2*MBhat^4*Ycut - 520*m2^2*MBhat^4*Ycut + 1473*Ycut^2 - 
           1574*m2*Ycut^2 - 11375*m2^2*Ycut^2 + 370*m2^3*Ycut^2 + 
           186*m2^4*Ycut^2 - 5064*MBhat*Ycut^2 + 16592*m2*MBhat*Ycut^2 + 
           16360*m2^2*MBhat*Ycut^2 - 3360*m2^3*MBhat*Ycut^2 + 
           432*m2^4*MBhat*Ycut^2 + 5985*MBhat^2*Ycut^2 - 24690*m2*MBhat^2*
            Ycut^2 + 4735*m2^2*MBhat^2*Ycut^2 - 2380*m2^3*MBhat^2*Ycut^2 + 
           750*m2^4*MBhat^2*Ycut^2 - 3120*MBhat^3*Ycut^2 + 
           11600*m2*MBhat^3*Ycut^2 - 5760*m2^2*MBhat^3*Ycut^2 + 
           1440*m2^3*MBhat^3*Ycut^2 + 690*MBhat^4*Ycut^2 - 
           950*m2*MBhat^4*Ycut^2 + 780*m2^2*MBhat^4*Ycut^2 - 611*Ycut^3 + 
           375*m2*Ycut^3 + 5600*m2^2*Ycut^3 + 10*m2^3*Ycut^3 - 
           124*m2^4*Ycut^3 + 2168*MBhat*Ycut^3 - 6520*m2*MBhat*Ycut^3 - 
           9360*m2^2*MBhat*Ycut^3 + 2000*m2^3*MBhat*Ycut^3 - 
           288*m2^4*MBhat*Ycut^3 - 2495*MBhat^2*Ycut^3 + 10595*m2*MBhat^2*
            Ycut^3 - 1450*m2^2*MBhat^2*Ycut^3 + 1350*m2^3*MBhat^2*Ycut^3 - 
           500*m2^4*MBhat^2*Ycut^3 + 1340*MBhat^3*Ycut^3 - 
           5720*m2*MBhat^3*Ycut^3 + 3340*m2^2*MBhat^3*Ycut^3 - 
           960*m2^3*MBhat^3*Ycut^3 - 350*MBhat^4*Ycut^3 + 
           680*m2*MBhat^4*Ycut^3 - 520*m2^2*MBhat^4*Ycut^3 + 30*Ycut^4 + 
           65*m2*Ycut^4 - 525*m2^2*Ycut^4 - 105*m2^3*Ycut^4 + 
           31*m2^4*Ycut^4 - 220*MBhat*Ycut^4 + 180*m2*MBhat*Ycut^4 + 
           1620*m2^2*MBhat*Ycut^4 - 500*m2^3*MBhat*Ycut^4 + 
           72*m2^4*MBhat*Ycut^4 - 310*MBhat^2*Ycut^4 + 435*m2*MBhat^2*
            Ycut^4 - 795*m2^2*MBhat^2*Ycut^4 - 175*m2^3*MBhat^2*Ycut^4 + 
           125*m2^4*MBhat^2*Ycut^4 + 300*MBhat^3*Ycut^4 + 
           340*m2*MBhat^3*Ycut^4 - 640*m2^2*MBhat^3*Ycut^4 + 
           240*m2^3*MBhat^3*Ycut^4 + 60*MBhat^4*Ycut^4 - 250*m2*MBhat^4*
            Ycut^4 + 130*m2^2*MBhat^4*Ycut^4 + 24*Ycut^5 + 29*m2*Ycut^5 - 
           196*m2^2*Ycut^5 + 59*m2^3*Ycut^5 + 178*MBhat*Ycut^5 - 
           122*m2*MBhat*Ycut^5 + 118*m2^2*MBhat*Ycut^5 + 18*m2^3*MBhat*
            Ycut^5 + 330*MBhat^2*Ycut^5 - 1095*m2*MBhat^2*Ycut^5 + 
           630*m2^2*MBhat^2*Ycut^5 - 65*m2^3*MBhat^2*Ycut^5 - 
           300*MBhat^3*Ycut^5 + 400*m2*MBhat^3*Ycut^5 - 60*m2^2*MBhat^3*
            Ycut^5 - 60*MBhat^4*Ycut^5 + 50*m2*MBhat^4*Ycut^5 - 32*Ycut^6 - 
           23*m2*Ycut^6 + 31*m2^2*Ycut^6 - 94*MBhat*Ycut^6 + 
           184*m2*MBhat*Ycut^6 - 58*m2^2*MBhat*Ycut^6 - 10*MBhat^2*Ycut^6 + 
           105*m2*MBhat^2*Ycut^6 - 55*m2^2*MBhat^2*Ycut^6 + 
           20*MBhat^3*Ycut^6 - 60*m2*MBhat^3*Ycut^6 + 40*MBhat^4*Ycut^6 + 
           12*Ycut^7 + 9*m2*Ycut^7 - 26*MBhat*Ycut^7 - 2*m2*MBhat*Ycut^7 + 
           10*MBhat^2*Ycut^7 + 15*m2*MBhat^2*Ycut^7 + 6*Ycut^8 + 
           2*MBhat*Ycut^8))/(-1 + Ycut)^4 + 4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 
         18*m2^3 + 3*m2^4 - 8*MBhat + 8*m2*MBhat + 96*m2^2*MBhat + 
         12*MBhat^2 - 34*m2*MBhat^2 - 47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 - 
         8*MBhat^3 + 24*m2*MBhat^3 + 2*MBhat^4 - 3*m2*MBhat^4 + 
         3*m2^2*MBhat^4)*Log[m2] - 4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 
         3*m2^4 - 8*MBhat + 8*m2*MBhat + 96*m2^2*MBhat + 12*MBhat^2 - 
         34*m2*MBhat^2 - 47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 - 8*MBhat^3 + 
         24*m2*MBhat^3 + 2*MBhat^4 - 3*m2*MBhat^4 + 3*m2^2*MBhat^4)*
        Log[1 - Ycut]) + 
     c[SL]*(-1/30*(Ycut^3*(-1 + m2 + Ycut)*(-80*MBhat^2 - 80*m2*MBhat^2 - 
           80*m2^2*MBhat^2 + 720*m2^3*MBhat^2 - 480*m2^4*MBhat^2 + 
           60*MBhat^3 + 140*m2*MBhat^3 + 500*m2^2*MBhat^3 - 
           700*m2^3*MBhat^3 - 240*m2^2*MBhat^4 + 80*MBhat*Ycut + 
           80*m2*MBhat*Ycut + 80*m2^2*MBhat*Ycut - 720*m2^3*MBhat*Ycut + 
           480*m2^4*MBhat*Ycut + 370*MBhat^2*Ycut + 220*m2*MBhat^2*Ycut - 
           400*m2^2*MBhat^2*Ycut - 1180*m2^3*MBhat^2*Ycut + 
           990*m2^4*MBhat^2*Ycut - 300*MBhat^3*Ycut - 640*m2*MBhat^3*Ycut - 
           1340*m2^2*MBhat^3*Ycut + 1800*m2^3*MBhat^3*Ycut - 
           10*MBhat^4*Ycut - 10*m2*MBhat^4*Ycut + 740*m2^2*MBhat^4*Ycut - 
           24*Ycut^2 - 24*m2*Ycut^2 - 24*m2^2*Ycut^2 + 216*m2^3*Ycut^2 - 
           144*m2^4*Ycut^2 - 412*MBhat*Ycut^2 - 312*m2*MBhat*Ycut^2 + 
           128*m2^2*MBhat*Ycut^2 + 1208*m2^3*MBhat*Ycut^2 - 
           612*m2^4*MBhat*Ycut^2 - 650*MBhat^2*Ycut^2 - 10*m2*MBhat^2*
            Ycut^2 + 1390*m2^2*MBhat^2*Ycut^2 + 570*m2^3*MBhat^2*Ycut^2 - 
           900*m2^4*MBhat^2*Ycut^2 + 610*MBhat^3*Ycut^2 + 
           1170*m2*MBhat^3*Ycut^2 + 1270*m2^2*MBhat^3*Ycut^2 - 
           1770*m2^3*MBhat^3*Ycut^2 + 50*MBhat^4*Ycut^2 + 
           40*m2*MBhat^4*Ycut^2 - 840*m2^2*MBhat^4*Ycut^2 + 129*Ycut^3 + 
           105*m2*Ycut^3 - 9*m2^2*Ycut^3 - 333*m2^3*Ycut^3 + 
           108*m2^4*Ycut^3 + 860*MBhat*Ycut^3 + 428*m2*MBhat*Ycut^3 - 
           644*m2^2*MBhat*Ycut^3 - 636*m2^3*MBhat*Ycut^3 + 
           312*m2^4*MBhat*Ycut^3 + 497*MBhat^2*Ycut^3 - 563*m2*MBhat^2*
            Ycut^3 - 1333*m2^2*MBhat^2*Ycut^3 - 123*m2^3*MBhat^2*Ycut^3 + 
           402*m2^4*MBhat^2*Ycut^3 - 650*MBhat^3*Ycut^3 - 
           1080*m2*MBhat^3*Ycut^3 - 530*m2^2*MBhat^3*Ycut^3 + 
           820*m2^3*MBhat^3*Ycut^3 - 100*MBhat^4*Ycut^3 - 
           60*m2*MBhat^4*Ycut^3 + 420*m2^2*MBhat^4*Ycut^3 - 285*Ycut^4 - 
           180*m2*Ycut^4 + 111*m2^2*Ycut^4 + 138*m2^3*Ycut^4 - 
           24*m2^4*Ycut^4 - 920*MBhat*Ycut^4 - 192*m2*MBhat*Ycut^4 + 
           604*m2^2*MBhat*Ycut^4 + 168*m2^3*MBhat*Ycut^4 - 
           60*m2^4*MBhat*Ycut^4 - 85*MBhat^2*Ycut^4 + 752*m2*MBhat^2*Ycut^4 + 
           499*m2^2*MBhat^2*Ycut^4 + 16*m2^3*MBhat^2*Ycut^4 - 
           72*m2^4*MBhat^2*Ycut^4 + 400*MBhat^3*Ycut^4 + 520*m2*MBhat^3*
            Ycut^4 + 110*m2^2*MBhat^3*Ycut^4 - 150*m2^3*MBhat^3*Ycut^4 + 
           100*MBhat^4*Ycut^4 + 40*m2*MBhat^4*Ycut^4 - 80*m2^2*MBhat^4*
            Ycut^4 + 330*Ycut^5 + 150*m2*Ycut^5 - 99*m2^2*Ycut^5 - 
           21*m2^3*Ycut^5 + 520*MBhat*Ycut^5 - 72*m2*MBhat*Ycut^5 - 
           188*m2^2*MBhat*Ycut^5 - 20*m2^3*MBhat*Ycut^5 - 
           100*MBhat^2*Ycut^5 - 398*m2*MBhat^2*Ycut^5 - 79*m2^2*MBhat^2*
            Ycut^5 - 3*m2^3*MBhat^2*Ycut^5 - 160*MBhat^3*Ycut^5 - 
           120*m2*MBhat^3*Ycut^5 - 10*m2^2*MBhat^3*Ycut^5 - 
           50*MBhat^4*Ycut^5 - 10*m2*MBhat^4*Ycut^5 - 210*Ycut^6 - 
           60*m2*Ycut^6 + 21*m2^2*Ycut^6 - 140*MBhat*Ycut^6 + 
           88*m2*MBhat*Ycut^6 + 20*m2^2*MBhat*Ycut^6 + 60*MBhat^2*Ycut^6 + 
           82*m2*MBhat^2*Ycut^6 + 3*m2^2*MBhat^2*Ycut^6 + 50*MBhat^3*Ycut^6 + 
           10*m2*MBhat^3*Ycut^6 + 10*MBhat^4*Ycut^6 + 69*Ycut^7 + 
           9*m2*Ycut^7 + 12*MBhat*Ycut^7 - 20*m2*MBhat*Ycut^7 - 
           15*MBhat^2*Ycut^7 - 3*m2*MBhat^2*Ycut^7 - 10*MBhat^3*Ycut^7 - 
           9*Ycut^8 + 3*MBhat^2*Ycut^8)*c[T])/(-1 + Ycut)^6 + 
       c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-491 - 3166*m2 - 2266*m2^2 + 
            34*m2^3 + 9*m2^4 + 1816*MBhat + 7016*m2*MBhat + 1416*m2^2*MBhat - 
            184*m2^3*MBhat + 16*m2^4*MBhat - 2475*MBhat^2 - 4980*m2*MBhat^2 + 
            300*m2^2*MBhat^2 - 60*m2^3*MBhat^2 + 15*m2^4*MBhat^2 + 
            1480*MBhat^3 + 1160*m2*MBhat^3 - 280*m2^2*MBhat^3 + 
            40*m2^3*MBhat^3 - 330*MBhat^4 - 60*m2*MBhat^4 + 30*m2^2*MBhat^4 + 
            1593*Ycut + 10867*m2*Ycut + 8241*m2^2*Ycut - 85*m2^3*Ycut - 
            36*m2^4*Ycut - 5928*MBhat*Ycut - 24512*m2*MBhat*Ycut - 
            5496*m2^2*MBhat*Ycut + 720*m2^3*MBhat*Ycut - 64*m2^4*MBhat*Ycut + 
            8145*MBhat^2*Ycut + 17865*m2*MBhat^2*Ycut - 975*m2^2*MBhat^2*
             Ycut + 225*m2^3*MBhat^2*Ycut - 60*m2^4*MBhat^2*Ycut - 
            4920*MBhat^3*Ycut - 4400*m2*MBhat^3*Ycut + 1080*m2^2*MBhat^3*
             Ycut - 160*m2^3*MBhat^3*Ycut + 1110*MBhat^4*Ycut + 
            270*m2*MBhat^4*Ycut - 120*m2^2*MBhat^4*Ycut - 1773*Ycut^2 - 
            13046*m2*Ycut^2 - 10745*m2^2*Ycut^2 + 30*m2^3*Ycut^2 + 
            54*m2^4*Ycut^2 + 6648*MBhat*Ycut^2 + 30136*m2*MBhat*Ycut^2 + 
            7840*m2^2*MBhat*Ycut^2 - 1040*m2^3*MBhat*Ycut^2 + 
            96*m2^4*MBhat*Ycut^2 - 9225*MBhat^2*Ycut^2 - 22770*m2*MBhat^2*
             Ycut^2 + 1005*m2^2*MBhat^2*Ycut^2 - 300*m2^3*MBhat^2*Ycut^2 + 
            90*m2^4*MBhat^2*Ycut^2 + 5640*MBhat^3*Ycut^2 + 
            6040*m2*MBhat^3*Ycut^2 - 1520*m2^2*MBhat^3*Ycut^2 + 
            240*m2^3*MBhat^3*Ycut^2 - 1290*MBhat^4*Ycut^2 - 
            450*m2*MBhat^4*Ycut^2 + 180*m2^2*MBhat^4*Ycut^2 + 711*Ycut^3 + 
            5905*m2*Ycut^3 + 5600*m2^2*Ycut^3 + 70*m2^3*Ycut^3 - 
            36*m2^4*Ycut^3 - 2696*MBhat*Ycut^3 - 14160*m2*MBhat*Ycut^3 - 
            4720*m2^2*MBhat*Ycut^3 + 640*m2^3*MBhat*Ycut^3 - 
            64*m2^4*MBhat*Ycut^3 + 3735*MBhat^2*Ycut^3 + 11505*m2*MBhat^2*
             Ycut^3 - 390*m2^2*MBhat^2*Ycut^3 + 210*m2^3*MBhat^2*Ycut^3 - 
            60*m2^4*MBhat^2*Ycut^3 - 2260*MBhat^3*Ycut^3 - 
            3560*m2*MBhat^3*Ycut^3 + 980*m2^2*MBhat^3*Ycut^3 - 
            160*m2^3*MBhat^3*Ycut^3 + 510*MBhat^4*Ycut^3 + 
            360*m2*MBhat^4*Ycut^3 - 120*m2^2*MBhat^4*Ycut^3 - 30*Ycut^4 - 
            425*m2*Ycut^4 - 675*m2^2*Ycut^4 - 55*m2^3*Ycut^4 + 
            9*m2^4*Ycut^4 + 180*MBhat*Ycut^4 + 980*m2*MBhat*Ycut^4 + 
            1020*m2^2*MBhat*Ycut^4 - 180*m2^3*MBhat*Ycut^4 + 
            16*m2^4*MBhat*Ycut^4 - 150*MBhat^2*Ycut^4 - 1235*m2*MBhat^2*
             Ycut^4 - 5*m2^2*MBhat^2*Ycut^4 - 65*m2^3*MBhat^2*Ycut^4 + 
            15*m2^4*MBhat^2*Ycut^4 - 60*MBhat^3*Ycut^4 + 780*m2*MBhat^3*
             Ycut^4 - 280*m2^2*MBhat^3*Ycut^4 + 40*m2^3*MBhat^3*Ycut^4 + 
            60*MBhat^4*Ycut^4 - 150*m2*MBhat^4*Ycut^4 + 30*m2^2*MBhat^4*
             Ycut^4 - 24*Ycut^5 - 29*m2*Ycut^5 - 164*m2^2*Ycut^5 + 
            21*m2^3*Ycut^5 - 66*MBhat*Ycut^5 + 434*m2*MBhat*Ycut^5 - 
            46*m2^2*MBhat*Ycut^5 + 14*m2^3*MBhat*Ycut^5 + 90*MBhat^2*Ycut^5 - 
            405*m2*MBhat^2*Ycut^5 + 70*m2^2*MBhat^2*Ycut^5 + 
            5*m2^3*MBhat^2*Ycut^5 + 60*MBhat^3*Ycut^5 + 20*m2^2*MBhat^3*
             Ycut^5 - 60*MBhat^4*Ycut^5 + 30*m2*MBhat^4*Ycut^5 + 32*Ycut^6 - 
            97*m2*Ycut^6 + 9*m2^2*Ycut^6 - 2*MBhat*Ycut^6 + 
            112*m2*MBhat*Ycut^6 - 14*m2^2*MBhat*Ycut^6 - 90*MBhat^2*Ycut^6 + 
            15*m2*MBhat^2*Ycut^6 - 5*m2^2*MBhat^2*Ycut^6 + 
            60*MBhat^3*Ycut^6 - 20*m2*MBhat^3*Ycut^6 - 12*Ycut^7 - 
            9*m2*Ycut^7 + 42*MBhat*Ycut^7 - 6*m2*MBhat*Ycut^7 - 
            30*MBhat^2*Ycut^7 + 5*m2*MBhat^2*Ycut^7 - 6*Ycut^8 + 
            6*MBhat*Ycut^8))/(10*(-1 + Ycut)^4) - 6*Sqrt[m2]*
          (-2 - 29*m2 - 54*m2^2 - 14*m2^3 + m2^4 + 8*MBhat + 80*m2*MBhat + 
           80*m2^2*MBhat - 12*MBhat^2 - 78*m2*MBhat^2 - 33*m2^2*MBhat^2 + 
           3*m2^3*MBhat^2 + 8*MBhat^3 + 32*m2*MBhat^3 - 2*MBhat^4 - 
           5*m2*MBhat^4 + m2^2*MBhat^4)*Log[m2] + 6*Sqrt[m2]*
          (-2 - 29*m2 - 54*m2^2 - 14*m2^3 + m2^4 + 8*MBhat + 80*m2*MBhat + 
           80*m2^2*MBhat - 12*MBhat^2 - 78*m2*MBhat^2 - 33*m2^2*MBhat^2 + 
           3*m2^3*MBhat^2 + 8*MBhat^3 + 32*m2*MBhat^3 - 2*MBhat^4 - 
           5*m2*MBhat^4 + m2^2*MBhat^4)*Log[1 - Ycut])) + 
     c[VL]*(((-1 + m2 + Ycut)*(-90 - 1230*m2 + 2400*m2^2 - 720*m2^3 - 
          390*m2^4 + 30*m2^5 + 384*MBhat + 504*m2*MBhat - 5616*m2^2*MBhat + 
          2544*m2^3*MBhat - 816*m2^4*MBhat + 120*m2^5*MBhat - 665*MBhat^2 + 
          2470*m2*MBhat^2 - 1250*m2^2*MBhat^2 + 910*m2^3*MBhat^2 - 
          1145*m2^4*MBhat^2 + 280*m2^5*MBhat^2 + 504*MBhat^3 - 
          2016*m2*MBhat^3 + 3024*m2^2*MBhat^3 - 2016*m2^3*MBhat^3 + 
          504*m2^4*MBhat^3 - 135*MBhat^4 + 225*m2*MBhat^4 - 
          855*m2^2*MBhat^4 + 225*m2^3*MBhat^4 + 450*Ycut + 6780*m2*Ycut - 
          12960*m2^2*Ycut + 3600*m2^3*Ycut + 2310*m2^4*Ycut - 180*m2^5*Ycut - 
          1920*MBhat*Ycut - 3576*m2*MBhat*Ycut + 31848*m2^2*MBhat*Ycut - 
          14568*m2^3*MBhat*Ycut + 4776*m2^4*MBhat*Ycut - 
          720*m2^5*MBhat*Ycut + 3325*MBhat^2*Ycut - 12295*m2*MBhat^2*Ycut + 
          5355*m2^2*MBhat^2*Ycut - 4595*m2^3*MBhat^2*Ycut + 
          6590*m2^4*MBhat^2*Ycut - 1680*m2^5*MBhat^2*Ycut - 
          2520*MBhat^3*Ycut + 10584*m2*MBhat^3*Ycut - 16632*m2^2*MBhat^3*
           Ycut + 11592*m2^3*MBhat^3*Ycut - 3024*m2^4*MBhat^3*Ycut + 
          675*MBhat^4*Ycut - 1260*m2*MBhat^4*Ycut + 4905*m2^2*MBhat^4*Ycut - 
          1350*m2^3*MBhat^4*Ycut - 900*Ycut^2 - 15180*m2*Ycut^2 + 
          28290*m2^2*Ycut^2 - 6990*m2^3*Ycut^2 - 5670*m2^4*Ycut^2 + 
          450*m2^5*Ycut^2 + 3840*MBhat*Ycut^2 + 9984*m2*MBhat*Ycut^2 - 
          73728*m2^2*MBhat*Ycut^2 + 34104*m2^3*MBhat*Ycut^2 - 
          11520*m2^4*MBhat*Ycut^2 + 1800*m2^5*MBhat*Ycut^2 - 
          6650*MBhat^2*Ycut^2 + 24120*m2*MBhat^2*Ycut^2 - 
          7515*m2^2*MBhat^2*Ycut^2 + 8740*m2^3*MBhat^2*Ycut^2 - 
          15495*m2^4*MBhat^2*Ycut^2 + 4200*m2^5*MBhat^2*Ycut^2 + 
          5040*MBhat^3*Ycut^2 - 22176*m2*MBhat^3*Ycut^2 + 
          36792*m2^2*MBhat^3*Ycut^2 - 27216*m2^3*MBhat^3*Ycut^2 + 
          7560*m2^4*MBhat^3*Ycut^2 - 1350*MBhat^4*Ycut^2 + 
          2790*m2*MBhat^4*Ycut^2 - 11475*m2^2*MBhat^4*Ycut^2 + 
          3375*m2^3*MBhat^4*Ycut^2 + 900*Ycut^3 + 17400*m2*Ycut^3 - 
          31350*m2^2*Ycut^3 + 6300*m2^3*Ycut^3 + 7350*m2^4*Ycut^3 - 
          600*m2^5*Ycut^3 - 3840*MBhat*Ycut^3 - 14016*m2*MBhat*Ycut^3 + 
          87936*m2^2*MBhat*Ycut^3 - 41160*m2^3*MBhat*Ycut^3 + 
          14520*m2^4*MBhat*Ycut^3 - 2400*m2^5*MBhat*Ycut^3 + 
          6890*MBhat^2*Ycut^3 - 22810*m2*MBhat^2*Ycut^3 + 
          1895*m2^2*MBhat^2*Ycut^3 - 9065*m2^3*MBhat^2*Ycut^3 + 
          20140*m2^4*MBhat^2*Ycut^3 - 5600*m2^5*MBhat^2*Ycut^3 - 
          5220*MBhat^3*Ycut^3 + 22764*m2*MBhat^3*Ycut^3 - 
          42324*m2^2*MBhat^3*Ycut^3 + 34860*m2^3*MBhat^3*Ycut^3 - 
          10080*m2^4*MBhat^3*Ycut^3 + 1350*MBhat^4*Ycut^3 - 
          3060*m2*MBhat^4*Ycut^3 + 14445*m2^2*MBhat^4*Ycut^3 - 
          4500*m2^3*MBhat^4*Ycut^3 - 450*Ycut^4 - 10410*m2*Ycut^4 + 
          17820*m2^2*Ycut^4 - 2160*m2^3*Ycut^4 - 5250*m2^4*Ycut^4 + 
          450*m2^5*Ycut^4 + 1680*MBhat*Ycut^4 + 10344*m2*MBhat*Ycut^4 - 
          56160*m2^2*MBhat*Ycut^4 + 28560*m2^3*MBhat*Ycut^4 - 
          11280*m2^4*MBhat*Ycut^4 + 1800*m2^5*MBhat*Ycut^4 - 
          4360*MBhat^2*Ycut^4 + 9430*m2*MBhat^2*Ycut^4 + 7455*m2^2*MBhat^2*
           Ycut^4 + 3040*m2^3*MBhat^2*Ycut^4 - 14545*m2^4*MBhat^2*Ycut^4 + 
          4200*m2^5*MBhat^2*Ycut^4 + 3420*MBhat^3*Ycut^4 - 
          10176*m2*MBhat^3*Ycut^4 + 25620*m2^2*MBhat^3*Ycut^4 - 
          25560*m2^3*MBhat^3*Ycut^4 + 7560*m2^4*MBhat^3*Ycut^4 - 
          690*MBhat^4*Ycut^4 + 1650*m2*MBhat^4*Ycut^4 - 10545*m2^2*MBhat^4*
           Ycut^4 + 3375*m2^3*MBhat^4*Ycut^4 + 162*Ycut^5 + 2712*m2*Ycut^5 - 
          3948*m2^2*Ycut^5 - 1068*m2^3*Ycut^5 + 2322*m2^4*Ycut^5 - 
          180*m2^5*Ycut^5 + 768*MBhat*Ycut^5 - 4008*m2*MBhat*Ycut^5 + 
          17412*m2^2*MBhat*Ycut^5 - 11328*m2^3*MBhat*Ycut^5 + 
          4932*m2^4*MBhat*Ycut^5 - 720*m2^5*MBhat*Ycut^5 + 
          2240*MBhat^2*Ycut^5 - 30*m2*MBhat^2*Ycut^5 - 11835*m2^2*MBhat^2*
           Ycut^5 + 3625*m2^3*MBhat^2*Ycut^5 + 5370*m2^4*MBhat^2*Ycut^5 - 
          1680*m2^5*MBhat^2*Ycut^5 - 2298*MBhat^3*Ycut^5 - 
          954*m2*MBhat^3*Ycut^5 - 5394*m2^2*MBhat^3*Ycut^5 + 
          9846*m2^3*MBhat^3*Ycut^5 - 3024*m2^4*MBhat^3*Ycut^5 + 
          210*MBhat^4*Ycut^5 - 300*m2*MBhat^4*Ycut^5 + 4275*m2^2*MBhat^4*
           Ycut^5 - 1350*m2^3*MBhat^4*Ycut^5 - 357*Ycut^6 + 195*m2*Ycut^6 - 
          783*m2^2*Ycut^6 + 1449*m2^3*Ycut^6 - 534*m2^4*Ycut^6 + 
          30*m2^5*Ycut^6 - 2160*MBhat*Ycut^6 + 1392*m2*MBhat*Ycut^6 - 
          996*m2^2*MBhat*Ycut^6 + 1716*m2^3*MBhat*Ycut^6 - 
          1032*m2^4*MBhat*Ycut^6 + 120*m2^5*MBhat*Ycut^6 - 
          751*MBhat^2*Ycut^6 - 1141*m2*MBhat^2*Ycut^6 + 8174*m2^2*MBhat^2*
           Ycut^6 - 3851*m2^3*MBhat^2*Ycut^6 - 671*m2^4*MBhat^2*Ycut^6 + 
          280*m2^5*MBhat^2*Ycut^6 + 1770*MBhat^3*Ycut^6 + 
          3096*m2*MBhat^3*Ycut^6 - 2298*m2^2*MBhat^3*Ycut^6 - 
          1452*m2^3*MBhat^3*Ycut^6 + 504*m2^4*MBhat^3*Ycut^6 - 
          150*MBhat^4*Ycut^6 - 90*m2*MBhat^4*Ycut^6 - 765*m2^2*MBhat^4*
           Ycut^6 + 225*m2^3*MBhat^4*Ycut^6 + 705*Ycut^7 - 540*m2*Ycut^7 + 
          657*m2^2*Ycut^7 - 414*m2^3*Ycut^7 + 42*m2^4*Ycut^7 + 
          1920*MBhat*Ycut^7 - 1008*m2*MBhat*Ycut^7 - 924*m2^2*MBhat*Ycut^7 + 
          192*m2^3*MBhat*Ycut^7 + 60*m2^4*MBhat*Ycut^7 - 445*MBhat^2*Ycut^7 + 
          274*m2*MBhat^2*Ycut^7 - 2532*m2^2*MBhat^2*Ycut^7 + 
          1357*m2^3*MBhat^2*Ycut^7 - 64*m2^4*MBhat^2*Ycut^7 - 
          840*MBhat^3*Ycut^7 - 1344*m2*MBhat^3*Ycut^7 + 1398*m2^2*MBhat^3*
           Ycut^7 - 54*m2^3*MBhat^3*Ycut^7 + 150*MBhat^4*Ycut^7 + 
          60*m2*MBhat^4*Ycut^7 + 15*m2^2*MBhat^4*Ycut^7 - 690*Ycut^8 + 
          390*m2*Ycut^8 - 123*m2^2*Ycut^8 + 3*m2^3*Ycut^8 - 
          720*MBhat*Ycut^8 + 432*m2*MBhat*Ycut^8 + 228*m2^2*MBhat*Ycut^8 - 
          60*m2^3*MBhat*Ycut^8 + 575*MBhat^2*Ycut^8 - 6*m2*MBhat^2*Ycut^8 + 
          252*m2^2*MBhat^2*Ycut^8 - 161*m2^3*MBhat^2*Ycut^8 + 
          120*MBhat^3*Ycut^8 + 216*m2*MBhat^3*Ycut^8 - 186*m2^2*MBhat^3*
           Ycut^8 - 75*MBhat^4*Ycut^8 - 15*m2*MBhat^4*Ycut^8 + 330*Ycut^9 - 
          120*m2*Ycut^9 - 3*m2^2*Ycut^9 - 48*m2*MBhat*Ycut^9 - 
          155*MBhat^2*Ycut^9 - 11*m2*MBhat^2*Ycut^9 + m2^2*MBhat^2*Ycut^9 + 
          30*MBhat^3*Ycut^9 + 6*m2*MBhat^3*Ycut^9 + 15*MBhat^4*Ycut^9 - 
          57*Ycut^10 + 3*m2*Ycut^10 + 48*MBhat*Ycut^10 - 5*MBhat^2*Ycut^10 - 
          m2*MBhat^2*Ycut^10 - 6*MBhat^3*Ycut^10 - 3*Ycut^11 + 
          MBhat^2*Ycut^11))/(45*(-1 + Ycut)^6) + 
       (4*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 72*m2*MBhat + 
          12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 9*m2*MBhat^4)*
         Log[m2])/3 - (4*m2*(12 - 6*m2 - 24*m2^2 + 18*m2^3 - 24*MBhat + 
          72*m2*MBhat + 12*MBhat^2 - 57*m2*MBhat^2 + 35*m2^2*MBhat^2 + 
          9*m2*MBhat^4)*Log[1 - Ycut])/3 + 
       c[VR]*(-1/15*(Sqrt[m2]*(-1 + m2 + Ycut)*(391 - 574*m2 - 2514*m2^2 + 
             146*m2^3 + 31*m2^4 - 1288*MBhat + 4512*m2*MBhat + 
             3072*m2^2*MBhat - 608*m2^3*MBhat + 72*m2^4*MBhat + 
             1395*MBhat^2 - 5960*m2*MBhat^2 + 1320*m2^2*MBhat^2 - 
             480*m2^3*MBhat^2 + 125*m2^4*MBhat^2 - 640*MBhat^3 + 
             2480*m2*MBhat^3 - 1120*m2^2*MBhat^3 + 240*m2^3*MBhat^3 + 
             130*MBhat^4 - 140*m2*MBhat^4 + 130*m2^2*MBhat^4 - 1293*Ycut + 
             1693*m2*Ycut + 8979*m2^2*Ycut - 435*m2^3*Ycut - 124*m2^4*Ycut + 
             4344*MBhat*Ycut - 14824*m2*MBhat*Ycut - 11752*m2^2*MBhat*Ycut + 
             2360*m2^3*MBhat*Ycut - 288*m2^4*MBhat*Ycut - 4905*MBhat^2*Ycut + 
             20595*m2*MBhat^2*Ycut - 4385*m2^2*MBhat^2*Ycut + 
             1795*m2^3*MBhat^2*Ycut - 500*m2^4*MBhat^2*Ycut + 
             2400*MBhat^3*Ycut - 9040*m2*MBhat^3*Ycut + 4240*m2^2*MBhat^3*
              Ycut - 960*m2^3*MBhat^3*Ycut - 510*MBhat^4*Ycut + 
             610*m2*MBhat^4*Ycut - 520*m2^2*MBhat^4*Ycut + 1473*Ycut^2 - 
             1574*m2*Ycut^2 - 11375*m2^2*Ycut^2 + 370*m2^3*Ycut^2 + 
             186*m2^4*Ycut^2 - 5064*MBhat*Ycut^2 + 16592*m2*MBhat*Ycut^2 + 
             16360*m2^2*MBhat*Ycut^2 - 3360*m2^3*MBhat*Ycut^2 + 
             432*m2^4*MBhat*Ycut^2 + 5985*MBhat^2*Ycut^2 - 24690*m2*MBhat^2*
              Ycut^2 + 4735*m2^2*MBhat^2*Ycut^2 - 2380*m2^3*MBhat^2*Ycut^2 + 
             750*m2^4*MBhat^2*Ycut^2 - 3120*MBhat^3*Ycut^2 + 
             11600*m2*MBhat^3*Ycut^2 - 5760*m2^2*MBhat^3*Ycut^2 + 
             1440*m2^3*MBhat^3*Ycut^2 + 690*MBhat^4*Ycut^2 - 
             950*m2*MBhat^4*Ycut^2 + 780*m2^2*MBhat^4*Ycut^2 - 611*Ycut^3 + 
             375*m2*Ycut^3 + 5600*m2^2*Ycut^3 + 10*m2^3*Ycut^3 - 
             124*m2^4*Ycut^3 + 2168*MBhat*Ycut^3 - 6520*m2*MBhat*Ycut^3 - 
             9360*m2^2*MBhat*Ycut^3 + 2000*m2^3*MBhat*Ycut^3 - 
             288*m2^4*MBhat*Ycut^3 - 2495*MBhat^2*Ycut^3 + 10595*m2*MBhat^2*
              Ycut^3 - 1450*m2^2*MBhat^2*Ycut^3 + 1350*m2^3*MBhat^2*Ycut^3 - 
             500*m2^4*MBhat^2*Ycut^3 + 1340*MBhat^3*Ycut^3 - 
             5720*m2*MBhat^3*Ycut^3 + 3340*m2^2*MBhat^3*Ycut^3 - 
             960*m2^3*MBhat^3*Ycut^3 - 350*MBhat^4*Ycut^3 + 
             680*m2*MBhat^4*Ycut^3 - 520*m2^2*MBhat^4*Ycut^3 + 30*Ycut^4 + 
             65*m2*Ycut^4 - 525*m2^2*Ycut^4 - 105*m2^3*Ycut^4 + 
             31*m2^4*Ycut^4 - 220*MBhat*Ycut^4 + 180*m2*MBhat*Ycut^4 + 
             1620*m2^2*MBhat*Ycut^4 - 500*m2^3*MBhat*Ycut^4 + 
             72*m2^4*MBhat*Ycut^4 - 310*MBhat^2*Ycut^4 + 435*m2*MBhat^2*
              Ycut^4 - 795*m2^2*MBhat^2*Ycut^4 - 175*m2^3*MBhat^2*Ycut^4 + 
             125*m2^4*MBhat^2*Ycut^4 + 300*MBhat^3*Ycut^4 + 
             340*m2*MBhat^3*Ycut^4 - 640*m2^2*MBhat^3*Ycut^4 + 
             240*m2^3*MBhat^3*Ycut^4 + 60*MBhat^4*Ycut^4 - 250*m2*MBhat^4*
              Ycut^4 + 130*m2^2*MBhat^4*Ycut^4 + 24*Ycut^5 + 29*m2*Ycut^5 - 
             196*m2^2*Ycut^5 + 59*m2^3*Ycut^5 + 178*MBhat*Ycut^5 - 
             122*m2*MBhat*Ycut^5 + 118*m2^2*MBhat*Ycut^5 + 18*m2^3*MBhat*
              Ycut^5 + 330*MBhat^2*Ycut^5 - 1095*m2*MBhat^2*Ycut^5 + 
             630*m2^2*MBhat^2*Ycut^5 - 65*m2^3*MBhat^2*Ycut^5 - 
             300*MBhat^3*Ycut^5 + 400*m2*MBhat^3*Ycut^5 - 60*m2^2*MBhat^3*
              Ycut^5 - 60*MBhat^4*Ycut^5 + 50*m2*MBhat^4*Ycut^5 - 32*Ycut^6 - 
             23*m2*Ycut^6 + 31*m2^2*Ycut^6 - 94*MBhat*Ycut^6 + 
             184*m2*MBhat*Ycut^6 - 58*m2^2*MBhat*Ycut^6 - 10*MBhat^2*Ycut^6 + 
             105*m2*MBhat^2*Ycut^6 - 55*m2^2*MBhat^2*Ycut^6 + 
             20*MBhat^3*Ycut^6 - 60*m2*MBhat^3*Ycut^6 + 40*MBhat^4*Ycut^6 + 
             12*Ycut^7 + 9*m2*Ycut^7 - 26*MBhat*Ycut^7 - 2*m2*MBhat*Ycut^7 + 
             10*MBhat^2*Ycut^7 + 15*m2*MBhat^2*Ycut^7 + 6*Ycut^8 + 
             2*MBhat*Ycut^8))/(-1 + Ycut)^4 + 4*Sqrt[m2]*
          (2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4 - 8*MBhat + 8*m2*MBhat + 
           96*m2^2*MBhat + 12*MBhat^2 - 34*m2*MBhat^2 - 47*m2^2*MBhat^2 + 
           9*m2^3*MBhat^2 - 8*MBhat^3 + 24*m2*MBhat^3 + 2*MBhat^4 - 
           3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[m2] - 
         4*Sqrt[m2]*(2 + 5*m2 - 34*m2^2 - 18*m2^3 + 3*m2^4 - 8*MBhat + 
           8*m2*MBhat + 96*m2^2*MBhat + 12*MBhat^2 - 34*m2*MBhat^2 - 
           47*m2^2*MBhat^2 + 9*m2^3*MBhat^2 - 8*MBhat^3 + 24*m2*MBhat^3 + 
           2*MBhat^4 - 3*m2*MBhat^4 + 3*m2^2*MBhat^4)*Log[1 - Ycut]))))/
  mb^3 + (2 - 48*m2 - 750*m2^2 + 750*m2^4 + 48*m2^5 - 2*m2^6 - 13*MBhat + 
   240*m2*MBhat + 2085*m2^2*MBhat - 1440*m2^3*MBhat - 915*m2^4*MBhat + 
   48*m2^5*MBhat - 5*m2^6*MBhat + 35*MBhat^2 - 477*m2*MBhat^2 - 
   1800*m2^2*MBhat^2 + 2160*m2^3*MBhat^2 + 45*m2^4*MBhat^2 + 
   45*m2^5*MBhat^2 - 8*m2^6*MBhat^2 - 39*MBhat^3 + 405*m2*MBhat^3 + 
   480*m2^2*MBhat^3 - 960*m2^3*MBhat^3 + 135*m2^4*MBhat^3 - 21*m2^5*MBhat^3 + 
   15*MBhat^4 - 120*m2*MBhat^4 + 120*m2^3*MBhat^4 - 15*m2^4*MBhat^4 - 
   8*Ycut + 192*m2*Ycut + 3360*m2^2*Ycut + 960*m2^3*Ycut - 2640*m2^4*Ycut - 
   192*m2^5*Ycut + 8*m2^6*Ycut + 52*MBhat*Ycut - 960*m2*MBhat*Ycut - 
   9600*m2^2*MBhat*Ycut + 3840*m2^3*MBhat*Ycut + 3480*m2^4*MBhat*Ycut - 
   192*m2^5*MBhat*Ycut + 20*m2^6*MBhat*Ycut - 140*MBhat^2*Ycut + 
   1908*m2*MBhat^2*Ycut + 8820*m2^2*MBhat^2*Ycut - 7500*m2^3*MBhat^2*Ycut - 
   180*m2^4*MBhat^2*Ycut - 180*m2^5*MBhat^2*Ycut + 32*m2^6*MBhat^2*Ycut + 
   156*MBhat^3*Ycut - 1620*m2*MBhat^3*Ycut - 2820*m2^2*MBhat^3*Ycut + 
   3660*m2^3*MBhat^3*Ycut - 540*m2^4*MBhat^3*Ycut + 84*m2^5*MBhat^3*Ycut - 
   60*MBhat^4*Ycut + 480*m2*MBhat^4*Ycut + 180*m2^2*MBhat^4*Ycut - 
   480*m2^3*MBhat^4*Ycut + 60*m2^4*MBhat^4*Ycut + 12*Ycut^2 - 288*m2*Ycut^2 - 
   5760*m2^2*Ycut^2 - 3360*m2^3*Ycut^2 + 3240*m2^4*Ycut^2 + 288*m2^5*Ycut^2 - 
   12*m2^6*Ycut^2 - 78*MBhat*Ycut^2 + 1440*m2*MBhat*Ycut^2 + 
   16920*m2^2*MBhat*Ycut^2 - 1920*m2^3*MBhat*Ycut^2 - 
   4860*m2^4*MBhat*Ycut^2 + 288*m2^5*MBhat*Ycut^2 - 30*m2^6*MBhat*Ycut^2 + 
   210*MBhat^2*Ycut^2 - 2862*m2*MBhat^2*Ycut^2 - 16470*m2^2*MBhat^2*Ycut^2 + 
   8970*m2^3*MBhat^2*Ycut^2 + 270*m2^4*MBhat^2*Ycut^2 + 
   270*m2^5*MBhat^2*Ycut^2 - 48*m2^6*MBhat^2*Ycut^2 - 234*MBhat^3*Ycut^2 + 
   2430*m2*MBhat^3*Ycut^2 + 6030*m2^2*MBhat^3*Ycut^2 - 
   5130*m2^3*MBhat^3*Ycut^2 + 810*m2^4*MBhat^3*Ycut^2 - 
   126*m2^5*MBhat^3*Ycut^2 + 90*MBhat^4*Ycut^2 - 720*m2*MBhat^4*Ycut^2 - 
   630*m2^2*MBhat^4*Ycut^2 + 720*m2^3*MBhat^4*Ycut^2 - 
   90*m2^4*MBhat^4*Ycut^2 - 8*Ycut^3 + 192*m2*Ycut^3 + 4560*m2^2*Ycut^3 + 
   4160*m2^3*Ycut^3 - 1440*m2^4*Ycut^3 - 192*m2^5*Ycut^3 + 8*m2^6*Ycut^3 + 
   52*MBhat*Ycut^3 - 960*m2*MBhat*Ycut^3 - 13800*m2^2*MBhat*Ycut^3 - 
   2560*m2^3*MBhat*Ycut^3 + 2880*m2^4*MBhat*Ycut^3 - 192*m2^5*MBhat*Ycut^3 + 
   20*m2^6*MBhat*Ycut^3 - 170*MBhat^2*Ycut^3 + 1998*m2*MBhat^2*Ycut^3 + 
   14160*m2^2*MBhat^2*Ycut^3 - 3760*m2^3*MBhat^2*Ycut^3 - 
   90*m2^4*MBhat^2*Ycut^3 - 210*m2^5*MBhat^2*Ycut^3 + 
   32*m2^6*MBhat^2*Ycut^3 + 216*MBhat^3*Ycut^3 - 1740*m2*MBhat^3*Ycut^3 - 
   5820*m2^2*MBhat^3*Ycut^3 + 3180*m2^3*MBhat^3*Ycut^3 - 
   600*m2^4*MBhat^3*Ycut^3 + 84*m2^5*MBhat^3*Ycut^3 - 90*MBhat^4*Ycut^3 + 
   510*m2*MBhat^4*Ycut^3 + 810*m2^2*MBhat^4*Ycut^3 - 
   510*m2^3*MBhat^4*Ycut^3 + 60*m2^4*MBhat^4*Ycut^3 + 2*Ycut^4 - 
   48*m2*Ycut^4 - 1500*m2^2*Ycut^4 - 2000*m2^3*Ycut^4 + 48*m2^5*Ycut^4 - 
   2*m2^6*Ycut^4 + 2*MBhat*Ycut^4 + 210*m2*MBhat*Ycut^4 + 
   4680*m2^2*MBhat*Ycut^4 + 2680*m2^3*MBhat*Ycut^4 - 645*m2^4*MBhat*Ycut^4 + 
   78*m2^5*MBhat*Ycut^4 - 5*m2^6*MBhat*Ycut^4 + 140*MBhat^2*Ycut^4 - 
   822*m2*MBhat^2*Ycut^4 - 4800*m2^2*MBhat^2*Ycut^4 - 
   320*m2^3*MBhat^2*Ycut^4 - 15*m2^4*MBhat^2*Ycut^4 + 
   75*m2^5*MBhat^2*Ycut^4 - 8*m2^6*MBhat^2*Ycut^4 - 294*MBhat^3*Ycut^4 + 
   900*m2*MBhat^3*Ycut^4 + 2220*m2^2*MBhat^3*Ycut^4 - 
   780*m2^3*MBhat^3*Ycut^4 + 225*m2^4*MBhat^3*Ycut^4 - 
   21*m2^5*MBhat^3*Ycut^4 + 150*MBhat^4*Ycut^4 - 240*m2*MBhat^4*Ycut^4 - 
   450*m2^2*MBhat^4*Ycut^4 + 180*m2^3*MBhat^4*Ycut^4 - 
   15*m2^4*MBhat^4*Ycut^4 - 3*Ycut^5 + 3*m2*Ycut^5 + 90*m2^2*Ycut^5 + 
   150*m2^3*Ycut^5 + 105*m2^4*Ycut^5 - 9*m2^5*Ycut^5 - 60*MBhat*Ycut^5 + 
   120*m2*MBhat*Ycut^5 - 240*m2^2*MBhat*Ycut^5 - 540*m2^3*MBhat*Ycut^5 + 
   60*m2^4*MBhat*Ycut^5 - 12*m2^5*MBhat*Ycut^5 - 111*MBhat^2*Ycut^5 + 
   477*m2*MBhat^2*Ycut^5 - 360*m2^2*MBhat^2*Ycut^5 + 
   600*m2^3*MBhat^2*Ycut^5 - 45*m2^4*MBhat^2*Ycut^5 - 9*m2^5*MBhat^2*Ycut^5 + 
   414*MBhat^3*Ycut^5 - 780*m2*MBhat^3*Ycut^5 + 180*m2^2*MBhat^3*Ycut^5 - 
   30*m2^4*MBhat^3*Ycut^5 - 240*MBhat^4*Ycut^5 + 180*m2*MBhat^4*Ycut^5 + 
   90*m2^2*MBhat^4*Ycut^5 - 30*m2^3*MBhat^4*Ycut^5 + 13*Ycut^6 - 
   12*m2*Ycut^6 - 30*m2^2*Ycut^6 + 100*m2^3*Ycut^6 - 15*m2^4*Ycut^6 + 
   88*MBhat*Ycut^6 - 180*m2*MBhat*Ycut^6 + 60*m2^2*MBhat*Ycut^6 - 
   80*m2^3*MBhat*Ycut^6 - 5*MBhat^2*Ycut^6 - 258*m2*MBhat^2*Ycut^6 + 
   510*m2^2*MBhat^2*Ycut^6 - 170*m2^3*MBhat^2*Ycut^6 + 
   15*m2^4*MBhat^2*Ycut^6 - 306*MBhat^3*Ycut^6 + 570*m2*MBhat^3*Ycut^6 - 
   330*m2^2*MBhat^3*Ycut^6 + 30*m2^3*MBhat^3*Ycut^6 + 210*MBhat^4*Ycut^6 - 
   120*m2*MBhat^4*Ycut^6 - 22*Ycut^7 + 18*m2*Ycut^7 + 30*m2^2*Ycut^7 - 
   10*m2^3*Ycut^7 - 52*MBhat*Ycut^7 + 120*m2*MBhat*Ycut^7 - 
   120*m2^2*MBhat*Ycut^7 + 20*m2^3*MBhat*Ycut^7 + 80*MBhat^2*Ycut^7 + 
   12*m2*MBhat^2*Ycut^7 - 60*m2^2*MBhat^2*Ycut^7 + 20*m2^3*MBhat^2*Ycut^7 + 
   84*MBhat^3*Ycut^7 - 180*m2*MBhat^3*Ycut^7 + 60*m2^2*MBhat^3*Ycut^7 - 
   90*MBhat^4*Ycut^7 + 30*m2*MBhat^4*Ycut^7 + 18*Ycut^8 - 12*m2*Ycut^8 + 
   3*MBhat*Ycut^8 - 30*m2*MBhat*Ycut^8 + 15*m2^2*MBhat*Ycut^8 - 
   45*MBhat^2*Ycut^8 + 27*m2*MBhat^2*Ycut^8 + 9*MBhat^3*Ycut^8 + 
   15*m2*MBhat^3*Ycut^8 + 15*MBhat^4*Ycut^8 - 7*Ycut^9 + 3*m2*Ycut^9 + 
   8*MBhat*Ycut^9 + 5*MBhat^2*Ycut^9 - 3*m2*MBhat^2*Ycut^9 - 
   6*MBhat^3*Ycut^9 + Ycut^10 - 2*MBhat*Ycut^10 + MBhat^2*Ycut^10 + 
   15*api*MBhat^4*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat^4*Ycut*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] + 
   90*api*MBhat^4*Ycut^2*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat^4*Ycut^3*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] + 
   15*api*MBhat^4*Ycut^4*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat^3*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   240*api*MBhat^3*Ycut*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] - 
   360*api*MBhat^3*Ycut^2*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   240*api*MBhat^3*Ycut^3*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat^3*Ycut^4*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   60*api*MBhat^2*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] - 
   240*api*MBhat^2*Ycut*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] + 
   360*api*MBhat^2*Ycut^2*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] - 
   240*api*MBhat^2*Ycut^3*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] + 
   60*api*MBhat^2*Ycut^4*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] + 
   30*api*MBhat^2*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   120*api*MBhat^2*Ycut*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] + 
   180*api*MBhat^2*Ycut^2*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   120*api*MBhat^2*Ycut^3*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] + 
   30*api*MBhat^2*Ycut^4*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   240*api*MBhat*Ycut*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] - 
   360*api*MBhat*Ycut^2*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   240*api*MBhat*Ycut^3*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat*Ycut^4*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   15*api*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] - 
   60*api*Ycut*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] + 
   90*api*Ycut^2*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] - 
   60*api*Ycut^3*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] + 
   15*api*Ycut^4*X1mix[2, 0, SM^2, Ycut, m2, mu2hat])/(15*(-1 + Ycut)^4) + 
 c[SL]^2*(3*m2^2*(-3 - 8*m2 - 3*m2^2 + 10*MBhat + 16*m2*MBhat + 
     2*m2^2*MBhat - 12*MBhat^2 - 10*m2*MBhat^2 + 6*MBhat^3 + 2*m2*MBhat^3 - 
     MBhat^4)*Log[m2] - 3*m2^2*(-3 - 8*m2 - 3*m2^2 + 10*MBhat + 16*m2*MBhat + 
     2*m2^2*MBhat - 12*MBhat^2 - 10*m2*MBhat^2 + 6*MBhat^3 + 2*m2*MBhat^3 - 
     MBhat^4)*Log[1 - Ycut] + (1 - 24*m2 - 375*m2^2 + 375*m2^4 + 24*m2^5 - 
     m2^6 - 6*MBhat + 112*m2*MBhat + 1010*m2^2*MBhat - 640*m2^3*MBhat - 
     490*m2^4*MBhat + 16*m2^5*MBhat - 2*m2^6*MBhat + 14*MBhat^2 - 
     198*m2*MBhat^2 - 870*m2^2*MBhat^2 + 960*m2^3*MBhat^2 + 90*m2^4*MBhat^2 + 
     6*m2^5*MBhat^2 - 2*m2^6*MBhat^2 - 14*MBhat^3 + 150*m2*MBhat^3 + 
     240*m2^2*MBhat^3 - 400*m2^3*MBhat^3 + 30*m2^4*MBhat^3 - 6*m2^5*MBhat^3 + 
     5*MBhat^4 - 40*m2*MBhat^4 + 40*m2^3*MBhat^4 - 5*m2^4*MBhat^4 - 4*Ycut + 
     96*m2*Ycut + 1680*m2^2*Ycut + 480*m2^3*Ycut - 1320*m2^4*Ycut - 
     96*m2^5*Ycut + 4*m2^6*Ycut + 24*MBhat*Ycut - 448*m2*MBhat*Ycut - 
     4640*m2^2*MBhat*Ycut + 1600*m2^3*MBhat*Ycut + 1840*m2^4*MBhat*Ycut - 
     64*m2^5*MBhat*Ycut + 8*m2^6*MBhat*Ycut - 56*MBhat^2*Ycut + 
     792*m2*MBhat^2*Ycut + 4200*m2^2*MBhat^2*Ycut - 3240*m2^3*MBhat^2*Ycut - 
     360*m2^4*MBhat^2*Ycut - 24*m2^5*MBhat^2*Ycut + 8*m2^6*MBhat^2*Ycut + 
     56*MBhat^3*Ycut - 600*m2*MBhat^3*Ycut - 1320*m2^2*MBhat^3*Ycut + 
     1480*m2^3*MBhat^3*Ycut - 120*m2^4*MBhat^3*Ycut + 24*m2^5*MBhat^3*Ycut - 
     20*MBhat^4*Ycut + 160*m2*MBhat^4*Ycut + 60*m2^2*MBhat^4*Ycut - 
     160*m2^3*MBhat^4*Ycut + 20*m2^4*MBhat^4*Ycut + 6*Ycut^2 - 
     144*m2*Ycut^2 - 2880*m2^2*Ycut^2 - 1680*m2^3*Ycut^2 + 1620*m2^4*Ycut^2 + 
     144*m2^5*Ycut^2 - 6*m2^6*Ycut^2 - 36*MBhat*Ycut^2 + 
     672*m2*MBhat*Ycut^2 + 8160*m2^2*MBhat*Ycut^2 - 480*m2^3*MBhat*Ycut^2 - 
     2520*m2^4*MBhat*Ycut^2 + 96*m2^5*MBhat*Ycut^2 - 12*m2^6*MBhat*Ycut^2 + 
     84*MBhat^2*Ycut^2 - 1188*m2*MBhat^2*Ycut^2 - 7740*m2^2*MBhat^2*Ycut^2 + 
     3660*m2^3*MBhat^2*Ycut^2 + 540*m2^4*MBhat^2*Ycut^2 + 
     36*m2^5*MBhat^2*Ycut^2 - 12*m2^6*MBhat^2*Ycut^2 - 84*MBhat^3*Ycut^2 + 
     900*m2*MBhat^3*Ycut^2 + 2700*m2^2*MBhat^3*Ycut^2 - 
     1980*m2^3*MBhat^3*Ycut^2 + 180*m2^4*MBhat^3*Ycut^2 - 
     36*m2^5*MBhat^3*Ycut^2 + 30*MBhat^4*Ycut^2 - 240*m2*MBhat^4*Ycut^2 - 
     210*m2^2*MBhat^4*Ycut^2 + 240*m2^3*MBhat^4*Ycut^2 - 
     30*m2^4*MBhat^4*Ycut^2 - 4*Ycut^3 + 96*m2*Ycut^3 + 2280*m2^2*Ycut^3 + 
     2080*m2^3*Ycut^3 - 720*m2^4*Ycut^3 - 96*m2^5*Ycut^3 + 4*m2^6*Ycut^3 + 
     24*MBhat*Ycut^3 - 448*m2*MBhat*Ycut^3 - 6640*m2^2*MBhat*Ycut^3 - 
     1600*m2^3*MBhat*Ycut^3 + 1440*m2^4*MBhat*Ycut^3 - 64*m2^5*MBhat*Ycut^3 + 
     8*m2^6*MBhat*Ycut^3 - 66*MBhat^2*Ycut^3 + 822*m2*MBhat^2*Ycut^3 + 
     6580*m2^2*MBhat^2*Ycut^3 - 1260*m2^3*MBhat^2*Ycut^3 - 
     330*m2^4*MBhat^2*Ycut^3 - 34*m2^5*MBhat^2*Ycut^3 + 
     8*m2^6*MBhat^2*Ycut^3 + 76*MBhat^3*Ycut^3 - 640*m2*MBhat^3*Ycut^3 - 
     2520*m2^2*MBhat^3*Ycut^3 + 1120*m2^3*MBhat^3*Ycut^3 - 
     140*m2^4*MBhat^3*Ycut^3 + 24*m2^5*MBhat^3*Ycut^3 - 30*MBhat^4*Ycut^3 + 
     170*m2*MBhat^4*Ycut^3 + 270*m2^2*MBhat^4*Ycut^3 - 
     170*m2^3*MBhat^4*Ycut^3 + 20*m2^4*MBhat^4*Ycut^3 + Ycut^4 - 
     24*m2*Ycut^4 - 750*m2^2*Ycut^4 - 1000*m2^3*Ycut^4 + 24*m2^5*Ycut^4 - 
     m2^6*Ycut^4 + 4*MBhat*Ycut^4 + 82*m2*MBhat*Ycut^4 + 
     2280*m2^2*MBhat*Ycut^4 + 1380*m2^3*MBhat*Ycut^4 - 
     270*m2^4*MBhat*Ycut^4 + 26*m2^5*MBhat*Ycut^4 - 2*m2^6*MBhat*Ycut^4 + 
     39*MBhat^2*Ycut^4 - 278*m2*MBhat^2*Ycut^4 - 2290*m2^2*MBhat^2*Ycut^4 - 
     300*m2^3*MBhat^2*Ycut^4 + 65*m2^4*MBhat^2*Ycut^4 + 
     16*m2^5*MBhat^2*Ycut^4 - 2*m2^6*MBhat^2*Ycut^4 - 94*MBhat^3*Ycut^4 + 
     300*m2*MBhat^3*Ycut^4 + 960*m2^2*MBhat^3*Ycut^4 - 
     220*m2^3*MBhat^3*Ycut^4 + 60*m2^4*MBhat^3*Ycut^4 - 
     6*m2^5*MBhat^3*Ycut^4 + 50*MBhat^4*Ycut^4 - 80*m2*MBhat^4*Ycut^4 - 
     150*m2^2*MBhat^4*Ycut^4 + 60*m2^3*MBhat^4*Ycut^4 - 
     5*m2^4*MBhat^4*Ycut^4 - 3*Ycut^5 + 9*m2*Ycut^5 + 30*m2^2*Ycut^5 + 
     90*m2^3*Ycut^5 + 45*m2^4*Ycut^5 - 3*m2^5*Ycut^5 - 40*MBhat*Ycut^5 + 
     108*m2*MBhat*Ycut^5 - 200*m2^2*MBhat*Ycut^5 - 200*m2^3*MBhat*Ycut^5 - 
     4*m2^5*MBhat*Ycut^5 + 9*MBhat^2*Ycut^5 + 23*m2*MBhat^2*Ycut^5 + 
     60*m2^2*MBhat^2*Ycut^5 + 180*m2^3*MBhat^2*Ycut^5 - 
     5*m2^4*MBhat^2*Ycut^5 - 3*m2^5*MBhat^2*Ycut^5 + 114*MBhat^3*Ycut^5 - 
     200*m2*MBhat^3*Ycut^5 - 10*m2^4*MBhat^3*Ycut^5 - 80*MBhat^4*Ycut^5 + 
     60*m2*MBhat^4*Ycut^5 + 30*m2^2*MBhat^4*Ycut^5 - 10*m2^3*MBhat^4*Ycut^5 + 
     14*Ycut^6 - 36*m2*Ycut^6 + 30*m2^2*Ycut^6 + 20*m2^3*Ycut^6 + 
     56*MBhat*Ycut^6 - 132*m2*MBhat*Ycut^6 + 80*m2^2*MBhat*Ycut^6 - 
     60*m2^3*MBhat*Ycut^6 - 84*MBhat^2*Ycut^6 + 108*m2*MBhat^2*Ycut^6 + 
     20*m2^2*MBhat^2*Ycut^6 - 56*MBhat^3*Ycut^6 + 100*m2*MBhat^3*Ycut^6 - 
     60*m2^2*MBhat^3*Ycut^6 + 70*MBhat^4*Ycut^6 - 40*m2*MBhat^4*Ycut^6 - 
     26*Ycut^7 + 54*m2*Ycut^7 - 30*m2^2*Ycut^7 + 10*m2^3*Ycut^7 - 
     24*MBhat*Ycut^7 + 48*m2*MBhat*Ycut^7 - 40*m2^2*MBhat*Ycut^7 + 
     96*MBhat^2*Ycut^7 - 112*m2*MBhat^2*Ycut^7 + 40*m2^2*MBhat^2*Ycut^7 - 
     16*MBhat^3*Ycut^7 - 30*MBhat^4*Ycut^7 + 10*m2*MBhat^4*Ycut^7 + 
     24*Ycut^8 - 36*m2*Ycut^8 + 15*m2^2*Ycut^8 - 14*MBhat*Ycut^8 + 
     18*m2*MBhat*Ycut^8 - 10*m2^2*MBhat*Ycut^8 - 39*MBhat^2*Ycut^8 + 
     28*m2*MBhat^2*Ycut^8 + 24*MBhat^3*Ycut^8 - 10*m2*MBhat^3*Ycut^8 + 
     5*MBhat^4*Ycut^8 - 11*Ycut^9 + 9*m2*Ycut^9 + 16*MBhat*Ycut^9 - 
     12*m2*MBhat*Ycut^9 + MBhat^2*Ycut^9 + 3*m2*MBhat^2*Ycut^9 - 
     6*MBhat^3*Ycut^9 + 2*Ycut^10 - 4*MBhat*Ycut^10 + 2*MBhat^2*Ycut^10 + 
     20*api*MBhat^4*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^4*Ycut*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     120*api*MBhat^4*Ycut^2*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^4*Ycut^3*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     20*api*MBhat^4*Ycut^4*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^3*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat^3*Ycut*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     480*api*MBhat^3*Ycut^2*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat^3*Ycut^3*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^3*Ycut^4*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     80*api*MBhat^2*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     320*api*MBhat^2*Ycut*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     480*api*MBhat^2*Ycut^2*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     320*api*MBhat^2*Ycut^3*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     80*api*MBhat^2*Ycut^4*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     40*api*MBhat^2*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     160*api*MBhat^2*Ycut*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat^2*Ycut^2*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     160*api*MBhat^2*Ycut^3*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     40*api*MBhat^2*Ycut^4*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat*Ycut*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     480*api*MBhat*Ycut^2*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat*Ycut^3*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat*Ycut^4*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     20*api*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*Ycut*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     120*api*Ycut^2*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     80*api*Ycut^3*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     20*api*Ycut^4*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat])/
    (20*(-1 + Ycut)^4)) + 
 c[SR]^2*(3*m2^2*(-3 - 8*m2 - 3*m2^2 + 10*MBhat + 16*m2*MBhat + 
     2*m2^2*MBhat - 12*MBhat^2 - 10*m2*MBhat^2 + 6*MBhat^3 + 2*m2*MBhat^3 - 
     MBhat^4)*Log[m2] - 3*m2^2*(-3 - 8*m2 - 3*m2^2 + 10*MBhat + 16*m2*MBhat + 
     2*m2^2*MBhat - 12*MBhat^2 - 10*m2*MBhat^2 + 6*MBhat^3 + 2*m2*MBhat^3 - 
     MBhat^4)*Log[1 - Ycut] + (1 - 24*m2 - 375*m2^2 + 375*m2^4 + 24*m2^5 - 
     m2^6 - 6*MBhat + 112*m2*MBhat + 1010*m2^2*MBhat - 640*m2^3*MBhat - 
     490*m2^4*MBhat + 16*m2^5*MBhat - 2*m2^6*MBhat + 14*MBhat^2 - 
     198*m2*MBhat^2 - 870*m2^2*MBhat^2 + 960*m2^3*MBhat^2 + 90*m2^4*MBhat^2 + 
     6*m2^5*MBhat^2 - 2*m2^6*MBhat^2 - 14*MBhat^3 + 150*m2*MBhat^3 + 
     240*m2^2*MBhat^3 - 400*m2^3*MBhat^3 + 30*m2^4*MBhat^3 - 6*m2^5*MBhat^3 + 
     5*MBhat^4 - 40*m2*MBhat^4 + 40*m2^3*MBhat^4 - 5*m2^4*MBhat^4 - 4*Ycut + 
     96*m2*Ycut + 1680*m2^2*Ycut + 480*m2^3*Ycut - 1320*m2^4*Ycut - 
     96*m2^5*Ycut + 4*m2^6*Ycut + 24*MBhat*Ycut - 448*m2*MBhat*Ycut - 
     4640*m2^2*MBhat*Ycut + 1600*m2^3*MBhat*Ycut + 1840*m2^4*MBhat*Ycut - 
     64*m2^5*MBhat*Ycut + 8*m2^6*MBhat*Ycut - 56*MBhat^2*Ycut + 
     792*m2*MBhat^2*Ycut + 4200*m2^2*MBhat^2*Ycut - 3240*m2^3*MBhat^2*Ycut - 
     360*m2^4*MBhat^2*Ycut - 24*m2^5*MBhat^2*Ycut + 8*m2^6*MBhat^2*Ycut + 
     56*MBhat^3*Ycut - 600*m2*MBhat^3*Ycut - 1320*m2^2*MBhat^3*Ycut + 
     1480*m2^3*MBhat^3*Ycut - 120*m2^4*MBhat^3*Ycut + 24*m2^5*MBhat^3*Ycut - 
     20*MBhat^4*Ycut + 160*m2*MBhat^4*Ycut + 60*m2^2*MBhat^4*Ycut - 
     160*m2^3*MBhat^4*Ycut + 20*m2^4*MBhat^4*Ycut + 6*Ycut^2 - 
     144*m2*Ycut^2 - 2880*m2^2*Ycut^2 - 1680*m2^3*Ycut^2 + 1620*m2^4*Ycut^2 + 
     144*m2^5*Ycut^2 - 6*m2^6*Ycut^2 - 36*MBhat*Ycut^2 + 
     672*m2*MBhat*Ycut^2 + 8160*m2^2*MBhat*Ycut^2 - 480*m2^3*MBhat*Ycut^2 - 
     2520*m2^4*MBhat*Ycut^2 + 96*m2^5*MBhat*Ycut^2 - 12*m2^6*MBhat*Ycut^2 + 
     84*MBhat^2*Ycut^2 - 1188*m2*MBhat^2*Ycut^2 - 7740*m2^2*MBhat^2*Ycut^2 + 
     3660*m2^3*MBhat^2*Ycut^2 + 540*m2^4*MBhat^2*Ycut^2 + 
     36*m2^5*MBhat^2*Ycut^2 - 12*m2^6*MBhat^2*Ycut^2 - 84*MBhat^3*Ycut^2 + 
     900*m2*MBhat^3*Ycut^2 + 2700*m2^2*MBhat^3*Ycut^2 - 
     1980*m2^3*MBhat^3*Ycut^2 + 180*m2^4*MBhat^3*Ycut^2 - 
     36*m2^5*MBhat^3*Ycut^2 + 30*MBhat^4*Ycut^2 - 240*m2*MBhat^4*Ycut^2 - 
     210*m2^2*MBhat^4*Ycut^2 + 240*m2^3*MBhat^4*Ycut^2 - 
     30*m2^4*MBhat^4*Ycut^2 - 4*Ycut^3 + 96*m2*Ycut^3 + 2280*m2^2*Ycut^3 + 
     2080*m2^3*Ycut^3 - 720*m2^4*Ycut^3 - 96*m2^5*Ycut^3 + 4*m2^6*Ycut^3 + 
     24*MBhat*Ycut^3 - 448*m2*MBhat*Ycut^3 - 6640*m2^2*MBhat*Ycut^3 - 
     1600*m2^3*MBhat*Ycut^3 + 1440*m2^4*MBhat*Ycut^3 - 64*m2^5*MBhat*Ycut^3 + 
     8*m2^6*MBhat*Ycut^3 - 66*MBhat^2*Ycut^3 + 822*m2*MBhat^2*Ycut^3 + 
     6580*m2^2*MBhat^2*Ycut^3 - 1260*m2^3*MBhat^2*Ycut^3 - 
     330*m2^4*MBhat^2*Ycut^3 - 34*m2^5*MBhat^2*Ycut^3 + 
     8*m2^6*MBhat^2*Ycut^3 + 76*MBhat^3*Ycut^3 - 640*m2*MBhat^3*Ycut^3 - 
     2520*m2^2*MBhat^3*Ycut^3 + 1120*m2^3*MBhat^3*Ycut^3 - 
     140*m2^4*MBhat^3*Ycut^3 + 24*m2^5*MBhat^3*Ycut^3 - 30*MBhat^4*Ycut^3 + 
     170*m2*MBhat^4*Ycut^3 + 270*m2^2*MBhat^4*Ycut^3 - 
     170*m2^3*MBhat^4*Ycut^3 + 20*m2^4*MBhat^4*Ycut^3 + Ycut^4 - 
     24*m2*Ycut^4 - 750*m2^2*Ycut^4 - 1000*m2^3*Ycut^4 + 24*m2^5*Ycut^4 - 
     m2^6*Ycut^4 + 4*MBhat*Ycut^4 + 82*m2*MBhat*Ycut^4 + 
     2280*m2^2*MBhat*Ycut^4 + 1380*m2^3*MBhat*Ycut^4 - 
     270*m2^4*MBhat*Ycut^4 + 26*m2^5*MBhat*Ycut^4 - 2*m2^6*MBhat*Ycut^4 + 
     39*MBhat^2*Ycut^4 - 278*m2*MBhat^2*Ycut^4 - 2290*m2^2*MBhat^2*Ycut^4 - 
     300*m2^3*MBhat^2*Ycut^4 + 65*m2^4*MBhat^2*Ycut^4 + 
     16*m2^5*MBhat^2*Ycut^4 - 2*m2^6*MBhat^2*Ycut^4 - 94*MBhat^3*Ycut^4 + 
     300*m2*MBhat^3*Ycut^4 + 960*m2^2*MBhat^3*Ycut^4 - 
     220*m2^3*MBhat^3*Ycut^4 + 60*m2^4*MBhat^3*Ycut^4 - 
     6*m2^5*MBhat^3*Ycut^4 + 50*MBhat^4*Ycut^4 - 80*m2*MBhat^4*Ycut^4 - 
     150*m2^2*MBhat^4*Ycut^4 + 60*m2^3*MBhat^4*Ycut^4 - 
     5*m2^4*MBhat^4*Ycut^4 - 3*Ycut^5 + 9*m2*Ycut^5 + 30*m2^2*Ycut^5 + 
     90*m2^3*Ycut^5 + 45*m2^4*Ycut^5 - 3*m2^5*Ycut^5 - 40*MBhat*Ycut^5 + 
     108*m2*MBhat*Ycut^5 - 200*m2^2*MBhat*Ycut^5 - 200*m2^3*MBhat*Ycut^5 - 
     4*m2^5*MBhat*Ycut^5 + 9*MBhat^2*Ycut^5 + 23*m2*MBhat^2*Ycut^5 + 
     60*m2^2*MBhat^2*Ycut^5 + 180*m2^3*MBhat^2*Ycut^5 - 
     5*m2^4*MBhat^2*Ycut^5 - 3*m2^5*MBhat^2*Ycut^5 + 114*MBhat^3*Ycut^5 - 
     200*m2*MBhat^3*Ycut^5 - 10*m2^4*MBhat^3*Ycut^5 - 80*MBhat^4*Ycut^5 + 
     60*m2*MBhat^4*Ycut^5 + 30*m2^2*MBhat^4*Ycut^5 - 10*m2^3*MBhat^4*Ycut^5 + 
     14*Ycut^6 - 36*m2*Ycut^6 + 30*m2^2*Ycut^6 + 20*m2^3*Ycut^6 + 
     56*MBhat*Ycut^6 - 132*m2*MBhat*Ycut^6 + 80*m2^2*MBhat*Ycut^6 - 
     60*m2^3*MBhat*Ycut^6 - 84*MBhat^2*Ycut^6 + 108*m2*MBhat^2*Ycut^6 + 
     20*m2^2*MBhat^2*Ycut^6 - 56*MBhat^3*Ycut^6 + 100*m2*MBhat^3*Ycut^6 - 
     60*m2^2*MBhat^3*Ycut^6 + 70*MBhat^4*Ycut^6 - 40*m2*MBhat^4*Ycut^6 - 
     26*Ycut^7 + 54*m2*Ycut^7 - 30*m2^2*Ycut^7 + 10*m2^3*Ycut^7 - 
     24*MBhat*Ycut^7 + 48*m2*MBhat*Ycut^7 - 40*m2^2*MBhat*Ycut^7 + 
     96*MBhat^2*Ycut^7 - 112*m2*MBhat^2*Ycut^7 + 40*m2^2*MBhat^2*Ycut^7 - 
     16*MBhat^3*Ycut^7 - 30*MBhat^4*Ycut^7 + 10*m2*MBhat^4*Ycut^7 + 
     24*Ycut^8 - 36*m2*Ycut^8 + 15*m2^2*Ycut^8 - 14*MBhat*Ycut^8 + 
     18*m2*MBhat*Ycut^8 - 10*m2^2*MBhat*Ycut^8 - 39*MBhat^2*Ycut^8 + 
     28*m2*MBhat^2*Ycut^8 + 24*MBhat^3*Ycut^8 - 10*m2*MBhat^3*Ycut^8 + 
     5*MBhat^4*Ycut^8 - 11*Ycut^9 + 9*m2*Ycut^9 + 16*MBhat*Ycut^9 - 
     12*m2*MBhat*Ycut^9 + MBhat^2*Ycut^9 + 3*m2*MBhat^2*Ycut^9 - 
     6*MBhat^3*Ycut^9 + 2*Ycut^10 - 4*MBhat*Ycut^10 + 2*MBhat^2*Ycut^10 + 
     20*api*MBhat^4*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^4*Ycut*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     120*api*MBhat^4*Ycut^2*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^4*Ycut^3*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     20*api*MBhat^4*Ycut^4*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^3*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat^3*Ycut*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     480*api*MBhat^3*Ycut^2*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat^3*Ycut^3*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat^3*Ycut^4*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     80*api*MBhat^2*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     320*api*MBhat^2*Ycut*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     480*api*MBhat^2*Ycut^2*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     320*api*MBhat^2*Ycut^3*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     80*api*MBhat^2*Ycut^4*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     40*api*MBhat^2*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     160*api*MBhat^2*Ycut*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat^2*Ycut^2*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     160*api*MBhat^2*Ycut^3*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     40*api*MBhat^2*Ycut^4*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat*Ycut*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     480*api*MBhat*Ycut^2*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     320*api*MBhat*Ycut^3*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*MBhat*Ycut^4*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     20*api*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*Ycut*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     120*api*Ycut^2*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     80*api*Ycut^3*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     20*api*Ycut^4*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat])/
    (20*(-1 + Ycut)^4)) + 
 c[SL]*(c[SR]*(12*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 4*MBhat - 
       16*m2*MBhat - 8*m2^2*MBhat + 6*MBhat^2 + 15*m2*MBhat^2 + 
       3*m2^2*MBhat^2 - 4*MBhat^3 - 6*m2*MBhat^3 + MBhat^4 + m2*MBhat^4)*
      Log[m2] - 12*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 4*MBhat - 
       16*m2*MBhat - 8*m2^2*MBhat + 6*MBhat^2 + 15*m2*MBhat^2 + 
       3*m2^2*MBhat^2 - 4*MBhat^3 - 6*m2*MBhat^3 + MBhat^4 + m2*MBhat^4)*
      Log[1 - Ycut] - (3*Sqrt[m2] + 175*m2^(3/2) + 300*m2^(5/2) - 
       300*m2^(7/2) - 175*m2^(9/2) - 3*m2^(11/2) - 16*Sqrt[m2]*MBhat - 
       620*m2^(3/2)*MBhat - 320*m2^(5/2)*MBhat + 880*m2^(7/2)*MBhat + 
       80*m2^(9/2)*MBhat - 4*m2^(11/2)*MBhat + 33*Sqrt[m2]*MBhat^2 + 
       795*m2^(3/2)*MBhat^2 - 240*m2^(5/2)*MBhat^2 - 600*m2^(7/2)*MBhat^2 + 
       15*m2^(9/2)*MBhat^2 - 3*m2^(11/2)*MBhat^2 - 30*Sqrt[m2]*MBhat^3 - 
       440*m2^(3/2)*MBhat^3 + 360*m2^(5/2)*MBhat^3 + 120*m2^(7/2)*MBhat^3 - 
       10*m2^(9/2)*MBhat^3 + 10*Sqrt[m2]*MBhat^4 + 90*m2^(3/2)*MBhat^4 - 
       90*m2^(5/2)*MBhat^4 - 10*m2^(7/2)*MBhat^4 - 9*Sqrt[m2]*Ycut - 
       585*m2^(3/2)*Ycut - 1260*m2^(5/2)*Ycut + 540*m2^(7/2)*Ycut + 
       465*m2^(9/2)*Ycut + 9*m2^(11/2)*Ycut + 48*Sqrt[m2]*MBhat*Ycut + 
       2100*m2^(3/2)*MBhat*Ycut + 1920*m2^(5/2)*MBhat*Ycut - 
       2160*m2^(7/2)*MBhat*Ycut - 240*m2^(9/2)*MBhat*Ycut + 
       12*m2^(11/2)*MBhat*Ycut - 99*Sqrt[m2]*MBhat^2*Ycut - 
       2745*m2^(3/2)*MBhat^2*Ycut - 180*m2^(5/2)*MBhat^2*Ycut + 
       1620*m2^(7/2)*MBhat^2*Ycut - 45*m2^(9/2)*MBhat^2*Ycut + 
       9*m2^(11/2)*MBhat^2*Ycut + 90*Sqrt[m2]*MBhat^3*Ycut + 
       1560*m2^(3/2)*MBhat^3*Ycut - 720*m2^(5/2)*MBhat^3*Ycut - 
       360*m2^(7/2)*MBhat^3*Ycut + 30*m2^(9/2)*MBhat^3*Ycut - 
       30*Sqrt[m2]*MBhat^4*Ycut - 330*m2^(3/2)*MBhat^4*Ycut + 
       210*m2^(5/2)*MBhat^4*Ycut + 30*m2^(7/2)*MBhat^4*Ycut + 
       9*Sqrt[m2]*Ycut^2 + 675*m2^(3/2)*Ycut^2 + 1800*m2^(5/2)*Ycut^2 - 
       375*m2^(9/2)*Ycut^2 - 9*m2^(11/2)*Ycut^2 - 48*Sqrt[m2]*MBhat*Ycut^2 - 
       2460*m2^(3/2)*MBhat*Ycut^2 - 3360*m2^(5/2)*MBhat*Ycut^2 + 
       1440*m2^(7/2)*MBhat*Ycut^2 + 240*m2^(9/2)*MBhat*Ycut^2 - 
       12*m2^(11/2)*MBhat*Ycut^2 + 99*Sqrt[m2]*MBhat^2*Ycut^2 + 
       3285*m2^(3/2)*MBhat^2*Ycut^2 + 1530*m2^(5/2)*MBhat^2*Ycut^2 - 
       1350*m2^(7/2)*MBhat^2*Ycut^2 + 45*m2^(9/2)*MBhat^2*Ycut^2 - 
       9*m2^(11/2)*MBhat^2*Ycut^2 - 90*Sqrt[m2]*MBhat^3*Ycut^2 - 
       1920*m2^(3/2)*MBhat^3*Ycut^2 + 180*m2^(5/2)*MBhat^3*Ycut^2 + 
       360*m2^(7/2)*MBhat^3*Ycut^2 - 30*m2^(9/2)*MBhat^3*Ycut^2 + 
       30*Sqrt[m2]*MBhat^4*Ycut^2 + 420*m2^(3/2)*MBhat^4*Ycut^2 - 
       120*m2^(5/2)*MBhat^4*Ycut^2 - 30*m2^(7/2)*MBhat^4*Ycut^2 - 
       3*Sqrt[m2]*Ycut^3 - 285*m2^(3/2)*Ycut^3 - 960*m2^(5/2)*Ycut^3 - 
       360*m2^(7/2)*Ycut^3 + 65*m2^(9/2)*Ycut^3 + 3*m2^(11/2)*Ycut^3 + 
       16*Sqrt[m2]*MBhat*Ycut^3 + 1060*m2^(3/2)*MBhat*Ycut^3 + 
       2080*m2^(5/2)*MBhat*Ycut^3 - 80*m2^(9/2)*MBhat*Ycut^3 + 
       4*m2^(11/2)*MBhat*Ycut^3 - 43*Sqrt[m2]*MBhat^2*Ycut^3 - 
       1415*m2^(3/2)*MBhat^2*Ycut^3 - 1470*m2^(5/2)*MBhat^2*Ycut^3 + 
       310*m2^(7/2)*MBhat^2*Ycut^3 - 25*m2^(9/2)*MBhat^2*Ycut^3 + 
       3*m2^(11/2)*MBhat^2*Ycut^3 + 50*Sqrt[m2]*MBhat^3*Ycut^3 + 
       820*m2^(3/2)*MBhat^3*Ycut^3 + 360*m2^(5/2)*MBhat^3*Ycut^3 - 
       140*m2^(7/2)*MBhat^3*Ycut^3 + 10*m2^(9/2)*MBhat^3*Ycut^3 - 
       20*Sqrt[m2]*MBhat^4*Ycut^3 - 180*m2^(3/2)*MBhat^4*Ycut^3 - 
       30*m2^(5/2)*MBhat^4*Ycut^3 + 10*m2^(7/2)*MBhat^4*Ycut^3 + 
       15*m2^(3/2)*Ycut^4 + 90*m2^(5/2)*Ycut^4 + 90*m2^(7/2)*Ycut^4 + 
       15*m2^(9/2)*Ycut^4 + 10*Sqrt[m2]*MBhat*Ycut^4 - 
       100*m2^(3/2)*MBhat*Ycut^4 - 180*m2^(5/2)*MBhat*Ycut^4 - 
       160*m2^(7/2)*MBhat*Ycut^4 + 10*m2^(9/2)*MBhat*Ycut^4 + 
       10*Sqrt[m2]*MBhat^2*Ycut^4 + 55*m2^(3/2)*MBhat^2*Ycut^4 + 
       270*m2^(5/2)*MBhat^2*Ycut^4 + 20*m2^(7/2)*MBhat^2*Ycut^4 + 
       5*m2^(9/2)*MBhat^2*Ycut^4 - 50*Sqrt[m2]*MBhat^3*Ycut^4 + 
       60*m2^(3/2)*MBhat^3*Ycut^4 - 180*m2^(5/2)*MBhat^3*Ycut^4 + 
       20*m2^(7/2)*MBhat^3*Ycut^4 + 30*Sqrt[m2]*MBhat^4*Ycut^4 - 
       30*m2^(3/2)*MBhat^4*Ycut^4 + 30*m2^(5/2)*MBhat^4*Ycut^4 - 
       3*Sqrt[m2]*Ycut^5 + 15*m2^(3/2)*Ycut^5 + 30*m2^(7/2)*Ycut^5 - 
       24*Sqrt[m2]*MBhat*Ycut^5 + 60*m2^(3/2)*MBhat*Ycut^5 - 
       120*m2^(5/2)*MBhat*Ycut^5 + 27*Sqrt[m2]*MBhat^2*Ycut^5 - 
       45*m2^(3/2)*MBhat^2*Ycut^5 + 90*m2^(5/2)*MBhat^2*Ycut^5 + 
       30*Sqrt[m2]*MBhat^3*Ycut^5 - 60*m2^(3/2)*MBhat^3*Ycut^5 - 
       30*Sqrt[m2]*MBhat^4*Ycut^5 + 30*m2^(3/2)*MBhat^4*Ycut^5 + 
       9*Sqrt[m2]*Ycut^6 - 25*m2^(3/2)*Ycut^6 + 30*m2^(5/2)*Ycut^6 + 
       12*Sqrt[m2]*MBhat*Ycut^6 - 20*m2^(3/2)*MBhat*Ycut^6 - 
       20*m2^(5/2)*MBhat*Ycut^6 - 41*Sqrt[m2]*MBhat^2*Ycut^6 + 
       65*m2^(3/2)*MBhat^2*Ycut^6 + 10*Sqrt[m2]*MBhat^3*Ycut^6 - 
       20*m2^(3/2)*MBhat^3*Ycut^6 + 10*Sqrt[m2]*MBhat^4*Ycut^6 - 
       9*Sqrt[m2]*Ycut^7 + 15*m2^(3/2)*Ycut^7 + 8*Sqrt[m2]*MBhat*Ycut^7 - 
       20*m2^(3/2)*MBhat*Ycut^7 + 11*Sqrt[m2]*MBhat^2*Ycut^7 + 
       5*m2^(3/2)*MBhat^2*Ycut^7 - 10*Sqrt[m2]*MBhat^3*Ycut^7 + 
       3*Sqrt[m2]*Ycut^8 - 6*Sqrt[m2]*MBhat*Ycut^8 + 3*Sqrt[m2]*MBhat^2*
        Ycut^8 + 5*api*MBhat^4*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       15*api*MBhat^4*Ycut*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       15*api*MBhat^4*Ycut^2*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       5*api*MBhat^4*Ycut^3*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       20*api*MBhat^3*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^3*Ycut*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat^3*Ycut^2*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*MBhat^3*Ycut^3*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*MBhat^2*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat^2*Ycut*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^2*Ycut^2*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       20*api*MBhat^2*Ycut^3*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       10*api*MBhat^2*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       30*api*MBhat^2*Ycut*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       30*api*MBhat^2*Ycut^2*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       10*api*MBhat^2*Ycut^3*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       20*api*MBhat*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat*Ycut*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat*Ycut^2*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*MBhat*Ycut^3*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       5*api*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       15*api*Ycut*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       15*api*Ycut^2*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       5*api*Ycut^3*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat])/
      (5*(-1 + Ycut)^3)) - (c[T]*(-10*MBhat^2*Ycut^3 + 50*m2*MBhat^2*Ycut^3 - 
      100*m2^2*MBhat^2*Ycut^3 + 100*m2^3*MBhat^2*Ycut^3 - 
      50*m2^4*MBhat^2*Ycut^3 + 10*m2^5*MBhat^2*Ycut^3 + 20*MBhat^3*Ycut^3 - 
      80*m2*MBhat^3*Ycut^3 + 120*m2^2*MBhat^3*Ycut^3 - 
      80*m2^3*MBhat^3*Ycut^3 + 20*m2^4*MBhat^3*Ycut^3 - 10*MBhat^4*Ycut^3 + 
      30*m2*MBhat^4*Ycut^3 - 30*m2^2*MBhat^4*Ycut^3 + 
      10*m2^3*MBhat^4*Ycut^3 + 10*MBhat*Ycut^4 - 50*m2*MBhat*Ycut^4 + 
      100*m2^2*MBhat*Ycut^4 - 100*m2^3*MBhat*Ycut^4 + 50*m2^4*MBhat*Ycut^4 - 
      10*m2^5*MBhat*Ycut^4 + 30*MBhat^2*Ycut^4 - 130*m2*MBhat^2*Ycut^4 + 
      220*m2^2*MBhat^2*Ycut^4 - 180*m2^3*MBhat^2*Ycut^4 + 
      70*m2^4*MBhat^2*Ycut^4 - 10*m2^5*MBhat^2*Ycut^4 - 90*MBhat^3*Ycut^4 + 
      300*m2*MBhat^3*Ycut^4 - 360*m2^2*MBhat^3*Ycut^4 + 
      180*m2^3*MBhat^3*Ycut^4 - 30*m2^4*MBhat^3*Ycut^4 + 50*MBhat^4*Ycut^4 - 
      120*m2*MBhat^4*Ycut^4 + 90*m2^2*MBhat^4*Ycut^4 - 
      20*m2^3*MBhat^4*Ycut^4 - 3*Ycut^5 + 15*m2*Ycut^5 - 30*m2^2*Ycut^5 + 
      30*m2^3*Ycut^5 - 15*m2^4*Ycut^5 + 3*m2^5*Ycut^5 - 44*MBhat*Ycut^5 + 
      180*m2*MBhat*Ycut^5 - 280*m2^2*MBhat*Ycut^5 + 200*m2^3*MBhat*Ycut^5 - 
      60*m2^4*MBhat*Ycut^5 + 4*m2^5*MBhat*Ycut^5 - 3*MBhat^2*Ycut^5 + 
      25*m2*MBhat^2*Ycut^5 - 60*m2^2*MBhat^2*Ycut^5 + 
      60*m2^3*MBhat^2*Ycut^5 - 25*m2^4*MBhat^2*Ycut^5 + 
      3*m2^5*MBhat^2*Ycut^5 + 150*MBhat^3*Ycut^5 - 400*m2*MBhat^3*Ycut^5 + 
      360*m2^2*MBhat^3*Ycut^5 - 120*m2^3*MBhat^3*Ycut^5 + 
      10*m2^4*MBhat^3*Ycut^5 - 100*MBhat^4*Ycut^5 + 180*m2*MBhat^4*Ycut^5 - 
      90*m2^2*MBhat^4*Ycut^5 + 10*m2^3*MBhat^4*Ycut^5 + 15*Ycut^6 - 
      60*m2*Ycut^6 + 90*m2^2*Ycut^6 - 60*m2^3*Ycut^6 + 15*m2^4*Ycut^6 + 
      70*MBhat*Ycut^6 - 220*m2*MBhat*Ycut^6 + 240*m2^2*MBhat*Ycut^6 - 
      100*m2^3*MBhat*Ycut^6 + 10*m2^4*MBhat*Ycut^6 - 85*MBhat^2*Ycut^6 + 
      200*m2*MBhat^2*Ycut^6 - 140*m2^2*MBhat^2*Ycut^6 + 
      20*m2^3*MBhat^2*Ycut^6 + 5*m2^4*MBhat^2*Ycut^6 - 100*MBhat^3*Ycut^6 + 
      200*m2*MBhat^3*Ycut^6 - 120*m2^2*MBhat^3*Ycut^6 + 
      20*m2^3*MBhat^3*Ycut^6 + 100*MBhat^4*Ycut^6 - 120*m2*MBhat^4*Ycut^6 + 
      30*m2^2*MBhat^4*Ycut^6 - 30*Ycut^7 + 90*m2*Ycut^7 - 90*m2^2*Ycut^7 + 
      30*m2^3*Ycut^7 - 40*MBhat*Ycut^7 + 80*m2*MBhat*Ycut^7 - 
      40*m2^2*MBhat*Ycut^7 + 120*MBhat^2*Ycut^7 - 200*m2*MBhat^2*Ycut^7 + 
      80*m2^2*MBhat^2*Ycut^7 - 50*MBhat^4*Ycut^7 + 30*m2*MBhat^4*Ycut^7 + 
      30*Ycut^8 - 60*m2*Ycut^8 + 30*m2^2*Ycut^8 - 10*MBhat*Ycut^8 + 
      30*m2*MBhat*Ycut^8 - 20*m2^2*MBhat*Ycut^8 - 60*MBhat^2*Ycut^8 + 
      50*m2*MBhat^2*Ycut^8 + 30*MBhat^3*Ycut^8 - 20*m2*MBhat^3*Ycut^8 + 
      10*MBhat^4*Ycut^8 - 15*Ycut^9 + 15*m2*Ycut^9 + 20*MBhat*Ycut^9 - 
      20*m2*MBhat*Ycut^9 + 5*MBhat^2*Ycut^9 + 5*m2*MBhat^2*Ycut^9 - 
      10*MBhat^3*Ycut^9 + 3*Ycut^10 - 6*MBhat*Ycut^10 + 3*MBhat^2*Ycut^10 - 
      5*api*MBhat^4*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*MBhat^4*Ycut*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      30*api*MBhat^4*Ycut^2*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*MBhat^4*Ycut^3*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      5*api*MBhat^4*Ycut^4*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*MBhat^3*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      80*api*MBhat^3*Ycut*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      120*api*MBhat^3*Ycut^2*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      80*api*MBhat^3*Ycut^3*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*MBhat^3*Ycut^4*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      20*api*MBhat^2*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      80*api*MBhat^2*Ycut*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      120*api*MBhat^2*Ycut^2*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      80*api*MBhat^2*Ycut^3*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      20*api*MBhat^2*Ycut^4*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      10*api*MBhat^2*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      40*api*MBhat^2*Ycut*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      60*api*MBhat^2*Ycut^2*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      40*api*MBhat^2*Ycut^3*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      10*api*MBhat^2*Ycut^4*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*MBhat*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      80*api*MBhat*Ycut*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      120*api*MBhat*Ycut^2*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      80*api*MBhat*Ycut^3*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*MBhat*Ycut^4*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      5*api*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*Ycut*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      30*api*Ycut^2*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      20*api*Ycut^3*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      5*api*Ycut^4*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat]))/
    (5*(-1 + Ycut)^4)) + 
 c[T]^2*(16*m2^2*(-15 - 40*m2 - 15*m2^2 + 54*MBhat + 80*m2*MBhat + 
     6*m2^2*MBhat - 72*MBhat^2 - 46*m2*MBhat^2 + 42*MBhat^3 + 6*m2*MBhat^3 - 
     9*MBhat^4)*Log[m2] - 16*m2^2*(-15 - 40*m2 - 15*m2^2 + 54*MBhat + 
     80*m2*MBhat + 6*m2^2*MBhat - 72*MBhat^2 - 46*m2*MBhat^2 + 42*MBhat^3 + 
     6*m2*MBhat^3 - 9*MBhat^4)*Log[1 - Ycut] + 
   (20 - 480*m2 - 7500*m2^2 + 7500*m2^4 + 480*m2^5 - 20*m2^6 - 136*MBhat + 
     2496*m2*MBhat + 21240*m2^2*MBhat - 15360*m2^3*MBhat - 8760*m2^4*MBhat + 
     576*m2^5*MBhat - 56*m2^6*MBhat + 392*MBhat^2 - 5256*m2*MBhat^2 - 
     18360*m2^2*MBhat^2 + 23040*m2^3*MBhat^2 - 360*m2^4*MBhat^2 + 
     648*m2^5*MBhat^2 - 104*m2^6*MBhat^2 - 456*MBhat^3 + 4680*m2*MBhat^3 + 
     4800*m2^2*MBhat^3 - 10560*m2^3*MBhat^3 + 1800*m2^4*MBhat^3 - 
     264*m2^5*MBhat^3 + 180*MBhat^4 - 1440*m2*MBhat^4 + 1440*m2^3*MBhat^4 - 
     180*m2^4*MBhat^4 - 80*Ycut + 1920*m2*Ycut + 33600*m2^2*Ycut + 
     9600*m2^3*Ycut - 26400*m2^4*Ycut - 1920*m2^5*Ycut + 80*m2^6*Ycut + 
     544*MBhat*Ycut - 9984*m2*MBhat*Ycut - 97920*m2^2*MBhat*Ycut + 
     42240*m2^3*MBhat*Ycut + 33600*m2^4*MBhat*Ycut - 2304*m2^5*MBhat*Ycut + 
     224*m2^6*MBhat*Ycut - 1568*MBhat^2*Ycut + 21024*m2*MBhat^2*Ycut + 
     90720*m2^2*MBhat^2*Ycut - 81120*m2^3*MBhat^2*Ycut + 
     1440*m2^4*MBhat^2*Ycut - 2592*m2^5*MBhat^2*Ycut + 
     416*m2^6*MBhat^2*Ycut + 1824*MBhat^3*Ycut - 18720*m2*MBhat^3*Ycut - 
     29280*m2^2*MBhat^3*Ycut + 40800*m2^3*MBhat^3*Ycut - 
     7200*m2^4*MBhat^3*Ycut + 1056*m2^5*MBhat^3*Ycut - 720*MBhat^4*Ycut + 
     5760*m2*MBhat^4*Ycut + 2160*m2^2*MBhat^4*Ycut - 5760*m2^3*MBhat^4*Ycut + 
     720*m2^4*MBhat^4*Ycut + 120*Ycut^2 - 2880*m2*Ycut^2 - 
     57600*m2^2*Ycut^2 - 33600*m2^3*Ycut^2 + 32400*m2^4*Ycut^2 + 
     2880*m2^5*Ycut^2 - 120*m2^6*Ycut^2 - 816*MBhat*Ycut^2 + 
     14976*m2*MBhat*Ycut^2 + 172800*m2^2*MBhat*Ycut^2 - 
     24960*m2^3*MBhat*Ycut^2 - 47520*m2^4*MBhat*Ycut^2 + 
     3456*m2^5*MBhat*Ycut^2 - 336*m2^6*MBhat*Ycut^2 + 2352*MBhat^2*Ycut^2 - 
     31536*m2*MBhat^2*Ycut^2 - 170640*m2^2*MBhat^2*Ycut^2 + 
     99600*m2^3*MBhat^2*Ycut^2 - 2160*m2^4*MBhat^2*Ycut^2 + 
     3888*m2^5*MBhat^2*Ycut^2 - 624*m2^6*MBhat^2*Ycut^2 - 
     2736*MBhat^3*Ycut^2 + 28080*m2*MBhat^3*Ycut^2 + 
     64080*m2^2*MBhat^3*Ycut^2 - 58320*m2^3*MBhat^3*Ycut^2 + 
     10800*m2^4*MBhat^3*Ycut^2 - 1584*m2^5*MBhat^3*Ycut^2 + 
     1080*MBhat^4*Ycut^2 - 8640*m2*MBhat^4*Ycut^2 - 
     7560*m2^2*MBhat^4*Ycut^2 + 8640*m2^3*MBhat^4*Ycut^2 - 
     1080*m2^4*MBhat^4*Ycut^2 - 80*Ycut^3 + 1920*m2*Ycut^3 + 
     45600*m2^2*Ycut^3 + 41600*m2^3*Ycut^3 - 14400*m2^4*Ycut^3 - 
     1920*m2^5*Ycut^3 + 80*m2^6*Ycut^3 + 544*MBhat*Ycut^3 - 
     9984*m2*MBhat*Ycut^3 - 141120*m2^2*MBhat*Ycut^3 - 
     21760*m2^3*MBhat*Ycut^3 + 28800*m2^4*MBhat*Ycut^3 - 
     2304*m2^5*MBhat*Ycut^3 + 224*m2^6*MBhat*Ycut^3 - 2168*MBhat^2*Ycut^3 + 
     23304*m2*MBhat^2*Ycut^3 + 145200*m2^2*MBhat^2*Ycut^3 - 
     42640*m2^3*MBhat^2*Ycut^3 + 1320*m2^4*MBhat^2*Ycut^3 - 
     2712*m2^5*MBhat^2*Ycut^3 + 416*m2^6*MBhat^2*Ycut^3 + 
     3024*MBhat^3*Ycut^3 - 22080*m2*MBhat^3*Ycut^3 - 
     60000*m2^2*MBhat^3*Ycut^3 + 35520*m2^3*MBhat^3*Ycut^3 - 
     7440*m2^4*MBhat^3*Ycut^3 + 1056*m2^5*MBhat^3*Ycut^3 - 
     1320*MBhat^4*Ycut^3 + 6840*m2*MBhat^4*Ycut^3 + 
     9000*m2^2*MBhat^4*Ycut^3 - 5880*m2^3*MBhat^4*Ycut^3 + 
     720*m2^4*MBhat^4*Ycut^3 + 20*Ycut^4 - 480*m2*Ycut^4 - 
     15000*m2^2*Ycut^4 - 20000*m2^3*Ycut^4 + 480*m2^5*Ycut^4 - 
     20*m2^6*Ycut^4 + 224*MBhat*Ycut^4 + 1176*m2*MBhat*Ycut^4 + 
     49920*m2^2*MBhat*Ycut^4 + 23920*m2^3*MBhat*Ycut^4 - 
     5880*m2^4*MBhat*Ycut^4 + 696*m2^5*MBhat*Ycut^4 - 56*m2^6*MBhat*Ycut^4 + 
     2492*MBhat^2*Ycut^4 - 12936*m2*MBhat^2*Ycut^4 - 
     44040*m2^2*MBhat^2*Ycut^4 - 5840*m2^3*MBhat^2*Ycut^4 + 
     660*m2^4*MBhat^2*Ycut^4 + 768*m2^5*MBhat^2*Ycut^4 - 
     104*m2^6*MBhat^2*Ycut^4 - 5736*MBhat^3*Ycut^4 + 
     18000*m2*MBhat^3*Ycut^4 + 15360*m2^2*MBhat^3*Ycut^4 - 
     5520*m2^3*MBhat^3*Ycut^4 + 2160*m2^4*MBhat^3*Ycut^4 - 
     264*m2^5*MBhat^3*Ycut^4 + 3000*MBhat^4*Ycut^4 - 5760*m2*MBhat^4*Ycut^4 - 
     3240*m2^2*MBhat^4*Ycut^4 + 1680*m2^3*MBhat^4*Ycut^4 - 
     180*m2^4*MBhat^4*Ycut^4 - 84*Ycut^5 + 300*m2*Ycut^5 + 360*m2^2*Ycut^5 + 
     2040*m2^3*Ycut^5 + 780*m2^4*Ycut^5 - 36*m2^5*Ycut^5 - 
     1536*MBhat*Ycut^5 + 4944*m2*MBhat*Ycut^5 - 8160*m2^2*MBhat*Ycut^5 - 
     1440*m2^3*MBhat*Ycut^5 - 480*m2^4*MBhat*Ycut^5 - 48*m2^5*MBhat*Ycut^5 - 
     1956*MBhat^2*Ycut^5 + 7956*m2*MBhat^2*Ycut^5 - 
     7920*m2^2*MBhat^2*Ycut^5 + 8880*m2^3*MBhat^2*Ycut^5 - 
     1260*m2^4*MBhat^2*Ycut^5 - 36*m2^5*MBhat^2*Ycut^5 + 
     8856*MBhat^3*Ycut^5 - 19680*m2*MBhat^3*Ycut^5 + 
     11520*m2^2*MBhat^3*Ycut^5 - 2880*m2^3*MBhat^3*Ycut^5 - 
     120*m2^4*MBhat^3*Ycut^5 - 5280*MBhat^4*Ycut^5 + 6480*m2*MBhat^4*Ycut^5 - 
     1080*m2^2*MBhat^4*Ycut^5 - 120*m2^3*MBhat^4*Ycut^5 + 400*Ycut^6 - 
     1200*m2*Ycut^6 + 1320*m2^2*Ycut^6 - 80*m2^3*Ycut^6 + 120*m2^4*Ycut^6 + 
     2416*MBhat*Ycut^6 - 6576*m2*MBhat*Ycut^6 + 5760*m2^2*MBhat*Ycut^6 - 
     2960*m2^3*MBhat*Ycut^6 + 240*m2^4*MBhat*Ycut^6 - 1112*MBhat^2*Ycut^6 - 
     624*m2*MBhat^2*Ycut^6 + 4560*m2^2*MBhat^2*Ycut^6 - 
     2240*m2^3*MBhat^2*Ycut^6 + 360*m2^4*MBhat^2*Ycut^6 - 
     6624*MBhat^3*Ycut^6 + 12720*m2*MBhat^3*Ycut^6 - 
     7440*m2^2*MBhat^3*Ycut^6 + 960*m2^3*MBhat^3*Ycut^6 + 
     4920*MBhat^4*Ycut^6 - 4320*m2*MBhat^4*Ycut^6 + 720*m2^2*MBhat^4*Ycut^6 - 
     760*Ycut^7 + 1800*m2*Ycut^7 - 1320*m2^2*Ycut^7 + 440*m2^3*Ycut^7 - 
     1504*MBhat*Ycut^7 + 3264*m2*MBhat*Ycut^7 - 2400*m2^2*MBhat*Ycut^7 + 
     320*m2^3*MBhat*Ycut^7 + 3008*MBhat^2*Ycut^7 - 3264*m2*MBhat^2*Ycut^7 + 
     480*m2^2*MBhat^2*Ycut^7 + 320*m2^3*MBhat^2*Ycut^7 + 
     1536*MBhat^3*Ycut^7 - 2880*m2*MBhat^3*Ycut^7 + 960*m2^2*MBhat^3*Ycut^7 - 
     2280*MBhat^4*Ycut^7 + 1080*m2*MBhat^4*Ycut^7 + 720*Ycut^8 - 
     1200*m2*Ycut^8 + 540*m2^2*Ycut^8 - 24*MBhat*Ycut^8 + 
     24*m2*MBhat*Ycut^8 - 120*m2^2*MBhat*Ycut^8 - 1692*MBhat^2*Ycut^8 + 
     1296*m2*MBhat^2*Ycut^8 + 576*MBhat^3*Ycut^8 - 120*m2*MBhat^3*Ycut^8 + 
     420*MBhat^4*Ycut^8 - 340*Ycut^9 + 300*m2*Ycut^9 + 416*MBhat*Ycut^9 - 
     336*m2*MBhat*Ycut^9 + 188*MBhat^2*Ycut^9 + 36*m2*MBhat^2*Ycut^9 - 
     264*MBhat^3*Ycut^9 + 64*Ycut^10 - 128*MBhat*Ycut^10 + 
     64*MBhat^2*Ycut^10 + 15*api*MBhat^4*X1mix[0, 0, c[T]^2, Ycut, m2, 
       mu2hat] - 60*api*MBhat^4*Ycut*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] + 
     90*api*MBhat^4*Ycut^2*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^4*Ycut^3*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] + 
     15*api*MBhat^4*Ycut^4*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^3*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat^3*Ycut*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] - 
     360*api*MBhat^3*Ycut^2*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat^3*Ycut^3*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^3*Ycut^4*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^2*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] - 
     240*api*MBhat^2*Ycut*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] + 
     360*api*MBhat^2*Ycut^2*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] - 
     240*api*MBhat^2*Ycut^3*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut^4*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] + 
     30*api*MBhat^2*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     120*api*MBhat^2*Ycut*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] + 
     180*api*MBhat^2*Ycut^2*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     120*api*MBhat^2*Ycut^3*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] + 
     30*api*MBhat^2*Ycut^4*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat*Ycut*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] - 
     360*api*MBhat*Ycut^2*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat*Ycut^3*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat*Ycut^4*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     15*api*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*Ycut*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] + 
     90*api*Ycut^2*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] - 
     60*api*Ycut^3*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] + 
     15*api*Ycut^4*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat])/
    (15*(-1 + Ycut)^4)) + 
 c[VL]^2*(4*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 32*m2*MBhat + 
     3*m2^2*MBhat - 27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 3*m2*MBhat^3 - 
     3*MBhat^4)*Log[m2] - 4*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 
     32*m2*MBhat + 3*m2^2*MBhat - 27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 
     3*m2*MBhat^3 - 3*MBhat^4)*Log[1 - Ycut] + 
   (2 - 48*m2 - 750*m2^2 + 750*m2^4 + 48*m2^5 - 2*m2^6 - 13*MBhat + 
     240*m2*MBhat + 2085*m2^2*MBhat - 1440*m2^3*MBhat - 915*m2^4*MBhat + 
     48*m2^5*MBhat - 5*m2^6*MBhat + 35*MBhat^2 - 477*m2*MBhat^2 - 
     1800*m2^2*MBhat^2 + 2160*m2^3*MBhat^2 + 45*m2^4*MBhat^2 + 
     45*m2^5*MBhat^2 - 8*m2^6*MBhat^2 - 39*MBhat^3 + 405*m2*MBhat^3 + 
     480*m2^2*MBhat^3 - 960*m2^3*MBhat^3 + 135*m2^4*MBhat^3 - 
     21*m2^5*MBhat^3 + 15*MBhat^4 - 120*m2*MBhat^4 + 120*m2^3*MBhat^4 - 
     15*m2^4*MBhat^4 - 8*Ycut + 192*m2*Ycut + 3360*m2^2*Ycut + 
     960*m2^3*Ycut - 2640*m2^4*Ycut - 192*m2^5*Ycut + 8*m2^6*Ycut + 
     52*MBhat*Ycut - 960*m2*MBhat*Ycut - 9600*m2^2*MBhat*Ycut + 
     3840*m2^3*MBhat*Ycut + 3480*m2^4*MBhat*Ycut - 192*m2^5*MBhat*Ycut + 
     20*m2^6*MBhat*Ycut - 140*MBhat^2*Ycut + 1908*m2*MBhat^2*Ycut + 
     8820*m2^2*MBhat^2*Ycut - 7500*m2^3*MBhat^2*Ycut - 
     180*m2^4*MBhat^2*Ycut - 180*m2^5*MBhat^2*Ycut + 32*m2^6*MBhat^2*Ycut + 
     156*MBhat^3*Ycut - 1620*m2*MBhat^3*Ycut - 2820*m2^2*MBhat^3*Ycut + 
     3660*m2^3*MBhat^3*Ycut - 540*m2^4*MBhat^3*Ycut + 84*m2^5*MBhat^3*Ycut - 
     60*MBhat^4*Ycut + 480*m2*MBhat^4*Ycut + 180*m2^2*MBhat^4*Ycut - 
     480*m2^3*MBhat^4*Ycut + 60*m2^4*MBhat^4*Ycut + 12*Ycut^2 - 
     288*m2*Ycut^2 - 5760*m2^2*Ycut^2 - 3360*m2^3*Ycut^2 + 3240*m2^4*Ycut^2 + 
     288*m2^5*Ycut^2 - 12*m2^6*Ycut^2 - 78*MBhat*Ycut^2 + 
     1440*m2*MBhat*Ycut^2 + 16920*m2^2*MBhat*Ycut^2 - 
     1920*m2^3*MBhat*Ycut^2 - 4860*m2^4*MBhat*Ycut^2 + 
     288*m2^5*MBhat*Ycut^2 - 30*m2^6*MBhat*Ycut^2 + 210*MBhat^2*Ycut^2 - 
     2862*m2*MBhat^2*Ycut^2 - 16470*m2^2*MBhat^2*Ycut^2 + 
     8970*m2^3*MBhat^2*Ycut^2 + 270*m2^4*MBhat^2*Ycut^2 + 
     270*m2^5*MBhat^2*Ycut^2 - 48*m2^6*MBhat^2*Ycut^2 - 234*MBhat^3*Ycut^2 + 
     2430*m2*MBhat^3*Ycut^2 + 6030*m2^2*MBhat^3*Ycut^2 - 
     5130*m2^3*MBhat^3*Ycut^2 + 810*m2^4*MBhat^3*Ycut^2 - 
     126*m2^5*MBhat^3*Ycut^2 + 90*MBhat^4*Ycut^2 - 720*m2*MBhat^4*Ycut^2 - 
     630*m2^2*MBhat^4*Ycut^2 + 720*m2^3*MBhat^4*Ycut^2 - 
     90*m2^4*MBhat^4*Ycut^2 - 8*Ycut^3 + 192*m2*Ycut^3 + 4560*m2^2*Ycut^3 + 
     4160*m2^3*Ycut^3 - 1440*m2^4*Ycut^3 - 192*m2^5*Ycut^3 + 8*m2^6*Ycut^3 + 
     52*MBhat*Ycut^3 - 960*m2*MBhat*Ycut^3 - 13800*m2^2*MBhat*Ycut^3 - 
     2560*m2^3*MBhat*Ycut^3 + 2880*m2^4*MBhat*Ycut^3 - 
     192*m2^5*MBhat*Ycut^3 + 20*m2^6*MBhat*Ycut^3 - 170*MBhat^2*Ycut^3 + 
     1998*m2*MBhat^2*Ycut^3 + 14160*m2^2*MBhat^2*Ycut^3 - 
     3760*m2^3*MBhat^2*Ycut^3 - 90*m2^4*MBhat^2*Ycut^3 - 
     210*m2^5*MBhat^2*Ycut^3 + 32*m2^6*MBhat^2*Ycut^3 + 216*MBhat^3*Ycut^3 - 
     1740*m2*MBhat^3*Ycut^3 - 5820*m2^2*MBhat^3*Ycut^3 + 
     3180*m2^3*MBhat^3*Ycut^3 - 600*m2^4*MBhat^3*Ycut^3 + 
     84*m2^5*MBhat^3*Ycut^3 - 90*MBhat^4*Ycut^3 + 510*m2*MBhat^4*Ycut^3 + 
     810*m2^2*MBhat^4*Ycut^3 - 510*m2^3*MBhat^4*Ycut^3 + 
     60*m2^4*MBhat^4*Ycut^3 + 2*Ycut^4 - 48*m2*Ycut^4 - 1500*m2^2*Ycut^4 - 
     2000*m2^3*Ycut^4 + 48*m2^5*Ycut^4 - 2*m2^6*Ycut^4 + 2*MBhat*Ycut^4 + 
     210*m2*MBhat*Ycut^4 + 4680*m2^2*MBhat*Ycut^4 + 2680*m2^3*MBhat*Ycut^4 - 
     645*m2^4*MBhat*Ycut^4 + 78*m2^5*MBhat*Ycut^4 - 5*m2^6*MBhat*Ycut^4 + 
     140*MBhat^2*Ycut^4 - 822*m2*MBhat^2*Ycut^4 - 4800*m2^2*MBhat^2*Ycut^4 - 
     320*m2^3*MBhat^2*Ycut^4 - 15*m2^4*MBhat^2*Ycut^4 + 
     75*m2^5*MBhat^2*Ycut^4 - 8*m2^6*MBhat^2*Ycut^4 - 294*MBhat^3*Ycut^4 + 
     900*m2*MBhat^3*Ycut^4 + 2220*m2^2*MBhat^3*Ycut^4 - 
     780*m2^3*MBhat^3*Ycut^4 + 225*m2^4*MBhat^3*Ycut^4 - 
     21*m2^5*MBhat^3*Ycut^4 + 150*MBhat^4*Ycut^4 - 240*m2*MBhat^4*Ycut^4 - 
     450*m2^2*MBhat^4*Ycut^4 + 180*m2^3*MBhat^4*Ycut^4 - 
     15*m2^4*MBhat^4*Ycut^4 - 3*Ycut^5 + 3*m2*Ycut^5 + 90*m2^2*Ycut^5 + 
     150*m2^3*Ycut^5 + 105*m2^4*Ycut^5 - 9*m2^5*Ycut^5 - 60*MBhat*Ycut^5 + 
     120*m2*MBhat*Ycut^5 - 240*m2^2*MBhat*Ycut^5 - 540*m2^3*MBhat*Ycut^5 + 
     60*m2^4*MBhat*Ycut^5 - 12*m2^5*MBhat*Ycut^5 - 111*MBhat^2*Ycut^5 + 
     477*m2*MBhat^2*Ycut^5 - 360*m2^2*MBhat^2*Ycut^5 + 
     600*m2^3*MBhat^2*Ycut^5 - 45*m2^4*MBhat^2*Ycut^5 - 
     9*m2^5*MBhat^2*Ycut^5 + 414*MBhat^3*Ycut^5 - 780*m2*MBhat^3*Ycut^5 + 
     180*m2^2*MBhat^3*Ycut^5 - 30*m2^4*MBhat^3*Ycut^5 - 240*MBhat^4*Ycut^5 + 
     180*m2*MBhat^4*Ycut^5 + 90*m2^2*MBhat^4*Ycut^5 - 
     30*m2^3*MBhat^4*Ycut^5 + 13*Ycut^6 - 12*m2*Ycut^6 - 30*m2^2*Ycut^6 + 
     100*m2^3*Ycut^6 - 15*m2^4*Ycut^6 + 88*MBhat*Ycut^6 - 
     180*m2*MBhat*Ycut^6 + 60*m2^2*MBhat*Ycut^6 - 80*m2^3*MBhat*Ycut^6 - 
     5*MBhat^2*Ycut^6 - 258*m2*MBhat^2*Ycut^6 + 510*m2^2*MBhat^2*Ycut^6 - 
     170*m2^3*MBhat^2*Ycut^6 + 15*m2^4*MBhat^2*Ycut^6 - 306*MBhat^3*Ycut^6 + 
     570*m2*MBhat^3*Ycut^6 - 330*m2^2*MBhat^3*Ycut^6 + 
     30*m2^3*MBhat^3*Ycut^6 + 210*MBhat^4*Ycut^6 - 120*m2*MBhat^4*Ycut^6 - 
     22*Ycut^7 + 18*m2*Ycut^7 + 30*m2^2*Ycut^7 - 10*m2^3*Ycut^7 - 
     52*MBhat*Ycut^7 + 120*m2*MBhat*Ycut^7 - 120*m2^2*MBhat*Ycut^7 + 
     20*m2^3*MBhat*Ycut^7 + 80*MBhat^2*Ycut^7 + 12*m2*MBhat^2*Ycut^7 - 
     60*m2^2*MBhat^2*Ycut^7 + 20*m2^3*MBhat^2*Ycut^7 + 84*MBhat^3*Ycut^7 - 
     180*m2*MBhat^3*Ycut^7 + 60*m2^2*MBhat^3*Ycut^7 - 90*MBhat^4*Ycut^7 + 
     30*m2*MBhat^4*Ycut^7 + 18*Ycut^8 - 12*m2*Ycut^8 + 3*MBhat*Ycut^8 - 
     30*m2*MBhat*Ycut^8 + 15*m2^2*MBhat*Ycut^8 - 45*MBhat^2*Ycut^8 + 
     27*m2*MBhat^2*Ycut^8 + 9*MBhat^3*Ycut^8 + 15*m2*MBhat^3*Ycut^8 + 
     15*MBhat^4*Ycut^8 - 7*Ycut^9 + 3*m2*Ycut^9 + 8*MBhat*Ycut^9 + 
     5*MBhat^2*Ycut^9 - 3*m2*MBhat^2*Ycut^9 - 6*MBhat^3*Ycut^9 + Ycut^10 - 
     2*MBhat*Ycut^10 + MBhat^2*Ycut^10 + 15*api*MBhat^4*
      X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 60*api*MBhat^4*Ycut*
      X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] + 90*api*MBhat^4*Ycut^2*
      X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 60*api*MBhat^4*Ycut^3*
      X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] + 15*api*MBhat^4*Ycut^4*
      X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 60*api*MBhat^3*
      X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 240*api*MBhat^3*Ycut*
      X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] - 360*api*MBhat^3*Ycut^2*
      X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 240*api*MBhat^3*Ycut^3*
      X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] - 60*api*MBhat^3*Ycut^4*
      X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 60*api*MBhat^2*
      X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] - 240*api*MBhat^2*Ycut*
      X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] + 360*api*MBhat^2*Ycut^2*
      X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] - 240*api*MBhat^2*Ycut^3*
      X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] + 60*api*MBhat^2*Ycut^4*
      X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] + 30*api*MBhat^2*
      X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 120*api*MBhat^2*Ycut*
      X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] + 180*api*MBhat^2*Ycut^2*
      X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 120*api*MBhat^2*Ycut^3*
      X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] + 30*api*MBhat^2*Ycut^4*
      X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat*Ycut*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     360*api*MBhat*Ycut^2*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     240*api*MBhat*Ycut^3*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat*Ycut^4*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     15*api*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     60*api*Ycut*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     90*api*Ycut^2*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     60*api*Ycut^3*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     15*api*Ycut^4*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat])/
    (15*(-1 + Ycut)^4)) + 
 c[VR]*(-24*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 4*MBhat - 16*m2*MBhat - 
     8*m2^2*MBhat + 6*MBhat^2 + 15*m2*MBhat^2 + 3*m2^2*MBhat^2 - 4*MBhat^3 - 
     6*m2*MBhat^3 + MBhat^4 + m2*MBhat^4)*Log[m2] + 
   24*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 4*MBhat - 16*m2*MBhat - 
     8*m2^2*MBhat + 6*MBhat^2 + 15*m2*MBhat^2 + 3*m2^2*MBhat^2 - 4*MBhat^3 - 
     6*m2*MBhat^3 + MBhat^4 + m2*MBhat^4)*Log[1 - Ycut] + 
   (6*Sqrt[m2] + 350*m2^(3/2) + 600*m2^(5/2) - 600*m2^(7/2) - 350*m2^(9/2) - 
     6*m2^(11/2) - 32*Sqrt[m2]*MBhat - 1240*m2^(3/2)*MBhat - 
     640*m2^(5/2)*MBhat + 1760*m2^(7/2)*MBhat + 160*m2^(9/2)*MBhat - 
     8*m2^(11/2)*MBhat + 66*Sqrt[m2]*MBhat^2 + 1590*m2^(3/2)*MBhat^2 - 
     480*m2^(5/2)*MBhat^2 - 1200*m2^(7/2)*MBhat^2 + 30*m2^(9/2)*MBhat^2 - 
     6*m2^(11/2)*MBhat^2 - 60*Sqrt[m2]*MBhat^3 - 880*m2^(3/2)*MBhat^3 + 
     720*m2^(5/2)*MBhat^3 + 240*m2^(7/2)*MBhat^3 - 20*m2^(9/2)*MBhat^3 + 
     20*Sqrt[m2]*MBhat^4 + 180*m2^(3/2)*MBhat^4 - 180*m2^(5/2)*MBhat^4 - 
     20*m2^(7/2)*MBhat^4 - 18*Sqrt[m2]*Ycut - 1170*m2^(3/2)*Ycut - 
     2520*m2^(5/2)*Ycut + 1080*m2^(7/2)*Ycut + 930*m2^(9/2)*Ycut + 
     18*m2^(11/2)*Ycut + 96*Sqrt[m2]*MBhat*Ycut + 4200*m2^(3/2)*MBhat*Ycut + 
     3840*m2^(5/2)*MBhat*Ycut - 4320*m2^(7/2)*MBhat*Ycut - 
     480*m2^(9/2)*MBhat*Ycut + 24*m2^(11/2)*MBhat*Ycut - 
     198*Sqrt[m2]*MBhat^2*Ycut - 5490*m2^(3/2)*MBhat^2*Ycut - 
     360*m2^(5/2)*MBhat^2*Ycut + 3240*m2^(7/2)*MBhat^2*Ycut - 
     90*m2^(9/2)*MBhat^2*Ycut + 18*m2^(11/2)*MBhat^2*Ycut + 
     180*Sqrt[m2]*MBhat^3*Ycut + 3120*m2^(3/2)*MBhat^3*Ycut - 
     1440*m2^(5/2)*MBhat^3*Ycut - 720*m2^(7/2)*MBhat^3*Ycut + 
     60*m2^(9/2)*MBhat^3*Ycut - 60*Sqrt[m2]*MBhat^4*Ycut - 
     660*m2^(3/2)*MBhat^4*Ycut + 420*m2^(5/2)*MBhat^4*Ycut + 
     60*m2^(7/2)*MBhat^4*Ycut + 18*Sqrt[m2]*Ycut^2 + 1350*m2^(3/2)*Ycut^2 + 
     3600*m2^(5/2)*Ycut^2 - 750*m2^(9/2)*Ycut^2 - 18*m2^(11/2)*Ycut^2 - 
     96*Sqrt[m2]*MBhat*Ycut^2 - 4920*m2^(3/2)*MBhat*Ycut^2 - 
     6720*m2^(5/2)*MBhat*Ycut^2 + 2880*m2^(7/2)*MBhat*Ycut^2 + 
     480*m2^(9/2)*MBhat*Ycut^2 - 24*m2^(11/2)*MBhat*Ycut^2 + 
     198*Sqrt[m2]*MBhat^2*Ycut^2 + 6570*m2^(3/2)*MBhat^2*Ycut^2 + 
     3060*m2^(5/2)*MBhat^2*Ycut^2 - 2700*m2^(7/2)*MBhat^2*Ycut^2 + 
     90*m2^(9/2)*MBhat^2*Ycut^2 - 18*m2^(11/2)*MBhat^2*Ycut^2 - 
     180*Sqrt[m2]*MBhat^3*Ycut^2 - 3840*m2^(3/2)*MBhat^3*Ycut^2 + 
     360*m2^(5/2)*MBhat^3*Ycut^2 + 720*m2^(7/2)*MBhat^3*Ycut^2 - 
     60*m2^(9/2)*MBhat^3*Ycut^2 + 60*Sqrt[m2]*MBhat^4*Ycut^2 + 
     840*m2^(3/2)*MBhat^4*Ycut^2 - 240*m2^(5/2)*MBhat^4*Ycut^2 - 
     60*m2^(7/2)*MBhat^4*Ycut^2 - 6*Sqrt[m2]*Ycut^3 - 570*m2^(3/2)*Ycut^3 - 
     1920*m2^(5/2)*Ycut^3 - 720*m2^(7/2)*Ycut^3 + 130*m2^(9/2)*Ycut^3 + 
     6*m2^(11/2)*Ycut^3 + 32*Sqrt[m2]*MBhat*Ycut^3 + 
     2120*m2^(3/2)*MBhat*Ycut^3 + 4160*m2^(5/2)*MBhat*Ycut^3 - 
     160*m2^(9/2)*MBhat*Ycut^3 + 8*m2^(11/2)*MBhat*Ycut^3 - 
     86*Sqrt[m2]*MBhat^2*Ycut^3 - 2830*m2^(3/2)*MBhat^2*Ycut^3 - 
     2940*m2^(5/2)*MBhat^2*Ycut^3 + 620*m2^(7/2)*MBhat^2*Ycut^3 - 
     50*m2^(9/2)*MBhat^2*Ycut^3 + 6*m2^(11/2)*MBhat^2*Ycut^3 + 
     100*Sqrt[m2]*MBhat^3*Ycut^3 + 1640*m2^(3/2)*MBhat^3*Ycut^3 + 
     720*m2^(5/2)*MBhat^3*Ycut^3 - 280*m2^(7/2)*MBhat^3*Ycut^3 + 
     20*m2^(9/2)*MBhat^3*Ycut^3 - 40*Sqrt[m2]*MBhat^4*Ycut^3 - 
     360*m2^(3/2)*MBhat^4*Ycut^3 - 60*m2^(5/2)*MBhat^4*Ycut^3 + 
     20*m2^(7/2)*MBhat^4*Ycut^3 + 30*m2^(3/2)*Ycut^4 + 180*m2^(5/2)*Ycut^4 + 
     180*m2^(7/2)*Ycut^4 + 30*m2^(9/2)*Ycut^4 + 20*Sqrt[m2]*MBhat*Ycut^4 - 
     200*m2^(3/2)*MBhat*Ycut^4 - 360*m2^(5/2)*MBhat*Ycut^4 - 
     320*m2^(7/2)*MBhat*Ycut^4 + 20*m2^(9/2)*MBhat*Ycut^4 + 
     20*Sqrt[m2]*MBhat^2*Ycut^4 + 110*m2^(3/2)*MBhat^2*Ycut^4 + 
     540*m2^(5/2)*MBhat^2*Ycut^4 + 40*m2^(7/2)*MBhat^2*Ycut^4 + 
     10*m2^(9/2)*MBhat^2*Ycut^4 - 100*Sqrt[m2]*MBhat^3*Ycut^4 + 
     120*m2^(3/2)*MBhat^3*Ycut^4 - 360*m2^(5/2)*MBhat^3*Ycut^4 + 
     40*m2^(7/2)*MBhat^3*Ycut^4 + 60*Sqrt[m2]*MBhat^4*Ycut^4 - 
     60*m2^(3/2)*MBhat^4*Ycut^4 + 60*m2^(5/2)*MBhat^4*Ycut^4 - 
     6*Sqrt[m2]*Ycut^5 + 30*m2^(3/2)*Ycut^5 + 60*m2^(7/2)*Ycut^5 - 
     48*Sqrt[m2]*MBhat*Ycut^5 + 120*m2^(3/2)*MBhat*Ycut^5 - 
     240*m2^(5/2)*MBhat*Ycut^5 + 54*Sqrt[m2]*MBhat^2*Ycut^5 - 
     90*m2^(3/2)*MBhat^2*Ycut^5 + 180*m2^(5/2)*MBhat^2*Ycut^5 + 
     60*Sqrt[m2]*MBhat^3*Ycut^5 - 120*m2^(3/2)*MBhat^3*Ycut^5 - 
     60*Sqrt[m2]*MBhat^4*Ycut^5 + 60*m2^(3/2)*MBhat^4*Ycut^5 + 
     18*Sqrt[m2]*Ycut^6 - 50*m2^(3/2)*Ycut^6 + 60*m2^(5/2)*Ycut^6 + 
     24*Sqrt[m2]*MBhat*Ycut^6 - 40*m2^(3/2)*MBhat*Ycut^6 - 
     40*m2^(5/2)*MBhat*Ycut^6 - 82*Sqrt[m2]*MBhat^2*Ycut^6 + 
     130*m2^(3/2)*MBhat^2*Ycut^6 + 20*Sqrt[m2]*MBhat^3*Ycut^6 - 
     40*m2^(3/2)*MBhat^3*Ycut^6 + 20*Sqrt[m2]*MBhat^4*Ycut^6 - 
     18*Sqrt[m2]*Ycut^7 + 30*m2^(3/2)*Ycut^7 + 16*Sqrt[m2]*MBhat*Ycut^7 - 
     40*m2^(3/2)*MBhat*Ycut^7 + 22*Sqrt[m2]*MBhat^2*Ycut^7 + 
     10*m2^(3/2)*MBhat^2*Ycut^7 - 20*Sqrt[m2]*MBhat^3*Ycut^7 + 
     6*Sqrt[m2]*Ycut^8 - 12*Sqrt[m2]*MBhat*Ycut^8 + 
     6*Sqrt[m2]*MBhat^2*Ycut^8 - 5*api*MBhat^4*X1mix[0, 0, SM*c[VR], Ycut, 
       m2, mu2hat] + 15*api*MBhat^4*Ycut*X1mix[0, 0, SM*c[VR], Ycut, m2, 
       mu2hat] - 15*api*MBhat^4*Ycut^2*X1mix[0, 0, SM*c[VR], Ycut, m2, 
       mu2hat] + 5*api*MBhat^4*Ycut^3*X1mix[0, 0, SM*c[VR], Ycut, m2, 
       mu2hat] + 20*api*MBhat^3*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^3*Ycut*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat^3*Ycut^2*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*MBhat^3*Ycut^3*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*MBhat^2*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^2*Ycut^2*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     20*api*MBhat^2*Ycut^3*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     10*api*MBhat^2*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     30*api*MBhat^2*Ycut*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     30*api*MBhat^2*Ycut^2*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     10*api*MBhat^2*Ycut^3*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     20*api*MBhat*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat*Ycut*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat*Ycut^2*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*MBhat*Ycut^3*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     5*api*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     15*api*Ycut*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     15*api*Ycut^2*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     5*api*Ycut^3*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat])/
    (5*(-1 + Ycut)^3)) + 
 c[VL]*(8*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 32*m2*MBhat + 3*m2^2*MBhat - 
     27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 3*m2*MBhat^3 - 3*MBhat^4)*
    Log[m2] - 8*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 32*m2*MBhat + 
     3*m2^2*MBhat - 27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 3*m2*MBhat^3 - 
     3*MBhat^4)*Log[1 - Ycut] + (4 - 96*m2 - 1500*m2^2 + 1500*m2^4 + 
     96*m2^5 - 4*m2^6 - 26*MBhat + 480*m2*MBhat + 4170*m2^2*MBhat - 
     2880*m2^3*MBhat - 1830*m2^4*MBhat + 96*m2^5*MBhat - 10*m2^6*MBhat + 
     70*MBhat^2 - 954*m2*MBhat^2 - 3600*m2^2*MBhat^2 + 4320*m2^3*MBhat^2 + 
     90*m2^4*MBhat^2 + 90*m2^5*MBhat^2 - 16*m2^6*MBhat^2 - 78*MBhat^3 + 
     810*m2*MBhat^3 + 960*m2^2*MBhat^3 - 1920*m2^3*MBhat^3 + 
     270*m2^4*MBhat^3 - 42*m2^5*MBhat^3 + 30*MBhat^4 - 240*m2*MBhat^4 + 
     240*m2^3*MBhat^4 - 30*m2^4*MBhat^4 - 16*Ycut + 384*m2*Ycut + 
     6720*m2^2*Ycut + 1920*m2^3*Ycut - 5280*m2^4*Ycut - 384*m2^5*Ycut + 
     16*m2^6*Ycut + 104*MBhat*Ycut - 1920*m2*MBhat*Ycut - 
     19200*m2^2*MBhat*Ycut + 7680*m2^3*MBhat*Ycut + 6960*m2^4*MBhat*Ycut - 
     384*m2^5*MBhat*Ycut + 40*m2^6*MBhat*Ycut - 280*MBhat^2*Ycut + 
     3816*m2*MBhat^2*Ycut + 17640*m2^2*MBhat^2*Ycut - 
     15000*m2^3*MBhat^2*Ycut - 360*m2^4*MBhat^2*Ycut - 
     360*m2^5*MBhat^2*Ycut + 64*m2^6*MBhat^2*Ycut + 312*MBhat^3*Ycut - 
     3240*m2*MBhat^3*Ycut - 5640*m2^2*MBhat^3*Ycut + 7320*m2^3*MBhat^3*Ycut - 
     1080*m2^4*MBhat^3*Ycut + 168*m2^5*MBhat^3*Ycut - 120*MBhat^4*Ycut + 
     960*m2*MBhat^4*Ycut + 360*m2^2*MBhat^4*Ycut - 960*m2^3*MBhat^4*Ycut + 
     120*m2^4*MBhat^4*Ycut + 24*Ycut^2 - 576*m2*Ycut^2 - 11520*m2^2*Ycut^2 - 
     6720*m2^3*Ycut^2 + 6480*m2^4*Ycut^2 + 576*m2^5*Ycut^2 - 24*m2^6*Ycut^2 - 
     156*MBhat*Ycut^2 + 2880*m2*MBhat*Ycut^2 + 33840*m2^2*MBhat*Ycut^2 - 
     3840*m2^3*MBhat*Ycut^2 - 9720*m2^4*MBhat*Ycut^2 + 
     576*m2^5*MBhat*Ycut^2 - 60*m2^6*MBhat*Ycut^2 + 420*MBhat^2*Ycut^2 - 
     5724*m2*MBhat^2*Ycut^2 - 32940*m2^2*MBhat^2*Ycut^2 + 
     17940*m2^3*MBhat^2*Ycut^2 + 540*m2^4*MBhat^2*Ycut^2 + 
     540*m2^5*MBhat^2*Ycut^2 - 96*m2^6*MBhat^2*Ycut^2 - 468*MBhat^3*Ycut^2 + 
     4860*m2*MBhat^3*Ycut^2 + 12060*m2^2*MBhat^3*Ycut^2 - 
     10260*m2^3*MBhat^3*Ycut^2 + 1620*m2^4*MBhat^3*Ycut^2 - 
     252*m2^5*MBhat^3*Ycut^2 + 180*MBhat^4*Ycut^2 - 1440*m2*MBhat^4*Ycut^2 - 
     1260*m2^2*MBhat^4*Ycut^2 + 1440*m2^3*MBhat^4*Ycut^2 - 
     180*m2^4*MBhat^4*Ycut^2 - 16*Ycut^3 + 384*m2*Ycut^3 + 9120*m2^2*Ycut^3 + 
     8320*m2^3*Ycut^3 - 2880*m2^4*Ycut^3 - 384*m2^5*Ycut^3 + 16*m2^6*Ycut^3 + 
     104*MBhat*Ycut^3 - 1920*m2*MBhat*Ycut^3 - 27600*m2^2*MBhat*Ycut^3 - 
     5120*m2^3*MBhat*Ycut^3 + 5760*m2^4*MBhat*Ycut^3 - 
     384*m2^5*MBhat*Ycut^3 + 40*m2^6*MBhat*Ycut^3 - 340*MBhat^2*Ycut^3 + 
     3996*m2*MBhat^2*Ycut^3 + 28320*m2^2*MBhat^2*Ycut^3 - 
     7520*m2^3*MBhat^2*Ycut^3 - 180*m2^4*MBhat^2*Ycut^3 - 
     420*m2^5*MBhat^2*Ycut^3 + 64*m2^6*MBhat^2*Ycut^3 + 432*MBhat^3*Ycut^3 - 
     3480*m2*MBhat^3*Ycut^3 - 11640*m2^2*MBhat^3*Ycut^3 + 
     6360*m2^3*MBhat^3*Ycut^3 - 1200*m2^4*MBhat^3*Ycut^3 + 
     168*m2^5*MBhat^3*Ycut^3 - 180*MBhat^4*Ycut^3 + 1020*m2*MBhat^4*Ycut^3 + 
     1620*m2^2*MBhat^4*Ycut^3 - 1020*m2^3*MBhat^4*Ycut^3 + 
     120*m2^4*MBhat^4*Ycut^3 + 4*Ycut^4 - 96*m2*Ycut^4 - 3000*m2^2*Ycut^4 - 
     4000*m2^3*Ycut^4 + 96*m2^5*Ycut^4 - 4*m2^6*Ycut^4 + 4*MBhat*Ycut^4 + 
     420*m2*MBhat*Ycut^4 + 9360*m2^2*MBhat*Ycut^4 + 5360*m2^3*MBhat*Ycut^4 - 
     1290*m2^4*MBhat*Ycut^4 + 156*m2^5*MBhat*Ycut^4 - 10*m2^6*MBhat*Ycut^4 + 
     280*MBhat^2*Ycut^4 - 1644*m2*MBhat^2*Ycut^4 - 9600*m2^2*MBhat^2*Ycut^4 - 
     640*m2^3*MBhat^2*Ycut^4 - 30*m2^4*MBhat^2*Ycut^4 + 
     150*m2^5*MBhat^2*Ycut^4 - 16*m2^6*MBhat^2*Ycut^4 - 588*MBhat^3*Ycut^4 + 
     1800*m2*MBhat^3*Ycut^4 + 4440*m2^2*MBhat^3*Ycut^4 - 
     1560*m2^3*MBhat^3*Ycut^4 + 450*m2^4*MBhat^3*Ycut^4 - 
     42*m2^5*MBhat^3*Ycut^4 + 300*MBhat^4*Ycut^4 - 480*m2*MBhat^4*Ycut^4 - 
     900*m2^2*MBhat^4*Ycut^4 + 360*m2^3*MBhat^4*Ycut^4 - 
     30*m2^4*MBhat^4*Ycut^4 - 6*Ycut^5 + 6*m2*Ycut^5 + 180*m2^2*Ycut^5 + 
     300*m2^3*Ycut^5 + 210*m2^4*Ycut^5 - 18*m2^5*Ycut^5 - 120*MBhat*Ycut^5 + 
     240*m2*MBhat*Ycut^5 - 480*m2^2*MBhat*Ycut^5 - 1080*m2^3*MBhat*Ycut^5 + 
     120*m2^4*MBhat*Ycut^5 - 24*m2^5*MBhat*Ycut^5 - 222*MBhat^2*Ycut^5 + 
     954*m2*MBhat^2*Ycut^5 - 720*m2^2*MBhat^2*Ycut^5 + 
     1200*m2^3*MBhat^2*Ycut^5 - 90*m2^4*MBhat^2*Ycut^5 - 
     18*m2^5*MBhat^2*Ycut^5 + 828*MBhat^3*Ycut^5 - 1560*m2*MBhat^3*Ycut^5 + 
     360*m2^2*MBhat^3*Ycut^5 - 60*m2^4*MBhat^3*Ycut^5 - 480*MBhat^4*Ycut^5 + 
     360*m2*MBhat^4*Ycut^5 + 180*m2^2*MBhat^4*Ycut^5 - 
     60*m2^3*MBhat^4*Ycut^5 + 26*Ycut^6 - 24*m2*Ycut^6 - 60*m2^2*Ycut^6 + 
     200*m2^3*Ycut^6 - 30*m2^4*Ycut^6 + 176*MBhat*Ycut^6 - 
     360*m2*MBhat*Ycut^6 + 120*m2^2*MBhat*Ycut^6 - 160*m2^3*MBhat*Ycut^6 - 
     10*MBhat^2*Ycut^6 - 516*m2*MBhat^2*Ycut^6 + 1020*m2^2*MBhat^2*Ycut^6 - 
     340*m2^3*MBhat^2*Ycut^6 + 30*m2^4*MBhat^2*Ycut^6 - 612*MBhat^3*Ycut^6 + 
     1140*m2*MBhat^3*Ycut^6 - 660*m2^2*MBhat^3*Ycut^6 + 
     60*m2^3*MBhat^3*Ycut^6 + 420*MBhat^4*Ycut^6 - 240*m2*MBhat^4*Ycut^6 - 
     44*Ycut^7 + 36*m2*Ycut^7 + 60*m2^2*Ycut^7 - 20*m2^3*Ycut^7 - 
     104*MBhat*Ycut^7 + 240*m2*MBhat*Ycut^7 - 240*m2^2*MBhat*Ycut^7 + 
     40*m2^3*MBhat*Ycut^7 + 160*MBhat^2*Ycut^7 + 24*m2*MBhat^2*Ycut^7 - 
     120*m2^2*MBhat^2*Ycut^7 + 40*m2^3*MBhat^2*Ycut^7 + 168*MBhat^3*Ycut^7 - 
     360*m2*MBhat^3*Ycut^7 + 120*m2^2*MBhat^3*Ycut^7 - 180*MBhat^4*Ycut^7 + 
     60*m2*MBhat^4*Ycut^7 + 36*Ycut^8 - 24*m2*Ycut^8 + 6*MBhat*Ycut^8 - 
     60*m2*MBhat*Ycut^8 + 30*m2^2*MBhat*Ycut^8 - 90*MBhat^2*Ycut^8 + 
     54*m2*MBhat^2*Ycut^8 + 18*MBhat^3*Ycut^8 + 30*m2*MBhat^3*Ycut^8 + 
     30*MBhat^4*Ycut^8 - 14*Ycut^9 + 6*m2*Ycut^9 + 16*MBhat*Ycut^9 + 
     10*MBhat^2*Ycut^9 - 6*m2*MBhat^2*Ycut^9 - 12*MBhat^3*Ycut^9 + 
     2*Ycut^10 - 4*MBhat*Ycut^10 + 2*MBhat^2*Ycut^10 + 
     15*api*MBhat^4*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*MBhat^4*Ycut*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     90*api*MBhat^4*Ycut^2*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*MBhat^4*Ycut^3*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     15*api*MBhat^4*Ycut^4*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*MBhat^3*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     240*api*MBhat^3*Ycut*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     360*api*MBhat^3*Ycut^2*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     240*api*MBhat^3*Ycut^3*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*MBhat^3*Ycut^4*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     60*api*MBhat^2*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     240*api*MBhat^2*Ycut*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     360*api*MBhat^2*Ycut^2*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     240*api*MBhat^2*Ycut^3*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut^4*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     30*api*MBhat^2*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     120*api*MBhat^2*Ycut*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     180*api*MBhat^2*Ycut^2*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     120*api*MBhat^2*Ycut^3*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     30*api*MBhat^2*Ycut^4*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*MBhat*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     240*api*MBhat*Ycut*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     360*api*MBhat*Ycut^2*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     240*api*MBhat*Ycut^3*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*MBhat*Ycut^4*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     15*api*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*Ycut*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     90*api*Ycut^2*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     60*api*Ycut^3*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     15*api*Ycut^4*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat])/
    (15*(-1 + Ycut)^4) + 
   c[VR]*(-24*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 4*MBhat - 16*m2*MBhat - 
       8*m2^2*MBhat + 6*MBhat^2 + 15*m2*MBhat^2 + 3*m2^2*MBhat^2 - 
       4*MBhat^3 - 6*m2*MBhat^3 + MBhat^4 + m2*MBhat^4)*Log[m2] + 
     24*m2^(3/2)*(1 + 6*m2 + 6*m2^2 + m2^3 - 4*MBhat - 16*m2*MBhat - 
       8*m2^2*MBhat + 6*MBhat^2 + 15*m2*MBhat^2 + 3*m2^2*MBhat^2 - 
       4*MBhat^3 - 6*m2*MBhat^3 + MBhat^4 + m2*MBhat^4)*Log[1 - Ycut] + 
     (6*Sqrt[m2] + 350*m2^(3/2) + 600*m2^(5/2) - 600*m2^(7/2) - 
       350*m2^(9/2) - 6*m2^(11/2) - 32*Sqrt[m2]*MBhat - 1240*m2^(3/2)*MBhat - 
       640*m2^(5/2)*MBhat + 1760*m2^(7/2)*MBhat + 160*m2^(9/2)*MBhat - 
       8*m2^(11/2)*MBhat + 66*Sqrt[m2]*MBhat^2 + 1590*m2^(3/2)*MBhat^2 - 
       480*m2^(5/2)*MBhat^2 - 1200*m2^(7/2)*MBhat^2 + 30*m2^(9/2)*MBhat^2 - 
       6*m2^(11/2)*MBhat^2 - 60*Sqrt[m2]*MBhat^3 - 880*m2^(3/2)*MBhat^3 + 
       720*m2^(5/2)*MBhat^3 + 240*m2^(7/2)*MBhat^3 - 20*m2^(9/2)*MBhat^3 + 
       20*Sqrt[m2]*MBhat^4 + 180*m2^(3/2)*MBhat^4 - 180*m2^(5/2)*MBhat^4 - 
       20*m2^(7/2)*MBhat^4 - 18*Sqrt[m2]*Ycut - 1170*m2^(3/2)*Ycut - 
       2520*m2^(5/2)*Ycut + 1080*m2^(7/2)*Ycut + 930*m2^(9/2)*Ycut + 
       18*m2^(11/2)*Ycut + 96*Sqrt[m2]*MBhat*Ycut + 4200*m2^(3/2)*MBhat*
        Ycut + 3840*m2^(5/2)*MBhat*Ycut - 4320*m2^(7/2)*MBhat*Ycut - 
       480*m2^(9/2)*MBhat*Ycut + 24*m2^(11/2)*MBhat*Ycut - 
       198*Sqrt[m2]*MBhat^2*Ycut - 5490*m2^(3/2)*MBhat^2*Ycut - 
       360*m2^(5/2)*MBhat^2*Ycut + 3240*m2^(7/2)*MBhat^2*Ycut - 
       90*m2^(9/2)*MBhat^2*Ycut + 18*m2^(11/2)*MBhat^2*Ycut + 
       180*Sqrt[m2]*MBhat^3*Ycut + 3120*m2^(3/2)*MBhat^3*Ycut - 
       1440*m2^(5/2)*MBhat^3*Ycut - 720*m2^(7/2)*MBhat^3*Ycut + 
       60*m2^(9/2)*MBhat^3*Ycut - 60*Sqrt[m2]*MBhat^4*Ycut - 
       660*m2^(3/2)*MBhat^4*Ycut + 420*m2^(5/2)*MBhat^4*Ycut + 
       60*m2^(7/2)*MBhat^4*Ycut + 18*Sqrt[m2]*Ycut^2 + 1350*m2^(3/2)*Ycut^2 + 
       3600*m2^(5/2)*Ycut^2 - 750*m2^(9/2)*Ycut^2 - 18*m2^(11/2)*Ycut^2 - 
       96*Sqrt[m2]*MBhat*Ycut^2 - 4920*m2^(3/2)*MBhat*Ycut^2 - 
       6720*m2^(5/2)*MBhat*Ycut^2 + 2880*m2^(7/2)*MBhat*Ycut^2 + 
       480*m2^(9/2)*MBhat*Ycut^2 - 24*m2^(11/2)*MBhat*Ycut^2 + 
       198*Sqrt[m2]*MBhat^2*Ycut^2 + 6570*m2^(3/2)*MBhat^2*Ycut^2 + 
       3060*m2^(5/2)*MBhat^2*Ycut^2 - 2700*m2^(7/2)*MBhat^2*Ycut^2 + 
       90*m2^(9/2)*MBhat^2*Ycut^2 - 18*m2^(11/2)*MBhat^2*Ycut^2 - 
       180*Sqrt[m2]*MBhat^3*Ycut^2 - 3840*m2^(3/2)*MBhat^3*Ycut^2 + 
       360*m2^(5/2)*MBhat^3*Ycut^2 + 720*m2^(7/2)*MBhat^3*Ycut^2 - 
       60*m2^(9/2)*MBhat^3*Ycut^2 + 60*Sqrt[m2]*MBhat^4*Ycut^2 + 
       840*m2^(3/2)*MBhat^4*Ycut^2 - 240*m2^(5/2)*MBhat^4*Ycut^2 - 
       60*m2^(7/2)*MBhat^4*Ycut^2 - 6*Sqrt[m2]*Ycut^3 - 570*m2^(3/2)*Ycut^3 - 
       1920*m2^(5/2)*Ycut^3 - 720*m2^(7/2)*Ycut^3 + 130*m2^(9/2)*Ycut^3 + 
       6*m2^(11/2)*Ycut^3 + 32*Sqrt[m2]*MBhat*Ycut^3 + 
       2120*m2^(3/2)*MBhat*Ycut^3 + 4160*m2^(5/2)*MBhat*Ycut^3 - 
       160*m2^(9/2)*MBhat*Ycut^3 + 8*m2^(11/2)*MBhat*Ycut^3 - 
       86*Sqrt[m2]*MBhat^2*Ycut^3 - 2830*m2^(3/2)*MBhat^2*Ycut^3 - 
       2940*m2^(5/2)*MBhat^2*Ycut^3 + 620*m2^(7/2)*MBhat^2*Ycut^3 - 
       50*m2^(9/2)*MBhat^2*Ycut^3 + 6*m2^(11/2)*MBhat^2*Ycut^3 + 
       100*Sqrt[m2]*MBhat^3*Ycut^3 + 1640*m2^(3/2)*MBhat^3*Ycut^3 + 
       720*m2^(5/2)*MBhat^3*Ycut^3 - 280*m2^(7/2)*MBhat^3*Ycut^3 + 
       20*m2^(9/2)*MBhat^3*Ycut^3 - 40*Sqrt[m2]*MBhat^4*Ycut^3 - 
       360*m2^(3/2)*MBhat^4*Ycut^3 - 60*m2^(5/2)*MBhat^4*Ycut^3 + 
       20*m2^(7/2)*MBhat^4*Ycut^3 + 30*m2^(3/2)*Ycut^4 + 
       180*m2^(5/2)*Ycut^4 + 180*m2^(7/2)*Ycut^4 + 30*m2^(9/2)*Ycut^4 + 
       20*Sqrt[m2]*MBhat*Ycut^4 - 200*m2^(3/2)*MBhat*Ycut^4 - 
       360*m2^(5/2)*MBhat*Ycut^4 - 320*m2^(7/2)*MBhat*Ycut^4 + 
       20*m2^(9/2)*MBhat*Ycut^4 + 20*Sqrt[m2]*MBhat^2*Ycut^4 + 
       110*m2^(3/2)*MBhat^2*Ycut^4 + 540*m2^(5/2)*MBhat^2*Ycut^4 + 
       40*m2^(7/2)*MBhat^2*Ycut^4 + 10*m2^(9/2)*MBhat^2*Ycut^4 - 
       100*Sqrt[m2]*MBhat^3*Ycut^4 + 120*m2^(3/2)*MBhat^3*Ycut^4 - 
       360*m2^(5/2)*MBhat^3*Ycut^4 + 40*m2^(7/2)*MBhat^3*Ycut^4 + 
       60*Sqrt[m2]*MBhat^4*Ycut^4 - 60*m2^(3/2)*MBhat^4*Ycut^4 + 
       60*m2^(5/2)*MBhat^4*Ycut^4 - 6*Sqrt[m2]*Ycut^5 + 30*m2^(3/2)*Ycut^5 + 
       60*m2^(7/2)*Ycut^5 - 48*Sqrt[m2]*MBhat*Ycut^5 + 
       120*m2^(3/2)*MBhat*Ycut^5 - 240*m2^(5/2)*MBhat*Ycut^5 + 
       54*Sqrt[m2]*MBhat^2*Ycut^5 - 90*m2^(3/2)*MBhat^2*Ycut^5 + 
       180*m2^(5/2)*MBhat^2*Ycut^5 + 60*Sqrt[m2]*MBhat^3*Ycut^5 - 
       120*m2^(3/2)*MBhat^3*Ycut^5 - 60*Sqrt[m2]*MBhat^4*Ycut^5 + 
       60*m2^(3/2)*MBhat^4*Ycut^5 + 18*Sqrt[m2]*Ycut^6 - 50*m2^(3/2)*Ycut^6 + 
       60*m2^(5/2)*Ycut^6 + 24*Sqrt[m2]*MBhat*Ycut^6 - 
       40*m2^(3/2)*MBhat*Ycut^6 - 40*m2^(5/2)*MBhat*Ycut^6 - 
       82*Sqrt[m2]*MBhat^2*Ycut^6 + 130*m2^(3/2)*MBhat^2*Ycut^6 + 
       20*Sqrt[m2]*MBhat^3*Ycut^6 - 40*m2^(3/2)*MBhat^3*Ycut^6 + 
       20*Sqrt[m2]*MBhat^4*Ycut^6 - 18*Sqrt[m2]*Ycut^7 + 30*m2^(3/2)*Ycut^7 + 
       16*Sqrt[m2]*MBhat*Ycut^7 - 40*m2^(3/2)*MBhat*Ycut^7 + 
       22*Sqrt[m2]*MBhat^2*Ycut^7 + 10*m2^(3/2)*MBhat^2*Ycut^7 - 
       20*Sqrt[m2]*MBhat^3*Ycut^7 + 6*Sqrt[m2]*Ycut^8 - 
       12*Sqrt[m2]*MBhat*Ycut^8 + 6*Sqrt[m2]*MBhat^2*Ycut^8 - 
       5*api*MBhat^4*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       15*api*MBhat^4*Ycut*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       15*api*MBhat^4*Ycut^2*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       5*api*MBhat^4*Ycut^3*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       20*api*MBhat^3*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^3*Ycut*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat^3*Ycut^2*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*MBhat^3*Ycut^3*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*MBhat^2*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat^2*Ycut*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^2*Ycut^2*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       20*api*MBhat^2*Ycut^3*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       10*api*MBhat^2*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       30*api*MBhat^2*Ycut*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       30*api*MBhat^2*Ycut^2*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       10*api*MBhat^2*Ycut^3*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       20*api*MBhat*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat*Ycut*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat*Ycut^2*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*MBhat*Ycut^3*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       5*api*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       15*api*Ycut*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       15*api*Ycut^2*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       5*api*Ycut^3*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat])/
      (5*(-1 + Ycut)^3))) + 
 c[VR]^2*(4*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 32*m2*MBhat + 
     3*m2^2*MBhat - 27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 3*m2*MBhat^3 - 
     3*MBhat^4)*Log[m2] - 4*m2^2*(-6 - 16*m2 - 6*m2^2 + 21*MBhat + 
     32*m2*MBhat + 3*m2^2*MBhat - 27*MBhat^2 - 19*m2*MBhat^2 + 15*MBhat^3 + 
     3*m2*MBhat^3 - 3*MBhat^4)*Log[1 - Ycut] - 
   (-2 + 48*m2 + 750*m2^2 - 750*m2^4 - 48*m2^5 + 2*m2^6 + 13*MBhat - 
     240*m2*MBhat - 2085*m2^2*MBhat + 1440*m2^3*MBhat + 915*m2^4*MBhat - 
     48*m2^5*MBhat + 5*m2^6*MBhat - 35*MBhat^2 + 477*m2*MBhat^2 + 
     1800*m2^2*MBhat^2 - 2160*m2^3*MBhat^2 - 45*m2^4*MBhat^2 - 
     45*m2^5*MBhat^2 + 8*m2^6*MBhat^2 + 39*MBhat^3 - 405*m2*MBhat^3 - 
     480*m2^2*MBhat^3 + 960*m2^3*MBhat^3 - 135*m2^4*MBhat^3 + 
     21*m2^5*MBhat^3 - 15*MBhat^4 + 120*m2*MBhat^4 - 120*m2^3*MBhat^4 + 
     15*m2^4*MBhat^4 + 4*Ycut - 96*m2*Ycut - 1860*m2^2*Ycut - 960*m2^3*Ycut + 
     1140*m2^4*Ycut + 96*m2^5*Ycut - 4*m2^6*Ycut - 26*MBhat*Ycut + 
     480*m2*MBhat*Ycut + 5430*m2^2*MBhat*Ycut - 960*m2^3*MBhat*Ycut - 
     1650*m2^4*MBhat*Ycut + 96*m2^5*MBhat*Ycut - 10*m2^6*MBhat*Ycut + 
     70*MBhat^2*Ycut - 954*m2*MBhat^2*Ycut - 5220*m2^2*MBhat^2*Ycut + 
     3180*m2^3*MBhat^2*Ycut + 90*m2^4*MBhat^2*Ycut + 90*m2^5*MBhat^2*Ycut - 
     16*m2^6*MBhat^2*Ycut - 78*MBhat^3*Ycut + 810*m2*MBhat^3*Ycut + 
     1860*m2^2*MBhat^3*Ycut - 1740*m2^3*MBhat^3*Ycut + 
     270*m2^4*MBhat^3*Ycut - 42*m2^5*MBhat^3*Ycut + 30*MBhat^4*Ycut - 
     240*m2*MBhat^4*Ycut - 180*m2^2*MBhat^4*Ycut + 240*m2^3*MBhat^4*Ycut - 
     30*m2^4*MBhat^4*Ycut - 2*Ycut^2 + 48*m2*Ycut^2 + 1290*m2^2*Ycut^2 + 
     1440*m2^3*Ycut^2 - 210*m2^4*Ycut^2 - 48*m2^5*Ycut^2 + 2*m2^6*Ycut^2 + 
     13*MBhat*Ycut^2 - 240*m2*MBhat*Ycut^2 - 3975*m2^2*MBhat*Ycut^2 - 
     1440*m2^3*MBhat*Ycut^2 + 645*m2^4*MBhat*Ycut^2 - 48*m2^5*MBhat*Ycut^2 + 
     5*m2^6*MBhat*Ycut^2 - 35*MBhat^2*Ycut^2 + 477*m2*MBhat^2*Ycut^2 + 
     4230*m2^2*MBhat^2*Ycut^2 - 450*m2^3*MBhat^2*Ycut^2 - 
     45*m2^4*MBhat^2*Ycut^2 - 45*m2^5*MBhat^2*Ycut^2 + 
     8*m2^6*MBhat^2*Ycut^2 + 39*MBhat^3*Ycut^2 - 405*m2*MBhat^3*Ycut^2 - 
     1830*m2^2*MBhat^3*Ycut^2 + 690*m2^3*MBhat^3*Ycut^2 - 
     135*m2^4*MBhat^3*Ycut^2 + 21*m2^5*MBhat^3*Ycut^2 - 15*MBhat^4*Ycut^2 + 
     120*m2*MBhat^4*Ycut^2 + 270*m2^2*MBhat^4*Ycut^2 - 
     120*m2^3*MBhat^4*Ycut^2 + 15*m2^4*MBhat^4*Ycut^2 - 120*m2^2*Ycut^3 - 
     320*m2^3*Ycut^3 - 120*m2^4*Ycut^3 + 420*m2^2*MBhat*Ycut^3 + 
     640*m2^3*MBhat*Ycut^3 + 60*m2^4*MBhat*Ycut^3 + 60*MBhat^2*Ycut^3 - 
     240*m2*MBhat^2*Ycut^3 - 180*m2^2*MBhat^2*Ycut^3 - 
     620*m2^3*MBhat^2*Ycut^3 + 60*m2^4*MBhat^2*Ycut^3 - 120*MBhat^3*Ycut^3 + 
     360*m2*MBhat^3*Ycut^3 - 60*m2^2*MBhat^3*Ycut^3 + 
     180*m2^3*MBhat^3*Ycut^3 + 60*MBhat^4*Ycut^3 - 120*m2*MBhat^4*Ycut^3 - 
     30*m2^2*Ycut^4 - 80*m2^3*Ycut^4 - 30*m2^4*Ycut^4 - 45*MBhat*Ycut^4 + 
     180*m2*MBhat*Ycut^4 - 165*m2^2*MBhat*Ycut^4 + 340*m2^3*MBhat*Ycut^4 - 
     30*m2^4*MBhat*Ycut^4 - 75*MBhat^2*Ycut^4 + 255*m2*MBhat^2*Ycut^4 - 
     450*m2^2*MBhat^2*Ycut^4 + 70*m2^3*MBhat^2*Ycut^4 - 
     30*m2^4*MBhat^2*Ycut^4 + 285*MBhat^3*Ycut^4 - 675*m2*MBhat^3*Ycut^4 + 
     570*m2^2*MBhat^3*Ycut^4 - 90*m2^3*MBhat^3*Ycut^4 - 165*MBhat^4*Ycut^4 + 
     240*m2*MBhat^4*Ycut^4 - 90*m2^2*MBhat^4*Ycut^4 + 12*Ycut^5 - 
     48*m2*Ycut^5 + 60*m2^2*Ycut^5 - 80*m2^3*Ycut^5 + 102*MBhat*Ycut^5 - 
     300*m2*MBhat*Ycut^5 + 330*m2^2*MBhat*Ycut^5 - 20*m2^3*MBhat*Ycut^5 - 
     90*MBhat^2*Ycut^5 + 198*m2*MBhat^2*Ycut^5 - 180*m2^2*MBhat^2*Ycut^5 - 
     20*m2^3*MBhat^2*Ycut^5 - 174*MBhat^3*Ycut^5 + 270*m2*MBhat^3*Ycut^5 - 
     60*m2^2*MBhat^3*Ycut^5 + 150*MBhat^4*Ycut^5 - 120*m2*MBhat^4*Ycut^5 - 
     34*Ycut^6 + 96*m2*Ycut^6 - 90*m2^2*Ycut^6 - 49*MBhat*Ycut^6 + 
     60*m2*MBhat*Ycut^6 + 45*m2^2*MBhat*Ycut^6 + 155*MBhat^2*Ycut^6 - 
     201*m2*MBhat^2*Ycut^6 - 27*MBhat^3*Ycut^6 + 45*m2*MBhat^3*Ycut^6 - 
     45*MBhat^4*Ycut^6 + 32*Ycut^7 - 48*m2*Ycut^7 - 28*MBhat*Ycut^7 + 
     60*m2*MBhat*Ycut^7 - 40*MBhat^2*Ycut^7 - 12*m2*MBhat^2*Ycut^7 + 
     36*MBhat^3*Ycut^7 - 10*Ycut^8 + 20*MBhat*Ycut^8 - 10*MBhat^2*Ycut^8 - 
     15*api*MBhat^4*X1mix[0, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     30*api*MBhat^4*Ycut*X1mix[0, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     15*api*MBhat^4*Ycut^2*X1mix[0, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^3*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     120*api*MBhat^3*Ycut*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^3*Ycut^2*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^2*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] + 
     120*api*MBhat^2*Ycut*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^2*Ycut^2*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     30*api*MBhat^2*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     30*api*MBhat^2*Ycut^2*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     120*api*MBhat*Ycut*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat*Ycut^2*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     15*api*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     30*api*Ycut*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     15*api*Ycut^2*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat])/(15*(-1 + Ycut)^2))
