6*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[m2] - 
 6*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[1 - Ycut] + 
 (mupi*(-1/60*((-1 + m2 + Ycut)*(9 - 126*m2 - 846*m2^2 - 126*m2^3 + 9*m2^4 + 
         30*MBhat^2 - 210*m2*MBhat^2 - 210*m2^2*MBhat^2 + 30*m2^3*MBhat^2 - 
         36*Ycut + 513*m2*Ycut + 3807*m2^2*Ycut + 621*m2^3*Ycut - 
         45*m2^4*Ycut - 120*MBhat^2*Ycut + 870*m2*MBhat^2*Ycut + 
         1020*m2^2*MBhat^2*Ycut - 150*m2^3*MBhat^2*Ycut + 54*Ycut^2 - 
         783*m2*Ycut^2 - 6606*m2^2*Ycut^2 - 1215*m2^3*Ycut^2 + 
         90*m2^4*Ycut^2 + 180*MBhat^2*Ycut^2 - 1350*m2*MBhat^2*Ycut^2 - 
         1950*m2^2*MBhat^2*Ycut^2 + 300*m2^3*MBhat^2*Ycut^2 - 36*Ycut^3 + 
         531*m2*Ycut^3 + 5355*m2^2*Ycut^3 + 1170*m2^3*Ycut^3 - 
         90*m2^4*Ycut^3 - 20*MBhat*Ycut^3 - 60*m2*MBhat*Ycut^3 + 
         180*m2^2*MBhat*Ycut^3 - 100*m2^3*MBhat*Ycut^3 - 120*MBhat^2*Ycut^3 + 
         930*m2*MBhat^2*Ycut^3 + 1800*m2^2*MBhat^2*Ycut^3 - 
         300*m2^3*MBhat^2*Ycut^3 + 34*Ycut^4 - 135*m2*Ycut^4 - 
         1920*m2^2*Ycut^4 - 490*m2^3*Ycut^4 + 45*m2^4*Ycut^4 + 
         120*MBhat*Ycut^4 + 260*m2*MBhat*Ycut^4 - 280*m2^2*MBhat*Ycut^4 - 
         100*m2^3*MBhat*Ycut^4 - 20*MBhat^2*Ycut^4 - 290*m2*MBhat^2*Ycut^4 - 
         950*m2^2*MBhat^2*Ycut^4 + 150*m2^3*MBhat^2*Ycut^4 - 124*Ycut^5 + 
         m2*Ycut^5 + 121*m2^2*Ycut^5 + 191*m2^3*Ycut^5 - 9*m2^4*Ycut^5 - 
         270*MBhat*Ycut^5 - 410*m2*MBhat*Ycut^5 + 330*m2^2*MBhat*Ycut^5 + 
         110*m2^3*MBhat*Ycut^5 + 200*MBhat^2*Ycut^5 + 150*m2*MBhat^2*Ycut^5 + 
         340*m2^2*MBhat^2*Ycut^5 - 30*m2^3*MBhat^2*Ycut^5 + 246*Ycut^6 - 
         3*m2*Ycut^6 + 88*m2^2*Ycut^6 - 31*m2^3*Ycut^6 + 280*MBhat*Ycut^6 + 
         270*m2*MBhat*Ycut^6 - 300*m2^2*MBhat*Ycut^6 - 30*m2^3*MBhat*Ycut^6 - 
         300*MBhat^2*Ycut^6 - 150*m2*MBhat^2*Ycut^6 - 50*m2^2*MBhat^2*
          Ycut^6 - 244*Ycut^7 + 3*m2*Ycut^7 + m2^2*Ycut^7 - 
         120*MBhat*Ycut^7 - 50*m2*MBhat*Ycut^7 + 70*m2^2*MBhat*Ycut^7 + 
         200*MBhat^2*Ycut^7 + 50*m2*MBhat^2*Ycut^7 + 121*Ycut^8 - m2*Ycut^8 - 
         10*m2*MBhat*Ycut^8 - 50*MBhat^2*Ycut^8 - 24*Ycut^9 + 
         10*MBhat*Ycut^9))/(-1 + Ycut)^5 + 3*m2^2*(3 + 3*m2 + 2*MBhat^2)*
      Log[m2] - 3*m2^2*(3 + 3*m2 + 2*MBhat^2)*Log[1 - Ycut] + 
     c[SL]^2*(-1/120*((-1 + m2 + Ycut)*(6 - 84*m2 - 564*m2^2 - 84*m2^3 + 
           6*m2^4 + 15*MBhat^2 - 105*m2*MBhat^2 - 105*m2^2*MBhat^2 + 
           15*m2^3*MBhat^2 - 24*Ycut + 342*m2*Ycut + 2538*m2^2*Ycut + 
           414*m2^3*Ycut - 30*m2^4*Ycut - 60*MBhat^2*Ycut + 
           435*m2*MBhat^2*Ycut + 510*m2^2*MBhat^2*Ycut - 75*m2^3*MBhat^2*
            Ycut + 36*Ycut^2 - 522*m2*Ycut^2 - 4404*m2^2*Ycut^2 - 
           810*m2^3*Ycut^2 + 60*m2^4*Ycut^2 + 90*MBhat^2*Ycut^2 - 
           675*m2*MBhat^2*Ycut^2 - 975*m2^2*MBhat^2*Ycut^2 + 
           150*m2^3*MBhat^2*Ycut^2 - 24*Ycut^3 + 354*m2*Ycut^3 + 
           3570*m2^2*Ycut^3 + 780*m2^3*Ycut^3 - 60*m2^4*Ycut^3 - 
           50*MBhat*Ycut^3 + 50*m2*MBhat*Ycut^3 + 50*m2^2*MBhat*Ycut^3 - 
           50*m2^3*MBhat*Ycut^3 - 60*MBhat^2*Ycut^3 + 465*m2*MBhat^2*Ycut^3 + 
           900*m2^2*MBhat^2*Ycut^3 - 150*m2^3*MBhat^2*Ycut^3 + 31*Ycut^4 - 
           115*m2*Ycut^4 - 1255*m2^2*Ycut^4 - 335*m2^3*Ycut^4 + 
           30*m2^4*Ycut^4 + 260*MBhat*Ycut^4 - 190*m2*MBhat*Ycut^4 - 
           20*m2^2*MBhat*Ycut^4 - 50*m2^3*MBhat*Ycut^4 - 10*MBhat^2*Ycut^4 - 
           145*m2*MBhat^2*Ycut^4 - 475*m2^2*MBhat^2*Ycut^4 + 
           75*m2^3*MBhat^2*Ycut^4 - 136*Ycut^5 + 89*m2*Ycut^5 + 
           64*m2^2*Ycut^5 + 109*m2^3*Ycut^5 - 6*m2^4*Ycut^5 - 
           525*MBhat*Ycut^5 + 285*m2*MBhat*Ycut^5 + 25*m2^2*MBhat*Ycut^5 + 
           55*m2^3*MBhat*Ycut^5 + 100*MBhat^2*Ycut^5 + 75*m2*MBhat^2*Ycut^5 + 
           170*m2^2*MBhat^2*Ycut^5 - 15*m2^3*MBhat^2*Ycut^5 + 294*Ycut^6 - 
           117*m2*Ycut^6 + 37*m2^2*Ycut^6 - 14*m2^3*Ycut^6 + 
           500*MBhat*Ycut^6 - 215*m2*MBhat*Ycut^6 - 70*m2^2*MBhat*Ycut^6 - 
           15*m2^3*MBhat*Ycut^6 - 150*MBhat^2*Ycut^6 - 75*m2*MBhat^2*Ycut^6 - 
           25*m2^2*MBhat^2*Ycut^6 - 316*Ycut^7 + 67*m2*Ycut^7 + 
           14*m2^2*Ycut^7 - 200*MBhat*Ycut^7 + 85*m2*MBhat*Ycut^7 + 
           15*m2^2*MBhat*Ycut^7 + 100*MBhat^2*Ycut^7 + 25*m2*MBhat^2*Ycut^7 + 
           169*Ycut^8 - 14*m2*Ycut^8 - 15*m2*MBhat*Ycut^8 - 
           25*MBhat^2*Ycut^8 - 36*Ycut^9 + 15*MBhat*Ycut^9))/(-1 + Ycut)^5 + 
       (3*m2^2*(2 + 2*m2 + MBhat^2)*Log[m2])/2 - 
       (3*m2^2*(2 + 2*m2 + MBhat^2)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/120*((-1 + m2 + Ycut)*(6 - 84*m2 - 564*m2^2 - 84*m2^3 + 
           6*m2^4 + 15*MBhat^2 - 105*m2*MBhat^2 - 105*m2^2*MBhat^2 + 
           15*m2^3*MBhat^2 - 24*Ycut + 342*m2*Ycut + 2538*m2^2*Ycut + 
           414*m2^3*Ycut - 30*m2^4*Ycut - 60*MBhat^2*Ycut + 
           435*m2*MBhat^2*Ycut + 510*m2^2*MBhat^2*Ycut - 75*m2^3*MBhat^2*
            Ycut + 36*Ycut^2 - 522*m2*Ycut^2 - 4404*m2^2*Ycut^2 - 
           810*m2^3*Ycut^2 + 60*m2^4*Ycut^2 + 90*MBhat^2*Ycut^2 - 
           675*m2*MBhat^2*Ycut^2 - 975*m2^2*MBhat^2*Ycut^2 + 
           150*m2^3*MBhat^2*Ycut^2 - 24*Ycut^3 + 354*m2*Ycut^3 + 
           3570*m2^2*Ycut^3 + 780*m2^3*Ycut^3 - 60*m2^4*Ycut^3 - 
           50*MBhat*Ycut^3 + 50*m2*MBhat*Ycut^3 + 50*m2^2*MBhat*Ycut^3 - 
           50*m2^3*MBhat*Ycut^3 - 60*MBhat^2*Ycut^3 + 465*m2*MBhat^2*Ycut^3 + 
           900*m2^2*MBhat^2*Ycut^3 - 150*m2^3*MBhat^2*Ycut^3 + 31*Ycut^4 - 
           115*m2*Ycut^4 - 1255*m2^2*Ycut^4 - 335*m2^3*Ycut^4 + 
           30*m2^4*Ycut^4 + 260*MBhat*Ycut^4 - 190*m2*MBhat*Ycut^4 - 
           20*m2^2*MBhat*Ycut^4 - 50*m2^3*MBhat*Ycut^4 - 10*MBhat^2*Ycut^4 - 
           145*m2*MBhat^2*Ycut^4 - 475*m2^2*MBhat^2*Ycut^4 + 
           75*m2^3*MBhat^2*Ycut^4 - 136*Ycut^5 + 89*m2*Ycut^5 + 
           64*m2^2*Ycut^5 + 109*m2^3*Ycut^5 - 6*m2^4*Ycut^5 - 
           525*MBhat*Ycut^5 + 285*m2*MBhat*Ycut^5 + 25*m2^2*MBhat*Ycut^5 + 
           55*m2^3*MBhat*Ycut^5 + 100*MBhat^2*Ycut^5 + 75*m2*MBhat^2*Ycut^5 + 
           170*m2^2*MBhat^2*Ycut^5 - 15*m2^3*MBhat^2*Ycut^5 + 294*Ycut^6 - 
           117*m2*Ycut^6 + 37*m2^2*Ycut^6 - 14*m2^3*Ycut^6 + 
           500*MBhat*Ycut^6 - 215*m2*MBhat*Ycut^6 - 70*m2^2*MBhat*Ycut^6 - 
           15*m2^3*MBhat*Ycut^6 - 150*MBhat^2*Ycut^6 - 75*m2*MBhat^2*Ycut^6 - 
           25*m2^2*MBhat^2*Ycut^6 - 316*Ycut^7 + 67*m2*Ycut^7 + 
           14*m2^2*Ycut^7 - 200*MBhat*Ycut^7 + 85*m2*MBhat*Ycut^7 + 
           15*m2^2*MBhat*Ycut^7 + 100*MBhat^2*Ycut^7 + 25*m2*MBhat^2*Ycut^7 + 
           169*Ycut^8 - 14*m2*Ycut^8 - 15*m2*MBhat*Ycut^8 - 
           25*MBhat^2*Ycut^8 - 36*Ycut^9 + 15*MBhat*Ycut^9))/(-1 + Ycut)^5 + 
       (3*m2^2*(2 + 2*m2 + MBhat^2)*Log[m2])/2 - 
       (3*m2^2*(2 + 2*m2 + MBhat^2)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(-3 + 42*m2 + 282*m2^2 + 42*m2^3 - 3*m2^4 - 
          10*MBhat^2 + 70*m2*MBhat^2 + 70*m2^2*MBhat^2 - 10*m2^3*MBhat^2 + 
          6*Ycut - 87*m2*Ycut - 705*m2^2*Ycut - 123*m2^3*Ycut + 9*m2^4*Ycut + 
          20*MBhat^2*Ycut - 150*m2*MBhat^2*Ycut - 200*m2^2*MBhat^2*Ycut + 
          30*m2^3*MBhat^2*Ycut - 3*Ycut^2 + 45*m2*Ycut^2 + 510*m2^2*Ycut^2 + 
          117*m2^3*Ycut^2 - 9*m2^4*Ycut^2 - 10*MBhat^2*Ycut^2 + 
          80*m2*MBhat^2*Ycut^2 + 180*m2^2*MBhat^2*Ycut^2 - 
          30*m2^3*MBhat^2*Ycut^2 - 60*m2^2*Ycut^3 - 33*m2^3*Ycut^3 + 
          3*m2^4*Ycut^3 + 40*MBhat*Ycut^3 - 80*m2*MBhat*Ycut^3 + 
          40*m2^2*MBhat*Ycut^3 - 40*m2^2*MBhat^2*Ycut^3 + 
          10*m2^3*MBhat^2*Ycut^3 - 25*Ycut^4 + 50*m2*Ycut^4 - 
          40*m2^2*Ycut^4 - 3*m2^3*Ycut^4 - 160*MBhat*Ycut^4 + 
          120*m2*MBhat*Ycut^4 + 40*m2^2*MBhat*Ycut^4 + 50*MBhat^2*Ycut^4 + 
          50*m2*MBhat^2*Ycut^4 - 10*m2^2*MBhat^2*Ycut^4 + 98*Ycut^5 - 
          77*m2*Ycut^5 - 27*m2^2*Ycut^5 + 180*MBhat*Ycut^5 - 
          60*m2*MBhat*Ycut^5 - 40*m2^2*MBhat*Ycut^5 - 100*MBhat^2*Ycut^5 - 
          50*m2*MBhat^2*Ycut^5 - 121*Ycut^6 + 27*m2*Ycut^6 - 
          40*MBhat*Ycut^6 + 20*m2*MBhat*Ycut^6 + 50*MBhat^2*Ycut^6 + 
          48*Ycut^7 - 20*MBhat*Ycut^7))/(20*(-1 + Ycut)^3) + 
       3*m2^2*(3 + 3*m2 + 2*MBhat^2)*Log[m2] - 3*m2^2*(3 + 3*m2 + 2*MBhat^2)*
        Log[1 - Ycut]) + c[VL]^2*
      (-1/60*((-1 + m2 + Ycut)*(9 - 126*m2 - 846*m2^2 - 126*m2^3 + 9*m2^4 + 
           30*MBhat^2 - 210*m2*MBhat^2 - 210*m2^2*MBhat^2 + 30*m2^3*MBhat^2 - 
           36*Ycut + 513*m2*Ycut + 3807*m2^2*Ycut + 621*m2^3*Ycut - 
           45*m2^4*Ycut - 120*MBhat^2*Ycut + 870*m2*MBhat^2*Ycut + 
           1020*m2^2*MBhat^2*Ycut - 150*m2^3*MBhat^2*Ycut + 54*Ycut^2 - 
           783*m2*Ycut^2 - 6606*m2^2*Ycut^2 - 1215*m2^3*Ycut^2 + 
           90*m2^4*Ycut^2 + 180*MBhat^2*Ycut^2 - 1350*m2*MBhat^2*Ycut^2 - 
           1950*m2^2*MBhat^2*Ycut^2 + 300*m2^3*MBhat^2*Ycut^2 - 36*Ycut^3 + 
           531*m2*Ycut^3 + 5355*m2^2*Ycut^3 + 1170*m2^3*Ycut^3 - 
           90*m2^4*Ycut^3 - 20*MBhat*Ycut^3 - 60*m2*MBhat*Ycut^3 + 
           180*m2^2*MBhat*Ycut^3 - 100*m2^3*MBhat*Ycut^3 - 
           120*MBhat^2*Ycut^3 + 930*m2*MBhat^2*Ycut^3 + 1800*m2^2*MBhat^2*
            Ycut^3 - 300*m2^3*MBhat^2*Ycut^3 + 34*Ycut^4 - 135*m2*Ycut^4 - 
           1920*m2^2*Ycut^4 - 490*m2^3*Ycut^4 + 45*m2^4*Ycut^4 + 
           120*MBhat*Ycut^4 + 260*m2*MBhat*Ycut^4 - 280*m2^2*MBhat*Ycut^4 - 
           100*m2^3*MBhat*Ycut^4 - 20*MBhat^2*Ycut^4 - 290*m2*MBhat^2*
            Ycut^4 - 950*m2^2*MBhat^2*Ycut^4 + 150*m2^3*MBhat^2*Ycut^4 - 
           124*Ycut^5 + m2*Ycut^5 + 121*m2^2*Ycut^5 + 191*m2^3*Ycut^5 - 
           9*m2^4*Ycut^5 - 270*MBhat*Ycut^5 - 410*m2*MBhat*Ycut^5 + 
           330*m2^2*MBhat*Ycut^5 + 110*m2^3*MBhat*Ycut^5 + 
           200*MBhat^2*Ycut^5 + 150*m2*MBhat^2*Ycut^5 + 340*m2^2*MBhat^2*
            Ycut^5 - 30*m2^3*MBhat^2*Ycut^5 + 246*Ycut^6 - 3*m2*Ycut^6 + 
           88*m2^2*Ycut^6 - 31*m2^3*Ycut^6 + 280*MBhat*Ycut^6 + 
           270*m2*MBhat*Ycut^6 - 300*m2^2*MBhat*Ycut^6 - 30*m2^3*MBhat*
            Ycut^6 - 300*MBhat^2*Ycut^6 - 150*m2*MBhat^2*Ycut^6 - 
           50*m2^2*MBhat^2*Ycut^6 - 244*Ycut^7 + 3*m2*Ycut^7 + m2^2*Ycut^7 - 
           120*MBhat*Ycut^7 - 50*m2*MBhat*Ycut^7 + 70*m2^2*MBhat*Ycut^7 + 
           200*MBhat^2*Ycut^7 + 50*m2*MBhat^2*Ycut^7 + 121*Ycut^8 - 
           m2*Ycut^8 - 10*m2*MBhat*Ycut^8 - 50*MBhat^2*Ycut^8 - 24*Ycut^9 + 
           10*MBhat*Ycut^9))/(-1 + Ycut)^5 + 3*m2^2*(3 + 3*m2 + 2*MBhat^2)*
        Log[m2] - 3*m2^2*(3 + 3*m2 + 2*MBhat^2)*Log[1 - Ycut]) + 
     c[T]^2*((-2*(-1 + m2 + Ycut)*(12 - 168*m2 - 1128*m2^2 - 168*m2^3 + 
          12*m2^4 + 45*MBhat^2 - 315*m2*MBhat^2 - 315*m2^2*MBhat^2 + 
          45*m2^3*MBhat^2 - 48*Ycut + 684*m2*Ycut + 5076*m2^2*Ycut + 
          828*m2^3*Ycut - 60*m2^4*Ycut - 180*MBhat^2*Ycut + 
          1305*m2*MBhat^2*Ycut + 1530*m2^2*MBhat^2*Ycut - 
          225*m2^3*MBhat^2*Ycut + 72*Ycut^2 - 1044*m2*Ycut^2 - 
          8808*m2^2*Ycut^2 - 1620*m2^3*Ycut^2 + 120*m2^4*Ycut^2 + 
          270*MBhat^2*Ycut^2 - 2025*m2*MBhat^2*Ycut^2 - 2925*m2^2*MBhat^2*
           Ycut^2 + 450*m2^3*MBhat^2*Ycut^2 - 48*Ycut^3 + 708*m2*Ycut^3 + 
          7140*m2^2*Ycut^3 + 1560*m2^3*Ycut^3 - 120*m2^4*Ycut^3 - 
          90*MBhat*Ycut^3 + 130*m2*MBhat*Ycut^3 + 10*m2^2*MBhat*Ycut^3 - 
          50*m2^3*MBhat*Ycut^3 - 180*MBhat^2*Ycut^3 + 1395*m2*MBhat^2*
           Ycut^3 + 2700*m2^2*MBhat^2*Ycut^3 - 450*m2^3*MBhat^2*Ycut^3 + 
          87*Ycut^4 - 305*m2*Ycut^4 - 2435*m2^2*Ycut^4 - 695*m2^3*Ycut^4 + 
          60*m2^4*Ycut^4 + 580*MBhat*Ycut^4 - 390*m2*MBhat*Ycut^4 - 
          140*m2^2*MBhat*Ycut^4 - 50*m2^3*MBhat*Ycut^4 - 130*MBhat^2*Ycut^4 - 
          535*m2*MBhat^2*Ycut^4 - 1225*m2^2*MBhat^2*Ycut^4 + 
          225*m2^3*MBhat^2*Ycut^4 - 432*Ycut^5 + 443*m2*Ycut^5 + 
          78*m2^2*Ycut^5 + 163*m2^3*Ycut^5 - 12*m2^4*Ycut^5 - 
          1365*MBhat*Ycut^5 + 445*m2*MBhat*Ycut^5 + 545*m2^2*MBhat*Ycut^5 + 
          55*m2^3*MBhat*Ycut^5 + 700*MBhat^2*Ycut^5 + 525*m2*MBhat^2*Ycut^5 + 
          230*m2^2*MBhat^2*Ycut^5 - 45*m2^3*MBhat^2*Ycut^5 + 978*Ycut^6 - 
          579*m2*Ycut^6 + 9*m2^2*Ycut^6 - 8*m2^3*Ycut^6 + 1460*MBhat*Ycut^6 - 
          295*m2*MBhat*Ycut^6 - 590*m2^2*MBhat*Ycut^6 - 
          15*m2^3*MBhat*Ycut^6 - 1050*MBhat^2*Ycut^6 - 525*m2*MBhat^2*
           Ycut^6 + 5*m2^2*MBhat^2*Ycut^6 - 1092*Ycut^7 + 329*m2*Ycut^7 + 
          68*m2^2*Ycut^7 - 640*MBhat*Ycut^7 + 165*m2*MBhat*Ycut^7 + 
          175*m2^2*MBhat*Ycut^7 + 700*MBhat^2*Ycut^7 + 175*m2*MBhat^2*
           Ycut^7 + 603*Ycut^8 - 68*m2*Ycut^8 - 55*m2*MBhat*Ycut^8 - 
          175*MBhat^2*Ycut^8 - 132*Ycut^9 + 55*MBhat*Ycut^9))/
        (15*(-1 + Ycut)^5) + 24*m2^2*(4 + 4*m2 + 3*MBhat^2)*Log[m2] - 
       24*m2^2*(4 + 4*m2 + 3*MBhat^2)*Log[1 - Ycut]) + 
     c[VR]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*(3 + 87*m2 + 87*m2^2 + 3*m2^3 + 
           6*MBhat^2 + 60*m2*MBhat^2 + 6*m2^2*MBhat^2 - 9*Ycut - 
           294*m2*Ycut - 315*m2^2*Ycut - 12*m2^3*Ycut - 18*MBhat^2*Ycut - 
           210*m2*MBhat^2*Ycut - 24*m2^2*MBhat^2*Ycut + 9*Ycut^2 + 
           345*m2*Ycut^2 + 408*m2^2*Ycut^2 + 18*m2^3*Ycut^2 + 
           18*MBhat^2*Ycut^2 + 258*m2*MBhat^2*Ycut^2 + 36*m2^2*MBhat^2*
            Ycut^2 - 3*Ycut^3 - 150*m2*Ycut^3 - 210*m2^2*Ycut^3 - 
           12*m2^3*Ycut^3 - 10*MBhat*Ycut^3 + 20*m2*MBhat*Ycut^3 - 
           10*m2^2*MBhat*Ycut^3 - 6*MBhat^2*Ycut^3 - 120*m2*MBhat^2*Ycut^3 - 
           24*m2^2*MBhat^2*Ycut^3 + 5*Ycut^4 - m2*Ycut^4 + 29*m2^2*Ycut^4 + 
           3*m2^3*Ycut^4 + 30*MBhat*Ycut^4 - 22*m2*MBhat*Ycut^4 - 
           8*m2^2*MBhat*Ycut^4 - 6*m2*MBhat^2*Ycut^4 + 6*m2^2*MBhat^2*
            Ycut^4 - 15*Ycut^5 + 8*m2*Ycut^5 + 13*m2^2*Ycut^5 - 
           30*MBhat*Ycut^5 + 8*m2*MBhat*Ycut^5 + 6*m2^2*MBhat*Ycut^5 + 
           18*m2*MBhat^2*Ycut^5 + 15*Ycut^6 + 5*m2*Ycut^6 + 10*MBhat*Ycut^6 - 
           6*m2*MBhat*Ycut^6 - 5*Ycut^7))/(-1 + Ycut)^4 + 
       12*m2^(3/2)*(1 + 3*m2 + m2^2 + MBhat^2 + m2*MBhat^2)*Log[m2] - 
       12*m2^(3/2)*(1 + 3*m2 + m2^2 + MBhat^2 + m2*MBhat^2)*Log[1 - Ycut]) + 
     c[VL]*(-1/30*((-1 + m2 + Ycut)*(9 - 126*m2 - 846*m2^2 - 126*m2^3 + 
           9*m2^4 + 30*MBhat^2 - 210*m2*MBhat^2 - 210*m2^2*MBhat^2 + 
           30*m2^3*MBhat^2 - 36*Ycut + 513*m2*Ycut + 3807*m2^2*Ycut + 
           621*m2^3*Ycut - 45*m2^4*Ycut - 120*MBhat^2*Ycut + 
           870*m2*MBhat^2*Ycut + 1020*m2^2*MBhat^2*Ycut - 150*m2^3*MBhat^2*
            Ycut + 54*Ycut^2 - 783*m2*Ycut^2 - 6606*m2^2*Ycut^2 - 
           1215*m2^3*Ycut^2 + 90*m2^4*Ycut^2 + 180*MBhat^2*Ycut^2 - 
           1350*m2*MBhat^2*Ycut^2 - 1950*m2^2*MBhat^2*Ycut^2 + 
           300*m2^3*MBhat^2*Ycut^2 - 36*Ycut^3 + 531*m2*Ycut^3 + 
           5355*m2^2*Ycut^3 + 1170*m2^3*Ycut^3 - 90*m2^4*Ycut^3 - 
           20*MBhat*Ycut^3 - 60*m2*MBhat*Ycut^3 + 180*m2^2*MBhat*Ycut^3 - 
           100*m2^3*MBhat*Ycut^3 - 120*MBhat^2*Ycut^3 + 930*m2*MBhat^2*
            Ycut^3 + 1800*m2^2*MBhat^2*Ycut^3 - 300*m2^3*MBhat^2*Ycut^3 + 
           34*Ycut^4 - 135*m2*Ycut^4 - 1920*m2^2*Ycut^4 - 490*m2^3*Ycut^4 + 
           45*m2^4*Ycut^4 + 120*MBhat*Ycut^4 + 260*m2*MBhat*Ycut^4 - 
           280*m2^2*MBhat*Ycut^4 - 100*m2^3*MBhat*Ycut^4 - 
           20*MBhat^2*Ycut^4 - 290*m2*MBhat^2*Ycut^4 - 950*m2^2*MBhat^2*
            Ycut^4 + 150*m2^3*MBhat^2*Ycut^4 - 124*Ycut^5 + m2*Ycut^5 + 
           121*m2^2*Ycut^5 + 191*m2^3*Ycut^5 - 9*m2^4*Ycut^5 - 
           270*MBhat*Ycut^5 - 410*m2*MBhat*Ycut^5 + 330*m2^2*MBhat*Ycut^5 + 
           110*m2^3*MBhat*Ycut^5 + 200*MBhat^2*Ycut^5 + 150*m2*MBhat^2*
            Ycut^5 + 340*m2^2*MBhat^2*Ycut^5 - 30*m2^3*MBhat^2*Ycut^5 + 
           246*Ycut^6 - 3*m2*Ycut^6 + 88*m2^2*Ycut^6 - 31*m2^3*Ycut^6 + 
           280*MBhat*Ycut^6 + 270*m2*MBhat*Ycut^6 - 300*m2^2*MBhat*Ycut^6 - 
           30*m2^3*MBhat*Ycut^6 - 300*MBhat^2*Ycut^6 - 150*m2*MBhat^2*
            Ycut^6 - 50*m2^2*MBhat^2*Ycut^6 - 244*Ycut^7 + 3*m2*Ycut^7 + 
           m2^2*Ycut^7 - 120*MBhat*Ycut^7 - 50*m2*MBhat*Ycut^7 + 
           70*m2^2*MBhat*Ycut^7 + 200*MBhat^2*Ycut^7 + 50*m2*MBhat^2*Ycut^7 + 
           121*Ycut^8 - m2*Ycut^8 - 10*m2*MBhat*Ycut^8 - 50*MBhat^2*Ycut^8 - 
           24*Ycut^9 + 10*MBhat*Ycut^9))/(-1 + Ycut)^5 + 
       6*m2^2*(3 + 3*m2 + 2*MBhat^2)*Log[m2] - 6*m2^2*(3 + 3*m2 + 2*MBhat^2)*
        Log[1 - Ycut] + c[VR]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*
            (3 + 87*m2 + 87*m2^2 + 3*m2^3 + 6*MBhat^2 + 60*m2*MBhat^2 + 
             6*m2^2*MBhat^2 - 9*Ycut - 294*m2*Ycut - 315*m2^2*Ycut - 
             12*m2^3*Ycut - 18*MBhat^2*Ycut - 210*m2*MBhat^2*Ycut - 
             24*m2^2*MBhat^2*Ycut + 9*Ycut^2 + 345*m2*Ycut^2 + 
             408*m2^2*Ycut^2 + 18*m2^3*Ycut^2 + 18*MBhat^2*Ycut^2 + 
             258*m2*MBhat^2*Ycut^2 + 36*m2^2*MBhat^2*Ycut^2 - 3*Ycut^3 - 
             150*m2*Ycut^3 - 210*m2^2*Ycut^3 - 12*m2^3*Ycut^3 - 
             10*MBhat*Ycut^3 + 20*m2*MBhat*Ycut^3 - 10*m2^2*MBhat*Ycut^3 - 
             6*MBhat^2*Ycut^3 - 120*m2*MBhat^2*Ycut^3 - 24*m2^2*MBhat^2*
              Ycut^3 + 5*Ycut^4 - m2*Ycut^4 + 29*m2^2*Ycut^4 + 
             3*m2^3*Ycut^4 + 30*MBhat*Ycut^4 - 22*m2*MBhat*Ycut^4 - 
             8*m2^2*MBhat*Ycut^4 - 6*m2*MBhat^2*Ycut^4 + 6*m2^2*MBhat^2*
              Ycut^4 - 15*Ycut^5 + 8*m2*Ycut^5 + 13*m2^2*Ycut^5 - 
             30*MBhat*Ycut^5 + 8*m2*MBhat*Ycut^5 + 6*m2^2*MBhat*Ycut^5 + 
             18*m2*MBhat^2*Ycut^5 + 15*Ycut^6 + 5*m2*Ycut^6 + 
             10*MBhat*Ycut^6 - 6*m2*MBhat*Ycut^6 - 5*Ycut^7))/(-1 + Ycut)^4 + 
         12*m2^(3/2)*(1 + 3*m2 + m2^2 + MBhat^2 + m2*MBhat^2)*Log[m2] - 
         12*m2^(3/2)*(1 + 3*m2 + m2^2 + MBhat^2 + m2*MBhat^2)*
          Log[1 - Ycut])) + 
     c[SL]*(-1/6*(Ycut^3*(-1 + m2 + Ycut)*(10*MBhat - 30*m2*MBhat + 
           30*m2^2*MBhat - 10*m2^3*MBhat - 5*Ycut + 15*m2*Ycut - 
           15*m2^2*Ycut + 5*m2^3*Ycut - 60*MBhat*Ycut + 110*m2*MBhat*Ycut - 
           40*m2^2*MBhat*Ycut - 10*m2^3*MBhat*Ycut + 10*MBhat^2*Ycut + 
           10*m2*MBhat^2*Ycut - 20*m2^2*MBhat^2*Ycut + 32*Ycut^2 - 
           53*m2*Ycut^2 + 10*m2^2*Ycut^2 + 11*m2^3*Ycut^2 + 
           135*MBhat*Ycut^2 - 155*m2*MBhat*Ycut^2 + 9*m2^2*MBhat*Ycut^2 + 
           11*m2^3*MBhat*Ycut^2 - 40*MBhat^2*Ycut^2 - 30*m2*MBhat^2*Ycut^2 + 
           28*m2^2*MBhat^2*Ycut^2 - 78*Ycut^3 + 69*m2*Ycut^3 + 
           13*m2^2*Ycut^3 - 4*m2^3*Ycut^3 - 140*MBhat*Ycut^3 + 
           105*m2*MBhat*Ycut^3 + 6*m2^2*MBhat*Ycut^3 - 3*m2^3*MBhat*Ycut^3 + 
           60*MBhat^2*Ycut^3 + 30*m2*MBhat^2*Ycut^3 - 8*m2^2*MBhat^2*Ycut^3 + 
           92*Ycut^4 - 39*m2*Ycut^4 - 8*m2^2*Ycut^4 + 60*MBhat*Ycut^4 - 
           35*m2*MBhat*Ycut^4 - 5*m2^2*MBhat*Ycut^4 - 40*MBhat^2*Ycut^4 - 
           10*m2*MBhat^2*Ycut^4 - 53*Ycut^5 + 8*m2*Ycut^5 + 
           5*m2*MBhat*Ycut^5 + 10*MBhat^2*Ycut^5 + 12*Ycut^6 - 
           5*MBhat*Ycut^6)*c[T])/(-1 + Ycut)^5 + 
       c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(3 + 87*m2 + 87*m2^2 + 3*m2^3 + 
            6*MBhat^2 + 60*m2*MBhat^2 + 6*m2^2*MBhat^2 - 9*Ycut - 
            294*m2*Ycut - 315*m2^2*Ycut - 12*m2^3*Ycut - 18*MBhat^2*Ycut - 
            210*m2*MBhat^2*Ycut - 24*m2^2*MBhat^2*Ycut + 9*Ycut^2 + 
            345*m2*Ycut^2 + 408*m2^2*Ycut^2 + 18*m2^3*Ycut^2 + 
            18*MBhat^2*Ycut^2 + 258*m2*MBhat^2*Ycut^2 + 36*m2^2*MBhat^2*
             Ycut^2 - 3*Ycut^3 - 150*m2*Ycut^3 - 210*m2^2*Ycut^3 - 
            12*m2^3*Ycut^3 - 10*MBhat*Ycut^3 + 20*m2*MBhat*Ycut^3 - 
            10*m2^2*MBhat*Ycut^3 - 6*MBhat^2*Ycut^3 - 120*m2*MBhat^2*Ycut^3 - 
            24*m2^2*MBhat^2*Ycut^3 + 5*Ycut^4 - m2*Ycut^4 + 29*m2^2*Ycut^4 + 
            3*m2^3*Ycut^4 + 30*MBhat*Ycut^4 - 22*m2*MBhat*Ycut^4 - 
            8*m2^2*MBhat*Ycut^4 - 6*m2*MBhat^2*Ycut^4 + 6*m2^2*MBhat^2*
             Ycut^4 - 15*Ycut^5 + 8*m2*Ycut^5 + 13*m2^2*Ycut^5 - 
            30*MBhat*Ycut^5 + 8*m2*MBhat*Ycut^5 + 6*m2^2*MBhat*Ycut^5 + 
            18*m2*MBhat^2*Ycut^5 + 15*Ycut^6 + 5*m2*Ycut^6 + 
            10*MBhat*Ycut^6 - 6*m2*MBhat*Ycut^6 - 5*Ycut^7))/
          (6*(-1 + Ycut)^4) - 6*m2^(3/2)*(1 + 3*m2 + m2^2 + MBhat^2 + 
           m2*MBhat^2)*Log[m2] + 6*m2^(3/2)*(1 + 3*m2 + m2^2 + MBhat^2 + 
           m2*MBhat^2)*Log[1 - Ycut]))) + 
   muG*(((-1 + m2 + Ycut)*(15 + 22*m2 + 22*m2^2 + 70*m2^3 - 9*m2^4 - 
        36*MBhat + 32*m2*MBhat - 176*m2^2*MBhat + 112*m2^3*MBhat - 
        28*m2^4*MBhat + 18*MBhat^2 - 30*m2*MBhat^2 + 114*m2^2*MBhat^2 - 
        30*m2^3*MBhat^2 - 45*Ycut - 99*m2*Ycut - 41*m2^2*Ycut - 
        271*m2^3*Ycut + 36*m2^4*Ycut + 108*MBhat*Ycut - 84*m2*MBhat*Ycut + 
        620*m2^2*MBhat*Ycut - 420*m2^3*MBhat*Ycut + 112*m2^4*MBhat*Ycut - 
        54*MBhat^2*Ycut + 108*m2*MBhat^2*Ycut - 426*m2^2*MBhat^2*Ycut + 
        120*m2^3*MBhat^2*Ycut + 45*Ycut^2 + 156*m2*Ycut^2 - 11*m2^2*Ycut^2 + 
        384*m2^3*Ycut^2 - 54*m2^4*Ycut^2 - 108*MBhat*Ycut^2 + 
        48*m2*MBhat*Ycut^2 - 748*m2^2*MBhat*Ycut^2 + 560*m2^3*MBhat*Ycut^2 - 
        168*m2^4*MBhat*Ycut^2 + 54*MBhat^2*Ycut^2 - 126*m2*MBhat^2*Ycut^2 + 
        564*m2^2*MBhat^2*Ycut^2 - 180*m2^3*MBhat^2*Ycut^2 - 15*Ycut^3 - 
        95*m2*Ycut^3 + 50*m2^2*Ycut^3 - 226*m2^3*Ycut^3 + 36*m2^4*Ycut^3 + 
        16*MBhat*Ycut^3 + 104*m2*MBhat*Ycut^3 + 348*m2^2*MBhat*Ycut^3 - 
        380*m2^3*MBhat*Ycut^3 + 112*m2^4*MBhat*Ycut^3 - 2*MBhat^2*Ycut^3 + 
        16*m2*MBhat^2*Ycut^3 - 356*m2^2*MBhat^2*Ycut^3 + 
        120*m2^3*MBhat^2*Ycut^3 + 7*Ycut^4 - 12*m2*Ycut^4 - 46*m2^2*Ycut^4 + 
        84*m2^3*Ycut^4 - 9*m2^4*Ycut^4 + 48*MBhat*Ycut^4 - 
        212*m2*MBhat*Ycut^4 + 44*m2^2*MBhat*Ycut^4 + 100*m2^3*MBhat*Ycut^4 - 
        28*m2^4*MBhat*Ycut^4 - 38*MBhat^2*Ycut^4 + 74*m2*MBhat^2*Ycut^4 + 
        114*m2^2*MBhat^2*Ycut^4 - 30*m2^3*MBhat^2*Ycut^4 - 17*Ycut^5 + 
        47*m2*Ycut^5 + 25*m2^2*Ycut^5 - 11*m2^3*Ycut^5 - 26*MBhat*Ycut^5 + 
        146*m2*MBhat*Ycut^5 - 110*m2^2*MBhat*Ycut^5 - 2*m2^3*MBhat*Ycut^5 + 
        18*MBhat^2*Ycut^5 - 52*m2*MBhat^2*Ycut^5 - 10*m2^2*MBhat^2*Ycut^5 + 
        9*Ycut^6 - 18*m2*Ycut^6 + m2^2*Ycut^6 - 10*MBhat*Ycut^6 - 
        32*m2*MBhat*Ycut^6 + 22*m2^2*MBhat*Ycut^6 + 14*MBhat^2*Ycut^6 + 
        10*m2*MBhat^2*Ycut^6 + 5*Ycut^7 - m2*Ycut^7 + 6*MBhat*Ycut^7 - 
        2*m2*MBhat*Ycut^7 - 10*MBhat^2*Ycut^7 - 4*Ycut^8 + 2*MBhat*Ycut^8))/
      (12*(-1 + Ycut)^4) - m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 4*m2*MBhat + 
       6*m2*MBhat^2)*Log[m2] + m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 4*m2*MBhat + 
       6*m2*MBhat^2)*Log[1 - Ycut] + 
     c[SL]^2*(((-1 + m2 + Ycut)*(-30 - 612*m2 - 132*m2^2 + 60*m2^3 - 6*m2^4 + 
          72*MBhat + 684*m2*MBhat - 84*m2^2*MBhat + 60*m2^3*MBhat - 
          12*m2^4*MBhat - 39*MBhat^2 - 135*m2*MBhat^2 + 81*m2^2*MBhat^2 - 
          15*m2^3*MBhat^2 + 90*Ycut + 2094*m2*Ycut + 546*m2^2*Ycut - 
          234*m2^3*Ycut + 24*m2^4*Ycut - 216*MBhat*Ycut - 
          2412*m2*MBhat*Ycut + 288*m2^2*MBhat*Ycut - 228*m2^3*MBhat*Ycut + 
          48*m2^4*MBhat*Ycut + 117*MBhat^2*Ycut + 510*m2*MBhat^2*Ycut - 
          309*m2^2*MBhat^2*Ycut + 60*m2^3*MBhat^2*Ycut - 90*Ycut^2 - 
          2496*m2*Ycut^2 - 834*m2^2*Ycut^2 + 336*m2^3*Ycut^2 - 
          36*m2^4*Ycut^2 + 216*MBhat*Ycut^2 + 2988*m2*MBhat*Ycut^2 - 
          324*m2^2*MBhat*Ycut^2 + 312*m2^3*MBhat*Ycut^2 - 
          72*m2^4*MBhat*Ycut^2 - 117*MBhat^2*Ycut^2 - 687*m2*MBhat^2*Ycut^2 + 
          426*m2^2*MBhat^2*Ycut^2 - 90*m2^3*MBhat^2*Ycut^2 + 30*Ycut^3 + 
          1110*m2*Ycut^3 + 540*m2^2*Ycut^3 - 204*m2^3*Ycut^3 + 
          24*m2^4*Ycut^3 - 82*MBhat*Ycut^3 - 1434*m2*MBhat*Ycut^3 + 
          186*m2^2*MBhat*Ycut^3 - 218*m2^3*MBhat*Ycut^3 + 
          48*m2^4*MBhat*Ycut^3 + 47*MBhat^2*Ycut^3 + 392*m2*MBhat^2*Ycut^3 - 
          274*m2^2*MBhat^2*Ycut^3 + 60*m2^3*MBhat^2*Ycut^3 + 5*Ycut^4 - 
          57*m2*Ycut^4 - 147*m2^2*Ycut^4 + 61*m2^3*Ycut^4 - 6*m2^4*Ycut^4 + 
          16*MBhat*Ycut^4 + 138*m2*MBhat*Ycut^4 - 60*m2^2*MBhat*Ycut^4 + 
          62*m2^3*MBhat*Ycut^4 - 12*m2^4*MBhat*Ycut^4 - 19*MBhat^2*Ycut^4 - 
          83*m2*MBhat^2*Ycut^4 + 81*m2^2*MBhat^2*Ycut^4 - 
          15*m2^3*MBhat^2*Ycut^4 - 9*Ycut^5 - 34*m2*Ycut^5 + 23*m2^2*Ycut^5 - 
          4*m2^3*Ycut^5 + 9*MBhat*Ycut^5 + 27*m2*MBhat*Ycut^5 - 
          9*m2^2*MBhat*Ycut^5 - 3*m2^3*MBhat*Ycut^5 + 9*MBhat^2*Ycut^5 - 
          2*m2*MBhat^2*Ycut^5 - 5*m2^2*MBhat^2*Ycut^5 - 3*Ycut^6 - 
          m2*Ycut^6 + 4*m2^2*Ycut^6 - 23*MBhat*Ycut^6 + 12*m2*MBhat*Ycut^6 + 
          3*m2^2*MBhat*Ycut^6 + 7*MBhat^2*Ycut^6 + 5*m2*MBhat^2*Ycut^6 + 
          13*Ycut^7 - 4*m2*Ycut^7 + 5*MBhat*Ycut^7 - 3*m2*MBhat*Ycut^7 - 
          5*MBhat^2*Ycut^7 - 6*Ycut^8 + 3*MBhat*Ycut^8))/(24*(-1 + Ycut)^4) - 
       (3*m2*(-8 - 14*m2 + 2*m2^2 + 12*MBhat + 8*m2*MBhat - 4*MBhat^2 + 
          m2*MBhat^2)*Log[m2])/2 + (3*m2*(-8 - 14*m2 + 2*m2^2 + 12*MBhat + 
          8*m2*MBhat - 4*MBhat^2 + m2*MBhat^2)*Log[1 - Ycut])/2) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(-30 - 612*m2 - 132*m2^2 + 60*m2^3 - 6*m2^4 + 
          72*MBhat + 684*m2*MBhat - 84*m2^2*MBhat + 60*m2^3*MBhat - 
          12*m2^4*MBhat - 39*MBhat^2 - 135*m2*MBhat^2 + 81*m2^2*MBhat^2 - 
          15*m2^3*MBhat^2 + 90*Ycut + 2094*m2*Ycut + 546*m2^2*Ycut - 
          234*m2^3*Ycut + 24*m2^4*Ycut - 216*MBhat*Ycut - 
          2412*m2*MBhat*Ycut + 288*m2^2*MBhat*Ycut - 228*m2^3*MBhat*Ycut + 
          48*m2^4*MBhat*Ycut + 117*MBhat^2*Ycut + 510*m2*MBhat^2*Ycut - 
          309*m2^2*MBhat^2*Ycut + 60*m2^3*MBhat^2*Ycut - 90*Ycut^2 - 
          2496*m2*Ycut^2 - 834*m2^2*Ycut^2 + 336*m2^3*Ycut^2 - 
          36*m2^4*Ycut^2 + 216*MBhat*Ycut^2 + 2988*m2*MBhat*Ycut^2 - 
          324*m2^2*MBhat*Ycut^2 + 312*m2^3*MBhat*Ycut^2 - 
          72*m2^4*MBhat*Ycut^2 - 117*MBhat^2*Ycut^2 - 687*m2*MBhat^2*Ycut^2 + 
          426*m2^2*MBhat^2*Ycut^2 - 90*m2^3*MBhat^2*Ycut^2 + 30*Ycut^3 + 
          1110*m2*Ycut^3 + 540*m2^2*Ycut^3 - 204*m2^3*Ycut^3 + 
          24*m2^4*Ycut^3 - 82*MBhat*Ycut^3 - 1434*m2*MBhat*Ycut^3 + 
          186*m2^2*MBhat*Ycut^3 - 218*m2^3*MBhat*Ycut^3 + 
          48*m2^4*MBhat*Ycut^3 + 47*MBhat^2*Ycut^3 + 392*m2*MBhat^2*Ycut^3 - 
          274*m2^2*MBhat^2*Ycut^3 + 60*m2^3*MBhat^2*Ycut^3 + 5*Ycut^4 - 
          57*m2*Ycut^4 - 147*m2^2*Ycut^4 + 61*m2^3*Ycut^4 - 6*m2^4*Ycut^4 + 
          16*MBhat*Ycut^4 + 138*m2*MBhat*Ycut^4 - 60*m2^2*MBhat*Ycut^4 + 
          62*m2^3*MBhat*Ycut^4 - 12*m2^4*MBhat*Ycut^4 - 19*MBhat^2*Ycut^4 - 
          83*m2*MBhat^2*Ycut^4 + 81*m2^2*MBhat^2*Ycut^4 - 
          15*m2^3*MBhat^2*Ycut^4 - 9*Ycut^5 - 34*m2*Ycut^5 + 23*m2^2*Ycut^5 - 
          4*m2^3*Ycut^5 + 9*MBhat*Ycut^5 + 27*m2*MBhat*Ycut^5 - 
          9*m2^2*MBhat*Ycut^5 - 3*m2^3*MBhat*Ycut^5 + 9*MBhat^2*Ycut^5 - 
          2*m2*MBhat^2*Ycut^5 - 5*m2^2*MBhat^2*Ycut^5 - 3*Ycut^6 - 
          m2*Ycut^6 + 4*m2^2*Ycut^6 - 23*MBhat*Ycut^6 + 12*m2*MBhat*Ycut^6 + 
          3*m2^2*MBhat*Ycut^6 + 7*MBhat^2*Ycut^6 + 5*m2*MBhat^2*Ycut^6 + 
          13*Ycut^7 - 4*m2*Ycut^7 + 5*MBhat*Ycut^7 - 3*m2*MBhat*Ycut^7 - 
          5*MBhat^2*Ycut^7 - 6*Ycut^8 + 3*MBhat*Ycut^8))/(24*(-1 + Ycut)^4) - 
       (3*m2*(-8 - 14*m2 + 2*m2^2 + 12*MBhat + 8*m2*MBhat - 4*MBhat^2 + 
          m2*MBhat^2)*Log[m2])/2 + (3*m2*(-8 - 14*m2 + 2*m2^2 + 12*MBhat + 
          8*m2*MBhat - 4*MBhat^2 + m2*MBhat^2)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(15 + 22*m2 + 22*m2^2 + 70*m2^3 - 9*m2^4 - 
          36*MBhat + 32*m2*MBhat - 176*m2^2*MBhat + 112*m2^3*MBhat - 
          28*m2^4*MBhat + 18*MBhat^2 - 30*m2*MBhat^2 + 114*m2^2*MBhat^2 - 
          30*m2^3*MBhat^2 - 15*Ycut - 55*m2*Ycut + 3*m2^2*Ycut - 
          131*m2^3*Ycut + 18*m2^4*Ycut + 36*MBhat*Ycut - 20*m2*MBhat*Ycut + 
          268*m2^2*MBhat*Ycut - 196*m2^3*MBhat*Ycut + 56*m2^4*MBhat*Ycut - 
          18*MBhat^2*Ycut + 48*m2*MBhat^2*Ycut - 198*m2^2*MBhat^2*Ycut + 
          60*m2^3*MBhat^2*Ycut + 24*m2*Ycut^2 - 27*m2^2*Ycut^2 + 
          52*m2^3*Ycut^2 - 9*m2^4*Ycut^2 - 24*m2*MBhat*Ycut^2 - 
          36*m2^2*MBhat*Ycut^2 + 56*m2^3*MBhat*Ycut^2 - 
          28*m2^4*MBhat*Ycut^2 + 54*m2^2*MBhat^2*Ycut^2 - 
          30*m2^3*MBhat^2*Ycut^2 + 8*m2*Ycut^3 - 7*m2^2*Ycut^3 + 
          9*m2^3*Ycut^3 + 72*MBhat*Ycut^3 - 8*m2*MBhat*Ycut^3 - 
          100*m2^2*MBhat*Ycut^3 + 28*m2^3*MBhat*Ycut^3 - 48*MBhat^2*Ycut^3 - 
          48*m2*MBhat^2*Ycut^3 + 30*m2^2*MBhat^2*Ycut^3 - 39*Ycut^4 + 
          22*m2*Ycut^4 + 21*m2^2*Ycut^4 - 96*MBhat*Ycut^4 + 
          32*m2*MBhat*Ycut^4 + 32*m2^2*MBhat*Ycut^4 + 78*MBhat^2*Ycut^4 + 
          30*m2*MBhat^2*Ycut^4 + 63*Ycut^5 - 21*m2*Ycut^5 + 12*MBhat*Ycut^5 - 
          12*m2*MBhat*Ycut^5 - 30*MBhat^2*Ycut^5 - 24*Ycut^6 + 
          12*MBhat*Ycut^6))/(12*(-1 + Ycut)^2) - 
       m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 4*m2*MBhat + 6*m2*MBhat^2)*Log[m2] + 
       m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 4*m2*MBhat + 6*m2*MBhat^2)*
        Log[1 - Ycut]) + c[VL]^2*
      (((-1 + m2 + Ycut)*(15 + 22*m2 + 22*m2^2 + 70*m2^3 - 9*m2^4 - 
          36*MBhat + 32*m2*MBhat - 176*m2^2*MBhat + 112*m2^3*MBhat - 
          28*m2^4*MBhat + 18*MBhat^2 - 30*m2*MBhat^2 + 114*m2^2*MBhat^2 - 
          30*m2^3*MBhat^2 - 45*Ycut - 99*m2*Ycut - 41*m2^2*Ycut - 
          271*m2^3*Ycut + 36*m2^4*Ycut + 108*MBhat*Ycut - 84*m2*MBhat*Ycut + 
          620*m2^2*MBhat*Ycut - 420*m2^3*MBhat*Ycut + 112*m2^4*MBhat*Ycut - 
          54*MBhat^2*Ycut + 108*m2*MBhat^2*Ycut - 426*m2^2*MBhat^2*Ycut + 
          120*m2^3*MBhat^2*Ycut + 45*Ycut^2 + 156*m2*Ycut^2 - 
          11*m2^2*Ycut^2 + 384*m2^3*Ycut^2 - 54*m2^4*Ycut^2 - 
          108*MBhat*Ycut^2 + 48*m2*MBhat*Ycut^2 - 748*m2^2*MBhat*Ycut^2 + 
          560*m2^3*MBhat*Ycut^2 - 168*m2^4*MBhat*Ycut^2 + 54*MBhat^2*Ycut^2 - 
          126*m2*MBhat^2*Ycut^2 + 564*m2^2*MBhat^2*Ycut^2 - 
          180*m2^3*MBhat^2*Ycut^2 - 15*Ycut^3 - 95*m2*Ycut^3 + 
          50*m2^2*Ycut^3 - 226*m2^3*Ycut^3 + 36*m2^4*Ycut^3 + 
          16*MBhat*Ycut^3 + 104*m2*MBhat*Ycut^3 + 348*m2^2*MBhat*Ycut^3 - 
          380*m2^3*MBhat*Ycut^3 + 112*m2^4*MBhat*Ycut^3 - 2*MBhat^2*Ycut^3 + 
          16*m2*MBhat^2*Ycut^3 - 356*m2^2*MBhat^2*Ycut^3 + 
          120*m2^3*MBhat^2*Ycut^3 + 7*Ycut^4 - 12*m2*Ycut^4 - 
          46*m2^2*Ycut^4 + 84*m2^3*Ycut^4 - 9*m2^4*Ycut^4 + 48*MBhat*Ycut^4 - 
          212*m2*MBhat*Ycut^4 + 44*m2^2*MBhat*Ycut^4 + 100*m2^3*MBhat*
           Ycut^4 - 28*m2^4*MBhat*Ycut^4 - 38*MBhat^2*Ycut^4 + 
          74*m2*MBhat^2*Ycut^4 + 114*m2^2*MBhat^2*Ycut^4 - 
          30*m2^3*MBhat^2*Ycut^4 - 17*Ycut^5 + 47*m2*Ycut^5 + 
          25*m2^2*Ycut^5 - 11*m2^3*Ycut^5 - 26*MBhat*Ycut^5 + 
          146*m2*MBhat*Ycut^5 - 110*m2^2*MBhat*Ycut^5 - 2*m2^3*MBhat*Ycut^5 + 
          18*MBhat^2*Ycut^5 - 52*m2*MBhat^2*Ycut^5 - 10*m2^2*MBhat^2*Ycut^5 + 
          9*Ycut^6 - 18*m2*Ycut^6 + m2^2*Ycut^6 - 10*MBhat*Ycut^6 - 
          32*m2*MBhat*Ycut^6 + 22*m2^2*MBhat*Ycut^6 + 14*MBhat^2*Ycut^6 + 
          10*m2*MBhat^2*Ycut^6 + 5*Ycut^7 - m2*Ycut^7 + 6*MBhat*Ycut^7 - 
          2*m2*MBhat*Ycut^7 - 10*MBhat^2*Ycut^7 - 4*Ycut^8 + 2*MBhat*Ycut^8))/
        (12*(-1 + Ycut)^4) - m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 4*m2*MBhat + 
         6*m2*MBhat^2)*Log[m2] + m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 
         4*m2*MBhat + 6*m2*MBhat^2)*Log[1 - Ycut]) + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(36 + 280*m2 + 88*m2^2 + 88*m2^3 - 12*m2^4 - 
          120*MBhat - 244*m2*MBhat - 180*m2^2*MBhat + 156*m2^3*MBhat - 
          44*m2^4*MBhat + 75*MBhat^2 + 75*m2*MBhat^2 + 147*m2^2*MBhat^2 - 
          45*m2^3*MBhat^2 - 108*Ycut - 996*m2*Ycut - 284*m2^2*Ycut - 
          340*m2^3*Ycut + 48*m2^4*Ycut + 360*MBhat*Ycut + 948*m2*MBhat*Ycut + 
          608*m2^2*MBhat*Ycut - 580*m2^3*MBhat*Ycut + 176*m2^4*MBhat*Ycut - 
          225*MBhat^2*Ycut - 294*m2*MBhat^2*Ycut - 543*m2^2*MBhat^2*Ycut + 
          180*m2^3*MBhat^2*Ycut + 108*Ycut^2 + 1248*m2*Ycut^2 + 
          316*m2^2*Ycut^2 + 480*m2^3*Ycut^2 - 72*m2^4*Ycut^2 - 
          360*MBhat*Ycut^2 - 1332*m2*MBhat*Ycut^2 - 676*m2^2*MBhat*Ycut^2 + 
          760*m2^3*MBhat*Ycut^2 - 264*m2^4*MBhat*Ycut^2 + 
          225*MBhat^2*Ycut^2 + 435*m2*MBhat^2*Ycut^2 + 702*m2^2*MBhat^2*
           Ycut^2 - 270*m2^3*MBhat^2*Ycut^2 - 36*Ycut^3 - 596*m2*Ycut^3 - 
          136*m2^2*Ycut^3 - 280*m2^3*Ycut^3 + 48*m2^4*Ycut^3 + 
          246*MBhat*Ycut^3 + 726*m2*MBhat*Ycut^3 + 162*m2^2*MBhat*Ycut^3 - 
          410*m2^3*MBhat*Ycut^3 + 176*m2^4*MBhat*Ycut^3 - 
          163*MBhat^2*Ycut^3 - 328*m2*MBhat^2*Ycut^3 - 358*m2^2*MBhat^2*
           Ycut^3 + 180*m2^3*MBhat^2*Ycut^3 - 45*Ycut^4 + 67*m2*Ycut^4 + 
          21*m2^2*Ycut^4 + 65*m2^3*Ycut^4 - 12*m2^4*Ycut^4 - 
          408*MBhat*Ycut^4 + 18*m2*MBhat*Ycut^4 + 260*m2^2*MBhat*Ycut^4 + 
          30*m2^3*MBhat*Ycut^4 - 44*m2^4*MBhat*Ycut^4 + 299*MBhat^2*Ycut^4 + 
          187*m2*MBhat^2*Ycut^4 + 27*m2^2*MBhat^2*Ycut^4 - 
          45*m2^3*MBhat^2*Ycut^4 + 157*Ycut^5 - 56*m2*Ycut^5 - 
          23*m2^2*Ycut^5 + 2*m2^3*Ycut^5 + 457*MBhat*Ycut^5 - 
          173*m2*MBhat*Ycut^5 - 225*m2^2*MBhat*Ycut^5 + 
          29*m2^3*MBhat*Ycut^5 - 369*MBhat^2*Ycut^5 - 110*m2*MBhat^2*Ycut^5 + 
          25*m2^2*MBhat^2*Ycut^5 - 201*Ycut^6 + 71*m2*Ycut^6 + 
          18*m2^2*Ycut^6 - 183*MBhat*Ycut^6 + 68*m2*MBhat*Ycut^6 + 
          51*m2^2*MBhat*Ycut^6 + 193*MBhat^2*Ycut^6 + 35*m2*MBhat^2*Ycut^6 + 
          111*Ycut^7 - 18*m2*Ycut^7 - 3*MBhat*Ycut^7 - 11*m2*MBhat*Ycut^7 - 
          35*MBhat^2*Ycut^7 - 22*Ycut^8 + 11*MBhat*Ycut^8))/
        (3*(-1 + Ycut)^4) - 8*m2*(16 + 12*m2 + 12*m2^2 - 28*MBhat - 
         8*m2*MBhat + 12*MBhat^2 + 9*m2*MBhat^2)*Log[m2] + 
       8*m2*(16 + 12*m2 + 12*m2^2 - 28*MBhat - 8*m2*MBhat + 12*MBhat^2 + 
         9*m2*MBhat^2)*Log[1 - Ycut]) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-59 + 137*m2 - 7*m2^2 - 11*m2^3 + 
          76*MBhat - 196*m2*MBhat + 92*m2^2*MBhat - 20*m2^3*MBhat - 
          26*MBhat^2 + 28*m2*MBhat^2 - 26*m2^2*MBhat^2 + 142*Ycut - 
          321*m2*Ycut - 4*m2^2*Ycut + 33*m2^3*Ycut - 200*MBhat*Ycut + 
          516*m2*MBhat*Ycut - 256*m2^2*MBhat*Ycut + 60*m2^3*MBhat*Ycut + 
          76*MBhat^2*Ycut - 94*m2*MBhat^2*Ycut + 78*m2^2*MBhat^2*Ycut - 
          95*Ycut^2 + 202*m2*Ycut^2 + 36*m2^2*Ycut^2 - 33*m2^3*Ycut^2 + 
          148*MBhat*Ycut^2 - 392*m2*MBhat*Ycut^2 + 216*m2^2*MBhat*Ycut^2 - 
          60*m2^3*MBhat*Ycut^2 - 62*MBhat^2*Ycut^2 + 96*m2*MBhat^2*Ycut^2 - 
          78*m2^2*MBhat^2*Ycut^2 + 8*Ycut^3 - 8*m2*Ycut^3 - 26*m2^2*Ycut^3 + 
          11*m2^3*Ycut^3 + 26*MBhat*Ycut^3 + 28*m2*MBhat*Ycut^3 - 
          62*m2^2*MBhat*Ycut^3 + 20*m2^3*MBhat*Ycut^3 - 16*MBhat^2*Ycut^3 - 
          40*m2*MBhat^2*Ycut^3 + 26*m2^2*MBhat^2*Ycut^3 - 13*Ycut^4 - 
          3*m2*Ycut^4 + 13*m2^2*Ycut^4 - 76*MBhat*Ycut^4 + 
          66*m2*MBhat*Ycut^4 - 2*m2^2*MBhat*Ycut^4 + 44*MBhat^2*Ycut^4 + 
          10*m2*MBhat^2*Ycut^4 + 26*Ycut^5 - 7*m2*Ycut^5 + 26*MBhat*Ycut^5 - 
          22*m2*MBhat*Ycut^5 - 16*MBhat^2*Ycut^5 - 9*Ycut^6))/
        (3*(-1 + Ycut)^3) - 4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 
         8*m2*MBhat + 2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2] + 
       4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 8*m2*MBhat + 
         2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[1 - Ycut]) + 
     c[SL]*((Ycut^3*(-1 + m2 + Ycut)^2*(14*MBhat + 36*m2*MBhat - 
          50*m2^2*MBhat - 8*MBhat^2 - 40*m2*MBhat^2 - 7*Ycut - 18*m2*Ycut + 
          25*m2^2*Ycut - 42*MBhat*Ycut - 24*m2*MBhat*Ycut + 
          50*m2^2*MBhat*Ycut + 26*MBhat^2*Ycut + 60*m2*MBhat^2*Ycut + 
          24*Ycut^2 + 18*m2*Ycut^2 - 10*m2^2*Ycut^2 + 37*MBhat*Ycut^2 - 
          22*m2*MBhat*Ycut^2 - 15*m2^2*MBhat*Ycut^2 - 28*MBhat^2*Ycut^2 - 
          20*m2*MBhat^2*Ycut^2 - 27*Ycut^3 - 4*MBhat*Ycut^3 + 
          10*m2*MBhat*Ycut^3 + 10*MBhat^2*Ycut^3 + 10*Ycut^4 - 
          5*MBhat*Ycut^4)*c[T])/(6*(-1 + Ycut)^4) + 
       c[SR]*(-1/2*(Sqrt[m2]*(-1 + m2 + Ycut)*(85 + 217*m2 + m2^2 - 3*m2^3 - 
             148*MBhat - 116*m2*MBhat + 28*m2^2*MBhat - 4*m2^3*MBhat + 
             66*MBhat^2 + 12*m2*MBhat^2 - 6*m2^2*MBhat^2 - 194*Ycut - 
             553*m2*Ycut - 12*m2^2*Ycut + 9*m2^3*Ycut + 344*MBhat*Ycut + 
             324*m2*MBhat*Ycut - 80*m2^2*MBhat*Ycut + 12*m2^3*MBhat*Ycut - 
             156*MBhat^2*Ycut - 42*m2*MBhat^2*Ycut + 18*m2^2*MBhat^2*Ycut + 
             121*Ycut^2 + 414*m2*Ycut^2 + 24*m2^2*Ycut^2 - 9*m2^3*Ycut^2 - 
             220*MBhat*Ycut^2 - 280*m2*MBhat*Ycut^2 + 72*m2^2*MBhat*Ycut^2 - 
             12*m2^3*MBhat*Ycut^2 + 102*MBhat^2*Ycut^2 + 48*m2*MBhat^2*
              Ycut^2 - 18*m2^2*MBhat^2*Ycut^2 - 8*Ycut^3 - 56*m2*Ycut^3 - 
             14*m2^2*Ycut^3 + 3*m2^3*Ycut^3 + 6*MBhat*Ycut^3 + 
             76*m2*MBhat*Ycut^3 - 26*m2^2*MBhat*Ycut^3 + 4*m2^3*MBhat*
              Ycut^3 - 24*m2*MBhat^2*Ycut^3 + 6*m2^2*MBhat^2*Ycut^3 + 
             3*Ycut^4 - 23*m2*Ycut^4 + 5*m2^2*Ycut^4 + 12*MBhat*Ycut^4 - 
             2*m2*MBhat*Ycut^4 + 2*m2^2*MBhat*Ycut^4 - 12*MBhat^2*Ycut^4 + 
             6*m2*MBhat^2*Ycut^4 - 6*Ycut^5 + m2*Ycut^5 + 6*MBhat*Ycut^5 - 
             2*m2*MBhat*Ycut^5 - Ycut^6))/(-1 + Ycut)^3 + 
         6*Sqrt[m2]*(-2 - 15*m2 - 9*m2^2 + m2^3 + 4*MBhat + 16*m2*MBhat - 
           2*MBhat^2 - 5*m2*MBhat^2 + m2^2*MBhat^2)*Log[m2] - 
         6*Sqrt[m2]*(-2 - 15*m2 - 9*m2^2 + m2^3 + 4*MBhat + 16*m2*MBhat - 
           2*MBhat^2 - 5*m2*MBhat^2 + m2^2*MBhat^2)*Log[1 - Ycut])) + 
     c[VL]*(((-1 + m2 + Ycut)*(15 + 22*m2 + 22*m2^2 + 70*m2^3 - 9*m2^4 - 
          36*MBhat + 32*m2*MBhat - 176*m2^2*MBhat + 112*m2^3*MBhat - 
          28*m2^4*MBhat + 18*MBhat^2 - 30*m2*MBhat^2 + 114*m2^2*MBhat^2 - 
          30*m2^3*MBhat^2 - 45*Ycut - 99*m2*Ycut - 41*m2^2*Ycut - 
          271*m2^3*Ycut + 36*m2^4*Ycut + 108*MBhat*Ycut - 84*m2*MBhat*Ycut + 
          620*m2^2*MBhat*Ycut - 420*m2^3*MBhat*Ycut + 112*m2^4*MBhat*Ycut - 
          54*MBhat^2*Ycut + 108*m2*MBhat^2*Ycut - 426*m2^2*MBhat^2*Ycut + 
          120*m2^3*MBhat^2*Ycut + 45*Ycut^2 + 156*m2*Ycut^2 - 
          11*m2^2*Ycut^2 + 384*m2^3*Ycut^2 - 54*m2^4*Ycut^2 - 
          108*MBhat*Ycut^2 + 48*m2*MBhat*Ycut^2 - 748*m2^2*MBhat*Ycut^2 + 
          560*m2^3*MBhat*Ycut^2 - 168*m2^4*MBhat*Ycut^2 + 54*MBhat^2*Ycut^2 - 
          126*m2*MBhat^2*Ycut^2 + 564*m2^2*MBhat^2*Ycut^2 - 
          180*m2^3*MBhat^2*Ycut^2 - 15*Ycut^3 - 95*m2*Ycut^3 + 
          50*m2^2*Ycut^3 - 226*m2^3*Ycut^3 + 36*m2^4*Ycut^3 + 
          16*MBhat*Ycut^3 + 104*m2*MBhat*Ycut^3 + 348*m2^2*MBhat*Ycut^3 - 
          380*m2^3*MBhat*Ycut^3 + 112*m2^4*MBhat*Ycut^3 - 2*MBhat^2*Ycut^3 + 
          16*m2*MBhat^2*Ycut^3 - 356*m2^2*MBhat^2*Ycut^3 + 
          120*m2^3*MBhat^2*Ycut^3 + 7*Ycut^4 - 12*m2*Ycut^4 - 
          46*m2^2*Ycut^4 + 84*m2^3*Ycut^4 - 9*m2^4*Ycut^4 + 48*MBhat*Ycut^4 - 
          212*m2*MBhat*Ycut^4 + 44*m2^2*MBhat*Ycut^4 + 100*m2^3*MBhat*
           Ycut^4 - 28*m2^4*MBhat*Ycut^4 - 38*MBhat^2*Ycut^4 + 
          74*m2*MBhat^2*Ycut^4 + 114*m2^2*MBhat^2*Ycut^4 - 
          30*m2^3*MBhat^2*Ycut^4 - 17*Ycut^5 + 47*m2*Ycut^5 + 
          25*m2^2*Ycut^5 - 11*m2^3*Ycut^5 - 26*MBhat*Ycut^5 + 
          146*m2*MBhat*Ycut^5 - 110*m2^2*MBhat*Ycut^5 - 2*m2^3*MBhat*Ycut^5 + 
          18*MBhat^2*Ycut^5 - 52*m2*MBhat^2*Ycut^5 - 10*m2^2*MBhat^2*Ycut^5 + 
          9*Ycut^6 - 18*m2*Ycut^6 + m2^2*Ycut^6 - 10*MBhat*Ycut^6 - 
          32*m2*MBhat*Ycut^6 + 22*m2^2*MBhat*Ycut^6 + 14*MBhat^2*Ycut^6 + 
          10*m2*MBhat^2*Ycut^6 + 5*Ycut^7 - m2*Ycut^7 + 6*MBhat*Ycut^7 - 
          2*m2*MBhat*Ycut^7 - 10*MBhat^2*Ycut^7 - 4*Ycut^8 + 2*MBhat*Ycut^8))/
        (6*(-1 + Ycut)^4) - 2*m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 4*m2*MBhat + 
         6*m2*MBhat^2)*Log[m2] + 2*m2*(4 - 3*m2 + 9*m2^2 - 4*MBhat - 
         4*m2*MBhat + 6*m2*MBhat^2)*Log[1 - Ycut] + 
       c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-59 + 137*m2 - 7*m2^2 - 11*m2^3 + 
            76*MBhat - 196*m2*MBhat + 92*m2^2*MBhat - 20*m2^3*MBhat - 
            26*MBhat^2 + 28*m2*MBhat^2 - 26*m2^2*MBhat^2 + 142*Ycut - 
            321*m2*Ycut - 4*m2^2*Ycut + 33*m2^3*Ycut - 200*MBhat*Ycut + 
            516*m2*MBhat*Ycut - 256*m2^2*MBhat*Ycut + 60*m2^3*MBhat*Ycut + 
            76*MBhat^2*Ycut - 94*m2*MBhat^2*Ycut + 78*m2^2*MBhat^2*Ycut - 
            95*Ycut^2 + 202*m2*Ycut^2 + 36*m2^2*Ycut^2 - 33*m2^3*Ycut^2 + 
            148*MBhat*Ycut^2 - 392*m2*MBhat*Ycut^2 + 216*m2^2*MBhat*Ycut^2 - 
            60*m2^3*MBhat*Ycut^2 - 62*MBhat^2*Ycut^2 + 96*m2*MBhat^2*Ycut^2 - 
            78*m2^2*MBhat^2*Ycut^2 + 8*Ycut^3 - 8*m2*Ycut^3 - 
            26*m2^2*Ycut^3 + 11*m2^3*Ycut^3 + 26*MBhat*Ycut^3 + 
            28*m2*MBhat*Ycut^3 - 62*m2^2*MBhat*Ycut^3 + 20*m2^3*MBhat*
             Ycut^3 - 16*MBhat^2*Ycut^3 - 40*m2*MBhat^2*Ycut^3 + 
            26*m2^2*MBhat^2*Ycut^3 - 13*Ycut^4 - 3*m2*Ycut^4 + 
            13*m2^2*Ycut^4 - 76*MBhat*Ycut^4 + 66*m2*MBhat*Ycut^4 - 
            2*m2^2*MBhat*Ycut^4 + 44*MBhat^2*Ycut^4 + 10*m2*MBhat^2*Ycut^4 + 
            26*Ycut^5 - 7*m2*Ycut^5 + 26*MBhat*Ycut^5 - 22*m2*MBhat*Ycut^5 - 
            16*MBhat^2*Ycut^5 - 9*Ycut^6))/(3*(-1 + Ycut)^3) - 
         4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 8*m2*MBhat + 
           2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2] + 
         4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 8*m2*MBhat + 
           2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[1 - Ycut]))))/
  mb^2 + 
 (rhoD*(-1/180*(-3645 + 9155*m2 - 6480*m2^2 + 720*m2^3 + 205*m2^4 + 45*m2^5 + 
        5424*MBhat - 10640*m2*MBhat + 6720*m2^2*MBhat - 960*m2^3*MBhat - 
        880*m2^4*MBhat + 336*m2^5*MBhat - 2310*MBhat^2 + 2640*m2*MBhat^2 - 
        720*m2^2*MBhat^2 + 240*m2^3*MBhat^2 + 150*m2^4*MBhat^2 + 23310*Ycut - 
        54450*m2*Ycut + 34740*m2^2*Ycut - 2700*m2^3*Ycut - 1230*m2^4*Ycut - 
        270*m2^5*Ycut - 35424*MBhat*Ycut + 64800*m2*MBhat*Ycut - 
        40320*m2^2*MBhat*Ycut + 5760*m2^3*MBhat*Ycut + 5280*m2^4*MBhat*Ycut - 
        2016*m2^5*MBhat*Ycut + 15300*MBhat^2*Ycut - 15840*m2*MBhat^2*Ycut + 
        5400*m2^2*MBhat^2*Ycut - 1440*m2^3*MBhat^2*Ycut - 
        900*m2^4*MBhat^2*Ycut - 62595*Ycut^2 + 134685*m2*Ycut^2 - 
        74430*m2^2*Ycut^2 + 1890*m2^3*Ycut^2 + 3075*m2^4*Ycut^2 + 
        675*m2^5*Ycut^2 + 97200*MBhat*Ycut^2 - 164880*m2*MBhat*Ycut^2 + 
        100800*m2^2*MBhat*Ycut^2 - 14400*m2^3*MBhat*Ycut^2 - 
        13200*m2^4*MBhat*Ycut^2 + 5040*m2^5*MBhat*Ycut^2 - 
        42570*MBhat^2*Ycut^2 + 39600*m2*MBhat^2*Ycut^2 - 
        16740*m2^2*MBhat^2*Ycut^2 + 3600*m2^3*MBhat^2*Ycut^2 + 
        2250*m2^4*MBhat^2*Ycut^2 + 90660*Ycut^3 - 177180*m2*Ycut^3 + 
        78540*m2^2*Ycut^3 + 5580*m2^3*Ycut^3 - 4100*m2^4*Ycut^3 - 
        900*m2^5*Ycut^3 - 142620*MBhat*Ycut^3 + 222480*m2*MBhat*Ycut^3 - 
        132120*m2^2*MBhat*Ycut^3 + 15600*m2^3*MBhat*Ycut^3 + 
        19700*m2^4*MBhat*Ycut^3 - 6720*m2^5*MBhat*Ycut^3 + 
        63000*MBhat^2*Ycut^3 - 51840*m2*MBhat^2*Ycut^3 + 
        26760*m2^2*MBhat^2*Ycut^3 - 3840*m2^3*MBhat^2*Ycut^3 - 
        3000*m2^4*MBhat^2*Ycut^3 - 75720*Ycut^4 + 131310*m2*Ycut^4 - 
        39330*m2^2*Ycut^4 - 10410*m2^3*Ycut^4 + 2025*m2^4*Ycut^4 + 
        675*m2^5*Ycut^4 + 114840*MBhat*Ycut^4 - 162960*m2*MBhat*Ycut^4 + 
        94080*m2^2*MBhat*Ycut^4 - 7440*m2^3*MBhat*Ycut^4 - 
        16200*m2^4*MBhat*Ycut^4 + 5040*m2^5*MBhat*Ycut^4 - 
        49800*MBhat^2*Ycut^4 + 35280*m2*MBhat^2*Ycut^4 - 
        23880*m2^2*MBhat^2*Ycut^4 + 2040*m2^3*MBhat^2*Ycut^4 + 
        2250*m2^4*MBhat^2*Ycut^4 + 37260*Ycut^5 - 54360*m2*Ycut^5 + 
        5580*m2^2*Ycut^5 + 7260*m2^3*Ycut^5 - 690*m2^4*Ycut^5 - 
        270*m2^5*Ycut^5 - 40638*MBhat*Ycut^5 + 52080*m2*MBhat*Ycut^5 - 
        32460*m2^2*MBhat*Ycut^5 + 960*m2^3*MBhat*Ycut^5 + 
        7530*m2^4*MBhat*Ycut^5 - 2016*m2^5*MBhat*Ycut^5 + 
        14040*MBhat^2*Ycut^5 - 8160*m2*MBhat^2*Ycut^5 + 
        12240*m2^2*MBhat^2*Ycut^5 - 720*m2^3*MBhat^2*Ycut^5 - 
        900*m2^4*MBhat^2*Ycut^5 - 13560*Ycut^6 + 14130*m2*Ycut^6 + 
        1830*m2^2*Ycut^6 - 2970*m2^3*Ycut^6 + 115*m2^4*Ycut^6 + 
        45*m2^5*Ycut^6 - 7332*MBhat*Ycut^6 + 7440*m2*MBhat*Ycut^6 + 
        1320*m2^2*MBhat*Ycut^6 + 720*m2^3*MBhat*Ycut^6 - 
        1780*m2^4*MBhat*Ycut^6 + 336*m2^5*MBhat*Ycut^6 + 
        9000*MBhat^2*Ycut^6 - 4080*m2*MBhat^2*Ycut^6 - 
        3540*m2^2*MBhat^2*Ycut^6 + 120*m2^3*MBhat^2*Ycut^6 + 
        150*m2^4*MBhat^2*Ycut^6 + 7620*Ycut^7 - 4980*m2*Ycut^7 - 
        540*m2^2*Ycut^7 + 660*m2^3*Ycut^7 + 12150*MBhat*Ycut^7 - 
        11280*m2*MBhat*Ycut^7 + 2760*m2^2*MBhat*Ycut^7 - 
        240*m2^3*MBhat*Ycut^7 + 150*m2^4*MBhat*Ycut^7 - 9480*MBhat^2*Ycut^7 + 
        2880*m2*MBhat^2*Ycut^7 + 480*m2^2*MBhat^2*Ycut^7 - 4635*Ycut^8 + 
        1935*m2*Ycut^8 + 90*m2^2*Ycut^8 - 30*m2^3*Ycut^8 - 
        4080*MBhat*Ycut^8 + 3360*m2*MBhat*Ycut^8 - 840*m2^2*MBhat*Ycut^8 + 
        3150*MBhat^2*Ycut^8 - 480*m2*MBhat^2*Ycut^8 + 1470*Ycut^9 - 
        230*m2*Ycut^9 + 510*MBhat*Ycut^9 - 400*m2*MBhat*Ycut^9 + 
        60*m2^2*MBhat*Ycut^9 - 300*MBhat^2*Ycut^9 - 165*Ycut^10 - 
        15*m2*Ycut^10 - 36*MBhat*Ycut^10 - 30*MBhat^2*Ycut^10 + 
        6*MBhat*Ycut^11)/(-1 + Ycut)^6 + 
     ((24 + 8*m2 - 69*m2^2 + 27*m2^3 - 48*MBhat + 16*m2*MBhat + 24*MBhat^2 + 
        18*m2^2*MBhat^2)*Log[m2])/3 + 
     ((-24 - 8*m2 + 69*m2^2 - 27*m2^3 + 48*MBhat - 16*m2*MBhat - 24*MBhat^2 - 
        18*m2^2*MBhat^2)*Log[1 - Ycut])/3 + 
     c[VR]^2*(-1/180*(-3645 + 9155*m2 - 6480*m2^2 + 720*m2^3 + 205*m2^4 + 
          45*m2^5 + 5424*MBhat - 10640*m2*MBhat + 6720*m2^2*MBhat - 
          960*m2^3*MBhat - 880*m2^4*MBhat + 336*m2^5*MBhat - 2310*MBhat^2 + 
          2640*m2*MBhat^2 - 720*m2^2*MBhat^2 + 240*m2^3*MBhat^2 + 
          150*m2^4*MBhat^2 + 16020*Ycut - 36140*m2*Ycut + 21780*m2^2*Ycut - 
          1260*m2^3*Ycut - 820*m2^4*Ycut - 180*m2^5*Ycut - 24576*MBhat*Ycut + 
          43520*m2*MBhat*Ycut - 26880*m2^2*MBhat*Ycut + 3840*m2^3*MBhat*
           Ycut + 3520*m2^4*MBhat*Ycut - 1344*m2^5*MBhat*Ycut + 
          10680*MBhat^2*Ycut - 10560*m2*MBhat^2*Ycut + 3960*m2^2*MBhat^2*
           Ycut - 960*m2^3*MBhat^2*Ycut - 600*m2^4*MBhat^2*Ycut - 
          26910*Ycut^2 + 53250*m2*Ycut^2 - 24390*m2^2*Ycut^2 - 
          1350*m2^3*Ycut^2 + 1230*m2^4*Ycut^2 + 270*m2^5*Ycut^2 + 
          42624*MBhat*Ycut^2 - 67200*m2*MBhat*Ycut^2 + 40320*m2^2*MBhat*
           Ycut^2 - 5760*m2^3*MBhat*Ycut^2 - 5280*m2^4*MBhat*Ycut^2 + 
          2016*m2^5*MBhat*Ycut^2 - 18900*MBhat^2*Ycut^2 + 
          15840*m2*MBhat^2*Ycut^2 - 8100*m2^2*MBhat^2*Ycut^2 + 
          1440*m2^3*MBhat^2*Ycut^2 + 900*m2^4*MBhat^2*Ycut^2 + 20820*Ycut^3 - 
          34540*m2*Ycut^3 + 7980*m2^2*Ycut^3 + 4140*m2^3*Ycut^3 - 
          820*m2^4*Ycut^3 - 180*m2^5*Ycut^3 - 32496*MBhat*Ycut^3 + 
          45760*m2*MBhat*Ycut^3 - 30000*m2^2*MBhat*Ycut^3 + 
          6240*m2^3*MBhat*Ycut^3 + 3520*m2^4*MBhat*Ycut^3 - 
          1344*m2^5*MBhat*Ycut^3 + 14520*MBhat^2*Ycut^3 - 
          10560*m2*MBhat^2*Ycut^3 + 8520*m2^2*MBhat^2*Ycut^3 - 
          960*m2^3*MBhat^2*Ycut^3 - 600*m2^4*MBhat^2*Ycut^3 - 7320*Ycut^4 + 
          8380*m2*Ycut^4 + 3720*m2^2*Ycut^4 - 3780*m2^3*Ycut^4 + 
          205*m2^4*Ycut^4 + 45*m2^5*Ycut^4 + 5424*MBhat*Ycut^4 - 
          9520*m2*MBhat*Ycut^4 + 12720*m2^2*MBhat*Ycut^4 - 
          4080*m2^3*MBhat*Ycut^4 - 880*m2^4*MBhat*Ycut^4 + 
          336*m2^5*MBhat*Ycut^4 - 1920*MBhat^2*Ycut^4 + 2640*m2*MBhat^2*
           Ycut^4 - 4560*m2^2*MBhat^2*Ycut^4 + 240*m2^3*MBhat^2*Ycut^4 + 
          150*m2^4*MBhat^2*Ycut^4 + 2700*Ycut^5 - 420*m2*Ycut^5 - 
          3480*m2^2*Ycut^5 + 1080*m2^3*Ycut^5 + 7236*MBhat*Ycut^5 - 
          3360*m2*MBhat*Ycut^5 - 3180*m2^2*MBhat*Ycut^5 + 
          1080*m2^3*MBhat*Ycut^5 - 3960*MBhat^2*Ycut^5 + 
          720*m2^2*MBhat^2*Ycut^5 - 3090*Ycut^6 + 230*m2*Ycut^6 + 
          870*m2^2*Ycut^6 + 90*m2^3*Ycut^6 - 4464*MBhat*Ycut^6 + 
          1600*m2*MBhat*Ycut^6 + 240*m2^2*MBhat*Ycut^6 + 
          2100*MBhat^2*Ycut^6 + 180*m2^2*MBhat^2*Ycut^6 + 1740*Ycut^7 + 
          220*m2*Ycut^7 + 936*MBhat*Ycut^7 - 160*m2*MBhat*Ycut^7 + 
          60*m2^2*MBhat*Ycut^7 - 120*MBhat^2*Ycut^7 - 315*Ycut^8 - 
          135*m2*Ycut^8 - 144*MBhat*Ycut^8 - 90*MBhat^2*Ycut^8 + 
          36*MBhat*Ycut^9)/(-1 + Ycut)^4 + 
       ((24 + 8*m2 - 69*m2^2 + 27*m2^3 - 48*MBhat + 16*m2*MBhat + 
          24*MBhat^2 + 18*m2^2*MBhat^2)*Log[m2])/3 + 
       ((-24 - 8*m2 + 69*m2^2 - 27*m2^3 + 48*MBhat - 16*m2*MBhat - 
          24*MBhat^2 - 18*m2^2*MBhat^2)*Log[1 - Ycut])/3) + 
     c[VL]^2*(-1/180*(-3645 + 9155*m2 - 6480*m2^2 + 720*m2^3 + 205*m2^4 + 
          45*m2^5 + 5424*MBhat - 10640*m2*MBhat + 6720*m2^2*MBhat - 
          960*m2^3*MBhat - 880*m2^4*MBhat + 336*m2^5*MBhat - 2310*MBhat^2 + 
          2640*m2*MBhat^2 - 720*m2^2*MBhat^2 + 240*m2^3*MBhat^2 + 
          150*m2^4*MBhat^2 + 23310*Ycut - 54450*m2*Ycut + 34740*m2^2*Ycut - 
          2700*m2^3*Ycut - 1230*m2^4*Ycut - 270*m2^5*Ycut - 
          35424*MBhat*Ycut + 64800*m2*MBhat*Ycut - 40320*m2^2*MBhat*Ycut + 
          5760*m2^3*MBhat*Ycut + 5280*m2^4*MBhat*Ycut - 2016*m2^5*MBhat*
           Ycut + 15300*MBhat^2*Ycut - 15840*m2*MBhat^2*Ycut + 
          5400*m2^2*MBhat^2*Ycut - 1440*m2^3*MBhat^2*Ycut - 
          900*m2^4*MBhat^2*Ycut - 62595*Ycut^2 + 134685*m2*Ycut^2 - 
          74430*m2^2*Ycut^2 + 1890*m2^3*Ycut^2 + 3075*m2^4*Ycut^2 + 
          675*m2^5*Ycut^2 + 97200*MBhat*Ycut^2 - 164880*m2*MBhat*Ycut^2 + 
          100800*m2^2*MBhat*Ycut^2 - 14400*m2^3*MBhat*Ycut^2 - 
          13200*m2^4*MBhat*Ycut^2 + 5040*m2^5*MBhat*Ycut^2 - 
          42570*MBhat^2*Ycut^2 + 39600*m2*MBhat^2*Ycut^2 - 
          16740*m2^2*MBhat^2*Ycut^2 + 3600*m2^3*MBhat^2*Ycut^2 + 
          2250*m2^4*MBhat^2*Ycut^2 + 90660*Ycut^3 - 177180*m2*Ycut^3 + 
          78540*m2^2*Ycut^3 + 5580*m2^3*Ycut^3 - 4100*m2^4*Ycut^3 - 
          900*m2^5*Ycut^3 - 142620*MBhat*Ycut^3 + 222480*m2*MBhat*Ycut^3 - 
          132120*m2^2*MBhat*Ycut^3 + 15600*m2^3*MBhat*Ycut^3 + 
          19700*m2^4*MBhat*Ycut^3 - 6720*m2^5*MBhat*Ycut^3 + 
          63000*MBhat^2*Ycut^3 - 51840*m2*MBhat^2*Ycut^3 + 
          26760*m2^2*MBhat^2*Ycut^3 - 3840*m2^3*MBhat^2*Ycut^3 - 
          3000*m2^4*MBhat^2*Ycut^3 - 75720*Ycut^4 + 131310*m2*Ycut^4 - 
          39330*m2^2*Ycut^4 - 10410*m2^3*Ycut^4 + 2025*m2^4*Ycut^4 + 
          675*m2^5*Ycut^4 + 114840*MBhat*Ycut^4 - 162960*m2*MBhat*Ycut^4 + 
          94080*m2^2*MBhat*Ycut^4 - 7440*m2^3*MBhat*Ycut^4 - 
          16200*m2^4*MBhat*Ycut^4 + 5040*m2^5*MBhat*Ycut^4 - 
          49800*MBhat^2*Ycut^4 + 35280*m2*MBhat^2*Ycut^4 - 
          23880*m2^2*MBhat^2*Ycut^4 + 2040*m2^3*MBhat^2*Ycut^4 + 
          2250*m2^4*MBhat^2*Ycut^4 + 37260*Ycut^5 - 54360*m2*Ycut^5 + 
          5580*m2^2*Ycut^5 + 7260*m2^3*Ycut^5 - 690*m2^4*Ycut^5 - 
          270*m2^5*Ycut^5 - 40638*MBhat*Ycut^5 + 52080*m2*MBhat*Ycut^5 - 
          32460*m2^2*MBhat*Ycut^5 + 960*m2^3*MBhat*Ycut^5 + 
          7530*m2^4*MBhat*Ycut^5 - 2016*m2^5*MBhat*Ycut^5 + 
          14040*MBhat^2*Ycut^5 - 8160*m2*MBhat^2*Ycut^5 + 
          12240*m2^2*MBhat^2*Ycut^5 - 720*m2^3*MBhat^2*Ycut^5 - 
          900*m2^4*MBhat^2*Ycut^5 - 13560*Ycut^6 + 14130*m2*Ycut^6 + 
          1830*m2^2*Ycut^6 - 2970*m2^3*Ycut^6 + 115*m2^4*Ycut^6 + 
          45*m2^5*Ycut^6 - 7332*MBhat*Ycut^6 + 7440*m2*MBhat*Ycut^6 + 
          1320*m2^2*MBhat*Ycut^6 + 720*m2^3*MBhat*Ycut^6 - 
          1780*m2^4*MBhat*Ycut^6 + 336*m2^5*MBhat*Ycut^6 + 
          9000*MBhat^2*Ycut^6 - 4080*m2*MBhat^2*Ycut^6 - 3540*m2^2*MBhat^2*
           Ycut^6 + 120*m2^3*MBhat^2*Ycut^6 + 150*m2^4*MBhat^2*Ycut^6 + 
          7620*Ycut^7 - 4980*m2*Ycut^7 - 540*m2^2*Ycut^7 + 660*m2^3*Ycut^7 + 
          12150*MBhat*Ycut^7 - 11280*m2*MBhat*Ycut^7 + 2760*m2^2*MBhat*
           Ycut^7 - 240*m2^3*MBhat*Ycut^7 + 150*m2^4*MBhat*Ycut^7 - 
          9480*MBhat^2*Ycut^7 + 2880*m2*MBhat^2*Ycut^7 + 
          480*m2^2*MBhat^2*Ycut^7 - 4635*Ycut^8 + 1935*m2*Ycut^8 + 
          90*m2^2*Ycut^8 - 30*m2^3*Ycut^8 - 4080*MBhat*Ycut^8 + 
          3360*m2*MBhat*Ycut^8 - 840*m2^2*MBhat*Ycut^8 + 
          3150*MBhat^2*Ycut^8 - 480*m2*MBhat^2*Ycut^8 + 1470*Ycut^9 - 
          230*m2*Ycut^9 + 510*MBhat*Ycut^9 - 400*m2*MBhat*Ycut^9 + 
          60*m2^2*MBhat*Ycut^9 - 300*MBhat^2*Ycut^9 - 165*Ycut^10 - 
          15*m2*Ycut^10 - 36*MBhat*Ycut^10 - 30*MBhat^2*Ycut^10 + 
          6*MBhat*Ycut^11)/(-1 + Ycut)^6 + 
       ((24 + 8*m2 - 69*m2^2 + 27*m2^3 - 48*MBhat + 16*m2*MBhat + 
          24*MBhat^2 + 18*m2^2*MBhat^2)*Log[m2])/3 + 
       ((-24 - 8*m2 + 69*m2^2 - 27*m2^3 + 48*MBhat - 16*m2*MBhat - 
          24*MBhat^2 - 18*m2^2*MBhat^2)*Log[1 - Ycut])/3) + 
     c[SL]^2*(-1/120*(-1330 + 1750*m2 - 640*m2^2 + 160*m2^3 + 50*m2^4 + 
          10*m2^5 + 2192*MBhat - 2800*m2*MBhat + 960*m2^2*MBhat - 
          320*m2^3*MBhat - 80*m2^4*MBhat + 48*m2^5*MBhat - 985*MBhat^2 + 
          920*m2*MBhat^2 + 40*m2^3*MBhat^2 + 25*m2^4*MBhat^2 + 8460*Ycut - 
          9540*m2*Ycut + 3240*m2^2*Ycut - 600*m2^3*Ycut - 300*m2^4*Ycut - 
          60*m2^5*Ycut - 14112*MBhat*Ycut + 15840*m2*MBhat*Ycut - 
          5760*m2^2*MBhat*Ycut + 1920*m2^3*MBhat*Ycut + 480*m2^4*MBhat*Ycut - 
          288*m2^5*MBhat*Ycut + 6390*MBhat^2*Ycut - 5040*m2*MBhat^2*Ycut + 
          180*m2^2*MBhat^2*Ycut - 240*m2^3*MBhat^2*Ycut - 
          150*m2^4*MBhat^2*Ycut - 22590*Ycut^2 + 20970*m2*Ycut^2 - 
          6300*m2^2*Ycut^2 + 420*m2^3*Ycut^2 + 750*m2^4*Ycut^2 + 
          150*m2^5*Ycut^2 + 38160*MBhat*Ycut^2 - 36720*m2*MBhat*Ycut^2 + 
          14400*m2^2*MBhat*Ycut^2 - 4800*m2^3*MBhat*Ycut^2 - 
          1200*m2^4*MBhat*Ycut^2 + 720*m2^5*MBhat*Ycut^2 - 
          17415*MBhat^2*Ycut^2 + 11160*m2*MBhat^2*Ycut^2 - 
          990*m2^2*MBhat^2*Ycut^2 + 600*m2^3*MBhat^2*Ycut^2 + 
          375*m2^4*MBhat^2*Ycut^2 + 32520*Ycut^3 - 23160*m2*Ycut^3 + 
          5400*m2^2*Ycut^3 + 1240*m2^3*Ycut^3 - 1000*m2^4*Ycut^3 - 
          200*m2^5*Ycut^3 - 55430*MBhat*Ycut^3 + 43860*m2*MBhat*Ycut^3 - 
          19000*m2^2*MBhat*Ycut^3 + 5900*m2^3*MBhat*Ycut^3 + 
          1950*m2^4*MBhat*Ycut^3 - 960*m2^5*MBhat*Ycut^3 + 
          25460*MBhat^2*Ycut^3 - 12320*m2*MBhat^2*Ycut^3 + 
          2060*m2^2*MBhat^2*Ycut^3 - 640*m2^3*MBhat^2*Ycut^3 - 
          500*m2^4*MBhat^2*Ycut^3 - 26915*Ycut^4 + 12720*m2*Ycut^4 - 
          1150*m2^2*Ycut^4 - 2480*m2^3*Ycut^4 + 575*m2^4*Ycut^4 + 
          150*m2^5*Ycut^4 + 45300*MBhat*Ycut^4 - 27080*m2*MBhat*Ycut^4 + 
          14080*m2^2*MBhat*Ycut^4 - 3960*m2^3*MBhat*Ycut^4 - 
          1700*m2^4*MBhat*Ycut^4 + 720*m2^5*MBhat*Ycut^4 - 
          20780*MBhat^2*Ycut^4 + 6280*m2*MBhat^2*Ycut^4 - 
          2180*m2^2*MBhat^2*Ycut^4 + 340*m2^3*MBhat^2*Ycut^4 + 
          375*m2^4*MBhat^2*Ycut^4 + 12810*Ycut^5 - 2760*m2*Ycut^5 - 
          1260*m2^2*Ycut^5 + 1920*m2^3*Ycut^5 - 210*m2^4*Ycut^5 - 
          60*m2^5*Ycut^5 - 18999*MBhat*Ycut^5 + 6540*m2*MBhat*Ycut^5 - 
          5880*m2^2*MBhat*Ycut^5 + 1500*m2^3*MBhat*Ycut^5 + 
          855*m2^4*MBhat*Ycut^5 - 288*m2^5*MBhat*Ycut^5 + 
          8340*MBhat^2*Ycut^5 - 240*m2*MBhat^2*Ycut^5 + 1320*m2^2*MBhat^2*
           Ycut^5 - 120*m2^3*MBhat^2*Ycut^5 - 150*m2^4*MBhat^2*Ycut^5 - 
          3885*Ycut^6 + 320*m2*Ycut^6 + 1000*m2^2*Ycut^6 - 820*m2^3*Ycut^6 + 
          35*m2^4*Ycut^6 + 10*m2^5*Ycut^6 + 2134*MBhat*Ycut^6 + 
          800*m2*MBhat*Ycut^6 + 1360*m2^2*MBhat*Ycut^6 - 
          240*m2^3*MBhat*Ycut^6 - 230*m2^4*MBhat*Ycut^6 + 
          48*m2^5*MBhat*Ycut^6 - 340*MBhat^2*Ycut^6 - 1080*m2*MBhat^2*
           Ycut^6 - 470*m2^2*MBhat^2*Ycut^6 + 20*m2^3*MBhat^2*Ycut^6 + 
          25*m2^4*MBhat^2*Ycut^6 + 1540*Ycut^7 - 440*m2*Ycut^7 - 
          380*m2^2*Ycut^7 + 160*m2^3*Ycut^7 + 915*MBhat*Ycut^7 - 
          340*m2*MBhat*Ycut^7 - 160*m2^2*MBhat*Ycut^7 + 
          25*m2^4*MBhat*Ycut^7 - 860*MBhat^2*Ycut^7 + 320*m2*MBhat^2*Ycut^7 + 
          80*m2^2*MBhat^2*Ycut^7 - 795*Ycut^8 + 90*m2*Ycut^8 + 
          90*m2^2*Ycut^8 - 120*MBhat*Ycut^8 - 120*m2*MBhat*Ycut^8 + 
          165*MBhat^2*Ycut^8 + 190*Ycut^9 + 60*m2*Ycut^9 - 25*MBhat*Ycut^9 + 
          20*m2*MBhat*Ycut^9 + 30*MBhat^2*Ycut^9 - 5*Ycut^10 - 
          10*m2*Ycut^10 - 18*MBhat*Ycut^10 - 5*MBhat^2*Ycut^10 + 
          3*MBhat*Ycut^11)/(-1 + Ycut)^6 + 
       ((8 + 16*m2 - 10*m2^2 + 6*m2^3 - 16*MBhat - 16*m2*MBhat + 8*MBhat^2 + 
          8*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2])/2 + 
       ((-8 - 16*m2 + 10*m2^2 - 6*m2^3 + 16*MBhat + 16*m2*MBhat - 8*MBhat^2 - 
          8*m2*MBhat^2 - 3*m2^2*MBhat^2)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/120*(-1330 + 1750*m2 - 640*m2^2 + 160*m2^3 + 50*m2^4 + 
          10*m2^5 + 2192*MBhat - 2800*m2*MBhat + 960*m2^2*MBhat - 
          320*m2^3*MBhat - 80*m2^4*MBhat + 48*m2^5*MBhat - 985*MBhat^2 + 
          920*m2*MBhat^2 + 40*m2^3*MBhat^2 + 25*m2^4*MBhat^2 + 8460*Ycut - 
          9540*m2*Ycut + 3240*m2^2*Ycut - 600*m2^3*Ycut - 300*m2^4*Ycut - 
          60*m2^5*Ycut - 14112*MBhat*Ycut + 15840*m2*MBhat*Ycut - 
          5760*m2^2*MBhat*Ycut + 1920*m2^3*MBhat*Ycut + 480*m2^4*MBhat*Ycut - 
          288*m2^5*MBhat*Ycut + 6390*MBhat^2*Ycut - 5040*m2*MBhat^2*Ycut + 
          180*m2^2*MBhat^2*Ycut - 240*m2^3*MBhat^2*Ycut - 
          150*m2^4*MBhat^2*Ycut - 22590*Ycut^2 + 20970*m2*Ycut^2 - 
          6300*m2^2*Ycut^2 + 420*m2^3*Ycut^2 + 750*m2^4*Ycut^2 + 
          150*m2^5*Ycut^2 + 38160*MBhat*Ycut^2 - 36720*m2*MBhat*Ycut^2 + 
          14400*m2^2*MBhat*Ycut^2 - 4800*m2^3*MBhat*Ycut^2 - 
          1200*m2^4*MBhat*Ycut^2 + 720*m2^5*MBhat*Ycut^2 - 
          17415*MBhat^2*Ycut^2 + 11160*m2*MBhat^2*Ycut^2 - 
          990*m2^2*MBhat^2*Ycut^2 + 600*m2^3*MBhat^2*Ycut^2 + 
          375*m2^4*MBhat^2*Ycut^2 + 32520*Ycut^3 - 23160*m2*Ycut^3 + 
          5400*m2^2*Ycut^3 + 1240*m2^3*Ycut^3 - 1000*m2^4*Ycut^3 - 
          200*m2^5*Ycut^3 - 55430*MBhat*Ycut^3 + 43860*m2*MBhat*Ycut^3 - 
          19000*m2^2*MBhat*Ycut^3 + 5900*m2^3*MBhat*Ycut^3 + 
          1950*m2^4*MBhat*Ycut^3 - 960*m2^5*MBhat*Ycut^3 + 
          25460*MBhat^2*Ycut^3 - 12320*m2*MBhat^2*Ycut^3 + 
          2060*m2^2*MBhat^2*Ycut^3 - 640*m2^3*MBhat^2*Ycut^3 - 
          500*m2^4*MBhat^2*Ycut^3 - 26915*Ycut^4 + 12720*m2*Ycut^4 - 
          1150*m2^2*Ycut^4 - 2480*m2^3*Ycut^4 + 575*m2^4*Ycut^4 + 
          150*m2^5*Ycut^4 + 45300*MBhat*Ycut^4 - 27080*m2*MBhat*Ycut^4 + 
          14080*m2^2*MBhat*Ycut^4 - 3960*m2^3*MBhat*Ycut^4 - 
          1700*m2^4*MBhat*Ycut^4 + 720*m2^5*MBhat*Ycut^4 - 
          20780*MBhat^2*Ycut^4 + 6280*m2*MBhat^2*Ycut^4 - 
          2180*m2^2*MBhat^2*Ycut^4 + 340*m2^3*MBhat^2*Ycut^4 + 
          375*m2^4*MBhat^2*Ycut^4 + 12810*Ycut^5 - 2760*m2*Ycut^5 - 
          1260*m2^2*Ycut^5 + 1920*m2^3*Ycut^5 - 210*m2^4*Ycut^5 - 
          60*m2^5*Ycut^5 - 18999*MBhat*Ycut^5 + 6540*m2*MBhat*Ycut^5 - 
          5880*m2^2*MBhat*Ycut^5 + 1500*m2^3*MBhat*Ycut^5 + 
          855*m2^4*MBhat*Ycut^5 - 288*m2^5*MBhat*Ycut^5 + 
          8340*MBhat^2*Ycut^5 - 240*m2*MBhat^2*Ycut^5 + 1320*m2^2*MBhat^2*
           Ycut^5 - 120*m2^3*MBhat^2*Ycut^5 - 150*m2^4*MBhat^2*Ycut^5 - 
          3885*Ycut^6 + 320*m2*Ycut^6 + 1000*m2^2*Ycut^6 - 820*m2^3*Ycut^6 + 
          35*m2^4*Ycut^6 + 10*m2^5*Ycut^6 + 2134*MBhat*Ycut^6 + 
          800*m2*MBhat*Ycut^6 + 1360*m2^2*MBhat*Ycut^6 - 
          240*m2^3*MBhat*Ycut^6 - 230*m2^4*MBhat*Ycut^6 + 
          48*m2^5*MBhat*Ycut^6 - 340*MBhat^2*Ycut^6 - 1080*m2*MBhat^2*
           Ycut^6 - 470*m2^2*MBhat^2*Ycut^6 + 20*m2^3*MBhat^2*Ycut^6 + 
          25*m2^4*MBhat^2*Ycut^6 + 1540*Ycut^7 - 440*m2*Ycut^7 - 
          380*m2^2*Ycut^7 + 160*m2^3*Ycut^7 + 915*MBhat*Ycut^7 - 
          340*m2*MBhat*Ycut^7 - 160*m2^2*MBhat*Ycut^7 + 
          25*m2^4*MBhat*Ycut^7 - 860*MBhat^2*Ycut^7 + 320*m2*MBhat^2*Ycut^7 + 
          80*m2^2*MBhat^2*Ycut^7 - 795*Ycut^8 + 90*m2*Ycut^8 + 
          90*m2^2*Ycut^8 - 120*MBhat*Ycut^8 - 120*m2*MBhat*Ycut^8 + 
          165*MBhat^2*Ycut^8 + 190*Ycut^9 + 60*m2*Ycut^9 - 25*MBhat*Ycut^9 + 
          20*m2*MBhat*Ycut^9 + 30*MBhat^2*Ycut^9 - 5*Ycut^10 - 
          10*m2*Ycut^10 - 18*MBhat*Ycut^10 - 5*MBhat^2*Ycut^10 + 
          3*MBhat*Ycut^11)/(-1 + Ycut)^6 + 
       ((8 + 16*m2 - 10*m2^2 + 6*m2^3 - 16*MBhat - 16*m2*MBhat + 8*MBhat^2 + 
          8*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2])/2 + 
       ((-8 - 16*m2 + 10*m2^2 - 6*m2^3 + 16*MBhat + 16*m2*MBhat - 8*MBhat^2 - 
          8*m2*MBhat^2 - 3*m2^2*MBhat^2)*Log[1 - Ycut])/2) + 
     c[VR]*((Sqrt[m2]*(385 + 100*m2 - 576*m2^2 + 92*m2^3 - m2^4 - 496*MBhat + 
          448*m2*MBhat + 64*m2^3*MBhat - 16*m2^4*MBhat + 150*MBhat^2 - 
          234*m2*MBhat^2 + 90*m2^2*MBhat^2 - 6*m2^3*MBhat^2 - 2045*Ycut - 
          1088*m2*Ycut + 2772*m2^2*Ycut - 424*m2^3*Ycut + 5*m2^4*Ycut + 
          2672*MBhat*Ycut - 1856*m2*MBhat*Ycut - 320*m2^3*MBhat*Ycut + 
          80*m2^4*MBhat*Ycut - 822*MBhat^2*Ycut + 1134*m2*MBhat^2*Ycut - 
          414*m2^2*MBhat^2*Ycut + 30*m2^3*MBhat^2*Ycut + 4390*Ycut^2 + 
          3646*m2*Ycut^2 - 5274*m2^2*Ycut^2 + 758*m2^3*Ycut^2 - 
          10*m2^4*Ycut^2 - 5824*MBhat*Ycut^2 + 2752*m2*MBhat*Ycut^2 + 
          640*m2^3*MBhat*Ycut^2 - 160*m2^4*MBhat*Ycut^2 + 
          1824*MBhat^2*Ycut^2 - 2178*m2*MBhat^2*Ycut^2 + 
          738*m2^2*MBhat^2*Ycut^2 - 60*m2^3*MBhat^2*Ycut^2 - 4790*Ycut^3 - 
          5606*m2*Ycut^3 + 4914*m2^2*Ycut^3 - 638*m2^3*Ycut^3 + 
          10*m2^4*Ycut^3 + 6434*MBhat*Ycut^3 - 1462*m2*MBhat*Ycut^3 + 
          70*m2^2*MBhat*Ycut^3 - 690*m2^3*MBhat*Ycut^3 + 
          160*m2^4*MBhat*Ycut^3 - 2048*MBhat^2*Ycut^3 + 2058*m2*MBhat^2*
           Ycut^3 - 634*m2^2*MBhat^2*Ycut^3 + 60*m2^3*MBhat^2*Ycut^3 + 
          2704*Ycut^4 + 4280*m2*Ycut^4 - 2228*m2^2*Ycut^4 + 254*m2^3*Ycut^4 - 
          5*m2^4*Ycut^4 - 3562*MBhat*Ycut^4 - 370*m2*MBhat*Ycut^4 - 
          46*m2^2*MBhat*Ycut^4 + 362*m2^3*MBhat*Ycut^4 - 
          80*m2^4*MBhat*Ycut^4 + 1126*MBhat^2*Ycut^4 - 886*m2*MBhat^2*
           Ycut^4 + 224*m2^2*MBhat^2*Ycut^4 - 30*m2^3*MBhat^2*Ycut^4 - 
          704*Ycut^5 - 1420*m2*Ycut^5 + 352*m2^2*Ycut^5 - 10*m2^3*Ycut^5 + 
          m2^4*Ycut^5 + 644*MBhat*Ycut^5 + 788*m2*MBhat*Ycut^5 - 
          106*m2^2*MBhat*Ycut^5 - 70*m2^3*MBhat*Ycut^5 + 
          16*m2^4*MBhat*Ycut^5 - 134*MBhat^2*Ycut^5 + 2*m2*MBhat^2*Ycut^5 + 
          20*m2^2*MBhat^2*Ycut^5 + 6*m2^3*MBhat^2*Ycut^5 + 106*Ycut^6 + 
          6*m2*Ycut^6 + 70*m2^2*Ycut^6 - 16*m2^3*Ycut^6 + 236*MBhat*Ycut^6 - 
          388*m2*MBhat*Ycut^6 + 106*m2^2*MBhat*Ycut^6 - 2*m2^3*MBhat*Ycut^6 - 
          172*MBhat^2*Ycut^6 + 146*m2*MBhat^2*Ycut^6 - 24*m2^2*MBhat^2*
           Ycut^6 - 74*Ycut^7 + 98*m2*Ycut^7 - 30*m2^2*Ycut^7 - 
          118*MBhat*Ycut^7 + 98*m2*MBhat*Ycut^7 - 24*m2^2*MBhat*Ycut^7 + 
          92*MBhat^2*Ycut^7 - 42*m2*MBhat^2*Ycut^7 + 31*Ycut^8 - 
          16*m2*Ycut^8 + 14*MBhat*Ycut^8 - 10*m2*MBhat*Ycut^8 - 
          16*MBhat^2*Ycut^8 - 3*Ycut^9))/(3*(-1 + Ycut)^5) + 
       4*Sqrt[m2]*(-10 - 49*m2 - 9*m2^2 + 3*m2^3 + 16*MBhat + 32*m2*MBhat - 
         6*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2] - 
       4*Sqrt[m2]*(-10 - 49*m2 - 9*m2^2 + 3*m2^3 + 16*MBhat + 32*m2*MBhat - 
         6*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[1 - Ycut]) + 
     c[T]^2*((-2*(-3300 + 13060*m2 - 11040*m2^2 + 960*m2^3 + 260*m2^4 + 
          60*m2^5 + 4272*MBhat - 12880*m2*MBhat + 10560*m2^2*MBhat - 
          960*m2^3*MBhat - 1520*m2^4*MBhat + 528*m2^5*MBhat - 1665*MBhat^2 + 
          2520*m2*MBhat^2 - 1440*m2^2*MBhat^2 + 360*m2^3*MBhat^2 + 
          225*m2^4*MBhat^2 + 21240*Ycut - 80280*m2*Ycut + 59760*m2^2*Ycut - 
          3600*m2^3*Ycut - 1560*m2^4*Ycut - 360*m2^5*Ycut - 
          28512*MBhat*Ycut + 82080*m2*MBhat*Ycut - 63360*m2^2*MBhat*Ycut + 
          5760*m2^3*MBhat*Ycut + 9120*m2^4*MBhat*Ycut - 3168*m2^5*MBhat*
           Ycut + 11430*MBhat^2*Ycut - 16560*m2*MBhat^2*Ycut + 
          10260*m2^2*MBhat^2*Ycut - 2160*m2^3*MBhat^2*Ycut - 
          1350*m2^4*MBhat^2*Ycut - 57420*Ycut^2 + 206460*m2*Ycut^2 - 
          129960*m2^2*Ycut^2 + 2520*m2^3*Ycut^2 + 3900*m2^4*Ycut^2 + 
          900*m2^5*Ycut^2 + 79920*MBhat*Ycut^2 - 219600*m2*MBhat*Ycut^2 + 
          158400*m2^2*MBhat*Ycut^2 - 14400*m2^3*MBhat*Ycut^2 - 
          22800*m2^4*MBhat*Ycut^2 + 7920*m2^5*MBhat*Ycut^2 - 
          32895*MBhat^2*Ycut^2 + 45720*m2*MBhat^2*Ycut^2 - 
          30510*m2^2*MBhat^2*Ycut^2 + 5400*m2^3*MBhat^2*Ycut^2 + 
          3375*m2^4*MBhat^2*Ycut^2 + 83760*Ycut^3 - 284880*m2*Ycut^3 + 
          140880*m2^2*Ycut^3 + 7440*m2^3*Ycut^3 - 5200*m2^4*Ycut^3 - 
          1200*m2^5*Ycut^3 - 118650*MBhat*Ycut^3 + 314580*m2*MBhat*Ycut^3 - 
          212640*m2^2*MBhat*Ycut^3 + 19500*m2^3*MBhat*Ycut^3 + 
          31450*m2^4*MBhat*Ycut^3 - 10560*m2^5*MBhat*Ycut^3 + 
          49620*MBhat^2*Ycut^3 - 67680*m2*MBhat^2*Ycut^3 + 
          49260*m2^2*MBhat^2*Ycut^3 - 6720*m2^3*MBhat^2*Ycut^3 - 
          4500*m2^4*MBhat^2*Ycut^3 - 70845*Ycut^4 + 223860*m2*Ycut^4 - 
          72510*m2^2*Ycut^4 - 16380*m2^3*Ycut^4 + 3375*m2^4*Ycut^4 + 
          900*m2^5*Ycut^4 + 91980*MBhat*Ycut^4 - 249960*m2*MBhat*Ycut^4 + 
          164880*m2^2*MBhat*Ycut^4 - 17880*m2^3*MBhat*Ycut^4 - 
          24300*m2^4*MBhat*Ycut^4 + 7920*m2^5*MBhat*Ycut^4 - 
          37320*MBhat^2*Ycut^4 + 56040*m2*MBhat^2*Ycut^4 - 
          47040*m2^2*MBhat^2*Ycut^4 + 4620*m2^3*MBhat^2*Ycut^4 + 
          3375*m2^4*MBhat^2*Ycut^4 + 36990*Ycut^5 - 97800*m2*Ycut^5 + 
          6420*m2^2*Ycut^5 + 14280*m2^3*Ycut^5 - 1290*m2^4*Ycut^5 - 
          360*m2^5*Ycut^5 - 19749*MBhat*Ycut^5 + 93900*m2*MBhat*Ycut^5 - 
          73440*m2^2*MBhat*Ycut^5 + 11940*m2^3*MBhat*Ycut^5 + 
          10245*m2^4*MBhat*Ycut^5 - 3168*m2^5*MBhat*Ycut^5 + 
          3420*MBhat^2*Ycut^5 - 23280*m2*MBhat^2*Ycut^5 + 
          26640*m2^2*MBhat^2*Ycut^5 - 1800*m2^3*MBhat^2*Ycut^5 - 
          1350*m2^4*MBhat^2*Ycut^5 - 17715*Ycut^6 + 22620*m2*Ycut^6 + 
          10380*m2^2*Ycut^6 - 6360*m2^3*Ycut^6 + 215*m2^4*Ycut^6 + 
          60*m2^5*Ycut^6 - 27246*MBhat*Ycut^6 + 3840*m2*MBhat*Ycut^6 + 
          16560*m2^2*MBhat*Ycut^6 - 4800*m2^3*MBhat*Ycut^6 - 
          1970*m2^4*MBhat*Ycut^6 + 528*m2^5*MBhat*Ycut^6 + 
          18120*MBhat^2*Ycut^6 + 1800*m2*MBhat^2*Ycut^6 - 
          7950*m2^2*MBhat^2*Ycut^6 + 300*m2^3*MBhat^2*Ycut^6 + 
          225*m2^4*MBhat^2*Ycut^6 + 13620*Ycut^7 - 4320*m2*Ycut^7 - 
          4620*m2^2*Ycut^7 + 1080*m2^3*Ycut^7 + 26505*MBhat*Ycut^7 - 
          16980*m2*MBhat*Ycut^7 - 360*m2^2*MBhat*Ycut^7 + 
          840*m2^3*MBhat*Ycut^7 + 75*m2^4*MBhat*Ycut^7 - 
          15180*MBhat^2*Ycut^7 + 1920*m2*MBhat^2*Ycut^7 + 
          600*m2^2*MBhat^2*Ycut^7 - 9135*Ycut^8 + 1320*m2*Ycut^8 + 
          690*m2^2*Ycut^8 + 60*m2^3*Ycut^8 - 10200*MBhat*Ycut^8 + 
          5640*m2*MBhat*Ycut^8 - 720*m2^2*MBhat*Ycut^8 + 
          4905*MBhat^2*Ycut^8 - 480*m2*MBhat^2*Ycut^8 + 180*m2^2*MBhat^2*
           Ycut^8 + 3270*Ycut^9 + 80*m2*Ycut^9 + 1845*MBhat*Ycut^9 - 
          620*m2*MBhat*Ycut^9 + 120*m2^2*MBhat*Ycut^9 - 330*MBhat^2*Ycut^9 - 
          465*Ycut^10 - 120*m2*Ycut^10 - 198*MBhat*Ycut^10 - 
          105*MBhat^2*Ycut^10 + 33*MBhat*Ycut^11))/(45*(-1 + Ycut)^6) + 
       (8*(24 - 32*m2 - 108*m2^2 + 36*m2^3 - 48*MBhat + 80*m2*MBhat + 
          24*MBhat^2 - 24*m2*MBhat^2 + 27*m2^2*MBhat^2)*Log[m2])/3 - 
       (8*(24 - 32*m2 - 108*m2^2 + 36*m2^3 - 48*MBhat + 80*m2*MBhat + 
          24*MBhat^2 - 24*m2*MBhat^2 + 27*m2^2*MBhat^2)*Log[1 - Ycut])/3) + 
     c[SL]*(-1/6*(Ycut^3*(-10*MBhat - 40*m2*MBhat + 180*m2^2*MBhat - 
           200*m2^3*MBhat + 70*m2^4*MBhat + 32*m2*MBhat^2 - 64*m2^2*MBhat^2 + 
           32*m2^3*MBhat^2 + 5*Ycut + 20*m2*Ycut - 90*m2^2*Ycut + 
           100*m2^3*Ycut - 35*m2^4*Ycut + 60*MBhat*Ycut + 176*m2*MBhat*Ycut - 
           632*m2^2*MBhat*Ycut + 496*m2^3*MBhat*Ycut - 100*m2^4*MBhat*Ycut + 
           2*MBhat^2*Ycut - 144*m2*MBhat^2*Ycut + 194*m2^2*MBhat^2*Ycut - 
           52*m2^3*MBhat^2*Ycut - 30*Ycut^2 - 88*m2*Ycut^2 + 
           284*m2^2*Ycut^2 - 184*m2^3*Ycut^2 + 18*m2^4*Ycut^2 - 
           151*MBhat*Ycut^2 - 312*m2*MBhat*Ycut^2 + 872*m2^2*MBhat*Ycut^2 - 
           484*m2^3*MBhat*Ycut^2 + 75*m2^4*MBhat*Ycut^2 - 12*MBhat^2*Ycut^2 + 
           256*m2*MBhat^2*Ycut^2 - 204*m2^2*MBhat^2*Ycut^2 + 
           24*m2^3*MBhat^2*Ycut^2 + 75*Ycut^3 + 156*m2*Ycut^3 - 
           324*m2^2*Ycut^3 + 96*m2^3*Ycut^3 - 3*m2^4*Ycut^3 + 
           206*MBhat*Ycut^3 + 288*m2*MBhat*Ycut^3 - 600*m2^2*MBhat*Ycut^3 + 
           232*m2^3*MBhat*Ycut^3 - 30*m2^4*MBhat*Ycut^3 + 30*MBhat^2*Ycut^3 - 
           224*m2*MBhat^2*Ycut^3 + 76*m2^2*MBhat^2*Ycut^3 - 
           4*m2^3*MBhat^2*Ycut^3 - 100*Ycut^4 - 144*m2*Ycut^4 + 
           156*m2^2*Ycut^4 - 8*m2^3*Ycut^4 - 165*MBhat*Ycut^4 - 
           152*m2*MBhat*Ycut^4 + 212*m2^2*MBhat*Ycut^4 - 44*m2^3*MBhat*
            Ycut^4 + 5*m2^4*MBhat*Ycut^4 - 40*MBhat^2*Ycut^4 + 
           96*m2*MBhat^2*Ycut^4 + 4*m2^2*MBhat^2*Ycut^4 + 75*Ycut^5 + 
           76*m2*Ycut^5 - 26*m2^2*Ycut^5 - 4*m2^3*Ycut^5 + 80*MBhat*Ycut^5 + 
           48*m2*MBhat*Ycut^5 - 32*m2^2*MBhat*Ycut^5 + 30*MBhat^2*Ycut^5 - 
           16*m2*MBhat^2*Ycut^5 - 6*m2^2*MBhat^2*Ycut^5 - 30*Ycut^6 - 
           24*m2*Ycut^6 - 25*MBhat*Ycut^6 - 8*m2*MBhat*Ycut^6 - 
           12*MBhat^2*Ycut^6 + 5*Ycut^7 + 4*m2*Ycut^7 + 6*MBhat*Ycut^7 + 
           2*MBhat^2*Ycut^7 - MBhat*Ycut^8)*c[T])/(-1 + Ycut)^6 + 
       c[SR]*(-1/6*(Sqrt[m2]*(551 + 332*m2 - 936*m2^2 + 52*m2^3 + m2^4 - 
             792*MBhat + 512*m2*MBhat + 288*m2^2*MBhat - 8*m2^4*MBhat + 
             286*MBhat^2 - 306*m2*MBhat^2 + 18*m2^2*MBhat^2 + 
             2*m2^3*MBhat^2 - 2923*Ycut - 2584*m2*Ycut + 4356*m2^2*Ycut - 
             224*m2^3*Ycut - 5*m2^4*Ycut + 4248*MBhat*Ycut - 
             1792*m2*MBhat*Ycut - 1440*m2^2*MBhat*Ycut + 40*m2^4*MBhat*Ycut - 
             1550*MBhat^2*Ycut + 1350*m2*MBhat^2*Ycut - 54*m2^2*MBhat^2*
              Ycut - 10*m2^3*MBhat^2*Ycut + 6266*Ycut^2 + 7478*m2*Ycut^2 - 
             7902*m2^2*Ycut^2 + 358*m2^3*Ycut^2 + 10*m2^4*Ycut^2 - 
             9216*MBhat*Ycut^2 + 1664*m2*MBhat*Ycut^2 + 2880*m2^2*MBhat*
              Ycut^2 - 80*m2^4*MBhat*Ycut^2 + 3400*MBhat^2*Ycut^2 - 
             2250*m2*MBhat^2*Ycut^2 + 18*m2^2*MBhat^2*Ycut^2 + 
             20*m2^3*MBhat^2*Ycut^2 - 6826*Ycut^3 - 10558*m2*Ycut^3 + 
             6822*m2^2*Ycut^3 - 238*m2^3*Ycut^3 - 10*m2^4*Ycut^3 + 
             10146*MBhat*Ycut^3 + 906*m2*MBhat*Ycut^3 - 2810*m2^2*MBhat*
              Ycut^3 - 50*m2^3*MBhat*Ycut^3 + 80*m2^4*MBhat*Ycut^3 - 
             3784*MBhat^2*Ycut^3 + 1650*m2*MBhat^2*Ycut^3 + 
             86*m2^2*MBhat^2*Ycut^3 - 20*m2^3*MBhat^2*Ycut^3 + 3848*Ycut^4 + 
             7584*m2*Ycut^4 - 2636*m2^2*Ycut^4 + 54*m2^3*Ycut^4 + 
             5*m2^4*Ycut^4 - 5682*MBhat*Ycut^4 - 2466*m2*MBhat*Ycut^4 + 
             1370*m2^2*MBhat*Ycut^4 + 42*m2^3*MBhat*Ycut^4 - 
             40*m2^4*MBhat*Ycut^4 + 2126*MBhat^2*Ycut^4 - 334*m2*MBhat^2*
              Ycut^4 - 136*m2^2*MBhat^2*Ycut^4 + 10*m2^3*MBhat^2*Ycut^4 - 
             1000*Ycut^5 - 2388*m2*Ycut^5 + 208*m2^2*Ycut^5 + 
             30*m2^3*Ycut^5 - m2^4*Ycut^5 + 1260*MBhat*Ycut^5 + 
             1428*m2*MBhat*Ycut^5 - 322*m2^2*MBhat*Ycut^5 - 
             6*m2^3*MBhat*Ycut^5 + 8*m2^4*MBhat*Ycut^5 - 430*MBhat^2*Ycut^5 - 
             214*m2*MBhat^2*Ycut^5 + 92*m2^2*MBhat^2*Ycut^5 - 
             2*m2^3*MBhat^2*Ycut^5 + 134*Ycut^6 + 46*m2*Ycut^6 + 
             106*m2^2*Ycut^6 - 16*m2^3*Ycut^6 + 60*MBhat*Ycut^6 - 
             228*m2*MBhat*Ycut^6 + 34*m2^2*MBhat*Ycut^6 - 2*m2^3*MBhat*
              Ycut^6 - 84*MBhat^2*Ycut^6 + 122*m2*MBhat^2*Ycut^6 - 
             24*m2^2*MBhat^2*Ycut^6 - 70*Ycut^7 + 90*m2*Ycut^7 - 
             18*m2^2*Ycut^7 - 6*MBhat*Ycut^7 - 30*m2*MBhat*Ycut^7 + 
             36*MBhat^2*Ycut^7 - 18*m2*MBhat^2*Ycut^7 + 17*Ycut^8 - 
             18*MBhat*Ycut^8 + 6*m2*MBhat*Ycut^8 + 3*Ycut^9))/(-1 + Ycut)^5 - 
         2*Sqrt[m2]*(-14 - 77*m2 - 27*m2^2 + 3*m2^3 + 24*MBhat + 
           64*m2*MBhat - 10*MBhat^2 - 15*m2*MBhat^2 + 3*m2^2*MBhat^2)*
          Log[m2] + 2*Sqrt[m2]*(-14 - 77*m2 - 27*m2^2 + 3*m2^3 + 24*MBhat + 
           64*m2*MBhat - 10*MBhat^2 - 15*m2*MBhat^2 + 3*m2^2*MBhat^2)*
          Log[1 - Ycut])) + 
     c[VL]*(-1/90*(-3645 + 9155*m2 - 6480*m2^2 + 720*m2^3 + 205*m2^4 + 
          45*m2^5 + 5424*MBhat - 10640*m2*MBhat + 6720*m2^2*MBhat - 
          960*m2^3*MBhat - 880*m2^4*MBhat + 336*m2^5*MBhat - 2310*MBhat^2 + 
          2640*m2*MBhat^2 - 720*m2^2*MBhat^2 + 240*m2^3*MBhat^2 + 
          150*m2^4*MBhat^2 + 23310*Ycut - 54450*m2*Ycut + 34740*m2^2*Ycut - 
          2700*m2^3*Ycut - 1230*m2^4*Ycut - 270*m2^5*Ycut - 
          35424*MBhat*Ycut + 64800*m2*MBhat*Ycut - 40320*m2^2*MBhat*Ycut + 
          5760*m2^3*MBhat*Ycut + 5280*m2^4*MBhat*Ycut - 2016*m2^5*MBhat*
           Ycut + 15300*MBhat^2*Ycut - 15840*m2*MBhat^2*Ycut + 
          5400*m2^2*MBhat^2*Ycut - 1440*m2^3*MBhat^2*Ycut - 
          900*m2^4*MBhat^2*Ycut - 62595*Ycut^2 + 134685*m2*Ycut^2 - 
          74430*m2^2*Ycut^2 + 1890*m2^3*Ycut^2 + 3075*m2^4*Ycut^2 + 
          675*m2^5*Ycut^2 + 97200*MBhat*Ycut^2 - 164880*m2*MBhat*Ycut^2 + 
          100800*m2^2*MBhat*Ycut^2 - 14400*m2^3*MBhat*Ycut^2 - 
          13200*m2^4*MBhat*Ycut^2 + 5040*m2^5*MBhat*Ycut^2 - 
          42570*MBhat^2*Ycut^2 + 39600*m2*MBhat^2*Ycut^2 - 
          16740*m2^2*MBhat^2*Ycut^2 + 3600*m2^3*MBhat^2*Ycut^2 + 
          2250*m2^4*MBhat^2*Ycut^2 + 90660*Ycut^3 - 177180*m2*Ycut^3 + 
          78540*m2^2*Ycut^3 + 5580*m2^3*Ycut^3 - 4100*m2^4*Ycut^3 - 
          900*m2^5*Ycut^3 - 142620*MBhat*Ycut^3 + 222480*m2*MBhat*Ycut^3 - 
          132120*m2^2*MBhat*Ycut^3 + 15600*m2^3*MBhat*Ycut^3 + 
          19700*m2^4*MBhat*Ycut^3 - 6720*m2^5*MBhat*Ycut^3 + 
          63000*MBhat^2*Ycut^3 - 51840*m2*MBhat^2*Ycut^3 + 
          26760*m2^2*MBhat^2*Ycut^3 - 3840*m2^3*MBhat^2*Ycut^3 - 
          3000*m2^4*MBhat^2*Ycut^3 - 75720*Ycut^4 + 131310*m2*Ycut^4 - 
          39330*m2^2*Ycut^4 - 10410*m2^3*Ycut^4 + 2025*m2^4*Ycut^4 + 
          675*m2^5*Ycut^4 + 114840*MBhat*Ycut^4 - 162960*m2*MBhat*Ycut^4 + 
          94080*m2^2*MBhat*Ycut^4 - 7440*m2^3*MBhat*Ycut^4 - 
          16200*m2^4*MBhat*Ycut^4 + 5040*m2^5*MBhat*Ycut^4 - 
          49800*MBhat^2*Ycut^4 + 35280*m2*MBhat^2*Ycut^4 - 
          23880*m2^2*MBhat^2*Ycut^4 + 2040*m2^3*MBhat^2*Ycut^4 + 
          2250*m2^4*MBhat^2*Ycut^4 + 37260*Ycut^5 - 54360*m2*Ycut^5 + 
          5580*m2^2*Ycut^5 + 7260*m2^3*Ycut^5 - 690*m2^4*Ycut^5 - 
          270*m2^5*Ycut^5 - 40638*MBhat*Ycut^5 + 52080*m2*MBhat*Ycut^5 - 
          32460*m2^2*MBhat*Ycut^5 + 960*m2^3*MBhat*Ycut^5 + 
          7530*m2^4*MBhat*Ycut^5 - 2016*m2^5*MBhat*Ycut^5 + 
          14040*MBhat^2*Ycut^5 - 8160*m2*MBhat^2*Ycut^5 + 
          12240*m2^2*MBhat^2*Ycut^5 - 720*m2^3*MBhat^2*Ycut^5 - 
          900*m2^4*MBhat^2*Ycut^5 - 13560*Ycut^6 + 14130*m2*Ycut^6 + 
          1830*m2^2*Ycut^6 - 2970*m2^3*Ycut^6 + 115*m2^4*Ycut^6 + 
          45*m2^5*Ycut^6 - 7332*MBhat*Ycut^6 + 7440*m2*MBhat*Ycut^6 + 
          1320*m2^2*MBhat*Ycut^6 + 720*m2^3*MBhat*Ycut^6 - 
          1780*m2^4*MBhat*Ycut^6 + 336*m2^5*MBhat*Ycut^6 + 
          9000*MBhat^2*Ycut^6 - 4080*m2*MBhat^2*Ycut^6 - 3540*m2^2*MBhat^2*
           Ycut^6 + 120*m2^3*MBhat^2*Ycut^6 + 150*m2^4*MBhat^2*Ycut^6 + 
          7620*Ycut^7 - 4980*m2*Ycut^7 - 540*m2^2*Ycut^7 + 660*m2^3*Ycut^7 + 
          12150*MBhat*Ycut^7 - 11280*m2*MBhat*Ycut^7 + 2760*m2^2*MBhat*
           Ycut^7 - 240*m2^3*MBhat*Ycut^7 + 150*m2^4*MBhat*Ycut^7 - 
          9480*MBhat^2*Ycut^7 + 2880*m2*MBhat^2*Ycut^7 + 
          480*m2^2*MBhat^2*Ycut^7 - 4635*Ycut^8 + 1935*m2*Ycut^8 + 
          90*m2^2*Ycut^8 - 30*m2^3*Ycut^8 - 4080*MBhat*Ycut^8 + 
          3360*m2*MBhat*Ycut^8 - 840*m2^2*MBhat*Ycut^8 + 
          3150*MBhat^2*Ycut^8 - 480*m2*MBhat^2*Ycut^8 + 1470*Ycut^9 - 
          230*m2*Ycut^9 + 510*MBhat*Ycut^9 - 400*m2*MBhat*Ycut^9 + 
          60*m2^2*MBhat*Ycut^9 - 300*MBhat^2*Ycut^9 - 165*Ycut^10 - 
          15*m2*Ycut^10 - 36*MBhat*Ycut^10 - 30*MBhat^2*Ycut^10 + 
          6*MBhat*Ycut^11)/(-1 + Ycut)^6 + 
       (2*(24 + 8*m2 - 69*m2^2 + 27*m2^3 - 48*MBhat + 16*m2*MBhat + 
          24*MBhat^2 + 18*m2^2*MBhat^2)*Log[m2])/3 - 
       (2*(24 + 8*m2 - 69*m2^2 + 27*m2^3 - 48*MBhat + 16*m2*MBhat + 
          24*MBhat^2 + 18*m2^2*MBhat^2)*Log[1 - Ycut])/3 + 
       c[VR]*((Sqrt[m2]*(385 + 100*m2 - 576*m2^2 + 92*m2^3 - m2^4 - 
            496*MBhat + 448*m2*MBhat + 64*m2^3*MBhat - 16*m2^4*MBhat + 
            150*MBhat^2 - 234*m2*MBhat^2 + 90*m2^2*MBhat^2 - 6*m2^3*MBhat^2 - 
            2045*Ycut - 1088*m2*Ycut + 2772*m2^2*Ycut - 424*m2^3*Ycut + 
            5*m2^4*Ycut + 2672*MBhat*Ycut - 1856*m2*MBhat*Ycut - 
            320*m2^3*MBhat*Ycut + 80*m2^4*MBhat*Ycut - 822*MBhat^2*Ycut + 
            1134*m2*MBhat^2*Ycut - 414*m2^2*MBhat^2*Ycut + 
            30*m2^3*MBhat^2*Ycut + 4390*Ycut^2 + 3646*m2*Ycut^2 - 
            5274*m2^2*Ycut^2 + 758*m2^3*Ycut^2 - 10*m2^4*Ycut^2 - 
            5824*MBhat*Ycut^2 + 2752*m2*MBhat*Ycut^2 + 640*m2^3*MBhat*
             Ycut^2 - 160*m2^4*MBhat*Ycut^2 + 1824*MBhat^2*Ycut^2 - 
            2178*m2*MBhat^2*Ycut^2 + 738*m2^2*MBhat^2*Ycut^2 - 
            60*m2^3*MBhat^2*Ycut^2 - 4790*Ycut^3 - 5606*m2*Ycut^3 + 
            4914*m2^2*Ycut^3 - 638*m2^3*Ycut^3 + 10*m2^4*Ycut^3 + 
            6434*MBhat*Ycut^3 - 1462*m2*MBhat*Ycut^3 + 70*m2^2*MBhat*Ycut^3 - 
            690*m2^3*MBhat*Ycut^3 + 160*m2^4*MBhat*Ycut^3 - 
            2048*MBhat^2*Ycut^3 + 2058*m2*MBhat^2*Ycut^3 - 634*m2^2*MBhat^2*
             Ycut^3 + 60*m2^3*MBhat^2*Ycut^3 + 2704*Ycut^4 + 4280*m2*Ycut^4 - 
            2228*m2^2*Ycut^4 + 254*m2^3*Ycut^4 - 5*m2^4*Ycut^4 - 
            3562*MBhat*Ycut^4 - 370*m2*MBhat*Ycut^4 - 46*m2^2*MBhat*Ycut^4 + 
            362*m2^3*MBhat*Ycut^4 - 80*m2^4*MBhat*Ycut^4 + 
            1126*MBhat^2*Ycut^4 - 886*m2*MBhat^2*Ycut^4 + 224*m2^2*MBhat^2*
             Ycut^4 - 30*m2^3*MBhat^2*Ycut^4 - 704*Ycut^5 - 1420*m2*Ycut^5 + 
            352*m2^2*Ycut^5 - 10*m2^3*Ycut^5 + m2^4*Ycut^5 + 
            644*MBhat*Ycut^5 + 788*m2*MBhat*Ycut^5 - 106*m2^2*MBhat*Ycut^5 - 
            70*m2^3*MBhat*Ycut^5 + 16*m2^4*MBhat*Ycut^5 - 
            134*MBhat^2*Ycut^5 + 2*m2*MBhat^2*Ycut^5 + 20*m2^2*MBhat^2*
             Ycut^5 + 6*m2^3*MBhat^2*Ycut^5 + 106*Ycut^6 + 6*m2*Ycut^6 + 
            70*m2^2*Ycut^6 - 16*m2^3*Ycut^6 + 236*MBhat*Ycut^6 - 
            388*m2*MBhat*Ycut^6 + 106*m2^2*MBhat*Ycut^6 - 
            2*m2^3*MBhat*Ycut^6 - 172*MBhat^2*Ycut^6 + 146*m2*MBhat^2*
             Ycut^6 - 24*m2^2*MBhat^2*Ycut^6 - 74*Ycut^7 + 98*m2*Ycut^7 - 
            30*m2^2*Ycut^7 - 118*MBhat*Ycut^7 + 98*m2*MBhat*Ycut^7 - 
            24*m2^2*MBhat*Ycut^7 + 92*MBhat^2*Ycut^7 - 42*m2*MBhat^2*Ycut^7 + 
            31*Ycut^8 - 16*m2*Ycut^8 + 14*MBhat*Ycut^8 - 10*m2*MBhat*Ycut^8 - 
            16*MBhat^2*Ycut^8 - 3*Ycut^9))/(3*(-1 + Ycut)^5) + 
         4*Sqrt[m2]*(-10 - 49*m2 - 9*m2^2 + 3*m2^3 + 16*MBhat + 32*m2*MBhat - 
           6*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2] - 
         4*Sqrt[m2]*(-10 - 49*m2 - 9*m2^2 + 3*m2^3 + 16*MBhat + 32*m2*MBhat - 
           6*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[1 - Ycut]))) + 
   rhoLS*(-1/60*((-1 + m2 + Ycut)*(-75 - 110*m2 - 110*m2^2 - 350*m2^3 + 
         45*m2^4 + 168*MBhat - 672*m2*MBhat + 1008*m2^2*MBhat - 
         672*m2^3*MBhat + 168*m2^4*MBhat - 90*MBhat^2 + 150*m2*MBhat^2 - 
         570*m2^2*MBhat^2 + 150*m2^3*MBhat^2 + 300*Ycut + 605*m2*Ycut + 
         315*m2^2*Ycut + 1705*m2^3*Ycut - 225*m2^4*Ycut - 672*MBhat*Ycut + 
         2856*m2*MBhat*Ycut - 4536*m2^2*MBhat*Ycut + 3192*m2^3*MBhat*Ycut - 
         840*m2^4*MBhat*Ycut + 360*MBhat^2*Ycut - 690*m2*MBhat^2*Ycut + 
         2700*m2^2*MBhat^2*Ycut - 750*m2^3*MBhat^2*Ycut - 450*Ycut^2 - 
         1275*m2*Ycut^2 - 150*m2^2*Ycut^2 - 3275*m2^3*Ycut^2 + 
         450*m2^4*Ycut^2 + 1008*MBhat*Ycut^2 - 4536*m2*MBhat*Ycut^2 + 
         7728*m2^2*MBhat*Ycut^2 - 5880*m2^3*MBhat*Ycut^2 + 
         1680*m2^4*MBhat*Ycut^2 - 540*MBhat^2*Ycut^2 + 1170*m2*MBhat^2*
          Ycut^2 - 4950*m2^2*MBhat^2*Ycut^2 + 1500*m2^3*MBhat^2*Ycut^2 + 
         300*Ycut^3 + 1255*m2*Ycut^3 - 305*m2^2*Ycut^3 + 3050*m2^3*Ycut^3 - 
         450*m2^4*Ycut^3 - 732*MBhat*Ycut^3 + 3052*m2*MBhat*Ycut^3 - 
         6380*m2^2*MBhat*Ycut^3 + 5740*m2^3*MBhat*Ycut^3 - 
         1680*m2^4*MBhat*Ycut^3 + 360*MBhat^2*Ycut^3 - 
         870*m2*MBhat^2*Ycut^3 + 4680*m2^2*MBhat^2*Ycut^3 - 
         1500*m2^3*MBhat^2*Ycut^3 - 30*Ycut^4 - 555*m2*Ycut^4 + 
         640*m2^2*Ycut^4 - 1650*m2^3*Ycut^4 + 225*m2^4*Ycut^4 + 
         408*MBhat*Ycut^4 - 340*m2*MBhat*Ycut^4 + 2160*m2^2*MBhat*Ycut^4 - 
         2780*m2^3*MBhat*Ycut^4 + 840*m2^4*MBhat*Ycut^4 - 
         100*MBhat^2*Ycut^4 + 230*m2*MBhat^2*Ycut^4 - 2350*m2^2*MBhat^2*
          Ycut^4 + 750*m2^3*MBhat^2*Ycut^4 - 180*Ycut^5 + 125*m2*Ycut^5 - 
         555*m2^2*Ycut^5 + 435*m2^3*Ycut^5 - 45*m2^4*Ycut^5 - 
         358*MBhat*Ycut^5 - 658*m2*MBhat*Ycut^5 + 362*m2^2*MBhat*Ycut^5 + 
         502*m2^3*MBhat*Ycut^5 - 168*m2^4*MBhat*Ycut^5 + 40*MBhat^2*Ycut^5 + 
         30*m2*MBhat^2*Ycut^5 + 500*m2^2*MBhat^2*Ycut^5 - 
         150*m2^3*MBhat^2*Ycut^5 + 270*Ycut^6 - 95*m2*Ycut^6 + 
         160*m2^2*Ycut^6 - 35*m2^3*Ycut^6 + 232*MBhat*Ycut^6 + 
         374*m2*MBhat*Ycut^6 - 404*m2^2*MBhat*Ycut^6 + 18*m2^3*MBhat*Ycut^6 - 
         60*MBhat^2*Ycut^6 - 30*m2*MBhat^2*Ycut^6 - 10*m2^2*MBhat^2*Ycut^6 - 
         180*Ycut^7 + 55*m2*Ycut^7 + 5*m2^2*Ycut^7 - 48*MBhat*Ycut^7 - 
         74*m2*MBhat*Ycut^7 + 62*m2^2*MBhat*Ycut^7 + 40*MBhat^2*Ycut^7 + 
         10*m2*MBhat^2*Ycut^7 + 45*Ycut^8 - 5*m2*Ycut^8 - 8*MBhat*Ycut^8 - 
         2*m2*MBhat*Ycut^8 - 10*MBhat^2*Ycut^8 + 2*MBhat*Ycut^9))/
       (-1 + Ycut)^5 + (8*Sqrt[m2]*Ycut^3*(-1 + m2 + Ycut)*
       (2*MBhat - 2*m2*MBhat - MBhat^2 - Ycut + m2*Ycut - 2*MBhat*Ycut + 
        m2*MBhat*Ycut + MBhat^2*Ycut + Ycut^2)*c[SR]*c[T])/(-1 + Ycut)^3 + 
     m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[m2] - 
     m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[1 - Ycut] + 
     c[SL]^2*(-1/120*((-1 + m2 + Ycut)*(150 + 3060*m2 + 660*m2^2 - 300*m2^3 + 
           30*m2^4 - 288*MBhat - 1848*m2*MBhat + 1032*m2^2*MBhat - 
           408*m2^3*MBhat + 72*m2^4*MBhat + 195*MBhat^2 + 675*m2*MBhat^2 - 
           405*m2^2*MBhat^2 + 75*m2^3*MBhat^2 - 600*Ycut - 13530*m2*Ycut - 
           3390*m2^2*Ycut + 1470*m2^3*Ycut - 150*m2^4*Ycut + 
           1152*MBhat*Ycut + 8544*m2*MBhat*Ycut - 4824*m2^2*MBhat*Ycut + 
           1968*m2^3*MBhat*Ycut - 360*m2^4*MBhat*Ycut - 780*MBhat^2*Ycut - 
           3225*m2*MBhat^2*Ycut + 1950*m2^2*MBhat^2*Ycut - 
           375*m2^3*MBhat^2*Ycut + 900*Ycut^2 + 22950*m2*Ycut^2 + 
           6900*m2^2*Ycut^2 - 2850*m2^3*Ycut^2 + 300*m2^4*Ycut^2 - 
           1728*MBhat*Ycut^2 - 15264*m2*MBhat*Ycut^2 + 8712*m2^2*MBhat*
            Ycut^2 - 3720*m2^3*MBhat*Ycut^2 + 720*m2^4*MBhat*Ycut^2 + 
           1170*MBhat^2*Ycut^2 + 5985*m2*MBhat^2*Ycut^2 - 3675*m2^2*MBhat^2*
            Ycut^2 + 750*m2^3*MBhat^2*Ycut^2 - 600*Ycut^3 - 18030*m2*Ycut^3 - 
           6870*m2^2*Ycut^3 + 2700*m2^3*Ycut^3 - 300*m2^4*Ycut^3 + 
           1102*MBhat*Ycut^3 + 13218*m2*MBhat*Ycut^3 - 8070*m2^2*MBhat*
            Ycut^3 + 3710*m2^3*MBhat*Ycut^3 - 720*m2^4*MBhat*Ycut^3 - 
           780*MBhat^2*Ycut^3 - 5475*m2*MBhat^2*Ycut^3 + 3540*m2^2*MBhat^2*
            Ycut^3 - 750*m2^3*MBhat^2*Ycut^3 + 175*Ycut^4 + 5685*m2*Ycut^4 + 
           3585*m2^2*Ycut^4 - 1375*m2^3*Ycut^4 + 150*m2^4*Ycut^4 - 
           88*MBhat*Ycut^4 - 5570*m2*MBhat*Ycut^4 + 3880*m2^2*MBhat*Ycut^4 - 
           1870*m2^3*MBhat*Ycut^4 + 360*m2^4*MBhat*Ycut^4 + 
           190*MBhat^2*Ycut^4 + 2515*m2*MBhat^2*Ycut^4 - 1775*m2^2*MBhat^2*
            Ycut^4 + 375*m2^3*MBhat^2*Ycut^4 - 100*Ycut^5 + 205*m2*Ycut^5 - 
           980*m2^2*Ycut^5 + 305*m2^3*Ycut^5 - 30*m2^4*Ycut^5 - 
           297*MBhat*Ycut^5 + 933*m2*MBhat*Ycut^5 - 707*m2^2*MBhat*Ycut^5 + 
           383*m2^3*MBhat*Ycut^5 - 72*m2^4*MBhat*Ycut^5 + 20*MBhat^2*Ycut^5 - 
           465*m2*MBhat^2*Ycut^5 + 370*m2^2*MBhat^2*Ycut^5 - 
           75*m2^3*MBhat^2*Ycut^5 + 150*Ycut^6 - 345*m2*Ycut^6 + 
           85*m2^2*Ycut^6 - 10*m2^3*Ycut^6 + 188*MBhat*Ycut^6 - 
           39*m2*MBhat*Ycut^6 - 26*m2^2*MBhat*Ycut^6 - 3*m2^3*MBhat*Ycut^6 - 
           30*MBhat^2*Ycut^6 - 15*m2*MBhat^2*Ycut^6 - 5*m2^2*MBhat^2*Ycut^6 - 
           100*Ycut^7 + 15*m2*Ycut^7 + 10*m2^2*Ycut^7 - 32*MBhat*Ycut^7 + 
           29*m2*MBhat*Ycut^7 + 3*m2^2*MBhat*Ycut^7 + 20*MBhat^2*Ycut^7 + 
           5*m2*MBhat^2*Ycut^7 + 25*Ycut^8 - 10*m2*Ycut^8 - 12*MBhat*Ycut^8 - 
           3*m2*MBhat*Ycut^8 - 5*MBhat^2*Ycut^8 + 3*MBhat*Ycut^9))/
         (-1 + Ycut)^5 + (3*m2*(-8 - 14*m2 + 2*m2^2 + 8*MBhat - 4*MBhat^2 + 
          m2*MBhat^2)*Log[m2])/2 - (3*m2*(-8 - 14*m2 + 2*m2^2 + 8*MBhat - 
          4*MBhat^2 + m2*MBhat^2)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/120*((-1 + m2 + Ycut)*(150 + 3060*m2 + 660*m2^2 - 300*m2^3 + 
           30*m2^4 - 288*MBhat - 1848*m2*MBhat + 1032*m2^2*MBhat - 
           408*m2^3*MBhat + 72*m2^4*MBhat + 195*MBhat^2 + 675*m2*MBhat^2 - 
           405*m2^2*MBhat^2 + 75*m2^3*MBhat^2 - 600*Ycut - 13530*m2*Ycut - 
           3390*m2^2*Ycut + 1470*m2^3*Ycut - 150*m2^4*Ycut + 
           1152*MBhat*Ycut + 8544*m2*MBhat*Ycut - 4824*m2^2*MBhat*Ycut + 
           1968*m2^3*MBhat*Ycut - 360*m2^4*MBhat*Ycut - 780*MBhat^2*Ycut - 
           3225*m2*MBhat^2*Ycut + 1950*m2^2*MBhat^2*Ycut - 
           375*m2^3*MBhat^2*Ycut + 900*Ycut^2 + 22950*m2*Ycut^2 + 
           6900*m2^2*Ycut^2 - 2850*m2^3*Ycut^2 + 300*m2^4*Ycut^2 - 
           1728*MBhat*Ycut^2 - 15264*m2*MBhat*Ycut^2 + 8712*m2^2*MBhat*
            Ycut^2 - 3720*m2^3*MBhat*Ycut^2 + 720*m2^4*MBhat*Ycut^2 + 
           1170*MBhat^2*Ycut^2 + 5985*m2*MBhat^2*Ycut^2 - 3675*m2^2*MBhat^2*
            Ycut^2 + 750*m2^3*MBhat^2*Ycut^2 - 600*Ycut^3 - 18030*m2*Ycut^3 - 
           6870*m2^2*Ycut^3 + 2700*m2^3*Ycut^3 - 300*m2^4*Ycut^3 + 
           1102*MBhat*Ycut^3 + 13218*m2*MBhat*Ycut^3 - 8070*m2^2*MBhat*
            Ycut^3 + 3710*m2^3*MBhat*Ycut^3 - 720*m2^4*MBhat*Ycut^3 - 
           780*MBhat^2*Ycut^3 - 5475*m2*MBhat^2*Ycut^3 + 3540*m2^2*MBhat^2*
            Ycut^3 - 750*m2^3*MBhat^2*Ycut^3 + 175*Ycut^4 + 5685*m2*Ycut^4 + 
           3585*m2^2*Ycut^4 - 1375*m2^3*Ycut^4 + 150*m2^4*Ycut^4 - 
           88*MBhat*Ycut^4 - 5570*m2*MBhat*Ycut^4 + 3880*m2^2*MBhat*Ycut^4 - 
           1870*m2^3*MBhat*Ycut^4 + 360*m2^4*MBhat*Ycut^4 + 
           190*MBhat^2*Ycut^4 + 2515*m2*MBhat^2*Ycut^4 - 1775*m2^2*MBhat^2*
            Ycut^4 + 375*m2^3*MBhat^2*Ycut^4 - 100*Ycut^5 + 205*m2*Ycut^5 - 
           980*m2^2*Ycut^5 + 305*m2^3*Ycut^5 - 30*m2^4*Ycut^5 - 
           297*MBhat*Ycut^5 + 933*m2*MBhat*Ycut^5 - 707*m2^2*MBhat*Ycut^5 + 
           383*m2^3*MBhat*Ycut^5 - 72*m2^4*MBhat*Ycut^5 + 20*MBhat^2*Ycut^5 - 
           465*m2*MBhat^2*Ycut^5 + 370*m2^2*MBhat^2*Ycut^5 - 
           75*m2^3*MBhat^2*Ycut^5 + 150*Ycut^6 - 345*m2*Ycut^6 + 
           85*m2^2*Ycut^6 - 10*m2^3*Ycut^6 + 188*MBhat*Ycut^6 - 
           39*m2*MBhat*Ycut^6 - 26*m2^2*MBhat*Ycut^6 - 3*m2^3*MBhat*Ycut^6 - 
           30*MBhat^2*Ycut^6 - 15*m2*MBhat^2*Ycut^6 - 5*m2^2*MBhat^2*Ycut^6 - 
           100*Ycut^7 + 15*m2*Ycut^7 + 10*m2^2*Ycut^7 - 32*MBhat*Ycut^7 + 
           29*m2*MBhat*Ycut^7 + 3*m2^2*MBhat*Ycut^7 + 20*MBhat^2*Ycut^7 + 
           5*m2*MBhat^2*Ycut^7 + 25*Ycut^8 - 10*m2*Ycut^8 - 12*MBhat*Ycut^8 - 
           3*m2*MBhat*Ycut^8 - 5*MBhat^2*Ycut^8 + 3*MBhat*Ycut^9))/
         (-1 + Ycut)^5 + (3*m2*(-8 - 14*m2 + 2*m2^2 + 8*MBhat - 4*MBhat^2 + 
          m2*MBhat^2)*Log[m2])/2 - (3*m2*(-8 - 14*m2 + 2*m2^2 + 8*MBhat - 
          4*MBhat^2 + m2*MBhat^2)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(75 + 110*m2 + 110*m2^2 + 350*m2^3 - 
          45*m2^4 - 168*MBhat + 672*m2*MBhat - 1008*m2^2*MBhat + 
          672*m2^3*MBhat - 168*m2^4*MBhat + 90*MBhat^2 - 150*m2*MBhat^2 + 
          570*m2^2*MBhat^2 - 150*m2^3*MBhat^2 - 150*Ycut - 385*m2*Ycut - 
          95*m2^2*Ycut - 1005*m2^3*Ycut + 135*m2^4*Ycut + 336*MBhat*Ycut - 
          1512*m2*MBhat*Ycut + 2520*m2^2*MBhat*Ycut - 1848*m2^3*MBhat*Ycut + 
          504*m2^4*MBhat*Ycut - 180*MBhat^2*Ycut + 390*m2*MBhat^2*Ycut - 
          1560*m2^2*MBhat^2*Ycut + 450*m2^3*MBhat^2*Ycut + 75*Ycut^2 + 
          395*m2*Ycut^2 - 150*m2^2*Ycut^2 + 915*m2^3*Ycut^2 - 
          135*m2^4*Ycut^2 - 168*MBhat*Ycut^2 + 840*m2*MBhat*Ycut^2 - 
          1680*m2^2*MBhat*Ycut^2 + 1512*m2^3*MBhat*Ycut^2 - 
          504*m2^4*MBhat*Ycut^2 + 90*MBhat^2*Ycut^2 - 240*m2*MBhat^2*Ycut^2 + 
          1260*m2^2*MBhat^2*Ycut^2 - 450*m2^3*MBhat^2*Ycut^2 - 80*m2*Ycut^3 + 
          100*m2^2*Ycut^3 - 215*m2^3*Ycut^3 + 45*m2^4*Ycut^3 - 
          168*m2^3*MBhat*Ycut^3 + 168*m2^4*MBhat*Ycut^3 - 
          120*m2^2*MBhat^2*Ycut^3 + 150*m2^3*MBhat^2*Ycut^3 - 15*Ycut^4 + 
          70*m2*Ycut^4 - 40*m2^2*Ycut^4 - 45*m2^3*Ycut^4 + 
          360*m2^2*MBhat*Ycut^4 - 168*m2^3*MBhat*Ycut^4 + 30*MBhat^2*Ycut^4 + 
          30*m2*MBhat^2*Ycut^4 - 150*m2^2*MBhat^2*Ycut^4 + 30*Ycut^5 - 
          155*m2*Ycut^5 - 45*m2^2*Ycut^5 - 12*MBhat*Ycut^5 - 
          12*m2*MBhat*Ycut^5 - 72*m2^2*MBhat*Ycut^5 - 60*MBhat^2*Ycut^5 - 
          30*m2*MBhat^2*Ycut^5 - 15*Ycut^6 + 45*m2*Ycut^6 + 24*MBhat*Ycut^6 + 
          12*m2*MBhat*Ycut^6 + 30*MBhat^2*Ycut^6 - 12*MBhat*Ycut^7))/
        (60*(-1 + Ycut)^3) + m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[m2] - 
       m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[1 - Ycut]) + 
     c[VL]^2*(-1/60*((-1 + m2 + Ycut)*(-75 - 110*m2 - 110*m2^2 - 350*m2^3 + 
           45*m2^4 + 168*MBhat - 672*m2*MBhat + 1008*m2^2*MBhat - 
           672*m2^3*MBhat + 168*m2^4*MBhat - 90*MBhat^2 + 150*m2*MBhat^2 - 
           570*m2^2*MBhat^2 + 150*m2^3*MBhat^2 + 300*Ycut + 605*m2*Ycut + 
           315*m2^2*Ycut + 1705*m2^3*Ycut - 225*m2^4*Ycut - 672*MBhat*Ycut + 
           2856*m2*MBhat*Ycut - 4536*m2^2*MBhat*Ycut + 3192*m2^3*MBhat*Ycut - 
           840*m2^4*MBhat*Ycut + 360*MBhat^2*Ycut - 690*m2*MBhat^2*Ycut + 
           2700*m2^2*MBhat^2*Ycut - 750*m2^3*MBhat^2*Ycut - 450*Ycut^2 - 
           1275*m2*Ycut^2 - 150*m2^2*Ycut^2 - 3275*m2^3*Ycut^2 + 
           450*m2^4*Ycut^2 + 1008*MBhat*Ycut^2 - 4536*m2*MBhat*Ycut^2 + 
           7728*m2^2*MBhat*Ycut^2 - 5880*m2^3*MBhat*Ycut^2 + 
           1680*m2^4*MBhat*Ycut^2 - 540*MBhat^2*Ycut^2 + 1170*m2*MBhat^2*
            Ycut^2 - 4950*m2^2*MBhat^2*Ycut^2 + 1500*m2^3*MBhat^2*Ycut^2 + 
           300*Ycut^3 + 1255*m2*Ycut^3 - 305*m2^2*Ycut^3 + 3050*m2^3*Ycut^3 - 
           450*m2^4*Ycut^3 - 732*MBhat*Ycut^3 + 3052*m2*MBhat*Ycut^3 - 
           6380*m2^2*MBhat*Ycut^3 + 5740*m2^3*MBhat*Ycut^3 - 
           1680*m2^4*MBhat*Ycut^3 + 360*MBhat^2*Ycut^3 - 870*m2*MBhat^2*
            Ycut^3 + 4680*m2^2*MBhat^2*Ycut^3 - 1500*m2^3*MBhat^2*Ycut^3 - 
           30*Ycut^4 - 555*m2*Ycut^4 + 640*m2^2*Ycut^4 - 1650*m2^3*Ycut^4 + 
           225*m2^4*Ycut^4 + 408*MBhat*Ycut^4 - 340*m2*MBhat*Ycut^4 + 
           2160*m2^2*MBhat*Ycut^4 - 2780*m2^3*MBhat*Ycut^4 + 
           840*m2^4*MBhat*Ycut^4 - 100*MBhat^2*Ycut^4 + 230*m2*MBhat^2*
            Ycut^4 - 2350*m2^2*MBhat^2*Ycut^4 + 750*m2^3*MBhat^2*Ycut^4 - 
           180*Ycut^5 + 125*m2*Ycut^5 - 555*m2^2*Ycut^5 + 435*m2^3*Ycut^5 - 
           45*m2^4*Ycut^5 - 358*MBhat*Ycut^5 - 658*m2*MBhat*Ycut^5 + 
           362*m2^2*MBhat*Ycut^5 + 502*m2^3*MBhat*Ycut^5 - 
           168*m2^4*MBhat*Ycut^5 + 40*MBhat^2*Ycut^5 + 30*m2*MBhat^2*Ycut^5 + 
           500*m2^2*MBhat^2*Ycut^5 - 150*m2^3*MBhat^2*Ycut^5 + 270*Ycut^6 - 
           95*m2*Ycut^6 + 160*m2^2*Ycut^6 - 35*m2^3*Ycut^6 + 
           232*MBhat*Ycut^6 + 374*m2*MBhat*Ycut^6 - 404*m2^2*MBhat*Ycut^6 + 
           18*m2^3*MBhat*Ycut^6 - 60*MBhat^2*Ycut^6 - 30*m2*MBhat^2*Ycut^6 - 
           10*m2^2*MBhat^2*Ycut^6 - 180*Ycut^7 + 55*m2*Ycut^7 + 
           5*m2^2*Ycut^7 - 48*MBhat*Ycut^7 - 74*m2*MBhat*Ycut^7 + 
           62*m2^2*MBhat*Ycut^7 + 40*MBhat^2*Ycut^7 + 10*m2*MBhat^2*Ycut^7 + 
           45*Ycut^8 - 5*m2*Ycut^8 - 8*MBhat*Ycut^8 - 2*m2*MBhat*Ycut^8 - 
           10*MBhat^2*Ycut^8 + 2*MBhat*Ycut^9))/(-1 + Ycut)^5 + 
       m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[m2] - 
       m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[1 - Ycut]) + 
     c[T]^2*((-2*(-1 + m2 + Ycut)*(-180 - 1400*m2 - 440*m2^2 - 440*m2^3 + 
          60*m2^4 + 624*MBhat + 504*m2*MBhat + 984*m2^2*MBhat - 
          936*m2^3*MBhat + 264*m2^4*MBhat - 375*MBhat^2 - 375*m2*MBhat^2 - 
          735*m2^2*MBhat^2 + 225*m2^3*MBhat^2 + 720*Ycut + 6380*m2*Ycut + 
          1860*m2^2*Ycut + 2140*m2^3*Ycut - 300*m2^4*Ycut - 2496*MBhat*Ycut - 
          2832*m2*MBhat*Ycut - 4248*m2^2*MBhat*Ycut + 4416*m2^3*MBhat*Ycut - 
          1320*m2^4*MBhat*Ycut + 1500*MBhat^2*Ycut + 1845*m2*MBhat^2*Ycut + 
          3450*m2^2*MBhat^2*Ycut - 1125*m2^3*MBhat^2*Ycut - 1080*Ycut^2 - 
          11220*m2*Ycut^2 - 3000*m2^2*Ycut^2 - 4100*m2^3*Ycut^2 + 
          600*m2^4*Ycut^2 + 3744*MBhat*Ycut^2 + 6192*m2*MBhat*Ycut^2 + 
          6744*m2^2*MBhat*Ycut^2 - 8040*m2^3*MBhat*Ycut^2 + 
          2640*m2^4*MBhat*Ycut^2 - 2250*MBhat^2*Ycut^2 - 
          3645*m2*MBhat^2*Ycut^2 - 6225*m2^2*MBhat^2*Ycut^2 + 
          2250*m2^3*MBhat^2*Ycut^2 + 720*Ycut^3 + 9220*m2*Ycut^3 + 
          2260*m2^2*Ycut^3 + 3800*m2^3*Ycut^3 - 600*m2^4*Ycut^3 - 
          2506*MBhat*Ycut^3 - 6974*m2*MBhat*Ycut^3 - 4190*m2^2*MBhat*Ycut^3 + 
          7070*m2^3*MBhat*Ycut^3 - 2640*m2^4*MBhat*Ycut^3 + 
          1500*MBhat^2*Ycut^3 + 3735*m2*MBhat^2*Ycut^3 + 5340*m2^2*MBhat^2*
           Ycut^3 - 2250*m2^3*MBhat^2*Ycut^3 - 265*Ycut^4 - 2985*m2*Ycut^4 - 
          755*m2^2*Ycut^4 - 1775*m2^3*Ycut^4 + 300*m2^4*Ycut^4 + 
          664*MBhat*Ycut^4 + 4390*m2*MBhat*Ycut^4 - 400*m2^2*MBhat*Ycut^4 - 
          2590*m2^3*MBhat*Ycut^4 + 1320*m2^4*MBhat*Ycut^4 - 
          410*MBhat^2*Ycut^4 - 2075*m2*MBhat^2*Ycut^4 - 1925*m2^2*MBhat^2*
           Ycut^4 + 1125*m2^3*MBhat^2*Ycut^4 + 340*Ycut^5 - 305*m2*Ycut^5 + 
          50*m2^2*Ycut^5 + 295*m2^3*Ycut^5 - 60*m2^4*Ycut^5 - 
          49*MBhat*Ycut^5 - 1579*m2*MBhat*Ycut^5 + 1861*m2^2*MBhat*Ycut^5 - 
          49*m2^3*MBhat*Ycut^5 - 264*m2^4*MBhat*Ycut^5 + 140*MBhat^2*Ycut^5 + 
          585*m2*MBhat^2*Ycut^5 - 50*m2^2*MBhat^2*Ycut^5 - 
          225*m2^3*MBhat^2*Ycut^5 - 510*Ycut^6 + 265*m2*Ycut^6 - 
          15*m2^2*Ycut^6 + 20*m2^3*Ycut^6 - 4*MBhat*Ycut^6 + 
          377*m2*MBhat*Ycut^6 - 882*m2^2*MBhat*Ycut^6 + 189*m2^3*MBhat*
           Ycut^6 - 210*MBhat^2*Ycut^6 - 105*m2*MBhat^2*Ycut^6 + 
          145*m2^2*MBhat^2*Ycut^6 + 340*Ycut^7 + 85*m2*Ycut^7 + 
          40*m2^2*Ycut^7 + 56*MBhat*Ycut^7 - 67*m2*MBhat*Ycut^7 + 
          131*m2^2*MBhat*Ycut^7 + 140*MBhat^2*Ycut^7 + 35*m2*MBhat^2*Ycut^7 - 
          85*Ycut^8 - 40*m2*Ycut^8 - 44*MBhat*Ycut^8 - 11*m2*MBhat*Ycut^8 - 
          35*MBhat^2*Ycut^8 + 11*MBhat*Ycut^9))/(15*(-1 + Ycut)^5) + 
       8*m2*(16 + 12*m2 + 12*m2^2 - 24*MBhat + 12*MBhat^2 + 9*m2*MBhat^2)*
        Log[m2] - 8*m2*(16 + 12*m2 + 12*m2^2 - 24*MBhat + 12*MBhat^2 + 
         9*m2*MBhat^2)*Log[1 - Ycut]) + 
     c[VR]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*(-59 + 137*m2 - 7*m2^2 - 
           11*m2^3 + 64*MBhat - 248*m2*MBhat + 112*m2^2*MBhat - 
           24*m2^3*MBhat - 26*MBhat^2 + 28*m2*MBhat^2 - 26*m2^2*MBhat^2 + 
           142*Ycut - 321*m2*Ycut - 4*m2^2*Ycut + 33*m2^3*Ycut - 
           176*MBhat*Ycut + 656*m2*MBhat*Ycut - 312*m2^2*MBhat*Ycut + 
           72*m2^3*MBhat*Ycut + 76*MBhat^2*Ycut - 94*m2*MBhat^2*Ycut + 
           78*m2^2*MBhat^2*Ycut - 95*Ycut^2 + 202*m2*Ycut^2 + 
           36*m2^2*Ycut^2 - 33*m2^3*Ycut^2 + 136*MBhat*Ycut^2 - 
           504*m2*MBhat*Ycut^2 + 264*m2^2*MBhat*Ycut^2 - 72*m2^3*MBhat*
            Ycut^2 - 62*MBhat^2*Ycut^2 + 96*m2*MBhat^2*Ycut^2 - 
           78*m2^2*MBhat^2*Ycut^2 + 8*Ycut^3 - 8*m2*Ycut^3 - 26*m2^2*Ycut^3 + 
           11*m2^3*Ycut^3 + 2*MBhat*Ycut^3 + 68*m2*MBhat*Ycut^3 - 
           70*m2^2*MBhat*Ycut^3 + 24*m2^3*MBhat*Ycut^3 + 8*MBhat^2*Ycut^3 - 
           40*m2*MBhat^2*Ycut^3 + 26*m2^2*MBhat^2*Ycut^3 - Ycut^4 - 
           15*m2*Ycut^4 + 13*m2^2*Ycut^4 - 28*MBhat*Ycut^4 + 
           34*m2*MBhat*Ycut^4 - 6*m2^2*MBhat*Ycut^4 - 4*MBhat^2*Ycut^4 + 
           10*m2*MBhat^2*Ycut^4 + 2*Ycut^5 + 5*m2*Ycut^5 + 2*MBhat*Ycut^5 - 
           6*m2*MBhat*Ycut^5 + 8*MBhat^2*Ycut^5 + 3*Ycut^6))/(-1 + Ycut)^3 + 
       4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 12*m2*MBhat + 
         2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2] - 
       4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 12*m2*MBhat + 
         2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[1 - Ycut]) + 
     c[SL]*(-1/6*(Ycut^3*(-1 + m2 + Ycut)*(-6*MBhat - 14*m2*MBhat - 
           50*m2^2*MBhat + 70*m2^3*MBhat + 48*m2^2*MBhat^2 + 3*Ycut + 
           7*m2*Ycut + 25*m2^2*Ycut - 35*m2^3*Ycut + 24*MBhat*Ycut + 
           50*m2*MBhat*Ycut + 84*m2^2*MBhat*Ycut - 110*m2^3*MBhat*Ycut + 
           2*MBhat^2*Ycut + 2*m2*MBhat^2*Ycut - 100*m2^2*MBhat^2*Ycut - 
           12*Ycut^2 - 25*m2*Ycut^2 - 42*m2^2*Ycut^2 + 31*m2^3*Ycut^2 - 
           37*MBhat*Ycut^2 - 67*m2*MBhat*Ycut^2 - 43*m2^2*MBhat*Ycut^2 + 
           67*m2^3*MBhat*Ycut^2 - 8*MBhat^2*Ycut^2 - 6*m2*MBhat^2*Ycut^2 + 
           68*m2^2*MBhat^2*Ycut^2 + 18*Ycut^3 + 33*m2*Ycut^3 + 
           21*m2^2*Ycut^3 - 8*m2^3*Ycut^3 + 28*MBhat*Ycut^3 + 
           41*m2*MBhat*Ycut^3 + 10*m2^2*MBhat*Ycut^3 - 15*m2^3*MBhat*Ycut^3 + 
           12*MBhat^2*Ycut^3 + 6*m2*MBhat^2*Ycut^3 - 16*m2^2*MBhat^2*Ycut^3 - 
           12*Ycut^4 - 19*m2*Ycut^4 - 4*m2^2*Ycut^4 - 12*MBhat*Ycut^4 - 
           11*m2*MBhat*Ycut^4 - m2^2*MBhat*Ycut^4 - 8*MBhat^2*Ycut^4 - 
           2*m2*MBhat^2*Ycut^4 + 3*Ycut^5 + 4*m2*Ycut^5 + 4*MBhat*Ycut^5 + 
           m2*MBhat*Ycut^5 + 2*MBhat^2*Ycut^5 - MBhat*Ycut^6)*c[T])/
         (-1 + Ycut)^5 + c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*
           (85 + 217*m2 + m2^2 - 3*m2^3 - 148*MBhat - 116*m2*MBhat + 
            28*m2^2*MBhat - 4*m2^3*MBhat + 66*MBhat^2 + 12*m2*MBhat^2 - 
            6*m2^2*MBhat^2 - 194*Ycut - 553*m2*Ycut - 12*m2^2*Ycut + 
            9*m2^3*Ycut + 344*MBhat*Ycut + 324*m2*MBhat*Ycut - 
            80*m2^2*MBhat*Ycut + 12*m2^3*MBhat*Ycut - 156*MBhat^2*Ycut - 
            42*m2*MBhat^2*Ycut + 18*m2^2*MBhat^2*Ycut + 121*Ycut^2 + 
            414*m2*Ycut^2 + 24*m2^2*Ycut^2 - 9*m2^3*Ycut^2 - 
            220*MBhat*Ycut^2 - 280*m2*MBhat*Ycut^2 + 72*m2^2*MBhat*Ycut^2 - 
            12*m2^3*MBhat*Ycut^2 + 102*MBhat^2*Ycut^2 + 48*m2*MBhat^2*
             Ycut^2 - 18*m2^2*MBhat^2*Ycut^2 - 8*Ycut^3 - 56*m2*Ycut^3 - 
            14*m2^2*Ycut^3 + 3*m2^3*Ycut^3 + 6*MBhat*Ycut^3 + 
            76*m2*MBhat*Ycut^3 - 26*m2^2*MBhat*Ycut^3 + 4*m2^3*MBhat*Ycut^3 - 
            24*m2*MBhat^2*Ycut^3 + 6*m2^2*MBhat^2*Ycut^3 + 3*Ycut^4 - 
            23*m2*Ycut^4 + 5*m2^2*Ycut^4 + 12*MBhat*Ycut^4 - 
            2*m2*MBhat*Ycut^4 + 2*m2^2*MBhat*Ycut^4 - 12*MBhat^2*Ycut^4 + 
            6*m2*MBhat^2*Ycut^4 - 6*Ycut^5 + m2*Ycut^5 + 6*MBhat*Ycut^5 - 
            2*m2*MBhat*Ycut^5 - Ycut^6))/(2*(-1 + Ycut)^3) - 
         6*Sqrt[m2]*(-2 - 15*m2 - 9*m2^2 + m2^3 + 4*MBhat + 16*m2*MBhat - 
           2*MBhat^2 - 5*m2*MBhat^2 + m2^2*MBhat^2)*Log[m2] + 
         6*Sqrt[m2]*(-2 - 15*m2 - 9*m2^2 + m2^3 + 4*MBhat + 16*m2*MBhat - 
           2*MBhat^2 - 5*m2*MBhat^2 + m2^2*MBhat^2)*Log[1 - Ycut])) + 
     c[VL]*(-1/30*((-1 + m2 + Ycut)*(-75 - 110*m2 - 110*m2^2 - 350*m2^3 + 
           45*m2^4 + 168*MBhat - 672*m2*MBhat + 1008*m2^2*MBhat - 
           672*m2^3*MBhat + 168*m2^4*MBhat - 90*MBhat^2 + 150*m2*MBhat^2 - 
           570*m2^2*MBhat^2 + 150*m2^3*MBhat^2 + 300*Ycut + 605*m2*Ycut + 
           315*m2^2*Ycut + 1705*m2^3*Ycut - 225*m2^4*Ycut - 672*MBhat*Ycut + 
           2856*m2*MBhat*Ycut - 4536*m2^2*MBhat*Ycut + 3192*m2^3*MBhat*Ycut - 
           840*m2^4*MBhat*Ycut + 360*MBhat^2*Ycut - 690*m2*MBhat^2*Ycut + 
           2700*m2^2*MBhat^2*Ycut - 750*m2^3*MBhat^2*Ycut - 450*Ycut^2 - 
           1275*m2*Ycut^2 - 150*m2^2*Ycut^2 - 3275*m2^3*Ycut^2 + 
           450*m2^4*Ycut^2 + 1008*MBhat*Ycut^2 - 4536*m2*MBhat*Ycut^2 + 
           7728*m2^2*MBhat*Ycut^2 - 5880*m2^3*MBhat*Ycut^2 + 
           1680*m2^4*MBhat*Ycut^2 - 540*MBhat^2*Ycut^2 + 1170*m2*MBhat^2*
            Ycut^2 - 4950*m2^2*MBhat^2*Ycut^2 + 1500*m2^3*MBhat^2*Ycut^2 + 
           300*Ycut^3 + 1255*m2*Ycut^3 - 305*m2^2*Ycut^3 + 3050*m2^3*Ycut^3 - 
           450*m2^4*Ycut^3 - 732*MBhat*Ycut^3 + 3052*m2*MBhat*Ycut^3 - 
           6380*m2^2*MBhat*Ycut^3 + 5740*m2^3*MBhat*Ycut^3 - 
           1680*m2^4*MBhat*Ycut^3 + 360*MBhat^2*Ycut^3 - 870*m2*MBhat^2*
            Ycut^3 + 4680*m2^2*MBhat^2*Ycut^3 - 1500*m2^3*MBhat^2*Ycut^3 - 
           30*Ycut^4 - 555*m2*Ycut^4 + 640*m2^2*Ycut^4 - 1650*m2^3*Ycut^4 + 
           225*m2^4*Ycut^4 + 408*MBhat*Ycut^4 - 340*m2*MBhat*Ycut^4 + 
           2160*m2^2*MBhat*Ycut^4 - 2780*m2^3*MBhat*Ycut^4 + 
           840*m2^4*MBhat*Ycut^4 - 100*MBhat^2*Ycut^4 + 230*m2*MBhat^2*
            Ycut^4 - 2350*m2^2*MBhat^2*Ycut^4 + 750*m2^3*MBhat^2*Ycut^4 - 
           180*Ycut^5 + 125*m2*Ycut^5 - 555*m2^2*Ycut^5 + 435*m2^3*Ycut^5 - 
           45*m2^4*Ycut^5 - 358*MBhat*Ycut^5 - 658*m2*MBhat*Ycut^5 + 
           362*m2^2*MBhat*Ycut^5 + 502*m2^3*MBhat*Ycut^5 - 
           168*m2^4*MBhat*Ycut^5 + 40*MBhat^2*Ycut^5 + 30*m2*MBhat^2*Ycut^5 + 
           500*m2^2*MBhat^2*Ycut^5 - 150*m2^3*MBhat^2*Ycut^5 + 270*Ycut^6 - 
           95*m2*Ycut^6 + 160*m2^2*Ycut^6 - 35*m2^3*Ycut^6 + 
           232*MBhat*Ycut^6 + 374*m2*MBhat*Ycut^6 - 404*m2^2*MBhat*Ycut^6 + 
           18*m2^3*MBhat*Ycut^6 - 60*MBhat^2*Ycut^6 - 30*m2*MBhat^2*Ycut^6 - 
           10*m2^2*MBhat^2*Ycut^6 - 180*Ycut^7 + 55*m2*Ycut^7 + 
           5*m2^2*Ycut^7 - 48*MBhat*Ycut^7 - 74*m2*MBhat*Ycut^7 + 
           62*m2^2*MBhat*Ycut^7 + 40*MBhat^2*Ycut^7 + 10*m2*MBhat^2*Ycut^7 + 
           45*Ycut^8 - 5*m2*Ycut^8 - 8*MBhat*Ycut^8 - 2*m2*MBhat*Ycut^8 - 
           10*MBhat^2*Ycut^8 + 2*MBhat*Ycut^9))/(-1 + Ycut)^5 + 
       2*m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[m2] - 
       2*m2*(4 - 3*m2 + 9*m2^2 + 6*m2*MBhat^2)*Log[1 - Ycut] + 
       c[VR]*(-1/3*(Sqrt[m2]*(-1 + m2 + Ycut)*(-59 + 137*m2 - 7*m2^2 - 
             11*m2^3 + 64*MBhat - 248*m2*MBhat + 112*m2^2*MBhat - 
             24*m2^3*MBhat - 26*MBhat^2 + 28*m2*MBhat^2 - 26*m2^2*MBhat^2 + 
             142*Ycut - 321*m2*Ycut - 4*m2^2*Ycut + 33*m2^3*Ycut - 
             176*MBhat*Ycut + 656*m2*MBhat*Ycut - 312*m2^2*MBhat*Ycut + 
             72*m2^3*MBhat*Ycut + 76*MBhat^2*Ycut - 94*m2*MBhat^2*Ycut + 
             78*m2^2*MBhat^2*Ycut - 95*Ycut^2 + 202*m2*Ycut^2 + 
             36*m2^2*Ycut^2 - 33*m2^3*Ycut^2 + 136*MBhat*Ycut^2 - 
             504*m2*MBhat*Ycut^2 + 264*m2^2*MBhat*Ycut^2 - 72*m2^3*MBhat*
              Ycut^2 - 62*MBhat^2*Ycut^2 + 96*m2*MBhat^2*Ycut^2 - 
             78*m2^2*MBhat^2*Ycut^2 + 8*Ycut^3 - 8*m2*Ycut^3 - 
             26*m2^2*Ycut^3 + 11*m2^3*Ycut^3 + 2*MBhat*Ycut^3 + 
             68*m2*MBhat*Ycut^3 - 70*m2^2*MBhat*Ycut^3 + 24*m2^3*MBhat*
              Ycut^3 + 8*MBhat^2*Ycut^3 - 40*m2*MBhat^2*Ycut^3 + 
             26*m2^2*MBhat^2*Ycut^3 - Ycut^4 - 15*m2*Ycut^4 + 
             13*m2^2*Ycut^4 - 28*MBhat*Ycut^4 + 34*m2*MBhat*Ycut^4 - 
             6*m2^2*MBhat*Ycut^4 - 4*MBhat^2*Ycut^4 + 10*m2*MBhat^2*Ycut^4 + 
             2*Ycut^5 + 5*m2*Ycut^5 + 2*MBhat*Ycut^5 - 6*m2*MBhat*Ycut^5 + 
             8*MBhat^2*Ycut^5 + 3*Ycut^6))/(-1 + Ycut)^3 + 
         4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 12*m2*MBhat + 
           2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[m2] - 
         4*Sqrt[m2]*(2 - m2 - 9*m2^2 + 3*m2^3 - 4*MBhat + 12*m2*MBhat + 
           2*MBhat^2 - 3*m2*MBhat^2 + 3*m2^2*MBhat^2)*Log[1 - Ycut]))))/
  mb^3 - (3 - 45*m2 - 240*m2^2 + 240*m2^3 + 45*m2^4 - 3*m2^5 - 13*MBhat + 
   135*m2*MBhat + 160*m2^2*MBhat - 320*m2^3*MBhat + 45*m2^4*MBhat - 
   7*m2^5*MBhat + 10*MBhat^2 - 80*m2*MBhat^2 + 80*m2^3*MBhat^2 - 
   10*m2^4*MBhat^2 - 9*Ycut + 135*m2*Ycut + 900*m2^2*Ycut - 540*m2^3*Ycut - 
   135*m2^4*Ycut + 9*m2^5*Ycut + 39*MBhat*Ycut - 405*m2*MBhat*Ycut - 
   780*m2^2*MBhat*Ycut + 900*m2^3*MBhat*Ycut - 135*m2^4*MBhat*Ycut + 
   21*m2^5*MBhat*Ycut - 30*MBhat^2*Ycut + 240*m2*MBhat^2*Ycut + 
   120*m2^2*MBhat^2*Ycut - 240*m2^3*MBhat^2*Ycut + 30*m2^4*MBhat^2*Ycut + 
   9*Ycut^2 - 135*m2*Ycut^2 - 1170*m2^2*Ycut^2 + 270*m2^3*Ycut^2 + 
   135*m2^4*Ycut^2 - 9*m2^5*Ycut^2 - 39*MBhat*Ycut^2 + 405*m2*MBhat*Ycut^2 + 
   1230*m2^2*MBhat*Ycut^2 - 810*m2^3*MBhat*Ycut^2 + 135*m2^4*MBhat*Ycut^2 - 
   21*m2^5*MBhat*Ycut^2 + 30*MBhat^2*Ycut^2 - 240*m2*MBhat^2*Ycut^2 - 
   300*m2^2*MBhat^2*Ycut^2 + 240*m2^3*MBhat^2*Ycut^2 - 
   30*m2^4*MBhat^2*Ycut^2 - 3*Ycut^3 + 45*m2*Ycut^3 + 570*m2^2*Ycut^3 + 
   90*m2^3*Ycut^3 - 45*m2^4*Ycut^3 + 3*m2^5*Ycut^3 + 33*MBhat*Ycut^3 - 
   175*m2*MBhat*Ycut^3 - 710*m2^2*MBhat*Ycut^3 + 250*m2^3*MBhat*Ycut^3 - 
   65*m2^4*MBhat*Ycut^3 + 7*m2^5*MBhat*Ycut^3 - 30*MBhat^2*Ycut^3 + 
   100*m2*MBhat^2*Ycut^3 + 240*m2^2*MBhat^2*Ycut^3 - 
   100*m2^3*MBhat^2*Ycut^3 + 10*m2^4*MBhat^2*Ycut^3 - 5*Ycut^4 + 
   5*m2*Ycut^4 - 30*m2^2*Ycut^4 - 70*m2^3*Ycut^4 + 10*m2^4*Ycut^4 - 
   65*MBhat*Ycut^4 + 125*m2*MBhat*Ycut^4 + 30*m2^2*MBhat*Ycut^4 - 
   10*m2^3*MBhat*Ycut^4 + 10*m2^4*MBhat*Ycut^4 + 70*MBhat^2*Ycut^4 - 
   60*m2*MBhat^2*Ycut^4 - 60*m2^2*MBhat^2*Ycut^4 + 20*m2^3*MBhat^2*Ycut^4 + 
   17*Ycut^5 - 15*m2*Ycut^5 - 30*m2^2*Ycut^5 + 10*m2^3*Ycut^5 + 
   73*MBhat*Ycut^5 - 135*m2*MBhat*Ycut^5 + 90*m2^2*MBhat*Ycut^5 - 
   10*m2^3*MBhat*Ycut^5 - 90*MBhat^2*Ycut^5 + 60*m2*MBhat^2*Ycut^5 - 
   21*Ycut^6 + 15*m2*Ycut^6 - 29*MBhat*Ycut^6 + 55*m2*MBhat*Ycut^6 - 
   20*m2^2*MBhat*Ycut^6 + 50*MBhat^2*Ycut^6 - 20*m2*MBhat^2*Ycut^6 + 
   11*Ycut^7 - 5*m2*Ycut^7 - MBhat*Ycut^7 - 5*m2*MBhat*Ycut^7 - 
   10*MBhat^2*Ycut^7 - 2*Ycut^8 + 2*MBhat*Ycut^8 + 
   10*api*MBhat^2*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   30*api*MBhat^2*Ycut*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] + 
   30*api*MBhat^2*Ycut^2*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   10*api*MBhat^2*Ycut^3*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   20*api*MBhat*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   60*api*MBhat*Ycut*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] - 
   60*api*MBhat*Ycut^2*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   20*api*MBhat*Ycut^3*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   10*api*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   30*api*Ycut*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] + 
   30*api*Ycut^2*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   10*api*Ycut^3*X1mix[1, 0, SM^2, Ycut, m2, mu2hat])/(10*(-1 + Ycut)^3) + 
 c[SL]^2*(3*m2^2*(1 + m2 - MBhat)*(-2 + MBhat)*Log[m2] - 
   3*m2^2*(1 + m2 - MBhat)*(-2 + MBhat)*Log[1 - Ycut] - 
   (2 - 30*m2 - 160*m2^2 + 160*m2^3 + 30*m2^4 - 2*m2^5 - 7*MBhat + 
     75*m2*MBhat + 120*m2^2*MBhat - 200*m2^3*MBhat + 15*m2^4*MBhat - 
     3*m2^5*MBhat + 5*MBhat^2 - 40*m2*MBhat^2 + 40*m2^3*MBhat^2 - 
     5*m2^4*MBhat^2 - 6*Ycut + 90*m2*Ycut + 600*m2^2*Ycut - 360*m2^3*Ycut - 
     90*m2^4*Ycut + 6*m2^5*Ycut + 21*MBhat*Ycut - 225*m2*MBhat*Ycut - 
     540*m2^2*MBhat*Ycut + 540*m2^3*MBhat*Ycut - 45*m2^4*MBhat*Ycut + 
     9*m2^5*MBhat*Ycut - 15*MBhat^2*Ycut + 120*m2*MBhat^2*Ycut + 
     60*m2^2*MBhat^2*Ycut - 120*m2^3*MBhat^2*Ycut + 15*m2^4*MBhat^2*Ycut + 
     6*Ycut^2 - 90*m2*Ycut^2 - 780*m2^2*Ycut^2 + 180*m2^3*Ycut^2 + 
     90*m2^4*Ycut^2 - 6*m2^5*Ycut^2 - 21*MBhat*Ycut^2 + 225*m2*MBhat*Ycut^2 + 
     810*m2^2*MBhat*Ycut^2 - 450*m2^3*MBhat*Ycut^2 + 45*m2^4*MBhat*Ycut^2 - 
     9*m2^5*MBhat*Ycut^2 + 15*MBhat^2*Ycut^2 - 120*m2*MBhat^2*Ycut^2 - 
     150*m2^2*MBhat^2*Ycut^2 + 120*m2^3*MBhat^2*Ycut^2 - 
     15*m2^4*MBhat^2*Ycut^2 - 2*Ycut^3 + 30*m2*Ycut^3 + 380*m2^2*Ycut^3 + 
     60*m2^3*Ycut^3 - 30*m2^4*Ycut^3 + 2*m2^5*Ycut^3 + 17*MBhat*Ycut^3 - 
     95*m2*MBhat*Ycut^3 - 450*m2^2*MBhat*Ycut^3 + 110*m2^3*MBhat*Ycut^3 - 
     25*m2^4*MBhat*Ycut^3 + 3*m2^5*MBhat*Ycut^3 - 15*MBhat^2*Ycut^3 + 
     50*m2*MBhat^2*Ycut^3 + 120*m2^2*MBhat^2*Ycut^3 - 
     50*m2^3*MBhat^2*Ycut^3 + 5*m2^4*MBhat^2*Ycut^3 - 5*Ycut^4 + 
     10*m2*Ycut^4 - 30*m2^2*Ycut^4 - 40*m2^3*Ycut^4 + 5*m2^4*Ycut^4 - 
     30*MBhat*Ycut^4 + 55*m2*MBhat*Ycut^4 + 30*m2^2*MBhat*Ycut^4 + 
     5*m2^4*MBhat*Ycut^4 + 35*MBhat^2*Ycut^4 - 30*m2*MBhat^2*Ycut^4 - 
     30*m2^2*MBhat^2*Ycut^4 + 10*m2^3*MBhat^2*Ycut^4 + 18*Ycut^5 - 
     30*m2*Ycut^5 + 27*MBhat*Ycut^5 - 45*m2*MBhat*Ycut^5 + 
     30*m2^2*MBhat*Ycut^5 - 45*MBhat^2*Ycut^5 + 30*m2*MBhat^2*Ycut^5 - 
     24*Ycut^6 + 30*m2*Ycut^6 - 10*m2^2*Ycut^6 - MBhat*Ycut^6 + 
     5*m2*MBhat*Ycut^6 + 25*MBhat^2*Ycut^6 - 10*m2*MBhat^2*Ycut^6 + 
     14*Ycut^7 - 10*m2*Ycut^7 - 9*MBhat*Ycut^7 + 5*m2*MBhat*Ycut^7 - 
     5*MBhat^2*Ycut^7 - 3*Ycut^8 + 3*MBhat*Ycut^8 + 
     20*api*MBhat^2*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^2*Ycut*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut^2*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     20*api*MBhat^2*Ycut^3*X1mix[0, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     40*api*MBhat*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     120*api*MBhat*Ycut*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     120*api*MBhat*Ycut^2*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     40*api*MBhat*Ycut^3*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     20*api*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     60*api*Ycut*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     60*api*Ycut^2*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     20*api*Ycut^3*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat])/
    (20*(-1 + Ycut)^3)) + 
 c[SR]^2*(3*m2^2*(1 + m2 - MBhat)*(-2 + MBhat)*Log[m2] - 
   3*m2^2*(1 + m2 - MBhat)*(-2 + MBhat)*Log[1 - Ycut] - 
   (2 - 30*m2 - 160*m2^2 + 160*m2^3 + 30*m2^4 - 2*m2^5 - 7*MBhat + 
     75*m2*MBhat + 120*m2^2*MBhat - 200*m2^3*MBhat + 15*m2^4*MBhat - 
     3*m2^5*MBhat + 5*MBhat^2 - 40*m2*MBhat^2 + 40*m2^3*MBhat^2 - 
     5*m2^4*MBhat^2 - 6*Ycut + 90*m2*Ycut + 600*m2^2*Ycut - 360*m2^3*Ycut - 
     90*m2^4*Ycut + 6*m2^5*Ycut + 21*MBhat*Ycut - 225*m2*MBhat*Ycut - 
     540*m2^2*MBhat*Ycut + 540*m2^3*MBhat*Ycut - 45*m2^4*MBhat*Ycut + 
     9*m2^5*MBhat*Ycut - 15*MBhat^2*Ycut + 120*m2*MBhat^2*Ycut + 
     60*m2^2*MBhat^2*Ycut - 120*m2^3*MBhat^2*Ycut + 15*m2^4*MBhat^2*Ycut + 
     6*Ycut^2 - 90*m2*Ycut^2 - 780*m2^2*Ycut^2 + 180*m2^3*Ycut^2 + 
     90*m2^4*Ycut^2 - 6*m2^5*Ycut^2 - 21*MBhat*Ycut^2 + 225*m2*MBhat*Ycut^2 + 
     810*m2^2*MBhat*Ycut^2 - 450*m2^3*MBhat*Ycut^2 + 45*m2^4*MBhat*Ycut^2 - 
     9*m2^5*MBhat*Ycut^2 + 15*MBhat^2*Ycut^2 - 120*m2*MBhat^2*Ycut^2 - 
     150*m2^2*MBhat^2*Ycut^2 + 120*m2^3*MBhat^2*Ycut^2 - 
     15*m2^4*MBhat^2*Ycut^2 - 2*Ycut^3 + 30*m2*Ycut^3 + 380*m2^2*Ycut^3 + 
     60*m2^3*Ycut^3 - 30*m2^4*Ycut^3 + 2*m2^5*Ycut^3 + 17*MBhat*Ycut^3 - 
     95*m2*MBhat*Ycut^3 - 450*m2^2*MBhat*Ycut^3 + 110*m2^3*MBhat*Ycut^3 - 
     25*m2^4*MBhat*Ycut^3 + 3*m2^5*MBhat*Ycut^3 - 15*MBhat^2*Ycut^3 + 
     50*m2*MBhat^2*Ycut^3 + 120*m2^2*MBhat^2*Ycut^3 - 
     50*m2^3*MBhat^2*Ycut^3 + 5*m2^4*MBhat^2*Ycut^3 - 5*Ycut^4 + 
     10*m2*Ycut^4 - 30*m2^2*Ycut^4 - 40*m2^3*Ycut^4 + 5*m2^4*Ycut^4 - 
     30*MBhat*Ycut^4 + 55*m2*MBhat*Ycut^4 + 30*m2^2*MBhat*Ycut^4 + 
     5*m2^4*MBhat*Ycut^4 + 35*MBhat^2*Ycut^4 - 30*m2*MBhat^2*Ycut^4 - 
     30*m2^2*MBhat^2*Ycut^4 + 10*m2^3*MBhat^2*Ycut^4 + 18*Ycut^5 - 
     30*m2*Ycut^5 + 27*MBhat*Ycut^5 - 45*m2*MBhat*Ycut^5 + 
     30*m2^2*MBhat*Ycut^5 - 45*MBhat^2*Ycut^5 + 30*m2*MBhat^2*Ycut^5 - 
     24*Ycut^6 + 30*m2*Ycut^6 - 10*m2^2*Ycut^6 - MBhat*Ycut^6 + 
     5*m2*MBhat*Ycut^6 + 25*MBhat^2*Ycut^6 - 10*m2*MBhat^2*Ycut^6 + 
     14*Ycut^7 - 10*m2*Ycut^7 - 9*MBhat*Ycut^7 + 5*m2*MBhat*Ycut^7 - 
     5*MBhat^2*Ycut^7 - 3*Ycut^8 + 3*MBhat*Ycut^8 + 
     20*api*MBhat^2*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat^2*Ycut*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut^2*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     20*api*MBhat^2*Ycut^3*X1mix[0, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     40*api*MBhat*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     120*api*MBhat*Ycut*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     120*api*MBhat*Ycut^2*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     40*api*MBhat*Ycut^3*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     20*api*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     60*api*Ycut*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     60*api*Ycut^2*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     20*api*Ycut^3*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat])/
    (20*(-1 + Ycut)^3)) + 
 c[SL]*(c[SR]*(12*m2^(3/2)*(1 + 3*m2 + m2^2 - 2*MBhat - 3*m2*MBhat + 
       MBhat^2 + m2*MBhat^2)*Log[m2] - 12*m2^(3/2)*(1 + 3*m2 + m2^2 - 
       2*MBhat - 3*m2*MBhat + MBhat^2 + m2*MBhat^2)*Log[1 - Ycut] - 
     (-Sqrt[m2] - 28*m2^(3/2) + 28*m2^(7/2) + m2^(9/2) + 3*Sqrt[m2]*MBhat + 
       44*m2^(3/2)*MBhat - 36*m2^(5/2)*MBhat - 12*m2^(7/2)*MBhat + 
       m2^(9/2)*MBhat - 2*Sqrt[m2]*MBhat^2 - 18*m2^(3/2)*MBhat^2 + 
       18*m2^(5/2)*MBhat^2 + 2*m2^(7/2)*MBhat^2 + 2*Sqrt[m2]*Ycut + 
       68*m2^(3/2)*Ycut + 36*m2^(5/2)*Ycut - 44*m2^(7/2)*Ycut - 
       2*m2^(9/2)*Ycut - 6*Sqrt[m2]*MBhat*Ycut - 112*m2^(3/2)*MBhat*Ycut + 
       36*m2^(5/2)*MBhat*Ycut + 24*m2^(7/2)*MBhat*Ycut - 
       2*m2^(9/2)*MBhat*Ycut + 4*Sqrt[m2]*MBhat^2*Ycut + 
       48*m2^(3/2)*MBhat^2*Ycut - 24*m2^(5/2)*MBhat^2*Ycut - 
       4*m2^(7/2)*MBhat^2*Ycut - Sqrt[m2]*Ycut^2 - 46*m2^(3/2)*Ycut^2 - 
       54*m2^(5/2)*Ycut^2 + 10*m2^(7/2)*Ycut^2 + m2^(9/2)*Ycut^2 + 
       3*Sqrt[m2]*MBhat*Ycut^2 + 80*m2^(3/2)*MBhat*Ycut^2 + 
       18*m2^(5/2)*MBhat*Ycut^2 - 12*m2^(7/2)*MBhat*Ycut^2 + 
       m2^(9/2)*MBhat*Ycut^2 - 2*Sqrt[m2]*MBhat^2*Ycut^2 - 
       36*m2^(3/2)*MBhat^2*Ycut^2 + 2*m2^(7/2)*MBhat^2*Ycut^2 + 
       4*m2^(3/2)*Ycut^3 + 12*m2^(5/2)*Ycut^3 + 4*m2^(7/2)*Ycut^3 - 
       2*Sqrt[m2]*MBhat*Ycut^3 - 2*m2^(3/2)*MBhat*Ycut^3 - 
       18*m2^(5/2)*MBhat*Ycut^3 + 2*m2^(7/2)*MBhat*Ycut^3 + 
       2*Sqrt[m2]*MBhat^2*Ycut^3 + 6*m2^(5/2)*MBhat^2*Ycut^3 + 
       Sqrt[m2]*Ycut^4 - 2*m2^(3/2)*Ycut^4 + 6*m2^(5/2)*Ycut^4 + 
       3*Sqrt[m2]*MBhat*Ycut^4 - 8*m2^(3/2)*MBhat*Ycut^4 - 
       4*Sqrt[m2]*MBhat^2*Ycut^4 + 6*m2^(3/2)*MBhat^2*Ycut^4 - 
       2*Sqrt[m2]*Ycut^5 + 4*m2^(3/2)*Ycut^5 - 2*m2^(3/2)*MBhat*Ycut^5 + 
       2*Sqrt[m2]*MBhat^2*Ycut^5 + Sqrt[m2]*Ycut^6 - Sqrt[m2]*MBhat*Ycut^6 - 
       api*MBhat^2*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       2*api*MBhat^2*Ycut*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       api*MBhat^2*Ycut^2*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       2*api*MBhat*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       4*api*MBhat*Ycut*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       2*api*MBhat*Ycut^2*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       api*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       2*api*Ycut*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       api*Ycut^2*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat])/
      (-1 + Ycut)^2) - (c[T]*(-2*MBhat*Ycut^3 + 8*m2*MBhat*Ycut^3 - 
      12*m2^2*MBhat*Ycut^3 + 8*m2^3*MBhat*Ycut^3 - 2*m2^4*MBhat*Ycut^3 + 
      2*MBhat^2*Ycut^3 - 6*m2*MBhat^2*Ycut^3 + 6*m2^2*MBhat^2*Ycut^3 - 
      2*m2^3*MBhat^2*Ycut^3 + Ycut^4 - 4*m2*Ycut^4 + 6*m2^2*Ycut^4 - 
      4*m2^3*Ycut^4 + m2^4*Ycut^4 + 7*MBhat*Ycut^4 - 22*m2*MBhat*Ycut^4 + 
      24*m2^2*MBhat*Ycut^4 - 10*m2^3*MBhat*Ycut^4 + m2^4*MBhat*Ycut^4 - 
      8*MBhat^2*Ycut^4 + 18*m2*MBhat^2*Ycut^4 - 12*m2^2*MBhat^2*Ycut^4 + 
      2*m2^3*MBhat^2*Ycut^4 - 4*Ycut^5 + 12*m2*Ycut^5 - 12*m2^2*Ycut^5 + 
      4*m2^3*Ycut^5 - 8*MBhat*Ycut^5 + 18*m2*MBhat*Ycut^5 - 
      12*m2^2*MBhat*Ycut^5 + 2*m2^3*MBhat*Ycut^5 + 12*MBhat^2*Ycut^5 - 
      18*m2*MBhat^2*Ycut^5 + 6*m2^2*MBhat^2*Ycut^5 + 6*Ycut^6 - 
      12*m2*Ycut^6 + 6*m2^2*Ycut^6 + 2*MBhat*Ycut^6 - 2*m2*MBhat*Ycut^6 - 
      8*MBhat^2*Ycut^6 + 6*m2*MBhat^2*Ycut^6 - 4*Ycut^7 + 4*m2*Ycut^7 + 
      2*MBhat*Ycut^7 - 2*m2*MBhat*Ycut^7 + 2*MBhat^2*Ycut^7 + Ycut^8 - 
      MBhat*Ycut^8 + api*MBhat^2*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      3*api*MBhat^2*Ycut*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      3*api*MBhat^2*Ycut^2*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      api*MBhat^2*Ycut^3*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      2*api*MBhat*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      6*api*MBhat*Ycut*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      6*api*MBhat*Ycut^2*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      2*api*MBhat*Ycut^3*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      api*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      3*api*Ycut*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      3*api*Ycut^2*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      api*Ycut^3*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat]))/(-1 + Ycut)^3) + 
 c[T]^2*(48*m2^2*(-4 - 4*m2 + 7*MBhat + m2*MBhat - 3*MBhat^2)*Log[m2] - 
   48*m2^2*(-4 - 4*m2 + 7*MBhat + m2*MBhat - 3*MBhat^2)*Log[1 - Ycut] - 
   (16 - 240*m2 - 1280*m2^2 + 1280*m2^3 + 240*m2^4 - 16*m2^5 - 76*MBhat + 
     780*m2*MBhat + 800*m2^2*MBhat - 1760*m2^3*MBhat + 300*m2^4*MBhat - 
     44*m2^5*MBhat + 60*MBhat^2 - 480*m2*MBhat^2 + 480*m2^3*MBhat^2 - 
     60*m2^4*MBhat^2 - 48*Ycut + 720*m2*Ycut + 4800*m2^2*Ycut - 
     2880*m2^3*Ycut - 720*m2^4*Ycut + 48*m2^5*Ycut + 228*MBhat*Ycut - 
     2340*m2*MBhat*Ycut - 4080*m2^2*MBhat*Ycut + 5040*m2^3*MBhat*Ycut - 
     900*m2^4*MBhat*Ycut + 132*m2^5*MBhat*Ycut - 180*MBhat^2*Ycut + 
     1440*m2*MBhat^2*Ycut + 720*m2^2*MBhat^2*Ycut - 1440*m2^3*MBhat^2*Ycut + 
     180*m2^4*MBhat^2*Ycut + 48*Ycut^2 - 720*m2*Ycut^2 - 6240*m2^2*Ycut^2 + 
     1440*m2^3*Ycut^2 + 720*m2^4*Ycut^2 - 48*m2^5*Ycut^2 - 228*MBhat*Ycut^2 + 
     2340*m2*MBhat*Ycut^2 + 6600*m2^2*MBhat*Ycut^2 - 4680*m2^3*MBhat*Ycut^2 + 
     900*m2^4*MBhat*Ycut^2 - 132*m2^5*MBhat*Ycut^2 + 180*MBhat^2*Ycut^2 - 
     1440*m2*MBhat^2*Ycut^2 - 1800*m2^2*MBhat^2*Ycut^2 + 
     1440*m2^3*MBhat^2*Ycut^2 - 180*m2^4*MBhat^2*Ycut^2 - 16*Ycut^3 + 
     240*m2*Ycut^3 + 3040*m2^2*Ycut^3 + 480*m2^3*Ycut^3 - 240*m2^4*Ycut^3 + 
     16*m2^5*Ycut^3 + 276*MBhat*Ycut^3 - 1340*m2*MBhat*Ycut^3 - 
     3400*m2^2*MBhat*Ycut^3 + 1240*m2^3*MBhat*Ycut^3 - 
     340*m2^4*MBhat*Ycut^3 + 44*m2^5*MBhat*Ycut^3 - 260*MBhat^2*Ycut^3 + 
     840*m2*MBhat^2*Ycut^3 + 1200*m2^2*MBhat^2*Ycut^3 - 
     520*m2^3*MBhat^2*Ycut^3 + 60*m2^4*MBhat^2*Ycut^3 - 60*Ycut^4 + 
     160*m2*Ycut^4 - 360*m2^2*Ycut^4 - 240*m2^3*Ycut^4 + 20*m2^4*Ycut^4 - 
     680*MBhat*Ycut^4 + 1660*m2*MBhat*Ycut^4 - 840*m2^2*MBhat*Ycut^4 + 
     320*m2^3*MBhat*Ycut^4 + 20*m2^4*MBhat*Ycut^4 + 740*MBhat^2*Ycut^4 - 
     1080*m2*MBhat^2*Ycut^4 + 120*m2^2*MBhat^2*Ycut^4 + 
     40*m2^3*MBhat^2*Ycut^4 + 224*Ycut^5 - 480*m2*Ycut^5 + 240*m2^2*Ycut^5 - 
     80*m2^3*Ycut^5 + 796*MBhat*Ycut^5 - 1620*m2*MBhat*Ycut^5 + 
     1080*m2^2*MBhat*Ycut^5 - 160*m2^3*MBhat*Ycut^5 - 1020*MBhat^2*Ycut^5 + 
     1080*m2*MBhat^2*Ycut^5 - 240*m2^2*MBhat^2*Ycut^5 - 312*Ycut^6 + 
     480*m2*Ycut^6 - 200*m2^2*Ycut^6 - 308*MBhat*Ycut^6 + 
     500*m2*MBhat*Ycut^6 - 160*m2^2*MBhat*Ycut^6 + 620*MBhat^2*Ycut^6 - 
     360*m2*MBhat^2*Ycut^6 + 192*Ycut^7 - 160*m2*Ycut^7 - 52*MBhat*Ycut^7 + 
     20*m2*MBhat*Ycut^7 - 140*MBhat^2*Ycut^7 - 44*Ycut^8 + 44*MBhat*Ycut^8 + 
     5*api*MBhat^2*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     15*api*MBhat^2*Ycut*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] + 
     15*api*MBhat^2*Ycut^2*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     5*api*MBhat^2*Ycut^3*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     10*api*MBhat*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     30*api*MBhat*Ycut*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] - 
     30*api*MBhat*Ycut^2*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     10*api*MBhat*Ycut^3*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     5*api*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     15*api*Ycut*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] + 
     15*api*Ycut^2*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     5*api*Ycut^3*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat])/(5*(-1 + Ycut)^3)) + 
 c[VL]^2*(6*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[m2] - 
   6*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[1 - Ycut] - 
   (3 - 45*m2 - 240*m2^2 + 240*m2^3 + 45*m2^4 - 3*m2^5 - 13*MBhat + 
     135*m2*MBhat + 160*m2^2*MBhat - 320*m2^3*MBhat + 45*m2^4*MBhat - 
     7*m2^5*MBhat + 10*MBhat^2 - 80*m2*MBhat^2 + 80*m2^3*MBhat^2 - 
     10*m2^4*MBhat^2 - 9*Ycut + 135*m2*Ycut + 900*m2^2*Ycut - 540*m2^3*Ycut - 
     135*m2^4*Ycut + 9*m2^5*Ycut + 39*MBhat*Ycut - 405*m2*MBhat*Ycut - 
     780*m2^2*MBhat*Ycut + 900*m2^3*MBhat*Ycut - 135*m2^4*MBhat*Ycut + 
     21*m2^5*MBhat*Ycut - 30*MBhat^2*Ycut + 240*m2*MBhat^2*Ycut + 
     120*m2^2*MBhat^2*Ycut - 240*m2^3*MBhat^2*Ycut + 30*m2^4*MBhat^2*Ycut + 
     9*Ycut^2 - 135*m2*Ycut^2 - 1170*m2^2*Ycut^2 + 270*m2^3*Ycut^2 + 
     135*m2^4*Ycut^2 - 9*m2^5*Ycut^2 - 39*MBhat*Ycut^2 + 
     405*m2*MBhat*Ycut^2 + 1230*m2^2*MBhat*Ycut^2 - 810*m2^3*MBhat*Ycut^2 + 
     135*m2^4*MBhat*Ycut^2 - 21*m2^5*MBhat*Ycut^2 + 30*MBhat^2*Ycut^2 - 
     240*m2*MBhat^2*Ycut^2 - 300*m2^2*MBhat^2*Ycut^2 + 
     240*m2^3*MBhat^2*Ycut^2 - 30*m2^4*MBhat^2*Ycut^2 - 3*Ycut^3 + 
     45*m2*Ycut^3 + 570*m2^2*Ycut^3 + 90*m2^3*Ycut^3 - 45*m2^4*Ycut^3 + 
     3*m2^5*Ycut^3 + 33*MBhat*Ycut^3 - 175*m2*MBhat*Ycut^3 - 
     710*m2^2*MBhat*Ycut^3 + 250*m2^3*MBhat*Ycut^3 - 65*m2^4*MBhat*Ycut^3 + 
     7*m2^5*MBhat*Ycut^3 - 30*MBhat^2*Ycut^3 + 100*m2*MBhat^2*Ycut^3 + 
     240*m2^2*MBhat^2*Ycut^3 - 100*m2^3*MBhat^2*Ycut^3 + 
     10*m2^4*MBhat^2*Ycut^3 - 5*Ycut^4 + 5*m2*Ycut^4 - 30*m2^2*Ycut^4 - 
     70*m2^3*Ycut^4 + 10*m2^4*Ycut^4 - 65*MBhat*Ycut^4 + 
     125*m2*MBhat*Ycut^4 + 30*m2^2*MBhat*Ycut^4 - 10*m2^3*MBhat*Ycut^4 + 
     10*m2^4*MBhat*Ycut^4 + 70*MBhat^2*Ycut^4 - 60*m2*MBhat^2*Ycut^4 - 
     60*m2^2*MBhat^2*Ycut^4 + 20*m2^3*MBhat^2*Ycut^4 + 17*Ycut^5 - 
     15*m2*Ycut^5 - 30*m2^2*Ycut^5 + 10*m2^3*Ycut^5 + 73*MBhat*Ycut^5 - 
     135*m2*MBhat*Ycut^5 + 90*m2^2*MBhat*Ycut^5 - 10*m2^3*MBhat*Ycut^5 - 
     90*MBhat^2*Ycut^5 + 60*m2*MBhat^2*Ycut^5 - 21*Ycut^6 + 15*m2*Ycut^6 - 
     29*MBhat*Ycut^6 + 55*m2*MBhat*Ycut^6 - 20*m2^2*MBhat*Ycut^6 + 
     50*MBhat^2*Ycut^6 - 20*m2*MBhat^2*Ycut^6 + 11*Ycut^7 - 5*m2*Ycut^7 - 
     MBhat*Ycut^7 - 5*m2*MBhat*Ycut^7 - 10*MBhat^2*Ycut^7 - 2*Ycut^8 + 
     2*MBhat*Ycut^8 + 10*api*MBhat^2*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     30*api*MBhat^2*Ycut*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     30*api*MBhat^2*Ycut^2*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     10*api*MBhat^2*Ycut^3*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     20*api*MBhat*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     60*api*MBhat*Ycut*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     60*api*MBhat*Ycut^2*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     20*api*MBhat*Ycut^3*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     10*api*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     30*api*Ycut*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     30*api*Ycut^2*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     10*api*Ycut^3*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat])/
    (10*(-1 + Ycut)^3)) + 
 c[VR]*(-24*m2^(3/2)*(1 + 3*m2 + m2^2 - 2*MBhat - 3*m2*MBhat + MBhat^2 + 
     m2*MBhat^2)*Log[m2] + 24*m2^(3/2)*(1 + 3*m2 + m2^2 - 2*MBhat - 
     3*m2*MBhat + MBhat^2 + m2*MBhat^2)*Log[1 - Ycut] + 
   (-2*Sqrt[m2] - 56*m2^(3/2) + 56*m2^(7/2) + 2*m2^(9/2) + 6*Sqrt[m2]*MBhat + 
     88*m2^(3/2)*MBhat - 72*m2^(5/2)*MBhat - 24*m2^(7/2)*MBhat + 
     2*m2^(9/2)*MBhat - 4*Sqrt[m2]*MBhat^2 - 36*m2^(3/2)*MBhat^2 + 
     36*m2^(5/2)*MBhat^2 + 4*m2^(7/2)*MBhat^2 + 4*Sqrt[m2]*Ycut + 
     136*m2^(3/2)*Ycut + 72*m2^(5/2)*Ycut - 88*m2^(7/2)*Ycut - 
     4*m2^(9/2)*Ycut - 12*Sqrt[m2]*MBhat*Ycut - 224*m2^(3/2)*MBhat*Ycut + 
     72*m2^(5/2)*MBhat*Ycut + 48*m2^(7/2)*MBhat*Ycut - 
     4*m2^(9/2)*MBhat*Ycut + 8*Sqrt[m2]*MBhat^2*Ycut + 
     96*m2^(3/2)*MBhat^2*Ycut - 48*m2^(5/2)*MBhat^2*Ycut - 
     8*m2^(7/2)*MBhat^2*Ycut - 2*Sqrt[m2]*Ycut^2 - 92*m2^(3/2)*Ycut^2 - 
     108*m2^(5/2)*Ycut^2 + 20*m2^(7/2)*Ycut^2 + 2*m2^(9/2)*Ycut^2 + 
     6*Sqrt[m2]*MBhat*Ycut^2 + 160*m2^(3/2)*MBhat*Ycut^2 + 
     36*m2^(5/2)*MBhat*Ycut^2 - 24*m2^(7/2)*MBhat*Ycut^2 + 
     2*m2^(9/2)*MBhat*Ycut^2 - 4*Sqrt[m2]*MBhat^2*Ycut^2 - 
     72*m2^(3/2)*MBhat^2*Ycut^2 + 4*m2^(7/2)*MBhat^2*Ycut^2 + 
     8*m2^(3/2)*Ycut^3 + 24*m2^(5/2)*Ycut^3 + 8*m2^(7/2)*Ycut^3 - 
     4*Sqrt[m2]*MBhat*Ycut^3 - 4*m2^(3/2)*MBhat*Ycut^3 - 
     36*m2^(5/2)*MBhat*Ycut^3 + 4*m2^(7/2)*MBhat*Ycut^3 + 
     4*Sqrt[m2]*MBhat^2*Ycut^3 + 12*m2^(5/2)*MBhat^2*Ycut^3 + 
     2*Sqrt[m2]*Ycut^4 - 4*m2^(3/2)*Ycut^4 + 12*m2^(5/2)*Ycut^4 + 
     6*Sqrt[m2]*MBhat*Ycut^4 - 16*m2^(3/2)*MBhat*Ycut^4 - 
     8*Sqrt[m2]*MBhat^2*Ycut^4 + 12*m2^(3/2)*MBhat^2*Ycut^4 - 
     4*Sqrt[m2]*Ycut^5 + 8*m2^(3/2)*Ycut^5 - 4*m2^(3/2)*MBhat*Ycut^5 + 
     4*Sqrt[m2]*MBhat^2*Ycut^5 + 2*Sqrt[m2]*Ycut^6 - 
     2*Sqrt[m2]*MBhat*Ycut^6 + api*MBhat^2*X1mix[0, 0, SM*c[VR], Ycut, m2, 
       mu2hat] - 2*api*MBhat^2*Ycut*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     api*MBhat^2*Ycut^2*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     2*api*MBhat*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     4*api*MBhat*Ycut*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     2*api*MBhat*Ycut^2*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     api*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     2*api*Ycut*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     api*Ycut^2*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat])/(-1 + Ycut)^2) + 
 c[VL]*(12*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[m2] - 
   12*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[1 - Ycut] - 
   (3 - 45*m2 - 240*m2^2 + 240*m2^3 + 45*m2^4 - 3*m2^5 - 13*MBhat + 
     135*m2*MBhat + 160*m2^2*MBhat - 320*m2^3*MBhat + 45*m2^4*MBhat - 
     7*m2^5*MBhat + 10*MBhat^2 - 80*m2*MBhat^2 + 80*m2^3*MBhat^2 - 
     10*m2^4*MBhat^2 - 9*Ycut + 135*m2*Ycut + 900*m2^2*Ycut - 540*m2^3*Ycut - 
     135*m2^4*Ycut + 9*m2^5*Ycut + 39*MBhat*Ycut - 405*m2*MBhat*Ycut - 
     780*m2^2*MBhat*Ycut + 900*m2^3*MBhat*Ycut - 135*m2^4*MBhat*Ycut + 
     21*m2^5*MBhat*Ycut - 30*MBhat^2*Ycut + 240*m2*MBhat^2*Ycut + 
     120*m2^2*MBhat^2*Ycut - 240*m2^3*MBhat^2*Ycut + 30*m2^4*MBhat^2*Ycut + 
     9*Ycut^2 - 135*m2*Ycut^2 - 1170*m2^2*Ycut^2 + 270*m2^3*Ycut^2 + 
     135*m2^4*Ycut^2 - 9*m2^5*Ycut^2 - 39*MBhat*Ycut^2 + 
     405*m2*MBhat*Ycut^2 + 1230*m2^2*MBhat*Ycut^2 - 810*m2^3*MBhat*Ycut^2 + 
     135*m2^4*MBhat*Ycut^2 - 21*m2^5*MBhat*Ycut^2 + 30*MBhat^2*Ycut^2 - 
     240*m2*MBhat^2*Ycut^2 - 300*m2^2*MBhat^2*Ycut^2 + 
     240*m2^3*MBhat^2*Ycut^2 - 30*m2^4*MBhat^2*Ycut^2 - 3*Ycut^3 + 
     45*m2*Ycut^3 + 570*m2^2*Ycut^3 + 90*m2^3*Ycut^3 - 45*m2^4*Ycut^3 + 
     3*m2^5*Ycut^3 + 33*MBhat*Ycut^3 - 175*m2*MBhat*Ycut^3 - 
     710*m2^2*MBhat*Ycut^3 + 250*m2^3*MBhat*Ycut^3 - 65*m2^4*MBhat*Ycut^3 + 
     7*m2^5*MBhat*Ycut^3 - 30*MBhat^2*Ycut^3 + 100*m2*MBhat^2*Ycut^3 + 
     240*m2^2*MBhat^2*Ycut^3 - 100*m2^3*MBhat^2*Ycut^3 + 
     10*m2^4*MBhat^2*Ycut^3 - 5*Ycut^4 + 5*m2*Ycut^4 - 30*m2^2*Ycut^4 - 
     70*m2^3*Ycut^4 + 10*m2^4*Ycut^4 - 65*MBhat*Ycut^4 + 
     125*m2*MBhat*Ycut^4 + 30*m2^2*MBhat*Ycut^4 - 10*m2^3*MBhat*Ycut^4 + 
     10*m2^4*MBhat*Ycut^4 + 70*MBhat^2*Ycut^4 - 60*m2*MBhat^2*Ycut^4 - 
     60*m2^2*MBhat^2*Ycut^4 + 20*m2^3*MBhat^2*Ycut^4 + 17*Ycut^5 - 
     15*m2*Ycut^5 - 30*m2^2*Ycut^5 + 10*m2^3*Ycut^5 + 73*MBhat*Ycut^5 - 
     135*m2*MBhat*Ycut^5 + 90*m2^2*MBhat*Ycut^5 - 10*m2^3*MBhat*Ycut^5 - 
     90*MBhat^2*Ycut^5 + 60*m2*MBhat^2*Ycut^5 - 21*Ycut^6 + 15*m2*Ycut^6 - 
     29*MBhat*Ycut^6 + 55*m2*MBhat*Ycut^6 - 20*m2^2*MBhat*Ycut^6 + 
     50*MBhat^2*Ycut^6 - 20*m2*MBhat^2*Ycut^6 + 11*Ycut^7 - 5*m2*Ycut^7 - 
     MBhat*Ycut^7 - 5*m2*MBhat*Ycut^7 - 10*MBhat^2*Ycut^7 - 2*Ycut^8 + 
     2*MBhat*Ycut^8 + 5*api*MBhat^2*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     15*api*MBhat^2*Ycut*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     15*api*MBhat^2*Ycut^2*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     5*api*MBhat^2*Ycut^3*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     10*api*MBhat*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     30*api*MBhat*Ycut*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     30*api*MBhat*Ycut^2*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     10*api*MBhat*Ycut^3*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     5*api*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     15*api*Ycut*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     15*api*Ycut^2*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     5*api*Ycut^3*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat])/
    (5*(-1 + Ycut)^3) + 
   c[VR]*(-24*m2^(3/2)*(1 + 3*m2 + m2^2 - 2*MBhat - 3*m2*MBhat + MBhat^2 + 
       m2*MBhat^2)*Log[m2] + 24*m2^(3/2)*(1 + 3*m2 + m2^2 - 2*MBhat - 
       3*m2*MBhat + MBhat^2 + m2*MBhat^2)*Log[1 - Ycut] + 
     (-2*Sqrt[m2] - 56*m2^(3/2) + 56*m2^(7/2) + 2*m2^(9/2) + 
       6*Sqrt[m2]*MBhat + 88*m2^(3/2)*MBhat - 72*m2^(5/2)*MBhat - 
       24*m2^(7/2)*MBhat + 2*m2^(9/2)*MBhat - 4*Sqrt[m2]*MBhat^2 - 
       36*m2^(3/2)*MBhat^2 + 36*m2^(5/2)*MBhat^2 + 4*m2^(7/2)*MBhat^2 + 
       4*Sqrt[m2]*Ycut + 136*m2^(3/2)*Ycut + 72*m2^(5/2)*Ycut - 
       88*m2^(7/2)*Ycut - 4*m2^(9/2)*Ycut - 12*Sqrt[m2]*MBhat*Ycut - 
       224*m2^(3/2)*MBhat*Ycut + 72*m2^(5/2)*MBhat*Ycut + 
       48*m2^(7/2)*MBhat*Ycut - 4*m2^(9/2)*MBhat*Ycut + 
       8*Sqrt[m2]*MBhat^2*Ycut + 96*m2^(3/2)*MBhat^2*Ycut - 
       48*m2^(5/2)*MBhat^2*Ycut - 8*m2^(7/2)*MBhat^2*Ycut - 
       2*Sqrt[m2]*Ycut^2 - 92*m2^(3/2)*Ycut^2 - 108*m2^(5/2)*Ycut^2 + 
       20*m2^(7/2)*Ycut^2 + 2*m2^(9/2)*Ycut^2 + 6*Sqrt[m2]*MBhat*Ycut^2 + 
       160*m2^(3/2)*MBhat*Ycut^2 + 36*m2^(5/2)*MBhat*Ycut^2 - 
       24*m2^(7/2)*MBhat*Ycut^2 + 2*m2^(9/2)*MBhat*Ycut^2 - 
       4*Sqrt[m2]*MBhat^2*Ycut^2 - 72*m2^(3/2)*MBhat^2*Ycut^2 + 
       4*m2^(7/2)*MBhat^2*Ycut^2 + 8*m2^(3/2)*Ycut^3 + 24*m2^(5/2)*Ycut^3 + 
       8*m2^(7/2)*Ycut^3 - 4*Sqrt[m2]*MBhat*Ycut^3 - 
       4*m2^(3/2)*MBhat*Ycut^3 - 36*m2^(5/2)*MBhat*Ycut^3 + 
       4*m2^(7/2)*MBhat*Ycut^3 + 4*Sqrt[m2]*MBhat^2*Ycut^3 + 
       12*m2^(5/2)*MBhat^2*Ycut^3 + 2*Sqrt[m2]*Ycut^4 - 4*m2^(3/2)*Ycut^4 + 
       12*m2^(5/2)*Ycut^4 + 6*Sqrt[m2]*MBhat*Ycut^4 - 
       16*m2^(3/2)*MBhat*Ycut^4 - 8*Sqrt[m2]*MBhat^2*Ycut^4 + 
       12*m2^(3/2)*MBhat^2*Ycut^4 - 4*Sqrt[m2]*Ycut^5 + 8*m2^(3/2)*Ycut^5 - 
       4*m2^(3/2)*MBhat*Ycut^5 + 4*Sqrt[m2]*MBhat^2*Ycut^5 + 
       2*Sqrt[m2]*Ycut^6 - 2*Sqrt[m2]*MBhat*Ycut^6 + 
       api*MBhat^2*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       2*api*MBhat^2*Ycut*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       api*MBhat^2*Ycut^2*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       2*api*MBhat*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       4*api*MBhat*Ycut*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       2*api*MBhat*Ycut^2*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       api*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       2*api*Ycut*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       api*Ycut^2*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat])/
      (-1 + Ycut)^2)) + 
 c[VR]^2*(6*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[m2] - 
   6*m2^2*(-3 - 3*m2 + 5*MBhat + m2*MBhat - 2*MBhat^2)*Log[1 - Ycut] - 
   (3 - 45*m2 - 240*m2^2 + 240*m2^3 + 45*m2^4 - 3*m2^5 - 13*MBhat + 
     135*m2*MBhat + 160*m2^2*MBhat - 320*m2^3*MBhat + 45*m2^4*MBhat - 
     7*m2^5*MBhat + 10*MBhat^2 - 80*m2*MBhat^2 + 80*m2^3*MBhat^2 - 
     10*m2^4*MBhat^2 - 3*Ycut + 45*m2*Ycut + 420*m2^2*Ycut - 60*m2^3*Ycut - 
     45*m2^4*Ycut + 3*m2^5*Ycut + 13*MBhat*Ycut - 135*m2*MBhat*Ycut - 
     460*m2^2*MBhat*Ycut + 260*m2^3*MBhat*Ycut - 45*m2^4*MBhat*Ycut + 
     7*m2^5*MBhat*Ycut - 10*MBhat^2*Ycut + 80*m2*MBhat^2*Ycut + 
     120*m2^2*MBhat^2*Ycut - 80*m2^3*MBhat^2*Ycut + 10*m2^4*MBhat^2*Ycut - 
     90*m2^2*Ycut^2 - 90*m2^3*Ycut^2 + 150*m2^2*MBhat*Ycut^2 + 
     30*m2^3*MBhat*Ycut^2 - 60*m2^2*MBhat^2*Ycut^2 - 30*m2^2*Ycut^3 - 
     30*m2^3*Ycut^3 + 40*MBhat*Ycut^3 - 120*m2*MBhat*Ycut^3 + 
     170*m2^2*MBhat*Ycut^3 - 30*m2^3*MBhat*Ycut^3 - 40*MBhat^2*Ycut^3 + 
     80*m2*MBhat^2*Ycut^3 - 60*m2^2*MBhat^2*Ycut^3 - 15*Ycut^4 + 
     45*m2*Ycut^4 - 60*m2^2*Ycut^4 - 55*MBhat*Ycut^4 + 105*m2*MBhat*Ycut^4 - 
     20*m2^2*MBhat*Ycut^4 + 70*MBhat^2*Ycut^4 - 80*m2*MBhat^2*Ycut^4 + 
     27*Ycut^5 - 45*m2*Ycut^5 + 3*MBhat*Ycut^5 + 15*m2*MBhat*Ycut^5 - 
     30*MBhat^2*Ycut^5 - 12*Ycut^6 + 12*MBhat*Ycut^6 + 
     10*api*MBhat^2*X1mix[0, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     10*api*MBhat^2*Ycut*X1mix[0, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     20*api*MBhat*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     20*api*MBhat*Ycut*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     10*api*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     10*api*Ycut*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat])/(10*(-1 + Ycut)))
