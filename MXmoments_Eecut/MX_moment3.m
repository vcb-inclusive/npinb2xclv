6*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 95*m2*MBhat + 
   55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 138*m2*MBhat^2 - 
   38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
   45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 3*m2*MBhat^5 - 2*MBhat^6)*
  Log[m2] - 6*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 95*m2*MBhat + 
   55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 138*m2*MBhat^2 - 
   38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
   45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 3*m2*MBhat^5 - 2*MBhat^6)*
  Log[1 - Ycut] + 
 (mupi*(-1/420*((-1 + m2 + Ycut)*(15 - 510*m2 - 16995*m2^2 - 40620*m2^3 - 
         16995*m2^4 - 510*m2^5 + 15*m2^6 - 362*MBhat^2 + 7226*m2*MBhat^2 + 
         87866*m2^2*MBhat^2 + 30116*m2^3*MBhat^2 - 6634*m2^4*MBhat^2 + 
         1178*m2^5*MBhat^2 - 110*m2^6*MBhat^2 + 1264*MBhat^3 - 
         17888*m2*MBhat^3 - 99368*m2^2*MBhat^3 + 7312*m2^3*MBhat^3 - 
         7808*m2^4*MBhat^3 + 2608*m2^5*MBhat^3 - 360*m2^6*MBhat^3 - 
         1127*MBhat^4 + 12418*m2*MBhat^4 + 32578*m2^2*MBhat^4 - 
         12782*m2^3*MBhat^4 + 3913*m2^4*MBhat^4 - 560*m2^5*MBhat^4 + 
         210*MBhat^6 - 1470*m2*MBhat^6 - 1470*m2^2*MBhat^6 + 
         210*m2^3*MBhat^6 - 90*Ycut + 3075*m2*Ycut + 107775*m2^2*Ycut + 
         264030*m2^3*Ycut + 113160*m2^4*Ycut + 3555*m2^5*Ycut - 
         105*m2^6*Ycut + 2172*MBhat^2*Ycut - 43718*m2*MBhat^2*Ycut - 
         565692*m2^2*MBhat^2*Ycut - 206086*m2^3*MBhat^2*Ycut + 
         45370*m2^4*MBhat^2*Ycut - 8136*m2^5*MBhat^2*Ycut + 
         770*m2^6*MBhat^2*Ycut - 7584*MBhat^3*Ycut + 108592*m2*MBhat^3*Ycut + 
         650144*m2^2*MBhat^3*Ycut - 45624*m2^3*MBhat^3*Ycut + 
         52408*m2^4*MBhat^3*Ycut - 17896*m2^5*MBhat^3*Ycut + 
         2520*m2^6*MBhat^3*Ycut + 6762*MBhat^4*Ycut - 75635*m2*MBhat^4*Ycut - 
         218197*m2^2*MBhat^4*Ycut + 86121*m2^3*MBhat^4*Ycut - 
         26831*m2^4*MBhat^4*Ycut + 3920*m2^5*MBhat^4*Ycut - 
         1260*MBhat^6*Ycut + 9030*m2*MBhat^6*Ycut + 10080*m2^2*MBhat^6*Ycut - 
         1470*m2^3*MBhat^6*Ycut + 225*Ycut^2 - 7725*m2*Ycut^2 - 
         287085*m2^2*Ycut^2 - 723930*m2^3*Ycut^2 - 319395*m2^4*Ycut^2 - 
         10605*m2^5*Ycut^2 + 315*m2^6*Ycut^2 - 5430*MBhat^2*Ycut^2 + 
         110200*m2*MBhat^2*Ycut^2 + 1532788*m2^2*MBhat^2*Ycut^2 + 
         599892*m2^3*MBhat^2*Ycut^2 - 131948*m2^4*MBhat^2*Ycut^2 + 
         23968*m2^5*MBhat^2*Ycut^2 - 2310*m2^6*MBhat^2*Ycut^2 + 
         18960*MBhat^3*Ycut^2 - 274640*m2*MBhat^3*Ycut^2 - 
         1794216*m2^2*MBhat^3*Ycut^2 + 116520*m2^3*MBhat^3*Ycut^2 - 
         148592*m2^4*MBhat^3*Ycut^2 + 52248*m2^5*MBhat^3*Ycut^2 - 
         7560*m2^6*MBhat^3*Ycut^2 - 16905*MBhat^4*Ycut^2 + 
         191905*m2*MBhat^4*Ycut^2 + 618198*m2^2*MBhat^4*Ycut^2 - 
         245511*m2^3*MBhat^4*Ycut^2 + 78253*m2^4*MBhat^4*Ycut^2 - 
         11760*m2^5*MBhat^4*Ycut^2 + 3150*MBhat^6*Ycut^2 - 
         23100*m2*MBhat^6*Ycut^2 - 29400*m2^2*MBhat^6*Ycut^2 + 
         4410*m2^3*MBhat^6*Ycut^2 - 300*Ycut^3 + 10350*m2*Ycut^3 + 
         412590*m2^2*Ycut^3 + 1077285*m2^3*Ycut^3 + 492765*m2^4*Ycut^3 + 
         17535*m2^5*Ycut^3 - 525*m2^6*Ycut^3 + 7240*MBhat^2*Ycut^3 - 
         148140*m2*MBhat^2*Ycut^3 - 2246672*m2^2*MBhat^2*Ycut^3 - 
         958750*m2^3*MBhat^2*Ycut^3 + 210532*m2^4*MBhat^2*Ycut^3 - 
         38920*m2^5*MBhat^2*Ycut^3 + 3850*m2^6*MBhat^2*Ycut^3 - 
         26540*MBhat^3*Ycut^3 + 372500*m2*MBhat^3*Ycut^3 + 
         2690504*m2^2*MBhat^3*Ycut^3 - 164616*m2^3*MBhat^3*Ycut^3 + 
         239092*m2^4*MBhat^3*Ycut^3 - 86660*m2^5*MBhat^3*Ycut^3 + 
         12600*m2^6*MBhat^3*Ycut^3 + 24220*MBhat^4*Ycut^3 - 
         259630*m2*MBhat^4*Ycut^3 - 963802*m2^2*MBhat^4*Ycut^3 + 
         394317*m2^3*MBhat^4*Ycut^3 - 130235*m2^4*MBhat^4*Ycut^3 + 
         19600*m2^5*MBhat^4*Ycut^3 - 420*MBhat^5*Ycut^3 - 
         1260*m2*MBhat^5*Ycut^3 + 3780*m2^2*MBhat^5*Ycut^3 - 
         2100*m2^3*MBhat^5*Ycut^3 - 4200*MBhat^6*Ycut^3 + 
         31500*m2*MBhat^6*Ycut^3 + 47040*m2^2*MBhat^6*Ycut^3 - 
         7350*m2^3*MBhat^6*Ycut^3 + 225*Ycut^4 - 7800*m2*Ycut^4 - 
         339660*m2^2*Ycut^4 - 926625*m2^3*Ycut^4 - 444360*m2^4*Ycut^4 - 
         17325*m2^5*Ycut^4 + 525*m2^6*Ycut^4 - 4065*MBhat^2*Ycut^4 + 
         110960*m2*MBhat^2*Ycut^4 + 1884108*m2^2*MBhat^2*Ycut^4 + 
         921928*m2^3*MBhat^2*Ycut^4 - 213395*m2^4*MBhat^2*Ycut^4 + 
         41790*m2^5*MBhat^2*Ycut^4 - 3850*m2^6*MBhat^2*Ycut^4 + 
         24980*MBhat^3*Ycut^4 - 295100*m2*MBhat^3*Ycut^4 - 
         2320776*m2^2*MBhat^3*Ycut^4 + 118888*m2^3*MBhat^3*Ycut^4 - 
         220780*m2^4*MBhat^3*Ycut^4 + 83580*m2^5*MBhat^3*Ycut^4 - 
         12600*m2^6*MBhat^3*Ycut^4 - 27300*MBhat^4*Ycut^4 + 
         199220*m2*MBhat^4*Ycut^4 + 890428*m2^2*MBhat^4*Ycut^4 - 
         378525*m2^3*MBhat^4*Ycut^4 + 126175*m2^4*MBhat^4*Ycut^4 - 
         19600*m2^5*MBhat^4*Ycut^4 + 3360*MBhat^5*Ycut^4 + 
         7980*m2*MBhat^5*Ycut^4 - 13440*m2^2*MBhat^5*Ycut^4 + 
         2100*m2^3*MBhat^5*Ycut^4 + 2800*MBhat^6*Ycut^4 - 
         24500*m2*MBhat^6*Ycut^4 - 45500*m2^2*MBhat^6*Ycut^4 + 
         7350*m2^3*MBhat^6*Ycut^4 - 90*Ycut^5 + 3135*m2*Ycut^5 + 
         154245*m2^2*Ycut^5 + 446670*m2^3*Ycut^5 + 229110*m2^4*Ycut^5 + 
         10185*m2^5*Ycut^5 - 315*m2^6*Ycut^5 - 714*MBhat*Ycut^5 + 
         210*m2*MBhat*Ycut^5 + 6300*m2^2*MBhat*Ycut^5 - 
         13020*m2^3*MBhat*Ycut^5 + 9870*m2^4*MBhat*Ycut^5 - 
         2646*m2^5*MBhat*Ycut^5 - 5514*MBhat^2*Ycut^5 - 
         37501*m2*MBhat^2*Ycut^5 - 860293*m2^2*MBhat^2*Ycut^5 - 
         534555*m2^3*MBhat^2*Ycut^5 + 129115*m2^4*MBhat^2*Ycut^5 - 
         23534*m2^5*MBhat^2*Ycut^5 + 2310*m2^6*MBhat^2*Ycut^5 - 
         16404*MBhat^3*Ycut^5 + 152260*m2*MBhat^3*Ycut^5 + 
         1082704*m2^2*MBhat^3*Ycut^5 - 24528*m2^3*MBhat^3*Ycut^5 + 
         119252*m2^4*MBhat^3*Ycut^5 - 47068*m2^5*MBhat^3*Ycut^5 + 
         7560*m2^6*MBhat^3*Ycut^5 + 33012*MBhat^4*Ycut^5 - 
         89698*m2*MBhat^4*Ycut^5 - 488880*m2^2*MBhat^4*Ycut^5 + 
         217665*m2^3*MBhat^4*Ycut^5 - 67865*m2^4*MBhat^4*Ycut^5 + 
         11760*m2^5*MBhat^4*Ycut^5 - 11130*MBhat^5*Ycut^5 - 
         20790*m2*MBhat^5*Ycut^5 + 22470*m2^2*MBhat^5*Ycut^5 + 
         4410*m2^3*MBhat^5*Ycut^5 + 840*MBhat^6*Ycut^5 + 
         11620*m2*MBhat^6*Ycut^5 + 28280*m2^2*MBhat^6*Ycut^5 - 
         4410*m2^3*MBhat^6*Ycut^5 + 162*Ycut^6 - 525*m2*Ycut^6 - 
         33390*m2^2*Ycut^6 - 98910*m2^3*Ycut^6 - 60795*m2^4*Ycut^6 - 
         2667*m2^5*Ycut^6 + 105*m2^6*Ycut^6 + 4368*MBhat*Ycut^6 - 
         1806*m2*MBhat*Ycut^6 - 20496*m2^2*MBhat*Ycut^6 + 
         28644*m2^3*MBhat*Ycut^6 - 10416*m2^4*MBhat*Ycut^6 - 
         294*m2^5*MBhat*Ycut^6 + 16536*MBhat^2*Ycut^6 - 
         15001*m2*MBhat^2*Ycut^6 + 173026*m2^2*MBhat^2*Ycut^6 + 
         171521*m2^3*MBhat^2*Ycut^6 - 42854*m2^4*MBhat^2*Ycut^6 + 
         6818*m2^5*MBhat^2*Ycut^6 - 770*m2^6*MBhat^2*Ycut^6 - 
         1284*MBhat^3*Ycut^6 - 73052*m2*MBhat^3*Ycut^6 - 
         179368*m2^2*MBhat^3*Ycut^6 - 13776*m2^3*MBhat^3*Ycut^6 - 
         39844*m2^4*MBhat^3*Ycut^6 + 13636*m2^5*MBhat^3*Ycut^6 - 
         2520*m2^6*MBhat^3*Ycut^6 - 34482*MBhat^4*Ycut^6 + 
         36050*m2*MBhat^4*Ycut^6 + 160160*m2^2*MBhat^4*Ycut^6 - 
         81585*m2^3*MBhat^4*Ycut^6 + 17395*m2^4*MBhat^4*Ycut^6 - 
         3920*m2^5*MBhat^4*Ycut^6 + 19740*MBhat^5*Ycut^6 + 
         28350*m2*MBhat^5*Ycut^6 - 26040*m2^2*MBhat^5*Ycut^6 - 
         7350*m2^3*MBhat^5*Ycut^6 - 5040*MBhat^6*Ycut^6 - 
         5180*m2*MBhat^6*Ycut^6 - 11760*m2^2*MBhat^6*Ycut^6 + 
         1470*m2^3*MBhat^6*Ycut^6 - 946*Ycut^7 + 83*m2*Ycut^7 + 
         5963*m2^2*Ycut^7 - 1877*m2^3*Ycut^7 + 5473*m2^4*Ycut^7 + 
         769*m2^5*Ycut^7 - 15*m2^6*Ycut^7 - 11088*MBhat*Ycut^7 + 
         5922*m2*MBhat*Ycut^7 + 23856*m2^2*MBhat*Ycut^7 - 
         20580*m2^3*MBhat*Ycut^7 + 1344*m2^4*MBhat*Ycut^7 + 
         546*m2^5*MBhat*Ycut^7 - 16494*MBhat^2*Ycut^7 + 
         34613*m2*MBhat^2*Ycut^7 - 12441*m2^2*MBhat^2*Ycut^7 - 
         31670*m2^3*MBhat^2*Ycut^7 + 11506*m2^4*MBhat^2*Ycut^7 - 
         534*m2^5*MBhat^2*Ycut^7 + 110*m2^6*MBhat^2*Ycut^7 + 
         22158*MBhat^3*Ycut^7 + 38230*m2*MBhat^3*Ycut^7 - 
         56718*m2^2*MBhat^3*Ycut^7 + 10986*m2^3*MBhat^3*Ycut^7 + 
         7052*m2^4*MBhat^3*Ycut^7 - 1068*m2^5*MBhat^3*Ycut^7 + 
         360*m2^6*MBhat^3*Ycut^7 + 19320*MBhat^4*Ycut^7 - 
         28000*m2*MBhat^4*Ycut^7 - 39830*m2^2*MBhat^4*Ycut^7 + 
         26355*m2^3*MBhat^4*Ycut^7 - 245*m2^4*MBhat^4*Ycut^7 + 
         560*m2^5*MBhat^4*Ycut^7 - 19950*MBhat^5*Ycut^7 - 
         21000*m2*MBhat^5*Ycut^7 + 21000*m2^2*MBhat^5*Ycut^7 + 
         3570*m2^3*MBhat^5*Ycut^7 + 7000*MBhat^6*Ycut^7 + 
         3500*m2*MBhat^6*Ycut^7 + 3080*m2^2*MBhat^6*Ycut^7 - 
         210*m2^3*MBhat^6*Ycut^7 + 2589*Ycut^8 - 415*m2*Ycut^8 - 
         6317*m2^2*Ycut^8 + 4161*m2^3*Ycut^8 + 1129*m2^4*Ycut^8 - 
         97*m2^5*Ycut^8 + 14784*MBhat*Ycut^8 - 9870*m2*MBhat*Ycut^8 - 
         11424*m2^2*MBhat*Ycut^8 + 7476*m2^3*MBhat*Ycut^8 - 
         840*m2^4*MBhat*Ycut^8 - 126*m2^5*MBhat*Ycut^8 + 
         2532*MBhat^2*Ycut^8 - 29005*m2*MBhat^2*Ycut^8 + 
         13574*m2^2*MBhat^2*Ycut^8 + 8364*m2^3*MBhat^2*Ycut^8 - 
         1865*m2^4*MBhat^2*Ycut^8 - 110*m2^5*MBhat^2*Ycut^8 - 
         26520*MBhat^3*Ycut^8 - 8870*m2*MBhat^3*Ycut^8 + 
         34792*m2^2*MBhat^3*Ycut^8 - 5742*m2^3*MBhat^3*Ycut^8 - 
         1000*m2^4*MBhat^3*Ycut^8 - 220*m2^5*MBhat^3*Ycut^8 + 
         945*MBhat^4*Ycut^8 + 19250*m2*MBhat^4*Ycut^8 + 10290*m2^2*MBhat^4*
          Ycut^8 - 7245*m2^3*MBhat^4*Ycut^8 - 560*m2^4*MBhat^4*Ycut^8 + 
         10920*MBhat^5*Ycut^8 + 7560*m2*MBhat^5*Ycut^8 - 
         9240*m2^2*MBhat^5*Ycut^8 - 630*m2^3*MBhat^5*Ycut^8 - 
         5250*MBhat^6*Ycut^8 - 1750*m2*MBhat^6*Ycut^8 - 
         350*m2^2*MBhat^6*Ycut^8 - 3900*Ycut^9 + 830*m2*Ycut^9 + 
         3648*m2^2*Ycut^9 - 276*m2^3*Ycut^9 - 92*m2^4*Ycut^9 - 
         10500*MBhat*Ycut^9 + 9030*m2*MBhat*Ycut^9 + 1596*m2^2*MBhat*Ycut^9 - 
         2688*m2^3*MBhat*Ycut^9 + 42*m2^4*MBhat*Ycut^9 + 
         9350*MBhat^2*Ycut^9 + 12195*m2*MBhat^2*Ycut^9 - 
         8251*m2^2*MBhat^2*Ycut^9 - 867*m2^3*MBhat^2*Ycut^9 + 
         173*m2^4*MBhat^2*Ycut^9 + 12890*MBhat^3*Ycut^9 - 
         4800*m2*MBhat^3*Ycut^9 - 8228*m2^2*MBhat^3*Ycut^9 + 
         590*m2^3*MBhat^3*Ycut^9 + 220*m2^4*MBhat^3*Ycut^9 - 
         7630*MBhat^4*Ycut^9 - 6755*m2*MBhat^4*Ycut^9 - 
         875*m2^2*MBhat^4*Ycut^9 + 1190*m2^3*MBhat^4*Ycut^9 - 
         2310*MBhat^5*Ycut^9 - 630*m2*MBhat^5*Ycut^9 + 1470*m2^2*MBhat^5*
          Ycut^9 + 2100*MBhat^6*Ycut^9 + 350*m2*MBhat^6*Ycut^9 + 
         3485*Ycut^10 - 830*m2*Ycut^10 - 857*m2^2*Ycut^10 + 92*m2^3*Ycut^10 + 
         3024*MBhat*Ycut^10 - 4410*m2*MBhat*Ycut^10 + 336*m2^2*MBhat*
          Ycut^10 + 168*m2^3*MBhat*Ycut^10 - 8514*MBhat^2*Ycut^10 - 
         1415*m2*MBhat^2*Ycut^10 + 2094*m2^2*MBhat^2*Ycut^10 + 
         107*m2^3*MBhat^2*Ycut^10 - 900*MBhat^3*Ycut^10 + 
         3288*m2*MBhat^3*Ycut^10 + 520*m2^2*MBhat^3*Ycut^10 - 
         10*m2^3*MBhat^3*Ycut^10 + 3675*MBhat^4*Ycut^10 + 
         805*m2*MBhat^4*Ycut^10 - 70*m2^2*MBhat^4*Ycut^10 - 
         420*MBhat^5*Ycut^10 - 210*m2*MBhat^5*Ycut^10 - 350*MBhat^6*Ycut^10 - 
         1842*Ycut^11 + 415*m2*Ycut^11 + 83*m2^2*Ycut^11 + 
         672*MBhat*Ycut^11 + 966*m2*MBhat*Ycut^11 - 168*m2^2*MBhat*Ycut^11 + 
         2742*MBhat^2*Ycut^11 - 521*m2*MBhat^2*Ycut^11 - 
         107*m2^2*MBhat^2*Ycut^11 - 1362*MBhat^3*Ycut^11 - 
         510*m2*MBhat^3*Ycut^11 + 10*m2^2*MBhat^3*Ycut^11 - 
         420*MBhat^4*Ycut^11 + 70*m2*MBhat^4*Ycut^11 + 210*MBhat^5*Ycut^11 + 
         531*Ycut^12 - 83*m2*Ycut^12 - 672*MBhat*Ycut^12 - 
         42*m2*MBhat*Ycut^12 - 121*MBhat^2*Ycut^12 + 107*m2*MBhat^2*Ycut^12 + 
         332*MBhat^3*Ycut^12 - 10*m2*MBhat^3*Ycut^12 - 70*MBhat^4*Ycut^12 - 
         64*Ycut^13 + 126*MBhat*Ycut^13 - 72*MBhat^2*Ycut^13 + 
         10*MBhat^3*Ycut^13))/(-1 + Ycut)^7 + 
     m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 108*MBhat^2 - 178*m2*MBhat^2 + 
       2*m2^2*MBhat^2 + 168*MBhat^3 + 104*m2*MBhat^3 - 81*MBhat^4 - 
       m2*MBhat^4 + 6*MBhat^6)*Log[m2] - 
     m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 108*MBhat^2 - 178*m2*MBhat^2 + 
       2*m2^2*MBhat^2 + 168*MBhat^3 + 104*m2*MBhat^3 - 81*MBhat^4 - 
       m2*MBhat^4 + 6*MBhat^6)*Log[1 - Ycut] + 
     c[SL]^2*(-1/840*((-1 + m2 + Ycut)*(12 - 408*m2 - 13596*m2^2 - 
           32496*m2^3 - 13596*m2^4 - 408*m2^5 + 12*m2^6 - 249*MBhat^2 + 
           5127*m2*MBhat^2 + 68442*m2^2*MBhat^2 + 36942*m2^3*MBhat^2 - 
           2433*m2^4*MBhat^2 + 591*m2^5*MBhat^2 - 60*m2^6*MBhat^2 + 
           720*MBhat^3 - 11040*m2*MBhat^3 - 80760*m2^2*MBhat^3 - 
           9360*m2^3*MBhat^3 - 960*m2^4*MBhat^3 + 720*m2^5*MBhat^3 - 
           120*m2^6*MBhat^3 - 588*MBhat^4 + 6972*m2*MBhat^4 + 
           27762*m2^2*MBhat^4 - 2478*m2^3*MBhat^4 + 1302*m2^4*MBhat^4 - 
           210*m2^5*MBhat^4 + 105*MBhat^6 - 735*m2*MBhat^6 - 
           735*m2^2*MBhat^6 + 105*m2^3*MBhat^6 - 72*Ycut + 2460*m2*Ycut + 
           86220*m2^2*Ycut + 211224*m2^3*Ycut + 90528*m2^4*Ycut + 
           2844*m2^5*Ycut - 84*m2^6*Ycut + 1494*MBhat^2*Ycut - 
           31011*m2*MBhat^2*Ycut - 439794*m2^2*MBhat^2*Ycut - 
           247872*m2^3*MBhat^2*Ycut + 16500*m2^4*MBhat^2*Ycut - 
           4077*m2^5*MBhat^2*Ycut + 420*m2^6*MBhat^2*Ycut - 
           4320*MBhat^3*Ycut + 66960*m2*MBhat^3*Ycut + 524640*m2^2*MBhat^3*
            Ycut + 65880*m2^3*MBhat^3*Ycut + 6120*m2^4*MBhat^3*Ycut - 
           4920*m2^5*MBhat^3*Ycut + 840*m2^6*MBhat^3*Ycut + 
           3528*MBhat^4*Ycut - 42420*m2*MBhat^4*Ycut - 182868*m2^2*MBhat^4*
            Ycut + 16254*m2^3*MBhat^4*Ycut - 8904*m2^4*MBhat^4*Ycut + 
           1470*m2^5*MBhat^4*Ycut - 630*MBhat^6*Ycut + 4515*m2*MBhat^6*Ycut + 
           5040*m2^2*MBhat^6*Ycut - 735*m2^3*MBhat^6*Ycut + 180*Ycut^2 - 
           6180*m2*Ycut^2 - 229668*m2^2*Ycut^2 - 579144*m2^3*Ycut^2 - 
           255516*m2^4*Ycut^2 - 8484*m2^5*Ycut^2 + 252*m2^6*Ycut^2 - 
           3735*MBhat^2*Ycut^2 + 78150*m2*MBhat^2*Ycut^2 + 
           1189101*m2^2*MBhat^2*Ycut^2 + 705609*m2^3*MBhat^2*Ycut^2 - 
           47436*m2^4*MBhat^2*Ycut^2 + 11991*m2^5*MBhat^2*Ycut^2 - 
           1260*m2^6*MBhat^2*Ycut^2 + 10800*MBhat^3*Ycut^2 - 
           169200*m2*MBhat^3*Ycut^2 - 1436280*m2^2*MBhat^3*Ycut^2 - 
           198600*m2^3*MBhat^3*Ycut^2 - 16080*m2^4*MBhat^3*Ycut^2 + 
           14280*m2^5*MBhat^3*Ycut^2 - 2520*m2^6*MBhat^3*Ycut^2 - 
           8820*MBhat^4*Ycut^2 + 107520*m2*MBhat^4*Ycut^2 + 
           508662*m2^2*MBhat^4*Ycut^2 - 44604*m2^3*MBhat^4*Ycut^2 + 
           25872*m2^4*MBhat^4*Ycut^2 - 4410*m2^5*MBhat^4*Ycut^2 + 
           1575*MBhat^6*Ycut^2 - 11550*m2*MBhat^6*Ycut^2 - 
           14700*m2^2*MBhat^6*Ycut^2 + 2205*m2^3*MBhat^6*Ycut^2 - 
           240*Ycut^3 + 8280*m2*Ycut^3 + 330072*m2^2*Ycut^3 + 
           861828*m2^3*Ycut^3 + 394212*m2^4*Ycut^3 + 14028*m2^5*Ycut^3 - 
           420*m2^6*Ycut^3 + 4980*MBhat^2*Ycut^3 - 105030*m2*MBhat^2*Ycut^3 - 
           1738644*m2^2*MBhat^2*Ycut^3 - 1098975*m2^3*MBhat^2*Ycut^3 + 
           74424*m2^4*MBhat^2*Ycut^3 - 19425*m2^5*MBhat^2*Ycut^3 + 
           2100*m2^6*MBhat^2*Ycut^3 - 15870*MBhat^3*Ycut^3 + 
           232410*m2*MBhat^3*Ycut^3 + 2127780*m2^2*MBhat^3*Ycut^3 + 
           328980*m2^3*MBhat^3*Ycut^3 + 26250*m2^4*MBhat^3*Ycut^3 - 
           24150*m2^5*MBhat^3*Ycut^3 + 4200*m2^6*MBhat^3*Ycut^3 + 
           14280*MBhat^4*Ycut^3 - 150360*m2*MBhat^4*Ycut^3 - 
           768768*m2^2*MBhat^4*Ycut^3 + 70308*m2^3*MBhat^4*Ycut^3 - 
           43680*m2^4*MBhat^4*Ycut^3 + 7350*m2^5*MBhat^4*Ycut^3 - 
           1050*MBhat^5*Ycut^3 + 1050*m2*MBhat^5*Ycut^3 + 1050*m2^2*MBhat^5*
            Ycut^3 - 1050*m2^3*MBhat^5*Ycut^3 - 2100*MBhat^6*Ycut^3 + 
           15750*m2*MBhat^6*Ycut^3 + 23520*m2^2*MBhat^6*Ycut^3 - 
           3675*m2^3*MBhat^6*Ycut^3 + 180*Ycut^4 - 6240*m2*Ycut^4 - 
           271728*m2^2*Ycut^4 - 741300*m2^3*Ycut^4 - 355488*m2^4*Ycut^4 - 
           13860*m2^5*Ycut^4 + 420*m2^6*Ycut^4 - 1530*MBhat^2*Ycut^4 + 
           72780*m2*MBhat^2*Ycut^4 + 1465551*m2^2*MBhat^2*Ycut^4 + 
           1005816*m2^3*MBhat^2*Ycut^4 - 74445*m2^4*MBhat^2*Ycut^4 + 
           20790*m2^5*MBhat^2*Ycut^4 - 2100*m2^6*MBhat^2*Ycut^4 + 
           16960*MBhat^3*Ycut^4 - 192050*m2*MBhat^3*Ycut^4 - 
           1803740*m2^2*MBhat^3*Ycut^4 - 332080*m2^3*MBhat^3*Ycut^4 - 
           21700*m2^4*MBhat^3*Ycut^4 + 23450*m2^5*MBhat^3*Ycut^4 - 
           4200*m2^6*MBhat^3*Ycut^4 - 24570*MBhat^4*Ycut^4 + 
           139440*m2*MBhat^4*Ycut^4 + 665952*m2^2*MBhat^4*Ycut^4 - 
           64260*m2^3*MBhat^4*Ycut^4 + 42630*m2^4*MBhat^4*Ycut^4 - 
           7350*m2^5*MBhat^4*Ycut^4 + 7560*MBhat^5*Ycut^4 - 
           6090*m2*MBhat^5*Ycut^4 - 2520*m2^2*MBhat^5*Ycut^4 + 
           1050*m2^3*MBhat^5*Ycut^4 + 1400*MBhat^6*Ycut^4 - 
           12250*m2*MBhat^6*Ycut^4 - 22750*m2^2*MBhat^6*Ycut^4 + 
           3675*m2^3*MBhat^6*Ycut^4 - 72*Ycut^5 + 2508*m2*Ycut^5 + 
           123396*m2^2*Ycut^5 + 357336*m2^3*Ycut^5 + 183288*m2^4*Ycut^5 + 
           8148*m2^5*Ycut^5 - 252*m2^6*Ycut^5 - 1323*MBhat*Ycut^5 + 
           3969*m2*MBhat*Ycut^5 - 2646*m2^2*MBhat*Ycut^5 - 
           2646*m2^3*MBhat*Ycut^5 + 3969*m2^4*MBhat*Ycut^5 - 
           1323*m2^5*MBhat*Ycut^5 - 11484*MBhat^2*Ycut^5 + 
           3336*m2*MBhat^2*Ycut^5 - 708078*m2^2*MBhat^2*Ycut^5 - 
           523782*m2^3*MBhat^2*Ycut^5 + 42168*m2^4*MBhat^2*Ycut^5 - 
           11634*m2^5*MBhat^2*Ycut^5 + 1260*m2^6*MBhat^2*Ycut^5 - 
           7386*MBhat^3*Ycut^5 + 93514*m2*MBhat^3*Ycut^5 + 
           832664*m2^2*MBhat^3*Ycut^5 + 205464*m2^3*MBhat^3*Ycut^5 + 
           10514*m2^4*MBhat^3*Ycut^5 - 13370*m2^5*MBhat^3*Ycut^5 + 
           2520*m2^6*MBhat^3*Ycut^5 + 42840*MBhat^4*Ycut^5 - 
           113190*m2*MBhat^4*Ycut^5 - 309078*m2^2*MBhat^4*Ycut^5 + 
           32382*m2^3*MBhat^4*Ycut^5 - 22638*m2^4*MBhat^4*Ycut^5 + 
           4410*m2^5*MBhat^4*Ycut^5 - 22995*MBhat^5*Ycut^5 + 
           15015*m2*MBhat^5*Ycut^5 + 2415*m2^2*MBhat^5*Ycut^5 + 
           2205*m2^3*MBhat^5*Ycut^5 + 420*MBhat^6*Ycut^5 + 
           5810*m2*MBhat^6*Ycut^5 + 14140*m2^2*MBhat^6*Ycut^5 - 
           2205*m2^3*MBhat^6*Ycut^5 + 306*Ycut^6 - 1302*m2*Ycut^6 - 
           24948*m2^2*Ycut^6 - 80892*m2^3*Ycut^6 - 47754*m2^4*Ycut^6 - 
           2310*m2^5*Ycut^6 + 84*m2^6*Ycut^6 + 8778*MBhat*Ycut^6 - 
           22785*m2*MBhat*Ycut^6 + 15834*m2^2*MBhat*Ycut^6 + 
           1428*m2^3*MBhat*Ycut^6 - 3108*m2^4*MBhat*Ycut^6 - 
           147*m2^5*MBhat*Ycut^6 + 28878*MBhat^2*Ycut^6 - 68166*m2*MBhat^2*
            Ycut^6 + 203721*m2^2*MBhat^2*Ycut^6 + 125139*m2^3*MBhat^2*
            Ycut^6 - 11508*m2^4*MBhat^2*Ycut^6 + 3318*m2^5*MBhat^2*Ycut^6 - 
           420*m2^6*MBhat^2*Ycut^6 - 26412*MBhat^3*Ycut^6 + 
           3892*m2*MBhat^3*Ycut^6 - 167034*m2^2*MBhat^3*Ycut^6 - 
           69090*m2^3*MBhat^3*Ycut^6 - 5866*m2^4*MBhat^3*Ycut^6 + 
           3990*m2^5*MBhat^3*Ycut^6 - 840*m2^6*MBhat^3*Ycut^6 - 
           47040*MBhat^4*Ycut^6 + 91140*m2*MBhat^4*Ycut^6 + 
           51072*m2^2*MBhat^4*Ycut^6 - 11466*m2^3*MBhat^4*Ycut^6 + 
           5586*m2^4*MBhat^4*Ycut^6 - 1470*m2^5*MBhat^4*Ycut^6 + 
           38010*MBhat^5*Ycut^6 - 20475*m2*MBhat^5*Ycut^6 - 
           2940*m2^2*MBhat^5*Ycut^6 - 3675*m2^3*MBhat^5*Ycut^6 - 
           2520*MBhat^6*Ycut^6 - 2590*m2*MBhat^6*Ycut^6 - 5880*m2^2*MBhat^6*
            Ycut^6 + 735*m2^3*MBhat^6*Ycut^6 - 2084*Ycut^7 + 5266*m2*Ycut^7 - 
           2756*m2^2*Ycut^7 + 3152*m2^3*Ycut^7 + 3488*m2^4*Ycut^7 + 
           506*m2^5*Ycut^7 - 12*m2^6*Ycut^7 - 24255*MBhat*Ycut^7 + 
           53508*m2*MBhat*Ycut^7 - 34293*m2^2*MBhat*Ycut^7 + 
           5355*m2^3*MBhat*Ycut^7 - 588*m2^4*MBhat*Ycut^7 + 
           273*m2^5*MBhat*Ycut^7 - 26064*MBhat^2*Ycut^7 + 69150*m2*MBhat^2*
            Ycut^7 - 58824*m2^2*MBhat^2*Ycut^7 - 885*m2^3*MBhat^2*Ycut^7 + 
           3252*m2^4*MBhat^2*Ycut^7 - 234*m2^5*MBhat^2*Ycut^7 + 
           60*m2^6*MBhat^2*Ycut^7 + 67488*MBhat^3*Ycut^7 - 
           68270*m2*MBhat^3*Ycut^7 + 5566*m2^2*MBhat^3*Ycut^7 + 
           10956*m2^3*MBhat^3*Ycut^7 + 2150*m2^4*MBhat^3*Ycut^7 - 
           370*m2^5*MBhat^3*Ycut^7 + 120*m2^6*MBhat^3*Ycut^7 + 
           17640*MBhat^4*Ycut^7 - 53130*m2*MBhat^4*Ycut^7 + 
           12432*m2^2*MBhat^4*Ycut^7 + 5166*m2^3*MBhat^4*Ycut^7 + 
           42*m2^4*MBhat^4*Ycut^7 + 210*m2^5*MBhat^4*Ycut^7 - 
           36225*MBhat^5*Ycut^7 + 16800*m2*MBhat^5*Ycut^7 + 
           3780*m2^2*MBhat^5*Ycut^7 + 1785*m2^3*MBhat^5*Ycut^7 + 
           3500*MBhat^6*Ycut^7 + 1750*m2*MBhat^6*Ycut^7 + 1540*m2^2*MBhat^6*
            Ycut^7 - 105*m2^3*MBhat^6*Ycut^7 + 6330*Ycut^8 - 
           13100*m2*Ycut^8 + 7664*m2^2*Ycut^8 - 804*m2^3*Ycut^8 + 
           794*m2^4*Ycut^8 - 44*m2^5*Ycut^8 + 35280*MBhat*Ycut^8 - 
           64680*m2*MBhat*Ycut^8 + 34272*m2^2*MBhat*Ycut^8 - 
           4473*m2^3*MBhat*Ycut^8 - 336*m2^4*MBhat*Ycut^8 - 
           63*m2^5*MBhat*Ycut^8 - 6345*MBhat^2*Ycut^8 - 16575*m2*MBhat^2*
            Ycut^8 + 20991*m2^2*MBhat^2*Ycut^8 - 2574*m2^3*MBhat^2*Ycut^8 - 
           582*m2^4*MBhat^2*Ycut^8 - 60*m2^5*MBhat^2*Ycut^8 - 
           69180*MBhat^3*Ycut^8 + 69820*m2*MBhat^3*Ycut^8 - 
           5464*m2^2*MBhat^3*Ycut^8 - 2628*m2^3*MBhat^3*Ycut^8 - 
           478*m2^4*MBhat^3*Ycut^8 - 50*m2^5*MBhat^3*Ycut^8 + 
           17640*MBhat^4*Ycut^8 + 13020*m2*MBhat^4*Ycut^8 - 
           6678*m2^2*MBhat^4*Ycut^8 - 1512*m2^3*MBhat^4*Ycut^8 - 
           210*m2^4*MBhat^4*Ycut^8 + 18900*MBhat^5*Ycut^8 - 
           8400*m2*MBhat^5*Ycut^8 - 2100*m2^2*MBhat^5*Ycut^8 - 
           315*m2^3*MBhat^5*Ycut^8 - 2625*MBhat^6*Ycut^8 - 
           875*m2*MBhat^6*Ycut^8 - 175*m2^2*MBhat^6*Ycut^8 - 10680*Ycut^9 + 
           17380*m2*Ycut^9 - 7716*m2^2*Ycut^9 + 1140*m2^3*Ycut^9 + 
           44*m2^4*Ycut^9 - 27195*MBhat*Ycut^9 + 40425*m2*MBhat*Ycut^9 - 
           15708*m2^2*MBhat*Ycut^9 + 399*m2^3*MBhat*Ycut^9 + 
           63*m2^4*MBhat*Ycut^9 + 33870*MBhat^2*Ycut^9 - 20925*m2*MBhat^2*
            Ycut^9 - 1824*m2^2*MBhat^2*Ycut^9 + 642*m2^3*MBhat^2*Ycut^9 + 
           60*m2^4*MBhat^2*Ycut^9 + 30360*MBhat^3*Ycut^9 - 
           30650*m2*MBhat^3*Ycut^9 + 3156*m2^2*MBhat^3*Ycut^9 + 
           528*m2^3*MBhat^3*Ycut^9 + 50*m2^4*MBhat^3*Ycut^9 - 
           23520*MBhat^4*Ycut^9 + 2730*m2*MBhat^4*Ycut^9 + 
           1722*m2^2*MBhat^4*Ycut^9 + 210*m2^3*MBhat^4*Ycut^9 - 
           3885*MBhat^5*Ycut^9 + 2415*m2*MBhat^5*Ycut^9 + 315*m2^2*MBhat^5*
            Ycut^9 + 1050*MBhat^6*Ycut^9 + 175*m2*MBhat^6*Ycut^9 + 
           10810*Ycut^10 - 12970*m2*Ycut^10 + 3716*m2^2*Ycut^10 - 
           44*m2^3*Ycut^10 + 7938*MBhat*Ycut^10 - 9849*m2*MBhat*Ycut^10 + 
           2478*m2^2*MBhat*Ycut^10 - 63*m2^3*MBhat*Ycut^10 - 
           28647*MBhat^2*Ycut^10 + 16872*m2*MBhat^2*Ycut^10 - 
           702*m2^2*MBhat^2*Ycut^10 - 60*m2^3*MBhat^2*Ycut^10 + 
           876*MBhat^3*Ycut^10 + 4036*m2*MBhat^3*Ycut^10 - 
           578*m2^2*MBhat^3*Ycut^10 - 50*m2^3*MBhat^3*Ycut^10 + 
           9828*MBhat^4*Ycut^10 - 1932*m2*MBhat^4*Ycut^10 - 
           210*m2^2*MBhat^4*Ycut^10 - 630*MBhat^5*Ycut^10 - 
           315*m2*MBhat^5*Ycut^10 - 175*MBhat^6*Ycut^10 - 6564*Ycut^11 + 
           5162*m2*Ycut^11 - 656*m2^2*Ycut^11 + 3087*MBhat*Ycut^11 - 
           1470*m2*MBhat*Ycut^11 + 63*m2^2*MBhat*Ycut^11 + 
           9468*MBhat^2*Ycut^11 - 3648*m2*MBhat^2*Ycut^11 + 
           60*m2^2*MBhat^2*Ycut^11 - 5298*MBhat^3*Ycut^11 + 
           628*m2*MBhat^3*Ycut^11 + 50*m2^2*MBhat^3*Ycut^11 - 
           1008*MBhat^4*Ycut^11 + 210*m2*MBhat^4*Ycut^11 + 
           315*MBhat^5*Ycut^11 + 2214*Ycut^12 - 856*m2*Ycut^12 - 
           2940*MBhat*Ycut^12 + 882*m2*MBhat*Ycut^12 - 276*MBhat^2*Ycut^12 - 
           60*m2*MBhat^2*Ycut^12 + 1212*MBhat^3*Ycut^12 - 
           50*m2*MBhat^3*Ycut^12 - 210*MBhat^4*Ycut^12 - 320*Ycut^13 + 
           630*MBhat*Ycut^13 - 360*MBhat^2*Ycut^13 + 50*MBhat^3*Ycut^13))/
         (-1 + Ycut)^7 + (3*m2^2*(4 + 20*m2 + 20*m2^2 + 4*m2^3 - 27*MBhat^2 - 
          52*m2*MBhat^2 - 7*m2^2*MBhat^2 + 40*MBhat^3 + 40*m2*MBhat^3 - 
          18*MBhat^4 - 8*m2*MBhat^4 + MBhat^6)*Log[m2])/2 - 
       (3*m2^2*(4 + 20*m2 + 20*m2^2 + 4*m2^3 - 27*MBhat^2 - 52*m2*MBhat^2 - 
          7*m2^2*MBhat^2 + 40*MBhat^3 + 40*m2*MBhat^3 - 18*MBhat^4 - 
          8*m2*MBhat^4 + MBhat^6)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/840*((-1 + m2 + Ycut)*(12 - 408*m2 - 13596*m2^2 - 
           32496*m2^3 - 13596*m2^4 - 408*m2^5 + 12*m2^6 - 249*MBhat^2 + 
           5127*m2*MBhat^2 + 68442*m2^2*MBhat^2 + 36942*m2^3*MBhat^2 - 
           2433*m2^4*MBhat^2 + 591*m2^5*MBhat^2 - 60*m2^6*MBhat^2 + 
           720*MBhat^3 - 11040*m2*MBhat^3 - 80760*m2^2*MBhat^3 - 
           9360*m2^3*MBhat^3 - 960*m2^4*MBhat^3 + 720*m2^5*MBhat^3 - 
           120*m2^6*MBhat^3 - 588*MBhat^4 + 6972*m2*MBhat^4 + 
           27762*m2^2*MBhat^4 - 2478*m2^3*MBhat^4 + 1302*m2^4*MBhat^4 - 
           210*m2^5*MBhat^4 + 105*MBhat^6 - 735*m2*MBhat^6 - 
           735*m2^2*MBhat^6 + 105*m2^3*MBhat^6 - 72*Ycut + 2460*m2*Ycut + 
           86220*m2^2*Ycut + 211224*m2^3*Ycut + 90528*m2^4*Ycut + 
           2844*m2^5*Ycut - 84*m2^6*Ycut + 1494*MBhat^2*Ycut - 
           31011*m2*MBhat^2*Ycut - 439794*m2^2*MBhat^2*Ycut - 
           247872*m2^3*MBhat^2*Ycut + 16500*m2^4*MBhat^2*Ycut - 
           4077*m2^5*MBhat^2*Ycut + 420*m2^6*MBhat^2*Ycut - 
           4320*MBhat^3*Ycut + 66960*m2*MBhat^3*Ycut + 524640*m2^2*MBhat^3*
            Ycut + 65880*m2^3*MBhat^3*Ycut + 6120*m2^4*MBhat^3*Ycut - 
           4920*m2^5*MBhat^3*Ycut + 840*m2^6*MBhat^3*Ycut + 
           3528*MBhat^4*Ycut - 42420*m2*MBhat^4*Ycut - 182868*m2^2*MBhat^4*
            Ycut + 16254*m2^3*MBhat^4*Ycut - 8904*m2^4*MBhat^4*Ycut + 
           1470*m2^5*MBhat^4*Ycut - 630*MBhat^6*Ycut + 4515*m2*MBhat^6*Ycut + 
           5040*m2^2*MBhat^6*Ycut - 735*m2^3*MBhat^6*Ycut + 180*Ycut^2 - 
           6180*m2*Ycut^2 - 229668*m2^2*Ycut^2 - 579144*m2^3*Ycut^2 - 
           255516*m2^4*Ycut^2 - 8484*m2^5*Ycut^2 + 252*m2^6*Ycut^2 - 
           3735*MBhat^2*Ycut^2 + 78150*m2*MBhat^2*Ycut^2 + 
           1189101*m2^2*MBhat^2*Ycut^2 + 705609*m2^3*MBhat^2*Ycut^2 - 
           47436*m2^4*MBhat^2*Ycut^2 + 11991*m2^5*MBhat^2*Ycut^2 - 
           1260*m2^6*MBhat^2*Ycut^2 + 10800*MBhat^3*Ycut^2 - 
           169200*m2*MBhat^3*Ycut^2 - 1436280*m2^2*MBhat^3*Ycut^2 - 
           198600*m2^3*MBhat^3*Ycut^2 - 16080*m2^4*MBhat^3*Ycut^2 + 
           14280*m2^5*MBhat^3*Ycut^2 - 2520*m2^6*MBhat^3*Ycut^2 - 
           8820*MBhat^4*Ycut^2 + 107520*m2*MBhat^4*Ycut^2 + 
           508662*m2^2*MBhat^4*Ycut^2 - 44604*m2^3*MBhat^4*Ycut^2 + 
           25872*m2^4*MBhat^4*Ycut^2 - 4410*m2^5*MBhat^4*Ycut^2 + 
           1575*MBhat^6*Ycut^2 - 11550*m2*MBhat^6*Ycut^2 - 
           14700*m2^2*MBhat^6*Ycut^2 + 2205*m2^3*MBhat^6*Ycut^2 - 
           240*Ycut^3 + 8280*m2*Ycut^3 + 330072*m2^2*Ycut^3 + 
           861828*m2^3*Ycut^3 + 394212*m2^4*Ycut^3 + 14028*m2^5*Ycut^3 - 
           420*m2^6*Ycut^3 + 4980*MBhat^2*Ycut^3 - 105030*m2*MBhat^2*Ycut^3 - 
           1738644*m2^2*MBhat^2*Ycut^3 - 1098975*m2^3*MBhat^2*Ycut^3 + 
           74424*m2^4*MBhat^2*Ycut^3 - 19425*m2^5*MBhat^2*Ycut^3 + 
           2100*m2^6*MBhat^2*Ycut^3 - 15870*MBhat^3*Ycut^3 + 
           232410*m2*MBhat^3*Ycut^3 + 2127780*m2^2*MBhat^3*Ycut^3 + 
           328980*m2^3*MBhat^3*Ycut^3 + 26250*m2^4*MBhat^3*Ycut^3 - 
           24150*m2^5*MBhat^3*Ycut^3 + 4200*m2^6*MBhat^3*Ycut^3 + 
           14280*MBhat^4*Ycut^3 - 150360*m2*MBhat^4*Ycut^3 - 
           768768*m2^2*MBhat^4*Ycut^3 + 70308*m2^3*MBhat^4*Ycut^3 - 
           43680*m2^4*MBhat^4*Ycut^3 + 7350*m2^5*MBhat^4*Ycut^3 - 
           1050*MBhat^5*Ycut^3 + 1050*m2*MBhat^5*Ycut^3 + 1050*m2^2*MBhat^5*
            Ycut^3 - 1050*m2^3*MBhat^5*Ycut^3 - 2100*MBhat^6*Ycut^3 + 
           15750*m2*MBhat^6*Ycut^3 + 23520*m2^2*MBhat^6*Ycut^3 - 
           3675*m2^3*MBhat^6*Ycut^3 + 180*Ycut^4 - 6240*m2*Ycut^4 - 
           271728*m2^2*Ycut^4 - 741300*m2^3*Ycut^4 - 355488*m2^4*Ycut^4 - 
           13860*m2^5*Ycut^4 + 420*m2^6*Ycut^4 - 1530*MBhat^2*Ycut^4 + 
           72780*m2*MBhat^2*Ycut^4 + 1465551*m2^2*MBhat^2*Ycut^4 + 
           1005816*m2^3*MBhat^2*Ycut^4 - 74445*m2^4*MBhat^2*Ycut^4 + 
           20790*m2^5*MBhat^2*Ycut^4 - 2100*m2^6*MBhat^2*Ycut^4 + 
           16960*MBhat^3*Ycut^4 - 192050*m2*MBhat^3*Ycut^4 - 
           1803740*m2^2*MBhat^3*Ycut^4 - 332080*m2^3*MBhat^3*Ycut^4 - 
           21700*m2^4*MBhat^3*Ycut^4 + 23450*m2^5*MBhat^3*Ycut^4 - 
           4200*m2^6*MBhat^3*Ycut^4 - 24570*MBhat^4*Ycut^4 + 
           139440*m2*MBhat^4*Ycut^4 + 665952*m2^2*MBhat^4*Ycut^4 - 
           64260*m2^3*MBhat^4*Ycut^4 + 42630*m2^4*MBhat^4*Ycut^4 - 
           7350*m2^5*MBhat^4*Ycut^4 + 7560*MBhat^5*Ycut^4 - 
           6090*m2*MBhat^5*Ycut^4 - 2520*m2^2*MBhat^5*Ycut^4 + 
           1050*m2^3*MBhat^5*Ycut^4 + 1400*MBhat^6*Ycut^4 - 
           12250*m2*MBhat^6*Ycut^4 - 22750*m2^2*MBhat^6*Ycut^4 + 
           3675*m2^3*MBhat^6*Ycut^4 - 72*Ycut^5 + 2508*m2*Ycut^5 + 
           123396*m2^2*Ycut^5 + 357336*m2^3*Ycut^5 + 183288*m2^4*Ycut^5 + 
           8148*m2^5*Ycut^5 - 252*m2^6*Ycut^5 - 1323*MBhat*Ycut^5 + 
           3969*m2*MBhat*Ycut^5 - 2646*m2^2*MBhat*Ycut^5 - 
           2646*m2^3*MBhat*Ycut^5 + 3969*m2^4*MBhat*Ycut^5 - 
           1323*m2^5*MBhat*Ycut^5 - 11484*MBhat^2*Ycut^5 + 
           3336*m2*MBhat^2*Ycut^5 - 708078*m2^2*MBhat^2*Ycut^5 - 
           523782*m2^3*MBhat^2*Ycut^5 + 42168*m2^4*MBhat^2*Ycut^5 - 
           11634*m2^5*MBhat^2*Ycut^5 + 1260*m2^6*MBhat^2*Ycut^5 - 
           7386*MBhat^3*Ycut^5 + 93514*m2*MBhat^3*Ycut^5 + 
           832664*m2^2*MBhat^3*Ycut^5 + 205464*m2^3*MBhat^3*Ycut^5 + 
           10514*m2^4*MBhat^3*Ycut^5 - 13370*m2^5*MBhat^3*Ycut^5 + 
           2520*m2^6*MBhat^3*Ycut^5 + 42840*MBhat^4*Ycut^5 - 
           113190*m2*MBhat^4*Ycut^5 - 309078*m2^2*MBhat^4*Ycut^5 + 
           32382*m2^3*MBhat^4*Ycut^5 - 22638*m2^4*MBhat^4*Ycut^5 + 
           4410*m2^5*MBhat^4*Ycut^5 - 22995*MBhat^5*Ycut^5 + 
           15015*m2*MBhat^5*Ycut^5 + 2415*m2^2*MBhat^5*Ycut^5 + 
           2205*m2^3*MBhat^5*Ycut^5 + 420*MBhat^6*Ycut^5 + 
           5810*m2*MBhat^6*Ycut^5 + 14140*m2^2*MBhat^6*Ycut^5 - 
           2205*m2^3*MBhat^6*Ycut^5 + 306*Ycut^6 - 1302*m2*Ycut^6 - 
           24948*m2^2*Ycut^6 - 80892*m2^3*Ycut^6 - 47754*m2^4*Ycut^6 - 
           2310*m2^5*Ycut^6 + 84*m2^6*Ycut^6 + 8778*MBhat*Ycut^6 - 
           22785*m2*MBhat*Ycut^6 + 15834*m2^2*MBhat*Ycut^6 + 
           1428*m2^3*MBhat*Ycut^6 - 3108*m2^4*MBhat*Ycut^6 - 
           147*m2^5*MBhat*Ycut^6 + 28878*MBhat^2*Ycut^6 - 68166*m2*MBhat^2*
            Ycut^6 + 203721*m2^2*MBhat^2*Ycut^6 + 125139*m2^3*MBhat^2*
            Ycut^6 - 11508*m2^4*MBhat^2*Ycut^6 + 3318*m2^5*MBhat^2*Ycut^6 - 
           420*m2^6*MBhat^2*Ycut^6 - 26412*MBhat^3*Ycut^6 + 
           3892*m2*MBhat^3*Ycut^6 - 167034*m2^2*MBhat^3*Ycut^6 - 
           69090*m2^3*MBhat^3*Ycut^6 - 5866*m2^4*MBhat^3*Ycut^6 + 
           3990*m2^5*MBhat^3*Ycut^6 - 840*m2^6*MBhat^3*Ycut^6 - 
           47040*MBhat^4*Ycut^6 + 91140*m2*MBhat^4*Ycut^6 + 
           51072*m2^2*MBhat^4*Ycut^6 - 11466*m2^3*MBhat^4*Ycut^6 + 
           5586*m2^4*MBhat^4*Ycut^6 - 1470*m2^5*MBhat^4*Ycut^6 + 
           38010*MBhat^5*Ycut^6 - 20475*m2*MBhat^5*Ycut^6 - 
           2940*m2^2*MBhat^5*Ycut^6 - 3675*m2^3*MBhat^5*Ycut^6 - 
           2520*MBhat^6*Ycut^6 - 2590*m2*MBhat^6*Ycut^6 - 5880*m2^2*MBhat^6*
            Ycut^6 + 735*m2^3*MBhat^6*Ycut^6 - 2084*Ycut^7 + 5266*m2*Ycut^7 - 
           2756*m2^2*Ycut^7 + 3152*m2^3*Ycut^7 + 3488*m2^4*Ycut^7 + 
           506*m2^5*Ycut^7 - 12*m2^6*Ycut^7 - 24255*MBhat*Ycut^7 + 
           53508*m2*MBhat*Ycut^7 - 34293*m2^2*MBhat*Ycut^7 + 
           5355*m2^3*MBhat*Ycut^7 - 588*m2^4*MBhat*Ycut^7 + 
           273*m2^5*MBhat*Ycut^7 - 26064*MBhat^2*Ycut^7 + 69150*m2*MBhat^2*
            Ycut^7 - 58824*m2^2*MBhat^2*Ycut^7 - 885*m2^3*MBhat^2*Ycut^7 + 
           3252*m2^4*MBhat^2*Ycut^7 - 234*m2^5*MBhat^2*Ycut^7 + 
           60*m2^6*MBhat^2*Ycut^7 + 67488*MBhat^3*Ycut^7 - 
           68270*m2*MBhat^3*Ycut^7 + 5566*m2^2*MBhat^3*Ycut^7 + 
           10956*m2^3*MBhat^3*Ycut^7 + 2150*m2^4*MBhat^3*Ycut^7 - 
           370*m2^5*MBhat^3*Ycut^7 + 120*m2^6*MBhat^3*Ycut^7 + 
           17640*MBhat^4*Ycut^7 - 53130*m2*MBhat^4*Ycut^7 + 
           12432*m2^2*MBhat^4*Ycut^7 + 5166*m2^3*MBhat^4*Ycut^7 + 
           42*m2^4*MBhat^4*Ycut^7 + 210*m2^5*MBhat^4*Ycut^7 - 
           36225*MBhat^5*Ycut^7 + 16800*m2*MBhat^5*Ycut^7 + 
           3780*m2^2*MBhat^5*Ycut^7 + 1785*m2^3*MBhat^5*Ycut^7 + 
           3500*MBhat^6*Ycut^7 + 1750*m2*MBhat^6*Ycut^7 + 1540*m2^2*MBhat^6*
            Ycut^7 - 105*m2^3*MBhat^6*Ycut^7 + 6330*Ycut^8 - 
           13100*m2*Ycut^8 + 7664*m2^2*Ycut^8 - 804*m2^3*Ycut^8 + 
           794*m2^4*Ycut^8 - 44*m2^5*Ycut^8 + 35280*MBhat*Ycut^8 - 
           64680*m2*MBhat*Ycut^8 + 34272*m2^2*MBhat*Ycut^8 - 
           4473*m2^3*MBhat*Ycut^8 - 336*m2^4*MBhat*Ycut^8 - 
           63*m2^5*MBhat*Ycut^8 - 6345*MBhat^2*Ycut^8 - 16575*m2*MBhat^2*
            Ycut^8 + 20991*m2^2*MBhat^2*Ycut^8 - 2574*m2^3*MBhat^2*Ycut^8 - 
           582*m2^4*MBhat^2*Ycut^8 - 60*m2^5*MBhat^2*Ycut^8 - 
           69180*MBhat^3*Ycut^8 + 69820*m2*MBhat^3*Ycut^8 - 
           5464*m2^2*MBhat^3*Ycut^8 - 2628*m2^3*MBhat^3*Ycut^8 - 
           478*m2^4*MBhat^3*Ycut^8 - 50*m2^5*MBhat^3*Ycut^8 + 
           17640*MBhat^4*Ycut^8 + 13020*m2*MBhat^4*Ycut^8 - 
           6678*m2^2*MBhat^4*Ycut^8 - 1512*m2^3*MBhat^4*Ycut^8 - 
           210*m2^4*MBhat^4*Ycut^8 + 18900*MBhat^5*Ycut^8 - 
           8400*m2*MBhat^5*Ycut^8 - 2100*m2^2*MBhat^5*Ycut^8 - 
           315*m2^3*MBhat^5*Ycut^8 - 2625*MBhat^6*Ycut^8 - 
           875*m2*MBhat^6*Ycut^8 - 175*m2^2*MBhat^6*Ycut^8 - 10680*Ycut^9 + 
           17380*m2*Ycut^9 - 7716*m2^2*Ycut^9 + 1140*m2^3*Ycut^9 + 
           44*m2^4*Ycut^9 - 27195*MBhat*Ycut^9 + 40425*m2*MBhat*Ycut^9 - 
           15708*m2^2*MBhat*Ycut^9 + 399*m2^3*MBhat*Ycut^9 + 
           63*m2^4*MBhat*Ycut^9 + 33870*MBhat^2*Ycut^9 - 20925*m2*MBhat^2*
            Ycut^9 - 1824*m2^2*MBhat^2*Ycut^9 + 642*m2^3*MBhat^2*Ycut^9 + 
           60*m2^4*MBhat^2*Ycut^9 + 30360*MBhat^3*Ycut^9 - 
           30650*m2*MBhat^3*Ycut^9 + 3156*m2^2*MBhat^3*Ycut^9 + 
           528*m2^3*MBhat^3*Ycut^9 + 50*m2^4*MBhat^3*Ycut^9 - 
           23520*MBhat^4*Ycut^9 + 2730*m2*MBhat^4*Ycut^9 + 
           1722*m2^2*MBhat^4*Ycut^9 + 210*m2^3*MBhat^4*Ycut^9 - 
           3885*MBhat^5*Ycut^9 + 2415*m2*MBhat^5*Ycut^9 + 315*m2^2*MBhat^5*
            Ycut^9 + 1050*MBhat^6*Ycut^9 + 175*m2*MBhat^6*Ycut^9 + 
           10810*Ycut^10 - 12970*m2*Ycut^10 + 3716*m2^2*Ycut^10 - 
           44*m2^3*Ycut^10 + 7938*MBhat*Ycut^10 - 9849*m2*MBhat*Ycut^10 + 
           2478*m2^2*MBhat*Ycut^10 - 63*m2^3*MBhat*Ycut^10 - 
           28647*MBhat^2*Ycut^10 + 16872*m2*MBhat^2*Ycut^10 - 
           702*m2^2*MBhat^2*Ycut^10 - 60*m2^3*MBhat^2*Ycut^10 + 
           876*MBhat^3*Ycut^10 + 4036*m2*MBhat^3*Ycut^10 - 
           578*m2^2*MBhat^3*Ycut^10 - 50*m2^3*MBhat^3*Ycut^10 + 
           9828*MBhat^4*Ycut^10 - 1932*m2*MBhat^4*Ycut^10 - 
           210*m2^2*MBhat^4*Ycut^10 - 630*MBhat^5*Ycut^10 - 
           315*m2*MBhat^5*Ycut^10 - 175*MBhat^6*Ycut^10 - 6564*Ycut^11 + 
           5162*m2*Ycut^11 - 656*m2^2*Ycut^11 + 3087*MBhat*Ycut^11 - 
           1470*m2*MBhat*Ycut^11 + 63*m2^2*MBhat*Ycut^11 + 
           9468*MBhat^2*Ycut^11 - 3648*m2*MBhat^2*Ycut^11 + 
           60*m2^2*MBhat^2*Ycut^11 - 5298*MBhat^3*Ycut^11 + 
           628*m2*MBhat^3*Ycut^11 + 50*m2^2*MBhat^3*Ycut^11 - 
           1008*MBhat^4*Ycut^11 + 210*m2*MBhat^4*Ycut^11 + 
           315*MBhat^5*Ycut^11 + 2214*Ycut^12 - 856*m2*Ycut^12 - 
           2940*MBhat*Ycut^12 + 882*m2*MBhat*Ycut^12 - 276*MBhat^2*Ycut^12 - 
           60*m2*MBhat^2*Ycut^12 + 1212*MBhat^3*Ycut^12 - 
           50*m2*MBhat^3*Ycut^12 - 210*MBhat^4*Ycut^12 - 320*Ycut^13 + 
           630*MBhat*Ycut^13 - 360*MBhat^2*Ycut^13 + 50*MBhat^3*Ycut^13))/
         (-1 + Ycut)^7 + (3*m2^2*(4 + 20*m2 + 20*m2^2 + 4*m2^3 - 27*MBhat^2 - 
          52*m2*MBhat^2 - 7*m2^2*MBhat^2 + 40*MBhat^3 + 40*m2*MBhat^3 - 
          18*MBhat^4 - 8*m2*MBhat^4 + MBhat^6)*Log[m2])/2 - 
       (3*m2^2*(4 + 20*m2 + 20*m2^2 + 4*m2^3 - 27*MBhat^2 - 52*m2*MBhat^2 - 
          7*m2^2*MBhat^2 + 40*MBhat^3 + 40*m2*MBhat^3 - 18*MBhat^4 - 
          8*m2*MBhat^4 + MBhat^6)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(-15 + 510*m2 + 16995*m2^2 + 40620*m2^3 + 
          16995*m2^4 + 510*m2^5 - 15*m2^6 + 362*MBhat^2 - 7226*m2*MBhat^2 - 
          87866*m2^2*MBhat^2 - 30116*m2^3*MBhat^2 + 6634*m2^4*MBhat^2 - 
          1178*m2^5*MBhat^2 + 110*m2^6*MBhat^2 - 1264*MBhat^3 + 
          17888*m2*MBhat^3 + 99368*m2^2*MBhat^3 - 7312*m2^3*MBhat^3 + 
          7808*m2^4*MBhat^3 - 2608*m2^5*MBhat^3 + 360*m2^6*MBhat^3 + 
          1127*MBhat^4 - 12418*m2*MBhat^4 - 32578*m2^2*MBhat^4 + 
          12782*m2^3*MBhat^4 - 3913*m2^4*MBhat^4 + 560*m2^5*MBhat^4 - 
          210*MBhat^6 + 1470*m2*MBhat^6 + 1470*m2^2*MBhat^6 - 
          210*m2^3*MBhat^6 + 60*Ycut - 2055*m2*Ycut - 73785*m2^2*Ycut - 
          182790*m2^3*Ycut - 79170*m2^4*Ycut - 2535*m2^5*Ycut + 
          75*m2^6*Ycut - 1448*MBhat^2*Ycut + 29266*m2*MBhat^2*Ycut + 
          389960*m2^2*MBhat^2*Ycut + 145854*m2^3*MBhat^2*Ycut - 
          32102*m2^4*MBhat^2*Ycut + 5780*m2^5*MBhat^2*Ycut - 
          550*m2^6*MBhat^2*Ycut + 5056*MBhat^3*Ycut - 72816*m2*MBhat^3*Ycut - 
          451408*m2^2*MBhat^3*Ycut + 31000*m2^3*MBhat^3*Ycut - 
          36792*m2^4*MBhat^3*Ycut + 12680*m2^5*MBhat^3*Ycut - 
          1800*m2^6*MBhat^3*Ycut - 4508*MBhat^4*Ycut + 50799*m2*MBhat^4*
           Ycut + 153041*m2^2*MBhat^4*Ycut - 60557*m2^3*MBhat^4*Ycut + 
          19005*m2^4*MBhat^4*Ycut - 2800*m2^5*MBhat^4*Ycut + 
          840*MBhat^6*Ycut - 6090*m2*MBhat^6*Ycut - 7140*m2^2*MBhat^6*Ycut + 
          1050*m2^3*MBhat^6*Ycut - 90*Ycut^2 + 3105*m2*Ycut^2 + 
          122520*m2^2*Ycut^2 + 317730*m2^3*Ycut^2 + 144060*m2^4*Ycut^2 + 
          5025*m2^5*Ycut^2 - 150*m2^6*Ycut^2 + 2172*MBhat^2*Ycut^2 - 
          44442*m2*MBhat^2*Ycut^2 - 665002*m2^2*MBhat^2*Ycut^2 - 
          278068*m2^3*MBhat^2*Ycut^2 + 61110*m2^4*MBhat^2*Ycut^2 - 
          11230*m2^5*MBhat^2*Ycut^2 + 1100*m2^6*MBhat^2*Ycut^2 - 
          7584*MBhat^3*Ycut^2 + 111120*m2*MBhat^3*Ycut^2 + 
          792032*m2^2*MBhat^3*Ycut^2 - 47208*m2^3*MBhat^3*Ycut^2 + 
          67200*m2^4*MBhat^3*Ycut^2 - 24280*m2^5*MBhat^3*Ycut^2 + 
          3600*m2^6*MBhat^3*Ycut^2 + 6762*MBhat^4*Ycut^2 - 
          77889*m2*MBhat^4*Ycut^2 - 279538*m2^2*MBhat^4*Ycut^2 + 
          111615*m2^3*MBhat^4*Ycut^2 - 36330*m2^4*MBhat^4*Ycut^2 + 
          5600*m2^5*MBhat^4*Ycut^2 - 1260*MBhat^6*Ycut^2 + 
          9450*m2*MBhat^6*Ycut^2 + 13650*m2^2*MBhat^6*Ycut^2 - 
          2100*m2^3*MBhat^6*Ycut^2 + 60*Ycut^3 - 2085*m2*Ycut^3 - 
          93765*m2^2*Ycut^3 - 259035*m2^3*Ycut^3 - 125475*m2^4*Ycut^3 - 
          4950*m2^5*Ycut^3 + 150*m2^6*Ycut^3 - 1448*MBhat^2*Ycut^3 + 
          29990*m2*MBhat^2*Ycut^3 + 526708*m2^2*MBhat^2*Ycut^3 + 
          256760*m2^3*MBhat^2*Ycut^3 - 56210*m2^4*MBhat^2*Ycut^3 + 
          10680*m2^5*MBhat^2*Ycut^3 - 1100*m2^6*MBhat^2*Ycut^3 + 
          9256*MBhat^3*Ycut^3 - 92144*m2*MBhat^3*Ycut^3 - 
          625632*m2^2*MBhat^3*Ycut^3 + 9800*m2^3*MBhat^3*Ycut^3 - 
          53200*m2^4*MBhat^3*Ycut^3 + 22480*m2^5*MBhat^3*Ycut^3 - 
          3600*m2^6*MBhat^3*Ycut^3 - 11228*MBhat^4*Ycut^3 + 
          73213*m2*MBhat^4*Ycut^3 + 221445*m2^2*MBhat^4*Ycut^3 - 
          90370*m2^3*MBhat^4*Ycut^3 + 33530*m2^4*MBhat^4*Ycut^3 - 
          5600*m2^5*MBhat^4*Ycut^3 + 2520*MBhat^5*Ycut^3 - 
          5040*m2*MBhat^5*Ycut^3 + 2520*m2^2*MBhat^5*Ycut^3 + 
          840*MBhat^6*Ycut^3 - 6510*m2*MBhat^6*Ycut^3 - 12600*m2^2*MBhat^6*
           Ycut^3 + 2100*m2^3*MBhat^6*Ycut^3 - 15*Ycut^4 + 525*m2*Ycut^4 + 
          29610*m2^2*Ycut^4 + 90825*m2^3*Ycut^4 + 49350*m2^4*Ycut^4 + 
          2400*m2^5*Ycut^4 - 75*m2^6*Ycut^4 - 5413*MBhat^2*Ycut^4 + 
          15512*m2*MBhat^2*Ycut^4 - 209790*m2^2*MBhat^2*Ycut^4 - 
          86240*m2^3*MBhat^2*Ycut^4 + 17815*m2^4*MBhat^2*Ycut^4 - 
          4790*m2^5*MBhat^2*Ycut^4 + 550*m2^6*MBhat^2*Ycut^4 - 
          12604*MBhat^3*Ycut^4 + 59472*m2*MBhat^3*Ycut^4 + 
          175560*m2^2*MBhat^3*Ycut^4 + 32200*m2^3*MBhat^3*Ycut^4 + 
          13860*m2^4*MBhat^3*Ycut^4 - 9440*m2^5*MBhat^3*Ycut^4 + 
          1800*m2^6*MBhat^3*Ycut^4 + 32312*MBhat^4*Ycut^4 - 
          86415*m2*MBhat^4*Ycut^4 - 38850*m2^2*MBhat^4*Ycut^4 + 
          25480*m2^3*MBhat^4*Ycut^4 - 13965*m2^4*MBhat^4*Ycut^4 + 
          2800*m2^5*MBhat^4*Ycut^4 - 15120*MBhat^5*Ycut^4 + 
          17640*m2*MBhat^5*Ycut^4 - 2520*m2^2*MBhat^5*Ycut^4 + 
          840*MBhat^6*Ycut^4 + 2730*m2*MBhat^6*Ycut^4 + 5250*m2^2*MBhat^6*
           Ycut^4 - 1050*m2^3*MBhat^6*Ycut^4 - 1260*m2^2*Ycut^5 - 
          5985*m2^3*Ycut^5 - 4935*m2^4*Ycut^5 - 435*m2^5*Ycut^5 + 
          15*m2^6*Ycut^5 + 3360*MBhat*Ycut^5 - 13440*m2*MBhat*Ycut^5 + 
          20160*m2^2*MBhat*Ycut^5 - 13440*m2^3*MBhat*Ycut^5 + 
          3360*m2^4*MBhat*Ycut^5 + 24108*MBhat^2*Ycut^5 - 
          75999*m2*MBhat^2*Ycut^5 + 92421*m2^2*MBhat^2*Ycut^5 - 
          22449*m2^3*MBhat^2*Ycut^5 + 1281*m2^4*MBhat^2*Ycut^5 + 
          628*m2^5*MBhat^2*Ycut^5 - 110*m2^6*MBhat^2*Ycut^5 - 
          7476*MBhat^3*Ycut^5 - 168*m2*MBhat^3*Ycut^5 + 12432*m2^2*MBhat^3*
           Ycut^5 - 27888*m2^3*MBhat^3*Ycut^5 + 3612*m2^4*MBhat^3*Ycut^5 + 
          808*m2^5*MBhat^3*Ycut^5 - 360*m2^6*MBhat^3*Ycut^5 - 
          49812*MBhat^4*Ycut^5 + 95193*m2*MBhat^4*Ycut^5 - 
          40677*m2^2*MBhat^4*Ycut^5 + 483*m2^3*MBhat^4*Ycut^5 + 
          1113*m2^4*MBhat^4*Ycut^5 - 560*m2^5*MBhat^4*Ycut^5 + 
          34020*MBhat^5*Ycut^5 - 23940*m2*MBhat^5*Ycut^5 - 
          5040*m2^2*MBhat^5*Ycut^5 - 4200*MBhat^6*Ycut^5 - 
          3150*m2*MBhat^6*Ycut^5 - 420*m2^2*MBhat^6*Ycut^5 + 
          210*m2^3*MBhat^6*Ycut^5 - 735*Ycut^6 + 2940*m2*Ycut^6 - 
          4620*m2^2*Ycut^6 + 1995*m2^3*Ycut^6 - 1365*m2^4*Ycut^6 - 
          15*m2^5*Ycut^6 - 16380*MBhat*Ycut^6 + 49560*m2*MBhat*Ycut^6 - 
          50400*m2^2*MBhat*Ycut^6 + 17640*m2^3*MBhat*Ycut^6 - 
          420*m2^4*MBhat*Ycut^6 - 30947*MBhat^2*Ycut^6 + 
          77189*m2*MBhat^2*Ycut^6 - 60130*m2^2*MBhat^2*Ycut^6 + 
          17241*m2^3*MBhat^2*Ycut^6 - 623*m2^4*MBhat^2*Ycut^6 + 
          110*m2^5*MBhat^2*Ycut^6 + 54544*MBhat^3*Ycut^6 - 
          72464*m2*MBhat^3*Ycut^6 + 4648*m2^2*MBhat^3*Ycut^6 + 
          11480*m2^3*MBhat^3*Ycut^6 - 1288*m2^4*MBhat^3*Ycut^6 + 
          360*m2^5*MBhat^3*Ycut^6 + 22498*MBhat^4*Ycut^6 - 
          49259*m2*MBhat^4*Ycut^6 + 20314*m2^2*MBhat^4*Ycut^6 + 
          2387*m2^3*MBhat^4*Ycut^6 + 560*m2^4*MBhat^4*Ycut^6 - 
          35280*MBhat^5*Ycut^6 + 16380*m2*MBhat^5*Ycut^6 + 
          7560*m2^2*MBhat^5*Ycut^6 + 6300*MBhat^6*Ycut^6 + 
          3150*m2*MBhat^6*Ycut^6 - 210*m2^2*MBhat^6*Ycut^6 + 3900*Ycut^7 - 
          11535*m2*Ycut^7 + 11145*m2^2*Ycut^7 - 3660*m2^3*Ycut^7 - 
          300*m2^4*Ycut^7 + 30030*MBhat*Ycut^7 - 65310*m2*MBhat*Ycut^7 + 
          40110*m2^2*MBhat*Ycut^7 - 4410*m2^3*MBhat*Ycut^7 - 
          420*m2^4*MBhat*Ycut^7 - 712*MBhat^2*Ycut^7 - 6718*m2*MBhat^2*
           Ycut^7 + 11692*m2^2*MBhat^2*Ycut^7 - 3127*m2^3*MBhat^2*Ycut^7 - 
          425*m2^4*MBhat^2*Ycut^7 - 63766*MBhat^3*Ycut^7 + 
          67050*m2*MBhat^3*Ycut^7 - 8102*m2^2*MBhat^3*Ycut^7 - 
          2222*m2^3*MBhat^3*Ycut^7 - 360*m2^4*MBhat^3*Ycut^7 + 
          19628*MBhat^4*Ycut^7 + 2919*m2*MBhat^4*Ycut^7 - 
          3857*m2^2*MBhat^4*Ycut^7 - 1820*m2^3*MBhat^4*Ycut^7 + 
          15120*MBhat^5*Ycut^7 - 6300*m2*MBhat^5*Ycut^7 - 
          2520*m2^2*MBhat^5*Ycut^7 - 4200*MBhat^6*Ycut^7 - 
          1050*m2*MBhat^6*Ycut^7 - 8250*Ycut^8 + 16965*m2*Ycut^8 - 
          9165*m2^2*Ycut^8 + 300*m2^3*Ycut^8 - 23520*MBhat*Ycut^8 + 
          32970*m2*MBhat*Ycut^8 - 9660*m2^2*MBhat*Ycut^8 + 
          210*m2^3*MBhat*Ycut^8 + 32283*MBhat^2*Ycut^8 - 
          29490*m2*MBhat^2*Ycut^8 + 2152*m2^2*MBhat^2*Ycut^8 + 
          145*m2^3*MBhat^2*Ycut^8 + 20844*MBhat^3*Ycut^8 - 
          16686*m2*MBhat^3*Ycut^8 + 1252*m2^2*MBhat^3*Ycut^8 + 
          150*m2^3*MBhat^3*Ycut^8 - 22407*MBhat^4*Ycut^8 + 
          4557*m2*MBhat^4*Ycut^8 + 700*m2^2*MBhat^4*Ycut^8 + 
          1260*m2*MBhat^5*Ycut^8 + 1050*MBhat^6*Ycut^8 + 8700*Ycut^9 - 
          11085*m2*Ycut^9 + 2325*m2^2*Ycut^9 + 3780*MBhat*Ycut^9 - 
          1050*m2*MBhat*Ycut^9 - 210*m2^2*MBhat*Ycut^9 - 
          23452*MBhat^2*Ycut^9 + 11773*m2*MBhat^2*Ycut^9 - 
          145*m2^2*MBhat^2*Ycut^9 + 7304*MBhat^3*Ycut^9 - 
          1402*m2*MBhat^3*Ycut^9 - 150*m2^2*MBhat^3*Ycut^9 + 
          4928*MBhat^4*Ycut^9 - 700*m2*MBhat^4*Ycut^9 - 1260*MBhat^5*Ycut^9 - 
          4575*Ycut^10 + 2715*m2*Ycut^10 + 4620*MBhat*Ycut^10 - 
          2730*m2*MBhat*Ycut^10 + 3415*MBhat^2*Ycut^10 + 
          145*m2*MBhat^2*Ycut^10 - 4160*MBhat^3*Ycut^10 + 
          150*m2*MBhat^3*Ycut^10 + 700*MBhat^4*Ycut^10 + 960*Ycut^11 - 
          1890*MBhat*Ycut^11 + 1080*MBhat^2*Ycut^11 - 150*MBhat^3*Ycut^11))/
        (420*(-1 + Ycut)^5) + m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 
         108*MBhat^2 - 178*m2*MBhat^2 + 2*m2^2*MBhat^2 + 168*MBhat^3 + 
         104*m2*MBhat^3 - 81*MBhat^4 - m2*MBhat^4 + 6*MBhat^6)*Log[m2] - 
       m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 108*MBhat^2 - 178*m2*MBhat^2 + 
         2*m2^2*MBhat^2 + 168*MBhat^3 + 104*m2*MBhat^3 - 81*MBhat^4 - 
         m2*MBhat^4 + 6*MBhat^6)*Log[1 - Ycut]) + 
     c[VL]^2*(-1/420*((-1 + m2 + Ycut)*(15 - 510*m2 - 16995*m2^2 - 
           40620*m2^3 - 16995*m2^4 - 510*m2^5 + 15*m2^6 - 362*MBhat^2 + 
           7226*m2*MBhat^2 + 87866*m2^2*MBhat^2 + 30116*m2^3*MBhat^2 - 
           6634*m2^4*MBhat^2 + 1178*m2^5*MBhat^2 - 110*m2^6*MBhat^2 + 
           1264*MBhat^3 - 17888*m2*MBhat^3 - 99368*m2^2*MBhat^3 + 
           7312*m2^3*MBhat^3 - 7808*m2^4*MBhat^3 + 2608*m2^5*MBhat^3 - 
           360*m2^6*MBhat^3 - 1127*MBhat^4 + 12418*m2*MBhat^4 + 
           32578*m2^2*MBhat^4 - 12782*m2^3*MBhat^4 + 3913*m2^4*MBhat^4 - 
           560*m2^5*MBhat^4 + 210*MBhat^6 - 1470*m2*MBhat^6 - 
           1470*m2^2*MBhat^6 + 210*m2^3*MBhat^6 - 90*Ycut + 3075*m2*Ycut + 
           107775*m2^2*Ycut + 264030*m2^3*Ycut + 113160*m2^4*Ycut + 
           3555*m2^5*Ycut - 105*m2^6*Ycut + 2172*MBhat^2*Ycut - 
           43718*m2*MBhat^2*Ycut - 565692*m2^2*MBhat^2*Ycut - 
           206086*m2^3*MBhat^2*Ycut + 45370*m2^4*MBhat^2*Ycut - 
           8136*m2^5*MBhat^2*Ycut + 770*m2^6*MBhat^2*Ycut - 
           7584*MBhat^3*Ycut + 108592*m2*MBhat^3*Ycut + 650144*m2^2*MBhat^3*
            Ycut - 45624*m2^3*MBhat^3*Ycut + 52408*m2^4*MBhat^3*Ycut - 
           17896*m2^5*MBhat^3*Ycut + 2520*m2^6*MBhat^3*Ycut + 
           6762*MBhat^4*Ycut - 75635*m2*MBhat^4*Ycut - 218197*m2^2*MBhat^4*
            Ycut + 86121*m2^3*MBhat^4*Ycut - 26831*m2^4*MBhat^4*Ycut + 
           3920*m2^5*MBhat^4*Ycut - 1260*MBhat^6*Ycut + 9030*m2*MBhat^6*
            Ycut + 10080*m2^2*MBhat^6*Ycut - 1470*m2^3*MBhat^6*Ycut + 
           225*Ycut^2 - 7725*m2*Ycut^2 - 287085*m2^2*Ycut^2 - 
           723930*m2^3*Ycut^2 - 319395*m2^4*Ycut^2 - 10605*m2^5*Ycut^2 + 
           315*m2^6*Ycut^2 - 5430*MBhat^2*Ycut^2 + 110200*m2*MBhat^2*Ycut^2 + 
           1532788*m2^2*MBhat^2*Ycut^2 + 599892*m2^3*MBhat^2*Ycut^2 - 
           131948*m2^4*MBhat^2*Ycut^2 + 23968*m2^5*MBhat^2*Ycut^2 - 
           2310*m2^6*MBhat^2*Ycut^2 + 18960*MBhat^3*Ycut^2 - 
           274640*m2*MBhat^3*Ycut^2 - 1794216*m2^2*MBhat^3*Ycut^2 + 
           116520*m2^3*MBhat^3*Ycut^2 - 148592*m2^4*MBhat^3*Ycut^2 + 
           52248*m2^5*MBhat^3*Ycut^2 - 7560*m2^6*MBhat^3*Ycut^2 - 
           16905*MBhat^4*Ycut^2 + 191905*m2*MBhat^4*Ycut^2 + 
           618198*m2^2*MBhat^4*Ycut^2 - 245511*m2^3*MBhat^4*Ycut^2 + 
           78253*m2^4*MBhat^4*Ycut^2 - 11760*m2^5*MBhat^4*Ycut^2 + 
           3150*MBhat^6*Ycut^2 - 23100*m2*MBhat^6*Ycut^2 - 
           29400*m2^2*MBhat^6*Ycut^2 + 4410*m2^3*MBhat^6*Ycut^2 - 
           300*Ycut^3 + 10350*m2*Ycut^3 + 412590*m2^2*Ycut^3 + 
           1077285*m2^3*Ycut^3 + 492765*m2^4*Ycut^3 + 17535*m2^5*Ycut^3 - 
           525*m2^6*Ycut^3 + 7240*MBhat^2*Ycut^3 - 148140*m2*MBhat^2*Ycut^3 - 
           2246672*m2^2*MBhat^2*Ycut^3 - 958750*m2^3*MBhat^2*Ycut^3 + 
           210532*m2^4*MBhat^2*Ycut^3 - 38920*m2^5*MBhat^2*Ycut^3 + 
           3850*m2^6*MBhat^2*Ycut^3 - 26540*MBhat^3*Ycut^3 + 
           372500*m2*MBhat^3*Ycut^3 + 2690504*m2^2*MBhat^3*Ycut^3 - 
           164616*m2^3*MBhat^3*Ycut^3 + 239092*m2^4*MBhat^3*Ycut^3 - 
           86660*m2^5*MBhat^3*Ycut^3 + 12600*m2^6*MBhat^3*Ycut^3 + 
           24220*MBhat^4*Ycut^3 - 259630*m2*MBhat^4*Ycut^3 - 
           963802*m2^2*MBhat^4*Ycut^3 + 394317*m2^3*MBhat^4*Ycut^3 - 
           130235*m2^4*MBhat^4*Ycut^3 + 19600*m2^5*MBhat^4*Ycut^3 - 
           420*MBhat^5*Ycut^3 - 1260*m2*MBhat^5*Ycut^3 + 3780*m2^2*MBhat^5*
            Ycut^3 - 2100*m2^3*MBhat^5*Ycut^3 - 4200*MBhat^6*Ycut^3 + 
           31500*m2*MBhat^6*Ycut^3 + 47040*m2^2*MBhat^6*Ycut^3 - 
           7350*m2^3*MBhat^6*Ycut^3 + 225*Ycut^4 - 7800*m2*Ycut^4 - 
           339660*m2^2*Ycut^4 - 926625*m2^3*Ycut^4 - 444360*m2^4*Ycut^4 - 
           17325*m2^5*Ycut^4 + 525*m2^6*Ycut^4 - 4065*MBhat^2*Ycut^4 + 
           110960*m2*MBhat^2*Ycut^4 + 1884108*m2^2*MBhat^2*Ycut^4 + 
           921928*m2^3*MBhat^2*Ycut^4 - 213395*m2^4*MBhat^2*Ycut^4 + 
           41790*m2^5*MBhat^2*Ycut^4 - 3850*m2^6*MBhat^2*Ycut^4 + 
           24980*MBhat^3*Ycut^4 - 295100*m2*MBhat^3*Ycut^4 - 
           2320776*m2^2*MBhat^3*Ycut^4 + 118888*m2^3*MBhat^3*Ycut^4 - 
           220780*m2^4*MBhat^3*Ycut^4 + 83580*m2^5*MBhat^3*Ycut^4 - 
           12600*m2^6*MBhat^3*Ycut^4 - 27300*MBhat^4*Ycut^4 + 
           199220*m2*MBhat^4*Ycut^4 + 890428*m2^2*MBhat^4*Ycut^4 - 
           378525*m2^3*MBhat^4*Ycut^4 + 126175*m2^4*MBhat^4*Ycut^4 - 
           19600*m2^5*MBhat^4*Ycut^4 + 3360*MBhat^5*Ycut^4 + 
           7980*m2*MBhat^5*Ycut^4 - 13440*m2^2*MBhat^5*Ycut^4 + 
           2100*m2^3*MBhat^5*Ycut^4 + 2800*MBhat^6*Ycut^4 - 
           24500*m2*MBhat^6*Ycut^4 - 45500*m2^2*MBhat^6*Ycut^4 + 
           7350*m2^3*MBhat^6*Ycut^4 - 90*Ycut^5 + 3135*m2*Ycut^5 + 
           154245*m2^2*Ycut^5 + 446670*m2^3*Ycut^5 + 229110*m2^4*Ycut^5 + 
           10185*m2^5*Ycut^5 - 315*m2^6*Ycut^5 - 714*MBhat*Ycut^5 + 
           210*m2*MBhat*Ycut^5 + 6300*m2^2*MBhat*Ycut^5 - 13020*m2^3*MBhat*
            Ycut^5 + 9870*m2^4*MBhat*Ycut^5 - 2646*m2^5*MBhat*Ycut^5 - 
           5514*MBhat^2*Ycut^5 - 37501*m2*MBhat^2*Ycut^5 - 
           860293*m2^2*MBhat^2*Ycut^5 - 534555*m2^3*MBhat^2*Ycut^5 + 
           129115*m2^4*MBhat^2*Ycut^5 - 23534*m2^5*MBhat^2*Ycut^5 + 
           2310*m2^6*MBhat^2*Ycut^5 - 16404*MBhat^3*Ycut^5 + 
           152260*m2*MBhat^3*Ycut^5 + 1082704*m2^2*MBhat^3*Ycut^5 - 
           24528*m2^3*MBhat^3*Ycut^5 + 119252*m2^4*MBhat^3*Ycut^5 - 
           47068*m2^5*MBhat^3*Ycut^5 + 7560*m2^6*MBhat^3*Ycut^5 + 
           33012*MBhat^4*Ycut^5 - 89698*m2*MBhat^4*Ycut^5 - 
           488880*m2^2*MBhat^4*Ycut^5 + 217665*m2^3*MBhat^4*Ycut^5 - 
           67865*m2^4*MBhat^4*Ycut^5 + 11760*m2^5*MBhat^4*Ycut^5 - 
           11130*MBhat^5*Ycut^5 - 20790*m2*MBhat^5*Ycut^5 + 
           22470*m2^2*MBhat^5*Ycut^5 + 4410*m2^3*MBhat^5*Ycut^5 + 
           840*MBhat^6*Ycut^5 + 11620*m2*MBhat^6*Ycut^5 + 28280*m2^2*MBhat^6*
            Ycut^5 - 4410*m2^3*MBhat^6*Ycut^5 + 162*Ycut^6 - 525*m2*Ycut^6 - 
           33390*m2^2*Ycut^6 - 98910*m2^3*Ycut^6 - 60795*m2^4*Ycut^6 - 
           2667*m2^5*Ycut^6 + 105*m2^6*Ycut^6 + 4368*MBhat*Ycut^6 - 
           1806*m2*MBhat*Ycut^6 - 20496*m2^2*MBhat*Ycut^6 + 
           28644*m2^3*MBhat*Ycut^6 - 10416*m2^4*MBhat*Ycut^6 - 
           294*m2^5*MBhat*Ycut^6 + 16536*MBhat^2*Ycut^6 - 15001*m2*MBhat^2*
            Ycut^6 + 173026*m2^2*MBhat^2*Ycut^6 + 171521*m2^3*MBhat^2*
            Ycut^6 - 42854*m2^4*MBhat^2*Ycut^6 + 6818*m2^5*MBhat^2*Ycut^6 - 
           770*m2^6*MBhat^2*Ycut^6 - 1284*MBhat^3*Ycut^6 - 
           73052*m2*MBhat^3*Ycut^6 - 179368*m2^2*MBhat^3*Ycut^6 - 
           13776*m2^3*MBhat^3*Ycut^6 - 39844*m2^4*MBhat^3*Ycut^6 + 
           13636*m2^5*MBhat^3*Ycut^6 - 2520*m2^6*MBhat^3*Ycut^6 - 
           34482*MBhat^4*Ycut^6 + 36050*m2*MBhat^4*Ycut^6 + 
           160160*m2^2*MBhat^4*Ycut^6 - 81585*m2^3*MBhat^4*Ycut^6 + 
           17395*m2^4*MBhat^4*Ycut^6 - 3920*m2^5*MBhat^4*Ycut^6 + 
           19740*MBhat^5*Ycut^6 + 28350*m2*MBhat^5*Ycut^6 - 
           26040*m2^2*MBhat^5*Ycut^6 - 7350*m2^3*MBhat^5*Ycut^6 - 
           5040*MBhat^6*Ycut^6 - 5180*m2*MBhat^6*Ycut^6 - 11760*m2^2*MBhat^6*
            Ycut^6 + 1470*m2^3*MBhat^6*Ycut^6 - 946*Ycut^7 + 83*m2*Ycut^7 + 
           5963*m2^2*Ycut^7 - 1877*m2^3*Ycut^7 + 5473*m2^4*Ycut^7 + 
           769*m2^5*Ycut^7 - 15*m2^6*Ycut^7 - 11088*MBhat*Ycut^7 + 
           5922*m2*MBhat*Ycut^7 + 23856*m2^2*MBhat*Ycut^7 - 
           20580*m2^3*MBhat*Ycut^7 + 1344*m2^4*MBhat*Ycut^7 + 
           546*m2^5*MBhat*Ycut^7 - 16494*MBhat^2*Ycut^7 + 34613*m2*MBhat^2*
            Ycut^7 - 12441*m2^2*MBhat^2*Ycut^7 - 31670*m2^3*MBhat^2*Ycut^7 + 
           11506*m2^4*MBhat^2*Ycut^7 - 534*m2^5*MBhat^2*Ycut^7 + 
           110*m2^6*MBhat^2*Ycut^7 + 22158*MBhat^3*Ycut^7 + 
           38230*m2*MBhat^3*Ycut^7 - 56718*m2^2*MBhat^3*Ycut^7 + 
           10986*m2^3*MBhat^3*Ycut^7 + 7052*m2^4*MBhat^3*Ycut^7 - 
           1068*m2^5*MBhat^3*Ycut^7 + 360*m2^6*MBhat^3*Ycut^7 + 
           19320*MBhat^4*Ycut^7 - 28000*m2*MBhat^4*Ycut^7 - 
           39830*m2^2*MBhat^4*Ycut^7 + 26355*m2^3*MBhat^4*Ycut^7 - 
           245*m2^4*MBhat^4*Ycut^7 + 560*m2^5*MBhat^4*Ycut^7 - 
           19950*MBhat^5*Ycut^7 - 21000*m2*MBhat^5*Ycut^7 + 
           21000*m2^2*MBhat^5*Ycut^7 + 3570*m2^3*MBhat^5*Ycut^7 + 
           7000*MBhat^6*Ycut^7 + 3500*m2*MBhat^6*Ycut^7 + 3080*m2^2*MBhat^6*
            Ycut^7 - 210*m2^3*MBhat^6*Ycut^7 + 2589*Ycut^8 - 415*m2*Ycut^8 - 
           6317*m2^2*Ycut^8 + 4161*m2^3*Ycut^8 + 1129*m2^4*Ycut^8 - 
           97*m2^5*Ycut^8 + 14784*MBhat*Ycut^8 - 9870*m2*MBhat*Ycut^8 - 
           11424*m2^2*MBhat*Ycut^8 + 7476*m2^3*MBhat*Ycut^8 - 
           840*m2^4*MBhat*Ycut^8 - 126*m2^5*MBhat*Ycut^8 + 
           2532*MBhat^2*Ycut^8 - 29005*m2*MBhat^2*Ycut^8 + 
           13574*m2^2*MBhat^2*Ycut^8 + 8364*m2^3*MBhat^2*Ycut^8 - 
           1865*m2^4*MBhat^2*Ycut^8 - 110*m2^5*MBhat^2*Ycut^8 - 
           26520*MBhat^3*Ycut^8 - 8870*m2*MBhat^3*Ycut^8 + 
           34792*m2^2*MBhat^3*Ycut^8 - 5742*m2^3*MBhat^3*Ycut^8 - 
           1000*m2^4*MBhat^3*Ycut^8 - 220*m2^5*MBhat^3*Ycut^8 + 
           945*MBhat^4*Ycut^8 + 19250*m2*MBhat^4*Ycut^8 + 10290*m2^2*MBhat^4*
            Ycut^8 - 7245*m2^3*MBhat^4*Ycut^8 - 560*m2^4*MBhat^4*Ycut^8 + 
           10920*MBhat^5*Ycut^8 + 7560*m2*MBhat^5*Ycut^8 - 
           9240*m2^2*MBhat^5*Ycut^8 - 630*m2^3*MBhat^5*Ycut^8 - 
           5250*MBhat^6*Ycut^8 - 1750*m2*MBhat^6*Ycut^8 - 350*m2^2*MBhat^6*
            Ycut^8 - 3900*Ycut^9 + 830*m2*Ycut^9 + 3648*m2^2*Ycut^9 - 
           276*m2^3*Ycut^9 - 92*m2^4*Ycut^9 - 10500*MBhat*Ycut^9 + 
           9030*m2*MBhat*Ycut^9 + 1596*m2^2*MBhat*Ycut^9 - 
           2688*m2^3*MBhat*Ycut^9 + 42*m2^4*MBhat*Ycut^9 + 
           9350*MBhat^2*Ycut^9 + 12195*m2*MBhat^2*Ycut^9 - 
           8251*m2^2*MBhat^2*Ycut^9 - 867*m2^3*MBhat^2*Ycut^9 + 
           173*m2^4*MBhat^2*Ycut^9 + 12890*MBhat^3*Ycut^9 - 
           4800*m2*MBhat^3*Ycut^9 - 8228*m2^2*MBhat^3*Ycut^9 + 
           590*m2^3*MBhat^3*Ycut^9 + 220*m2^4*MBhat^3*Ycut^9 - 
           7630*MBhat^4*Ycut^9 - 6755*m2*MBhat^4*Ycut^9 - 875*m2^2*MBhat^4*
            Ycut^9 + 1190*m2^3*MBhat^4*Ycut^9 - 2310*MBhat^5*Ycut^9 - 
           630*m2*MBhat^5*Ycut^9 + 1470*m2^2*MBhat^5*Ycut^9 + 
           2100*MBhat^6*Ycut^9 + 350*m2*MBhat^6*Ycut^9 + 3485*Ycut^10 - 
           830*m2*Ycut^10 - 857*m2^2*Ycut^10 + 92*m2^3*Ycut^10 + 
           3024*MBhat*Ycut^10 - 4410*m2*MBhat*Ycut^10 + 336*m2^2*MBhat*
            Ycut^10 + 168*m2^3*MBhat*Ycut^10 - 8514*MBhat^2*Ycut^10 - 
           1415*m2*MBhat^2*Ycut^10 + 2094*m2^2*MBhat^2*Ycut^10 + 
           107*m2^3*MBhat^2*Ycut^10 - 900*MBhat^3*Ycut^10 + 
           3288*m2*MBhat^3*Ycut^10 + 520*m2^2*MBhat^3*Ycut^10 - 
           10*m2^3*MBhat^3*Ycut^10 + 3675*MBhat^4*Ycut^10 + 
           805*m2*MBhat^4*Ycut^10 - 70*m2^2*MBhat^4*Ycut^10 - 
           420*MBhat^5*Ycut^10 - 210*m2*MBhat^5*Ycut^10 - 
           350*MBhat^6*Ycut^10 - 1842*Ycut^11 + 415*m2*Ycut^11 + 
           83*m2^2*Ycut^11 + 672*MBhat*Ycut^11 + 966*m2*MBhat*Ycut^11 - 
           168*m2^2*MBhat*Ycut^11 + 2742*MBhat^2*Ycut^11 - 
           521*m2*MBhat^2*Ycut^11 - 107*m2^2*MBhat^2*Ycut^11 - 
           1362*MBhat^3*Ycut^11 - 510*m2*MBhat^3*Ycut^11 + 
           10*m2^2*MBhat^3*Ycut^11 - 420*MBhat^4*Ycut^11 + 
           70*m2*MBhat^4*Ycut^11 + 210*MBhat^5*Ycut^11 + 531*Ycut^12 - 
           83*m2*Ycut^12 - 672*MBhat*Ycut^12 - 42*m2*MBhat*Ycut^12 - 
           121*MBhat^2*Ycut^12 + 107*m2*MBhat^2*Ycut^12 + 
           332*MBhat^3*Ycut^12 - 10*m2*MBhat^3*Ycut^12 - 70*MBhat^4*Ycut^12 - 
           64*Ycut^13 + 126*MBhat*Ycut^13 - 72*MBhat^2*Ycut^13 + 
           10*MBhat^3*Ycut^13))/(-1 + Ycut)^7 + 
       m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 108*MBhat^2 - 178*m2*MBhat^2 + 
         2*m2^2*MBhat^2 + 168*MBhat^3 + 104*m2*MBhat^3 - 81*MBhat^4 - 
         m2*MBhat^4 + 6*MBhat^6)*Log[m2] - m2^2*(15 + 75*m2 + 75*m2^2 + 
         15*m2^3 - 108*MBhat^2 - 178*m2*MBhat^2 + 2*m2^2*MBhat^2 + 
         168*MBhat^3 + 104*m2*MBhat^3 - 81*MBhat^4 - m2*MBhat^4 + 6*MBhat^6)*
        Log[1 - Ycut]) + c[T]^2*
      ((-2*(-1 + m2 + Ycut)*(18 - 612*m2 - 20394*m2^2 - 48744*m2^3 - 
          20394*m2^4 - 612*m2^5 + 18*m2^6 - 475*MBhat^2 + 9325*m2*MBhat^2 + 
          107290*m2^2*MBhat^2 + 23290*m2^3*MBhat^2 - 10835*m2^4*MBhat^2 + 
          1765*m2^5*MBhat^2 - 160*m2^6*MBhat^2 + 1808*MBhat^3 - 
          24736*m2*MBhat^3 - 117976*m2^2*MBhat^3 + 23984*m2^3*MBhat^3 - 
          14656*m2^4*MBhat^3 + 4496*m2^5*MBhat^3 - 600*m2^6*MBhat^3 - 
          1666*MBhat^4 + 17864*m2*MBhat^4 + 37394*m2^2*MBhat^4 - 
          23086*m2^3*MBhat^4 + 6524*m2^4*MBhat^4 - 910*m2^5*MBhat^4 + 
          315*MBhat^6 - 2205*m2*MBhat^6 - 2205*m2^2*MBhat^6 + 
          315*m2^3*MBhat^6 - 108*Ycut + 3690*m2*Ycut + 129330*m2^2*Ycut + 
          316836*m2^3*Ycut + 135792*m2^4*Ycut + 4266*m2^5*Ycut - 
          126*m2^6*Ycut + 2850*MBhat^2*Ycut - 56425*m2*MBhat^2*Ycut - 
          691590*m2^2*MBhat^2*Ycut - 164300*m2^3*MBhat^2*Ycut + 
          74240*m2^4*MBhat^2*Ycut - 12195*m2^5*MBhat^2*Ycut + 
          1120*m2^6*MBhat^2*Ycut - 10848*MBhat^3*Ycut + 150224*m2*MBhat^3*
           Ycut + 775648*m2^2*MBhat^3*Ycut - 157128*m2^3*MBhat^3*Ycut + 
          98696*m2^4*MBhat^3*Ycut - 30872*m2^5*MBhat^3*Ycut + 
          4200*m2^6*MBhat^3*Ycut + 9996*MBhat^4*Ycut - 108850*m2*MBhat^4*
           Ycut - 253526*m2^2*MBhat^4*Ycut + 155988*m2^3*MBhat^4*Ycut - 
          44758*m2^4*MBhat^4*Ycut + 6370*m2^5*MBhat^4*Ycut - 
          1890*MBhat^6*Ycut + 13545*m2*MBhat^6*Ycut + 15120*m2^2*MBhat^6*
           Ycut - 2205*m2^3*MBhat^6*Ycut + 270*Ycut^2 - 9270*m2*Ycut^2 - 
          344502*m2^2*Ycut^2 - 868716*m2^3*Ycut^2 - 383274*m2^4*Ycut^2 - 
          12726*m2^5*Ycut^2 + 378*m2^6*Ycut^2 - 7125*MBhat^2*Ycut^2 + 
          142250*m2*MBhat^2*Ycut^2 + 1876475*m2^2*MBhat^2*Ycut^2 + 
          494175*m2^3*MBhat^2*Ycut^2 - 216460*m2^4*MBhat^2*Ycut^2 + 
          35945*m2^5*MBhat^2*Ycut^2 - 3360*m2^6*MBhat^2*Ycut^2 + 
          27120*MBhat^3*Ycut^2 - 380080*m2*MBhat^3*Ycut^2 - 
          2152152*m2^2*MBhat^3*Ycut^2 + 431640*m2^3*MBhat^3*Ycut^2 - 
          281104*m2^4*MBhat^3*Ycut^2 + 90216*m2^5*MBhat^3*Ycut^2 - 
          12600*m2^6*MBhat^3*Ycut^2 - 24990*MBhat^4*Ycut^2 + 
          276290*m2*MBhat^4*Ycut^2 + 727734*m2^2*MBhat^4*Ycut^2 - 
          446418*m2^3*MBhat^4*Ycut^2 + 130634*m2^4*MBhat^4*Ycut^2 - 
          19110*m2^5*MBhat^4*Ycut^2 + 4725*MBhat^6*Ycut^2 - 
          34650*m2*MBhat^6*Ycut^2 - 44100*m2^2*MBhat^6*Ycut^2 + 
          6615*m2^3*MBhat^6*Ycut^2 - 360*Ycut^3 + 12420*m2*Ycut^3 + 
          495108*m2^2*Ycut^3 + 1292742*m2^3*Ycut^3 + 591318*m2^4*Ycut^3 + 
          21042*m2^5*Ycut^3 - 630*m2^6*Ycut^3 + 9500*MBhat^2*Ycut^3 - 
          191250*m2*MBhat^2*Ycut^3 - 2754700*m2^2*MBhat^2*Ycut^3 - 
          818525*m2^3*MBhat^2*Ycut^3 + 346640*m2^4*MBhat^2*Ycut^3 - 
          58415*m2^5*MBhat^2*Ycut^3 + 5600*m2^6*MBhat^2*Ycut^3 - 
          40150*MBhat^3*Ycut^3 + 527290*m2*MBhat^3*Ycut^3 + 
          3223828*m2^2*MBhat^3*Ycut^3 - 628812*m2^3*MBhat^3*Ycut^3 + 
          437234*m2^4*MBhat^3*Ycut^3 - 146230*m2^5*MBhat^3*Ycut^3 + 
          21000*m2^6*MBhat^3*Ycut^3 + 39200*MBhat^4*Ycut^3 - 
          389060*m2*MBhat^4*Ycut^3 - 1128596*m2^2*MBhat^4*Ycut^3 + 
          698166*m2^3*MBhat^4*Ycut^3 - 211750*m2^4*MBhat^4*Ycut^3 + 
          31850*m2^5*MBhat^4*Ycut^3 - 1890*MBhat^5*Ycut^3 + 
          2730*m2*MBhat^5*Ycut^3 + 210*m2^2*MBhat^5*Ycut^3 - 
          1050*m2^3*MBhat^5*Ycut^3 - 6300*MBhat^6*Ycut^3 + 
          47250*m2*MBhat^6*Ycut^3 + 70560*m2^2*MBhat^6*Ycut^3 - 
          11025*m2^3*MBhat^6*Ycut^3 + 270*Ycut^4 - 9360*m2*Ycut^4 - 
          407592*m2^2*Ycut^4 - 1111950*m2^3*Ycut^4 - 533232*m2^4*Ycut^4 - 
          20790*m2^5*Ycut^4 + 630*m2^6*Ycut^4 - 2190*MBhat^2*Ycut^4 + 
          127090*m2*MBhat^2*Ycut^4 + 2346765*m2^2*MBhat^2*Ycut^4 + 
          793940*m2^3*MBhat^2*Ycut^4 - 330295*m2^4*MBhat^2*Ycut^4 + 
          58380*m2^5*MBhat^2*Ycut^4 - 5600*m2^6*MBhat^2*Ycut^4 + 
          46720*MBhat^3*Ycut^4 - 457930*m2*MBhat^3*Ycut^4 - 
          2735892*m2^2*MBhat^3*Ycut^4 + 485576*m2^3*MBhat^3*Ycut^4 - 
          386540*m2^4*MBhat^3*Ycut^4 + 138810*m2^5*MBhat^3*Ycut^4 - 
          21000*m2^6*MBhat^3*Ycut^4 - 64260*MBhat^4*Ycut^4 + 
          370510*m2*MBhat^4*Ycut^4 + 985754*m2^2*MBhat^4*Ycut^4 - 
          632100*m2^3*MBhat^4*Ycut^4 + 200900*m2^4*MBhat^4*Ycut^4 - 
          31850*m2^5*MBhat^4*Ycut^4 + 15960*MBhat^5*Ycut^4 - 
          13650*m2*MBhat^5*Ycut^4 - 3360*m2^2*MBhat^5*Ycut^4 + 
          1050*m2^3*MBhat^5*Ycut^4 + 3500*MBhat^6*Ycut^4 - 
          37450*m2*MBhat^6*Ycut^4 - 66850*m2^2*MBhat^6*Ycut^4 + 
          11025*m2^3*MBhat^6*Ycut^4 - 108*Ycut^5 + 3762*m2*Ycut^5 + 
          185094*m2^2*Ycut^5 + 536004*m2^3*Ycut^5 + 274932*m2^4*Ycut^5 + 
          12222*m2^5*Ycut^5 - 378*m2^6*Ycut^5 - 2751*MBhat*Ycut^5 + 
          9681*m2*MBhat*Ycut^5 - 11214*m2^2*MBhat*Ycut^5 + 
          3066*m2^3*MBhat*Ycut^5 + 2541*m2^4*MBhat*Ycut^5 - 
          1323*m2^5*MBhat*Ycut^5 - 27516*MBhat^2*Ycut^5 + 
          36196*m2*MBhat^2*Ycut^5 - 1190924*m2^2*MBhat^2*Ycut^5 - 
          417564*m2^3*MBhat^2*Ycut^5 + 177506*m2^4*MBhat^2*Ycut^5 - 
          32788*m2^5*MBhat^2*Ycut^5 + 3360*m2^6*MBhat^2*Ycut^5 - 
          36006*MBhat^3*Ycut^5 + 270002*m2*MBhat^3*Ycut^5 + 
          1214360*m2^2*MBhat^3*Ycut^5 - 147504*m2^3*MBhat^3*Ycut^5 + 
          186046*m2^4*MBhat^3*Ycut^5 - 75866*m2^5*MBhat^3*Ycut^5 + 
          12600*m2^6*MBhat^3*Ycut^5 + 115836*MBhat^4*Ycut^5 - 
          317744*m2*MBhat^4*Ycut^5 - 438270*m2^2*MBhat^4*Ycut^5 + 
          326130*m2^3*MBhat^4*Ycut^5 - 107800*m2^4*MBhat^4*Ycut^5 + 
          19110*m2^5*MBhat^4*Ycut^5 - 54915*MBhat^5*Ycut^5 + 
          28455*m2*MBhat^5*Ycut^5 + 17535*m2^2*MBhat^5*Ycut^5 + 
          2205*m2^3*MBhat^5*Ycut^5 + 5460*MBhat^6*Ycut^5 + 
          20930*m2*MBhat^6*Ycut^5 + 37660*m2^2*MBhat^6*Ycut^5 - 
          6615*m2^3*MBhat^6*Ycut^5 + 606*Ycut^6 - 2688*m2*Ycut^6 - 
          35952*m2^2*Ycut^6 - 122808*m2^3*Ycut^6 - 70896*m2^4*Ycut^6 - 
          3612*m2^5*Ycut^6 + 126*m2^6*Ycut^6 + 18690*MBhat*Ycut^6 - 
          55461*m2*MBhat*Ycut^6 + 54390*m2^2*MBhat*Ycut^6 - 
          17304*m2^3*MBhat*Ycut^6 - 168*m2^4*MBhat*Ycut^6 - 
          147*m2^5*MBhat*Ycut^6 + 72234*MBhat^2*Ycut^6 - 191534*m2*MBhat^2*
           Ycut^6 + 424067*m2^2*MBhat^2*Ycut^6 + 70483*m2^3*MBhat^2*Ycut^6 - 
          45976*m2^4*MBhat^2*Ycut^6 + 9436*m2^5*MBhat^2*Ycut^6 - 
          1120*m2^6*MBhat^2*Ycut^6 - 31764*MBhat^3*Ycut^6 - 
          64288*m2*MBhat^3*Ycut^6 - 167678*m2^2*MBhat^3*Ycut^6 - 
          44142*m2^3*MBhat^3*Ycut^6 - 39326*m2^4*MBhat^3*Ycut^6 + 
          20342*m2^5*MBhat^3*Ycut^6 - 4200*m2^6*MBhat^3*Ycut^6 - 
          141876*MBhat^4*Ycut^6 + 270970*m2*MBhat^4*Ycut^6 + 
          46270*m2^2*MBhat^4*Ycut^6 - 97020*m2^3*MBhat^4*Ycut^6 + 
          27440*m2^4*MBhat^4*Ycut^6 - 6370*m2^5*MBhat^4*Ycut^6 + 
          100170*MBhat^5*Ycut^6 - 33075*m2*MBhat^5*Ycut^6 - 
          38220*m2^2*MBhat^5*Ycut^6 - 3675*m2^3*MBhat^5*Ycut^6 - 
          18060*MBhat^6*Ycut^6 - 14770*m2*MBhat^6*Ycut^6 - 
          11760*m2^2*MBhat^6*Ycut^6 + 2205*m2^3*MBhat^6*Ycut^6 - 
          4232*Ycut^7 + 12232*m2*Ycut^7 - 10406*m2^2*Ycut^7 + 
          8606*m2^3*Ycut^7 + 4490*m2^4*Ycut^7 + 668*m2^5*Ycut^7 - 
          18*m2^6*Ycut^7 - 52983*MBhat*Ycut^7 + 130284*m2*MBhat*Ycut^7 - 
          102921*m2^2*MBhat*Ycut^7 + 27195*m2^3*MBhat*Ycut^7 - 
          1848*m2^4*MBhat*Ycut^7 + 273*m2^5*MBhat*Ycut^7 - 
          75720*MBhat^2*Ycut^7 + 202558*m2*MBhat^2*Ycut^7 - 
          177990*m2^2*MBhat^2*Ycut^7 + 29273*m2^3*MBhat^2*Ycut^7 + 
          6152*m2^4*MBhat^2*Ycut^7 - 708*m2^5*MBhat^2*Ycut^7 + 
          160*m2^6*MBhat^2*Ycut^7 + 135000*MBhat^3*Ycut^7 - 
          105310*m2*MBhat^3*Ycut^7 - 57318*m2^2*MBhat^3*Ycut^7 + 
          53100*m2^3*MBhat^3*Ycut^7 - 926*m2^4*MBhat^3*Ycut^7 - 
          786*m2^5*MBhat^3*Ycut^7 + 600*m2^6*MBhat^3*Ycut^7 + 
          76860*MBhat^4*Ycut^7 - 171500*m2*MBhat^4*Ycut^7 + 
          32900*m2^2*MBhat^4*Ycut^7 + 27300*m2^3*MBhat^4*Ycut^7 - 
          280*m2^4*MBhat^4*Ycut^7 + 910*m2^5*MBhat^4*Ycut^7 - 
          103425*MBhat^5*Ycut^7 + 25200*m2*MBhat^5*Ycut^7 + 
          39900*m2^2*MBhat^5*Ycut^7 + 1785*m2^3*MBhat^5*Ycut^7 + 
          24500*MBhat^6*Ycut^7 + 12250*m2*MBhat^6*Ycut^7 + 
          1540*m2^2*MBhat^6*Ycut^7 - 315*m2^3*MBhat^6*Ycut^7 + 13044*Ycut^8 - 
          30290*m2*Ycut^8 + 22094*m2^2*Ycut^8 - 4650*m2^3*Ycut^8 + 
          1100*m2^4*Ycut^8 - 38*m2^5*Ycut^8 + 79464*MBhat*Ycut^8 - 
          158340*m2*MBhat*Ycut^8 + 94584*m2^2*MBhat*Ycut^8 - 
          14721*m2^3*MBhat*Ycut^8 - 924*m2^4*MBhat*Ycut^8 - 
          63*m2^5*MBhat*Ycut^8 + 6117*MBhat^2*Ycut^8 - 73565*m2*MBhat^2*
           Ycut^8 + 73945*m2^2*MBhat^2*Ycut^8 - 12702*m2^3*MBhat^2*Ycut^8 - 
          1510*m2^4*MBhat^2*Ycut^8 - 160*m2^5*MBhat^2*Ycut^8 - 
          160260*MBhat^3*Ycut^8 + 144560*m2*MBhat^3*Ycut^8 + 
          18152*m2^2*MBhat^3*Ycut^8 - 19188*m2^3*MBhat^3*Ycut^8 + 
          46*m2^4*MBhat^3*Ycut^8 - 530*m2^5*MBhat^3*Ycut^8 + 
          22470*MBhat^4*Ycut^8 + 56770*m2*MBhat^4*Ycut^8 - 
          11760*m2^2*MBhat^4*Ycut^8 - 11760*m2^3*MBhat^4*Ycut^8 - 
          910*m2^4*MBhat^4*Ycut^8 + 57540*MBhat^5*Ycut^8 - 
          14280*m2*MBhat^5*Ycut^8 - 19740*m2^2*MBhat^5*Ycut^8 - 
          315*m2^3*MBhat^5*Ycut^8 - 18375*MBhat^6*Ycut^8 - 
          6125*m2*MBhat^6*Ycut^8 + 35*m2^2*MBhat^6*Ycut^8 - 22320*Ycut^9 + 
          40000*m2*Ycut^9 - 20436*m2^2*Ycut^9 + 2844*m2^3*Ycut^9 + 
          164*m2^4*Ycut^9 - 64155*MBhat*Ycut^9 + 100905*m2*MBhat*Ycut^9 - 
          41916*m2^2*MBhat*Ycut^9 + 1743*m2^3*MBhat*Ycut^9 + 
          399*m2^4*MBhat*Ycut^9 + 64210*MBhat^2*Ycut^9 - 
          30915*m2*MBhat^2*Ycut^9 - 13670*m2^2*MBhat^2*Ycut^9 + 
          1908*m2^3*MBhat^2*Ycut^9 + 538*m2^4*MBhat^2*Ycut^9 + 
          80680*MBhat^3*Ycut^9 - 73170*m2*MBhat^3*Ycut^9 - 
          628*m2^2*MBhat^3*Ycut^9 + 2584*m2^3*MBhat^3*Ycut^9 + 
          530*m2^4*MBhat^3*Ycut^9 - 53480*MBhat^4*Ycut^9 - 
          2590*m2*MBhat^4*Ycut^9 + 2660*m2^2*MBhat^4*Ycut^9 + 
          2800*m2^3*MBhat^4*Ycut^9 - 12285*MBhat^5*Ycut^9 + 
          5775*m2*MBhat^5*Ycut^9 + 3675*m2^2*MBhat^5*Ycut^9 + 
          7350*MBhat^6*Ycut^9 + 1225*m2*MBhat^6*Ycut^9 + 22900*Ycut^10 - 
          29710*m2*Ycut^10 + 9242*m2^2*Ycut^10 - 164*m2^3*Ycut^10 + 
          21546*MBhat*Ycut^10 - 26901*m2*MBhat*Ycut^10 + 
          7098*m2^2*MBhat*Ycut^10 + 21*m2^3*MBhat*Ycut^10 - 
          62469*MBhat^2*Ycut^10 + 34604*m2*MBhat^2*Ycut^10 + 
          354*m2^2*MBhat^2*Ycut^10 + 22*m2^3*MBhat^2*Ycut^10 - 
          3852*MBhat^3*Ycut^10 + 12984*m2*MBhat^3*Ycut^10 - 
          454*m2^2*MBhat^3*Ycut^10 - 110*m2^3*MBhat^3*Ycut^10 + 
          25410*MBhat^4*Ycut^10 - 3220*m2*MBhat^4*Ycut^10 - 
          560*m2^2*MBhat^4*Ycut^10 - 2310*MBhat^5*Ycut^10 - 
          1155*m2*MBhat^5*Ycut^10 - 1225*MBhat^6*Ycut^10 - 14088*Ycut^11 + 
          11768*m2*Ycut^11 - 1586*m2^2*Ycut^11 + 4935*MBhat*Ycut^11 - 
          1974*m2*MBhat*Ycut^11 - 21*m2^2*MBhat*Ycut^11 + 
          22476*MBhat^2*Ycut^11 - 8356*m2*MBhat^2*Ycut^11 - 
          22*m2^2*MBhat^2*Ycut^11 - 11538*MBhat^3*Ycut^11 + 
          564*m2*MBhat^3*Ycut^11 + 110*m2^2*MBhat^3*Ycut^11 - 
          2940*MBhat^4*Ycut^11 + 560*m2*MBhat^4*Ycut^11 + 
          1155*MBhat^5*Ycut^11 + 4812*Ycut^12 - 1942*m2*Ycut^12 - 
          6132*MBhat*Ycut^12 + 1806*m2*MBhat*Ycut^12 - 1100*MBhat^2*Ycut^12 + 
          22*m2*MBhat^2*Ycut^12 + 2980*MBhat^3*Ycut^12 - 
          110*m2*MBhat^3*Ycut^12 - 560*MBhat^4*Ycut^12 - 704*Ycut^13 + 
          1386*MBhat*Ycut^13 - 792*MBhat^2*Ycut^13 + 110*MBhat^3*Ycut^13))/
        (105*(-1 + Ycut)^7) + 8*m2^2*(18 + 90*m2 + 90*m2^2 + 18*m2^3 - 
         135*MBhat^2 - 200*m2*MBhat^2 + 25*m2^2*MBhat^2 + 216*MBhat^3 + 
         88*m2*MBhat^3 - 108*MBhat^4 + 22*m2*MBhat^4 + 9*MBhat^6)*Log[m2] - 
       8*m2^2*(18 + 90*m2 + 90*m2^2 + 18*m2^3 - 135*MBhat^2 - 
         200*m2*MBhat^2 + 25*m2^2*MBhat^2 + 216*MBhat^3 + 88*m2*MBhat^3 - 
         108*MBhat^4 + 22*m2*MBhat^4 + 9*MBhat^6)*Log[1 - Ycut]) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-2 - 204*m2 - 1054*m2^2 - 1054*m2^3 - 
          204*m2^4 - 2*m2^5 + 32*MBhat^2 + 1487*m2*MBhat^2 + 
          2312*m2^2*MBhat^2 - 388*m2^3*MBhat^2 - 88*m2^4*MBhat^2 + 
          5*m2^5*MBhat^2 - 80*MBhat^3 - 2272*m2*MBhat^3 - 1272*m2^2*MBhat^3 + 
          328*m2^3*MBhat^3 - 72*m2^4*MBhat^3 + 8*m2^5*MBhat^3 + 60*MBhat^4 + 
          1095*m2*MBhat^4 - 105*m2^2*MBhat^4 - 165*m2^3*MBhat^4 + 
          15*m2^4*MBhat^4 - 10*MBhat^6 - 100*m2*MBhat^6 - 10*m2^2*MBhat^6 + 
          10*Ycut + 1078*m2*Ycut + 5724*m2^2*Ycut + 5870*m2^3*Ycut + 
          1166*m2^4*Ycut + 12*m2^5*Ycut - 160*MBhat^2*Ycut - 
          7943*m2*MBhat^2*Ycut - 12921*m2^2*MBhat^2*Ycut + 
          2051*m2^3*MBhat^2*Ycut + 523*m2^4*MBhat^2*Ycut - 
          30*m2^5*MBhat^2*Ycut + 400*MBhat^3*Ycut + 12240*m2*MBhat^3*Ycut + 
          7368*m2^2*MBhat^3*Ycut - 1904*m2^3*MBhat^3*Ycut + 
          424*m2^4*MBhat^3*Ycut - 48*m2^5*MBhat^3*Ycut - 300*MBhat^4*Ycut - 
          5955*m2*MBhat^4*Ycut + 420*m2^2*MBhat^4*Ycut + 
          975*m2^3*MBhat^4*Ycut - 90*m2^4*MBhat^4*Ycut + 50*MBhat^6*Ycut + 
          550*m2*MBhat^6*Ycut + 60*m2^2*MBhat^6*Ycut - 20*Ycut^2 - 
          2302*m2*Ycut^2 - 12628*m2^2*Ycut^2 - 13358*m2^3*Ycut^2 - 
          2742*m2^4*Ycut^2 - 30*m2^5*Ycut^2 + 320*MBhat^2*Ycut^2 + 
          17172*m2*MBhat^2*Ycut^2 + 29496*m2^2*MBhat^2*Ycut^2 - 
          4333*m2^3*MBhat^2*Ycut^2 - 1290*m2^4*MBhat^2*Ycut^2 + 
          75*m2^5*MBhat^2*Ycut^2 - 800*MBhat^3*Ycut^2 - 26720*m2*MBhat^3*
           Ycut^2 - 17552*m2^2*MBhat^3*Ycut^2 + 4544*m2^3*MBhat^3*Ycut^2 - 
          1032*m2^4*MBhat^3*Ycut^2 + 120*m2^5*MBhat^3*Ycut^2 + 
          600*MBhat^4*Ycut^2 + 13140*m2*MBhat^4*Ycut^2 - 
          480*m2^2*MBhat^4*Ycut^2 - 2385*m2^3*MBhat^4*Ycut^2 + 
          225*m2^4*MBhat^4*Ycut^2 - 100*MBhat^6*Ycut^2 - 
          1230*m2*MBhat^6*Ycut^2 - 150*m2^2*MBhat^6*Ycut^2 + 20*Ycut^3 + 
          2498*m2*Ycut^3 + 14270*m2^2*Ycut^3 + 15712*m2^3*Ycut^3 + 
          3370*m2^4*Ycut^3 + 40*m2^5*Ycut^3 - 320*MBhat^2*Ycut^3 - 
          18908*m2*MBhat^2*Ycut^3 - 34772*m2^2*MBhat^2*Ycut^3 + 
          4535*m2^3*MBhat^2*Ycut^3 + 1685*m2^4*MBhat^2*Ycut^3 - 
          100*m2^5*MBhat^2*Ycut^3 + 870*MBhat^3*Ycut^3 + 
          29480*m2*MBhat^3*Ycut^3 + 22228*m2^2*MBhat^3*Ycut^3 - 
          5928*m2^3*MBhat^3*Ycut^3 + 1390*m2^4*MBhat^3*Ycut^3 - 
          160*m2^5*MBhat^3*Ycut^3 - 720*MBhat^4*Ycut^3 - 
          14460*m2*MBhat^4*Ycut^3 - 540*m2^2*MBhat^4*Ycut^3 + 
          3195*m2^3*MBhat^4*Ycut^3 - 300*m2^4*MBhat^4*Ycut^3 + 
          50*MBhat^5*Ycut^3 - 100*m2*MBhat^5*Ycut^3 + 50*m2^2*MBhat^5*
           Ycut^3 + 100*MBhat^6*Ycut^3 + 1410*m2*MBhat^6*Ycut^3 + 
          200*m2^2*MBhat^6*Ycut^3 - 10*Ycut^4 - 1397*m2*Ycut^4 - 
          8427*m2^2*Ycut^4 - 9815*m2^3*Ycut^4 - 2245*m2^4*Ycut^4 - 
          30*m2^5*Ycut^4 + 55*MBhat^2*Ycut^4 + 11192*m2*MBhat^2*Ycut^4 + 
          21090*m2^2*MBhat^2*Ycut^4 - 1855*m2^3*MBhat^2*Ycut^4 - 
          1325*m2^4*MBhat^2*Ycut^4 + 75*m2^5*MBhat^2*Ycut^4 - 
          570*MBhat^3*Ycut^4 - 16590*m2*MBhat^3*Ycut^4 - 15402*m2^2*MBhat^3*
           Ycut^4 + 4230*m2^3*MBhat^3*Ycut^4 - 1020*m2^4*MBhat^3*Ycut^4 + 
          120*m2^5*MBhat^3*Ycut^4 + 825*MBhat^4*Ycut^4 + 
          7455*m2*MBhat^4*Ycut^4 + 1695*m2^2*MBhat^4*Ycut^4 - 
          2370*m2^3*MBhat^4*Ycut^4 + 225*m2^4*MBhat^4*Ycut^4 - 
          250*MBhat^5*Ycut^4 + 310*m2*MBhat^5*Ycut^4 - 60*m2^2*MBhat^5*
           Ycut^4 - 50*MBhat^6*Ycut^4 - 820*m2*MBhat^6*Ycut^4 - 
          150*m2^2*MBhat^6*Ycut^4 + 2*Ycut^5 + 339*m2*Ycut^5 + 
          2232*m2^2*Ycut^5 + 2857*m2^3*Ycut^5 + 732*m2^4*Ycut^5 + 
          12*m2^5*Ycut^5 + 63*MBhat*Ycut^5 - 252*m2*MBhat*Ycut^5 + 
          378*m2^2*MBhat*Ycut^5 - 252*m2^3*MBhat*Ycut^5 + 
          63*m2^4*MBhat*Ycut^5 + 385*MBhat^2*Ycut^5 - 3993*m2*MBhat^2*
           Ycut^5 - 4863*m2^2*MBhat^2*Ycut^5 - 238*m2^3*MBhat^2*Ycut^5 + 
          507*m2^4*MBhat^2*Ycut^5 - 30*m2^5*MBhat^2*Ycut^5 - 
          75*MBhat^3*Ycut^5 + 4434*m2*MBhat^3*Ycut^5 + 5112*m2^2*MBhat^3*
           Ycut^5 - 1578*m2^3*MBhat^3*Ycut^5 + 387*m2^4*MBhat^3*Ycut^5 - 
          48*m2^5*MBhat^3*Ycut^5 - 885*MBhat^4*Ycut^5 - 630*m2*MBhat^4*
           Ycut^5 - 1455*m2^2*MBhat^4*Ycut^5 + 855*m2^3*MBhat^4*Ycut^5 - 
          90*m2^4*MBhat^4*Ycut^5 + 500*MBhat^5*Ycut^5 - 360*m2*MBhat^5*
           Ycut^5 - 60*m2^2*MBhat^5*Ycut^5 + 10*MBhat^6*Ycut^5 + 
          150*m2*MBhat^6*Ycut^5 + 60*m2^2*MBhat^6*Ycut^5 - 14*Ycut^6 + 
          46*m2*Ycut^6 - 182*m2^2*Ycut^6 - 125*m2^3*Ycut^6 - 83*m2^4*Ycut^6 - 
          2*m2^5*Ycut^6 - 291*MBhat*Ycut^6 + 867*m2*MBhat*Ycut^6 - 
          855*m2^2*MBhat*Ycut^6 + 273*m2^3*MBhat*Ycut^6 + 
          6*m2^4*MBhat*Ycut^6 - 520*MBhat^2*Ycut^6 + 1382*m2*MBhat^2*Ycut^6 - 
          661*m2^2*MBhat^2*Ycut^6 + 341*m2^3*MBhat^2*Ycut^6 - 
          67*m2^4*MBhat^2*Ycut^6 + 5*m2^5*MBhat^2*Ycut^6 + 
          875*MBhat^3*Ycut^6 - 1225*m2*MBhat^3*Ycut^6 - 433*m2^2*MBhat^3*
           Ycut^6 + 349*m2^3*MBhat^3*Ycut^6 - 54*m2^4*MBhat^3*Ycut^6 + 
          8*m2^5*MBhat^3*Ycut^6 + 450*MBhat^4*Ycut^6 - 930*m2*MBhat^4*
           Ycut^6 + 615*m2^2*MBhat^4*Ycut^6 - 90*m2^3*MBhat^4*Ycut^6 + 
          15*m2^4*MBhat^4*Ycut^6 - 500*MBhat^5*Ycut^6 + 220*m2*MBhat^5*
           Ycut^6 + 100*m2^2*MBhat^5*Ycut^6 + 70*m2*MBhat^6*Ycut^6 - 
          10*m2^2*MBhat^6*Ycut^6 + 70*Ycut^7 - 204*m2*Ycut^7 + 
          174*m2^2*Ycut^7 - 71*m2^3*Ycut^7 - 14*m2^4*Ycut^7 + 
          510*MBhat*Ycut^7 - 1068*m2*MBhat*Ycut^7 + 597*m2^2*MBhat*Ycut^7 - 
          30*m2^3*MBhat*Ycut^7 - 9*m2^4*MBhat*Ycut^7 + 20*MBhat^2*Ycut^7 - 
          218*m2*MBhat^2*Ycut^7 + 381*m2^2*MBhat^2*Ycut^7 - 
          118*m2^3*MBhat^2*Ycut^7 - 5*m2^4*MBhat^2*Ycut^7 - 
          1000*MBhat^3*Ycut^7 + 960*m2*MBhat^3*Ycut^7 - 93*m2^2*MBhat^3*
           Ycut^7 - 44*m2^3*MBhat^3*Ycut^7 - 3*m2^4*MBhat^3*Ycut^7 + 
          150*MBhat^4*Ycut^7 + 300*m2*MBhat^4*Ycut^7 - 165*m2^2*MBhat^4*
           Ycut^7 - 15*m2^3*MBhat^4*Ycut^7 + 250*MBhat^5*Ycut^7 - 
          100*m2*MBhat^5*Ycut^7 - 30*m2^2*MBhat^5*Ycut^7 - 
          30*m2*MBhat^6*Ycut^7 - 140*Ycut^8 + 271*m2*Ycut^8 - 
          125*m2^2*Ycut^8 - 16*m2^3*Ycut^8 - 390*MBhat*Ycut^8 + 
          522*m2*MBhat*Ycut^8 - 141*m2^2*MBhat*Ycut^8 + 9*m2^3*MBhat*Ycut^8 + 
          455*MBhat^2*Ycut^8 - 303*m2*MBhat^2*Ycut^8 - 57*m2^2*MBhat^2*
           Ycut^8 + 5*m2^3*MBhat^2*Ycut^8 + 380*MBhat^3*Ycut^8 - 
          340*m2*MBhat^3*Ycut^8 + 47*m2^2*MBhat^3*Ycut^8 + 
          3*m2^3*MBhat^3*Ycut^8 - 255*MBhat^4*Ycut^8 + 15*m2^2*MBhat^4*
           Ycut^8 - 50*MBhat^5*Ycut^8 + 30*m2*MBhat^5*Ycut^8 + 140*Ycut^9 - 
          159*m2*Ycut^9 + 16*m2^2*Ycut^9 + 75*MBhat*Ycut^9 - 
          48*m2*MBhat*Ycut^9 + 21*m2^2*MBhat*Ycut^9 - 335*MBhat^2*Ycut^9 + 
          142*m2*MBhat^2*Ycut^9 - 5*m2^2*MBhat^2*Ycut^9 + 45*MBhat^3*Ycut^9 + 
          30*m2*MBhat^3*Ycut^9 - 3*m2^2*MBhat^3*Ycut^9 + 75*MBhat^4*Ycut^9 - 
          15*m2*MBhat^4*Ycut^9 - 70*Ycut^10 + 34*m2*Ycut^10 + 
          57*MBhat*Ycut^10 - 21*m2*MBhat*Ycut^10 + 58*MBhat^2*Ycut^10 - 
          10*m2*MBhat^2*Ycut^10 - 45*MBhat^3*Ycut^10 + 3*m2*MBhat^3*Ycut^10 + 
          14*Ycut^11 - 24*MBhat*Ycut^11 + 10*MBhat^2*Ycut^11))/
        (5*(-1 + Ycut)^6) + 12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + 
         m2^4 - 9*MBhat^2 - 39*m2*MBhat^2 - 14*m2^2*MBhat^2 + 
         6*m2^3*MBhat^2 + 16*MBhat^3 + 40*m2*MBhat^3 - 9*MBhat^4 - 
         12*m2*MBhat^4 + 6*m2^2*MBhat^4 + MBhat^6 + m2*MBhat^6)*Log[m2] - 
       12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 9*MBhat^2 - 
         39*m2*MBhat^2 - 14*m2^2*MBhat^2 + 6*m2^3*MBhat^2 + 16*MBhat^3 + 
         40*m2*MBhat^3 - 9*MBhat^4 - 12*m2*MBhat^4 + 6*m2^2*MBhat^4 + 
         MBhat^6 + m2*MBhat^6)*Log[1 - Ycut]) + 
     c[VL]*(-1/210*((-1 + m2 + Ycut)*(15 - 510*m2 - 16995*m2^2 - 40620*m2^3 - 
           16995*m2^4 - 510*m2^5 + 15*m2^6 - 362*MBhat^2 + 7226*m2*MBhat^2 + 
           87866*m2^2*MBhat^2 + 30116*m2^3*MBhat^2 - 6634*m2^4*MBhat^2 + 
           1178*m2^5*MBhat^2 - 110*m2^6*MBhat^2 + 1264*MBhat^3 - 
           17888*m2*MBhat^3 - 99368*m2^2*MBhat^3 + 7312*m2^3*MBhat^3 - 
           7808*m2^4*MBhat^3 + 2608*m2^5*MBhat^3 - 360*m2^6*MBhat^3 - 
           1127*MBhat^4 + 12418*m2*MBhat^4 + 32578*m2^2*MBhat^4 - 
           12782*m2^3*MBhat^4 + 3913*m2^4*MBhat^4 - 560*m2^5*MBhat^4 + 
           210*MBhat^6 - 1470*m2*MBhat^6 - 1470*m2^2*MBhat^6 + 
           210*m2^3*MBhat^6 - 90*Ycut + 3075*m2*Ycut + 107775*m2^2*Ycut + 
           264030*m2^3*Ycut + 113160*m2^4*Ycut + 3555*m2^5*Ycut - 
           105*m2^6*Ycut + 2172*MBhat^2*Ycut - 43718*m2*MBhat^2*Ycut - 
           565692*m2^2*MBhat^2*Ycut - 206086*m2^3*MBhat^2*Ycut + 
           45370*m2^4*MBhat^2*Ycut - 8136*m2^5*MBhat^2*Ycut + 
           770*m2^6*MBhat^2*Ycut - 7584*MBhat^3*Ycut + 108592*m2*MBhat^3*
            Ycut + 650144*m2^2*MBhat^3*Ycut - 45624*m2^3*MBhat^3*Ycut + 
           52408*m2^4*MBhat^3*Ycut - 17896*m2^5*MBhat^3*Ycut + 
           2520*m2^6*MBhat^3*Ycut + 6762*MBhat^4*Ycut - 75635*m2*MBhat^4*
            Ycut - 218197*m2^2*MBhat^4*Ycut + 86121*m2^3*MBhat^4*Ycut - 
           26831*m2^4*MBhat^4*Ycut + 3920*m2^5*MBhat^4*Ycut - 
           1260*MBhat^6*Ycut + 9030*m2*MBhat^6*Ycut + 10080*m2^2*MBhat^6*
            Ycut - 1470*m2^3*MBhat^6*Ycut + 225*Ycut^2 - 7725*m2*Ycut^2 - 
           287085*m2^2*Ycut^2 - 723930*m2^3*Ycut^2 - 319395*m2^4*Ycut^2 - 
           10605*m2^5*Ycut^2 + 315*m2^6*Ycut^2 - 5430*MBhat^2*Ycut^2 + 
           110200*m2*MBhat^2*Ycut^2 + 1532788*m2^2*MBhat^2*Ycut^2 + 
           599892*m2^3*MBhat^2*Ycut^2 - 131948*m2^4*MBhat^2*Ycut^2 + 
           23968*m2^5*MBhat^2*Ycut^2 - 2310*m2^6*MBhat^2*Ycut^2 + 
           18960*MBhat^3*Ycut^2 - 274640*m2*MBhat^3*Ycut^2 - 
           1794216*m2^2*MBhat^3*Ycut^2 + 116520*m2^3*MBhat^3*Ycut^2 - 
           148592*m2^4*MBhat^3*Ycut^2 + 52248*m2^5*MBhat^3*Ycut^2 - 
           7560*m2^6*MBhat^3*Ycut^2 - 16905*MBhat^4*Ycut^2 + 
           191905*m2*MBhat^4*Ycut^2 + 618198*m2^2*MBhat^4*Ycut^2 - 
           245511*m2^3*MBhat^4*Ycut^2 + 78253*m2^4*MBhat^4*Ycut^2 - 
           11760*m2^5*MBhat^4*Ycut^2 + 3150*MBhat^6*Ycut^2 - 
           23100*m2*MBhat^6*Ycut^2 - 29400*m2^2*MBhat^6*Ycut^2 + 
           4410*m2^3*MBhat^6*Ycut^2 - 300*Ycut^3 + 10350*m2*Ycut^3 + 
           412590*m2^2*Ycut^3 + 1077285*m2^3*Ycut^3 + 492765*m2^4*Ycut^3 + 
           17535*m2^5*Ycut^3 - 525*m2^6*Ycut^3 + 7240*MBhat^2*Ycut^3 - 
           148140*m2*MBhat^2*Ycut^3 - 2246672*m2^2*MBhat^2*Ycut^3 - 
           958750*m2^3*MBhat^2*Ycut^3 + 210532*m2^4*MBhat^2*Ycut^3 - 
           38920*m2^5*MBhat^2*Ycut^3 + 3850*m2^6*MBhat^2*Ycut^3 - 
           26540*MBhat^3*Ycut^3 + 372500*m2*MBhat^3*Ycut^3 + 
           2690504*m2^2*MBhat^3*Ycut^3 - 164616*m2^3*MBhat^3*Ycut^3 + 
           239092*m2^4*MBhat^3*Ycut^3 - 86660*m2^5*MBhat^3*Ycut^3 + 
           12600*m2^6*MBhat^3*Ycut^3 + 24220*MBhat^4*Ycut^3 - 
           259630*m2*MBhat^4*Ycut^3 - 963802*m2^2*MBhat^4*Ycut^3 + 
           394317*m2^3*MBhat^4*Ycut^3 - 130235*m2^4*MBhat^4*Ycut^3 + 
           19600*m2^5*MBhat^4*Ycut^3 - 420*MBhat^5*Ycut^3 - 
           1260*m2*MBhat^5*Ycut^3 + 3780*m2^2*MBhat^5*Ycut^3 - 
           2100*m2^3*MBhat^5*Ycut^3 - 4200*MBhat^6*Ycut^3 + 
           31500*m2*MBhat^6*Ycut^3 + 47040*m2^2*MBhat^6*Ycut^3 - 
           7350*m2^3*MBhat^6*Ycut^3 + 225*Ycut^4 - 7800*m2*Ycut^4 - 
           339660*m2^2*Ycut^4 - 926625*m2^3*Ycut^4 - 444360*m2^4*Ycut^4 - 
           17325*m2^5*Ycut^4 + 525*m2^6*Ycut^4 - 4065*MBhat^2*Ycut^4 + 
           110960*m2*MBhat^2*Ycut^4 + 1884108*m2^2*MBhat^2*Ycut^4 + 
           921928*m2^3*MBhat^2*Ycut^4 - 213395*m2^4*MBhat^2*Ycut^4 + 
           41790*m2^5*MBhat^2*Ycut^4 - 3850*m2^6*MBhat^2*Ycut^4 + 
           24980*MBhat^3*Ycut^4 - 295100*m2*MBhat^3*Ycut^4 - 
           2320776*m2^2*MBhat^3*Ycut^4 + 118888*m2^3*MBhat^3*Ycut^4 - 
           220780*m2^4*MBhat^3*Ycut^4 + 83580*m2^5*MBhat^3*Ycut^4 - 
           12600*m2^6*MBhat^3*Ycut^4 - 27300*MBhat^4*Ycut^4 + 
           199220*m2*MBhat^4*Ycut^4 + 890428*m2^2*MBhat^4*Ycut^4 - 
           378525*m2^3*MBhat^4*Ycut^4 + 126175*m2^4*MBhat^4*Ycut^4 - 
           19600*m2^5*MBhat^4*Ycut^4 + 3360*MBhat^5*Ycut^4 + 
           7980*m2*MBhat^5*Ycut^4 - 13440*m2^2*MBhat^5*Ycut^4 + 
           2100*m2^3*MBhat^5*Ycut^4 + 2800*MBhat^6*Ycut^4 - 
           24500*m2*MBhat^6*Ycut^4 - 45500*m2^2*MBhat^6*Ycut^4 + 
           7350*m2^3*MBhat^6*Ycut^4 - 90*Ycut^5 + 3135*m2*Ycut^5 + 
           154245*m2^2*Ycut^5 + 446670*m2^3*Ycut^5 + 229110*m2^4*Ycut^5 + 
           10185*m2^5*Ycut^5 - 315*m2^6*Ycut^5 - 714*MBhat*Ycut^5 + 
           210*m2*MBhat*Ycut^5 + 6300*m2^2*MBhat*Ycut^5 - 13020*m2^3*MBhat*
            Ycut^5 + 9870*m2^4*MBhat*Ycut^5 - 2646*m2^5*MBhat*Ycut^5 - 
           5514*MBhat^2*Ycut^5 - 37501*m2*MBhat^2*Ycut^5 - 
           860293*m2^2*MBhat^2*Ycut^5 - 534555*m2^3*MBhat^2*Ycut^5 + 
           129115*m2^4*MBhat^2*Ycut^5 - 23534*m2^5*MBhat^2*Ycut^5 + 
           2310*m2^6*MBhat^2*Ycut^5 - 16404*MBhat^3*Ycut^5 + 
           152260*m2*MBhat^3*Ycut^5 + 1082704*m2^2*MBhat^3*Ycut^5 - 
           24528*m2^3*MBhat^3*Ycut^5 + 119252*m2^4*MBhat^3*Ycut^5 - 
           47068*m2^5*MBhat^3*Ycut^5 + 7560*m2^6*MBhat^3*Ycut^5 + 
           33012*MBhat^4*Ycut^5 - 89698*m2*MBhat^4*Ycut^5 - 
           488880*m2^2*MBhat^4*Ycut^5 + 217665*m2^3*MBhat^4*Ycut^5 - 
           67865*m2^4*MBhat^4*Ycut^5 + 11760*m2^5*MBhat^4*Ycut^5 - 
           11130*MBhat^5*Ycut^5 - 20790*m2*MBhat^5*Ycut^5 + 
           22470*m2^2*MBhat^5*Ycut^5 + 4410*m2^3*MBhat^5*Ycut^5 + 
           840*MBhat^6*Ycut^5 + 11620*m2*MBhat^6*Ycut^5 + 28280*m2^2*MBhat^6*
            Ycut^5 - 4410*m2^3*MBhat^6*Ycut^5 + 162*Ycut^6 - 525*m2*Ycut^6 - 
           33390*m2^2*Ycut^6 - 98910*m2^3*Ycut^6 - 60795*m2^4*Ycut^6 - 
           2667*m2^5*Ycut^6 + 105*m2^6*Ycut^6 + 4368*MBhat*Ycut^6 - 
           1806*m2*MBhat*Ycut^6 - 20496*m2^2*MBhat*Ycut^6 + 
           28644*m2^3*MBhat*Ycut^6 - 10416*m2^4*MBhat*Ycut^6 - 
           294*m2^5*MBhat*Ycut^6 + 16536*MBhat^2*Ycut^6 - 15001*m2*MBhat^2*
            Ycut^6 + 173026*m2^2*MBhat^2*Ycut^6 + 171521*m2^3*MBhat^2*
            Ycut^6 - 42854*m2^4*MBhat^2*Ycut^6 + 6818*m2^5*MBhat^2*Ycut^6 - 
           770*m2^6*MBhat^2*Ycut^6 - 1284*MBhat^3*Ycut^6 - 
           73052*m2*MBhat^3*Ycut^6 - 179368*m2^2*MBhat^3*Ycut^6 - 
           13776*m2^3*MBhat^3*Ycut^6 - 39844*m2^4*MBhat^3*Ycut^6 + 
           13636*m2^5*MBhat^3*Ycut^6 - 2520*m2^6*MBhat^3*Ycut^6 - 
           34482*MBhat^4*Ycut^6 + 36050*m2*MBhat^4*Ycut^6 + 
           160160*m2^2*MBhat^4*Ycut^6 - 81585*m2^3*MBhat^4*Ycut^6 + 
           17395*m2^4*MBhat^4*Ycut^6 - 3920*m2^5*MBhat^4*Ycut^6 + 
           19740*MBhat^5*Ycut^6 + 28350*m2*MBhat^5*Ycut^6 - 
           26040*m2^2*MBhat^5*Ycut^6 - 7350*m2^3*MBhat^5*Ycut^6 - 
           5040*MBhat^6*Ycut^6 - 5180*m2*MBhat^6*Ycut^6 - 11760*m2^2*MBhat^6*
            Ycut^6 + 1470*m2^3*MBhat^6*Ycut^6 - 946*Ycut^7 + 83*m2*Ycut^7 + 
           5963*m2^2*Ycut^7 - 1877*m2^3*Ycut^7 + 5473*m2^4*Ycut^7 + 
           769*m2^5*Ycut^7 - 15*m2^6*Ycut^7 - 11088*MBhat*Ycut^7 + 
           5922*m2*MBhat*Ycut^7 + 23856*m2^2*MBhat*Ycut^7 - 
           20580*m2^3*MBhat*Ycut^7 + 1344*m2^4*MBhat*Ycut^7 + 
           546*m2^5*MBhat*Ycut^7 - 16494*MBhat^2*Ycut^7 + 34613*m2*MBhat^2*
            Ycut^7 - 12441*m2^2*MBhat^2*Ycut^7 - 31670*m2^3*MBhat^2*Ycut^7 + 
           11506*m2^4*MBhat^2*Ycut^7 - 534*m2^5*MBhat^2*Ycut^7 + 
           110*m2^6*MBhat^2*Ycut^7 + 22158*MBhat^3*Ycut^7 + 
           38230*m2*MBhat^3*Ycut^7 - 56718*m2^2*MBhat^3*Ycut^7 + 
           10986*m2^3*MBhat^3*Ycut^7 + 7052*m2^4*MBhat^3*Ycut^7 - 
           1068*m2^5*MBhat^3*Ycut^7 + 360*m2^6*MBhat^3*Ycut^7 + 
           19320*MBhat^4*Ycut^7 - 28000*m2*MBhat^4*Ycut^7 - 
           39830*m2^2*MBhat^4*Ycut^7 + 26355*m2^3*MBhat^4*Ycut^7 - 
           245*m2^4*MBhat^4*Ycut^7 + 560*m2^5*MBhat^4*Ycut^7 - 
           19950*MBhat^5*Ycut^7 - 21000*m2*MBhat^5*Ycut^7 + 
           21000*m2^2*MBhat^5*Ycut^7 + 3570*m2^3*MBhat^5*Ycut^7 + 
           7000*MBhat^6*Ycut^7 + 3500*m2*MBhat^6*Ycut^7 + 3080*m2^2*MBhat^6*
            Ycut^7 - 210*m2^3*MBhat^6*Ycut^7 + 2589*Ycut^8 - 415*m2*Ycut^8 - 
           6317*m2^2*Ycut^8 + 4161*m2^3*Ycut^8 + 1129*m2^4*Ycut^8 - 
           97*m2^5*Ycut^8 + 14784*MBhat*Ycut^8 - 9870*m2*MBhat*Ycut^8 - 
           11424*m2^2*MBhat*Ycut^8 + 7476*m2^3*MBhat*Ycut^8 - 
           840*m2^4*MBhat*Ycut^8 - 126*m2^5*MBhat*Ycut^8 + 
           2532*MBhat^2*Ycut^8 - 29005*m2*MBhat^2*Ycut^8 + 
           13574*m2^2*MBhat^2*Ycut^8 + 8364*m2^3*MBhat^2*Ycut^8 - 
           1865*m2^4*MBhat^2*Ycut^8 - 110*m2^5*MBhat^2*Ycut^8 - 
           26520*MBhat^3*Ycut^8 - 8870*m2*MBhat^3*Ycut^8 + 
           34792*m2^2*MBhat^3*Ycut^8 - 5742*m2^3*MBhat^3*Ycut^8 - 
           1000*m2^4*MBhat^3*Ycut^8 - 220*m2^5*MBhat^3*Ycut^8 + 
           945*MBhat^4*Ycut^8 + 19250*m2*MBhat^4*Ycut^8 + 10290*m2^2*MBhat^4*
            Ycut^8 - 7245*m2^3*MBhat^4*Ycut^8 - 560*m2^4*MBhat^4*Ycut^8 + 
           10920*MBhat^5*Ycut^8 + 7560*m2*MBhat^5*Ycut^8 - 
           9240*m2^2*MBhat^5*Ycut^8 - 630*m2^3*MBhat^5*Ycut^8 - 
           5250*MBhat^6*Ycut^8 - 1750*m2*MBhat^6*Ycut^8 - 350*m2^2*MBhat^6*
            Ycut^8 - 3900*Ycut^9 + 830*m2*Ycut^9 + 3648*m2^2*Ycut^9 - 
           276*m2^3*Ycut^9 - 92*m2^4*Ycut^9 - 10500*MBhat*Ycut^9 + 
           9030*m2*MBhat*Ycut^9 + 1596*m2^2*MBhat*Ycut^9 - 
           2688*m2^3*MBhat*Ycut^9 + 42*m2^4*MBhat*Ycut^9 + 
           9350*MBhat^2*Ycut^9 + 12195*m2*MBhat^2*Ycut^9 - 
           8251*m2^2*MBhat^2*Ycut^9 - 867*m2^3*MBhat^2*Ycut^9 + 
           173*m2^4*MBhat^2*Ycut^9 + 12890*MBhat^3*Ycut^9 - 
           4800*m2*MBhat^3*Ycut^9 - 8228*m2^2*MBhat^3*Ycut^9 + 
           590*m2^3*MBhat^3*Ycut^9 + 220*m2^4*MBhat^3*Ycut^9 - 
           7630*MBhat^4*Ycut^9 - 6755*m2*MBhat^4*Ycut^9 - 875*m2^2*MBhat^4*
            Ycut^9 + 1190*m2^3*MBhat^4*Ycut^9 - 2310*MBhat^5*Ycut^9 - 
           630*m2*MBhat^5*Ycut^9 + 1470*m2^2*MBhat^5*Ycut^9 + 
           2100*MBhat^6*Ycut^9 + 350*m2*MBhat^6*Ycut^9 + 3485*Ycut^10 - 
           830*m2*Ycut^10 - 857*m2^2*Ycut^10 + 92*m2^3*Ycut^10 + 
           3024*MBhat*Ycut^10 - 4410*m2*MBhat*Ycut^10 + 336*m2^2*MBhat*
            Ycut^10 + 168*m2^3*MBhat*Ycut^10 - 8514*MBhat^2*Ycut^10 - 
           1415*m2*MBhat^2*Ycut^10 + 2094*m2^2*MBhat^2*Ycut^10 + 
           107*m2^3*MBhat^2*Ycut^10 - 900*MBhat^3*Ycut^10 + 
           3288*m2*MBhat^3*Ycut^10 + 520*m2^2*MBhat^3*Ycut^10 - 
           10*m2^3*MBhat^3*Ycut^10 + 3675*MBhat^4*Ycut^10 + 
           805*m2*MBhat^4*Ycut^10 - 70*m2^2*MBhat^4*Ycut^10 - 
           420*MBhat^5*Ycut^10 - 210*m2*MBhat^5*Ycut^10 - 
           350*MBhat^6*Ycut^10 - 1842*Ycut^11 + 415*m2*Ycut^11 + 
           83*m2^2*Ycut^11 + 672*MBhat*Ycut^11 + 966*m2*MBhat*Ycut^11 - 
           168*m2^2*MBhat*Ycut^11 + 2742*MBhat^2*Ycut^11 - 
           521*m2*MBhat^2*Ycut^11 - 107*m2^2*MBhat^2*Ycut^11 - 
           1362*MBhat^3*Ycut^11 - 510*m2*MBhat^3*Ycut^11 + 
           10*m2^2*MBhat^3*Ycut^11 - 420*MBhat^4*Ycut^11 + 
           70*m2*MBhat^4*Ycut^11 + 210*MBhat^5*Ycut^11 + 531*Ycut^12 - 
           83*m2*Ycut^12 - 672*MBhat*Ycut^12 - 42*m2*MBhat*Ycut^12 - 
           121*MBhat^2*Ycut^12 + 107*m2*MBhat^2*Ycut^12 + 
           332*MBhat^3*Ycut^12 - 10*m2*MBhat^3*Ycut^12 - 70*MBhat^4*Ycut^12 - 
           64*Ycut^13 + 126*MBhat*Ycut^13 - 72*MBhat^2*Ycut^13 + 
           10*MBhat^3*Ycut^13))/(-1 + Ycut)^7 + 
       2*m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 108*MBhat^2 - 
         178*m2*MBhat^2 + 2*m2^2*MBhat^2 + 168*MBhat^3 + 104*m2*MBhat^3 - 
         81*MBhat^4 - m2*MBhat^4 + 6*MBhat^6)*Log[m2] - 
       2*m2^2*(15 + 75*m2 + 75*m2^2 + 15*m2^3 - 108*MBhat^2 - 
         178*m2*MBhat^2 + 2*m2^2*MBhat^2 + 168*MBhat^3 + 104*m2*MBhat^3 - 
         81*MBhat^4 - m2*MBhat^4 + 6*MBhat^6)*Log[1 - Ycut] + 
       c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-2 - 204*m2 - 1054*m2^2 - 
            1054*m2^3 - 204*m2^4 - 2*m2^5 + 32*MBhat^2 + 1487*m2*MBhat^2 + 
            2312*m2^2*MBhat^2 - 388*m2^3*MBhat^2 - 88*m2^4*MBhat^2 + 
            5*m2^5*MBhat^2 - 80*MBhat^3 - 2272*m2*MBhat^3 - 
            1272*m2^2*MBhat^3 + 328*m2^3*MBhat^3 - 72*m2^4*MBhat^3 + 
            8*m2^5*MBhat^3 + 60*MBhat^4 + 1095*m2*MBhat^4 - 
            105*m2^2*MBhat^4 - 165*m2^3*MBhat^4 + 15*m2^4*MBhat^4 - 
            10*MBhat^6 - 100*m2*MBhat^6 - 10*m2^2*MBhat^6 + 10*Ycut + 
            1078*m2*Ycut + 5724*m2^2*Ycut + 5870*m2^3*Ycut + 1166*m2^4*Ycut + 
            12*m2^5*Ycut - 160*MBhat^2*Ycut - 7943*m2*MBhat^2*Ycut - 
            12921*m2^2*MBhat^2*Ycut + 2051*m2^3*MBhat^2*Ycut + 
            523*m2^4*MBhat^2*Ycut - 30*m2^5*MBhat^2*Ycut + 400*MBhat^3*Ycut + 
            12240*m2*MBhat^3*Ycut + 7368*m2^2*MBhat^3*Ycut - 
            1904*m2^3*MBhat^3*Ycut + 424*m2^4*MBhat^3*Ycut - 
            48*m2^5*MBhat^3*Ycut - 300*MBhat^4*Ycut - 5955*m2*MBhat^4*Ycut + 
            420*m2^2*MBhat^4*Ycut + 975*m2^3*MBhat^4*Ycut - 
            90*m2^4*MBhat^4*Ycut + 50*MBhat^6*Ycut + 550*m2*MBhat^6*Ycut + 
            60*m2^2*MBhat^6*Ycut - 20*Ycut^2 - 2302*m2*Ycut^2 - 
            12628*m2^2*Ycut^2 - 13358*m2^3*Ycut^2 - 2742*m2^4*Ycut^2 - 
            30*m2^5*Ycut^2 + 320*MBhat^2*Ycut^2 + 17172*m2*MBhat^2*Ycut^2 + 
            29496*m2^2*MBhat^2*Ycut^2 - 4333*m2^3*MBhat^2*Ycut^2 - 
            1290*m2^4*MBhat^2*Ycut^2 + 75*m2^5*MBhat^2*Ycut^2 - 
            800*MBhat^3*Ycut^2 - 26720*m2*MBhat^3*Ycut^2 - 17552*m2^2*MBhat^3*
             Ycut^2 + 4544*m2^3*MBhat^3*Ycut^2 - 1032*m2^4*MBhat^3*Ycut^2 + 
            120*m2^5*MBhat^3*Ycut^2 + 600*MBhat^4*Ycut^2 + 13140*m2*MBhat^4*
             Ycut^2 - 480*m2^2*MBhat^4*Ycut^2 - 2385*m2^3*MBhat^4*Ycut^2 + 
            225*m2^4*MBhat^4*Ycut^2 - 100*MBhat^6*Ycut^2 - 
            1230*m2*MBhat^6*Ycut^2 - 150*m2^2*MBhat^6*Ycut^2 + 20*Ycut^3 + 
            2498*m2*Ycut^3 + 14270*m2^2*Ycut^3 + 15712*m2^3*Ycut^3 + 
            3370*m2^4*Ycut^3 + 40*m2^5*Ycut^3 - 320*MBhat^2*Ycut^3 - 
            18908*m2*MBhat^2*Ycut^3 - 34772*m2^2*MBhat^2*Ycut^3 + 
            4535*m2^3*MBhat^2*Ycut^3 + 1685*m2^4*MBhat^2*Ycut^3 - 
            100*m2^5*MBhat^2*Ycut^3 + 870*MBhat^3*Ycut^3 + 29480*m2*MBhat^3*
             Ycut^3 + 22228*m2^2*MBhat^3*Ycut^3 - 5928*m2^3*MBhat^3*Ycut^3 + 
            1390*m2^4*MBhat^3*Ycut^3 - 160*m2^5*MBhat^3*Ycut^3 - 
            720*MBhat^4*Ycut^3 - 14460*m2*MBhat^4*Ycut^3 - 540*m2^2*MBhat^4*
             Ycut^3 + 3195*m2^3*MBhat^4*Ycut^3 - 300*m2^4*MBhat^4*Ycut^3 + 
            50*MBhat^5*Ycut^3 - 100*m2*MBhat^5*Ycut^3 + 50*m2^2*MBhat^5*
             Ycut^3 + 100*MBhat^6*Ycut^3 + 1410*m2*MBhat^6*Ycut^3 + 
            200*m2^2*MBhat^6*Ycut^3 - 10*Ycut^4 - 1397*m2*Ycut^4 - 
            8427*m2^2*Ycut^4 - 9815*m2^3*Ycut^4 - 2245*m2^4*Ycut^4 - 
            30*m2^5*Ycut^4 + 55*MBhat^2*Ycut^4 + 11192*m2*MBhat^2*Ycut^4 + 
            21090*m2^2*MBhat^2*Ycut^4 - 1855*m2^3*MBhat^2*Ycut^4 - 
            1325*m2^4*MBhat^2*Ycut^4 + 75*m2^5*MBhat^2*Ycut^4 - 
            570*MBhat^3*Ycut^4 - 16590*m2*MBhat^3*Ycut^4 - 15402*m2^2*MBhat^3*
             Ycut^4 + 4230*m2^3*MBhat^3*Ycut^4 - 1020*m2^4*MBhat^3*Ycut^4 + 
            120*m2^5*MBhat^3*Ycut^4 + 825*MBhat^4*Ycut^4 + 
            7455*m2*MBhat^4*Ycut^4 + 1695*m2^2*MBhat^4*Ycut^4 - 
            2370*m2^3*MBhat^4*Ycut^4 + 225*m2^4*MBhat^4*Ycut^4 - 
            250*MBhat^5*Ycut^4 + 310*m2*MBhat^5*Ycut^4 - 60*m2^2*MBhat^5*
             Ycut^4 - 50*MBhat^6*Ycut^4 - 820*m2*MBhat^6*Ycut^4 - 
            150*m2^2*MBhat^6*Ycut^4 + 2*Ycut^5 + 339*m2*Ycut^5 + 
            2232*m2^2*Ycut^5 + 2857*m2^3*Ycut^5 + 732*m2^4*Ycut^5 + 
            12*m2^5*Ycut^5 + 63*MBhat*Ycut^5 - 252*m2*MBhat*Ycut^5 + 
            378*m2^2*MBhat*Ycut^5 - 252*m2^3*MBhat*Ycut^5 + 
            63*m2^4*MBhat*Ycut^5 + 385*MBhat^2*Ycut^5 - 3993*m2*MBhat^2*
             Ycut^5 - 4863*m2^2*MBhat^2*Ycut^5 - 238*m2^3*MBhat^2*Ycut^5 + 
            507*m2^4*MBhat^2*Ycut^5 - 30*m2^5*MBhat^2*Ycut^5 - 
            75*MBhat^3*Ycut^5 + 4434*m2*MBhat^3*Ycut^5 + 5112*m2^2*MBhat^3*
             Ycut^5 - 1578*m2^3*MBhat^3*Ycut^5 + 387*m2^4*MBhat^3*Ycut^5 - 
            48*m2^5*MBhat^3*Ycut^5 - 885*MBhat^4*Ycut^5 - 630*m2*MBhat^4*
             Ycut^5 - 1455*m2^2*MBhat^4*Ycut^5 + 855*m2^3*MBhat^4*Ycut^5 - 
            90*m2^4*MBhat^4*Ycut^5 + 500*MBhat^5*Ycut^5 - 360*m2*MBhat^5*
             Ycut^5 - 60*m2^2*MBhat^5*Ycut^5 + 10*MBhat^6*Ycut^5 + 
            150*m2*MBhat^6*Ycut^5 + 60*m2^2*MBhat^6*Ycut^5 - 14*Ycut^6 + 
            46*m2*Ycut^6 - 182*m2^2*Ycut^6 - 125*m2^3*Ycut^6 - 
            83*m2^4*Ycut^6 - 2*m2^5*Ycut^6 - 291*MBhat*Ycut^6 + 
            867*m2*MBhat*Ycut^6 - 855*m2^2*MBhat*Ycut^6 + 273*m2^3*MBhat*
             Ycut^6 + 6*m2^4*MBhat*Ycut^6 - 520*MBhat^2*Ycut^6 + 
            1382*m2*MBhat^2*Ycut^6 - 661*m2^2*MBhat^2*Ycut^6 + 
            341*m2^3*MBhat^2*Ycut^6 - 67*m2^4*MBhat^2*Ycut^6 + 
            5*m2^5*MBhat^2*Ycut^6 + 875*MBhat^3*Ycut^6 - 1225*m2*MBhat^3*
             Ycut^6 - 433*m2^2*MBhat^3*Ycut^6 + 349*m2^3*MBhat^3*Ycut^6 - 
            54*m2^4*MBhat^3*Ycut^6 + 8*m2^5*MBhat^3*Ycut^6 + 
            450*MBhat^4*Ycut^6 - 930*m2*MBhat^4*Ycut^6 + 615*m2^2*MBhat^4*
             Ycut^6 - 90*m2^3*MBhat^4*Ycut^6 + 15*m2^4*MBhat^4*Ycut^6 - 
            500*MBhat^5*Ycut^6 + 220*m2*MBhat^5*Ycut^6 + 100*m2^2*MBhat^5*
             Ycut^6 + 70*m2*MBhat^6*Ycut^6 - 10*m2^2*MBhat^6*Ycut^6 + 
            70*Ycut^7 - 204*m2*Ycut^7 + 174*m2^2*Ycut^7 - 71*m2^3*Ycut^7 - 
            14*m2^4*Ycut^7 + 510*MBhat*Ycut^7 - 1068*m2*MBhat*Ycut^7 + 
            597*m2^2*MBhat*Ycut^7 - 30*m2^3*MBhat*Ycut^7 - 
            9*m2^4*MBhat*Ycut^7 + 20*MBhat^2*Ycut^7 - 218*m2*MBhat^2*Ycut^7 + 
            381*m2^2*MBhat^2*Ycut^7 - 118*m2^3*MBhat^2*Ycut^7 - 
            5*m2^4*MBhat^2*Ycut^7 - 1000*MBhat^3*Ycut^7 + 960*m2*MBhat^3*
             Ycut^7 - 93*m2^2*MBhat^3*Ycut^7 - 44*m2^3*MBhat^3*Ycut^7 - 
            3*m2^4*MBhat^3*Ycut^7 + 150*MBhat^4*Ycut^7 + 300*m2*MBhat^4*
             Ycut^7 - 165*m2^2*MBhat^4*Ycut^7 - 15*m2^3*MBhat^4*Ycut^7 + 
            250*MBhat^5*Ycut^7 - 100*m2*MBhat^5*Ycut^7 - 30*m2^2*MBhat^5*
             Ycut^7 - 30*m2*MBhat^6*Ycut^7 - 140*Ycut^8 + 271*m2*Ycut^8 - 
            125*m2^2*Ycut^8 - 16*m2^3*Ycut^8 - 390*MBhat*Ycut^8 + 
            522*m2*MBhat*Ycut^8 - 141*m2^2*MBhat*Ycut^8 + 
            9*m2^3*MBhat*Ycut^8 + 455*MBhat^2*Ycut^8 - 303*m2*MBhat^2*
             Ycut^8 - 57*m2^2*MBhat^2*Ycut^8 + 5*m2^3*MBhat^2*Ycut^8 + 
            380*MBhat^3*Ycut^8 - 340*m2*MBhat^3*Ycut^8 + 47*m2^2*MBhat^3*
             Ycut^8 + 3*m2^3*MBhat^3*Ycut^8 - 255*MBhat^4*Ycut^8 + 
            15*m2^2*MBhat^4*Ycut^8 - 50*MBhat^5*Ycut^8 + 30*m2*MBhat^5*
             Ycut^8 + 140*Ycut^9 - 159*m2*Ycut^9 + 16*m2^2*Ycut^9 + 
            75*MBhat*Ycut^9 - 48*m2*MBhat*Ycut^9 + 21*m2^2*MBhat*Ycut^9 - 
            335*MBhat^2*Ycut^9 + 142*m2*MBhat^2*Ycut^9 - 5*m2^2*MBhat^2*
             Ycut^9 + 45*MBhat^3*Ycut^9 + 30*m2*MBhat^3*Ycut^9 - 
            3*m2^2*MBhat^3*Ycut^9 + 75*MBhat^4*Ycut^9 - 15*m2*MBhat^4*
             Ycut^9 - 70*Ycut^10 + 34*m2*Ycut^10 + 57*MBhat*Ycut^10 - 
            21*m2*MBhat*Ycut^10 + 58*MBhat^2*Ycut^10 - 10*m2*MBhat^2*
             Ycut^10 - 45*MBhat^3*Ycut^10 + 3*m2*MBhat^3*Ycut^10 + 
            14*Ycut^11 - 24*MBhat*Ycut^11 + 10*MBhat^2*Ycut^11))/
          (5*(-1 + Ycut)^6) + 12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + 
           m2^4 - 9*MBhat^2 - 39*m2*MBhat^2 - 14*m2^2*MBhat^2 + 
           6*m2^3*MBhat^2 + 16*MBhat^3 + 40*m2*MBhat^3 - 9*MBhat^4 - 
           12*m2*MBhat^4 + 6*m2^2*MBhat^4 + MBhat^6 + m2*MBhat^6)*Log[m2] - 
         12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 9*MBhat^2 - 
           39*m2*MBhat^2 - 14*m2^2*MBhat^2 + 6*m2^3*MBhat^2 + 16*MBhat^3 + 
           40*m2*MBhat^3 - 9*MBhat^4 - 12*m2*MBhat^4 + 6*m2^2*MBhat^4 + 
           MBhat^6 + m2*MBhat^6)*Log[1 - Ycut])) + 
     c[SL]*(-1/30*(Ycut^3*(-1 + m2 + Ycut)*(210*MBhat^3 - 1050*m2*MBhat^3 + 
           2100*m2^2*MBhat^3 - 2100*m2^3*MBhat^3 + 1050*m2^4*MBhat^3 - 
           210*m2^5*MBhat^3 - 360*MBhat^4 + 1440*m2*MBhat^4 - 
           2160*m2^2*MBhat^4 + 1440*m2^3*MBhat^4 - 360*m2^4*MBhat^4 + 
           150*MBhat^5 - 450*m2*MBhat^5 + 450*m2^2*MBhat^5 - 
           150*m2^3*MBhat^5 - 315*MBhat^2*Ycut + 1575*m2*MBhat^2*Ycut - 
           3150*m2^2*MBhat^2*Ycut + 3150*m2^3*MBhat^2*Ycut - 
           1575*m2^4*MBhat^2*Ycut + 315*m2^5*MBhat^2*Ycut - 
           980*MBhat^3*Ycut + 4270*m2*MBhat^3*Ycut - 7280*m2^2*MBhat^3*Ycut + 
           6020*m2^3*MBhat^3*Ycut - 2380*m2^4*MBhat^3*Ycut + 
           350*m2^5*MBhat^3*Ycut + 2445*MBhat^4*Ycut - 7965*m2*MBhat^4*Ycut + 
           9225*m2^2*MBhat^4*Ycut - 4335*m2^3*MBhat^4*Ycut + 
           630*m2^4*MBhat^4*Ycut - 1200*MBhat^5*Ycut + 2550*m2*MBhat^5*Ycut - 
           1500*m2^2*MBhat^5*Ycut + 150*m2^3*MBhat^5*Ycut + 50*MBhat^6*Ycut + 
           50*m2*MBhat^6*Ycut - 100*m2^2*MBhat^6*Ycut + 189*MBhat*Ycut^2 - 
           945*m2*MBhat*Ycut^2 + 1890*m2^2*MBhat*Ycut^2 - 
           1890*m2^3*MBhat*Ycut^2 + 945*m2^4*MBhat*Ycut^2 - 
           189*m2^5*MBhat*Ycut^2 + 1998*MBhat^2*Ycut^2 - 8181*m2*MBhat^2*
            Ycut^2 + 12744*m2^2*MBhat^2*Ycut^2 - 9126*m2^3*MBhat^2*Ycut^2 + 
           2754*m2^4*MBhat^2*Ycut^2 - 189*m2^5*MBhat^2*Ycut^2 + 
           756*MBhat^3*Ycut^2 - 4214*m2*MBhat^3*Ycut^2 + 8456*m2^2*MBhat^3*
            Ycut^2 - 7644*m2^3*MBhat^3*Ycut^2 + 2996*m2^4*MBhat^3*Ycut^2 - 
           350*m2^5*MBhat^3*Ycut^2 - 6618*MBhat^4*Ycut^2 + 
           17967*m2*MBhat^4*Ycut^2 - 16458*m2^2*MBhat^4*Ycut^2 + 
           5487*m2^3*MBhat^4*Ycut^2 - 378*m2^4*MBhat^4*Ycut^2 + 
           3975*MBhat^5*Ycut^2 - 6075*m2*MBhat^5*Ycut^2 + 1785*m2^2*MBhat^5*
            Ycut^2 + 315*m2^3*MBhat^5*Ycut^2 - 300*MBhat^6*Ycut^2 - 
           250*m2*MBhat^6*Ycut^2 + 340*m2^2*MBhat^6*Ycut^2 - 42*Ycut^3 + 
           210*m2*Ycut^3 - 420*m2^2*Ycut^3 + 420*m2^3*Ycut^3 - 
           210*m2^4*Ycut^3 + 42*m2^5*Ycut^3 - 1338*MBhat*Ycut^3 + 
           5331*m2*MBhat*Ycut^3 - 7944*m2^2*MBhat*Ycut^3 + 
           5226*m2^3*MBhat*Ycut^3 - 1254*m2^4*MBhat*Ycut^3 - 
           21*m2^5*MBhat*Ycut^3 - 4860*MBhat^2*Ycut^3 + 16407*m2*MBhat^2*
            Ycut^3 - 20124*m2^2*MBhat^2*Ycut^3 + 10530*m2^3*MBhat^2*Ycut^3 - 
           2016*m2^4*MBhat^2*Ycut^3 + 63*m2^5*MBhat^2*Ycut^3 + 
           3972*MBhat^3*Ycut^3 - 6122*m2*MBhat^3*Ycut^3 - 1716*m2^2*MBhat^3*
            Ycut^3 + 6120*m2^3*MBhat^3*Ycut^3 - 2464*m2^4*MBhat^3*Ycut^3 + 
           210*m2^5*MBhat^3*Ycut^3 + 8568*MBhat^4*Ycut^3 - 
           20715*m2*MBhat^4*Ycut^3 + 15927*m2^2*MBhat^4*Ycut^3 - 
           3906*m2^3*MBhat^4*Ycut^3 + 126*m2^4*MBhat^4*Ycut^3 - 
           7050*MBhat^5*Ycut^3 + 7875*m2*MBhat^5*Ycut^3 - 780*m2^2*MBhat^5*
            Ycut^3 - 525*m2^3*MBhat^5*Ycut^3 + 750*MBhat^6*Ycut^3 + 
           500*m2*MBhat^6*Ycut^3 - 420*m2^2*MBhat^6*Ycut^3 + 316*Ycut^4 - 
           1238*m2*Ycut^4 + 1792*m2^2*Ycut^4 - 1108*m2^3*Ycut^4 + 
           212*m2^4*Ycut^4 + 26*m2^5*Ycut^4 + 3933*MBhat*Ycut^4 - 
           12282*m2*MBhat*Ycut^4 + 13209*m2^2*MBhat*Ycut^4 - 
           5265*m2^3*MBhat*Ycut^4 + 366*m2^4*MBhat*Ycut^4 + 
           39*m2^5*MBhat*Ycut^4 + 4914*MBhat^2*Ycut^4 - 14463*m2*MBhat^2*
            Ycut^4 + 15138*m2^2*MBhat^2*Ycut^4 - 6552*m2^3*MBhat^2*Ycut^4 + 
           972*m2^4*MBhat^2*Ycut^4 - 9*m2^5*MBhat^2*Ycut^4 - 
           11298*MBhat^3*Ycut^4 + 17860*m2*MBhat^3*Ycut^4 - 
           4406*m2^2*MBhat^3*Ycut^4 - 3006*m2^3*MBhat^3*Ycut^4 + 
           920*m2^4*MBhat^3*Ycut^4 - 70*m2^5*MBhat^3*Ycut^4 - 
           3990*MBhat^4*Ycut^4 + 12045*m2*MBhat^4*Ycut^4 - 
           8928*m2^2*MBhat^4*Ycut^4 + 1446*m2^3*MBhat^4*Ycut^4 - 
           18*m2^4*MBhat^4*Ycut^4 + 7125*MBhat^5*Ycut^4 - 
           6000*m2*MBhat^5*Ycut^4 - 120*m2^2*MBhat^5*Ycut^4 + 
           255*m2^3*MBhat^5*Ycut^4 - 1000*MBhat^6*Ycut^4 - 
           500*m2*MBhat^6*Ycut^4 + 220*m2^2*MBhat^6*Ycut^4 - 1014*Ycut^5 + 
           3040*m2*Ycut^5 - 3028*m2^2*Ycut^5 + 984*m2^3*Ycut^5 + 
           26*m2^4*Ycut^5 - 8*m2^5*Ycut^5 - 6084*MBhat*Ycut^5 + 
           14520*m2*MBhat*Ycut^5 - 10836*m2^2*MBhat*Ycut^5 + 
           2439*m2^3*MBhat*Ycut^5 - 30*m2^4*MBhat*Ycut^5 - 
           9*m2^5*MBhat*Ycut^5 + 378*MBhat^2*Ycut^5 + 2295*m2*MBhat^2*
            Ycut^5 - 4842*m2^2*MBhat^2*Ycut^5 + 2286*m2^3*MBhat^2*Ycut^5 - 
           117*m2^4*MBhat^2*Ycut^5 + 12600*MBhat^3*Ycut^5 - 
           16580*m2*MBhat^3*Ycut^5 + 4064*m2^2*MBhat^3*Ycut^5 + 
           738*m2^3*MBhat^3*Ycut^5 - 112*m2^4*MBhat^3*Ycut^5 + 
           10*m2^5*MBhat^3*Ycut^5 - 2730*MBhat^4*Ycut^5 - 
           2235*m2*MBhat^4*Ycut^5 + 2787*m2^2*MBhat^4*Ycut^5 - 
           87*m2^3*MBhat^4*Ycut^5 - 3900*MBhat^5*Ycut^5 + 
           2700*m2*MBhat^5*Ycut^5 + 240*m2^2*MBhat^5*Ycut^5 - 
           45*m2^3*MBhat^5*Ycut^5 + 750*MBhat^6*Ycut^5 + 250*m2*MBhat^6*
            Ycut^5 - 40*m2^2*MBhat^6*Ycut^5 + 1800*Ycut^6 - 3980*m2*Ycut^6 + 
           2532*m2^2*Ycut^6 - 324*m2^3*Ycut^6 - 28*m2^4*Ycut^6 + 
           5025*MBhat*Ycut^6 - 8805*m2*MBhat*Ycut^6 + 4344*m2^2*MBhat*
            Ycut^6 - 537*m2^3*MBhat*Ycut^6 - 27*m2^4*MBhat*Ycut^6 - 
           5670*MBhat^2*Ycut^6 + 5445*m2*MBhat^2*Ycut^6 - 
           72*m2^2*MBhat^2*Ycut^6 - 306*m2^3*MBhat^2*Ycut^6 - 
           18*m2^4*MBhat^2*Ycut^6 - 6090*MBhat^3*Ycut^6 + 
           6730*m2*MBhat^3*Ycut^6 - 1356*m2^2*MBhat^3*Ycut^6 - 
           138*m2^3*MBhat^3*Ycut^6 - 10*m2^4*MBhat^3*Ycut^6 + 
           4410*MBhat^4*Ycut^6 - 975*m2*MBhat^4*Ycut^6 - 438*m2^2*MBhat^4*
            Ycut^6 - 45*m2^3*MBhat^4*Ycut^6 + 825*MBhat^5*Ycut^6 - 
           675*m2*MBhat^5*Ycut^6 - 75*m2^2*MBhat^5*Ycut^6 - 
           300*MBhat^6*Ycut^6 - 50*m2*MBhat^6*Ycut^6 - 1910*Ycut^7 + 
           2930*m2*Ycut^7 - 1048*m2^2*Ycut^7 + 28*m2^3*Ycut^7 - 
           1674*MBhat*Ycut^7 + 1995*m2*MBhat*Ycut^7 - 636*m2^2*MBhat*Ycut^7 + 
           27*m2^3*MBhat*Ycut^7 + 5292*MBhat^2*Ycut^7 - 3879*m2*MBhat^2*
            Ycut^7 + 324*m2^2*MBhat^2*Ycut^7 + 18*m2^3*MBhat^2*Ycut^7 + 
           84*MBhat^3*Ycut^7 - 746*m2*MBhat^3*Ycut^7 + 148*m2^2*MBhat^3*
            Ycut^7 + 10*m2^3*MBhat^3*Ycut^7 - 1992*MBhat^4*Ycut^7 + 
           483*m2*MBhat^4*Ycut^7 + 45*m2^2*MBhat^4*Ycut^7 + 
           150*MBhat^5*Ycut^7 + 75*m2*MBhat^5*Ycut^7 + 50*MBhat^6*Ycut^7 + 
           1212*Ycut^8 - 1150*m2*Ycut^8 + 172*m2^2*Ycut^8 - 
           477*MBhat*Ycut^8 + 384*m2*MBhat*Ycut^8 - 27*m2^2*MBhat*Ycut^8 - 
           1890*MBhat^2*Ycut^8 + 783*m2*MBhat^2*Ycut^8 - 18*m2^2*MBhat^2*
            Ycut^8 + 1008*MBhat^3*Ycut^8 - 158*m2*MBhat^3*Ycut^8 - 
           10*m2^2*MBhat^3*Ycut^8 + 222*MBhat^4*Ycut^8 - 45*m2*MBhat^4*
            Ycut^8 - 75*MBhat^5*Ycut^8 - 426*Ycut^9 + 188*m2*Ycut^9 + 
           552*MBhat*Ycut^9 - 198*m2*MBhat*Ycut^9 + 81*MBhat^2*Ycut^9 + 
           18*m2*MBhat^2*Ycut^9 - 252*MBhat^3*Ycut^9 + 10*m2*MBhat^3*Ycut^9 + 
           45*MBhat^4*Ycut^9 + 64*Ycut^10 - 126*MBhat*Ycut^10 + 
           72*MBhat^2*Ycut^10 - 10*MBhat^3*Ycut^10)*c[T])/(-1 + Ycut)^7 + 
       c[SR]*(-1/10*(Sqrt[m2]*(-1 + m2 + Ycut)*(-2 - 204*m2 - 1054*m2^2 - 
             1054*m2^3 - 204*m2^4 - 2*m2^5 + 32*MBhat^2 + 1487*m2*MBhat^2 + 
             2312*m2^2*MBhat^2 - 388*m2^3*MBhat^2 - 88*m2^4*MBhat^2 + 
             5*m2^5*MBhat^2 - 80*MBhat^3 - 2272*m2*MBhat^3 - 
             1272*m2^2*MBhat^3 + 328*m2^3*MBhat^3 - 72*m2^4*MBhat^3 + 
             8*m2^5*MBhat^3 + 60*MBhat^4 + 1095*m2*MBhat^4 - 
             105*m2^2*MBhat^4 - 165*m2^3*MBhat^4 + 15*m2^4*MBhat^4 - 
             10*MBhat^6 - 100*m2*MBhat^6 - 10*m2^2*MBhat^6 + 10*Ycut + 
             1078*m2*Ycut + 5724*m2^2*Ycut + 5870*m2^3*Ycut + 
             1166*m2^4*Ycut + 12*m2^5*Ycut - 160*MBhat^2*Ycut - 
             7943*m2*MBhat^2*Ycut - 12921*m2^2*MBhat^2*Ycut + 
             2051*m2^3*MBhat^2*Ycut + 523*m2^4*MBhat^2*Ycut - 
             30*m2^5*MBhat^2*Ycut + 400*MBhat^3*Ycut + 12240*m2*MBhat^3*
              Ycut + 7368*m2^2*MBhat^3*Ycut - 1904*m2^3*MBhat^3*Ycut + 
             424*m2^4*MBhat^3*Ycut - 48*m2^5*MBhat^3*Ycut - 
             300*MBhat^4*Ycut - 5955*m2*MBhat^4*Ycut + 420*m2^2*MBhat^4*
              Ycut + 975*m2^3*MBhat^4*Ycut - 90*m2^4*MBhat^4*Ycut + 
             50*MBhat^6*Ycut + 550*m2*MBhat^6*Ycut + 60*m2^2*MBhat^6*Ycut - 
             20*Ycut^2 - 2302*m2*Ycut^2 - 12628*m2^2*Ycut^2 - 
             13358*m2^3*Ycut^2 - 2742*m2^4*Ycut^2 - 30*m2^5*Ycut^2 + 
             320*MBhat^2*Ycut^2 + 17172*m2*MBhat^2*Ycut^2 + 
             29496*m2^2*MBhat^2*Ycut^2 - 4333*m2^3*MBhat^2*Ycut^2 - 
             1290*m2^4*MBhat^2*Ycut^2 + 75*m2^5*MBhat^2*Ycut^2 - 
             800*MBhat^3*Ycut^2 - 26720*m2*MBhat^3*Ycut^2 - 
             17552*m2^2*MBhat^3*Ycut^2 + 4544*m2^3*MBhat^3*Ycut^2 - 
             1032*m2^4*MBhat^3*Ycut^2 + 120*m2^5*MBhat^3*Ycut^2 + 
             600*MBhat^4*Ycut^2 + 13140*m2*MBhat^4*Ycut^2 - 480*m2^2*MBhat^4*
              Ycut^2 - 2385*m2^3*MBhat^4*Ycut^2 + 225*m2^4*MBhat^4*Ycut^2 - 
             100*MBhat^6*Ycut^2 - 1230*m2*MBhat^6*Ycut^2 - 150*m2^2*MBhat^6*
              Ycut^2 + 20*Ycut^3 + 2498*m2*Ycut^3 + 14270*m2^2*Ycut^3 + 
             15712*m2^3*Ycut^3 + 3370*m2^4*Ycut^3 + 40*m2^5*Ycut^3 - 
             320*MBhat^2*Ycut^3 - 18908*m2*MBhat^2*Ycut^3 - 
             34772*m2^2*MBhat^2*Ycut^3 + 4535*m2^3*MBhat^2*Ycut^3 + 
             1685*m2^4*MBhat^2*Ycut^3 - 100*m2^5*MBhat^2*Ycut^3 + 
             870*MBhat^3*Ycut^3 + 29480*m2*MBhat^3*Ycut^3 + 
             22228*m2^2*MBhat^3*Ycut^3 - 5928*m2^3*MBhat^3*Ycut^3 + 
             1390*m2^4*MBhat^3*Ycut^3 - 160*m2^5*MBhat^3*Ycut^3 - 
             720*MBhat^4*Ycut^3 - 14460*m2*MBhat^4*Ycut^3 - 540*m2^2*MBhat^4*
              Ycut^3 + 3195*m2^3*MBhat^4*Ycut^3 - 300*m2^4*MBhat^4*Ycut^3 + 
             50*MBhat^5*Ycut^3 - 100*m2*MBhat^5*Ycut^3 + 50*m2^2*MBhat^5*
              Ycut^3 + 100*MBhat^6*Ycut^3 + 1410*m2*MBhat^6*Ycut^3 + 
             200*m2^2*MBhat^6*Ycut^3 - 10*Ycut^4 - 1397*m2*Ycut^4 - 
             8427*m2^2*Ycut^4 - 9815*m2^3*Ycut^4 - 2245*m2^4*Ycut^4 - 
             30*m2^5*Ycut^4 + 55*MBhat^2*Ycut^4 + 11192*m2*MBhat^2*Ycut^4 + 
             21090*m2^2*MBhat^2*Ycut^4 - 1855*m2^3*MBhat^2*Ycut^4 - 
             1325*m2^4*MBhat^2*Ycut^4 + 75*m2^5*MBhat^2*Ycut^4 - 
             570*MBhat^3*Ycut^4 - 16590*m2*MBhat^3*Ycut^4 - 
             15402*m2^2*MBhat^3*Ycut^4 + 4230*m2^3*MBhat^3*Ycut^4 - 
             1020*m2^4*MBhat^3*Ycut^4 + 120*m2^5*MBhat^3*Ycut^4 + 
             825*MBhat^4*Ycut^4 + 7455*m2*MBhat^4*Ycut^4 + 1695*m2^2*MBhat^4*
              Ycut^4 - 2370*m2^3*MBhat^4*Ycut^4 + 225*m2^4*MBhat^4*Ycut^4 - 
             250*MBhat^5*Ycut^4 + 310*m2*MBhat^5*Ycut^4 - 60*m2^2*MBhat^5*
              Ycut^4 - 50*MBhat^6*Ycut^4 - 820*m2*MBhat^6*Ycut^4 - 
             150*m2^2*MBhat^6*Ycut^4 + 2*Ycut^5 + 339*m2*Ycut^5 + 
             2232*m2^2*Ycut^5 + 2857*m2^3*Ycut^5 + 732*m2^4*Ycut^5 + 
             12*m2^5*Ycut^5 + 63*MBhat*Ycut^5 - 252*m2*MBhat*Ycut^5 + 
             378*m2^2*MBhat*Ycut^5 - 252*m2^3*MBhat*Ycut^5 + 
             63*m2^4*MBhat*Ycut^5 + 385*MBhat^2*Ycut^5 - 3993*m2*MBhat^2*
              Ycut^5 - 4863*m2^2*MBhat^2*Ycut^5 - 238*m2^3*MBhat^2*Ycut^5 + 
             507*m2^4*MBhat^2*Ycut^5 - 30*m2^5*MBhat^2*Ycut^5 - 
             75*MBhat^3*Ycut^5 + 4434*m2*MBhat^3*Ycut^5 + 5112*m2^2*MBhat^3*
              Ycut^5 - 1578*m2^3*MBhat^3*Ycut^5 + 387*m2^4*MBhat^3*Ycut^5 - 
             48*m2^5*MBhat^3*Ycut^5 - 885*MBhat^4*Ycut^5 - 630*m2*MBhat^4*
              Ycut^5 - 1455*m2^2*MBhat^4*Ycut^5 + 855*m2^3*MBhat^4*Ycut^5 - 
             90*m2^4*MBhat^4*Ycut^5 + 500*MBhat^5*Ycut^5 - 360*m2*MBhat^5*
              Ycut^5 - 60*m2^2*MBhat^5*Ycut^5 + 10*MBhat^6*Ycut^5 + 
             150*m2*MBhat^6*Ycut^5 + 60*m2^2*MBhat^6*Ycut^5 - 14*Ycut^6 + 
             46*m2*Ycut^6 - 182*m2^2*Ycut^6 - 125*m2^3*Ycut^6 - 
             83*m2^4*Ycut^6 - 2*m2^5*Ycut^6 - 291*MBhat*Ycut^6 + 
             867*m2*MBhat*Ycut^6 - 855*m2^2*MBhat*Ycut^6 + 273*m2^3*MBhat*
              Ycut^6 + 6*m2^4*MBhat*Ycut^6 - 520*MBhat^2*Ycut^6 + 
             1382*m2*MBhat^2*Ycut^6 - 661*m2^2*MBhat^2*Ycut^6 + 
             341*m2^3*MBhat^2*Ycut^6 - 67*m2^4*MBhat^2*Ycut^6 + 
             5*m2^5*MBhat^2*Ycut^6 + 875*MBhat^3*Ycut^6 - 1225*m2*MBhat^3*
              Ycut^6 - 433*m2^2*MBhat^3*Ycut^6 + 349*m2^3*MBhat^3*Ycut^6 - 
             54*m2^4*MBhat^3*Ycut^6 + 8*m2^5*MBhat^3*Ycut^6 + 
             450*MBhat^4*Ycut^6 - 930*m2*MBhat^4*Ycut^6 + 615*m2^2*MBhat^4*
              Ycut^6 - 90*m2^3*MBhat^4*Ycut^6 + 15*m2^4*MBhat^4*Ycut^6 - 
             500*MBhat^5*Ycut^6 + 220*m2*MBhat^5*Ycut^6 + 100*m2^2*MBhat^5*
              Ycut^6 + 70*m2*MBhat^6*Ycut^6 - 10*m2^2*MBhat^6*Ycut^6 + 
             70*Ycut^7 - 204*m2*Ycut^7 + 174*m2^2*Ycut^7 - 71*m2^3*Ycut^7 - 
             14*m2^4*Ycut^7 + 510*MBhat*Ycut^7 - 1068*m2*MBhat*Ycut^7 + 
             597*m2^2*MBhat*Ycut^7 - 30*m2^3*MBhat*Ycut^7 - 
             9*m2^4*MBhat*Ycut^7 + 20*MBhat^2*Ycut^7 - 218*m2*MBhat^2*
              Ycut^7 + 381*m2^2*MBhat^2*Ycut^7 - 118*m2^3*MBhat^2*Ycut^7 - 
             5*m2^4*MBhat^2*Ycut^7 - 1000*MBhat^3*Ycut^7 + 960*m2*MBhat^3*
              Ycut^7 - 93*m2^2*MBhat^3*Ycut^7 - 44*m2^3*MBhat^3*Ycut^7 - 
             3*m2^4*MBhat^3*Ycut^7 + 150*MBhat^4*Ycut^7 + 300*m2*MBhat^4*
              Ycut^7 - 165*m2^2*MBhat^4*Ycut^7 - 15*m2^3*MBhat^4*Ycut^7 + 
             250*MBhat^5*Ycut^7 - 100*m2*MBhat^5*Ycut^7 - 30*m2^2*MBhat^5*
              Ycut^7 - 30*m2*MBhat^6*Ycut^7 - 140*Ycut^8 + 271*m2*Ycut^8 - 
             125*m2^2*Ycut^8 - 16*m2^3*Ycut^8 - 390*MBhat*Ycut^8 + 
             522*m2*MBhat*Ycut^8 - 141*m2^2*MBhat*Ycut^8 + 9*m2^3*MBhat*
              Ycut^8 + 455*MBhat^2*Ycut^8 - 303*m2*MBhat^2*Ycut^8 - 
             57*m2^2*MBhat^2*Ycut^8 + 5*m2^3*MBhat^2*Ycut^8 + 
             380*MBhat^3*Ycut^8 - 340*m2*MBhat^3*Ycut^8 + 47*m2^2*MBhat^3*
              Ycut^8 + 3*m2^3*MBhat^3*Ycut^8 - 255*MBhat^4*Ycut^8 + 
             15*m2^2*MBhat^4*Ycut^8 - 50*MBhat^5*Ycut^8 + 30*m2*MBhat^5*
              Ycut^8 + 140*Ycut^9 - 159*m2*Ycut^9 + 16*m2^2*Ycut^9 + 
             75*MBhat*Ycut^9 - 48*m2*MBhat*Ycut^9 + 21*m2^2*MBhat*Ycut^9 - 
             335*MBhat^2*Ycut^9 + 142*m2*MBhat^2*Ycut^9 - 5*m2^2*MBhat^2*
              Ycut^9 + 45*MBhat^3*Ycut^9 + 30*m2*MBhat^3*Ycut^9 - 
             3*m2^2*MBhat^3*Ycut^9 + 75*MBhat^4*Ycut^9 - 15*m2*MBhat^4*
              Ycut^9 - 70*Ycut^10 + 34*m2*Ycut^10 + 57*MBhat*Ycut^10 - 
             21*m2*MBhat*Ycut^10 + 58*MBhat^2*Ycut^10 - 10*m2*MBhat^2*
              Ycut^10 - 45*MBhat^3*Ycut^10 + 3*m2*MBhat^3*Ycut^10 + 
             14*Ycut^11 - 24*MBhat*Ycut^11 + 10*MBhat^2*Ycut^11))/
           (-1 + Ycut)^6 - 6*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 
           9*MBhat^2 - 39*m2*MBhat^2 - 14*m2^2*MBhat^2 + 6*m2^3*MBhat^2 + 
           16*MBhat^3 + 40*m2*MBhat^3 - 9*MBhat^4 - 12*m2*MBhat^4 + 
           6*m2^2*MBhat^4 + MBhat^6 + m2*MBhat^6)*Log[m2] + 
         6*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 9*MBhat^2 - 
           39*m2*MBhat^2 - 14*m2^2*MBhat^2 + 6*m2^3*MBhat^2 + 16*MBhat^3 + 
           40*m2*MBhat^3 - 9*MBhat^4 - 12*m2*MBhat^4 + 6*m2^2*MBhat^4 + 
           MBhat^6 + m2*MBhat^6)*Log[1 - Ycut]))) + 
   muG*(((-1 + m2 + Ycut)*(345 + 12378*m2 - 16497*m2^2 - 49572*m2^3 + 
        6603*m2^4 + 1458*m2^5 - 75*m2^6 - 2360*MBhat - 52060*m2*MBhat + 
        104180*m2^2*MBhat + 93680*m2^3*MBhat - 11320*m2^4*MBhat + 
        2540*m2^5*MBhat - 260*m2^6*MBhat + 6786*MBhat^2 + 80846*m2*MBhat^2 - 
        188374*m2^2*MBhat^2 - 30244*m2^3*MBhat^2 - 4414*m2^4*MBhat^2 + 
        3230*m2^5*MBhat^2 - 550*m2^6*MBhat^2 - 10392*MBhat^3 - 
        53400*m2*MBhat^3 + 120060*m2^2*MBhat^3 - 6360*m2^3*MBhat^3 - 
        2160*m2^4*MBhat^3 + 3552*m2^5*MBhat^3 - 900*m2^6*MBhat^3 + 
        8771*MBhat^4 + 9926*m2*MBhat^4 - 5194*m2^2*MBhat^4 - 
        16954*m2^3*MBhat^4 + 12131*m2^4*MBhat^4 - 2800*m2^5*MBhat^4 - 
        3780*MBhat^5 + 3360*m2*MBhat^5 - 18480*m2^2*MBhat^5 + 
        11760*m2^3*MBhat^5 - 2940*m2^4*MBhat^5 + 630*MBhat^6 - 
        1050*m2*MBhat^6 + 3990*m2^2*MBhat^6 - 1050*m2^3*MBhat^6 - 1725*Ycut - 
        66585*m2*Ycut + 83868*m2^2*Ycut + 276846*m2^3*Ycut - 
        34701*m2^4*Ycut - 8673*m2^5*Ycut + 450*m2^6*Ycut + 11800*MBhat*Ycut + 
        283140*m2*MBhat*Ycut - 550120*m2^2*MBhat*Ycut - 
        536240*m2^3*MBhat*Ycut + 65640*m2^4*MBhat*Ycut - 
        14980*m2^5*MBhat*Ycut + 1560*m2^6*MBhat*Ycut - 33930*MBhat^2*Ycut - 
        447844*m2*MBhat^2*Ycut + 1019422*m2^2*MBhat^2*Ycut + 
        184038*m2^3*MBhat^2*Ycut + 23804*m2^4*MBhat^2*Ycut - 
        18830*m2^5*MBhat^2*Ycut + 3300*m2^6*MBhat^2*Ycut + 
        51960*MBhat^3*Ycut + 307008*m2*MBhat^3*Ycut - 669132*m2^2*MBhat^3*
         Ycut + 37668*m2^3*MBhat^3*Ycut + 10308*m2^4*MBhat^3*Ycut - 
        20412*m2^5*MBhat^3*Ycut + 5400*m2^6*MBhat^3*Ycut - 
        43855*MBhat^4*Ycut - 66059*m2*MBhat^4*Ycut + 38367*m2^2*MBhat^4*
         Ycut + 92393*m2^3*MBhat^4*Ycut - 69986*m2^4*MBhat^4*Ycut + 
        16800*m2^5*MBhat^4*Ycut + 18900*MBhat^5*Ycut - 
        15540*m2*MBhat^5*Ycut + 102060*m2^2*MBhat^5*Ycut - 
        67620*m2^3*MBhat^5*Ycut + 17640*m2^4*MBhat^5*Ycut - 
        3150*MBhat^6*Ycut + 5880*m2*MBhat^6*Ycut - 22890*m2^2*MBhat^6*Ycut + 
        6300*m2^3*MBhat^6*Ycut + 3450*Ycut^2 + 145080*m2*Ycut^2 - 
        169527*m2^2*Ycut^2 - 631356*m2^3*Ycut^2 + 72618*m2^4*Ycut^2 + 
        21420*m2^5*Ycut^2 - 1125*m2^6*Ycut^2 - 23600*MBhat*Ycut^2 - 
        624560*m2*MBhat*Ycut^2 + 1168920*m2^2*MBhat*Ycut^2 + 
        1260580*m2^3*MBhat*Ycut^2 - 156380*m2^4*MBhat*Ycut^2 + 
        36540*m2^5*MBhat*Ycut^2 - 3900*m2^6*MBhat*Ycut^2 + 
        67860*MBhat^2*Ycut^2 + 1008116*m2*MBhat^2*Ycut^2 - 
        2232522*m2^2*MBhat^2*Ycut^2 - 466554*m2^3*MBhat^2*Ycut^2 - 
        50680*m2^4*MBhat^2*Ycut^2 + 45150*m2^5*MBhat^2*Ycut^2 - 
        8250*m2^6*MBhat^2*Ycut^2 - 103920*MBhat^3*Ycut^2 - 
        719232*m2*MBhat^3*Ycut^2 + 1518456*m2^2*MBhat^3*Ycut^2 - 
        90696*m2^3*MBhat^3*Ycut^2 - 17388*m2^4*MBhat^3*Ycut^2 + 
        47880*m2^5*MBhat^3*Ycut^2 - 13500*m2^6*MBhat^3*Ycut^2 + 
        87710*MBhat^4*Ycut^2 + 177576*m2*MBhat^4*Ycut^2 - 
        114807*m2^2*MBhat^4*Ycut^2 - 201124*m2^3*MBhat^4*Ycut^2 + 
        165165*m2^4*MBhat^4*Ycut^2 - 42000*m2^5*MBhat^4*Ycut^2 - 
        37800*MBhat^5*Ycut^2 + 26040*m2*MBhat^5*Ycut^2 - 
        227220*m2^2*MBhat^5*Ycut^2 + 158760*m2^3*MBhat^5*Ycut^2 - 
        44100*m2^4*MBhat^5*Ycut^2 + 6300*MBhat^6*Ycut^2 - 
        13020*m2*MBhat^6*Ycut^2 + 53550*m2^2*MBhat^6*Ycut^2 - 
        15750*m2^3*MBhat^6*Ycut^2 - 3450*Ycut^3 - 161190*m2*Ycut^3 + 
        169083*m2^2*Ycut^3 + 743127*m2^3*Ycut^3 - 74655*m2^4*Ycut^3 - 
        28035*m2^5*Ycut^3 + 1500*m2^6*Ycut^3 + 23600*MBhat*Ycut^3 + 
        703840*m2*MBhat*Ycut^3 - 1252040*m2^2*MBhat*Ycut^3 - 
        1542660*m2^3*MBhat*Ycut^3 + 193760*m2^4*MBhat*Ycut^3 - 
        46900*m2^5*MBhat*Ycut^3 + 5200*m2^6*MBhat*Ycut^3 - 
        67860*MBhat^2*Ycut^3 - 1162544*m2*MBhat^2*Ycut^3 + 
        2486614*m2^2*MBhat^2*Ycut^3 + 629020*m2^3*MBhat^2*Ycut^3 + 
        51380*m2^4*MBhat^2*Ycut^3 - 56350*m2^5*MBhat^2*Ycut^3 + 
        11000*m2^6*MBhat^2*Ycut^3 + 102940*MBhat^3*Ycut^3 + 
        873868*m2*MBhat^3*Ycut^3 - 1779536*m2^2*MBhat^3*Ycut^3 + 
        109928*m2^3*MBhat^3*Ycut^3 + 19040*m2^4*MBhat^3*Ycut^3 - 
        62440*m2^5*MBhat^3*Ycut^3 + 18000*m2^6*MBhat^3*Ycut^3 - 
        85190*MBhat^4*Ycut^3 - 259154*m2*MBhat^4*Ycut^3 + 
        186739*m2^2*MBhat^4*Ycut^3 + 231035*m2^3*MBhat^4*Ycut^3 - 
        213220*m2^4*MBhat^4*Ycut^3 + 56000*m2^5*MBhat^4*Ycut^3 + 
        35700*MBhat^5*Ycut^3 - 7980*m2*MBhat^5*Ycut^3 + 
        258720*m2^2*MBhat^5*Ycut^3 - 201600*m2^3*MBhat^5*Ycut^3 + 
        58800*m2^4*MBhat^5*Ycut^3 - 5740*MBhat^6*Ycut^3 + 
        13160*m2*MBhat^6*Ycut^3 - 66850*m2^2*MBhat^6*Ycut^3 + 
        21000*m2^3*MBhat^6*Ycut^3 + 1725*Ycut^4 + 92850*m2*Ycut^4 - 
        81417*m2^2*Ycut^4 - 462840*m2^3*Ycut^4 + 35805*m2^4*Ycut^4 + 
        20370*m2^5*Ycut^4 - 1125*m2^6*Ycut^4 - 11800*MBhat*Ycut^4 - 
        412560*m2*MBhat*Ycut^4 + 679000*m2^2*MBhat*Ycut^4 + 
        1013740*m2^3*MBhat*Ycut^4 - 128100*m2^4*MBhat*Ycut^4 + 
        32900*m2^5*MBhat*Ycut^4 - 3900*m2^6*MBhat*Ycut^4 + 
        34875*MBhat^2*Ycut^4 + 693556*m2*MBhat^2*Ycut^4 - 
        1417990*m2^2*MBhat^2*Ycut^4 - 462840*m2^3*MBhat^2*Ycut^4 - 
        38815*m2^4*MBhat^2*Ycut^4 + 44800*m2^5*MBhat^2*Ycut^4 - 
        8250*m2^6*MBhat^2*Ycut^4 - 49510*MBhat^3*Ycut^4 - 
        569412*m2*MBhat^3*Ycut^4 + 1127392*m2^2*MBhat^3*Ycut^4 - 
        107660*m2^3*MBhat^3*Ycut^4 + 6090*m2^4*MBhat^3*Ycut^4 + 
        45080*m2^5*MBhat^3*Ycut^4 - 13500*m2^6*MBhat^3*Ycut^4 + 
        33670*MBhat^4*Ycut^4 + 237566*m2*MBhat^4*Ycut^4 - 
        207585*m2^2*MBhat^4*Ycut^4 - 127960*m2^3*MBhat^4*Ycut^4 + 
        157465*m2^4*MBhat^4*Ycut^4 - 42000*m2^5*MBhat^4*Ycut^4 - 
        9660*MBhat^5*Ycut^4 - 39060*m2*MBhat^5*Ycut^4 - 
        147000*m2^2*MBhat^5*Ycut^4 + 149100*m2^3*MBhat^5*Ycut^4 - 
        44100*m2^4*MBhat^5*Ycut^4 + 700*MBhat^6*Ycut^4 - 
        2940*m2*MBhat^6*Ycut^4 + 48650*m2^2*MBhat^6*Ycut^4 - 
        15750*m2^3*MBhat^6*Ycut^4 - 345*Ycut^5 - 23541*m2*Ycut^5 + 
        13482*m2^2*Ycut^5 + 132762*m2^3*Ycut^5 - 4053*m2^4*Ycut^5 - 
        7623*m2^5*Ycut^5 + 450*m2^6*Ycut^5 + 1898*MBhat*Ycut^5 + 
        110558*m2*MBhat*Ycut^5 - 153972*m2^2*MBhat*Ycut^5 - 
        323792*m2^3*MBhat*Ycut^5 + 50218*m2^4*MBhat*Ycut^5 - 
        15750*m2^5*MBhat*Ycut^5 + 1560*m2^6*MBhat*Ycut^5 - 
        10083*MBhat^2*Ycut^5 - 166817*m2*MBhat^2*Ycut^5 + 
        317793*m2^2*MBhat^2*Ycut^5 + 184653*m2^3*MBhat^2*Ycut^5 + 
        16268*m2^4*MBhat^2*Ycut^5 - 19950*m2^5*MBhat^2*Ycut^5 + 
        3300*m2^6*MBhat^2*Ycut^5 + 11162*MBhat^3*Ycut^5 + 
        168266*m2*MBhat^3*Ycut^5 - 368802*m2^2*MBhat^3*Ycut^5 + 
        112378*m2^3*MBhat^3*Ycut^5 - 35252*m2^4*MBhat^3*Ycut^5 - 
        16632*m2^5*MBhat^3*Ycut^5 + 5400*m2^6*MBhat^3*Ycut^5 + 
        5278*MBhat^4*Ycut^5 - 152796*m2*MBhat^4*Ycut^5 + 
        190239*m2^2*MBhat^4*Ycut^5 - 8701*m2^3*MBhat^4*Ycut^5 - 
        61026*m2^4*MBhat^4*Ycut^5 + 16800*m2^5*MBhat^4*Ycut^5 - 
        11130*MBhat^5*Ycut^5 + 70770*m2*MBhat^5*Ycut^5 + 
        15750*m2^2*MBhat^5*Ycut^5 - 61110*m2^3*MBhat^5*Ycut^5 + 
        17640*m2^4*MBhat^5*Ycut^5 + 3220*MBhat^6*Ycut^5 - 
        6440*m2*MBhat^6*Ycut^5 - 20790*m2^2*MBhat^6*Ycut^5 + 
        6300*m2^3*MBhat^6*Ycut^5 + 91*Ycut^6 + 196*m2*Ycut^6 + 
        1288*m2^2*Ycut^6 - 5740*m2^3*Ycut^6 - 4228*m2^4*Ycut^6 + 
        1988*m2^5*Ycut^6 - 75*m2^6*Ycut^6 + 1904*MBhat*Ycut^6 - 
        15848*m2*MBhat*Ycut^6 + 11620*m2^2*MBhat*Ycut^6 + 
        32088*m2^3*MBhat*Ycut^6 - 14224*m2^4*MBhat*Ycut^6 + 
        3920*m2^5*MBhat*Ycut^6 - 260*m2^6*MBhat*Ycut^6 + 
        2919*MBhat^2*Ycut^6 - 13118*m2*MBhat^2*Ycut^6 + 
        41335*m2^2*MBhat^2*Ycut^6 - 59822*m2^3*MBhat^2*Ycut^6 + 
        5936*m2^4*MBhat^2*Ycut^6 + 4340*m2^5*MBhat^2*Ycut^6 - 
        550*m2^6*MBhat^2*Ycut^6 - 6664*MBhat^3*Ycut^6 + 
        9436*m2*MBhat^3*Ycut^6 + 50134*m2^2*MBhat^3*Ycut^6 - 
        71708*m2^3*MBhat^3*Ycut^6 + 27230*m2^4*MBhat^3*Ycut^6 + 
        2072*m2^5*MBhat^3*Ycut^6 - 900*m2^6*MBhat^3*Ycut^6 - 
        5600*MBhat^4*Ycut^6 + 69034*m2*MBhat^4*Ycut^6 - 
        130697*m2^2*MBhat^4*Ycut^6 + 49952*m2^3*MBhat^4*Ycut^6 + 
        9191*m2^4*MBhat^4*Ycut^6 - 2800*m2^5*MBhat^4*Ycut^6 + 
        9450*MBhat^5*Ycut^6 - 56280*m2*MBhat^5*Ycut^6 + 
        30030*m2^2*MBhat^5*Ycut^6 + 10920*m2^3*MBhat^5*Ycut^6 - 
        2940*m2^4*MBhat^5*Ycut^6 - 2100*MBhat^6*Ycut^6 + 
        6580*m2*MBhat^6*Ycut^6 + 4690*m2^2*MBhat^6*Ycut^6 - 
        1050*m2^3*MBhat^6*Ycut^6 - 415*Ycut^7 + 2511*m2*Ycut^7 - 
        1241*m2^2*Ycut^7 - 4181*m2^3*Ycut^7 + 2721*m2^4*Ycut^7 - 
        205*m2^5*Ycut^7 - 2680*MBhat*Ycut^7 + 13392*m2*MBhat*Ycut^7 - 
        14678*m2^2*MBhat*Ycut^7 + 6490*m2^3*MBhat*Ycut^7 + 
        246*m2^4*MBhat*Ycut^7 - 370*m2^5*MBhat*Ycut^7 + 1845*MBhat^2*Ycut^7 + 
        1537*m2*MBhat^2*Ycut^7 - 29368*m2^2*MBhat^2*Ycut^7 + 
        25050*m2^3*MBhat^2*Ycut^7 - 4084*m2^4*MBhat^2*Ycut^7 - 
        290*m2^5*MBhat^2*Ycut^7 + 4610*MBhat^3*Ycut^7 - 
        21234*m2*MBhat^3*Ycut^7 + 7480*m2^2*MBhat^3*Ycut^7 + 
        18652*m2^3*MBhat^3*Ycut^7 - 8928*m2^4*MBhat^3*Ycut^7 + 
        200*m2^5*MBhat^3*Ycut^7 - 2660*MBhat^4*Ycut^7 - 
        15526*m2*MBhat^4*Ycut^7 + 49077*m2^2*MBhat^4*Ycut^7 - 
        21511*m2^3*MBhat^4*Ycut^7 + 280*m2^4*MBhat^4*Ycut^7 + 
        21840*m2*MBhat^5*Ycut^7 - 16170*m2^2*MBhat^5*Ycut^7 - 
        210*m2^3*MBhat^5*Ycut^7 - 700*MBhat^6*Ycut^7 - 
        2520*m2*MBhat^6*Ycut^7 - 350*m2^2*MBhat^6*Ycut^7 + 710*Ycut^8 - 
        3184*m2*Ycut^8 + 1560*m2^2*Ycut^8 + 844*m2^3*Ycut^8 - 
        110*m2^4*Ycut^8 + 1010*MBhat*Ycut^8 - 5548*m2*MBhat*Ycut^8 + 
        8334*m2^2*MBhat*Ycut^8 - 4076*m2^3*MBhat*Ycut^8 + 
        160*m2^4*MBhat*Ycut^8 - 3765*MBhat^2*Ycut^8 + 
        9322*m2*MBhat^2*Ycut^8 + 1374*m2^2*MBhat^2*Ycut^8 - 
        3396*m2^3*MBhat^2*Ycut^8 + 605*m2^4*MBhat^2*Ycut^8 + 
        1940*MBhat^3*Ycut^8 + 3596*m2*MBhat^3*Ycut^8 - 
        6564*m2^2*MBhat^3*Ycut^8 - 2192*m2^3*MBhat^3*Ycut^8 + 
        1060*m2^4*MBhat^3*Ycut^8 + 1015*MBhat^4*Ycut^8 - 
        1596*m2*MBhat^4*Ycut^8 - 6069*m2^2*MBhat^4*Ycut^8 + 
        2870*m2^3*MBhat^4*Ycut^8 - 2100*MBhat^5*Ycut^8 - 
        2940*m2*MBhat^5*Ycut^8 + 2310*m2^2*MBhat^5*Ycut^8 + 
        1190*MBhat^6*Ycut^8 + 350*m2*MBhat^6*Ycut^8 - 510*Ycut^9 + 
        1766*m2*Ycut^9 - 664*m2^2*Ycut^9 + 110*m2^3*Ycut^9 + 
        850*MBhat*Ycut^9 - 1198*m2*MBhat*Ycut^9 - 1054*m2^2*MBhat*Ycut^9 + 
        190*m2^3*MBhat*Ycut^9 + 705*MBhat^2*Ycut^9 - 2923*m2*MBhat^2*Ycut^9 + 
        1811*m2^2*MBhat^2*Ycut^9 + 95*m2^3*MBhat^2*Ycut^9 - 
        2410*MBhat^3*Ycut^9 + 1606*m2*MBhat^3*Ycut^9 + 
        502*m2^2*MBhat^3*Ycut^9 - 10*m2^3*MBhat^3*Ycut^9 + 
        1505*MBhat^4*Ycut^9 + 959*m2*MBhat^4*Ycut^9 - 
        70*m2^2*MBhat^4*Ycut^9 + 210*MBhat^5*Ycut^9 - 210*m2*MBhat^5*Ycut^9 - 
        350*MBhat^6*Ycut^9 + 55*Ycut^10 - 216*m2*Ycut^10 + 65*m2^2*Ycut^10 - 
        668*MBhat*Ycut^10 + 864*m2*MBhat*Ycut^10 - 190*m2^2*MBhat*Ycut^10 + 
        1017*MBhat^2*Ycut^10 - 226*m2*MBhat^2*Ycut^10 - 
        95*m2^2*MBhat^2*Ycut^10 - 40*MBhat^3*Ycut^10 - 
        492*m2*MBhat^3*Ycut^10 + 10*m2^2*MBhat^3*Ycut^10 - 
        574*MBhat^4*Ycut^10 + 70*m2*MBhat^4*Ycut^10 + 210*MBhat^5*Ycut^10 + 
        109*Ycut^11 - 65*m2*Ycut^11 - 44*MBhat*Ycut^11 - 
        20*m2*MBhat*Ycut^11 - 309*MBhat^2*Ycut^11 + 95*m2*MBhat^2*Ycut^11 + 
        314*MBhat^3*Ycut^11 - 10*m2*MBhat^3*Ycut^11 - 70*MBhat^4*Ycut^11 - 
        40*Ycut^12 + 90*MBhat*Ycut^12 - 60*MBhat^2*Ycut^12 + 
        10*MBhat^3*Ycut^12))/(420*(-1 + Ycut)^6) - 
     m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 60*MBhat + 
       340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 96*m2*MBhat^2 - 
       342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 132*m2*MBhat^3 + 
       108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - m2^2*MBhat^4 - 
       12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[m2] + 
     m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 60*MBhat + 
       340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 96*m2*MBhat^2 - 
       342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 132*m2*MBhat^3 + 
       108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - m2^2*MBhat^4 - 
       12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[1 - Ycut] + 
     c[SL]^2*(((-1 + m2 + Ycut)*(-732 - 66504*m2 - 286164*m2^2 - 
          191664*m2^3 - 564*m2^4 + 1368*m2^5 - 60*m2^6 + 4860*MBhat + 
          322128*m2*MBhat + 923148*m2^2*MBhat + 299448*m2^3*MBhat - 
          9252*m2^4*MBhat + 2088*m2^5*MBhat - 180*m2^6*MBhat - 
          13551*MBhat^2 - 624987*m2*MBhat^2 - 1080162*m2^2*MBhat^2 - 
          103662*m2^3*MBhat^2 - 1287*m2^4*MBhat^2 + 1989*m2^5*MBhat^2 - 
          300*m2^6*MBhat^2 + 20280*MBhat^3 + 611640*m2*MBhat^3 + 
          533940*m2^2*MBhat^3 - 9960*m2^3*MBhat^3 + 2640*m2^4*MBhat^3 + 
          960*m2^5*MBhat^3 - 300*m2^6*MBhat^3 - 17052*MBhat^4 - 
          309372*m2*MBhat^4 - 84462*m2^2*MBhat^4 - 3822*m2^3*MBhat^4 + 
          4998*m2^4*MBhat^4 - 1050*m2^5*MBhat^4 + 7560*MBhat^5 + 
          71820*m2*MBhat^5 - 8820*m2^2*MBhat^5 + 6300*m2^3*MBhat^5 - 
          1260*m2^4*MBhat^5 - 1365*MBhat^6 - 4725*m2*MBhat^6 + 
          2835*m2^2*MBhat^6 - 525*m2^3*MBhat^6 + 3660*Ycut + 351948*m2*Ycut + 
          1560144*m2^2*Ycut + 1078680*m2^3*Ycut + 7116*m2^4*Ycut - 
          8148*m2^5*Ycut + 360*m2^6*Ycut - 24300*MBhat*Ycut - 
          1711620*m2*MBhat*Ycut - 5074992*m2^2*MBhat*Ycut - 
          1713744*m2^3*MBhat*Ycut + 53604*m2^4*MBhat*Ycut - 
          12348*m2^5*MBhat*Ycut + 1080*m2^6*MBhat*Ycut + 67755*MBhat^2*Ycut + 
          3338184*m2*MBhat^2*Ycut + 6005892*m2^2*MBhat^2*Ycut + 
          612750*m2^3*MBhat^2*Ycut + 6033*m2^4*MBhat^2*Ycut - 
          11634*m2^5*MBhat^2*Ycut + 1800*m2^6*MBhat^2*Ycut - 
          101400*MBhat^3*Ycut - 3289920*m2*MBhat^3*Ycut - 
          3020580*m2^2*MBhat^3*Ycut + 56460*m2^3*MBhat^3*Ycut - 
          16500*m2^4*MBhat^3*Ycut - 5460*m2^5*MBhat^3*Ycut + 
          1800*m2^6*MBhat^3*Ycut + 85260*MBhat^4*Ycut + 1681008*m2*MBhat^4*
           Ycut + 496566*m2^2*MBhat^4*Ycut + 18984*m2^3*MBhat^4*Ycut - 
          28938*m2^4*MBhat^4*Ycut + 6300*m2^5*MBhat^4*Ycut - 
          37800*MBhat^5*Ycut - 396900*m2*MBhat^5*Ycut + 47880*m2^2*MBhat^5*
           Ycut - 36540*m2^3*MBhat^5*Ycut + 7560*m2^4*MBhat^5*Ycut + 
          6825*MBhat^6*Ycut + 27300*m2*MBhat^6*Ycut - 16485*m2^2*MBhat^6*
           Ycut + 3150*m2^3*MBhat^6*Ycut - 7320*Ycut^2 - 752832*m2*Ycut^2 - 
          3457788*m2^2*Ycut^2 - 2486208*m2^3*Ycut^2 - 28392*m2^4*Ycut^2 + 
          20160*m2^5*Ycut^2 - 900*m2^6*Ycut^2 + 48600*MBhat*Ycut^2 + 
          3678120*m2*MBhat*Ycut^2 + 11360628*m2^2*MBhat*Ycut^2 + 
          4033584*m2^3*MBhat*Ycut^2 - 127512*m2^4*MBhat*Ycut^2 + 
          30240*m2^5*MBhat*Ycut^2 - 2700*m2^6*MBhat*Ycut^2 - 
          135510*MBhat^2*Ycut^2 - 7216266*m2*MBhat^2*Ycut^2 - 
          13630509*m2^2*MBhat^2*Ycut^2 - 1502619*m2^3*MBhat^2*Ycut^2 - 
          9471*m2^4*MBhat^2*Ycut^2 + 28035*m2^5*MBhat^2*Ycut^2 - 
          4500*m2^6*MBhat^2*Ycut^2 + 202800*MBhat^3*Ycut^2 + 
          7169280*m2*MBhat^3*Ycut^2 + 7002600*m2^2*MBhat^3*Ycut^2 - 
          129240*m2^3*MBhat^3*Ycut^2 + 43260*m2^4*MBhat^3*Ycut^2 + 
          12600*m2^5*MBhat^3*Ycut^2 - 4500*m2^6*MBhat^3*Ycut^2 - 
          170520*MBhat^4*Ycut^2 - 3705912*m2*MBhat^4*Ycut^2 - 
          1207836*m2^2*MBhat^4*Ycut^2 - 34692*m2^3*MBhat^4*Ycut^2 + 
          68670*m2^4*MBhat^4*Ycut^2 - 15750*m2^5*MBhat^4*Ycut^2 + 
          75600*MBhat^5*Ycut^2 + 892080*m2*MBhat^5*Ycut^2 - 
          103320*m2^2*MBhat^5*Ycut^2 + 86940*m2^3*MBhat^5*Ycut^2 - 
          18900*m2^4*MBhat^5*Ycut^2 - 13650*MBhat^6*Ycut^2 - 
          64470*m2*MBhat^6*Ycut^2 + 39375*m2^2*MBhat^6*Ycut^2 - 
          7875*m2^3*MBhat^6*Ycut^2 + 7320*Ycut^3 + 818568*m2*Ycut^3 + 
          3929580*m2^2*Ycut^3 + 2972172*m2^3*Ycut^3 + 54180*m2^4*Ycut^3 - 
          26460*m2^5*Ycut^3 + 1200*m2^6*Ycut^3 - 48600*MBhat*Ycut^3 - 
          4021200*m2*MBhat*Ycut^3 - 13072572*m2^2*MBhat*Ycut^3 - 
          4956588*m2^3*MBhat*Ycut^3 + 157500*m2^4*MBhat*Ycut^3 - 
          39060*m2^5*MBhat*Ycut^3 + 3600*m2^6*MBhat*Ycut^3 + 
          135510*MBhat^2*Ycut^3 + 7945164*m2*MBhat^2*Ycut^3 + 
          15958935*m2^2*MBhat^2*Ycut^3 + 1950396*m2^3*MBhat^2*Ycut^3 + 
          2205*m2^4*MBhat^2*Ycut^3 - 35280*m2^5*MBhat^2*Ycut^3 + 
          6000*m2^6*MBhat^2*Ycut^3 - 203290*MBhat^3*Ycut^3 - 
          7969210*m2*MBhat^3*Ycut^3 - 8418460*m2^2*MBhat^3*Ycut^3 + 
          135100*m2^3*MBhat^3*Ycut^3 - 51590*m2^4*MBhat^3*Ycut^3 - 
          17150*m2^5*MBhat^3*Ycut^3 + 6000*m2^6*MBhat^3*Ycut^3 + 
          171780*MBhat^4*Ycut^3 + 4178328*m2*MBhat^4*Ycut^3 + 
          1531572*m2^2*MBhat^4*Ycut^3 + 41160*m2^3*MBhat^4*Ycut^3 - 
          90510*m2^4*MBhat^4*Ycut^3 + 21000*m2^5*MBhat^4*Ycut^3 - 
          76650*MBhat^5*Ycut^3 - 1031310*m2*MBhat^5*Ycut^3 + 
          117810*m2^2*MBhat^5*Ycut^3 - 112350*m2^3*MBhat^5*Ycut^3 + 
          25200*m2^4*MBhat^5*Ycut^3 + 13930*MBhat^6*Ycut^3 + 
          79660*m2*MBhat^6*Ycut^3 - 50225*m2^2*MBhat^6*Ycut^3 + 
          10500*m2^3*MBhat^6*Ycut^3 - 3660*Ycut^4 - 458952*m2*Ycut^4 - 
          2337972*m2^2*Ycut^4 - 1898400*m2^3*Ycut^4 - 55020*m2^4*Ycut^4 + 
          19320*m2^5*Ycut^4 - 900*m2^6*Ycut^4 + 24300*MBhat*Ycut^4 + 
          2270340*m2*MBhat*Ycut^4 + 7908768*m2^2*MBhat*Ycut^4 + 
          3292380*m2^3*MBhat*Ycut^4 - 103320*m2^4*MBhat*Ycut^4 + 
          27720*m2^5*MBhat*Ycut^4 - 2700*m2^6*MBhat*Ycut^4 - 
          67020*MBhat^2*Ycut^4 - 4525296*m2*MBhat^2*Ycut^4 - 
          9894696*m2^2*MBhat^2*Ycut^4 - 1383060*m2^3*MBhat^2*Ycut^4 - 
          3990*m2^4*MBhat^2*Ycut^4 + 27510*m2^5*MBhat^2*Ycut^4 - 
          4500*m2^6*MBhat^2*Ycut^4 + 101260*MBhat^3*Ycut^4 + 
          4592820*m2*MBhat^3*Ycut^4 + 5416880*m2^2*MBhat^3*Ycut^4 - 
          70000*m2^3*MBhat^3*Ycut^4 + 35280*m2^4*MBhat^3*Ycut^4 + 
          13300*m2^5*MBhat^3*Ycut^4 - 4500*m2^6*MBhat^3*Ycut^4 - 
          88410*MBhat^4*Ycut^4 - 2453892*m2*MBhat^4*Ycut^4 - 
          1048320*m2^2*MBhat^4*Ycut^4 - 36960*m2^3*MBhat^4*Ycut^4 + 
          69720*m2^4*MBhat^4*Ycut^4 - 15750*m2^5*MBhat^4*Ycut^4 + 
          41580*MBhat^5*Ycut^4 + 629370*m2*MBhat^5*Ycut^4 - 
          79380*m2^2*MBhat^5*Ycut^4 + 85050*m2^3*MBhat^5*Ycut^4 - 
          18900*m2^4*MBhat^5*Ycut^4 - 8050*MBhat^6*Ycut^4 - 
          54390*m2*MBhat^6*Ycut^4 + 36925*m2^2*MBhat^6*Ycut^4 - 
          7875*m2^3*MBhat^6*Ycut^4 + 732*Ycut^5 + 111804*m2*Ycut^5 + 
          626472*m2^2*Ycut^5 + 572712*m2^3*Ycut^5 + 28812*m2^4*Ycut^5 - 
          7308*m2^5*Ycut^5 + 360*m2^6*Ycut^5 - 5301*MBhat*Ycut^5 - 
          559377*m2*MBhat*Ycut^5 - 2169594*m2^2*MBhat*Ycut^5 - 
          1072134*m2^3*MBhat*Ycut^5 + 38871*m2^4*MBhat*Ycut^5 - 
          12033*m2^5*MBhat*Ycut^5 + 1080*m2^6*MBhat*Ycut^5 + 
          12270*MBhat^2*Ycut^5 + 1127616*m2*MBhat^2*Ycut^5 + 
          2839410*m2^2*MBhat^2*Ycut^5 + 487410*m2^3*MBhat^2*Ycut^5 + 
          7140*m2^4*MBhat^2*Ycut^5 - 11844*m2^5*MBhat^2*Ycut^5 + 
          1800*m2^6*MBhat^2*Ycut^5 - 16010*MBhat^3*Ycut^5 - 
          1161860*m2*MBhat^3*Ycut^5 - 1670130*m2^2*MBhat^3*Ycut^5 + 
          28910*m2^3*MBhat^3*Ycut^5 - 17080*m2^4*MBhat^3*Ycut^5 - 
          5670*m2^5*MBhat^3*Ycut^5 + 1800*m2^6*MBhat^3*Ycut^5 + 
          16044*MBhat^4*Ycut^5 + 639072*m2*MBhat^4*Ycut^5 + 
          358092*m2^2*MBhat^4*Ycut^5 + 16212*m2^3*MBhat^4*Ycut^5 - 
          29358*m2^4*MBhat^4*Ycut^5 + 6300*m2^5*MBhat^4*Ycut^5 - 
          11025*MBhat^5*Ycut^5 - 176715*m2*MBhat^5*Ycut^5 + 
          31185*m2^2*MBhat^5*Ycut^5 - 36225*m2^3*MBhat^5*Ycut^5 + 
          7560*m2^4*MBhat^5*Ycut^5 + 3290*MBhat^6*Ycut^5 + 
          19460*m2*MBhat^6*Ycut^5 - 15435*m2^2*MBhat^6*Ycut^5 + 
          3150*m2^3*MBhat^6*Ycut^5 + 98*Ycut^6 - 3262*m2*Ycut^6 - 
          30100*m2^2*Ycut^6 - 38108*m2^3*Ycut^6 - 7826*m2^4*Ycut^6 + 
          1498*m2^5*Ycut^6 - 60*m2^6*Ycut^6 + 1113*MBhat*Ycut^6 + 
          19572*m2*MBhat*Ycut^6 + 95928*m2^2*MBhat*Ycut^6 + 
          111594*m2^3*MBhat*Ycut^6 - 10185*m2^4*MBhat*Ycut^6 + 
          2478*m2^5*MBhat*Ycut^6 - 180*m2^6*MBhat*Ycut^6 - 
          2520*MBhat^2*Ycut^6 - 34776*m2*MBhat^2*Ycut^6 - 
          160461*m2^2*MBhat^2*Ycut^6 - 64491*m2^3*MBhat^2*Ycut^6 - 
          126*m2^4*MBhat^2*Ycut^6 + 2394*m2^5*MBhat^2*Ycut^6 - 
          300*m2^6*MBhat^2*Ycut^6 - 3416*MBhat^3*Ycut^6 + 
          36974*m2*MBhat^3*Ycut^6 + 137144*m2^2*MBhat^3*Ycut^6 - 
          10486*m2^3*MBhat^3*Ycut^6 + 4564*m2^4*MBhat^3*Ycut^6 + 
          1120*m2^5*MBhat^3*Ycut^6 - 300*m2^6*MBhat^3*Ycut^6 + 
          8400*MBhat^4*Ycut^6 - 25998*m2*MBhat^4*Ycut^6 - 
          45276*m2^2*MBhat^4*Ycut^6 + 336*m2^3*MBhat^4*Ycut^6 + 
          5628*m2^4*MBhat^4*Ycut^6 - 1050*m2^5*MBhat^4*Ycut^6 - 
          2625*MBhat^5*Ycut^6 + 10080*m2*MBhat^5*Ycut^6 - 
          4095*m2^2*MBhat^5*Ycut^6 + 7140*m2^3*MBhat^5*Ycut^6 - 
          1260*m2^4*MBhat^5*Ycut^6 - 1050*MBhat^6*Ycut^6 - 
          2590*m2*MBhat^6*Ycut^6 + 3185*m2^2*MBhat^6*Ycut^6 - 
          525*m2^3*MBhat^6*Ycut^6 - 290*Ycut^7 - 1032*m2*Ycut^7 - 
          472*m2^2*Ycut^7 - 9460*m2^3*Ycut^7 + 1614*m2^4*Ycut^7 - 
          80*m2^5*Ycut^7 + 600*MBhat*Ycut^7 - 2508*m2*MBhat*Ycut^7 + 
          23805*m2^2*MBhat*Ycut^7 + 5619*m2^3*MBhat*Ycut^7 + 
          159*m2^4*MBhat*Ycut^7 - 135*m2^5*MBhat*Ycut^7 + 
          6390*MBhat^2*Ycut^7 - 10116*m2*MBhat^2*Ycut^7 - 
          30717*m2^2*MBhat^2*Ycut^7 + 2652*m2^3*MBhat^2*Ycut^7 - 
          624*m2^4*MBhat^2*Ycut^7 - 120*m2^5*MBhat^2*Ycut^7 - 
          7820*MBhat^3*Ycut^7 + 13404*m2*MBhat^3*Ycut^7 + 
          17198*m2^2*MBhat^3*Ycut^7 - 1408*m2^3*MBhat^3*Ycut^7 - 
          624*m2^4*MBhat^3*Ycut^7 - 50*m2^5*MBhat^3*Ycut^7 - 
          4830*MBhat^4*Ycut^7 + 672*m2*MBhat^4*Ycut^7 - 1764*m2^2*MBhat^4*
           Ycut^7 - 1428*m2^3*MBhat^4*Ycut^7 - 210*m2^4*MBhat^4*Ycut^7 + 
          6300*MBhat^5*Ycut^7 - 1575*m2^2*MBhat^5*Ycut^7 - 
          315*m2^3*MBhat^5*Ycut^7 - 350*MBhat^6*Ycut^7 - 
          420*m2*MBhat^6*Ycut^7 - 175*m2^2*MBhat^6*Ycut^7 - 20*Ycut^8 + 
          1468*m2*Ycut^8 - 4044*m2^2*Ycut^8 + 356*m2^3*Ycut^8 + 
          80*m2^4*Ycut^8 - 4260*MBhat*Ycut^8 + 8352*m2*MBhat*Ycut^8 + 
          1917*m2^2*MBhat*Ycut^8 - 24*m2^3*MBhat*Ycut^8 + 
          135*m2^4*MBhat*Ycut^8 - 765*MBhat^2*Ycut^8 - 801*m2*MBhat^2*
           Ycut^8 - 6948*m2^2*MBhat^2*Ycut^8 + 744*m2^3*MBhat^2*Ycut^8 + 
          120*m2^4*MBhat^2*Ycut^8 + 12430*MBhat^3*Ycut^8 - 
          8816*m2*MBhat^3*Ycut^8 + 2082*m2^2*MBhat^3*Ycut^8 + 
          674*m2^3*MBhat^3*Ycut^8 + 50*m2^4*MBhat^3*Ycut^8 - 
          4830*MBhat^4*Ycut^8 - 2268*m2*MBhat^4*Ycut^8 + 1638*m2^2*MBhat^4*
           Ycut^8 + 210*m2^3*MBhat^4*Ycut^8 - 3150*MBhat^5*Ycut^8 + 
          1890*m2*MBhat^5*Ycut^8 + 315*m2^2*MBhat^5*Ycut^8 + 
          595*MBhat^6*Ycut^8 + 175*m2*MBhat^6*Ycut^8 + 1020*Ycut^9 - 
          2552*m2*Ycut^9 + 964*m2^2*Ycut^9 - 80*m2^3*Ycut^9 + 
          4215*MBhat*Ycut^9 - 5073*m2*MBhat*Ycut^9 + 2829*m2^2*MBhat*Ycut^9 - 
          135*m2^3*MBhat*Ycut^9 - 6765*MBhat^2*Ycut^9 + 4824*m2*MBhat^2*
           Ycut^9 - 864*m2^2*MBhat^2*Ycut^9 - 120*m2^3*MBhat^2*Ycut^9 - 
          3650*MBhat^3*Ycut^9 + 4964*m2*MBhat^3*Ycut^9 - 
          724*m2^2*MBhat^3*Ycut^9 - 50*m2^3*MBhat^3*Ycut^9 + 
          5460*MBhat^4*Ycut^9 - 1848*m2*MBhat^4*Ycut^9 - 
          210*m2^2*MBhat^4*Ycut^9 - 105*MBhat^5*Ycut^9 - 
          315*m2*MBhat^5*Ycut^9 - 175*MBhat^6*Ycut^9 - 1510*Ycut^10 + 
          1986*m2*Ycut^10 - 620*m2^2*Ycut^10 - 519*MBhat*Ycut^10 + 
          456*m2*MBhat*Ycut^10 + 135*m2^2*MBhat*Ycut^10 + 
          5106*MBhat^2*Ycut^10 - 3426*m2*MBhat^2*Ycut^10 + 
          120*m2^2*MBhat^2*Ycut^10 - 2300*MBhat^3*Ycut^10 + 
          774*m2*MBhat^3*Ycut^10 + 50*m2^2*MBhat^3*Ycut^10 - 
          1092*MBhat^4*Ycut^10 + 210*m2*MBhat^4*Ycut^10 + 
          315*MBhat^5*Ycut^10 + 902*Ycut^11 - 640*m2*Ycut^11 - 
          1158*MBhat*Ycut^11 + 810*m2*MBhat*Ycut^11 - 600*MBhat^2*Ycut^11 - 
          120*m2*MBhat^2*Ycut^11 + 1066*MBhat^3*Ycut^11 - 
          50*m2*MBhat^3*Ycut^11 - 210*MBhat^4*Ycut^11 - 200*Ycut^12 + 
          450*MBhat*Ycut^12 - 300*MBhat^2*Ycut^12 + 50*MBhat^3*Ycut^12))/
        (840*(-1 + Ycut)^6) - (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 
          4*m2^4 + 84*MBhat + 540*m2*MBhat + 540*m2^2*MBhat + 60*m2^3*MBhat - 
          180*MBhat^2 - 807*m2*MBhat^2 - 452*m2^2*MBhat^2 - 7*m2^3*MBhat^2 + 
          200*MBhat^3 + 580*m2*MBhat^3 + 140*m2^2*MBhat^3 - 120*MBhat^4 - 
          198*m2*MBhat^4 - 8*m2^2*MBhat^4 + 36*MBhat^5 + 24*m2*MBhat^5 - 
          4*MBhat^6 + m2*MBhat^6)*Log[m2])/2 + 
       (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 4*m2^4 + 84*MBhat + 
          540*m2*MBhat + 540*m2^2*MBhat + 60*m2^3*MBhat - 180*MBhat^2 - 
          807*m2*MBhat^2 - 452*m2^2*MBhat^2 - 7*m2^3*MBhat^2 + 200*MBhat^3 + 
          580*m2*MBhat^3 + 140*m2^2*MBhat^3 - 120*MBhat^4 - 198*m2*MBhat^4 - 
          8*m2^2*MBhat^4 + 36*MBhat^5 + 24*m2*MBhat^5 - 4*MBhat^6 + 
          m2*MBhat^6)*Log[1 - Ycut])/2) + 
     c[SR]^2*(((-1 + m2 + Ycut)*(-732 - 66504*m2 - 286164*m2^2 - 
          191664*m2^3 - 564*m2^4 + 1368*m2^5 - 60*m2^6 + 4860*MBhat + 
          322128*m2*MBhat + 923148*m2^2*MBhat + 299448*m2^3*MBhat - 
          9252*m2^4*MBhat + 2088*m2^5*MBhat - 180*m2^6*MBhat - 
          13551*MBhat^2 - 624987*m2*MBhat^2 - 1080162*m2^2*MBhat^2 - 
          103662*m2^3*MBhat^2 - 1287*m2^4*MBhat^2 + 1989*m2^5*MBhat^2 - 
          300*m2^6*MBhat^2 + 20280*MBhat^3 + 611640*m2*MBhat^3 + 
          533940*m2^2*MBhat^3 - 9960*m2^3*MBhat^3 + 2640*m2^4*MBhat^3 + 
          960*m2^5*MBhat^3 - 300*m2^6*MBhat^3 - 17052*MBhat^4 - 
          309372*m2*MBhat^4 - 84462*m2^2*MBhat^4 - 3822*m2^3*MBhat^4 + 
          4998*m2^4*MBhat^4 - 1050*m2^5*MBhat^4 + 7560*MBhat^5 + 
          71820*m2*MBhat^5 - 8820*m2^2*MBhat^5 + 6300*m2^3*MBhat^5 - 
          1260*m2^4*MBhat^5 - 1365*MBhat^6 - 4725*m2*MBhat^6 + 
          2835*m2^2*MBhat^6 - 525*m2^3*MBhat^6 + 3660*Ycut + 351948*m2*Ycut + 
          1560144*m2^2*Ycut + 1078680*m2^3*Ycut + 7116*m2^4*Ycut - 
          8148*m2^5*Ycut + 360*m2^6*Ycut - 24300*MBhat*Ycut - 
          1711620*m2*MBhat*Ycut - 5074992*m2^2*MBhat*Ycut - 
          1713744*m2^3*MBhat*Ycut + 53604*m2^4*MBhat*Ycut - 
          12348*m2^5*MBhat*Ycut + 1080*m2^6*MBhat*Ycut + 67755*MBhat^2*Ycut + 
          3338184*m2*MBhat^2*Ycut + 6005892*m2^2*MBhat^2*Ycut + 
          612750*m2^3*MBhat^2*Ycut + 6033*m2^4*MBhat^2*Ycut - 
          11634*m2^5*MBhat^2*Ycut + 1800*m2^6*MBhat^2*Ycut - 
          101400*MBhat^3*Ycut - 3289920*m2*MBhat^3*Ycut - 
          3020580*m2^2*MBhat^3*Ycut + 56460*m2^3*MBhat^3*Ycut - 
          16500*m2^4*MBhat^3*Ycut - 5460*m2^5*MBhat^3*Ycut + 
          1800*m2^6*MBhat^3*Ycut + 85260*MBhat^4*Ycut + 1681008*m2*MBhat^4*
           Ycut + 496566*m2^2*MBhat^4*Ycut + 18984*m2^3*MBhat^4*Ycut - 
          28938*m2^4*MBhat^4*Ycut + 6300*m2^5*MBhat^4*Ycut - 
          37800*MBhat^5*Ycut - 396900*m2*MBhat^5*Ycut + 47880*m2^2*MBhat^5*
           Ycut - 36540*m2^3*MBhat^5*Ycut + 7560*m2^4*MBhat^5*Ycut + 
          6825*MBhat^6*Ycut + 27300*m2*MBhat^6*Ycut - 16485*m2^2*MBhat^6*
           Ycut + 3150*m2^3*MBhat^6*Ycut - 7320*Ycut^2 - 752832*m2*Ycut^2 - 
          3457788*m2^2*Ycut^2 - 2486208*m2^3*Ycut^2 - 28392*m2^4*Ycut^2 + 
          20160*m2^5*Ycut^2 - 900*m2^6*Ycut^2 + 48600*MBhat*Ycut^2 + 
          3678120*m2*MBhat*Ycut^2 + 11360628*m2^2*MBhat*Ycut^2 + 
          4033584*m2^3*MBhat*Ycut^2 - 127512*m2^4*MBhat*Ycut^2 + 
          30240*m2^5*MBhat*Ycut^2 - 2700*m2^6*MBhat*Ycut^2 - 
          135510*MBhat^2*Ycut^2 - 7216266*m2*MBhat^2*Ycut^2 - 
          13630509*m2^2*MBhat^2*Ycut^2 - 1502619*m2^3*MBhat^2*Ycut^2 - 
          9471*m2^4*MBhat^2*Ycut^2 + 28035*m2^5*MBhat^2*Ycut^2 - 
          4500*m2^6*MBhat^2*Ycut^2 + 202800*MBhat^3*Ycut^2 + 
          7169280*m2*MBhat^3*Ycut^2 + 7002600*m2^2*MBhat^3*Ycut^2 - 
          129240*m2^3*MBhat^3*Ycut^2 + 43260*m2^4*MBhat^3*Ycut^2 + 
          12600*m2^5*MBhat^3*Ycut^2 - 4500*m2^6*MBhat^3*Ycut^2 - 
          170520*MBhat^4*Ycut^2 - 3705912*m2*MBhat^4*Ycut^2 - 
          1207836*m2^2*MBhat^4*Ycut^2 - 34692*m2^3*MBhat^4*Ycut^2 + 
          68670*m2^4*MBhat^4*Ycut^2 - 15750*m2^5*MBhat^4*Ycut^2 + 
          75600*MBhat^5*Ycut^2 + 892080*m2*MBhat^5*Ycut^2 - 
          103320*m2^2*MBhat^5*Ycut^2 + 86940*m2^3*MBhat^5*Ycut^2 - 
          18900*m2^4*MBhat^5*Ycut^2 - 13650*MBhat^6*Ycut^2 - 
          64470*m2*MBhat^6*Ycut^2 + 39375*m2^2*MBhat^6*Ycut^2 - 
          7875*m2^3*MBhat^6*Ycut^2 + 7320*Ycut^3 + 818568*m2*Ycut^3 + 
          3929580*m2^2*Ycut^3 + 2972172*m2^3*Ycut^3 + 54180*m2^4*Ycut^3 - 
          26460*m2^5*Ycut^3 + 1200*m2^6*Ycut^3 - 48600*MBhat*Ycut^3 - 
          4021200*m2*MBhat*Ycut^3 - 13072572*m2^2*MBhat*Ycut^3 - 
          4956588*m2^3*MBhat*Ycut^3 + 157500*m2^4*MBhat*Ycut^3 - 
          39060*m2^5*MBhat*Ycut^3 + 3600*m2^6*MBhat*Ycut^3 + 
          135510*MBhat^2*Ycut^3 + 7945164*m2*MBhat^2*Ycut^3 + 
          15958935*m2^2*MBhat^2*Ycut^3 + 1950396*m2^3*MBhat^2*Ycut^3 + 
          2205*m2^4*MBhat^2*Ycut^3 - 35280*m2^5*MBhat^2*Ycut^3 + 
          6000*m2^6*MBhat^2*Ycut^3 - 203290*MBhat^3*Ycut^3 - 
          7969210*m2*MBhat^3*Ycut^3 - 8418460*m2^2*MBhat^3*Ycut^3 + 
          135100*m2^3*MBhat^3*Ycut^3 - 51590*m2^4*MBhat^3*Ycut^3 - 
          17150*m2^5*MBhat^3*Ycut^3 + 6000*m2^6*MBhat^3*Ycut^3 + 
          171780*MBhat^4*Ycut^3 + 4178328*m2*MBhat^4*Ycut^3 + 
          1531572*m2^2*MBhat^4*Ycut^3 + 41160*m2^3*MBhat^4*Ycut^3 - 
          90510*m2^4*MBhat^4*Ycut^3 + 21000*m2^5*MBhat^4*Ycut^3 - 
          76650*MBhat^5*Ycut^3 - 1031310*m2*MBhat^5*Ycut^3 + 
          117810*m2^2*MBhat^5*Ycut^3 - 112350*m2^3*MBhat^5*Ycut^3 + 
          25200*m2^4*MBhat^5*Ycut^3 + 13930*MBhat^6*Ycut^3 + 
          79660*m2*MBhat^6*Ycut^3 - 50225*m2^2*MBhat^6*Ycut^3 + 
          10500*m2^3*MBhat^6*Ycut^3 - 3660*Ycut^4 - 458952*m2*Ycut^4 - 
          2337972*m2^2*Ycut^4 - 1898400*m2^3*Ycut^4 - 55020*m2^4*Ycut^4 + 
          19320*m2^5*Ycut^4 - 900*m2^6*Ycut^4 + 24300*MBhat*Ycut^4 + 
          2270340*m2*MBhat*Ycut^4 + 7908768*m2^2*MBhat*Ycut^4 + 
          3292380*m2^3*MBhat*Ycut^4 - 103320*m2^4*MBhat*Ycut^4 + 
          27720*m2^5*MBhat*Ycut^4 - 2700*m2^6*MBhat*Ycut^4 - 
          67020*MBhat^2*Ycut^4 - 4525296*m2*MBhat^2*Ycut^4 - 
          9894696*m2^2*MBhat^2*Ycut^4 - 1383060*m2^3*MBhat^2*Ycut^4 - 
          3990*m2^4*MBhat^2*Ycut^4 + 27510*m2^5*MBhat^2*Ycut^4 - 
          4500*m2^6*MBhat^2*Ycut^4 + 101260*MBhat^3*Ycut^4 + 
          4592820*m2*MBhat^3*Ycut^4 + 5416880*m2^2*MBhat^3*Ycut^4 - 
          70000*m2^3*MBhat^3*Ycut^4 + 35280*m2^4*MBhat^3*Ycut^4 + 
          13300*m2^5*MBhat^3*Ycut^4 - 4500*m2^6*MBhat^3*Ycut^4 - 
          88410*MBhat^4*Ycut^4 - 2453892*m2*MBhat^4*Ycut^4 - 
          1048320*m2^2*MBhat^4*Ycut^4 - 36960*m2^3*MBhat^4*Ycut^4 + 
          69720*m2^4*MBhat^4*Ycut^4 - 15750*m2^5*MBhat^4*Ycut^4 + 
          41580*MBhat^5*Ycut^4 + 629370*m2*MBhat^5*Ycut^4 - 
          79380*m2^2*MBhat^5*Ycut^4 + 85050*m2^3*MBhat^5*Ycut^4 - 
          18900*m2^4*MBhat^5*Ycut^4 - 8050*MBhat^6*Ycut^4 - 
          54390*m2*MBhat^6*Ycut^4 + 36925*m2^2*MBhat^6*Ycut^4 - 
          7875*m2^3*MBhat^6*Ycut^4 + 732*Ycut^5 + 111804*m2*Ycut^5 + 
          626472*m2^2*Ycut^5 + 572712*m2^3*Ycut^5 + 28812*m2^4*Ycut^5 - 
          7308*m2^5*Ycut^5 + 360*m2^6*Ycut^5 - 5301*MBhat*Ycut^5 - 
          559377*m2*MBhat*Ycut^5 - 2169594*m2^2*MBhat*Ycut^5 - 
          1072134*m2^3*MBhat*Ycut^5 + 38871*m2^4*MBhat*Ycut^5 - 
          12033*m2^5*MBhat*Ycut^5 + 1080*m2^6*MBhat*Ycut^5 + 
          12270*MBhat^2*Ycut^5 + 1127616*m2*MBhat^2*Ycut^5 + 
          2839410*m2^2*MBhat^2*Ycut^5 + 487410*m2^3*MBhat^2*Ycut^5 + 
          7140*m2^4*MBhat^2*Ycut^5 - 11844*m2^5*MBhat^2*Ycut^5 + 
          1800*m2^6*MBhat^2*Ycut^5 - 16010*MBhat^3*Ycut^5 - 
          1161860*m2*MBhat^3*Ycut^5 - 1670130*m2^2*MBhat^3*Ycut^5 + 
          28910*m2^3*MBhat^3*Ycut^5 - 17080*m2^4*MBhat^3*Ycut^5 - 
          5670*m2^5*MBhat^3*Ycut^5 + 1800*m2^6*MBhat^3*Ycut^5 + 
          16044*MBhat^4*Ycut^5 + 639072*m2*MBhat^4*Ycut^5 + 
          358092*m2^2*MBhat^4*Ycut^5 + 16212*m2^3*MBhat^4*Ycut^5 - 
          29358*m2^4*MBhat^4*Ycut^5 + 6300*m2^5*MBhat^4*Ycut^5 - 
          11025*MBhat^5*Ycut^5 - 176715*m2*MBhat^5*Ycut^5 + 
          31185*m2^2*MBhat^5*Ycut^5 - 36225*m2^3*MBhat^5*Ycut^5 + 
          7560*m2^4*MBhat^5*Ycut^5 + 3290*MBhat^6*Ycut^5 + 
          19460*m2*MBhat^6*Ycut^5 - 15435*m2^2*MBhat^6*Ycut^5 + 
          3150*m2^3*MBhat^6*Ycut^5 + 98*Ycut^6 - 3262*m2*Ycut^6 - 
          30100*m2^2*Ycut^6 - 38108*m2^3*Ycut^6 - 7826*m2^4*Ycut^6 + 
          1498*m2^5*Ycut^6 - 60*m2^6*Ycut^6 + 1113*MBhat*Ycut^6 + 
          19572*m2*MBhat*Ycut^6 + 95928*m2^2*MBhat*Ycut^6 + 
          111594*m2^3*MBhat*Ycut^6 - 10185*m2^4*MBhat*Ycut^6 + 
          2478*m2^5*MBhat*Ycut^6 - 180*m2^6*MBhat*Ycut^6 - 
          2520*MBhat^2*Ycut^6 - 34776*m2*MBhat^2*Ycut^6 - 
          160461*m2^2*MBhat^2*Ycut^6 - 64491*m2^3*MBhat^2*Ycut^6 - 
          126*m2^4*MBhat^2*Ycut^6 + 2394*m2^5*MBhat^2*Ycut^6 - 
          300*m2^6*MBhat^2*Ycut^6 - 3416*MBhat^3*Ycut^6 + 
          36974*m2*MBhat^3*Ycut^6 + 137144*m2^2*MBhat^3*Ycut^6 - 
          10486*m2^3*MBhat^3*Ycut^6 + 4564*m2^4*MBhat^3*Ycut^6 + 
          1120*m2^5*MBhat^3*Ycut^6 - 300*m2^6*MBhat^3*Ycut^6 + 
          8400*MBhat^4*Ycut^6 - 25998*m2*MBhat^4*Ycut^6 - 
          45276*m2^2*MBhat^4*Ycut^6 + 336*m2^3*MBhat^4*Ycut^6 + 
          5628*m2^4*MBhat^4*Ycut^6 - 1050*m2^5*MBhat^4*Ycut^6 - 
          2625*MBhat^5*Ycut^6 + 10080*m2*MBhat^5*Ycut^6 - 
          4095*m2^2*MBhat^5*Ycut^6 + 7140*m2^3*MBhat^5*Ycut^6 - 
          1260*m2^4*MBhat^5*Ycut^6 - 1050*MBhat^6*Ycut^6 - 
          2590*m2*MBhat^6*Ycut^6 + 3185*m2^2*MBhat^6*Ycut^6 - 
          525*m2^3*MBhat^6*Ycut^6 - 290*Ycut^7 - 1032*m2*Ycut^7 - 
          472*m2^2*Ycut^7 - 9460*m2^3*Ycut^7 + 1614*m2^4*Ycut^7 - 
          80*m2^5*Ycut^7 + 600*MBhat*Ycut^7 - 2508*m2*MBhat*Ycut^7 + 
          23805*m2^2*MBhat*Ycut^7 + 5619*m2^3*MBhat*Ycut^7 + 
          159*m2^4*MBhat*Ycut^7 - 135*m2^5*MBhat*Ycut^7 + 
          6390*MBhat^2*Ycut^7 - 10116*m2*MBhat^2*Ycut^7 - 
          30717*m2^2*MBhat^2*Ycut^7 + 2652*m2^3*MBhat^2*Ycut^7 - 
          624*m2^4*MBhat^2*Ycut^7 - 120*m2^5*MBhat^2*Ycut^7 - 
          7820*MBhat^3*Ycut^7 + 13404*m2*MBhat^3*Ycut^7 + 
          17198*m2^2*MBhat^3*Ycut^7 - 1408*m2^3*MBhat^3*Ycut^7 - 
          624*m2^4*MBhat^3*Ycut^7 - 50*m2^5*MBhat^3*Ycut^7 - 
          4830*MBhat^4*Ycut^7 + 672*m2*MBhat^4*Ycut^7 - 1764*m2^2*MBhat^4*
           Ycut^7 - 1428*m2^3*MBhat^4*Ycut^7 - 210*m2^4*MBhat^4*Ycut^7 + 
          6300*MBhat^5*Ycut^7 - 1575*m2^2*MBhat^5*Ycut^7 - 
          315*m2^3*MBhat^5*Ycut^7 - 350*MBhat^6*Ycut^7 - 
          420*m2*MBhat^6*Ycut^7 - 175*m2^2*MBhat^6*Ycut^7 - 20*Ycut^8 + 
          1468*m2*Ycut^8 - 4044*m2^2*Ycut^8 + 356*m2^3*Ycut^8 + 
          80*m2^4*Ycut^8 - 4260*MBhat*Ycut^8 + 8352*m2*MBhat*Ycut^8 + 
          1917*m2^2*MBhat*Ycut^8 - 24*m2^3*MBhat*Ycut^8 + 
          135*m2^4*MBhat*Ycut^8 - 765*MBhat^2*Ycut^8 - 801*m2*MBhat^2*
           Ycut^8 - 6948*m2^2*MBhat^2*Ycut^8 + 744*m2^3*MBhat^2*Ycut^8 + 
          120*m2^4*MBhat^2*Ycut^8 + 12430*MBhat^3*Ycut^8 - 
          8816*m2*MBhat^3*Ycut^8 + 2082*m2^2*MBhat^3*Ycut^8 + 
          674*m2^3*MBhat^3*Ycut^8 + 50*m2^4*MBhat^3*Ycut^8 - 
          4830*MBhat^4*Ycut^8 - 2268*m2*MBhat^4*Ycut^8 + 1638*m2^2*MBhat^4*
           Ycut^8 + 210*m2^3*MBhat^4*Ycut^8 - 3150*MBhat^5*Ycut^8 + 
          1890*m2*MBhat^5*Ycut^8 + 315*m2^2*MBhat^5*Ycut^8 + 
          595*MBhat^6*Ycut^8 + 175*m2*MBhat^6*Ycut^8 + 1020*Ycut^9 - 
          2552*m2*Ycut^9 + 964*m2^2*Ycut^9 - 80*m2^3*Ycut^9 + 
          4215*MBhat*Ycut^9 - 5073*m2*MBhat*Ycut^9 + 2829*m2^2*MBhat*Ycut^9 - 
          135*m2^3*MBhat*Ycut^9 - 6765*MBhat^2*Ycut^9 + 4824*m2*MBhat^2*
           Ycut^9 - 864*m2^2*MBhat^2*Ycut^9 - 120*m2^3*MBhat^2*Ycut^9 - 
          3650*MBhat^3*Ycut^9 + 4964*m2*MBhat^3*Ycut^9 - 
          724*m2^2*MBhat^3*Ycut^9 - 50*m2^3*MBhat^3*Ycut^9 + 
          5460*MBhat^4*Ycut^9 - 1848*m2*MBhat^4*Ycut^9 - 
          210*m2^2*MBhat^4*Ycut^9 - 105*MBhat^5*Ycut^9 - 
          315*m2*MBhat^5*Ycut^9 - 175*MBhat^6*Ycut^9 - 1510*Ycut^10 + 
          1986*m2*Ycut^10 - 620*m2^2*Ycut^10 - 519*MBhat*Ycut^10 + 
          456*m2*MBhat*Ycut^10 + 135*m2^2*MBhat*Ycut^10 + 
          5106*MBhat^2*Ycut^10 - 3426*m2*MBhat^2*Ycut^10 + 
          120*m2^2*MBhat^2*Ycut^10 - 2300*MBhat^3*Ycut^10 + 
          774*m2*MBhat^3*Ycut^10 + 50*m2^2*MBhat^3*Ycut^10 - 
          1092*MBhat^4*Ycut^10 + 210*m2*MBhat^4*Ycut^10 + 
          315*MBhat^5*Ycut^10 + 902*Ycut^11 - 640*m2*Ycut^11 - 
          1158*MBhat*Ycut^11 + 810*m2*MBhat*Ycut^11 - 600*MBhat^2*Ycut^11 - 
          120*m2*MBhat^2*Ycut^11 + 1066*MBhat^3*Ycut^11 - 
          50*m2*MBhat^3*Ycut^11 - 210*MBhat^4*Ycut^11 - 200*Ycut^12 + 
          450*MBhat*Ycut^12 - 300*MBhat^2*Ycut^12 + 50*MBhat^3*Ycut^12))/
        (840*(-1 + Ycut)^6) - (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 
          4*m2^4 + 84*MBhat + 540*m2*MBhat + 540*m2^2*MBhat + 60*m2^3*MBhat - 
          180*MBhat^2 - 807*m2*MBhat^2 - 452*m2^2*MBhat^2 - 7*m2^3*MBhat^2 + 
          200*MBhat^3 + 580*m2*MBhat^3 + 140*m2^2*MBhat^3 - 120*MBhat^4 - 
          198*m2*MBhat^4 - 8*m2^2*MBhat^4 + 36*MBhat^5 + 24*m2*MBhat^5 - 
          4*MBhat^6 + m2*MBhat^6)*Log[m2])/2 + 
       (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 4*m2^4 + 84*MBhat + 
          540*m2*MBhat + 540*m2^2*MBhat + 60*m2^3*MBhat - 180*MBhat^2 - 
          807*m2*MBhat^2 - 452*m2^2*MBhat^2 - 7*m2^3*MBhat^2 + 200*MBhat^3 + 
          580*m2*MBhat^3 + 140*m2^2*MBhat^3 - 120*MBhat^4 - 198*m2*MBhat^4 - 
          8*m2^2*MBhat^4 + 36*MBhat^5 + 24*m2*MBhat^5 - 4*MBhat^6 + 
          m2*MBhat^6)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(345 + 12378*m2 - 16497*m2^2 - 49572*m2^3 + 
          6603*m2^4 + 1458*m2^5 - 75*m2^6 - 2360*MBhat - 52060*m2*MBhat + 
          104180*m2^2*MBhat + 93680*m2^3*MBhat - 11320*m2^4*MBhat + 
          2540*m2^5*MBhat - 260*m2^6*MBhat + 6786*MBhat^2 + 
          80846*m2*MBhat^2 - 188374*m2^2*MBhat^2 - 30244*m2^3*MBhat^2 - 
          4414*m2^4*MBhat^2 + 3230*m2^5*MBhat^2 - 550*m2^6*MBhat^2 - 
          10392*MBhat^3 - 53400*m2*MBhat^3 + 120060*m2^2*MBhat^3 - 
          6360*m2^3*MBhat^3 - 2160*m2^4*MBhat^3 + 3552*m2^5*MBhat^3 - 
          900*m2^6*MBhat^3 + 8771*MBhat^4 + 9926*m2*MBhat^4 - 
          5194*m2^2*MBhat^4 - 16954*m2^3*MBhat^4 + 12131*m2^4*MBhat^4 - 
          2800*m2^5*MBhat^4 - 3780*MBhat^5 + 3360*m2*MBhat^5 - 
          18480*m2^2*MBhat^5 + 11760*m2^3*MBhat^5 - 2940*m2^4*MBhat^5 + 
          630*MBhat^6 - 1050*m2*MBhat^6 + 3990*m2^2*MBhat^6 - 
          1050*m2^3*MBhat^6 - 1035*Ycut - 41829*m2*Ycut + 50874*m2^2*Ycut + 
          177702*m2^3*Ycut - 21495*m2^4*Ycut - 5757*m2^5*Ycut + 
          300*m2^6*Ycut + 7080*MBhat*Ycut + 179020*m2*MBhat*Ycut - 
          341760*m2^2*MBhat*Ycut - 348880*m2^3*MBhat*Ycut + 
          43000*m2^4*MBhat*Ycut - 9900*m2^5*MBhat*Ycut + 
          1040*m2^6*MBhat*Ycut - 20358*MBhat^2*Ycut - 286152*m2*MBhat^2*
           Ycut + 642674*m2^2*MBhat^2*Ycut + 123550*m2^3*MBhat^2*Ycut + 
          14976*m2^4*MBhat^2*Ycut - 12370*m2^5*MBhat^2*Ycut + 
          2200*m2^6*MBhat^2*Ycut + 31176*MBhat^3*Ycut + 200208*m2*MBhat^3*
           Ycut - 429012*m2^2*MBhat^3*Ycut + 24948*m2^3*MBhat^3*Ycut + 
          5988*m2^4*MBhat^3*Ycut - 13308*m2^5*MBhat^3*Ycut + 
          3600*m2^6*MBhat^3*Ycut - 26313*MBhat^4*Ycut - 46207*m2*MBhat^4*
           Ycut + 27979*m2^2*MBhat^4*Ycut + 58485*m2^3*MBhat^4*Ycut - 
          45724*m2^4*MBhat^4*Ycut + 11200*m2^5*MBhat^4*Ycut + 
          11340*MBhat^5*Ycut - 8820*m2*MBhat^5*Ycut + 65100*m2^2*MBhat^5*
           Ycut - 44100*m2^3*MBhat^5*Ycut + 11760*m2^4*MBhat^5*Ycut - 
          1890*MBhat^6*Ycut + 3780*m2*MBhat^6*Ycut - 14910*m2^2*MBhat^6*
           Ycut + 4200*m2^3*MBhat^6*Ycut + 1035*Ycut^2 + 49044*m2*Ycut^2 - 
          51282*m2^2*Ycut^2 - 226380*m2^3*Ycut^2 + 23025*m2^4*Ycut^2 + 
          8448*m2^5*Ycut^2 - 450*m2^6*Ycut^2 - 7080*MBhat*Ycut^2 - 
          214460*m2*MBhat*Ycut^2 + 381220*m2^2*MBhat*Ycut^2 + 
          469140*m2^3*MBhat*Ycut^2 - 59060*m2^4*MBhat*Ycut^2 + 
          14200*m2^5*MBhat*Ycut^2 - 1560*m2^6*MBhat*Ycut^2 + 
          20358*MBhat^2*Ycut^2 + 354966*m2*MBhat^2*Ycut^2 - 
          758800*m2^2*MBhat^2*Ycut^2 - 189210*m2^3*MBhat^2*Ycut^2 - 
          16314*m2^4*MBhat^2*Ycut^2 + 17180*m2^5*MBhat^2*Ycut^2 - 
          3300*m2^6*MBhat^2*Ycut^2 - 31176*MBhat^3*Ycut^2 - 
          265416*m2*MBhat^3*Ycut^2 + 540372*m2^2*MBhat^3*Ycut^2 - 
          34440*m2^3*MBhat^3*Ycut^2 - 3252*m2^4*MBhat^3*Ycut^2 + 
          17712*m2^5*MBhat^3*Ycut^2 - 5400*m2^6*MBhat^3*Ycut^2 + 
          26313*MBhat^4*Ycut^2 + 75236*m2*MBhat^4*Ycut^2 - 
          53655*m2^2*MBhat^4*Ycut^2 - 67200*m2^3*MBhat^4*Ycut^2 + 
          61586*m2^4*MBhat^4*Ycut^2 - 16800*m2^5*MBhat^4*Ycut^2 - 
          11340*MBhat^5*Ycut^2 + 5040*m2*MBhat^5*Ycut^2 - 
          78540*m2^2*MBhat^5*Ycut^2 + 58800*m2^3*MBhat^5*Ycut^2 - 
          17640*m2^4*MBhat^5*Ycut^2 + 1890*MBhat^6*Ycut^2 - 
          4410*m2*MBhat^6*Ycut^2 + 19740*m2^2*MBhat^6*Ycut^2 - 
          6300*m2^3*MBhat^6*Ycut^2 - 345*Ycut^3 - 21273*m2*Ycut^3 + 
          15645*m2^2*Ycut^3 + 112665*m2^3*Ycut^3 - 7110*m2^4*Ycut^3 - 
          5382*m2^5*Ycut^3 + 300*m2^6*Ycut^3 + 2360*MBhat*Ycut^3 + 
          95900*m2*MBhat*Ycut^3 - 147840*m2^2*MBhat*Ycut^3 - 
          255500*m2^3*MBhat*Ycut^3 + 32640*m2^4*MBhat*Ycut^3 - 
          8600*m2^5*MBhat*Ycut^3 + 1040*m2^6*MBhat*Ycut^3 - 
          6786*MBhat^2*Ycut^3 - 166460*m2*MBhat^2*Ycut^3 + 
          326340*m2^2*MBhat^2*Ycut^3 + 127050*m2^3*MBhat^2*Ycut^3 + 
          3776*m2^4*MBhat^2*Ycut^3 - 9620*m2^5*MBhat^2*Ycut^3 + 
          2200*m2^6*MBhat^2*Ycut^3 + 14592*MBhat^3*Ycut^3 + 
          127008*m2*MBhat^3*Ycut^3 - 258300*m2^2*MBhat^3*Ycut^3 + 
          24780*m2^3*MBhat^3*Ycut^3 - 7872*m2^4*MBhat^3*Ycut^3 - 
          8808*m2^5*MBhat^3*Ycut^3 + 3600*m2^6*MBhat^3*Ycut^3 - 
          18851*MBhat^4*Ycut^3 - 37275*m2*MBhat^4*Ycut^3 + 
          51450*m2^2*MBhat^4*Ycut^3 + 12950*m2^3*MBhat^4*Ycut^3 - 
          31724*m2^4*MBhat^4*Ycut^3 + 11200*m2^5*MBhat^4*Ycut^3 + 
          11340*MBhat^5*Ycut^3 + 2100*m2*MBhat^5*Ycut^3 + 
          25200*m2^2*MBhat^5*Ycut^3 - 29400*m2^3*MBhat^5*Ycut^3 + 
          11760*m2^4*MBhat^5*Ycut^3 - 2310*MBhat^6*Ycut^3 - 
          9660*m2^2*MBhat^6*Ycut^3 + 4200*m2^3*MBhat^6*Ycut^3 + 
          1260*m2*Ycut^4 + 1155*m2^2*Ycut^4 - 11130*m2^3*Ycut^4 - 
          1440*m2^4*Ycut^4 + 1158*m2^5*Ycut^4 - 75*m2^6*Ycut^4 - 
          6300*m2*MBhat*Ycut^4 + 2100*m2^2*MBhat*Ycut^4 + 
          33600*m2^3*MBhat*Ycut^4 - 3760*m2^4*MBhat*Ycut^4 + 
          1500*m2^5*MBhat*Ycut^4 - 260*m2^6*MBhat*Ycut^4 - 
          6825*MBhat^2*Ycut^4 + 29400*m2*MBhat^2*Ycut^4 - 
          23730*m2^2*MBhat^2*Ycut^4 - 32550*m2^3*MBhat^2*Ycut^4 + 
          6681*m2^4*MBhat^2*Ycut^4 + 1030*m2^5*MBhat^2*Ycut^4 - 
          550*m2^6*MBhat^2*Ycut^4 + 630*MBhat^3*Ycut^4 - 
          8820*m2*MBhat^3*Ycut^4 + 18060*m2^2*MBhat^3*Ycut^4 - 
          11340*m2^3*MBhat^3*Ycut^4 + 12498*m2^4*MBhat^3*Ycut^4 - 
          48*m2^5*MBhat^3*Ycut^4 - 900*m2^6*MBhat^3*Ycut^4 + 
          25305*MBhat^4*Ycut^4 - 22470*m2*MBhat^4*Ycut^4 - 
          28980*m2^2*MBhat^4*Ycut^4 + 29190*m2^3*MBhat^4*Ycut^4 + 
          931*m2^4*MBhat^4*Ycut^4 - 2800*m2^5*MBhat^4*Ycut^4 - 
          25200*MBhat^5*Ycut^4 + 2520*m2*MBhat^5*Ycut^4 + 
          20580*m2^2*MBhat^5*Ycut^4 - 2940*m2^4*MBhat^5*Ycut^4 + 
          6090*MBhat^6*Ycut^4 + 4410*m2*MBhat^6*Ycut^4 - 
          210*m2^2*MBhat^6*Ycut^4 - 1050*m2^3*MBhat^6*Ycut^4 + 
          252*m2*Ycut^5 + 147*m2^2*Ycut^5 - 2163*m2^3*Ycut^5 + 
          177*m2^4*Ycut^5 + 75*m2^5*Ycut^5 + 4200*MBhat*Ycut^5 - 
          12180*m2*MBhat*Ycut^5 + 8400*m2^2*MBhat*Ycut^5 + 
          6720*m2^3*MBhat*Ycut^5 - 2920*m2^4*MBhat*Ycut^5 + 
          260*m2^5*MBhat*Ycut^5 + 15099*MBhat^2*Ycut^5 - 
          33201*m2*MBhat^2*Ycut^5 + 17829*m2^2*MBhat^2*Ycut^5 + 
          399*m2^3*MBhat^2*Ycut^5 - 5100*m2^4*MBhat^2*Ycut^5 + 
          550*m2^5*MBhat^2*Ycut^5 - 30198*MBhat^3*Ycut^5 + 
          35070*m2*MBhat^3*Ycut^5 + 210*m2^2*MBhat^3*Ycut^5 + 
          2310*m2^3*MBhat^3*Ycut^5 - 6612*m2^4*MBhat^3*Ycut^5 + 
          900*m2^5*MBhat^3*Ycut^5 - 9891*MBhat^4*Ycut^5 + 
          22659*m2*MBhat^4*Ycut^5 + 8799*m2^2*MBhat^4*Ycut^5 - 
          19971*m2^3*MBhat^4*Ycut^5 + 2800*m2^4*MBhat^4*Ycut^5 + 
          28980*MBhat^5*Ycut^5 - 8820*m2*MBhat^5*Ycut^5 - 
          17220*m2^2*MBhat^5*Ycut^5 + 2940*m2^3*MBhat^5*Ycut^5 - 
          8190*MBhat^6*Ycut^5 - 3780*m2*MBhat^6*Ycut^5 + 1050*m2^2*MBhat^6*
           Ycut^5 - 945*Ycut^6 + 2604*m2*Ycut^6 - 1869*m2^2*Ycut^6 - 
          672*m2^3*Ycut^6 + 450*m2^4*Ycut^6 - 13090*MBhat*Ycut^6 + 
          28000*m2*MBhat*Ycut^6 - 16100*m2^2*MBhat*Ycut^6 + 
          1680*m2^3*MBhat*Ycut^6 + 790*m2^4*MBhat*Ycut^6 + 
          168*MBhat^2*Ycut^6 + 9142*m2*MBhat^2*Ycut^6 - 12929*m2^2*MBhat^2*
           Ycut^6 + 1330*m2^3*MBhat^2*Ycut^6 + 1025*m2^4*MBhat^2*Ycut^6 + 
          41034*MBhat^3*Ycut^6 - 50148*m2*MBhat^3*Ycut^6 + 
          10542*m2^2*MBhat^3*Ycut^6 + 252*m2^3*MBhat^3*Ycut^6 + 
          1200*m2^4*MBhat^3*Ycut^6 - 20657*MBhat^4*Ycut^6 + 
          3472*m2*MBhat^4*Ycut^6 + 301*m2^2*MBhat^4*Ycut^6 + 
          3500*m2^3*MBhat^4*Ycut^6 - 11340*MBhat^5*Ycut^6 + 
          5880*m2*MBhat^5*Ycut^6 + 3360*m2^2*MBhat^5*Ycut^6 + 
          4830*MBhat^6*Ycut^6 + 1050*m2*MBhat^6*Ycut^6 + 3435*Ycut^7 - 
          7149*m2*Ycut^7 + 4002*m2^2*Ycut^7 - 450*m2^3*Ycut^7 + 
          12720*MBhat*Ycut^7 - 21160*m2*MBhat*Ycut^7 + 9360*m2^2*MBhat*
           Ycut^7 - 440*m2^3*MBhat*Ycut^7 - 21948*MBhat^2*Ycut^7 + 
          22194*m2*MBhat^2*Ycut^7 - 3335*m2^2*MBhat^2*Ycut^7 - 
          325*m2^3*MBhat^2*Ycut^7 - 11364*MBhat^3*Ycut^7 + 
          13416*m2*MBhat^3*Ycut^7 - 2082*m2^2*MBhat^3*Ycut^7 - 
          150*m2^3*MBhat^3*Ycut^7 + 19467*MBhat^4*Ycut^7 - 
          6041*m2*MBhat^4*Ycut^7 - 700*m2^2*MBhat^4*Ycut^7 - 
          1260*MBhat^5*Ycut^7 - 1260*m2*MBhat^5*Ycut^7 - 
          1050*MBhat^6*Ycut^7 - 4635*Ycut^8 + 6738*m2*Ycut^8 - 
          2175*m2^2*Ycut^8 - 1620*MBhat*Ycut^8 + 740*m2*MBhat*Ycut^8 + 
          440*m2^2*MBhat*Ycut^8 + 16221*MBhat^2*Ycut^8 - 
          10410*m2*MBhat^2*Ycut^8 + 325*m2^2*MBhat^2*Ycut^8 - 
          7782*MBhat^3*Ycut^8 + 2232*m2*MBhat^3*Ycut^8 + 
          150*m2^2*MBhat^3*Ycut^8 - 3444*MBhat^4*Ycut^8 + 
          700*m2*MBhat^4*Ycut^8 + 1260*MBhat^5*Ycut^8 + 2745*Ycut^9 - 
          2025*m2*Ycut^9 - 3560*MBhat*Ycut^9 + 2500*m2*MBhat*Ycut^9 - 
          1815*MBhat^2*Ycut^9 - 325*m2*MBhat^2*Ycut^9 + 3330*MBhat^3*Ycut^9 - 
          150*m2*MBhat^3*Ycut^9 - 700*MBhat^4*Ycut^9 - 600*Ycut^10 + 
          1350*MBhat*Ycut^10 - 900*MBhat^2*Ycut^10 + 150*MBhat^3*Ycut^10))/
        (420*(-1 + Ycut)^4) - m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 
         60*MBhat + 340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 
         96*m2*MBhat^2 - 342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 
         132*m2*MBhat^3 + 108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - 
         m2^2*MBhat^4 - 12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[m2] + 
       m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 60*MBhat + 
         340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 96*m2*MBhat^2 - 
         342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 132*m2*MBhat^3 + 
         108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - m2^2*MBhat^4 - 
         12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[1 - Ycut]) + 
     c[VL]^2*(((-1 + m2 + Ycut)*(345 + 12378*m2 - 16497*m2^2 - 49572*m2^3 + 
          6603*m2^4 + 1458*m2^5 - 75*m2^6 - 2360*MBhat - 52060*m2*MBhat + 
          104180*m2^2*MBhat + 93680*m2^3*MBhat - 11320*m2^4*MBhat + 
          2540*m2^5*MBhat - 260*m2^6*MBhat + 6786*MBhat^2 + 
          80846*m2*MBhat^2 - 188374*m2^2*MBhat^2 - 30244*m2^3*MBhat^2 - 
          4414*m2^4*MBhat^2 + 3230*m2^5*MBhat^2 - 550*m2^6*MBhat^2 - 
          10392*MBhat^3 - 53400*m2*MBhat^3 + 120060*m2^2*MBhat^3 - 
          6360*m2^3*MBhat^3 - 2160*m2^4*MBhat^3 + 3552*m2^5*MBhat^3 - 
          900*m2^6*MBhat^3 + 8771*MBhat^4 + 9926*m2*MBhat^4 - 
          5194*m2^2*MBhat^4 - 16954*m2^3*MBhat^4 + 12131*m2^4*MBhat^4 - 
          2800*m2^5*MBhat^4 - 3780*MBhat^5 + 3360*m2*MBhat^5 - 
          18480*m2^2*MBhat^5 + 11760*m2^3*MBhat^5 - 2940*m2^4*MBhat^5 + 
          630*MBhat^6 - 1050*m2*MBhat^6 + 3990*m2^2*MBhat^6 - 
          1050*m2^3*MBhat^6 - 1725*Ycut - 66585*m2*Ycut + 83868*m2^2*Ycut + 
          276846*m2^3*Ycut - 34701*m2^4*Ycut - 8673*m2^5*Ycut + 
          450*m2^6*Ycut + 11800*MBhat*Ycut + 283140*m2*MBhat*Ycut - 
          550120*m2^2*MBhat*Ycut - 536240*m2^3*MBhat*Ycut + 
          65640*m2^4*MBhat*Ycut - 14980*m2^5*MBhat*Ycut + 
          1560*m2^6*MBhat*Ycut - 33930*MBhat^2*Ycut - 447844*m2*MBhat^2*
           Ycut + 1019422*m2^2*MBhat^2*Ycut + 184038*m2^3*MBhat^2*Ycut + 
          23804*m2^4*MBhat^2*Ycut - 18830*m2^5*MBhat^2*Ycut + 
          3300*m2^6*MBhat^2*Ycut + 51960*MBhat^3*Ycut + 307008*m2*MBhat^3*
           Ycut - 669132*m2^2*MBhat^3*Ycut + 37668*m2^3*MBhat^3*Ycut + 
          10308*m2^4*MBhat^3*Ycut - 20412*m2^5*MBhat^3*Ycut + 
          5400*m2^6*MBhat^3*Ycut - 43855*MBhat^4*Ycut - 66059*m2*MBhat^4*
           Ycut + 38367*m2^2*MBhat^4*Ycut + 92393*m2^3*MBhat^4*Ycut - 
          69986*m2^4*MBhat^4*Ycut + 16800*m2^5*MBhat^4*Ycut + 
          18900*MBhat^5*Ycut - 15540*m2*MBhat^5*Ycut + 102060*m2^2*MBhat^5*
           Ycut - 67620*m2^3*MBhat^5*Ycut + 17640*m2^4*MBhat^5*Ycut - 
          3150*MBhat^6*Ycut + 5880*m2*MBhat^6*Ycut - 22890*m2^2*MBhat^6*
           Ycut + 6300*m2^3*MBhat^6*Ycut + 3450*Ycut^2 + 145080*m2*Ycut^2 - 
          169527*m2^2*Ycut^2 - 631356*m2^3*Ycut^2 + 72618*m2^4*Ycut^2 + 
          21420*m2^5*Ycut^2 - 1125*m2^6*Ycut^2 - 23600*MBhat*Ycut^2 - 
          624560*m2*MBhat*Ycut^2 + 1168920*m2^2*MBhat*Ycut^2 + 
          1260580*m2^3*MBhat*Ycut^2 - 156380*m2^4*MBhat*Ycut^2 + 
          36540*m2^5*MBhat*Ycut^2 - 3900*m2^6*MBhat*Ycut^2 + 
          67860*MBhat^2*Ycut^2 + 1008116*m2*MBhat^2*Ycut^2 - 
          2232522*m2^2*MBhat^2*Ycut^2 - 466554*m2^3*MBhat^2*Ycut^2 - 
          50680*m2^4*MBhat^2*Ycut^2 + 45150*m2^5*MBhat^2*Ycut^2 - 
          8250*m2^6*MBhat^2*Ycut^2 - 103920*MBhat^3*Ycut^2 - 
          719232*m2*MBhat^3*Ycut^2 + 1518456*m2^2*MBhat^3*Ycut^2 - 
          90696*m2^3*MBhat^3*Ycut^2 - 17388*m2^4*MBhat^3*Ycut^2 + 
          47880*m2^5*MBhat^3*Ycut^2 - 13500*m2^6*MBhat^3*Ycut^2 + 
          87710*MBhat^4*Ycut^2 + 177576*m2*MBhat^4*Ycut^2 - 
          114807*m2^2*MBhat^4*Ycut^2 - 201124*m2^3*MBhat^4*Ycut^2 + 
          165165*m2^4*MBhat^4*Ycut^2 - 42000*m2^5*MBhat^4*Ycut^2 - 
          37800*MBhat^5*Ycut^2 + 26040*m2*MBhat^5*Ycut^2 - 
          227220*m2^2*MBhat^5*Ycut^2 + 158760*m2^3*MBhat^5*Ycut^2 - 
          44100*m2^4*MBhat^5*Ycut^2 + 6300*MBhat^6*Ycut^2 - 
          13020*m2*MBhat^6*Ycut^2 + 53550*m2^2*MBhat^6*Ycut^2 - 
          15750*m2^3*MBhat^6*Ycut^2 - 3450*Ycut^3 - 161190*m2*Ycut^3 + 
          169083*m2^2*Ycut^3 + 743127*m2^3*Ycut^3 - 74655*m2^4*Ycut^3 - 
          28035*m2^5*Ycut^3 + 1500*m2^6*Ycut^3 + 23600*MBhat*Ycut^3 + 
          703840*m2*MBhat*Ycut^3 - 1252040*m2^2*MBhat*Ycut^3 - 
          1542660*m2^3*MBhat*Ycut^3 + 193760*m2^4*MBhat*Ycut^3 - 
          46900*m2^5*MBhat*Ycut^3 + 5200*m2^6*MBhat*Ycut^3 - 
          67860*MBhat^2*Ycut^3 - 1162544*m2*MBhat^2*Ycut^3 + 
          2486614*m2^2*MBhat^2*Ycut^3 + 629020*m2^3*MBhat^2*Ycut^3 + 
          51380*m2^4*MBhat^2*Ycut^3 - 56350*m2^5*MBhat^2*Ycut^3 + 
          11000*m2^6*MBhat^2*Ycut^3 + 102940*MBhat^3*Ycut^3 + 
          873868*m2*MBhat^3*Ycut^3 - 1779536*m2^2*MBhat^3*Ycut^3 + 
          109928*m2^3*MBhat^3*Ycut^3 + 19040*m2^4*MBhat^3*Ycut^3 - 
          62440*m2^5*MBhat^3*Ycut^3 + 18000*m2^6*MBhat^3*Ycut^3 - 
          85190*MBhat^4*Ycut^3 - 259154*m2*MBhat^4*Ycut^3 + 
          186739*m2^2*MBhat^4*Ycut^3 + 231035*m2^3*MBhat^4*Ycut^3 - 
          213220*m2^4*MBhat^4*Ycut^3 + 56000*m2^5*MBhat^4*Ycut^3 + 
          35700*MBhat^5*Ycut^3 - 7980*m2*MBhat^5*Ycut^3 + 
          258720*m2^2*MBhat^5*Ycut^3 - 201600*m2^3*MBhat^5*Ycut^3 + 
          58800*m2^4*MBhat^5*Ycut^3 - 5740*MBhat^6*Ycut^3 + 
          13160*m2*MBhat^6*Ycut^3 - 66850*m2^2*MBhat^6*Ycut^3 + 
          21000*m2^3*MBhat^6*Ycut^3 + 1725*Ycut^4 + 92850*m2*Ycut^4 - 
          81417*m2^2*Ycut^4 - 462840*m2^3*Ycut^4 + 35805*m2^4*Ycut^4 + 
          20370*m2^5*Ycut^4 - 1125*m2^6*Ycut^4 - 11800*MBhat*Ycut^4 - 
          412560*m2*MBhat*Ycut^4 + 679000*m2^2*MBhat*Ycut^4 + 
          1013740*m2^3*MBhat*Ycut^4 - 128100*m2^4*MBhat*Ycut^4 + 
          32900*m2^5*MBhat*Ycut^4 - 3900*m2^6*MBhat*Ycut^4 + 
          34875*MBhat^2*Ycut^4 + 693556*m2*MBhat^2*Ycut^4 - 
          1417990*m2^2*MBhat^2*Ycut^4 - 462840*m2^3*MBhat^2*Ycut^4 - 
          38815*m2^4*MBhat^2*Ycut^4 + 44800*m2^5*MBhat^2*Ycut^4 - 
          8250*m2^6*MBhat^2*Ycut^4 - 49510*MBhat^3*Ycut^4 - 
          569412*m2*MBhat^3*Ycut^4 + 1127392*m2^2*MBhat^3*Ycut^4 - 
          107660*m2^3*MBhat^3*Ycut^4 + 6090*m2^4*MBhat^3*Ycut^4 + 
          45080*m2^5*MBhat^3*Ycut^4 - 13500*m2^6*MBhat^3*Ycut^4 + 
          33670*MBhat^4*Ycut^4 + 237566*m2*MBhat^4*Ycut^4 - 
          207585*m2^2*MBhat^4*Ycut^4 - 127960*m2^3*MBhat^4*Ycut^4 + 
          157465*m2^4*MBhat^4*Ycut^4 - 42000*m2^5*MBhat^4*Ycut^4 - 
          9660*MBhat^5*Ycut^4 - 39060*m2*MBhat^5*Ycut^4 - 
          147000*m2^2*MBhat^5*Ycut^4 + 149100*m2^3*MBhat^5*Ycut^4 - 
          44100*m2^4*MBhat^5*Ycut^4 + 700*MBhat^6*Ycut^4 - 
          2940*m2*MBhat^6*Ycut^4 + 48650*m2^2*MBhat^6*Ycut^4 - 
          15750*m2^3*MBhat^6*Ycut^4 - 345*Ycut^5 - 23541*m2*Ycut^5 + 
          13482*m2^2*Ycut^5 + 132762*m2^3*Ycut^5 - 4053*m2^4*Ycut^5 - 
          7623*m2^5*Ycut^5 + 450*m2^6*Ycut^5 + 1898*MBhat*Ycut^5 + 
          110558*m2*MBhat*Ycut^5 - 153972*m2^2*MBhat*Ycut^5 - 
          323792*m2^3*MBhat*Ycut^5 + 50218*m2^4*MBhat*Ycut^5 - 
          15750*m2^5*MBhat*Ycut^5 + 1560*m2^6*MBhat*Ycut^5 - 
          10083*MBhat^2*Ycut^5 - 166817*m2*MBhat^2*Ycut^5 + 
          317793*m2^2*MBhat^2*Ycut^5 + 184653*m2^3*MBhat^2*Ycut^5 + 
          16268*m2^4*MBhat^2*Ycut^5 - 19950*m2^5*MBhat^2*Ycut^5 + 
          3300*m2^6*MBhat^2*Ycut^5 + 11162*MBhat^3*Ycut^5 + 
          168266*m2*MBhat^3*Ycut^5 - 368802*m2^2*MBhat^3*Ycut^5 + 
          112378*m2^3*MBhat^3*Ycut^5 - 35252*m2^4*MBhat^3*Ycut^5 - 
          16632*m2^5*MBhat^3*Ycut^5 + 5400*m2^6*MBhat^3*Ycut^5 + 
          5278*MBhat^4*Ycut^5 - 152796*m2*MBhat^4*Ycut^5 + 
          190239*m2^2*MBhat^4*Ycut^5 - 8701*m2^3*MBhat^4*Ycut^5 - 
          61026*m2^4*MBhat^4*Ycut^5 + 16800*m2^5*MBhat^4*Ycut^5 - 
          11130*MBhat^5*Ycut^5 + 70770*m2*MBhat^5*Ycut^5 + 
          15750*m2^2*MBhat^5*Ycut^5 - 61110*m2^3*MBhat^5*Ycut^5 + 
          17640*m2^4*MBhat^5*Ycut^5 + 3220*MBhat^6*Ycut^5 - 
          6440*m2*MBhat^6*Ycut^5 - 20790*m2^2*MBhat^6*Ycut^5 + 
          6300*m2^3*MBhat^6*Ycut^5 + 91*Ycut^6 + 196*m2*Ycut^6 + 
          1288*m2^2*Ycut^6 - 5740*m2^3*Ycut^6 - 4228*m2^4*Ycut^6 + 
          1988*m2^5*Ycut^6 - 75*m2^6*Ycut^6 + 1904*MBhat*Ycut^6 - 
          15848*m2*MBhat*Ycut^6 + 11620*m2^2*MBhat*Ycut^6 + 
          32088*m2^3*MBhat*Ycut^6 - 14224*m2^4*MBhat*Ycut^6 + 
          3920*m2^5*MBhat*Ycut^6 - 260*m2^6*MBhat*Ycut^6 + 
          2919*MBhat^2*Ycut^6 - 13118*m2*MBhat^2*Ycut^6 + 
          41335*m2^2*MBhat^2*Ycut^6 - 59822*m2^3*MBhat^2*Ycut^6 + 
          5936*m2^4*MBhat^2*Ycut^6 + 4340*m2^5*MBhat^2*Ycut^6 - 
          550*m2^6*MBhat^2*Ycut^6 - 6664*MBhat^3*Ycut^6 + 
          9436*m2*MBhat^3*Ycut^6 + 50134*m2^2*MBhat^3*Ycut^6 - 
          71708*m2^3*MBhat^3*Ycut^6 + 27230*m2^4*MBhat^3*Ycut^6 + 
          2072*m2^5*MBhat^3*Ycut^6 - 900*m2^6*MBhat^3*Ycut^6 - 
          5600*MBhat^4*Ycut^6 + 69034*m2*MBhat^4*Ycut^6 - 
          130697*m2^2*MBhat^4*Ycut^6 + 49952*m2^3*MBhat^4*Ycut^6 + 
          9191*m2^4*MBhat^4*Ycut^6 - 2800*m2^5*MBhat^4*Ycut^6 + 
          9450*MBhat^5*Ycut^6 - 56280*m2*MBhat^5*Ycut^6 + 
          30030*m2^2*MBhat^5*Ycut^6 + 10920*m2^3*MBhat^5*Ycut^6 - 
          2940*m2^4*MBhat^5*Ycut^6 - 2100*MBhat^6*Ycut^6 + 
          6580*m2*MBhat^6*Ycut^6 + 4690*m2^2*MBhat^6*Ycut^6 - 
          1050*m2^3*MBhat^6*Ycut^6 - 415*Ycut^7 + 2511*m2*Ycut^7 - 
          1241*m2^2*Ycut^7 - 4181*m2^3*Ycut^7 + 2721*m2^4*Ycut^7 - 
          205*m2^5*Ycut^7 - 2680*MBhat*Ycut^7 + 13392*m2*MBhat*Ycut^7 - 
          14678*m2^2*MBhat*Ycut^7 + 6490*m2^3*MBhat*Ycut^7 + 
          246*m2^4*MBhat*Ycut^7 - 370*m2^5*MBhat*Ycut^7 + 
          1845*MBhat^2*Ycut^7 + 1537*m2*MBhat^2*Ycut^7 - 29368*m2^2*MBhat^2*
           Ycut^7 + 25050*m2^3*MBhat^2*Ycut^7 - 4084*m2^4*MBhat^2*Ycut^7 - 
          290*m2^5*MBhat^2*Ycut^7 + 4610*MBhat^3*Ycut^7 - 
          21234*m2*MBhat^3*Ycut^7 + 7480*m2^2*MBhat^3*Ycut^7 + 
          18652*m2^3*MBhat^3*Ycut^7 - 8928*m2^4*MBhat^3*Ycut^7 + 
          200*m2^5*MBhat^3*Ycut^7 - 2660*MBhat^4*Ycut^7 - 
          15526*m2*MBhat^4*Ycut^7 + 49077*m2^2*MBhat^4*Ycut^7 - 
          21511*m2^3*MBhat^4*Ycut^7 + 280*m2^4*MBhat^4*Ycut^7 + 
          21840*m2*MBhat^5*Ycut^7 - 16170*m2^2*MBhat^5*Ycut^7 - 
          210*m2^3*MBhat^5*Ycut^7 - 700*MBhat^6*Ycut^7 - 
          2520*m2*MBhat^6*Ycut^7 - 350*m2^2*MBhat^6*Ycut^7 + 710*Ycut^8 - 
          3184*m2*Ycut^8 + 1560*m2^2*Ycut^8 + 844*m2^3*Ycut^8 - 
          110*m2^4*Ycut^8 + 1010*MBhat*Ycut^8 - 5548*m2*MBhat*Ycut^8 + 
          8334*m2^2*MBhat*Ycut^8 - 4076*m2^3*MBhat*Ycut^8 + 
          160*m2^4*MBhat*Ycut^8 - 3765*MBhat^2*Ycut^8 + 9322*m2*MBhat^2*
           Ycut^8 + 1374*m2^2*MBhat^2*Ycut^8 - 3396*m2^3*MBhat^2*Ycut^8 + 
          605*m2^4*MBhat^2*Ycut^8 + 1940*MBhat^3*Ycut^8 + 
          3596*m2*MBhat^3*Ycut^8 - 6564*m2^2*MBhat^3*Ycut^8 - 
          2192*m2^3*MBhat^3*Ycut^8 + 1060*m2^4*MBhat^3*Ycut^8 + 
          1015*MBhat^4*Ycut^8 - 1596*m2*MBhat^4*Ycut^8 - 6069*m2^2*MBhat^4*
           Ycut^8 + 2870*m2^3*MBhat^4*Ycut^8 - 2100*MBhat^5*Ycut^8 - 
          2940*m2*MBhat^5*Ycut^8 + 2310*m2^2*MBhat^5*Ycut^8 + 
          1190*MBhat^6*Ycut^8 + 350*m2*MBhat^6*Ycut^8 - 510*Ycut^9 + 
          1766*m2*Ycut^9 - 664*m2^2*Ycut^9 + 110*m2^3*Ycut^9 + 
          850*MBhat*Ycut^9 - 1198*m2*MBhat*Ycut^9 - 1054*m2^2*MBhat*Ycut^9 + 
          190*m2^3*MBhat*Ycut^9 + 705*MBhat^2*Ycut^9 - 2923*m2*MBhat^2*
           Ycut^9 + 1811*m2^2*MBhat^2*Ycut^9 + 95*m2^3*MBhat^2*Ycut^9 - 
          2410*MBhat^3*Ycut^9 + 1606*m2*MBhat^3*Ycut^9 + 
          502*m2^2*MBhat^3*Ycut^9 - 10*m2^3*MBhat^3*Ycut^9 + 
          1505*MBhat^4*Ycut^9 + 959*m2*MBhat^4*Ycut^9 - 70*m2^2*MBhat^4*
           Ycut^9 + 210*MBhat^5*Ycut^9 - 210*m2*MBhat^5*Ycut^9 - 
          350*MBhat^6*Ycut^9 + 55*Ycut^10 - 216*m2*Ycut^10 + 
          65*m2^2*Ycut^10 - 668*MBhat*Ycut^10 + 864*m2*MBhat*Ycut^10 - 
          190*m2^2*MBhat*Ycut^10 + 1017*MBhat^2*Ycut^10 - 
          226*m2*MBhat^2*Ycut^10 - 95*m2^2*MBhat^2*Ycut^10 - 
          40*MBhat^3*Ycut^10 - 492*m2*MBhat^3*Ycut^10 + 10*m2^2*MBhat^3*
           Ycut^10 - 574*MBhat^4*Ycut^10 + 70*m2*MBhat^4*Ycut^10 + 
          210*MBhat^5*Ycut^10 + 109*Ycut^11 - 65*m2*Ycut^11 - 
          44*MBhat*Ycut^11 - 20*m2*MBhat*Ycut^11 - 309*MBhat^2*Ycut^11 + 
          95*m2*MBhat^2*Ycut^11 + 314*MBhat^3*Ycut^11 - 
          10*m2*MBhat^3*Ycut^11 - 70*MBhat^4*Ycut^11 - 40*Ycut^12 + 
          90*MBhat*Ycut^12 - 60*MBhat^2*Ycut^12 + 10*MBhat^3*Ycut^12))/
        (420*(-1 + Ycut)^6) - m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 
         60*MBhat + 340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 
         96*m2*MBhat^2 - 342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 
         132*m2*MBhat^3 + 108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - 
         m2^2*MBhat^4 - 12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[m2] + 
       m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 60*MBhat + 
         340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 96*m2*MBhat^2 - 
         342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 132*m2*MBhat^3 + 
         108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - m2^2*MBhat^4 - 
         12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[1 - Ycut]) + 
     c[T]^2*((2*(-1 + m2 + Ycut)*(582 + 27588*m2 + 25698*m2^2 - 34152*m2^3 + 
          8898*m2^4 + 1716*m2^5 - 90*m2^6 - 4260*MBhat - 133648*m2*MBhat - 
          23188*m2^2*MBhat + 75512*m2^3*MBhat - 14788*m2^4*MBhat + 
          3272*m2^5*MBhat - 340*m2^6*MBhat + 13459*MBhat^2 + 
          256247*m2*MBhat^2 - 63898*m2^2*MBhat^2 - 11818*m2^3*MBhat^2 - 
          5413*m2^4*MBhat^2 + 4583*m2^5*MBhat^2 - 800*m2^6*MBhat^2 - 
          23816*MBhat^3 - 240872*m2*MBhat^3 + 77908*m2^2*MBhat^3 - 
          20792*m2^3*MBhat^3 - 1472*m2^4*MBhat^3 + 5584*m2^5*MBhat^3 - 
          1500*m2^6*MBhat^3 + 24010*MBhat^4 + 113680*m2*MBhat^4 - 
          3710*m2^2*MBhat^4 - 17150*m2^3*MBhat^4 + 17920*m2^4*MBhat^4 - 
          4550*m2^5*MBhat^4 - 12600*MBhat^5 - 25620*m2*MBhat^5 - 
          18900*m2^2*MBhat^5 + 16380*m2^3*MBhat^5 - 4620*m2^4*MBhat^5 + 
          2625*MBhat^6 + 2625*m2*MBhat^6 + 5145*m2^2*MBhat^6 - 
          1575*m2^3*MBhat^6 - 2910*Ycut - 147438*m2*Ycut - 148200*m2^2*Ycut + 
          189348*m2^3*Ycut - 47454*m2^4*Ycut - 10206*m2^5*Ycut + 
          540*m2^6*Ycut + 21300*MBhat*Ycut + 719420*m2*MBhat*Ycut + 
          159472*m2^2*MBhat*Ycut - 432816*m2^3*MBhat*Ycut + 
          85796*m2^4*MBhat*Ycut - 19292*m2^5*MBhat*Ycut + 
          2040*m2^6*MBhat*Ycut - 67295*MBhat^2*Ycut - 1393776*m2*MBhat^2*
           Ycut + 313256*m2^2*MBhat^2*Ycut + 83038*m2^3*MBhat^2*Ycut + 
          28695*m2^4*MBhat^2*Ycut - 26698*m2^5*MBhat^2*Ycut + 
          4800*m2^6*MBhat^2*Ycut + 119080*MBhat^3*Ycut + 1331744*m2*MBhat^3*
           Ycut - 427428*m2^2*MBhat^3*Ycut + 122140*m2^3*MBhat^3*Ycut + 
          4748*m2^4*MBhat^3*Ycut - 32004*m2^5*MBhat^3*Ycut + 
          9000*m2^6*MBhat^3*Ycut - 120050*MBhat^4*Ycut - 645190*m2*MBhat^4*
           Ycut + 35280*m2^2*MBhat^4*Ycut + 89530*m2^3*MBhat^4*Ycut - 
          102970*m2^4*MBhat^4*Ycut + 27300*m2^5*MBhat^4*Ycut + 
          63000*MBhat^5*Ycut + 150780*m2*MBhat^5*Ycut + 101640*m2^2*MBhat^5*
           Ycut - 93660*m2^3*MBhat^5*Ycut + 27720*m2^4*MBhat^5*Ycut - 
          13125*MBhat^6*Ycut - 15540*m2*MBhat^6*Ycut - 29295*m2^2*MBhat^6*
           Ycut + 9450*m2^3*MBhat^6*Ycut + 5820*Ycut^2 + 318912*m2*Ycut^2 + 
          350262*m2^2*Ycut^2 - 427440*m2^3*Ycut^2 + 101556*m2^4*Ycut^2 + 
          25200*m2^5*Ycut^2 - 1350*m2^6*Ycut^2 - 42600*MBhat*Ycut^2 - 
          1568920*m2*MBhat*Ycut^2 - 445548*m2^2*MBhat*Ycut^2 + 
          1017936*m2^3*MBhat*Ycut^2 - 204568*m2^4*MBhat*Ycut^2 + 
          47040*m2^5*MBhat*Ycut^2 - 5100*m2^6*MBhat*Ycut^2 + 
          134590*MBhat^2*Ycut^2 + 3075634*m2*MBhat^2*Ycut^2 - 
          588615*m2^2*MBhat^2*Ycut^2 - 241817*m2^3*MBhat^2*Ycut^2 - 
          59297*m2^4*MBhat^2*Ycut^2 + 63945*m2^5*MBhat^2*Ycut^2 - 
          12000*m2^6*MBhat^2*Ycut^2 - 238160*MBhat^3*Ycut^2 - 
          2993856*m2*MBhat^3*Ycut^2 + 944616*m2^2*MBhat^3*Ycut^2 - 
          293624*m2^3*MBhat^3*Ycut^2 + 924*m2^4*MBhat^3*Ycut^2 + 
          74760*m2^5*MBhat^3*Ycut^2 - 22500*m2^6*MBhat^3*Ycut^2 + 
          240100*MBhat^4*Ycut^2 + 1494360*m2*MBhat^4*Ycut^2 - 
          120330*m2^2*MBhat^4*Ycut^2 - 181580*m2^3*MBhat^4*Ycut^2 + 
          241500*m2^4*MBhat^4*Ycut^2 - 68250*m2^5*MBhat^4*Ycut^2 - 
          126000*MBhat^5*Ycut^2 - 364560*m2*MBhat^5*Ycut^2 - 
          217560*m2^2*MBhat^5*Ycut^2 + 217980*m2^3*MBhat^5*Ycut^2 - 
          69300*m2^4*MBhat^5*Ycut^2 + 26250*MBhat^6*Ycut^2 + 
          38430*m2*MBhat^6*Ycut^2 + 67725*m2^2*MBhat^6*Ycut^2 - 
          23625*m2^3*MBhat^6*Ycut^2 - 5820*Ycut^3 - 351348*m2*Ycut^3 - 
          429486*m2^2*Ycut^3 + 495474*m2^3*Ycut^3 - 108570*m2^4*Ycut^3 - 
          32970*m2^5*Ycut^3 + 1800*m2^6*Ycut^3 + 42600*MBhat*Ycut^3 + 
          1745200*m2*MBhat*Ycut^3 + 644452*m2^2*MBhat*Ycut^3 - 
          1244012*m2^3*MBhat*Ycut^3 + 253820*m2^4*MBhat*Ycut^3 - 
          60340*m2^5*MBhat*Ycut^3 + 6800*m2^6*MBhat*Ycut^3 - 
          134590*MBhat^2*Ycut^3 - 3468716*m2*MBhat^2*Ycut^3 + 
          496309*m2^2*MBhat^2*Ycut^3 + 373212*m2^3*MBhat^2*Ycut^3 + 
          56315*m2^4*MBhat^2*Ycut^3 - 79660*m2^5*MBhat^2*Ycut^3 + 
          16000*m2^6*MBhat^2*Ycut^3 + 245230*MBhat^3*Ycut^3 + 
          3436294*m2*MBhat^3*Ycut^3 - 1046540*m2^2*MBhat^3*Ycut^3 + 
          370076*m2^3*MBhat^3*Ycut^3 - 22750*m2^4*MBhat^3*Ycut^3 - 
          91630*m2^5*MBhat^3*Ycut^3 + 30000*m2^6*MBhat^3*Ycut^3 - 
          257320*MBhat^4*Ycut^3 - 1764700*m2*MBhat^4*Ycut^3 + 
          206570*m2^2*MBhat^4*Ycut^3 + 165550*m2^3*MBhat^4*Ycut^3 - 
          296450*m2^4*MBhat^4*Ycut^3 + 91000*m2^5*MBhat^4*Ycut^3 + 
          139230*MBhat^5*Ycut^3 + 455490*m2*MBhat^5*Ycut^3 + 
          222810*m2^2*MBhat^5*Ycut^3 - 263550*m2^3*MBhat^5*Ycut^3 + 
          92400*m2^4*MBhat^5*Ycut^3 - 29330*MBhat^6*Ycut^3 - 
          52220*m2*MBhat^6*Ycut^3 - 80675*m2^2*MBhat^6*Ycut^3 + 
          31500*m2^3*MBhat^6*Ycut^3 + 2910*Ycut^4 + 200292*m2*Ycut^4 + 
          281106*m2^2*Ycut^4 - 300720*m2^3*Ycut^4 + 56910*m2^4*Ycut^4 + 
          23940*m2^5*Ycut^4 - 1350*m2^6*Ycut^4 - 21300*MBhat*Ycut^4 - 
          1006940*m2*MBhat*Ycut^4 - 501088*m2^2*MBhat*Ycut^4 + 
          812700*m2^3*MBhat*Ycut^4 - 168280*m2^4*MBhat*Ycut^4 + 
          42280*m2^5*MBhat*Ycut^4 - 5100*m2^6*MBhat*Ycut^4 + 
          59840*MBhat^2*Ycut^4 + 2052594*m2*MBhat^2*Ycut^4 - 
          142352*m2^2*MBhat^2*Ycut^4 - 318080*m2^3*MBhat^2*Ycut^4 - 
          22890*m2^4*MBhat^2*Ycut^4 + 56420*m2^5*MBhat^2*Ycut^4 - 
          12000*m2^6*MBhat^2*Ycut^4 - 140220*MBhat^3*Ycut^4 - 
          2028476*m2*MBhat^3*Ycut^4 + 569744*m2^2*MBhat^3*Ycut^4 - 
          278880*m2^3*MBhat^3*Ycut^4 + 58520*m2^4*MBhat^3*Ycut^4 + 
          58660*m2^5*MBhat^3*Ycut^4 - 22500*m2^6*MBhat^3*Ycut^4 + 
          201320*MBhat^4*Ycut^4 + 1028230*m2*MBhat^4*Ycut^4 - 
          209160*m2^2*MBhat^4*Ycut^4 - 12950*m2^3*MBhat^4*Ycut^4 + 
          193550*m2^4*MBhat^4*Ycut^4 - 68250*m2^5*MBhat^4*Ycut^4 - 
          132300*MBhat^5*Ycut^4 - 290430*m2*MBhat^5*Ycut^4 - 
          77700*m2^2*MBhat^5*Ycut^4 + 169050*m2^3*MBhat^5*Ycut^4 - 
          69300*m2^4*MBhat^5*Ycut^4 + 29750*MBhat^6*Ycut^4 + 
          44730*m2*MBhat^6*Ycut^4 + 50575*m2^2*MBhat^6*Ycut^4 - 
          23625*m2^3*MBhat^6*Ycut^4 - 582*Ycut^5 - 50022*m2*Ycut^5 - 
          86436*m2^2*Ycut^5 + 81564*m2^3*Ycut^5 - 10206*m2^4*Ycut^5 - 
          8946*m2^5*Ycut^5 + 540*m2^6*Ycut^5 + 8103*MBhat*Ycut^5 + 
          246799*m2*MBhat*Ycut^5 + 195006*m2^2*MBhat*Ycut^5 - 
          252294*m2^3*MBhat*Ycut^5 + 55811*m2^4*MBhat*Ycut^5 - 
          16737*m2^5*MBhat*Ycut^5 + 2040*m2^6*MBhat*Ycut^5 + 
          18650*MBhat^2*Ycut^5 - 602182*m2*MBhat^2*Ycut^5 - 
          11844*m2^2*MBhat^2*Ycut^5 + 150136*m2^3*MBhat^2*Ycut^5 - 
          10934*m2^4*MBhat^2*Ycut^5 - 19908*m2^5*MBhat^2*Ycut^5 + 
          4800*m2^6*MBhat^2*Ycut^5 + 21114*MBhat^3*Ycut^5 + 
          518980*m2*MBhat^3*Ycut^5 - 100506*m2^2*MBhat^3*Ycut^5 + 
          143094*m2^3*MBhat^3*Ycut^5 - 74116*m2^4*MBhat^3*Ycut^5 - 
          15414*m2^5*MBhat^3*Ycut^5 + 9000*m2^6*MBhat^3*Ycut^5 - 
          167230*MBhat^4*Ycut^5 - 139440*m2*MBhat^4*Ycut^5 + 
          141540*m2^2*MBhat^4*Ycut^5 - 120050*m2^3*MBhat^4*Ycut^5 - 
          54390*m2^4*MBhat^4*Ycut^5 + 27300*m2^5*MBhat^4*Ycut^5 + 
          159495*MBhat^5*Ycut^5 + 54285*m2*MBhat^5*Ycut^5 - 
          61215*m2^2*MBhat^5*Ycut^5 - 46305*m2^3*MBhat^5*Ycut^5 + 
          27720*m2^4*MBhat^5*Ycut^5 - 39550*MBhat^6*Ycut^5 - 
          28420*m2*MBhat^6*Ycut^5 - 13545*m2^2*MBhat^6*Ycut^5 + 
          9450*m2^3*MBhat^6*Ycut^5 - 784*Ycut^6 + 3626*m2*Ycut^6 + 
          4340*m2^2*Ycut^6 - 2156*m2^3*Ycut^6 - 2282*m2^4*Ycut^6 + 
          1666*m2^5*Ycut^6 - 90*m2^6*Ycut^6 - 19467*MBhat*Ycut^6 + 
          33376*m2*MBhat*Ycut^6 - 45668*m2^2*MBhat*Ycut^6 + 
          20398*m2^3*MBhat*Ycut^6 - 4921*m2^4*MBhat*Ycut^6 + 
          2702*m2^5*MBhat*Ycut^6 - 340*m2^6*MBhat*Ycut^6 - 
          45304*MBhat^2*Ycut^6 + 129010*m2*MBhat^2*Ycut^6 - 
          33089*m2^2*MBhat^2*Ycut^6 - 46053*m2^3*MBhat^2*Ycut^6 + 
          21728*m2^4*MBhat^2*Ycut^6 + 1988*m2^5*MBhat^2*Ycut^6 - 
          800*m2^6*MBhat^2*Ycut^6 + 80220*MBhat^3*Ycut^6 - 
          75782*m2*MBhat^3*Ycut^6 - 25508*m2^2*MBhat^3*Ycut^6 - 
          49434*m2^3*MBhat^3*Ycut^6 + 48020*m2^4*MBhat^3*Ycut^6 - 
          1456*m2^5*MBhat^3*Ycut^6 - 1500*m2^6*MBhat^3*Ycut^6 + 
          100310*MBhat^4*Ycut^6 - 151060*m2*MBhat^4*Ycut^6 - 
          66850*m2^2*MBhat^4*Ycut^6 + 113680*m2^3*MBhat^4*Ycut^6 - 
          2450*m2^4*MBhat^4*Ycut^6 - 4550*m2^5*MBhat^4*Ycut^6 - 
          158025*MBhat^5*Ycut^6 + 45360*m2*MBhat^5*Ycut^6 + 
          79905*m2^2*MBhat^5*Ycut^6 - 2940*m2^3*MBhat^5*Ycut^6 - 
          4620*m2^4*MBhat^5*Ycut^6 + 43050*MBhat^6*Ycut^6 + 
          15470*m2*MBhat^6*Ycut^6 - 805*m2^2*MBhat^6*Ycut^6 - 
          1575*m2^3*MBhat^6*Ycut^6 + 4360*Ycut^7 - 9234*m2*Ycut^7 + 
          7286*m2^2*Ycut^7 - 2710*m2^3*Ycut^7 + 888*m2^4*Ycut^7 - 
          50*m2^5*Ycut^7 + 38700*MBhat*Ycut^7 - 77024*m2*MBhat*Ycut^7 + 
          37013*m2^2*MBhat*Ycut^7 + 3231*m2^3*MBhat*Ycut^7 - 
          3685*m2^4*MBhat*Ycut^7 + 25*m2^5*MBhat*Ycut^7 + 
          6470*MBhat^2*Ycut^7 - 49110*m2*MBhat^2*Ycut^7 + 
          39181*m2^2*MBhat^2*Ycut^7 + 12028*m2^3*MBhat^2*Ycut^7 - 
          9714*m2^4*MBhat^2*Ycut^7 + 380*m2^5*MBhat^2*Ycut^7 - 
          115680*MBhat^3*Ycut^7 + 106528*m2*MBhat^3*Ycut^7 + 
          6470*m2^2*MBhat^3*Ycut^7 + 7716*m2^3*MBhat^3*Ycut^7 - 
          16084*m2^4*MBhat^3*Ycut^7 + 1150*m2^5*MBhat^3*Ycut^7 + 
          7700*MBhat^4*Ycut^7 + 68740*m2*MBhat^4*Ycut^7 + 
          17010*m2^2*MBhat^4*Ycut^7 - 43190*m2^3*MBhat^4*Ycut^7 + 
          3290*m2^4*MBhat^4*Ycut^7 + 86100*MBhat^5*Ycut^7 - 
          33600*m2*MBhat^5*Ycut^7 - 34335*m2^2*MBhat^5*Ycut^7 + 
          3045*m2^3*MBhat^5*Ycut^7 - 27650*MBhat^6*Ycut^7 - 
          6300*m2*MBhat^6*Ycut^7 + 875*m2^2*MBhat^6*Ycut^7 - 10040*Ycut^8 + 
          18316*m2*Ycut^8 - 9468*m2^2*Ycut^8 + 1052*m2^3*Ycut^8 + 
          260*m2^4*Ycut^8 - 36000*MBhat*Ycut^8 + 60856*m2*MBhat*Ycut^8 - 
          26451*m2^2*MBhat*Ycut^8 - 540*m2^3*MBhat*Ycut^8 + 
          815*m2^4*MBhat*Ycut^8 + 45245*MBhat^2*Ycut^8 - 
          23605*m2*MBhat^2*Ycut^8 - 8364*m2^2*MBhat^2*Ycut^8 - 
          536*m2^3*MBhat^2*Ycut^8 + 1510*m2^4*MBhat^2*Ycut^8 + 
          58650*MBhat^3*Ycut^8 - 65192*m2*MBhat^3*Ycut^8 + 
          2178*m2^2*MBhat^3*Ycut^8 - 186*m2^3*MBhat^3*Ycut^8 + 
          2210*m2^4*MBhat^3*Ycut^8 - 49630*MBhat^4*Ycut^8 - 
          1050*m2*MBhat^4*Ycut^8 + 210*m2^2*MBhat^4*Ycut^8 + 
          6160*m2^3*MBhat^4*Ycut^8 - 17430*MBhat^5*Ycut^8 + 
          9450*m2*MBhat^5*Ycut^8 + 5355*m2^2*MBhat^5*Ycut^8 + 
          9205*MBhat^6*Ycut^8 + 1225*m2*MBhat^6*Ycut^8 + 12240*Ycut^9 - 
          17324*m2*Ycut^9 + 6388*m2^2*Ycut^9 - 260*m2^3*Ycut^9 + 
          11835*MBhat*Ycut^9 - 18169*m2*MBhat*Ycut^9 + 5885*m2^2*MBhat*
           Ycut^9 - 115*m2^3*MBhat*Ycut^9 - 45935*MBhat^2*Ycut^9 + 
          31190*m2*MBhat^2*Ycut^9 - 694*m2^2*MBhat^2*Ycut^9 - 
          110*m2^3*MBhat^2*Ycut^9 + 2330*MBhat^3*Ycut^9 + 
          9588*m2*MBhat^3*Ycut^9 - 1044*m2^2*MBhat^3*Ycut^9 - 
          110*m2^3*MBhat^3*Ycut^9 + 23380*MBhat^4*Ycut^9 - 
          4130*m2*MBhat^4*Ycut^9 - 560*m2^2*MBhat^4*Ycut^9 - 
          2625*MBhat^5*Ycut^9 - 1155*m2*MBhat^5*Ycut^9 - 
          1225*MBhat^6*Ycut^9 - 8320*Ycut^10 + 8082*m2*Ycut^10 - 
          1490*m2^2*Ycut^10 + 4797*MBhat*Ycut^10 - 2620*m2*MBhat*Ycut^10 + 
          115*m2^2*MBhat*Ycut^10 + 15646*MBhat^2*Ycut^10 - 
          7176*m2*MBhat^2*Ycut^10 + 110*m2^2*MBhat^2*Ycut^10 - 
          11248*MBhat^3*Ycut^10 + 1154*m2*MBhat^3*Ycut^10 + 
          110*m2^2*MBhat^3*Ycut^10 - 2030*MBhat^4*Ycut^10 + 
          560*m2*MBhat^4*Ycut^10 + 1155*MBhat^5*Ycut^10 + 2984*Ycut^11 - 
          1450*m2*Ycut^11 - 4698*MBhat*Ycut^11 + 1670*m2*MBhat*Ycut^11 - 
          116*MBhat^2*Ycut^11 - 110*m2*MBhat^2*Ycut^11 + 
          2390*MBhat^3*Ycut^11 - 110*m2*MBhat^3*Ycut^11 - 
          560*MBhat^4*Ycut^11 - 440*Ycut^12 + 990*MBhat*Ycut^12 - 
          660*MBhat^2*Ycut^12 + 110*MBhat^3*Ycut^12))/(105*(-1 + Ycut)^6) - 
       8*m2*(24 + 90*m2 - 30*m2^2 - 30*m2^3 + 18*m2^4 - 132*MBhat - 
         300*m2*MBhat + 180*m2^2*MBhat + 20*m2^3*MBhat + 300*MBhat^2 + 
         357*m2*MBhat^2 - 224*m2^2*MBhat^2 + 25*m2^3*MBhat^2 - 360*MBhat^3 - 
         180*m2*MBhat^3 + 52*m2^2*MBhat^3 + 240*MBhat^4 + 48*m2*MBhat^4 + 
         22*m2^2*MBhat^4 - 84*MBhat^5 - 24*m2*MBhat^5 + 12*MBhat^6 + 
         9*m2*MBhat^6)*Log[m2] + 8*m2*(24 + 90*m2 - 30*m2^2 - 30*m2^3 + 
         18*m2^4 - 132*MBhat - 300*m2*MBhat + 180*m2^2*MBhat + 
         20*m2^3*MBhat + 300*MBhat^2 + 357*m2*MBhat^2 - 224*m2^2*MBhat^2 + 
         25*m2^3*MBhat^2 - 360*MBhat^3 - 180*m2*MBhat^3 + 52*m2^2*MBhat^3 + 
         240*MBhat^4 + 48*m2*MBhat^4 + 22*m2^2*MBhat^4 - 84*MBhat^5 - 
         24*m2*MBhat^5 + 12*MBhat^6 + 9*m2*MBhat^6)*Log[1 - Ycut]) + 
     c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-460 - 430*m2 + 8270*m2^2 + 
          5470*m2^3 - 230*m2^4 - 20*m2^5 + 2524*MBhat - 2420*m2*MBhat - 
          29120*m2^2*MBhat - 5120*m2^3*MBhat + 580*m2^4*MBhat - 
          44*m2^5*MBhat - 5618*MBhat^2 + 12637*m2*MBhat^2 + 
          32032*m2^2*MBhat^2 - 2348*m2^3*MBhat^2 + 322*m2^4*MBhat^2 - 
          65*m2^5*MBhat^2 + 6384*MBhat^3 - 18792*m2*MBhat^3 - 
          10212*m2^2*MBhat^3 + 1148*m2^3*MBhat^3 + 188*m2^4*MBhat^3 - 
          76*m2^5*MBhat^3 - 3840*MBhat^4 + 11805*m2*MBhat^4 - 
          2355*m2^2*MBhat^4 + 945*m2^3*MBhat^4 - 255*m2^4*MBhat^4 + 
          1140*MBhat^5 - 2940*m2*MBhat^5 + 1380*m2^2*MBhat^5 - 
          300*m2^3*MBhat^5 - 130*MBhat^6 + 140*m2*MBhat^6 - 
          130*m2^2*MBhat^6 + 1960*Ycut + 2280*m2*Ycut - 36550*m2^2*Ycut - 
          25480*m2^3*Ycut + 990*m2^4*Ycut + 100*m2^5*Ycut - 
          10816*MBhat*Ycut + 8604*m2*MBhat*Ycut + 130984*m2^2*MBhat*Ycut + 
          25064*m2^3*MBhat*Ycut - 2856*m2^4*MBhat*Ycut + 
          220*m2^5*MBhat*Ycut + 24272*MBhat^2*Ycut - 52026*m2*MBhat^2*Ycut - 
          147749*m2^2*MBhat^2*Ycut + 10403*m2^3*MBhat^2*Ycut - 
          1545*m2^4*MBhat^2*Ycut + 325*m2^5*MBhat^2*Ycut - 
          27936*MBhat^3*Ycut + 80592*m2*MBhat^3*Ycut + 49800*m2^2*MBhat^3*
           Ycut - 5852*m2^3*MBhat^3*Ycut - 864*m2^4*MBhat^3*Ycut + 
          380*m2^5*MBhat^3*Ycut + 17160*MBhat^4*Ycut - 52320*m2*MBhat^4*
           Ycut + 10005*m2^2*MBhat^4*Ycut - 4470*m2^3*MBhat^4*Ycut + 
          1275*m2^4*MBhat^4*Ycut - 5280*MBhat^5*Ycut + 13620*m2*MBhat^5*
           Ycut - 6600*m2^2*MBhat^5*Ycut + 1500*m2^3*MBhat^5*Ycut + 
          640*MBhat^6*Ycut - 750*m2*MBhat^6*Ycut + 650*m2^2*MBhat^6*Ycut - 
          3180*Ycut^2 - 4650*m2*Ycut^2 + 62000*m2^2*Ycut^2 + 
          46320*m2^3*Ycut^2 - 1590*m2^4*Ycut^2 - 200*m2^5*Ycut^2 + 
          17664*MBhat*Ycut^2 - 10212*m2*MBhat*Ycut^2 - 227228*m2^2*MBhat*
           Ycut^2 - 48564*m2^3*MBhat*Ycut^2 + 5580*m2^4*MBhat*Ycut^2 - 
          440*m2^5*MBhat*Ycut^2 - 40008*MBhat^2*Ycut^2 + 
          79986*m2*MBhat^2*Ycut^2 + 264697*m2^2*MBhat^2*Ycut^2 - 
          17400*m2^3*MBhat^2*Ycut^2 + 2895*m2^4*MBhat^2*Ycut^2 - 
          650*m2^5*MBhat^2*Ycut^2 + 46704*MBhat^3*Ycut^2 - 
          130944*m2*MBhat^3*Ycut^2 - 95784*m2^2*MBhat^3*Ycut^2 + 
          11964*m2^3*MBhat^3*Ycut^2 + 1500*m2^4*MBhat^3*Ycut^2 - 
          760*m2^5*MBhat^3*Ycut^2 - 29340*MBhat^4*Ycut^2 + 
          88560*m2*MBhat^4*Ycut^2 - 15495*m2^2*MBhat^4*Ycut^2 + 
          8175*m2^3*MBhat^4*Ycut^2 - 2550*m2^4*MBhat^4*Ycut^2 + 
          9360*MBhat^5*Ycut^2 - 24300*m2*MBhat^5*Ycut^2 + 
          12300*m2^2*MBhat^5*Ycut^2 - 3000*m2^3*MBhat^5*Ycut^2 - 
          1200*MBhat^6*Ycut^2 + 1560*m2*MBhat^6*Ycut^2 - 1300*m2^2*MBhat^6*
           Ycut^2 + 2360*Ycut^3 + 4460*m2*Ycut^3 - 48740*m2^2*Ycut^3 - 
          40220*m2^3*Ycut^3 + 1090*m2^4*Ycut^3 + 200*m2^5*Ycut^3 - 
          13216*MBhat*Ycut^3 + 3452*m2*MBhat*Ycut^3 + 184224*m2^2*MBhat*
           Ycut^3 + 46060*m2^3*MBhat*Ycut^3 - 5360*m2^4*MBhat*Ycut^3 + 
          440*m2^5*MBhat*Ycut^3 + 30272*MBhat^2*Ycut^3 - 
          53962*m2*MBhat^2*Ycut^3 - 224325*m2^2*MBhat^2*Ycut^3 + 
          12775*m2^3*MBhat^2*Ycut^3 - 2570*m2^4*MBhat^2*Ycut^3 + 
          650*m2^5*MBhat^2*Ycut^3 - 35546*MBhat^3*Ycut^3 + 
          95200*m2*MBhat^3*Ycut^3 + 89956*m2^2*MBhat^3*Ycut^3 - 
          11980*m2^3*MBhat^3*Ycut^3 - 1330*m2^4*MBhat^3*Ycut^3 + 
          760*m2^5*MBhat^3*Ycut^3 + 22260*MBhat^4*Ycut^3 - 
          67440*m2*MBhat^4*Ycut^3 + 9645*m2^2*MBhat^4*Ycut^3 - 
          7440*m2^3*MBhat^4*Ycut^3 + 2550*m2^4*MBhat^4*Ycut^3 - 
          7050*MBhat^5*Ycut^3 + 19920*m2*MBhat^5*Ycut^3 - 
          11250*m2^2*MBhat^5*Ycut^3 + 3000*m2^3*MBhat^5*Ycut^3 + 
          920*MBhat^6*Ycut^3 - 1630*m2*MBhat^6*Ycut^3 + 1300*m2^2*MBhat^6*
           Ycut^3 - 710*Ycut^4 - 1875*m2*Ycut^4 + 15985*m2^2*Ycut^4 + 
          15665*m2^3*Ycut^4 - 195*m2^4*Ycut^4 - 100*m2^5*Ycut^4 + 
          4024*MBhat*Ycut^4 + 1236*m2*MBhat*Ycut^4 - 63540*m2^2*MBhat*
           Ycut^4 - 20680*m2^3*MBhat*Ycut^4 + 2460*m2^4*MBhat*Ycut^4 - 
          220*m2^5*MBhat*Ycut^4 - 9803*MBhat^2*Ycut^4 + 13920*m2*MBhat^2*
           Ycut^4 + 82725*m2^2*MBhat^2*Ycut^4 - 3400*m2^3*MBhat^2*Ycut^4 + 
          1275*m2^4*MBhat^2*Ycut^4 - 325*m2^5*MBhat^2*Ycut^4 + 
          10814*MBhat^3*Ycut^4 - 25116*m2*MBhat^3*Ycut^4 - 
          41360*m2^2*MBhat^3*Ycut^4 + 6760*m2^3*MBhat^3*Ycut^4 + 
          510*m2^4*MBhat^3*Ycut^4 - 380*m2^5*MBhat^3*Ycut^4 - 
          4695*MBhat^4*Ycut^4 + 16635*m2*MBhat^4*Ycut^4 + 
          420*m2^2*MBhat^4*Ycut^4 + 3300*m2^3*MBhat^4*Ycut^4 - 
          1275*m2^4*MBhat^4*Ycut^4 + 300*MBhat^5*Ycut^4 - 
          5730*m2*MBhat^5*Ycut^4 + 5070*m2^2*MBhat^5*Ycut^4 - 
          1500*m2^3*MBhat^5*Ycut^4 + 70*MBhat^6*Ycut^4 + 
          930*m2*MBhat^6*Ycut^4 - 650*m2^2*MBhat^6*Ycut^4 + 24*Ycut^5 + 
          174*m2*Ycut^5 - 761*m2^2*Ycut^5 - 1476*m2^3*Ycut^5 - 
          81*m2^4*Ycut^5 + 20*m2^5*Ycut^5 + 87*MBhat*Ycut^5 - 
          1044*m2*MBhat*Ycut^5 + 3846*m2^2*MBhat*Ycut^5 + 
          3216*m2^3*MBhat*Ycut^5 - 549*m2^4*MBhat*Ycut^5 + 
          44*m2^5*MBhat*Ycut^5 + 1494*MBhat^2*Ycut^5 - 2391*m2*MBhat^2*
           Ycut^5 - 4356*m2^2*MBhat^2*Ycut^5 - 696*m2^3*MBhat^2*Ycut^5 - 
          276*m2^4*MBhat^2*Ycut^5 + 65*m2^5*MBhat^2*Ycut^5 - 
          1485*MBhat^3*Ycut^5 - 534*m2*MBhat^3*Ycut^5 + 8146*m2^2*MBhat^3*
           Ycut^5 - 2634*m2^3*MBhat^3*Ycut^5 - 9*m2^4*MBhat^3*Ycut^5 + 
          76*m2^5*MBhat^3*Ycut^5 - 2580*MBhat^4*Ycut^5 + 
          5985*m2*MBhat^4*Ycut^5 - 3975*m2^2*MBhat^4*Ycut^5 - 
          435*m2^3*MBhat^4*Ycut^5 + 255*m2^4*MBhat^4*Ycut^5 + 
          3060*MBhat^5*Ycut^5 - 1890*m2*MBhat^5*Ycut^5 - 
          870*m2^2*MBhat^5*Ycut^5 + 300*m2^3*MBhat^5*Ycut^5 - 
          600*MBhat^6*Ycut^5 - 300*m2*MBhat^6*Ycut^5 + 130*m2^2*MBhat^6*
           Ycut^5 - 44*Ycut^6 + 130*m2*Ycut^6 - 151*m2^2*Ycut^6 - 
          287*m2^3*Ycut^6 + 52*m2^4*Ycut^6 - 772*MBhat*Ycut^6 + 
          1379*m2*MBhat*Ycut^6 + 35*m2^2*MBhat*Ycut^6 + 121*m2^3*MBhat*
           Ycut^6 + 37*m2^4*MBhat*Ycut^6 - 454*MBhat^2*Ycut^6 + 
          2165*m2*MBhat^2*Ycut^6 - 3361*m2^2*MBhat^2*Ycut^6 + 
          763*m2^3*MBhat^2*Ycut^6 + 7*m2^4*MBhat^2*Ycut^6 + 
          2240*MBhat^3*Ycut^6 - 1789*m2*MBhat^3*Ycut^6 - 
          423*m2^2*MBhat^3*Ycut^6 + 683*m2^3*MBhat^3*Ycut^6 - 
          31*m2^4*MBhat^3*Ycut^6 + 570*MBhat^4*Ycut^6 - 3585*m2*MBhat^4*
           Ycut^6 + 2040*m2^2*MBhat^4*Ycut^6 - 75*m2^3*MBhat^4*Ycut^6 - 
          1920*MBhat^5*Ycut^6 + 1650*m2*MBhat^5*Ycut^6 - 
          30*m2^2*MBhat^5*Ycut^6 + 380*MBhat^6*Ycut^6 + 
          50*m2*MBhat^6*Ycut^6 + 176*Ycut^7 - 294*m2*Ycut^7 + 
          35*m2^2*Ycut^7 + 8*m2^3*Ycut^7 + 778*MBhat*Ycut^7 - 
          1353*m2*MBhat*Ycut^7 + 872*m2^2*MBhat*Ycut^7 - 
          97*m2^3*MBhat*Ycut^7 - 824*MBhat^2*Ycut^7 + 171*m2*MBhat^2*Ycut^7 + 
          320*m2^2*MBhat^2*Ycut^7 - 97*m2^3*MBhat^2*Ycut^7 - 
          1160*MBhat^3*Ycut^7 + 1491*m2*MBhat^3*Ycut^7 - 
          132*m2^2*MBhat^3*Ycut^7 - 89*m2^3*MBhat^3*Ycut^7 + 
          720*MBhat^4*Ycut^7 + 315*m2*MBhat^4*Ycut^7 - 285*m2^2*MBhat^4*
           Ycut^7 + 390*MBhat^5*Ycut^7 - 330*m2*MBhat^5*Ycut^7 - 
          80*MBhat^6*Ycut^7 - 234*Ycut^8 + 297*m2*Ycut^8 - 88*m2^2*Ycut^8 - 
          192*MBhat*Ycut^8 + 285*m2*MBhat*Ycut^8 - 73*m2^2*MBhat*Ycut^8 + 
          831*MBhat^2*Ycut^8 - 528*m2*MBhat^2*Ycut^8 + 17*m2^2*MBhat^2*
           Ycut^8 - 150*MBhat^3*Ycut^8 - 99*m2*MBhat^3*Ycut^8 + 
          9*m2^2*MBhat^3*Ycut^8 - 255*MBhat^4*Ycut^8 + 45*m2*MBhat^4*Ycut^8 + 
          136*Ycut^9 - 92*m2*Ycut^9 - 137*MBhat*Ycut^9 + 73*m2*MBhat*Ycut^9 - 
          134*MBhat^2*Ycut^9 + 28*m2*MBhat^2*Ycut^9 + 135*MBhat^3*Ycut^9 - 
          9*m2*MBhat^3*Ycut^9 - 28*Ycut^10 + 56*MBhat*Ycut^10 - 
          28*MBhat^2*Ycut^10))/(15*(-1 + Ycut)^5) - 
       4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5 - 
         12*MBhat - 48*m2*MBhat + 300*m2^2*MBhat + 320*m2^3*MBhat + 
         30*MBhat^2 + 39*m2*MBhat^2 - 513*m2^2*MBhat^2 - 190*m2^3*MBhat^2 + 
         18*m2^4*MBhat^2 - 40*MBhat^3 + 24*m2*MBhat^3 + 372*m2^2*MBhat^3 + 
         30*MBhat^4 - 51*m2*MBhat^4 - 102*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 
         12*MBhat^5 + 24*m2*MBhat^5 + 2*MBhat^6 - 3*m2*MBhat^6 + 
         3*m2^2*MBhat^6)*Log[m2] + 4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 
         140*m2^3 - 30*m2^4 + 3*m2^5 - 12*MBhat - 48*m2*MBhat + 
         300*m2^2*MBhat + 320*m2^3*MBhat + 30*MBhat^2 + 39*m2*MBhat^2 - 
         513*m2^2*MBhat^2 - 190*m2^3*MBhat^2 + 18*m2^4*MBhat^2 - 40*MBhat^3 + 
         24*m2*MBhat^3 + 372*m2^2*MBhat^3 + 30*MBhat^4 - 51*m2*MBhat^4 - 
         102*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 12*MBhat^5 + 24*m2*MBhat^5 + 
         2*MBhat^6 - 3*m2*MBhat^6 + 3*m2^2*MBhat^6)*Log[1 - Ycut]) + 
     c[SL]*((Ycut^3*(-1 + m2 + Ycut)^2*(130*MBhat^3 - 40*m2*MBhat^3 - 
          660*m2^2*MBhat^3 + 920*m2^3*MBhat^3 - 350*m2^4*MBhat^3 - 
          300*MBhat^4 - 300*m2*MBhat^4 + 1500*m2^2*MBhat^4 - 
          900*m2^3*MBhat^4 + 210*MBhat^5 + 540*m2*MBhat^5 - 
          750*m2^2*MBhat^5 - 40*MBhat^6 - 200*m2*MBhat^6 - 195*MBhat^2*Ycut + 
          60*m2*MBhat^2*Ycut + 990*m2^2*MBhat^2*Ycut - 1380*m2^3*MBhat^2*
           Ycut + 525*m2^4*MBhat^2*Ycut - 240*MBhat^3*Ycut + 
          980*m2*MBhat^3*Ycut - 540*m2^2*MBhat^3*Ycut - 900*m2^3*MBhat^3*
           Ycut + 700*m2^4*MBhat^3*Ycut + 1275*MBhat^4*Ycut - 
          300*m2*MBhat^4*Ycut - 3225*m2^2*MBhat^4*Ycut + 2250*m2^3*MBhat^4*
           Ycut - 1050*MBhat^5*Ycut - 1440*m2*MBhat^5*Ycut + 
          2250*m2^2*MBhat^5*Ycut + 210*MBhat^6*Ycut + 700*m2*MBhat^6*Ycut + 
          117*MBhat*Ycut^2 - 36*m2*MBhat*Ycut^2 - 594*m2^2*MBhat*Ycut^2 + 
          828*m2^3*MBhat*Ycut^2 - 315*m2^4*MBhat*Ycut^2 + 
          786*MBhat^2*Ycut^2 - 1128*m2*MBhat^2*Ycut^2 - 732*m2^2*MBhat^2*
           Ycut^2 + 1704*m2^3*MBhat^2*Ycut^2 - 630*m2^4*MBhat^2*Ycut^2 - 
          778*MBhat^3*Ycut^2 - 1596*m2*MBhat^3*Ycut^2 + 3456*m2^2*MBhat^3*
           Ycut^2 - 452*m2^3*MBhat^3*Ycut^2 - 630*m2^4*MBhat^3*Ycut^2 - 
          1710*MBhat^4*Ycut^2 + 2730*m2*MBhat^4*Ycut^2 + 1620*m2^2*MBhat^4*
           Ycut^2 - 2160*m2^3*MBhat^4*Ycut^2 + 2025*MBhat^5*Ycut^2 + 
          930*m2*MBhat^5*Ycut^2 - 2475*m2^2*MBhat^5*Ycut^2 - 
          440*MBhat^6*Ycut^2 - 900*m2*MBhat^6*Ycut^2 - 26*Ycut^3 + 
          8*m2*Ycut^3 + 132*m2^2*Ycut^3 - 184*m2^3*Ycut^3 + 70*m2^4*Ycut^3 - 
          582*MBhat*Ycut^3 + 600*m2*MBhat*Ycut^3 + 756*m2^2*MBhat*Ycut^3 - 
          984*m2^3*MBhat*Ycut^3 + 210*m2^4*MBhat*Ycut^3 - 
          843*MBhat^2*Ycut^3 + 2526*m2*MBhat^2*Ycut^3 - 1728*m2^2*MBhat^2*
           Ycut^3 - 270*m2^3*MBhat^2*Ycut^3 + 315*m2^4*MBhat^2*Ycut^3 + 
          2656*MBhat^3*Ycut^3 - 784*m2*MBhat^3*Ycut^3 - 2580*m2^2*MBhat^3*
           Ycut^3 + 668*m2^3*MBhat^3*Ycut^3 + 280*m2^4*MBhat^3*Ycut^3 + 
          135*MBhat^4*Ycut^3 - 3300*m2*MBhat^4*Ycut^3 + 570*m2^2*MBhat^4*
           Ycut^3 + 990*m2^3*MBhat^4*Ycut^3 - 1800*MBhat^5*Ycut^3 + 
          450*m2*MBhat^5*Ycut^3 + 1200*m2^2*MBhat^5*Ycut^3 + 
          460*MBhat^6*Ycut^3 + 500*m2*MBhat^6*Ycut^3 + 144*Ycut^4 - 
          124*m2*Ycut^4 - 204*m2^2*Ycut^4 + 204*m2^3*Ycut^4 - 
          20*m2^4*Ycut^4 + 1068*MBhat*Ycut^4 - 1494*m2*MBhat*Ycut^4 + 
          315*m2^2*MBhat*Ycut^4 + 156*m2^3*MBhat*Ycut^4 - 
          45*m2^4*MBhat*Ycut^4 - 528*MBhat^2*Ycut^4 - 1380*m2*MBhat^2*
           Ycut^4 + 1692*m2^2*MBhat^2*Ycut^4 - 84*m2^3*MBhat^2*Ycut^4 - 
          60*m2^4*MBhat^2*Ycut^4 - 2604*MBhat^3*Ycut^4 + 
          2708*m2*MBhat^3*Ycut^4 + 222*m2^2*MBhat^3*Ycut^4 - 
          276*m2^3*MBhat^3*Ycut^4 - 50*m2^4*MBhat^3*Ycut^4 + 
          1560*MBhat^4*Ycut^4 + 1020*m2*MBhat^4*Ycut^4 - 
          600*m2^2*MBhat^4*Ycut^4 - 180*m2^3*MBhat^4*Ycut^4 + 
          600*MBhat^5*Ycut^4 - 630*m2*MBhat^5*Ycut^4 - 225*m2^2*MBhat^5*
           Ycut^4 - 240*MBhat^6*Ycut^4 - 100*m2*MBhat^6*Ycut^4 - 316*Ycut^5 + 
          324*m2*Ycut^5 + 12*m2^2*Ycut^5 - 20*m2^3*Ycut^5 - 
          792*MBhat*Ycut^5 + 1242*m2*MBhat*Ycut^5 - 522*m2^2*MBhat*Ycut^5 + 
          1647*MBhat^2*Ycut^5 - 636*m2*MBhat^2*Ycut^5 - 222*m2^2*MBhat^2*
           Ycut^5 + 30*m2^3*MBhat^2*Ycut^5 + 536*MBhat^3*Ycut^5 - 
          1320*m2*MBhat^3*Ycut^5 + 132*m2^2*MBhat^3*Ycut^5 + 
          40*m2^3*MBhat^3*Ycut^5 - 1215*MBhat^4*Ycut^5 + 
          240*m2*MBhat^4*Ycut^5 + 135*m2^2*MBhat^4*Ycut^5 + 
          90*MBhat^5*Ycut^5 + 150*m2*MBhat^5*Ycut^5 + 50*MBhat^6*Ycut^5 + 
          344*Ycut^6 - 308*m2*Ycut^6 + 60*m2^2*Ycut^6 + 33*MBhat*Ycut^6 - 
          222*m2*MBhat*Ycut^6 + 45*m2^2*MBhat*Ycut^6 - 1038*MBhat^2*Ycut^6 + 
          588*m2*MBhat^2*Ycut^6 + 526*MBhat^3*Ycut^6 + 32*m2*MBhat^3*Ycut^6 - 
          30*m2^2*MBhat^3*Ycut^6 + 210*MBhat^4*Ycut^6 - 
          90*m2*MBhat^4*Ycut^6 - 75*MBhat^5*Ycut^6 - 186*Ycut^7 + 
          100*m2*Ycut^7 + 246*MBhat*Ycut^7 - 90*m2*MBhat*Ycut^7 + 
          111*MBhat^2*Ycut^7 - 30*m2*MBhat^2*Ycut^7 - 216*MBhat^3*Ycut^7 + 
          20*m2*MBhat^3*Ycut^7 + 45*MBhat^4*Ycut^7 + 40*Ycut^8 - 
          90*MBhat*Ycut^8 + 60*MBhat^2*Ycut^8 - 10*MBhat^3*Ycut^8)*c[T])/
        (30*(-1 + Ycut)^6) + 
       c[SR]*(-1/10*(Sqrt[m2]*(-1 + m2 + Ycut)*(542 + 6436*m2 + 11886*m2^2 + 
             3886*m2^3 - 64*m2^4 - 6*m2^5 - 3084*MBhat - 26196*m2*MBhat - 
             28896*m2^2*MBhat - 2496*m2^3*MBhat + 204*m2^4*MBhat - 
             12*m2^5*MBhat + 7242*MBhat^2 + 41427*m2*MBhat^2 + 
             22752*m2^2*MBhat^2 - 948*m2^3*MBhat^2 + 102*m2^4*MBhat^2 - 
             15*m2^5*MBhat^2 - 8960*MBhat^3 - 31672*m2*MBhat^3 - 
             5172*m2^2*MBhat^3 + 428*m2^3*MBhat^3 + 28*m2^4*MBhat^3 - 
             12*m2^5*MBhat^3 + 6150*MBhat^4 + 11685*m2*MBhat^4 - 
             915*m2^2*MBhat^4 + 225*m2^3*MBhat^4 - 45*m2^4*MBhat^4 - 
             2220*MBhat^5 - 1740*m2*MBhat^5 + 420*m2^2*MBhat^5 - 
             60*m2^3*MBhat^5 + 330*MBhat^6 + 60*m2*MBhat^6 - 
             30*m2^2*MBhat^6 - 2288*Ycut - 28142*m2*Ycut - 53706*m2^2*Ycut - 
             18220*m2^3*Ycut + 266*m2^4*Ycut + 30*m2^5*Ycut + 
             13056*MBhat*Ycut + 115380*m2*MBhat*Ycut + 132384*m2^2*MBhat*
              Ycut + 12288*m2^3*MBhat*Ycut - 1008*m2^4*MBhat*Ycut + 
             60*m2^5*MBhat*Ycut - 30768*MBhat^2*Ycut - 184206*m2*MBhat^2*
              Ycut - 106419*m2^2*MBhat^2*Ycut + 4293*m2^3*MBhat^2*Ycut - 
             495*m2^4*MBhat^2*Ycut + 75*m2^5*MBhat^2*Ycut + 
             38240*MBhat^3*Ycut + 142688*m2*MBhat^3*Ycut + 25416*m2^2*MBhat^3*
              Ycut - 2156*m2^3*MBhat^3*Ycut - 128*m2^4*MBhat^3*Ycut + 
             60*m2^5*MBhat^3*Ycut - 26400*MBhat^4*Ycut - 53730*m2*MBhat^4*
              Ycut + 4035*m2^2*MBhat^4*Ycut - 1080*m2^3*MBhat^4*Ycut + 
             225*m2^4*MBhat^4*Ycut + 9600*MBhat^5*Ycut + 8340*m2*MBhat^5*
              Ycut - 2040*m2^2*MBhat^5*Ycut + 300*m2^3*MBhat^5*Ycut - 
             1440*MBhat^6*Ycut - 330*m2*MBhat^6*Ycut + 150*m2^2*MBhat^6*
              Ycut + 3672*Ycut^2 + 47160*m2*Ycut^2 + 93854*m2^2*Ycut^2 + 
             33434*m2^3*Ycut^2 - 400*m2^4*Ycut^2 - 60*m2^5*Ycut^2 - 
             21024*MBhat*Ycut^2 - 195084*m2*MBhat*Ycut^2 - 235500*m2^2*MBhat*
              Ycut^2 - 24012*m2^3*MBhat*Ycut^2 + 1980*m2^4*MBhat*Ycut^2 - 
             120*m2^5*MBhat*Ycut^2 + 49752*MBhat^2*Ycut^2 + 315126*m2*MBhat^2*
              Ycut^2 + 194487*m2^2*MBhat^2*Ycut^2 - 7440*m2^3*MBhat^2*
              Ycut^2 + 945*m2^4*MBhat^2*Ycut^2 - 150*m2^5*MBhat^2*Ycut^2 - 
             62160*MBhat^3*Ycut^2 - 248112*m2*MBhat^3*Ycut^2 - 
             49496*m2^2*MBhat^3*Ycut^2 + 4348*m2^3*MBhat^3*Ycut^2 + 
             220*m2^4*MBhat^3*Ycut^2 - 120*m2^5*MBhat^3*Ycut^2 + 
             43200*MBhat^4*Ycut^2 + 95850*m2*MBhat^4*Ycut^2 - 
             6675*m2^2*MBhat^4*Ycut^2 + 2025*m2^3*MBhat^4*Ycut^2 - 
             450*m2^4*MBhat^4*Ycut^2 - 15840*MBhat^5*Ycut^2 - 
             15660*m2*MBhat^5*Ycut^2 + 3900*m2^2*MBhat^5*Ycut^2 - 
             600*m2^3*MBhat^5*Ycut^2 + 2400*MBhat^6*Ycut^2 + 
             720*m2*MBhat^6*Ycut^2 - 300*m2^2*MBhat^6*Ycut^2 - 2688*Ycut^3 - 
             36558*m2*Ycut^3 - 77104*m2^2*Ycut^3 - 29470*m2^3*Ycut^3 + 
             230*m2^4*Ycut^3 + 60*m2^5*Ycut^3 + 15456*MBhat*Ycut^3 + 
             153012*m2*MBhat*Ycut^3 + 198312*m2^2*MBhat*Ycut^3 + 
             23100*m2^3*MBhat*Ycut^3 - 1920*m2^4*MBhat*Ycut^3 + 
             120*m2^5*MBhat*Ycut^3 - 36768*MBhat^2*Ycut^3 - 251022*m2*MBhat^2*
              Ycut^3 - 170115*m2^2*MBhat^2*Ycut^3 + 5865*m2^3*MBhat^2*
              Ycut^3 - 870*m2^4*MBhat^2*Ycut^3 + 150*m2^5*MBhat^2*Ycut^3 + 
             46170*MBhat^3*Ycut^3 + 202248*m2*MBhat^3*Ycut^3 + 
             46852*m2^2*MBhat^3*Ycut^3 - 4100*m2^3*MBhat^3*Ycut^3 - 
             230*m2^4*MBhat^3*Ycut^3 + 120*m2^5*MBhat^3*Ycut^3 - 
             32220*MBhat^4*Ycut^3 - 81270*m2*MBhat^4*Ycut^3 + 
             5295*m2^2*MBhat^4*Ycut^3 - 1980*m2^3*MBhat^4*Ycut^3 + 
             450*m2^4*MBhat^4*Ycut^3 + 11850*MBhat^5*Ycut^3 + 
             14400*m2*MBhat^5*Ycut^3 - 3750*m2^2*MBhat^5*Ycut^3 + 
             600*m2^3*MBhat^5*Ycut^3 - 1800*MBhat^6*Ycut^3 - 
             810*m2*MBhat^6*Ycut^3 + 300*m2^2*MBhat^6*Ycut^3 + 792*Ycut^4 + 
             11799*m2*Ycut^4 + 27395*m2^2*Ycut^4 + 11825*m2^3*Ycut^4 + 
             5*m2^4*Ycut^4 - 30*m2^5*Ycut^4 - 4584*MBhat*Ycut^4 - 
             50292*m2*MBhat*Ycut^4 - 73380*m2^2*MBhat*Ycut^4 - 
             10680*m2^3*MBhat*Ycut^4 + 900*m2^4*MBhat*Ycut^4 - 
             60*m2^5*MBhat*Ycut^4 + 11097*MBhat^2*Ycut^4 + 84090*m2*MBhat^2*
              Ycut^4 + 67665*m2^2*MBhat^2*Ycut^4 - 2130*m2^3*MBhat^2*Ycut^4 + 
             465*m2^4*MBhat^2*Ycut^4 - 75*m2^5*MBhat^2*Ycut^4 - 
             14010*MBhat^3*Ycut^4 - 70272*m2*MBhat^3*Ycut^4 - 
             21260*m2^2*MBhat^3*Ycut^4 + 1900*m2^3*MBhat^3*Ycut^4 + 
             130*m2^4*MBhat^3*Ycut^4 - 60*m2^5*MBhat^3*Ycut^4 + 
             9555*MBhat^4*Ycut^4 + 30675*m2*MBhat^4*Ycut^4 - 
             2010*m2^2*MBhat^4*Ycut^4 + 1050*m2^3*MBhat^4*Ycut^4 - 
             225*m2^4*MBhat^4*Ycut^4 - 3300*MBhat^5*Ycut^4 - 
             6510*m2*MBhat^5*Ycut^4 + 1890*m2^2*MBhat^5*Ycut^4 - 
             300*m2^3*MBhat^5*Ycut^4 + 450*MBhat^6*Ycut^4 + 
             510*m2*MBhat^6*Ycut^4 - 150*m2^2*MBhat^6*Ycut^4 - 24*Ycut^5 - 
             558*m2*Ycut^5 - 1903*m2^2*Ycut^5 - 1258*m2^3*Ycut^5 - 
             43*m2^4*Ycut^5 + 6*m2^5*Ycut^5 + 81*MBhat*Ycut^5 + 
             2808*m2*MBhat*Ycut^5 + 5478*m2^2*MBhat*Ycut^5 + 
             1908*m2^3*MBhat*Ycut^5 - 207*m2^4*MBhat*Ycut^5 + 
             12*m2^5*MBhat*Ycut^5 - 510*MBhat^2*Ycut^5 - 4155*m2*MBhat^2*
              Ycut^5 - 7260*m2^2*MBhat^2*Ycut^5 + 270*m2^3*MBhat^2*Ycut^5 - 
             120*m2^4*MBhat^2*Ycut^5 + 15*m2^5*MBhat^2*Ycut^5 + 
             933*MBhat^3*Ycut^5 + 3270*m2*MBhat^3*Ycut^5 + 3850*m2^2*MBhat^3*
              Ycut^5 - 470*m2^3*MBhat^3*Ycut^5 - 35*m2^4*MBhat^3*Ycut^5 + 
             12*m2^5*MBhat^3*Ycut^5 - 420*MBhat^4*Ycut^5 - 2355*m2*MBhat^4*
              Ycut^5 + 135*m2^2*MBhat^4*Ycut^5 - 255*m2^3*MBhat^4*Ycut^5 + 
             45*m2^4*MBhat^4*Ycut^5 - 180*MBhat^5*Ycut^5 + 1170*m2*MBhat^5*
              Ycut^5 - 450*m2^2*MBhat^5*Ycut^5 + 60*m2^3*MBhat^5*Ycut^5 + 
             120*MBhat^6*Ycut^5 - 180*m2*MBhat^6*Ycut^5 + 30*m2^2*MBhat^6*
              Ycut^5 + 10*Ycut^6 - 148*m2*Ycut^6 - 211*m2^2*Ycut^6 - 
             209*m2^3*Ycut^6 + 18*m2^4*Ycut^6 + 168*MBhat*Ycut^6 - 
             39*m2*MBhat*Ycut^6 + 1389*m2^2*MBhat*Ycut^6 - 93*m2^3*MBhat*
              Ycut^6 + 15*m2^4*MBhat*Ycut^6 - 216*MBhat^2*Ycut^6 - 
             381*m2*MBhat^2*Ycut^6 - 1191*m2^2*MBhat^2*Ycut^6 + 
             99*m2^3*MBhat^2*Ycut^6 + 9*m2^4*MBhat^2*Ycut^6 - 
             232*MBhat^3*Ycut^6 + 1393*m2*MBhat^3*Ycut^6 - 137*m2^2*MBhat^3*
              Ycut^6 + 53*m2^3*MBhat^3*Ycut^6 + 3*m2^4*MBhat^3*Ycut^6 + 
             330*MBhat^4*Ycut^6 - 885*m2*MBhat^4*Ycut^6 + 150*m2^2*MBhat^4*
              Ycut^6 + 15*m2^3*MBhat^4*Ycut^6 + 30*m2*MBhat^5*Ycut^6 + 
             30*m2^2*MBhat^5*Ycut^6 - 60*MBhat^6*Ycut^6 + 30*m2*MBhat^6*
              Ycut^6 - 40*Ycut^7 + 92*m2*Ycut^7 - 199*m2^2*Ycut^7 + 
             12*m2^3*Ycut^7 - 42*MBhat*Ycut^7 + 189*m2*MBhat*Ycut^7 + 
             228*m2^2*MBhat*Ycut^7 - 15*m2^3*MBhat*Ycut^7 + 
             264*MBhat^2*Ycut^7 - 747*m2*MBhat^2*Ycut^7 + 72*m2^2*MBhat^2*
              Ycut^7 - 9*m2^3*MBhat^2*Ycut^7 - 152*MBhat^3*Ycut^7 + 
             481*m2*MBhat^3*Ycut^7 - 56*m2^2*MBhat^3*Ycut^7 - 
             3*m2^3*MBhat^3*Ycut^7 - 120*MBhat^4*Ycut^7 + 15*m2*MBhat^4*
              Ycut^7 - 15*m2^2*MBhat^4*Ycut^7 + 90*MBhat^5*Ycut^7 - 
             30*m2*MBhat^5*Ycut^7 + 30*Ycut^8 - 63*m2*Ycut^8 - 
             12*m2^2*Ycut^8 - 72*MBhat*Ycut^8 + 207*m2*MBhat*Ycut^8 - 
             15*m2^2*MBhat*Ycut^8 - 21*MBhat^2*Ycut^8 - 138*m2*MBhat^2*
              Ycut^8 + 9*m2^2*MBhat^2*Ycut^8 + 138*MBhat^3*Ycut^8 - 
             21*m2*MBhat^3*Ycut^8 + 3*m2^2*MBhat^3*Ycut^8 - 
             75*MBhat^4*Ycut^8 + 15*m2*MBhat^4*Ycut^8 - 18*m2*Ycut^9 + 
             33*MBhat*Ycut^9 + 15*m2*MBhat*Ycut^9 - 66*MBhat^2*Ycut^9 + 
             6*m2*MBhat^2*Ycut^9 + 33*MBhat^3*Ycut^9 - 3*m2*MBhat^3*Ycut^9 - 
             6*Ycut^10 + 12*MBhat*Ycut^10 - 6*MBhat^2*Ycut^10))/
           (-1 + Ycut)^5 + 6*Sqrt[m2]*(-2 - 47*m2 - 170*m2^2 - 140*m2^3 - 
           20*m2^4 + m2^5 + 12*MBhat + 216*m2*MBhat + 540*m2^2*MBhat + 
           240*m2^3*MBhat - 30*MBhat^2 - 399*m2*MBhat^2 - 639*m2^2*MBhat^2 - 
           114*m2^3*MBhat^2 + 6*m2^4*MBhat^2 + 40*MBhat^3 + 376*m2*MBhat^3 + 
           340*m2^2*MBhat^3 - 30*MBhat^4 - 189*m2*MBhat^4 - 72*m2^2*MBhat^4 + 
           6*m2^3*MBhat^4 + 12*MBhat^5 + 48*m2*MBhat^5 - 2*MBhat^6 - 
           5*m2*MBhat^6 + m2^2*MBhat^6)*Log[m2] - 6*Sqrt[m2]*
          (-2 - 47*m2 - 170*m2^2 - 140*m2^3 - 20*m2^4 + m2^5 + 12*MBhat + 
           216*m2*MBhat + 540*m2^2*MBhat + 240*m2^3*MBhat - 30*MBhat^2 - 
           399*m2*MBhat^2 - 639*m2^2*MBhat^2 - 114*m2^3*MBhat^2 + 
           6*m2^4*MBhat^2 + 40*MBhat^3 + 376*m2*MBhat^3 + 340*m2^2*MBhat^3 - 
           30*MBhat^4 - 189*m2*MBhat^4 - 72*m2^2*MBhat^4 + 6*m2^3*MBhat^4 + 
           12*MBhat^5 + 48*m2*MBhat^5 - 2*MBhat^6 - 5*m2*MBhat^6 + 
           m2^2*MBhat^6)*Log[1 - Ycut])) + 
     c[VL]*(((-1 + m2 + Ycut)*(345 + 12378*m2 - 16497*m2^2 - 49572*m2^3 + 
          6603*m2^4 + 1458*m2^5 - 75*m2^6 - 2360*MBhat - 52060*m2*MBhat + 
          104180*m2^2*MBhat + 93680*m2^3*MBhat - 11320*m2^4*MBhat + 
          2540*m2^5*MBhat - 260*m2^6*MBhat + 6786*MBhat^2 + 
          80846*m2*MBhat^2 - 188374*m2^2*MBhat^2 - 30244*m2^3*MBhat^2 - 
          4414*m2^4*MBhat^2 + 3230*m2^5*MBhat^2 - 550*m2^6*MBhat^2 - 
          10392*MBhat^3 - 53400*m2*MBhat^3 + 120060*m2^2*MBhat^3 - 
          6360*m2^3*MBhat^3 - 2160*m2^4*MBhat^3 + 3552*m2^5*MBhat^3 - 
          900*m2^6*MBhat^3 + 8771*MBhat^4 + 9926*m2*MBhat^4 - 
          5194*m2^2*MBhat^4 - 16954*m2^3*MBhat^4 + 12131*m2^4*MBhat^4 - 
          2800*m2^5*MBhat^4 - 3780*MBhat^5 + 3360*m2*MBhat^5 - 
          18480*m2^2*MBhat^5 + 11760*m2^3*MBhat^5 - 2940*m2^4*MBhat^5 + 
          630*MBhat^6 - 1050*m2*MBhat^6 + 3990*m2^2*MBhat^6 - 
          1050*m2^3*MBhat^6 - 1725*Ycut - 66585*m2*Ycut + 83868*m2^2*Ycut + 
          276846*m2^3*Ycut - 34701*m2^4*Ycut - 8673*m2^5*Ycut + 
          450*m2^6*Ycut + 11800*MBhat*Ycut + 283140*m2*MBhat*Ycut - 
          550120*m2^2*MBhat*Ycut - 536240*m2^3*MBhat*Ycut + 
          65640*m2^4*MBhat*Ycut - 14980*m2^5*MBhat*Ycut + 
          1560*m2^6*MBhat*Ycut - 33930*MBhat^2*Ycut - 447844*m2*MBhat^2*
           Ycut + 1019422*m2^2*MBhat^2*Ycut + 184038*m2^3*MBhat^2*Ycut + 
          23804*m2^4*MBhat^2*Ycut - 18830*m2^5*MBhat^2*Ycut + 
          3300*m2^6*MBhat^2*Ycut + 51960*MBhat^3*Ycut + 307008*m2*MBhat^3*
           Ycut - 669132*m2^2*MBhat^3*Ycut + 37668*m2^3*MBhat^3*Ycut + 
          10308*m2^4*MBhat^3*Ycut - 20412*m2^5*MBhat^3*Ycut + 
          5400*m2^6*MBhat^3*Ycut - 43855*MBhat^4*Ycut - 66059*m2*MBhat^4*
           Ycut + 38367*m2^2*MBhat^4*Ycut + 92393*m2^3*MBhat^4*Ycut - 
          69986*m2^4*MBhat^4*Ycut + 16800*m2^5*MBhat^4*Ycut + 
          18900*MBhat^5*Ycut - 15540*m2*MBhat^5*Ycut + 102060*m2^2*MBhat^5*
           Ycut - 67620*m2^3*MBhat^5*Ycut + 17640*m2^4*MBhat^5*Ycut - 
          3150*MBhat^6*Ycut + 5880*m2*MBhat^6*Ycut - 22890*m2^2*MBhat^6*
           Ycut + 6300*m2^3*MBhat^6*Ycut + 3450*Ycut^2 + 145080*m2*Ycut^2 - 
          169527*m2^2*Ycut^2 - 631356*m2^3*Ycut^2 + 72618*m2^4*Ycut^2 + 
          21420*m2^5*Ycut^2 - 1125*m2^6*Ycut^2 - 23600*MBhat*Ycut^2 - 
          624560*m2*MBhat*Ycut^2 + 1168920*m2^2*MBhat*Ycut^2 + 
          1260580*m2^3*MBhat*Ycut^2 - 156380*m2^4*MBhat*Ycut^2 + 
          36540*m2^5*MBhat*Ycut^2 - 3900*m2^6*MBhat*Ycut^2 + 
          67860*MBhat^2*Ycut^2 + 1008116*m2*MBhat^2*Ycut^2 - 
          2232522*m2^2*MBhat^2*Ycut^2 - 466554*m2^3*MBhat^2*Ycut^2 - 
          50680*m2^4*MBhat^2*Ycut^2 + 45150*m2^5*MBhat^2*Ycut^2 - 
          8250*m2^6*MBhat^2*Ycut^2 - 103920*MBhat^3*Ycut^2 - 
          719232*m2*MBhat^3*Ycut^2 + 1518456*m2^2*MBhat^3*Ycut^2 - 
          90696*m2^3*MBhat^3*Ycut^2 - 17388*m2^4*MBhat^3*Ycut^2 + 
          47880*m2^5*MBhat^3*Ycut^2 - 13500*m2^6*MBhat^3*Ycut^2 + 
          87710*MBhat^4*Ycut^2 + 177576*m2*MBhat^4*Ycut^2 - 
          114807*m2^2*MBhat^4*Ycut^2 - 201124*m2^3*MBhat^4*Ycut^2 + 
          165165*m2^4*MBhat^4*Ycut^2 - 42000*m2^5*MBhat^4*Ycut^2 - 
          37800*MBhat^5*Ycut^2 + 26040*m2*MBhat^5*Ycut^2 - 
          227220*m2^2*MBhat^5*Ycut^2 + 158760*m2^3*MBhat^5*Ycut^2 - 
          44100*m2^4*MBhat^5*Ycut^2 + 6300*MBhat^6*Ycut^2 - 
          13020*m2*MBhat^6*Ycut^2 + 53550*m2^2*MBhat^6*Ycut^2 - 
          15750*m2^3*MBhat^6*Ycut^2 - 3450*Ycut^3 - 161190*m2*Ycut^3 + 
          169083*m2^2*Ycut^3 + 743127*m2^3*Ycut^3 - 74655*m2^4*Ycut^3 - 
          28035*m2^5*Ycut^3 + 1500*m2^6*Ycut^3 + 23600*MBhat*Ycut^3 + 
          703840*m2*MBhat*Ycut^3 - 1252040*m2^2*MBhat*Ycut^3 - 
          1542660*m2^3*MBhat*Ycut^3 + 193760*m2^4*MBhat*Ycut^3 - 
          46900*m2^5*MBhat*Ycut^3 + 5200*m2^6*MBhat*Ycut^3 - 
          67860*MBhat^2*Ycut^3 - 1162544*m2*MBhat^2*Ycut^3 + 
          2486614*m2^2*MBhat^2*Ycut^3 + 629020*m2^3*MBhat^2*Ycut^3 + 
          51380*m2^4*MBhat^2*Ycut^3 - 56350*m2^5*MBhat^2*Ycut^3 + 
          11000*m2^6*MBhat^2*Ycut^3 + 102940*MBhat^3*Ycut^3 + 
          873868*m2*MBhat^3*Ycut^3 - 1779536*m2^2*MBhat^3*Ycut^3 + 
          109928*m2^3*MBhat^3*Ycut^3 + 19040*m2^4*MBhat^3*Ycut^3 - 
          62440*m2^5*MBhat^3*Ycut^3 + 18000*m2^6*MBhat^3*Ycut^3 - 
          85190*MBhat^4*Ycut^3 - 259154*m2*MBhat^4*Ycut^3 + 
          186739*m2^2*MBhat^4*Ycut^3 + 231035*m2^3*MBhat^4*Ycut^3 - 
          213220*m2^4*MBhat^4*Ycut^3 + 56000*m2^5*MBhat^4*Ycut^3 + 
          35700*MBhat^5*Ycut^3 - 7980*m2*MBhat^5*Ycut^3 + 
          258720*m2^2*MBhat^5*Ycut^3 - 201600*m2^3*MBhat^5*Ycut^3 + 
          58800*m2^4*MBhat^5*Ycut^3 - 5740*MBhat^6*Ycut^3 + 
          13160*m2*MBhat^6*Ycut^3 - 66850*m2^2*MBhat^6*Ycut^3 + 
          21000*m2^3*MBhat^6*Ycut^3 + 1725*Ycut^4 + 92850*m2*Ycut^4 - 
          81417*m2^2*Ycut^4 - 462840*m2^3*Ycut^4 + 35805*m2^4*Ycut^4 + 
          20370*m2^5*Ycut^4 - 1125*m2^6*Ycut^4 - 11800*MBhat*Ycut^4 - 
          412560*m2*MBhat*Ycut^4 + 679000*m2^2*MBhat*Ycut^4 + 
          1013740*m2^3*MBhat*Ycut^4 - 128100*m2^4*MBhat*Ycut^4 + 
          32900*m2^5*MBhat*Ycut^4 - 3900*m2^6*MBhat*Ycut^4 + 
          34875*MBhat^2*Ycut^4 + 693556*m2*MBhat^2*Ycut^4 - 
          1417990*m2^2*MBhat^2*Ycut^4 - 462840*m2^3*MBhat^2*Ycut^4 - 
          38815*m2^4*MBhat^2*Ycut^4 + 44800*m2^5*MBhat^2*Ycut^4 - 
          8250*m2^6*MBhat^2*Ycut^4 - 49510*MBhat^3*Ycut^4 - 
          569412*m2*MBhat^3*Ycut^4 + 1127392*m2^2*MBhat^3*Ycut^4 - 
          107660*m2^3*MBhat^3*Ycut^4 + 6090*m2^4*MBhat^3*Ycut^4 + 
          45080*m2^5*MBhat^3*Ycut^4 - 13500*m2^6*MBhat^3*Ycut^4 + 
          33670*MBhat^4*Ycut^4 + 237566*m2*MBhat^4*Ycut^4 - 
          207585*m2^2*MBhat^4*Ycut^4 - 127960*m2^3*MBhat^4*Ycut^4 + 
          157465*m2^4*MBhat^4*Ycut^4 - 42000*m2^5*MBhat^4*Ycut^4 - 
          9660*MBhat^5*Ycut^4 - 39060*m2*MBhat^5*Ycut^4 - 
          147000*m2^2*MBhat^5*Ycut^4 + 149100*m2^3*MBhat^5*Ycut^4 - 
          44100*m2^4*MBhat^5*Ycut^4 + 700*MBhat^6*Ycut^4 - 
          2940*m2*MBhat^6*Ycut^4 + 48650*m2^2*MBhat^6*Ycut^4 - 
          15750*m2^3*MBhat^6*Ycut^4 - 345*Ycut^5 - 23541*m2*Ycut^5 + 
          13482*m2^2*Ycut^5 + 132762*m2^3*Ycut^5 - 4053*m2^4*Ycut^5 - 
          7623*m2^5*Ycut^5 + 450*m2^6*Ycut^5 + 1898*MBhat*Ycut^5 + 
          110558*m2*MBhat*Ycut^5 - 153972*m2^2*MBhat*Ycut^5 - 
          323792*m2^3*MBhat*Ycut^5 + 50218*m2^4*MBhat*Ycut^5 - 
          15750*m2^5*MBhat*Ycut^5 + 1560*m2^6*MBhat*Ycut^5 - 
          10083*MBhat^2*Ycut^5 - 166817*m2*MBhat^2*Ycut^5 + 
          317793*m2^2*MBhat^2*Ycut^5 + 184653*m2^3*MBhat^2*Ycut^5 + 
          16268*m2^4*MBhat^2*Ycut^5 - 19950*m2^5*MBhat^2*Ycut^5 + 
          3300*m2^6*MBhat^2*Ycut^5 + 11162*MBhat^3*Ycut^5 + 
          168266*m2*MBhat^3*Ycut^5 - 368802*m2^2*MBhat^3*Ycut^5 + 
          112378*m2^3*MBhat^3*Ycut^5 - 35252*m2^4*MBhat^3*Ycut^5 - 
          16632*m2^5*MBhat^3*Ycut^5 + 5400*m2^6*MBhat^3*Ycut^5 + 
          5278*MBhat^4*Ycut^5 - 152796*m2*MBhat^4*Ycut^5 + 
          190239*m2^2*MBhat^4*Ycut^5 - 8701*m2^3*MBhat^4*Ycut^5 - 
          61026*m2^4*MBhat^4*Ycut^5 + 16800*m2^5*MBhat^4*Ycut^5 - 
          11130*MBhat^5*Ycut^5 + 70770*m2*MBhat^5*Ycut^5 + 
          15750*m2^2*MBhat^5*Ycut^5 - 61110*m2^3*MBhat^5*Ycut^5 + 
          17640*m2^4*MBhat^5*Ycut^5 + 3220*MBhat^6*Ycut^5 - 
          6440*m2*MBhat^6*Ycut^5 - 20790*m2^2*MBhat^6*Ycut^5 + 
          6300*m2^3*MBhat^6*Ycut^5 + 91*Ycut^6 + 196*m2*Ycut^6 + 
          1288*m2^2*Ycut^6 - 5740*m2^3*Ycut^6 - 4228*m2^4*Ycut^6 + 
          1988*m2^5*Ycut^6 - 75*m2^6*Ycut^6 + 1904*MBhat*Ycut^6 - 
          15848*m2*MBhat*Ycut^6 + 11620*m2^2*MBhat*Ycut^6 + 
          32088*m2^3*MBhat*Ycut^6 - 14224*m2^4*MBhat*Ycut^6 + 
          3920*m2^5*MBhat*Ycut^6 - 260*m2^6*MBhat*Ycut^6 + 
          2919*MBhat^2*Ycut^6 - 13118*m2*MBhat^2*Ycut^6 + 
          41335*m2^2*MBhat^2*Ycut^6 - 59822*m2^3*MBhat^2*Ycut^6 + 
          5936*m2^4*MBhat^2*Ycut^6 + 4340*m2^5*MBhat^2*Ycut^6 - 
          550*m2^6*MBhat^2*Ycut^6 - 6664*MBhat^3*Ycut^6 + 
          9436*m2*MBhat^3*Ycut^6 + 50134*m2^2*MBhat^3*Ycut^6 - 
          71708*m2^3*MBhat^3*Ycut^6 + 27230*m2^4*MBhat^3*Ycut^6 + 
          2072*m2^5*MBhat^3*Ycut^6 - 900*m2^6*MBhat^3*Ycut^6 - 
          5600*MBhat^4*Ycut^6 + 69034*m2*MBhat^4*Ycut^6 - 
          130697*m2^2*MBhat^4*Ycut^6 + 49952*m2^3*MBhat^4*Ycut^6 + 
          9191*m2^4*MBhat^4*Ycut^6 - 2800*m2^5*MBhat^4*Ycut^6 + 
          9450*MBhat^5*Ycut^6 - 56280*m2*MBhat^5*Ycut^6 + 
          30030*m2^2*MBhat^5*Ycut^6 + 10920*m2^3*MBhat^5*Ycut^6 - 
          2940*m2^4*MBhat^5*Ycut^6 - 2100*MBhat^6*Ycut^6 + 
          6580*m2*MBhat^6*Ycut^6 + 4690*m2^2*MBhat^6*Ycut^6 - 
          1050*m2^3*MBhat^6*Ycut^6 - 415*Ycut^7 + 2511*m2*Ycut^7 - 
          1241*m2^2*Ycut^7 - 4181*m2^3*Ycut^7 + 2721*m2^4*Ycut^7 - 
          205*m2^5*Ycut^7 - 2680*MBhat*Ycut^7 + 13392*m2*MBhat*Ycut^7 - 
          14678*m2^2*MBhat*Ycut^7 + 6490*m2^3*MBhat*Ycut^7 + 
          246*m2^4*MBhat*Ycut^7 - 370*m2^5*MBhat*Ycut^7 + 
          1845*MBhat^2*Ycut^7 + 1537*m2*MBhat^2*Ycut^7 - 29368*m2^2*MBhat^2*
           Ycut^7 + 25050*m2^3*MBhat^2*Ycut^7 - 4084*m2^4*MBhat^2*Ycut^7 - 
          290*m2^5*MBhat^2*Ycut^7 + 4610*MBhat^3*Ycut^7 - 
          21234*m2*MBhat^3*Ycut^7 + 7480*m2^2*MBhat^3*Ycut^7 + 
          18652*m2^3*MBhat^3*Ycut^7 - 8928*m2^4*MBhat^3*Ycut^7 + 
          200*m2^5*MBhat^3*Ycut^7 - 2660*MBhat^4*Ycut^7 - 
          15526*m2*MBhat^4*Ycut^7 + 49077*m2^2*MBhat^4*Ycut^7 - 
          21511*m2^3*MBhat^4*Ycut^7 + 280*m2^4*MBhat^4*Ycut^7 + 
          21840*m2*MBhat^5*Ycut^7 - 16170*m2^2*MBhat^5*Ycut^7 - 
          210*m2^3*MBhat^5*Ycut^7 - 700*MBhat^6*Ycut^7 - 
          2520*m2*MBhat^6*Ycut^7 - 350*m2^2*MBhat^6*Ycut^7 + 710*Ycut^8 - 
          3184*m2*Ycut^8 + 1560*m2^2*Ycut^8 + 844*m2^3*Ycut^8 - 
          110*m2^4*Ycut^8 + 1010*MBhat*Ycut^8 - 5548*m2*MBhat*Ycut^8 + 
          8334*m2^2*MBhat*Ycut^8 - 4076*m2^3*MBhat*Ycut^8 + 
          160*m2^4*MBhat*Ycut^8 - 3765*MBhat^2*Ycut^8 + 9322*m2*MBhat^2*
           Ycut^8 + 1374*m2^2*MBhat^2*Ycut^8 - 3396*m2^3*MBhat^2*Ycut^8 + 
          605*m2^4*MBhat^2*Ycut^8 + 1940*MBhat^3*Ycut^8 + 
          3596*m2*MBhat^3*Ycut^8 - 6564*m2^2*MBhat^3*Ycut^8 - 
          2192*m2^3*MBhat^3*Ycut^8 + 1060*m2^4*MBhat^3*Ycut^8 + 
          1015*MBhat^4*Ycut^8 - 1596*m2*MBhat^4*Ycut^8 - 6069*m2^2*MBhat^4*
           Ycut^8 + 2870*m2^3*MBhat^4*Ycut^8 - 2100*MBhat^5*Ycut^8 - 
          2940*m2*MBhat^5*Ycut^8 + 2310*m2^2*MBhat^5*Ycut^8 + 
          1190*MBhat^6*Ycut^8 + 350*m2*MBhat^6*Ycut^8 - 510*Ycut^9 + 
          1766*m2*Ycut^9 - 664*m2^2*Ycut^9 + 110*m2^3*Ycut^9 + 
          850*MBhat*Ycut^9 - 1198*m2*MBhat*Ycut^9 - 1054*m2^2*MBhat*Ycut^9 + 
          190*m2^3*MBhat*Ycut^9 + 705*MBhat^2*Ycut^9 - 2923*m2*MBhat^2*
           Ycut^9 + 1811*m2^2*MBhat^2*Ycut^9 + 95*m2^3*MBhat^2*Ycut^9 - 
          2410*MBhat^3*Ycut^9 + 1606*m2*MBhat^3*Ycut^9 + 
          502*m2^2*MBhat^3*Ycut^9 - 10*m2^3*MBhat^3*Ycut^9 + 
          1505*MBhat^4*Ycut^9 + 959*m2*MBhat^4*Ycut^9 - 70*m2^2*MBhat^4*
           Ycut^9 + 210*MBhat^5*Ycut^9 - 210*m2*MBhat^5*Ycut^9 - 
          350*MBhat^6*Ycut^9 + 55*Ycut^10 - 216*m2*Ycut^10 + 
          65*m2^2*Ycut^10 - 668*MBhat*Ycut^10 + 864*m2*MBhat*Ycut^10 - 
          190*m2^2*MBhat*Ycut^10 + 1017*MBhat^2*Ycut^10 - 
          226*m2*MBhat^2*Ycut^10 - 95*m2^2*MBhat^2*Ycut^10 - 
          40*MBhat^3*Ycut^10 - 492*m2*MBhat^3*Ycut^10 + 10*m2^2*MBhat^3*
           Ycut^10 - 574*MBhat^4*Ycut^10 + 70*m2*MBhat^4*Ycut^10 + 
          210*MBhat^5*Ycut^10 + 109*Ycut^11 - 65*m2*Ycut^11 - 
          44*MBhat*Ycut^11 - 20*m2*MBhat*Ycut^11 - 309*MBhat^2*Ycut^11 + 
          95*m2*MBhat^2*Ycut^11 + 314*MBhat^3*Ycut^11 - 
          10*m2*MBhat^3*Ycut^11 - 70*MBhat^4*Ycut^11 - 40*Ycut^12 + 
          90*MBhat*Ycut^12 - 60*MBhat^2*Ycut^12 + 10*MBhat^3*Ycut^12))/
        (210*(-1 + Ycut)^6) - 2*m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 
         15*m2^4 - 60*MBhat + 340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 
         96*m2*MBhat^2 - 342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 
         132*m2*MBhat^3 + 108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - 
         m2^2*MBhat^4 - 12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[m2] + 
       2*m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 60*MBhat + 
         340*m2^2*MBhat + 40*m2^3*MBhat + 120*MBhat^2 - 96*m2*MBhat^2 - 
         342*m2^2*MBhat^2 + 2*m2^3*MBhat^2 - 120*MBhat^3 + 132*m2*MBhat^3 + 
         108*m2^2*MBhat^3 + 60*MBhat^4 - 45*m2*MBhat^4 - m2^2*MBhat^4 - 
         12*MBhat^5 - 12*m2*MBhat^5 + 6*m2*MBhat^6)*Log[1 - Ycut] + 
       c[VR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(-460 - 430*m2 + 8270*m2^2 + 
            5470*m2^3 - 230*m2^4 - 20*m2^5 + 2524*MBhat - 2420*m2*MBhat - 
            29120*m2^2*MBhat - 5120*m2^3*MBhat + 580*m2^4*MBhat - 
            44*m2^5*MBhat - 5618*MBhat^2 + 12637*m2*MBhat^2 + 
            32032*m2^2*MBhat^2 - 2348*m2^3*MBhat^2 + 322*m2^4*MBhat^2 - 
            65*m2^5*MBhat^2 + 6384*MBhat^3 - 18792*m2*MBhat^3 - 
            10212*m2^2*MBhat^3 + 1148*m2^3*MBhat^3 + 188*m2^4*MBhat^3 - 
            76*m2^5*MBhat^3 - 3840*MBhat^4 + 11805*m2*MBhat^4 - 
            2355*m2^2*MBhat^4 + 945*m2^3*MBhat^4 - 255*m2^4*MBhat^4 + 
            1140*MBhat^5 - 2940*m2*MBhat^5 + 1380*m2^2*MBhat^5 - 
            300*m2^3*MBhat^5 - 130*MBhat^6 + 140*m2*MBhat^6 - 
            130*m2^2*MBhat^6 + 1960*Ycut + 2280*m2*Ycut - 36550*m2^2*Ycut - 
            25480*m2^3*Ycut + 990*m2^4*Ycut + 100*m2^5*Ycut - 
            10816*MBhat*Ycut + 8604*m2*MBhat*Ycut + 130984*m2^2*MBhat*Ycut + 
            25064*m2^3*MBhat*Ycut - 2856*m2^4*MBhat*Ycut + 
            220*m2^5*MBhat*Ycut + 24272*MBhat^2*Ycut - 52026*m2*MBhat^2*
             Ycut - 147749*m2^2*MBhat^2*Ycut + 10403*m2^3*MBhat^2*Ycut - 
            1545*m2^4*MBhat^2*Ycut + 325*m2^5*MBhat^2*Ycut - 
            27936*MBhat^3*Ycut + 80592*m2*MBhat^3*Ycut + 49800*m2^2*MBhat^3*
             Ycut - 5852*m2^3*MBhat^3*Ycut - 864*m2^4*MBhat^3*Ycut + 
            380*m2^5*MBhat^3*Ycut + 17160*MBhat^4*Ycut - 52320*m2*MBhat^4*
             Ycut + 10005*m2^2*MBhat^4*Ycut - 4470*m2^3*MBhat^4*Ycut + 
            1275*m2^4*MBhat^4*Ycut - 5280*MBhat^5*Ycut + 13620*m2*MBhat^5*
             Ycut - 6600*m2^2*MBhat^5*Ycut + 1500*m2^3*MBhat^5*Ycut + 
            640*MBhat^6*Ycut - 750*m2*MBhat^6*Ycut + 650*m2^2*MBhat^6*Ycut - 
            3180*Ycut^2 - 4650*m2*Ycut^2 + 62000*m2^2*Ycut^2 + 
            46320*m2^3*Ycut^2 - 1590*m2^4*Ycut^2 - 200*m2^5*Ycut^2 + 
            17664*MBhat*Ycut^2 - 10212*m2*MBhat*Ycut^2 - 227228*m2^2*MBhat*
             Ycut^2 - 48564*m2^3*MBhat*Ycut^2 + 5580*m2^4*MBhat*Ycut^2 - 
            440*m2^5*MBhat*Ycut^2 - 40008*MBhat^2*Ycut^2 + 79986*m2*MBhat^2*
             Ycut^2 + 264697*m2^2*MBhat^2*Ycut^2 - 17400*m2^3*MBhat^2*
             Ycut^2 + 2895*m2^4*MBhat^2*Ycut^2 - 650*m2^5*MBhat^2*Ycut^2 + 
            46704*MBhat^3*Ycut^2 - 130944*m2*MBhat^3*Ycut^2 - 
            95784*m2^2*MBhat^3*Ycut^2 + 11964*m2^3*MBhat^3*Ycut^2 + 
            1500*m2^4*MBhat^3*Ycut^2 - 760*m2^5*MBhat^3*Ycut^2 - 
            29340*MBhat^4*Ycut^2 + 88560*m2*MBhat^4*Ycut^2 - 
            15495*m2^2*MBhat^4*Ycut^2 + 8175*m2^3*MBhat^4*Ycut^2 - 
            2550*m2^4*MBhat^4*Ycut^2 + 9360*MBhat^5*Ycut^2 - 
            24300*m2*MBhat^5*Ycut^2 + 12300*m2^2*MBhat^5*Ycut^2 - 
            3000*m2^3*MBhat^5*Ycut^2 - 1200*MBhat^6*Ycut^2 + 
            1560*m2*MBhat^6*Ycut^2 - 1300*m2^2*MBhat^6*Ycut^2 + 2360*Ycut^3 + 
            4460*m2*Ycut^3 - 48740*m2^2*Ycut^3 - 40220*m2^3*Ycut^3 + 
            1090*m2^4*Ycut^3 + 200*m2^5*Ycut^3 - 13216*MBhat*Ycut^3 + 
            3452*m2*MBhat*Ycut^3 + 184224*m2^2*MBhat*Ycut^3 + 
            46060*m2^3*MBhat*Ycut^3 - 5360*m2^4*MBhat*Ycut^3 + 
            440*m2^5*MBhat*Ycut^3 + 30272*MBhat^2*Ycut^3 - 53962*m2*MBhat^2*
             Ycut^3 - 224325*m2^2*MBhat^2*Ycut^3 + 12775*m2^3*MBhat^2*
             Ycut^3 - 2570*m2^4*MBhat^2*Ycut^3 + 650*m2^5*MBhat^2*Ycut^3 - 
            35546*MBhat^3*Ycut^3 + 95200*m2*MBhat^3*Ycut^3 + 
            89956*m2^2*MBhat^3*Ycut^3 - 11980*m2^3*MBhat^3*Ycut^3 - 
            1330*m2^4*MBhat^3*Ycut^3 + 760*m2^5*MBhat^3*Ycut^3 + 
            22260*MBhat^4*Ycut^3 - 67440*m2*MBhat^4*Ycut^3 + 
            9645*m2^2*MBhat^4*Ycut^3 - 7440*m2^3*MBhat^4*Ycut^3 + 
            2550*m2^4*MBhat^4*Ycut^3 - 7050*MBhat^5*Ycut^3 + 
            19920*m2*MBhat^5*Ycut^3 - 11250*m2^2*MBhat^5*Ycut^3 + 
            3000*m2^3*MBhat^5*Ycut^3 + 920*MBhat^6*Ycut^3 - 
            1630*m2*MBhat^6*Ycut^3 + 1300*m2^2*MBhat^6*Ycut^3 - 710*Ycut^4 - 
            1875*m2*Ycut^4 + 15985*m2^2*Ycut^4 + 15665*m2^3*Ycut^4 - 
            195*m2^4*Ycut^4 - 100*m2^5*Ycut^4 + 4024*MBhat*Ycut^4 + 
            1236*m2*MBhat*Ycut^4 - 63540*m2^2*MBhat*Ycut^4 - 
            20680*m2^3*MBhat*Ycut^4 + 2460*m2^4*MBhat*Ycut^4 - 
            220*m2^5*MBhat*Ycut^4 - 9803*MBhat^2*Ycut^4 + 13920*m2*MBhat^2*
             Ycut^4 + 82725*m2^2*MBhat^2*Ycut^4 - 3400*m2^3*MBhat^2*Ycut^4 + 
            1275*m2^4*MBhat^2*Ycut^4 - 325*m2^5*MBhat^2*Ycut^4 + 
            10814*MBhat^3*Ycut^4 - 25116*m2*MBhat^3*Ycut^4 - 
            41360*m2^2*MBhat^3*Ycut^4 + 6760*m2^3*MBhat^3*Ycut^4 + 
            510*m2^4*MBhat^3*Ycut^4 - 380*m2^5*MBhat^3*Ycut^4 - 
            4695*MBhat^4*Ycut^4 + 16635*m2*MBhat^4*Ycut^4 + 
            420*m2^2*MBhat^4*Ycut^4 + 3300*m2^3*MBhat^4*Ycut^4 - 
            1275*m2^4*MBhat^4*Ycut^4 + 300*MBhat^5*Ycut^4 - 
            5730*m2*MBhat^5*Ycut^4 + 5070*m2^2*MBhat^5*Ycut^4 - 
            1500*m2^3*MBhat^5*Ycut^4 + 70*MBhat^6*Ycut^4 + 
            930*m2*MBhat^6*Ycut^4 - 650*m2^2*MBhat^6*Ycut^4 + 24*Ycut^5 + 
            174*m2*Ycut^5 - 761*m2^2*Ycut^5 - 1476*m2^3*Ycut^5 - 
            81*m2^4*Ycut^5 + 20*m2^5*Ycut^5 + 87*MBhat*Ycut^5 - 
            1044*m2*MBhat*Ycut^5 + 3846*m2^2*MBhat*Ycut^5 + 
            3216*m2^3*MBhat*Ycut^5 - 549*m2^4*MBhat*Ycut^5 + 
            44*m2^5*MBhat*Ycut^5 + 1494*MBhat^2*Ycut^5 - 2391*m2*MBhat^2*
             Ycut^5 - 4356*m2^2*MBhat^2*Ycut^5 - 696*m2^3*MBhat^2*Ycut^5 - 
            276*m2^4*MBhat^2*Ycut^5 + 65*m2^5*MBhat^2*Ycut^5 - 
            1485*MBhat^3*Ycut^5 - 534*m2*MBhat^3*Ycut^5 + 8146*m2^2*MBhat^3*
             Ycut^5 - 2634*m2^3*MBhat^3*Ycut^5 - 9*m2^4*MBhat^3*Ycut^5 + 
            76*m2^5*MBhat^3*Ycut^5 - 2580*MBhat^4*Ycut^5 + 
            5985*m2*MBhat^4*Ycut^5 - 3975*m2^2*MBhat^4*Ycut^5 - 
            435*m2^3*MBhat^4*Ycut^5 + 255*m2^4*MBhat^4*Ycut^5 + 
            3060*MBhat^5*Ycut^5 - 1890*m2*MBhat^5*Ycut^5 - 870*m2^2*MBhat^5*
             Ycut^5 + 300*m2^3*MBhat^5*Ycut^5 - 600*MBhat^6*Ycut^5 - 
            300*m2*MBhat^6*Ycut^5 + 130*m2^2*MBhat^6*Ycut^5 - 44*Ycut^6 + 
            130*m2*Ycut^6 - 151*m2^2*Ycut^6 - 287*m2^3*Ycut^6 + 
            52*m2^4*Ycut^6 - 772*MBhat*Ycut^6 + 1379*m2*MBhat*Ycut^6 + 
            35*m2^2*MBhat*Ycut^6 + 121*m2^3*MBhat*Ycut^6 + 
            37*m2^4*MBhat*Ycut^6 - 454*MBhat^2*Ycut^6 + 2165*m2*MBhat^2*
             Ycut^6 - 3361*m2^2*MBhat^2*Ycut^6 + 763*m2^3*MBhat^2*Ycut^6 + 
            7*m2^4*MBhat^2*Ycut^6 + 2240*MBhat^3*Ycut^6 - 1789*m2*MBhat^3*
             Ycut^6 - 423*m2^2*MBhat^3*Ycut^6 + 683*m2^3*MBhat^3*Ycut^6 - 
            31*m2^4*MBhat^3*Ycut^6 + 570*MBhat^4*Ycut^6 - 3585*m2*MBhat^4*
             Ycut^6 + 2040*m2^2*MBhat^4*Ycut^6 - 75*m2^3*MBhat^4*Ycut^6 - 
            1920*MBhat^5*Ycut^6 + 1650*m2*MBhat^5*Ycut^6 - 
            30*m2^2*MBhat^5*Ycut^6 + 380*MBhat^6*Ycut^6 + 50*m2*MBhat^6*
             Ycut^6 + 176*Ycut^7 - 294*m2*Ycut^7 + 35*m2^2*Ycut^7 + 
            8*m2^3*Ycut^7 + 778*MBhat*Ycut^7 - 1353*m2*MBhat*Ycut^7 + 
            872*m2^2*MBhat*Ycut^7 - 97*m2^3*MBhat*Ycut^7 - 
            824*MBhat^2*Ycut^7 + 171*m2*MBhat^2*Ycut^7 + 320*m2^2*MBhat^2*
             Ycut^7 - 97*m2^3*MBhat^2*Ycut^7 - 1160*MBhat^3*Ycut^7 + 
            1491*m2*MBhat^3*Ycut^7 - 132*m2^2*MBhat^3*Ycut^7 - 
            89*m2^3*MBhat^3*Ycut^7 + 720*MBhat^4*Ycut^7 + 315*m2*MBhat^4*
             Ycut^7 - 285*m2^2*MBhat^4*Ycut^7 + 390*MBhat^5*Ycut^7 - 
            330*m2*MBhat^5*Ycut^7 - 80*MBhat^6*Ycut^7 - 234*Ycut^8 + 
            297*m2*Ycut^8 - 88*m2^2*Ycut^8 - 192*MBhat*Ycut^8 + 
            285*m2*MBhat*Ycut^8 - 73*m2^2*MBhat*Ycut^8 + 831*MBhat^2*Ycut^8 - 
            528*m2*MBhat^2*Ycut^8 + 17*m2^2*MBhat^2*Ycut^8 - 
            150*MBhat^3*Ycut^8 - 99*m2*MBhat^3*Ycut^8 + 9*m2^2*MBhat^3*
             Ycut^8 - 255*MBhat^4*Ycut^8 + 45*m2*MBhat^4*Ycut^8 + 
            136*Ycut^9 - 92*m2*Ycut^9 - 137*MBhat*Ycut^9 + 
            73*m2*MBhat*Ycut^9 - 134*MBhat^2*Ycut^9 + 28*m2*MBhat^2*Ycut^9 + 
            135*MBhat^3*Ycut^9 - 9*m2*MBhat^3*Ycut^9 - 28*Ycut^10 + 
            56*MBhat*Ycut^10 - 28*MBhat^2*Ycut^10))/(15*(-1 + Ycut)^5) - 
         4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5 - 
           12*MBhat - 48*m2*MBhat + 300*m2^2*MBhat + 320*m2^3*MBhat + 
           30*MBhat^2 + 39*m2*MBhat^2 - 513*m2^2*MBhat^2 - 190*m2^3*MBhat^2 + 
           18*m2^4*MBhat^2 - 40*MBhat^3 + 24*m2*MBhat^3 + 372*m2^2*MBhat^3 + 
           30*MBhat^4 - 51*m2*MBhat^4 - 102*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 
           12*MBhat^5 + 24*m2*MBhat^5 + 2*MBhat^6 - 3*m2*MBhat^6 + 
           3*m2^2*MBhat^6)*Log[m2] + 4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 
           140*m2^3 - 30*m2^4 + 3*m2^5 - 12*MBhat - 48*m2*MBhat + 
           300*m2^2*MBhat + 320*m2^3*MBhat + 30*MBhat^2 + 39*m2*MBhat^2 - 
           513*m2^2*MBhat^2 - 190*m2^3*MBhat^2 + 18*m2^4*MBhat^2 - 
           40*MBhat^3 + 24*m2*MBhat^3 + 372*m2^2*MBhat^3 + 30*MBhat^4 - 
           51*m2*MBhat^4 - 102*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 12*MBhat^5 + 
           24*m2*MBhat^5 + 2*MBhat^6 - 3*m2*MBhat^6 + 3*m2^2*MBhat^6)*
          Log[1 - Ycut]))))/mb^2 + 
 (rhoD*(-1/420*(-11897 + 66843*m2 + 377475*m2^2 - 207025*m2^3 - 235375*m2^4 + 
        9597*m2^5 + 357*m2^6 + 25*m2^7 + 65872*MBhat - 329616*m2*MBhat - 
        807408*m2^2*MBhat + 932400*m2^3*MBhat + 148400*m2^4*MBhat - 
        9072*m2^5*MBhat - 784*m2^6*MBhat + 208*m2^7*MBhat - 148946*MBhat^2 + 
        641900*m2*MBhat^2 + 353220*m2^2*MBhat^2 - 894810*m2^3*MBhat^2 + 
        45290*m2^4*MBhat^2 + 6636*m2^5*MBhat^2 - 4060*m2^6*MBhat^2 + 
        770*m2^7*MBhat^2 + 174368*MBhat^3 - 617792*m2*MBhat^3 + 
        248640*m2^2*MBhat^3 + 219520*m2^3*MBhat^3 - 34720*m2^4*MBhat^3 + 
        17472*m2^5*MBhat^3 - 9408*m2^6*MBhat^3 + 1920*m2^7*MBhat^3 - 
        112035*MBhat^4 + 308245*m2*MBhat^4 - 225120*m2^2*MBhat^4 + 
        19600*m2^3*MBhat^4 + 21875*m2^4*MBhat^4 - 16485*m2^5*MBhat^4 + 
        3920*m2^6*MBhat^4 + 37968*MBhat^5 - 74480*m2*MBhat^5 + 
        47040*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 - 6160*m2^4*MBhat^5 + 
        2352*m2^5*MBhat^5 - 5390*MBhat^6 + 6160*m2*MBhat^6 - 
        1680*m2^2*MBhat^6 + 560*m2^3*MBhat^6 + 350*m2^4*MBhat^6 + 
        98536*Ycut - 531384*m2*Ycut - 3265500*m2^2*Ycut + 1267700*m2^3*Ycut + 
        1813700*m2^4*Ycut - 70476*m2^5*Ycut - 2856*m2^6*Ycut - 
        200*m2^7*Ycut - 547136*MBhat*Ycut + 2630208*m2*MBhat*Ycut + 
        7245504*m2^2*MBhat*Ycut - 6820800*m2^3*MBhat*Ycut - 
        1187200*m2^4*MBhat*Ycut + 72576*m2^5*MBhat*Ycut + 
        6272*m2^6*MBhat*Ycut - 1664*m2^7*MBhat*Ycut + 1241968*MBhat^2*Ycut - 
        5141920*m2*MBhat^2*Ycut - 3753120*m2^2*MBhat^2*Ycut + 
        6877080*m2^3*MBhat^2*Ycut - 337960*m2^4*MBhat^2*Ycut - 
        53088*m2^5*MBhat^2*Ycut + 32480*m2^6*MBhat^2*Ycut - 
        6160*m2^7*MBhat^2*Ycut - 1462144*MBhat^3*Ycut + 
        4969216*m2*MBhat^3*Ycut - 1505280*m2^2*MBhat^3*Ycut - 
        1756160*m2^3*MBhat^3*Ycut + 277760*m2^4*MBhat^3*Ycut - 
        139776*m2^5*MBhat^3*Ycut + 75264*m2^6*MBhat^3*Ycut - 
        15360*m2^7*MBhat^3*Ycut + 946680*MBhat^4*Ycut - 
        2489480*m2*MBhat^4*Ycut + 1701420*m2^2*MBhat^4*Ycut - 
        138740*m2^3*MBhat^4*Ycut - 175000*m2^4*MBhat^4*Ycut + 
        131880*m2^5*MBhat^4*Ycut - 31360*m2^6*MBhat^4*Ycut - 
        323904*MBhat^5*Ycut + 602560*m2*MBhat^5*Ycut - 376320*m2^2*MBhat^5*
         Ycut + 53760*m2^3*MBhat^5*Ycut + 49280*m2^4*MBhat^5*Ycut - 
        18816*m2^5*MBhat^5*Ycut + 46480*MBhat^6*Ycut - 
        49280*m2*MBhat^6*Ycut + 15960*m2^2*MBhat^6*Ycut - 
        4480*m2^3*MBhat^6*Ycut - 2800*m2^4*MBhat^6*Ycut - 358316*Ycut^2 + 
        1846404*m2*Ycut^2 + 12412050*m2^2*Ycut^2 - 2882950*m2^3*Ycut^2 - 
        6070750*m2^4*Ycut^2 + 221466*m2^5*Ycut^2 + 9996*m2^6*Ycut^2 + 
        700*m2^7*Ycut^2 + 1995616*MBhat*Ycut^2 - 9178848*m2*MBhat*Ycut^2 - 
        28504224*m2^2*MBhat*Ycut^2 + 21319200*m2^3*MBhat*Ycut^2 + 
        4155200*m2^4*MBhat*Ycut^2 - 254016*m2^5*MBhat*Ycut^2 - 
        21952*m2^6*MBhat*Ycut^2 + 5824*m2^7*MBhat*Ycut^2 - 
        4548488*MBhat^2*Ycut^2 + 18023600*m2*MBhat^2*Ycut^2 + 
        16845360*m2^2*MBhat^2*Ycut^2 - 22944180*m2^3*MBhat^2*Ycut^2 + 
        1085420*m2^4*MBhat^2*Ycut^2 + 185808*m2^5*MBhat^2*Ycut^2 - 
        113680*m2^6*MBhat^2*Ycut^2 + 21560*m2^7*MBhat^2*Ycut^2 + 
        5386304*MBhat^3*Ycut^2 - 17499776*m2*MBhat^3*Ycut^2 + 
        3333120*m2^2*MBhat^3*Ycut^2 + 6146560*m2^3*MBhat^3*Ycut^2 - 
        972160*m2^4*MBhat^3*Ycut^2 + 489216*m2^5*MBhat^3*Ycut^2 - 
        263424*m2^6*MBhat^3*Ycut^2 + 53760*m2^7*MBhat^3*Ycut^2 - 
        3514980*MBhat^4*Ycut^2 + 8807260*m2*MBhat^4*Ycut^2 - 
        5556810*m2^2*MBhat^4*Ycut^2 + 413350*m2^3*MBhat^4*Ycut^2 + 
        612500*m2^4*MBhat^4*Ycut^2 - 461580*m2^5*MBhat^4*Ycut^2 + 
        109760*m2^6*MBhat^4*Ycut^2 + 1214304*MBhat^5*Ycut^2 - 
        2135840*m2*MBhat^5*Ycut^2 + 1317120*m2^2*MBhat^5*Ycut^2 - 
        188160*m2^3*MBhat^5*Ycut^2 - 172480*m2^4*MBhat^5*Ycut^2 + 
        65856*m2^5*MBhat^5*Ycut^2 - 176120*MBhat^6*Ycut^2 + 
        172480*m2*MBhat^6*Ycut^2 - 65940*m2^2*MBhat^6*Ycut^2 + 
        15680*m2^3*MBhat^6*Ycut^2 + 9800*m2^4*MBhat^6*Ycut^2 + 
        747992*Ycut^3 - 3661448*m2*Ycut^3 - 27117300*m2^2*Ycut^3 + 
        2139900*m2^3*Ycut^3 + 11494700*m2^4*Ycut^3 - 384132*m2^5*Ycut^3 - 
        19992*m2^6*Ycut^3 - 1400*m2^7*Ycut^3 - 4179392*MBhat*Ycut^3 + 
        18294976*m2*MBhat*Ycut^3 + 64346688*m2^2*MBhat*Ycut^3 - 
        36680000*m2^3*MBhat*Ycut^3 - 8310400*m2^4*MBhat*Ycut^3 + 
        508032*m2^5*MBhat*Ycut^3 + 43904*m2^6*MBhat*Ycut^3 - 
        11648*m2^7*MBhat*Ycut^3 + 9567376*MBhat^2*Ycut^3 - 
        36109920*m2*MBhat^2*Ycut^3 - 42346080*m2^2*MBhat^2*Ycut^3 + 
        43261960*m2^3*MBhat^2*Ycut^3 - 1943480*m2^4*MBhat^2*Ycut^3 - 
        371616*m2^5*MBhat^2*Ycut^3 + 227360*m2^6*MBhat^2*Ycut^3 - 
        43120*m2^7*MBhat^2*Ycut^3 - 11393508*MBhat^3*Ycut^3 + 
        35235032*m2*MBhat^3*Ycut^3 - 2137100*m2^2*MBhat^3*Ycut^3 - 
        12318320*m2^3*MBhat^3*Ycut^3 + 1999620*m2^4*MBhat^3*Ycut^3 - 
        1027432*m2^5*MBhat^3*Ycut^3 + 541548*m2^6*MBhat^3*Ycut^3 - 
        107520*m2^7*MBhat^3*Ycut^3 + 7486920*MBhat^4*Ycut^3 - 
        17807160*m2*MBhat^4*Ycut^3 + 10157700*m2^2*MBhat^4*Ycut^3 - 
        604380*m2^3*MBhat^4*Ycut^3 - 1292200*m2^4*MBhat^4*Ycut^3 + 
        950040*m2^5*MBhat^4*Ycut^3 - 219520*m2^6*MBhat^4*Ycut^3 - 
        2607108*MBhat^5*Ycut^3 + 4319280*m2*MBhat^5*Ycut^3 - 
        2618280*m2^2*MBhat^5*Ycut^3 + 351120*m2^3*MBhat^5*Ycut^3 + 
        359660*m2^4*MBhat^5*Ycut^3 - 131712*m2^5*MBhat^5*Ycut^3 + 
        381360*MBhat^6*Ycut^3 - 342720*m2*MBhat^6*Ycut^3 + 
        153160*m2^2*MBhat^6*Ycut^3 - 29120*m2^3*MBhat^6*Ycut^3 - 
        19600*m2^4*MBhat^6*Ycut^3 - 982030*Ycut^4 + 4529770*m2*Ycut^4 + 
        37336425*m2^2*Ycut^4 + 2764125*m2^3*Ycut^4 - 13398175*m2^4*Ycut^4 + 
        391965*m2^5*Ycut^4 + 24990*m2^6*Ycut^4 + 1750*m2^7*Ycut^4 + 
        5506480*MBhat*Ycut^4 - 22774640*m2*MBhat*Ycut^4 - 
        91440720*m2^2*MBhat*Ycut^4 + 36912400*m2^3*MBhat*Ycut^4 + 
        10388000*m2^4*MBhat*Ycut^4 - 635040*m2^5*MBhat*Ycut^4 - 
        54880*m2^6*MBhat*Ycut^4 + 14560*m2^7*MBhat*Ycut^4 - 
        12671645*MBhat^2*Ycut^4 + 45247755*m2*MBhat^2*Ycut^4 + 
        65896740*m2^2*MBhat^2*Ycut^4 - 50088500*m2^3*MBhat^2*Ycut^4 + 
        1993285*m2^4*MBhat^2*Ycut^4 + 541695*m2^5*MBhat^2*Ycut^4 - 
        306250*m2^6*MBhat^2*Ycut^4 + 53900*m2^7*MBhat^2*Ycut^4 + 
        15155840*MBhat^3*Ycut^4 - 44356480*m2*MBhat^3*Ycut^4 - 
        4122720*m2^2*MBhat^3*Ycut^4 + 15366400*m2^3*MBhat^3*Ycut^4 - 
        2516640*m2^4*MBhat^3*Ycut^4 + 1337280*m2^5*MBhat^3*Ycut^4 - 
        697760*m2^6*MBhat^3*Ycut^4 + 134400*m2^7*MBhat^3*Ycut^4 - 
        9986865*MBhat^4*Ycut^4 + 22446585*m2*MBhat^4*Ycut^4 - 
        11213160*m2^2*MBhat^4*Ycut^4 + 388080*m2^3*MBhat^4*Ycut^4 + 
        1752800*m2^4*MBhat^4*Ycut^4 - 1236690*m2^5*MBhat^4*Ycut^4 + 
        274400*m2^6*MBhat^4*Ycut^4 + 3480960*MBhat^5*Ycut^4 - 
        5409600*m2*MBhat^5*Ycut^4 + 3213840*m2^2*MBhat^5*Ycut^4 - 
        371280*m2^3*MBhat^5*Ycut^4 - 481600*m2^4*MBhat^5*Ycut^4 + 
        164640*m2^5*MBhat^5*Ycut^4 - 509530*MBhat^6*Ycut^4 + 
        416640*m2*MBhat^6*Ycut^4 - 219660*m2^2*MBhat^6*Ycut^4 + 
        31080*m2^3*MBhat^6*Ycut^4 + 24500*m2^4*MBhat^6*Ycut^4 + 
        832664*Ycut^5 - 3576776*m2*Ycut^5 - 33308940*m2^2*Ycut^5 - 
        7650300*m2^3*Ycut^5 + 9748340*m2^4*Ycut^5 - 225372*m2^5*Ycut^5 - 
        19992*m2^6*Ycut^5 - 1400*m2^7*Ycut^5 - 4683854*MBhat*Ycut^5 + 
        18117232*m2*MBhat*Ycut^5 + 84171066*m2^2*MBhat*Ycut^5 - 
        20624240*m2^3*MBhat*Ycut^5 - 8250970*m2^4*MBhat*Ycut^5 + 
        460992*m2^5*MBhat*Ycut^5 + 57134*m2^6*MBhat*Ycut^5 - 
        11648*m2^7*MBhat*Ycut^5 + 10881640*MBhat^2*Ycut^5 - 
        36374520*m2*MBhat^2*Ycut^5 - 65644320*m2^2*MBhat^2*Ycut^5 + 
        36051400*m2^3*MBhat^2*Ycut^5 - 1109360*m2^4*MBhat^2*Ycut^5 - 
        528024*m2^5*MBhat^2*Ycut^5 + 262640*m2^6*MBhat^2*Ycut^5 - 
        43120*m2^7*MBhat^2*Ycut^5 - 13028596*MBhat^3*Ycut^5 + 
        35757708*m2*MBhat^3*Ycut^5 + 10057320*m2^2*MBhat^3*Ycut^5 - 
        12088160*m2^3*MBhat^3*Ycut^5 + 1902740*m2^4*MBhat^3*Ycut^5 - 
        1107372*m2^5*MBhat^3*Ycut^5 + 582904*m2^6*MBhat^3*Ycut^5 - 
        107520*m2^7*MBhat^3*Ycut^5 + 8490720*MBhat^4*Ycut^5 - 
        17937360*m2*MBhat^4*Ycut^5 + 7432740*m2^2*MBhat^4*Ycut^5 + 
        20580*m2^3*MBhat^4*Ycut^5 - 1532440*m2^4*MBhat^4*Ycut^5 + 
        1040760*m2^5*MBhat^4*Ycut^5 - 219520*m2^6*MBhat^4*Ycut^5 - 
        2890566*MBhat^5*Ycut^5 + 4203360*m2*MBhat^5*Ycut^5 - 
        2469180*m2^2*MBhat^5*Ycut^5 + 220080*m2^3*MBhat^5*Ycut^5 + 
        417410*m2^4*MBhat^5*Ycut^5 - 131712*m2^5*MBhat^5*Ycut^5 + 
        412160*MBhat^6*Ycut^5 - 304640*m2*MBhat^6*Ycut^5 + 
        202440*m2^2*MBhat^6*Ycut^5 - 20160*m2^3*MBhat^6*Ycut^5 - 
        19600*m2^4*MBhat^6*Ycut^5 - 448427*Ycut^6 + 1758743*m2*Ycut^6 + 
        18945220*m2^2*Ycut^6 + 7458500*m2^3*Ycut^6 - 4240845*m2^4*Ycut^6 + 
        64421*m2^5*Ycut^6 + 7056*m2^6*Ycut^6 + 700*m2^7*Ycut^6 + 
        2506784*MBhat*Ycut^6 - 8948576*m2*MBhat*Ycut^6 - 
        49463568*m2^2*MBhat*Ycut^6 + 4427360*m2^3*MBhat*Ycut^6 + 
        4019680*m2^4*MBhat*Ycut^6 - 177408*m2^5*MBhat*Ycut^6 - 
        33712*m2^6*MBhat*Ycut^6 + 5824*m2^7*MBhat*Ycut^6 - 
        6005265*MBhat^2*Ycut^6 + 18430027*m2*MBhat^2*Ycut^6 + 
        41414940*m2^2*MBhat^2*Ycut^6 - 15411200*m2^3*MBhat^2*Ycut^6 + 
        274785*m2^4*MBhat^2*Ycut^6 + 319137*m2^5*MBhat^2*Ycut^6 - 
        145432*m2^6*MBhat^2*Ycut^6 + 21560*m2^7*MBhat^2*Ycut^6 + 
        7161728*MBhat^3*Ycut^6 - 18072544*m2*MBhat^3*Ycut^6 - 
        9494240*m2^2*MBhat^3*Ycut^6 + 5761280*m2^3*MBhat^3*Ycut^6 - 
        805280*m2^4*MBhat^3*Ycut^6 + 583296*m2^5*MBhat^3*Ycut^6 - 
        312032*m2^6*MBhat^3*Ycut^6 + 53760*m2^7*MBhat^3*Ycut^6 - 
        4404134*MBhat^4*Ycut^6 + 8672160*m2*MBhat^4*Ycut^6 - 
        2625420*m2^2*MBhat^4*Ycut^6 - 184660*m2^3*MBhat^4*Ycut^6 + 
        853790*m2^4*MBhat^4*Ycut^6 - 562716*m2^5*MBhat^4*Ycut^6 + 
        109760*m2^6*MBhat^4*Ycut^6 + 1321488*MBhat^5*Ycut^6 - 
        1817760*m2*MBhat^5*Ycut^6 + 1122240*m2^2*MBhat^5*Ycut^6 - 
        60480*m2^3*MBhat^5*Ycut^6 - 231280*m2^4*MBhat^5*Ycut^6 + 
        65856*m2^5*MBhat^5*Ycut^6 - 160720*MBhat^6*Ycut^6 + 
        110880*m2*MBhat^6*Ycut^6 - 121100*m2^2*MBhat^6*Ycut^6 + 
        8400*m2^3*MBhat^6*Ycut^6 + 9800*m2^4*MBhat^6*Ycut^6 + 146784*Ycut^7 - 
        499408*m2*Ycut^7 - 6386100*m2^2*Ycut^7 - 3704260*m2^3*Ycut^7 + 
        959700*m2^4*Ycut^7 - 3948*m2^5*Ycut^7 - 1736*m2^6*Ycut^7 - 
        200*m2^7*Ycut^7 - 728998*MBhat*Ycut^7 + 2412256*m2*MBhat*Ycut^7 + 
        17334828*m2^2*MBhat*Ycut^7 + 1243760*m2^3*MBhat*Ycut^7 - 
        1097390*m2^4*MBhat*Ycut^7 + 34272*m2^5*MBhat*Ycut^7 + 
        12152*m2^6*MBhat*Ycut^7 - 1664*m2^7*MBhat*Ycut^7 + 
        2034480*MBhat^2*Ycut^7 - 5504576*m2*MBhat^2*Ycut^7 - 
        15506400*m2^2*MBhat^2*Ycut^7 + 3360280*m2^3*MBhat^2*Ycut^7 + 
        22680*m2^4*MBhat^2*Ycut^7 - 126672*m2^5*MBhat^2*Ycut^7 + 
        49616*m2^6*MBhat^2*Ycut^7 - 6160*m2^7*MBhat^2*Ycut^7 - 
        2452166*MBhat^3*Ycut^7 + 5329184*m2*MBhat^3*Ycut^7 + 
        4604880*m2^2*MBhat^3*Ycut^7 - 1436960*m2^3*MBhat^3*Ycut^7 + 
        112630*m2^4*MBhat^3*Ycut^7 - 172368*m2^5*MBhat^3*Ycut^7 + 
        100912*m2^6*MBhat^3*Ycut^7 - 15360*m2^7*MBhat^3*Ycut^7 + 
        1172752*MBhat^4*Ycut^7 - 2074800*m2*MBhat^4*Ycut^7 + 
        194460*m2^2*MBhat^4*Ycut^7 + 112700*m2^3*MBhat^4*Ycut^7 - 
        285880*m2^4*MBhat^4*Ycut^7 + 184968*m2^5*MBhat^4*Ycut^7 - 
        31360*m2^6*MBhat^4*Ycut^7 - 96768*MBhat^5*Ycut^7 + 
        181440*m2*MBhat^5*Ycut^7 - 226380*m2^2*MBhat^5*Ycut^7 - 
        5040*m2^3*MBhat^5*Ycut^7 + 78680*m2^4*MBhat^5*Ycut^7 - 
        18816*m2^5*MBhat^5*Ycut^7 - 31360*MBhat^6*Ycut^7 + 
        6720*m2*MBhat^6*Ycut^7 + 46200*m2^2*MBhat^6*Ycut^7 - 
        2240*m2^3*MBhat^6*Ycut^7 - 2800*m2^4*MBhat^6*Ycut^7 - 38445*Ycut^8 + 
        86751*m2*Ycut^8 + 1028685*m2^2*Ycut^8 + 866705*m2^3*Ycut^8 - 
        62230*m2^4*Ycut^8 - 5082*m2^5*Ycut^8 + 217*m2^6*Ycut^8 + 
        25*m2^7*Ycut^8 - 2320*MBhat*Ycut^8 - 128912*m2*MBhat*Ycut^8 - 
        2997456*m2^2*MBhat*Ycut^8 - 804160*m2^3*MBhat*Ycut^8 + 
        135520*m2^4*MBhat*Ycut^8 + 672*m2^5*MBhat*Ycut^8 - 
        2464*m2^6*MBhat*Ycut^8 + 208*m2^7*MBhat*Ycut^8 - 
        363300*MBhat^2*Ycut^8 + 824866*m2*MBhat^2*Ycut^8 + 
        2840880*m2^2*MBhat^2*Ycut^8 - 173810*m2^3*MBhat^2*Ycut^8 - 
        39795*m2^4*MBhat^2*Ycut^8 + 28749*m2^5*MBhat^2*Ycut^8 - 
        9226*m2^6*MBhat^2*Ycut^8 + 770*m2^7*MBhat^2*Ycut^8 + 
        580240*MBhat^3*Ycut^8 - 819168*m2*MBhat^3*Ycut^8 - 
        1024800*m2^2*MBhat^3*Ycut^8 + 51520*m2^3*MBhat^3*Ycut^8 + 
        66640*m2^4*MBhat^3*Ycut^8 + 16128*m2^5*MBhat^3*Ycut^8 - 
        17024*m2^6*MBhat^3*Ycut^8 + 1920*m2^7*MBhat^3*Ycut^8 - 
        49637*MBhat^4*Ycut^8 - 37065*m2*MBhat^4*Ycut^8 + 
        197400*m2^2*MBhat^4*Ycut^8 - 26320*m2^3*MBhat^4*Ycut^8 + 
        43925*m2^4*MBhat^4*Ycut^8 - 32193*m2^5*MBhat^4*Ycut^8 + 
        3920*m2^6*MBhat^4*Ycut^8 - 249984*MBhat^5*Ycut^8 + 
        233520*m2*MBhat^5*Ycut^8 - 35280*m2^2*MBhat^5*Ycut^8 + 
        8400*m2^3*MBhat^5*Ycut^8 - 14560*m2^4*MBhat^5*Ycut^8 + 
        2352*m2^5*MBhat^5*Ycut^8 + 72590*MBhat^6*Ycut^8 - 
        24080*m2*MBhat^6*Ycut^8 - 10500*m2^2*MBhat^6*Ycut^8 + 
        280*m2^3*MBhat^6*Ycut^8 + 350*m2^4*MBhat^6*Ycut^8 + 31976*Ycut^9 - 
        42280*m2*Ycut^9 - 14840*m2^2*Ycut^9 - 42840*m2^3*Ycut^9 - 
        10360*m2^4*Ycut^9 + 1624*m2^5*Ycut^9 + 115108*MBhat*Ycut^9 - 
        143360*m2*MBhat*Ycut^9 + 112560*m2^2*MBhat*Ycut^9 + 
        82320*m2^3*MBhat*Ycut^9 + 1330*m2^4*MBhat*Ycut^9 - 
        1008*m2^5*MBhat*Ycut^9 + 210*m2^6*MBhat*Ycut^9 - 
        29680*MBhat^2*Ycut^9 - 20272*m2*MBhat^2*Ycut^9 - 
        80640*m2^2*MBhat^2*Ycut^9 - 39200*m2^3*MBhat^2*Ycut^9 + 
        9240*m2^4*MBhat^2*Ycut^9 - 2520*m2^5*MBhat^2*Ycut^9 + 
        672*m2^6*MBhat^2*Ycut^9 - 168784*MBhat^3*Ycut^9 + 
        102760*m2*MBhat^3*Ycut^9 + 13720*m2^2*MBhat^3*Ycut^9 + 
        72800*m2^3*MBhat^3*Ycut^9 - 36960*m2^4*MBhat^3*Ycut^9 + 
        4564*m2^5*MBhat^3*Ycut^9 + 980*m2^6*MBhat^3*Ycut^9 - 
        15176*MBhat^4*Ycut^9 + 132440*m2*MBhat^4*Ycut^9 - 
        72240*m2^2*MBhat^4*Ycut^9 - 1680*m2^3*MBhat^4*Ycut^9 + 
        1680*m2^4*MBhat^4*Ycut^9 + 2016*m2^5*MBhat^4*Ycut^9 + 
        145740*MBhat^5*Ycut^9 - 128800*m2*MBhat^5*Ycut^9 + 
        31500*m2^2*MBhat^5*Ycut^9 - 1680*m2^3*MBhat^5*Ycut^9 + 
        1050*m2^4*MBhat^5*Ycut^9 - 37520*MBhat^6*Ycut^9 + 
        8960*m2*MBhat^6*Ycut^9 + 1120*m2^2*MBhat^6*Ycut^9 - 35434*Ycut^10 + 
        37058*m2*Ycut^10 - 7350*m2^2*Ycut^10 - 9310*m2^3*Ycut^10 + 
        1295*m2^4*Ycut^10 - 63*m2^5*Ycut^10 - 62048*MBhat*Ycut^10 + 
        59584*m2*MBhat*Ycut^10 - 4368*m2^2*MBhat*Ycut^10 + 
        13440*m2^3*MBhat*Ycut^10 - 2240*m2^4*MBhat*Ycut^10 + 
        73766*MBhat^2*Ycut^10 - 29890*m2*MBhat^2*Ycut^10 - 
        14700*m2^2*MBhat^2*Ycut^10 - 140*m2^3*MBhat^2*Ycut^10 - 
        105*m2^4*MBhat^2*Ycut^10 - 105*m2^5*MBhat^2*Ycut^10 + 
        49616*MBhat^3*Ycut^10 - 31136*m2*MBhat^3*Ycut^10 + 
        30240*m2^2*MBhat^3*Ycut^10 - 20160*m2^3*MBhat^3*Ycut^10 + 
        6720*m2^4*MBhat^3*Ycut^10 - 1008*m2^5*MBhat^3*Ycut^10 - 
        23240*MBhat^4*Ycut^10 - 20860*m2*MBhat^4*Ycut^10 + 
        9030*m2^2*MBhat^4*Ycut^10 + 1470*m2^3*MBhat^4*Ycut^10 - 
        1050*m2^4*MBhat^4*Ycut^10 - 35952*MBhat^5*Ycut^10 + 
        29120*m2*MBhat^5*Ycut^10 - 6720*m2^2*MBhat^5*Ycut^10 + 
        8680*MBhat^6*Ycut^10 - 1120*m2*MBhat^6*Ycut^10 + 25256*Ycut^11 - 
        18984*m2*Ycut^11 + 840*m2^2*Ycut^11 - 280*m2^3*Ycut^11 + 
        12292*MBhat*Ycut^11 - 9632*m2*MBhat*Ycut^11 + 
        6804*m2^2*MBhat*Ycut^11 - 1680*m2^3*MBhat*Ycut^11 + 
        70*m2^4*MBhat*Ycut^11 - 40768*MBhat^2*Ycut^11 + 
        15008*m2*MBhat^2*Ycut^11 - 6720*m2^2*MBhat^2*Ycut^11 + 
        1120*m2^3*MBhat^2*Ycut^11 + 56*MBhat^3*Ycut^11 + 
        2408*m2*MBhat^3*Ycut^11 - 3780*m2^2*MBhat^3*Ycut^11 + 
        1680*m2^3*MBhat^3*Ycut^11 - 350*m2^4*MBhat^3*Ycut^11 + 
        10024*MBhat^4*Ycut^11 - 280*m2*MBhat^4*Ycut^11 + 
        4116*MBhat^5*Ycut^11 - 2800*m2*MBhat^5*Ycut^11 + 
        420*m2^2*MBhat^5*Ycut^11 - 560*MBhat^6*Ycut^11 - 11228*Ycut^12 + 
        5404*m2*Ycut^12 - 665*m2^2*Ycut^12 + 35*m2^3*Ycut^12 + 
        3472*MBhat*Ycut^12 - 1008*m2*MBhat*Ycut^12 + 336*m2^2*MBhat*Ycut^12 + 
        9443*MBhat^2*Ycut^12 - 2009*m2*MBhat^2*Ycut^12 + 
        840*m2^2*MBhat^2*Ycut^12 - 3248*MBhat^3*Ycut^12 + 
        672*m2*MBhat^3*Ycut^12 - 1127*MBhat^4*Ycut^12 + 
        315*m2*MBhat^4*Ycut^12 - 336*MBhat^5*Ycut^12 - 70*MBhat^6*Ycut^12 + 
        2968*Ycut^13 - 728*m2*Ycut^13 - 2198*MBhat*Ycut^13 + 
        336*m2*MBhat*Ycut^13 - 42*m2^2*MBhat*Ycut^13 - 616*MBhat^2*Ycut^13 - 
        56*m2*MBhat^2*Ycut^13 + 308*MBhat^3*Ycut^13 - 84*m2*MBhat^3*Ycut^13 + 
        112*MBhat^4*Ycut^13 + 42*MBhat^5*Ycut^13 - 431*Ycut^14 + 
        35*m2*Ycut^14 + 352*MBhat*Ycut^14 + 35*MBhat^2*Ycut^14 + 
        7*m2*MBhat^2*Ycut^14 - 16*MBhat^3*Ycut^14 - 14*MBhat^4*Ycut^14 + 
        32*Ycut^15 - 30*MBhat*Ycut^15 + 2*MBhat^3*Ycut^15)/(-1 + Ycut)^8 + 
     (8 + 8*m2 - 585*m2^2 - 925*m2^3 - 165*m2^4 + 15*m2^5 - 48*MBhat - 
       16*m2*MBhat + 1872*m2^2*MBhat + 1520*m2^3*MBhat + 120*MBhat^2 - 
       16*m2*MBhat^2 - 2208*m2^2*MBhat^2 - 670*m2^3*MBhat^2 + 
       58*m2^4*MBhat^2 - 160*MBhat^3 + 64*m2*MBhat^3 + 1152*m2^2*MBhat^3 + 
       120*MBhat^4 - 56*m2*MBhat^4 - 237*m2^2*MBhat^4 + 43*m2^3*MBhat^4 - 
       48*MBhat^5 + 16*m2*MBhat^5 + 8*MBhat^6 + 6*m2^2*MBhat^6)*Log[m2] + 
     (-8 - 8*m2 + 585*m2^2 + 925*m2^3 + 165*m2^4 - 15*m2^5 + 48*MBhat + 
       16*m2*MBhat - 1872*m2^2*MBhat - 1520*m2^3*MBhat - 120*MBhat^2 + 
       16*m2*MBhat^2 + 2208*m2^2*MBhat^2 + 670*m2^3*MBhat^2 - 
       58*m2^4*MBhat^2 + 160*MBhat^3 - 64*m2*MBhat^3 - 1152*m2^2*MBhat^3 - 
       120*MBhat^4 + 56*m2*MBhat^4 + 237*m2^2*MBhat^4 - 43*m2^3*MBhat^4 + 
       48*MBhat^5 - 16*m2*MBhat^5 - 8*MBhat^6 - 6*m2^2*MBhat^6)*
      Log[1 - Ycut] + c[VR]^2*(-1/420*(-11897 + 66843*m2 + 377475*m2^2 - 
          207025*m2^3 - 235375*m2^4 + 9597*m2^5 + 357*m2^6 + 25*m2^7 + 
          65872*MBhat - 329616*m2*MBhat - 807408*m2^2*MBhat + 
          932400*m2^3*MBhat + 148400*m2^4*MBhat - 9072*m2^5*MBhat - 
          784*m2^6*MBhat + 208*m2^7*MBhat - 148946*MBhat^2 + 
          641900*m2*MBhat^2 + 353220*m2^2*MBhat^2 - 894810*m2^3*MBhat^2 + 
          45290*m2^4*MBhat^2 + 6636*m2^5*MBhat^2 - 4060*m2^6*MBhat^2 + 
          770*m2^7*MBhat^2 + 174368*MBhat^3 - 617792*m2*MBhat^3 + 
          248640*m2^2*MBhat^3 + 219520*m2^3*MBhat^3 - 34720*m2^4*MBhat^3 + 
          17472*m2^5*MBhat^3 - 9408*m2^6*MBhat^3 + 1920*m2^7*MBhat^3 - 
          112035*MBhat^4 + 308245*m2*MBhat^4 - 225120*m2^2*MBhat^4 + 
          19600*m2^3*MBhat^4 + 21875*m2^4*MBhat^4 - 16485*m2^5*MBhat^4 + 
          3920*m2^6*MBhat^4 + 37968*MBhat^5 - 74480*m2*MBhat^5 + 
          47040*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 - 6160*m2^4*MBhat^5 + 
          2352*m2^5*MBhat^5 - 5390*MBhat^6 + 6160*m2*MBhat^6 - 
          1680*m2^2*MBhat^6 + 560*m2^3*MBhat^6 + 350*m2^4*MBhat^6 + 
          74742*Ycut - 397698*m2*Ycut - 2510550*m2^2*Ycut + 
          853650*m2^3*Ycut + 1342950*m2^4*Ycut - 51282*m2^5*Ycut - 
          2142*m2^6*Ycut - 150*m2^7*Ycut - 415392*MBhat*Ycut + 
          1970976*m2*MBhat*Ycut + 5630688*m2^2*MBhat*Ycut - 
          4956000*m2^3*MBhat*Ycut - 890400*m2^4*MBhat*Ycut + 
          54432*m2^5*MBhat*Ycut + 4704*m2^6*MBhat*Ycut - 
          1248*m2^7*MBhat*Ycut + 944076*MBhat^2*Ycut - 3858120*m2*MBhat^2*
           Ycut - 3046680*m2^2*MBhat^2*Ycut + 5087460*m2^3*MBhat^2*Ycut - 
          247380*m2^4*MBhat^2*Ycut - 39816*m2^5*MBhat^2*Ycut + 
          24360*m2^6*MBhat^2*Ycut - 4620*m2^7*MBhat^2*Ycut - 
          1113408*MBhat^3*Ycut + 3733632*m2*MBhat^3*Ycut - 
          1008000*m2^2*MBhat^3*Ycut - 1317120*m2^3*MBhat^3*Ycut + 
          208320*m2^4*MBhat^3*Ycut - 104832*m2^5*MBhat^3*Ycut + 
          56448*m2^6*MBhat^3*Ycut - 11520*m2^7*MBhat^3*Ycut + 
          722610*MBhat^4*Ycut - 1872990*m2*MBhat^4*Ycut + 
          1251180*m2^2*MBhat^4*Ycut - 99540*m2^3*MBhat^4*Ycut - 
          131250*m2^4*MBhat^4*Ycut + 98910*m2^5*MBhat^4*Ycut - 
          23520*m2^6*MBhat^4*Ycut - 247968*MBhat^5*Ycut + 
          453600*m2*MBhat^5*Ycut - 282240*m2^2*MBhat^5*Ycut + 
          40320*m2^3*MBhat^5*Ycut + 36960*m2^4*MBhat^5*Ycut - 
          14112*m2^5*MBhat^5*Ycut + 35700*MBhat^6*Ycut - 
          36960*m2*MBhat^6*Ycut + 12600*m2^2*MBhat^6*Ycut - 
          3360*m2^3*MBhat^6*Ycut - 2100*m2^4*MBhat^6*Ycut - 196935*Ycut^2 + 
          984165*m2*Ycut^2 + 7013475*m2^2*Ycut^2 - 968625*m2^3*Ycut^2 - 
          3149475*m2^4*Ycut^2 + 109305*m2^5*Ycut^2 + 5355*m2^6*Ycut^2 + 
          375*m2^7*Ycut^2 + 1098960*MBhat*Ycut^2 - 4907280*m2*MBhat*Ycut^2 - 
          16435440*m2^2*MBhat*Ycut^2 + 10474800*m2^3*MBhat*Ycut^2 + 
          2226000*m2^4*MBhat*Ycut^2 - 136080*m2^5*MBhat*Ycut^2 - 
          11760*m2^6*MBhat*Ycut^2 + 3120*m2^7*MBhat*Ycut^2 - 
          2511390*MBhat^2*Ycut^2 + 9665460*m2*MBhat^2*Ycut^2 + 
          10398780*m2^2*MBhat^2*Ycut^2 - 11874450*m2^3*MBhat^2*Ycut^2 + 
          545370*m2^4*MBhat^2*Ycut^2 + 99540*m2^5*MBhat^2*Ycut^2 - 
          60900*m2^6*MBhat^2*Ycut^2 + 11550*m2^7*MBhat^2*Ycut^2 + 
          2985120*MBhat^3*Ycut^2 - 9414720*m2*MBhat^3*Ycut^2 + 
          1068480*m2^2*MBhat^3*Ycut^2 + 3292800*m2^3*MBhat^3*Ycut^2 - 
          520800*m2^4*MBhat^3*Ycut^2 + 262080*m2^5*MBhat^3*Ycut^2 - 
          141120*m2^6*MBhat^3*Ycut^2 + 28800*m2^7*MBhat^3*Ycut^2 - 
          1957725*MBhat^4*Ycut^2 + 4753035*m2*MBhat^4*Ycut^2 - 
          2829330*m2^2*MBhat^4*Ycut^2 + 194670*m2^3*MBhat^4*Ycut^2 + 
          328125*m2^4*MBhat^4*Ycut^2 - 247275*m2^5*MBhat^4*Ycut^2 + 
          58800*m2^6*MBhat^4*Ycut^2 + 680400*MBhat^5*Ycut^2 - 
          1154160*m2*MBhat^5*Ycut^2 + 705600*m2^2*MBhat^5*Ycut^2 - 
          100800*m2^3*MBhat^5*Ycut^2 - 92400*m2^4*MBhat^5*Ycut^2 + 
          35280*m2^5*MBhat^5*Ycut^2 - 99330*MBhat^6*Ycut^2 + 
          92400*m2*MBhat^6*Ycut^2 - 39060*m2^2*MBhat^6*Ycut^2 + 
          8400*m2^3*MBhat^6*Ycut^2 + 5250*m2^4*MBhat^6*Ycut^2 + 
          279380*Ycut^3 - 1295420*m2*Ycut^3 - 10579800*m2^2*Ycut^3 - 
          651000*m2^3*Ycut^3 + 3852800*m2^4*Ycut^3 - 114240*m2^5*Ycut^3 - 
          7140*m2^6*Ycut^3 - 500*m2^7*Ycut^3 - 1566080*MBhat*Ycut^3 + 
          6509440*m2*MBhat*Ycut^3 + 25845120*m2^2*MBhat*Ycut^3 - 
          10774400*m2^3*MBhat*Ycut^3 - 2968000*m2^4*MBhat*Ycut^3 + 
          181440*m2^5*MBhat*Ycut^3 + 15680*m2^6*MBhat*Ycut^3 - 
          4160*m2^7*MBhat*Ycut^3 + 3600520*MBhat^2*Ycut^3 - 
          12920880*m2*MBhat^2*Ycut^3 - 18501840*m2^2*MBhat^2*Ycut^3 + 
          14425600*m2^3*MBhat^2*Ycut^3 - 605360*m2^4*MBhat^2*Ycut^3 - 
          132720*m2^5*MBhat^2*Ycut^3 + 81200*m2^6*MBhat^2*Ycut^3 - 
          15400*m2^7*MBhat^2*Ycut^3 - 4304960*MBhat^3*Ycut^3 + 
          12662160*m2*MBhat^3*Ycut^3 + 983360*m2^2*MBhat^3*Ycut^3 - 
          4317600*m2^3*MBhat^3*Ycut^3 + 627200*m2^4*MBhat^3*Ycut^3 - 
          329840*m2^5*MBhat^3*Ycut^3 + 188160*m2^6*MBhat^3*Ycut^3 - 
          38400*m2^7*MBhat^3*Ycut^3 + 2842140*MBhat^4*Ycut^3 - 
          6428100*m2*MBhat^4*Ycut^3 + 3315060*m2^2*MBhat^4*Ycut^3 - 
          249900*m2^3*MBhat^4*Ycut^3 - 403900*m2^4*MBhat^4*Ycut^3 + 
          329700*m2^5*MBhat^4*Ycut^3 - 78400*m2^6*MBhat^4*Ycut^3 - 
          996240*MBhat^5*Ycut^3 + 1565760*m2*MBhat^5*Ycut^3 - 
          962640*m2^2*MBhat^5*Ycut^3 + 151200*m2^3*MBhat^5*Ycut^3 + 
          123200*m2^4*MBhat^5*Ycut^3 - 47040*m2^5*MBhat^5*Ycut^3 + 
          147000*MBhat^6*Ycut^3 - 123200*m2*MBhat^6*Ycut^3 + 
          66920*m2^2*MBhat^6*Ycut^3 - 11200*m2^3*MBhat^6*Ycut^3 - 
          7000*m2^4*MBhat^6*Ycut^3 - 226335*Ycut^4 + 954765*m2*Ycut^4 + 
          9163350*m2^2*Ycut^4 + 2430750*m2^3*Ycut^4 - 2543100*m2^4*Ycut^4 + 
          54180*m2^5*Ycut^4 + 5355*m2^6*Ycut^4 + 375*m2^7*Ycut^4 + 
          1275360*MBhat*Ycut^4 - 4848480*m2*MBhat*Ycut^4 - 
          23315040*m2^2*MBhat*Ycut^4 + 4888800*m2^3*MBhat*Ycut^4 + 
          2226000*m2^4*MBhat*Ycut^4 - 136080*m2^5*MBhat*Ycut^4 - 
          11760*m2^6*MBhat*Ycut^4 + 3120*m2^7*MBhat*Ycut^4 - 
          2966565*MBhat^2*Ycut^4 + 9755235*m2*MBhat^2*Ycut^4 + 
          18531030*m2^2*MBhat^2*Ycut^4 - 9509850*m2^3*MBhat^2*Ycut^4 + 
          420945*m2^4*MBhat^2*Ycut^4 + 73815*m2^5*MBhat^2*Ycut^4 - 
          60900*m2^6*MBhat^2*Ycut^4 + 11550*m2^7*MBhat^2*Ycut^4 + 
          3529440*MBhat^3*Ycut^4 - 9539040*m2*MBhat^3*Ycut^4 - 
          3185280*m2^2*MBhat^3*Ycut^4 + 3124800*m2^3*MBhat^3*Ycut^4 - 
          349440*m2^4*MBhat^3*Ycut^4 + 211680*m2^5*MBhat^3*Ycut^4 - 
          141120*m2^6*MBhat^3*Ycut^4 + 28800*m2^7*MBhat^3*Ycut^4 - 
          2292150*MBhat^4*Ycut^4 + 4818450*m2*MBhat^4*Ycut^4 - 
          2096010*m2^2*MBhat^4*Ycut^4 + 306810*m2^3*MBhat^4*Ycut^4 + 
          229425*m2^4*MBhat^4*Ycut^4 - 247275*m2^5*MBhat^4*Ycut^4 + 
          58800*m2^6*MBhat^4*Ycut^4 + 791280*MBhat^5*Ycut^4 - 
          1177680*m2*MBhat^5*Ycut^4 + 791280*m2^2*MBhat^5*Ycut^4 - 
          156240*m2^3*MBhat^5*Ycut^4 - 92400*m2^4*MBhat^5*Ycut^4 + 
          35280*m2^5*MBhat^5*Ycut^4 - 116340*MBhat^6*Ycut^4 + 
          92400*m2*MBhat^6*Ycut^4 - 69300*m2^2*MBhat^6*Ycut^4 + 
          8400*m2^3*MBhat^6*Ycut^4 + 5250*m2^4*MBhat^6*Ycut^4 + 
          100614*Ycut^5 - 371826*m2*Ycut^5 - 4402440*m2^2*Ycut^5 - 
          2137800*m2^3*Ycut^5 + 809340*m2^4*Ycut^5 - 2772*m2^5*Ycut^5 - 
          2142*m2^6*Ycut^5 - 150*m2^7*Ycut^5 - 562644*MBhat*Ycut^5 + 
          1902012*m2*MBhat*Ycut^5 + 11673816*m2^2*MBhat*Ycut^5 + 
          15960*m2^3*MBhat*Ycut^5 - 941220*m2^4*MBhat*Ycut^5 + 
          69132*m2^5*MBhat*Ycut^5 + 4704*m2^6*MBhat*Ycut^5 - 
          1248*m2^7*MBhat*Ycut^5 + 1404102*MBhat^2*Ycut^5 - 
          4060770*m2*MBhat^2*Ycut^5 - 10206420*m2^2*MBhat^2*Ycut^5 + 
          3163860*m2^3*MBhat^2*Ycut^5 - 245070*m2^4*MBhat^2*Ycut^5 + 
          294*m2^5*MBhat^2*Ycut^5 + 24360*m2^6*MBhat^2*Ycut^5 - 
          4620*m2^7*MBhat^2*Ycut^5 - 1591536*MBhat^3*Ycut^5 + 
          3766644*m2*MBhat^3*Ycut^5 + 2877000*m2^2*MBhat^3*Ycut^5 - 
          1212960*m2^3*MBhat^3*Ycut^5 + 21000*m2^4*MBhat^3*Ycut^5 - 
          46452*m2^5*MBhat^3*Ycut^5 + 56448*m2^6*MBhat^3*Ycut^5 - 
          11520*m2^7*MBhat^3*Ycut^5 + 884520*MBhat^4*Ycut^5 - 
          1754760*m2*MBhat^4*Ycut^5 + 648060*m2^2*MBhat^4*Ycut^5 - 
          312060*m2^3*MBhat^4*Ycut^5 - 16170*m2^4*MBhat^4*Ycut^5 + 
          98910*m2^5*MBhat^4*Ycut^5 - 23520*m2^6*MBhat^4*Ycut^5 - 
          252756*MBhat^5*Ycut^5 + 430080*m2*MBhat^5*Ycut^5 - 
          410340*m2^2*MBhat^5*Ycut^5 + 108360*m2^3*MBhat^5*Ycut^5 + 
          36960*m2^4*MBhat^5*Ycut^5 - 14112*m2^5*MBhat^5*Ycut^5 + 
          33600*MBhat^6*Ycut^5 - 36960*m2*MBhat^6*Ycut^5 + 
          42840*m2^2*MBhat^6*Ycut^5 - 3360*m2^3*MBhat^6*Ycut^5 - 
          2100*m2^4*MBhat^6*Ycut^5 - 21844*Ycut^6 + 62286*m2*Ycut^6 + 
          981890*m2^2*Ycut^6 + 732550*m2^3*Ycut^6 - 54565*m2^4*Ycut^6 - 
          9023*m2^5*Ycut^6 + 357*m2^6*Ycut^6 + 25*m2^7*Ycut^6 + 
          70184*MBhat*Ycut^6 - 225512*m2*MBhat*Ycut^6 - 2711856*m2^2*MBhat*
           Ycut^6 - 772240*m2^3*MBhat*Ycut^6 + 238840*m2^4*MBhat*Ycut^6 - 
          23352*m2^5*MBhat*Ycut^6 - 784*m2^6*MBhat*Ycut^6 + 
          208*m2^7*MBhat*Ycut^6 - 413322*MBhat^2*Ycut^6 + 
          947800*m2*MBhat^2*Ycut^6 + 2552970*m2^2*MBhat^2*Ycut^6 - 
          390110*m2^3*MBhat^2*Ycut^6 + 118650*m2^4*MBhat^2*Ycut^6 - 
          17934*m2^5*MBhat^2*Ycut^6 - 4060*m2^6*MBhat^2*Ycut^6 + 
          770*m2^7*MBhat^2*Ycut^6 + 404416*MBhat^3*Ycut^6 - 
          595224*m2*MBhat^3*Ycut^6 - 1176560*m2^2*MBhat^3*Ycut^6 + 
          217280*m2^3*MBhat^3*Ycut^6 + 90160*m2^4*MBhat^3*Ycut^6 - 
          19208*m2^5*MBhat^3*Ycut^6 - 9408*m2^6*MBhat^3*Ycut^6 + 
          1920*m2^7*MBhat^3*Ycut^6 + 3850*MBhat^4*Ycut^6 + 
          33390*m2*MBhat^4*Ycut^6 - 50190*m2^2*MBhat^4*Ycut^6 + 
          201530*m2^3*MBhat^4*Ycut^6 - 45325*m2^4*MBhat^4*Ycut^6 - 
          16485*m2^5*MBhat^4*Ycut^6 + 3920*m2^6*MBhat^4*Ycut^6 - 
          94584*MBhat^5*Ycut^6 - 8400*m2*MBhat^5*Ycut^6 + 
          135240*m2^2*MBhat^5*Ycut^6 - 43680*m2^3*MBhat^5*Ycut^6 - 
          6160*m2^4*MBhat^5*Ycut^6 + 2352*m2^5*MBhat^5*Ycut^6 + 
          18900*MBhat^6*Ycut^6 + 6160*m2*MBhat^6*Ycut^6 - 
          13580*m2^2*MBhat^6*Ycut^6 + 560*m2^3*MBhat^6*Ycut^6 + 
          350*m2^4*MBhat^6*Ycut^6 + 10770*Ycut^7 - 18690*m2*Ycut^7 - 
          41160*m2^2*Ycut^7 - 25200*m2^3*Ycut^7 - 27090*m2^4*Ycut^7 + 
          2730*m2^5*Ycut^7 + 102450*MBhat*Ycut^7 - 181020*m2*MBhat*Ycut^7 + 
          119700*m2^2*MBhat*Ycut^7 + 199080*m2^3*MBhat*Ycut^7 - 
          44310*m2^4*MBhat*Ycut^7 + 3780*m2^5*MBhat*Ycut^7 + 
          128310*MBhat^2*Ycut^7 - 268590*m2*MBhat^2*Ycut^7 + 
          15960*m2^2*MBhat^2*Ycut^7 - 5880*m2^3*MBhat^2*Ycut^7 - 
          39270*m2^4*MBhat^2*Ycut^7 + 6510*m2^5*MBhat^2*Ycut^7 - 
          183090*MBhat^3*Ycut^7 + 55020*m2*MBhat^3*Ycut^7 + 
          204960*m2^2*MBhat^3*Ycut^7 + 1680*m2^3*MBhat^3*Ycut^7 - 
          51870*m2^4*MBhat^3*Ycut^7 + 12180*m2^5*MBhat^3*Ycut^7 - 
          114660*MBhat^4*Ycut^7 + 200340*m2*MBhat^4*Ycut^7 - 
          16380*m2^2*MBhat^4*Ycut^7 - 72660*m2^3*MBhat^4*Ycut^7 + 
          19320*m2^4*MBhat^4*Ycut^7 + 119700*MBhat^5*Ycut^7 - 
          47040*m2*MBhat^5*Ycut^7 - 25200*m2^2*MBhat^5*Ycut^7 + 
          7560*m2^3*MBhat^5*Ycut^7 - 19320*MBhat^6*Ycut^7 + 
          840*m2^2*MBhat^6*Ycut^7 - 26085*Ycut^8 + 40845*m2*Ycut^8 - 
          1785*m2^2*Ycut^8 - 29925*m2^3*Ycut^8 + 4515*m2^4*Ycut^8 + 
          105*m2^5*Ycut^8 - 130020*MBhat*Ycut^8 + 188160*m2*MBhat*Ycut^8 - 
          27720*m2^2*MBhat*Ycut^8 - 10080*m2^3*MBhat*Ycut^8 + 
          4620*m2^4*MBhat*Ycut^8 - 16170*MBhat^2*Ycut^8 + 
          109830*m2*MBhat^2*Ycut^8 - 108570*m2^2*MBhat^2*Ycut^8 - 
          2940*m2^3*MBhat^2*Ycut^8 + 6825*m2^4*MBhat^2*Ycut^8 - 
          525*m2^5*MBhat^2*Ycut^8 + 154380*MBhat^3*Ycut^8 - 
          78960*m2*MBhat^3*Ycut^8 - 15120*m2^2*MBhat^3*Ycut^8 - 
          10080*m2^3*MBhat^3*Ycut^8 + 10500*m2^4*MBhat^3*Ycut^8 - 
          1680*m2^5*MBhat^3*Ycut^8 + 16485*MBhat^4*Ycut^8 - 
          68775*m2*MBhat^4*Ycut^8 + 2730*m2^2*MBhat^4*Ycut^8 + 
          11550*m2^3*MBhat^4*Ycut^8 - 2100*m2^4*MBhat^4*Ycut^8 - 
          45360*MBhat^5*Ycut^8 + 13440*m2*MBhat^5*Ycut^8 + 
          840*m2^2*MBhat^5*Ycut^8 + 5250*MBhat^6*Ycut^8 + 
          420*m2^2*MBhat^6*Ycut^8 + 36460*Ycut^9 - 44940*m2*Ycut^9 + 
          2590*m2^2*Ycut^9 + 3150*m2^3*Ycut^9 + 87190*MBhat*Ycut^9 - 
          102620*m2*MBhat*Ycut^9 + 31080*m2^2*MBhat*Ycut^9 + 
          1680*m2^3*MBhat*Ycut^9 + 70*m2^4*MBhat*Ycut^9 - 
          49210*MBhat^2*Ycut^9 - 5110*m2*MBhat^2*Ycut^9 + 
          13860*m2^2*MBhat^2*Ycut^9 + 1120*m2^3*MBhat^2*Ycut^9 - 
          66190*MBhat^3*Ycut^9 + 33740*m2*MBhat^3*Ycut^9 + 
          2520*m2^2*MBhat^3*Ycut^9 + 1680*m2^3*MBhat^3*Ycut^9 - 
          350*m2^4*MBhat^3*Ycut^9 + 10150*MBhat^4*Ycut^9 + 
          12950*m2*MBhat^4*Ycut^9 + 8820*MBhat^5*Ycut^9 - 
          1120*m2*MBhat^5*Ycut^9 + 420*m2^2*MBhat^5*Ycut^9 + 
          140*MBhat^6*Ycut^9 - 30117*Ycut^10 + 26733*m2*Ycut^10 - 
          3045*m2^2*Ycut^10 - 525*m2^3*Ycut^10 - 28128*MBhat*Ycut^10 + 
          25704*m2*MBhat*Ycut^10 - 3528*m2^2*MBhat*Ycut^10 + 
          39144*MBhat^2*Ycut^10 - 7980*m2*MBhat^2*Ycut^10 - 
          2310*m2^2*MBhat^2*Ycut^10 + 12168*MBhat^3*Ycut^10 - 
          6552*m2*MBhat^3*Ycut^10 - 3885*MBhat^4*Ycut^10 - 
          1785*m2*MBhat^4*Ycut^10 - 1512*MBhat^5*Ycut^10 - 
          210*MBhat^6*Ycut^10 + 14802*Ycut^11 - 7938*m2*Ycut^11 + 
          558*MBhat*Ycut^11 - 1764*m2*MBhat*Ycut^11 + 588*m2^2*MBhat*
           Ycut^11 - 12054*MBhat^2*Ycut^11 + 1470*m2*MBhat^2*Ycut^11 - 
          558*MBhat^3*Ycut^11 + 1092*m2*MBhat^3*Ycut^11 + 
          840*MBhat^4*Ycut^11 + 252*MBhat^5*Ycut^11 - 4035*Ycut^12 + 
          875*m2*Ycut^12 + 2140*MBhat*Ycut^12 + 1505*MBhat^2*Ycut^12 - 
          245*m2*MBhat^2*Ycut^12 - 180*MBhat^3*Ycut^12 - 
          140*MBhat^4*Ycut^12 + 480*Ycut^13 - 450*MBhat*Ycut^13 + 
          30*MBhat^3*Ycut^13)/(-1 + Ycut)^6 + 
       (8 + 8*m2 - 585*m2^2 - 925*m2^3 - 165*m2^4 + 15*m2^5 - 48*MBhat - 
         16*m2*MBhat + 1872*m2^2*MBhat + 1520*m2^3*MBhat + 120*MBhat^2 - 
         16*m2*MBhat^2 - 2208*m2^2*MBhat^2 - 670*m2^3*MBhat^2 + 
         58*m2^4*MBhat^2 - 160*MBhat^3 + 64*m2*MBhat^3 + 1152*m2^2*MBhat^3 + 
         120*MBhat^4 - 56*m2*MBhat^4 - 237*m2^2*MBhat^4 + 43*m2^3*MBhat^4 - 
         48*MBhat^5 + 16*m2*MBhat^5 + 8*MBhat^6 + 6*m2^2*MBhat^6)*Log[m2] + 
       (-8 - 8*m2 + 585*m2^2 + 925*m2^3 + 165*m2^4 - 15*m2^5 + 48*MBhat + 
         16*m2*MBhat - 1872*m2^2*MBhat - 1520*m2^3*MBhat - 120*MBhat^2 + 
         16*m2*MBhat^2 + 2208*m2^2*MBhat^2 + 670*m2^3*MBhat^2 - 
         58*m2^4*MBhat^2 + 160*MBhat^3 - 64*m2*MBhat^3 - 1152*m2^2*MBhat^3 - 
         120*MBhat^4 + 56*m2*MBhat^4 + 237*m2^2*MBhat^4 - 43*m2^3*MBhat^4 + 
         48*MBhat^5 - 16*m2*MBhat^5 - 8*MBhat^6 - 6*m2^2*MBhat^6)*
        Log[1 - Ycut]) + c[VL]^2*
      (-1/420*(-11897 + 66843*m2 + 377475*m2^2 - 207025*m2^3 - 235375*m2^4 + 
          9597*m2^5 + 357*m2^6 + 25*m2^7 + 65872*MBhat - 329616*m2*MBhat - 
          807408*m2^2*MBhat + 932400*m2^3*MBhat + 148400*m2^4*MBhat - 
          9072*m2^5*MBhat - 784*m2^6*MBhat + 208*m2^7*MBhat - 
          148946*MBhat^2 + 641900*m2*MBhat^2 + 353220*m2^2*MBhat^2 - 
          894810*m2^3*MBhat^2 + 45290*m2^4*MBhat^2 + 6636*m2^5*MBhat^2 - 
          4060*m2^6*MBhat^2 + 770*m2^7*MBhat^2 + 174368*MBhat^3 - 
          617792*m2*MBhat^3 + 248640*m2^2*MBhat^3 + 219520*m2^3*MBhat^3 - 
          34720*m2^4*MBhat^3 + 17472*m2^5*MBhat^3 - 9408*m2^6*MBhat^3 + 
          1920*m2^7*MBhat^3 - 112035*MBhat^4 + 308245*m2*MBhat^4 - 
          225120*m2^2*MBhat^4 + 19600*m2^3*MBhat^4 + 21875*m2^4*MBhat^4 - 
          16485*m2^5*MBhat^4 + 3920*m2^6*MBhat^4 + 37968*MBhat^5 - 
          74480*m2*MBhat^5 + 47040*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 - 
          6160*m2^4*MBhat^5 + 2352*m2^5*MBhat^5 - 5390*MBhat^6 + 
          6160*m2*MBhat^6 - 1680*m2^2*MBhat^6 + 560*m2^3*MBhat^6 + 
          350*m2^4*MBhat^6 + 98536*Ycut - 531384*m2*Ycut - 
          3265500*m2^2*Ycut + 1267700*m2^3*Ycut + 1813700*m2^4*Ycut - 
          70476*m2^5*Ycut - 2856*m2^6*Ycut - 200*m2^7*Ycut - 
          547136*MBhat*Ycut + 2630208*m2*MBhat*Ycut + 7245504*m2^2*MBhat*
           Ycut - 6820800*m2^3*MBhat*Ycut - 1187200*m2^4*MBhat*Ycut + 
          72576*m2^5*MBhat*Ycut + 6272*m2^6*MBhat*Ycut - 
          1664*m2^7*MBhat*Ycut + 1241968*MBhat^2*Ycut - 5141920*m2*MBhat^2*
           Ycut - 3753120*m2^2*MBhat^2*Ycut + 6877080*m2^3*MBhat^2*Ycut - 
          337960*m2^4*MBhat^2*Ycut - 53088*m2^5*MBhat^2*Ycut + 
          32480*m2^6*MBhat^2*Ycut - 6160*m2^7*MBhat^2*Ycut - 
          1462144*MBhat^3*Ycut + 4969216*m2*MBhat^3*Ycut - 
          1505280*m2^2*MBhat^3*Ycut - 1756160*m2^3*MBhat^3*Ycut + 
          277760*m2^4*MBhat^3*Ycut - 139776*m2^5*MBhat^3*Ycut + 
          75264*m2^6*MBhat^3*Ycut - 15360*m2^7*MBhat^3*Ycut + 
          946680*MBhat^4*Ycut - 2489480*m2*MBhat^4*Ycut + 
          1701420*m2^2*MBhat^4*Ycut - 138740*m2^3*MBhat^4*Ycut - 
          175000*m2^4*MBhat^4*Ycut + 131880*m2^5*MBhat^4*Ycut - 
          31360*m2^6*MBhat^4*Ycut - 323904*MBhat^5*Ycut + 
          602560*m2*MBhat^5*Ycut - 376320*m2^2*MBhat^5*Ycut + 
          53760*m2^3*MBhat^5*Ycut + 49280*m2^4*MBhat^5*Ycut - 
          18816*m2^5*MBhat^5*Ycut + 46480*MBhat^6*Ycut - 
          49280*m2*MBhat^6*Ycut + 15960*m2^2*MBhat^6*Ycut - 
          4480*m2^3*MBhat^6*Ycut - 2800*m2^4*MBhat^6*Ycut - 358316*Ycut^2 + 
          1846404*m2*Ycut^2 + 12412050*m2^2*Ycut^2 - 2882950*m2^3*Ycut^2 - 
          6070750*m2^4*Ycut^2 + 221466*m2^5*Ycut^2 + 9996*m2^6*Ycut^2 + 
          700*m2^7*Ycut^2 + 1995616*MBhat*Ycut^2 - 9178848*m2*MBhat*Ycut^2 - 
          28504224*m2^2*MBhat*Ycut^2 + 21319200*m2^3*MBhat*Ycut^2 + 
          4155200*m2^4*MBhat*Ycut^2 - 254016*m2^5*MBhat*Ycut^2 - 
          21952*m2^6*MBhat*Ycut^2 + 5824*m2^7*MBhat*Ycut^2 - 
          4548488*MBhat^2*Ycut^2 + 18023600*m2*MBhat^2*Ycut^2 + 
          16845360*m2^2*MBhat^2*Ycut^2 - 22944180*m2^3*MBhat^2*Ycut^2 + 
          1085420*m2^4*MBhat^2*Ycut^2 + 185808*m2^5*MBhat^2*Ycut^2 - 
          113680*m2^6*MBhat^2*Ycut^2 + 21560*m2^7*MBhat^2*Ycut^2 + 
          5386304*MBhat^3*Ycut^2 - 17499776*m2*MBhat^3*Ycut^2 + 
          3333120*m2^2*MBhat^3*Ycut^2 + 6146560*m2^3*MBhat^3*Ycut^2 - 
          972160*m2^4*MBhat^3*Ycut^2 + 489216*m2^5*MBhat^3*Ycut^2 - 
          263424*m2^6*MBhat^3*Ycut^2 + 53760*m2^7*MBhat^3*Ycut^2 - 
          3514980*MBhat^4*Ycut^2 + 8807260*m2*MBhat^4*Ycut^2 - 
          5556810*m2^2*MBhat^4*Ycut^2 + 413350*m2^3*MBhat^4*Ycut^2 + 
          612500*m2^4*MBhat^4*Ycut^2 - 461580*m2^5*MBhat^4*Ycut^2 + 
          109760*m2^6*MBhat^4*Ycut^2 + 1214304*MBhat^5*Ycut^2 - 
          2135840*m2*MBhat^5*Ycut^2 + 1317120*m2^2*MBhat^5*Ycut^2 - 
          188160*m2^3*MBhat^5*Ycut^2 - 172480*m2^4*MBhat^5*Ycut^2 + 
          65856*m2^5*MBhat^5*Ycut^2 - 176120*MBhat^6*Ycut^2 + 
          172480*m2*MBhat^6*Ycut^2 - 65940*m2^2*MBhat^6*Ycut^2 + 
          15680*m2^3*MBhat^6*Ycut^2 + 9800*m2^4*MBhat^6*Ycut^2 + 
          747992*Ycut^3 - 3661448*m2*Ycut^3 - 27117300*m2^2*Ycut^3 + 
          2139900*m2^3*Ycut^3 + 11494700*m2^4*Ycut^3 - 384132*m2^5*Ycut^3 - 
          19992*m2^6*Ycut^3 - 1400*m2^7*Ycut^3 - 4179392*MBhat*Ycut^3 + 
          18294976*m2*MBhat*Ycut^3 + 64346688*m2^2*MBhat*Ycut^3 - 
          36680000*m2^3*MBhat*Ycut^3 - 8310400*m2^4*MBhat*Ycut^3 + 
          508032*m2^5*MBhat*Ycut^3 + 43904*m2^6*MBhat*Ycut^3 - 
          11648*m2^7*MBhat*Ycut^3 + 9567376*MBhat^2*Ycut^3 - 
          36109920*m2*MBhat^2*Ycut^3 - 42346080*m2^2*MBhat^2*Ycut^3 + 
          43261960*m2^3*MBhat^2*Ycut^3 - 1943480*m2^4*MBhat^2*Ycut^3 - 
          371616*m2^5*MBhat^2*Ycut^3 + 227360*m2^6*MBhat^2*Ycut^3 - 
          43120*m2^7*MBhat^2*Ycut^3 - 11393508*MBhat^3*Ycut^3 + 
          35235032*m2*MBhat^3*Ycut^3 - 2137100*m2^2*MBhat^3*Ycut^3 - 
          12318320*m2^3*MBhat^3*Ycut^3 + 1999620*m2^4*MBhat^3*Ycut^3 - 
          1027432*m2^5*MBhat^3*Ycut^3 + 541548*m2^6*MBhat^3*Ycut^3 - 
          107520*m2^7*MBhat^3*Ycut^3 + 7486920*MBhat^4*Ycut^3 - 
          17807160*m2*MBhat^4*Ycut^3 + 10157700*m2^2*MBhat^4*Ycut^3 - 
          604380*m2^3*MBhat^4*Ycut^3 - 1292200*m2^4*MBhat^4*Ycut^3 + 
          950040*m2^5*MBhat^4*Ycut^3 - 219520*m2^6*MBhat^4*Ycut^3 - 
          2607108*MBhat^5*Ycut^3 + 4319280*m2*MBhat^5*Ycut^3 - 
          2618280*m2^2*MBhat^5*Ycut^3 + 351120*m2^3*MBhat^5*Ycut^3 + 
          359660*m2^4*MBhat^5*Ycut^3 - 131712*m2^5*MBhat^5*Ycut^3 + 
          381360*MBhat^6*Ycut^3 - 342720*m2*MBhat^6*Ycut^3 + 
          153160*m2^2*MBhat^6*Ycut^3 - 29120*m2^3*MBhat^6*Ycut^3 - 
          19600*m2^4*MBhat^6*Ycut^3 - 982030*Ycut^4 + 4529770*m2*Ycut^4 + 
          37336425*m2^2*Ycut^4 + 2764125*m2^3*Ycut^4 - 13398175*m2^4*Ycut^4 + 
          391965*m2^5*Ycut^4 + 24990*m2^6*Ycut^4 + 1750*m2^7*Ycut^4 + 
          5506480*MBhat*Ycut^4 - 22774640*m2*MBhat*Ycut^4 - 
          91440720*m2^2*MBhat*Ycut^4 + 36912400*m2^3*MBhat*Ycut^4 + 
          10388000*m2^4*MBhat*Ycut^4 - 635040*m2^5*MBhat*Ycut^4 - 
          54880*m2^6*MBhat*Ycut^4 + 14560*m2^7*MBhat*Ycut^4 - 
          12671645*MBhat^2*Ycut^4 + 45247755*m2*MBhat^2*Ycut^4 + 
          65896740*m2^2*MBhat^2*Ycut^4 - 50088500*m2^3*MBhat^2*Ycut^4 + 
          1993285*m2^4*MBhat^2*Ycut^4 + 541695*m2^5*MBhat^2*Ycut^4 - 
          306250*m2^6*MBhat^2*Ycut^4 + 53900*m2^7*MBhat^2*Ycut^4 + 
          15155840*MBhat^3*Ycut^4 - 44356480*m2*MBhat^3*Ycut^4 - 
          4122720*m2^2*MBhat^3*Ycut^4 + 15366400*m2^3*MBhat^3*Ycut^4 - 
          2516640*m2^4*MBhat^3*Ycut^4 + 1337280*m2^5*MBhat^3*Ycut^4 - 
          697760*m2^6*MBhat^3*Ycut^4 + 134400*m2^7*MBhat^3*Ycut^4 - 
          9986865*MBhat^4*Ycut^4 + 22446585*m2*MBhat^4*Ycut^4 - 
          11213160*m2^2*MBhat^4*Ycut^4 + 388080*m2^3*MBhat^4*Ycut^4 + 
          1752800*m2^4*MBhat^4*Ycut^4 - 1236690*m2^5*MBhat^4*Ycut^4 + 
          274400*m2^6*MBhat^4*Ycut^4 + 3480960*MBhat^5*Ycut^4 - 
          5409600*m2*MBhat^5*Ycut^4 + 3213840*m2^2*MBhat^5*Ycut^4 - 
          371280*m2^3*MBhat^5*Ycut^4 - 481600*m2^4*MBhat^5*Ycut^4 + 
          164640*m2^5*MBhat^5*Ycut^4 - 509530*MBhat^6*Ycut^4 + 
          416640*m2*MBhat^6*Ycut^4 - 219660*m2^2*MBhat^6*Ycut^4 + 
          31080*m2^3*MBhat^6*Ycut^4 + 24500*m2^4*MBhat^6*Ycut^4 + 
          832664*Ycut^5 - 3576776*m2*Ycut^5 - 33308940*m2^2*Ycut^5 - 
          7650300*m2^3*Ycut^5 + 9748340*m2^4*Ycut^5 - 225372*m2^5*Ycut^5 - 
          19992*m2^6*Ycut^5 - 1400*m2^7*Ycut^5 - 4683854*MBhat*Ycut^5 + 
          18117232*m2*MBhat*Ycut^5 + 84171066*m2^2*MBhat*Ycut^5 - 
          20624240*m2^3*MBhat*Ycut^5 - 8250970*m2^4*MBhat*Ycut^5 + 
          460992*m2^5*MBhat*Ycut^5 + 57134*m2^6*MBhat*Ycut^5 - 
          11648*m2^7*MBhat*Ycut^5 + 10881640*MBhat^2*Ycut^5 - 
          36374520*m2*MBhat^2*Ycut^5 - 65644320*m2^2*MBhat^2*Ycut^5 + 
          36051400*m2^3*MBhat^2*Ycut^5 - 1109360*m2^4*MBhat^2*Ycut^5 - 
          528024*m2^5*MBhat^2*Ycut^5 + 262640*m2^6*MBhat^2*Ycut^5 - 
          43120*m2^7*MBhat^2*Ycut^5 - 13028596*MBhat^3*Ycut^5 + 
          35757708*m2*MBhat^3*Ycut^5 + 10057320*m2^2*MBhat^3*Ycut^5 - 
          12088160*m2^3*MBhat^3*Ycut^5 + 1902740*m2^4*MBhat^3*Ycut^5 - 
          1107372*m2^5*MBhat^3*Ycut^5 + 582904*m2^6*MBhat^3*Ycut^5 - 
          107520*m2^7*MBhat^3*Ycut^5 + 8490720*MBhat^4*Ycut^5 - 
          17937360*m2*MBhat^4*Ycut^5 + 7432740*m2^2*MBhat^4*Ycut^5 + 
          20580*m2^3*MBhat^4*Ycut^5 - 1532440*m2^4*MBhat^4*Ycut^5 + 
          1040760*m2^5*MBhat^4*Ycut^5 - 219520*m2^6*MBhat^4*Ycut^5 - 
          2890566*MBhat^5*Ycut^5 + 4203360*m2*MBhat^5*Ycut^5 - 
          2469180*m2^2*MBhat^5*Ycut^5 + 220080*m2^3*MBhat^5*Ycut^5 + 
          417410*m2^4*MBhat^5*Ycut^5 - 131712*m2^5*MBhat^5*Ycut^5 + 
          412160*MBhat^6*Ycut^5 - 304640*m2*MBhat^6*Ycut^5 + 
          202440*m2^2*MBhat^6*Ycut^5 - 20160*m2^3*MBhat^6*Ycut^5 - 
          19600*m2^4*MBhat^6*Ycut^5 - 448427*Ycut^6 + 1758743*m2*Ycut^6 + 
          18945220*m2^2*Ycut^6 + 7458500*m2^3*Ycut^6 - 4240845*m2^4*Ycut^6 + 
          64421*m2^5*Ycut^6 + 7056*m2^6*Ycut^6 + 700*m2^7*Ycut^6 + 
          2506784*MBhat*Ycut^6 - 8948576*m2*MBhat*Ycut^6 - 
          49463568*m2^2*MBhat*Ycut^6 + 4427360*m2^3*MBhat*Ycut^6 + 
          4019680*m2^4*MBhat*Ycut^6 - 177408*m2^5*MBhat*Ycut^6 - 
          33712*m2^6*MBhat*Ycut^6 + 5824*m2^7*MBhat*Ycut^6 - 
          6005265*MBhat^2*Ycut^6 + 18430027*m2*MBhat^2*Ycut^6 + 
          41414940*m2^2*MBhat^2*Ycut^6 - 15411200*m2^3*MBhat^2*Ycut^6 + 
          274785*m2^4*MBhat^2*Ycut^6 + 319137*m2^5*MBhat^2*Ycut^6 - 
          145432*m2^6*MBhat^2*Ycut^6 + 21560*m2^7*MBhat^2*Ycut^6 + 
          7161728*MBhat^3*Ycut^6 - 18072544*m2*MBhat^3*Ycut^6 - 
          9494240*m2^2*MBhat^3*Ycut^6 + 5761280*m2^3*MBhat^3*Ycut^6 - 
          805280*m2^4*MBhat^3*Ycut^6 + 583296*m2^5*MBhat^3*Ycut^6 - 
          312032*m2^6*MBhat^3*Ycut^6 + 53760*m2^7*MBhat^3*Ycut^6 - 
          4404134*MBhat^4*Ycut^6 + 8672160*m2*MBhat^4*Ycut^6 - 
          2625420*m2^2*MBhat^4*Ycut^6 - 184660*m2^3*MBhat^4*Ycut^6 + 
          853790*m2^4*MBhat^4*Ycut^6 - 562716*m2^5*MBhat^4*Ycut^6 + 
          109760*m2^6*MBhat^4*Ycut^6 + 1321488*MBhat^5*Ycut^6 - 
          1817760*m2*MBhat^5*Ycut^6 + 1122240*m2^2*MBhat^5*Ycut^6 - 
          60480*m2^3*MBhat^5*Ycut^6 - 231280*m2^4*MBhat^5*Ycut^6 + 
          65856*m2^5*MBhat^5*Ycut^6 - 160720*MBhat^6*Ycut^6 + 
          110880*m2*MBhat^6*Ycut^6 - 121100*m2^2*MBhat^6*Ycut^6 + 
          8400*m2^3*MBhat^6*Ycut^6 + 9800*m2^4*MBhat^6*Ycut^6 + 
          146784*Ycut^7 - 499408*m2*Ycut^7 - 6386100*m2^2*Ycut^7 - 
          3704260*m2^3*Ycut^7 + 959700*m2^4*Ycut^7 - 3948*m2^5*Ycut^7 - 
          1736*m2^6*Ycut^7 - 200*m2^7*Ycut^7 - 728998*MBhat*Ycut^7 + 
          2412256*m2*MBhat*Ycut^7 + 17334828*m2^2*MBhat*Ycut^7 + 
          1243760*m2^3*MBhat*Ycut^7 - 1097390*m2^4*MBhat*Ycut^7 + 
          34272*m2^5*MBhat*Ycut^7 + 12152*m2^6*MBhat*Ycut^7 - 
          1664*m2^7*MBhat*Ycut^7 + 2034480*MBhat^2*Ycut^7 - 
          5504576*m2*MBhat^2*Ycut^7 - 15506400*m2^2*MBhat^2*Ycut^7 + 
          3360280*m2^3*MBhat^2*Ycut^7 + 22680*m2^4*MBhat^2*Ycut^7 - 
          126672*m2^5*MBhat^2*Ycut^7 + 49616*m2^6*MBhat^2*Ycut^7 - 
          6160*m2^7*MBhat^2*Ycut^7 - 2452166*MBhat^3*Ycut^7 + 
          5329184*m2*MBhat^3*Ycut^7 + 4604880*m2^2*MBhat^3*Ycut^7 - 
          1436960*m2^3*MBhat^3*Ycut^7 + 112630*m2^4*MBhat^3*Ycut^7 - 
          172368*m2^5*MBhat^3*Ycut^7 + 100912*m2^6*MBhat^3*Ycut^7 - 
          15360*m2^7*MBhat^3*Ycut^7 + 1172752*MBhat^4*Ycut^7 - 
          2074800*m2*MBhat^4*Ycut^7 + 194460*m2^2*MBhat^4*Ycut^7 + 
          112700*m2^3*MBhat^4*Ycut^7 - 285880*m2^4*MBhat^4*Ycut^7 + 
          184968*m2^5*MBhat^4*Ycut^7 - 31360*m2^6*MBhat^4*Ycut^7 - 
          96768*MBhat^5*Ycut^7 + 181440*m2*MBhat^5*Ycut^7 - 
          226380*m2^2*MBhat^5*Ycut^7 - 5040*m2^3*MBhat^5*Ycut^7 + 
          78680*m2^4*MBhat^5*Ycut^7 - 18816*m2^5*MBhat^5*Ycut^7 - 
          31360*MBhat^6*Ycut^7 + 6720*m2*MBhat^6*Ycut^7 + 
          46200*m2^2*MBhat^6*Ycut^7 - 2240*m2^3*MBhat^6*Ycut^7 - 
          2800*m2^4*MBhat^6*Ycut^7 - 38445*Ycut^8 + 86751*m2*Ycut^8 + 
          1028685*m2^2*Ycut^8 + 866705*m2^3*Ycut^8 - 62230*m2^4*Ycut^8 - 
          5082*m2^5*Ycut^8 + 217*m2^6*Ycut^8 + 25*m2^7*Ycut^8 - 
          2320*MBhat*Ycut^8 - 128912*m2*MBhat*Ycut^8 - 2997456*m2^2*MBhat*
           Ycut^8 - 804160*m2^3*MBhat*Ycut^8 + 135520*m2^4*MBhat*Ycut^8 + 
          672*m2^5*MBhat*Ycut^8 - 2464*m2^6*MBhat*Ycut^8 + 
          208*m2^7*MBhat*Ycut^8 - 363300*MBhat^2*Ycut^8 + 
          824866*m2*MBhat^2*Ycut^8 + 2840880*m2^2*MBhat^2*Ycut^8 - 
          173810*m2^3*MBhat^2*Ycut^8 - 39795*m2^4*MBhat^2*Ycut^8 + 
          28749*m2^5*MBhat^2*Ycut^8 - 9226*m2^6*MBhat^2*Ycut^8 + 
          770*m2^7*MBhat^2*Ycut^8 + 580240*MBhat^3*Ycut^8 - 
          819168*m2*MBhat^3*Ycut^8 - 1024800*m2^2*MBhat^3*Ycut^8 + 
          51520*m2^3*MBhat^3*Ycut^8 + 66640*m2^4*MBhat^3*Ycut^8 + 
          16128*m2^5*MBhat^3*Ycut^8 - 17024*m2^6*MBhat^3*Ycut^8 + 
          1920*m2^7*MBhat^3*Ycut^8 - 49637*MBhat^4*Ycut^8 - 
          37065*m2*MBhat^4*Ycut^8 + 197400*m2^2*MBhat^4*Ycut^8 - 
          26320*m2^3*MBhat^4*Ycut^8 + 43925*m2^4*MBhat^4*Ycut^8 - 
          32193*m2^5*MBhat^4*Ycut^8 + 3920*m2^6*MBhat^4*Ycut^8 - 
          249984*MBhat^5*Ycut^8 + 233520*m2*MBhat^5*Ycut^8 - 
          35280*m2^2*MBhat^5*Ycut^8 + 8400*m2^3*MBhat^5*Ycut^8 - 
          14560*m2^4*MBhat^5*Ycut^8 + 2352*m2^5*MBhat^5*Ycut^8 + 
          72590*MBhat^6*Ycut^8 - 24080*m2*MBhat^6*Ycut^8 - 
          10500*m2^2*MBhat^6*Ycut^8 + 280*m2^3*MBhat^6*Ycut^8 + 
          350*m2^4*MBhat^6*Ycut^8 + 31976*Ycut^9 - 42280*m2*Ycut^9 - 
          14840*m2^2*Ycut^9 - 42840*m2^3*Ycut^9 - 10360*m2^4*Ycut^9 + 
          1624*m2^5*Ycut^9 + 115108*MBhat*Ycut^9 - 143360*m2*MBhat*Ycut^9 + 
          112560*m2^2*MBhat*Ycut^9 + 82320*m2^3*MBhat*Ycut^9 + 
          1330*m2^4*MBhat*Ycut^9 - 1008*m2^5*MBhat*Ycut^9 + 
          210*m2^6*MBhat*Ycut^9 - 29680*MBhat^2*Ycut^9 - 
          20272*m2*MBhat^2*Ycut^9 - 80640*m2^2*MBhat^2*Ycut^9 - 
          39200*m2^3*MBhat^2*Ycut^9 + 9240*m2^4*MBhat^2*Ycut^9 - 
          2520*m2^5*MBhat^2*Ycut^9 + 672*m2^6*MBhat^2*Ycut^9 - 
          168784*MBhat^3*Ycut^9 + 102760*m2*MBhat^3*Ycut^9 + 
          13720*m2^2*MBhat^3*Ycut^9 + 72800*m2^3*MBhat^3*Ycut^9 - 
          36960*m2^4*MBhat^3*Ycut^9 + 4564*m2^5*MBhat^3*Ycut^9 + 
          980*m2^6*MBhat^3*Ycut^9 - 15176*MBhat^4*Ycut^9 + 
          132440*m2*MBhat^4*Ycut^9 - 72240*m2^2*MBhat^4*Ycut^9 - 
          1680*m2^3*MBhat^4*Ycut^9 + 1680*m2^4*MBhat^4*Ycut^9 + 
          2016*m2^5*MBhat^4*Ycut^9 + 145740*MBhat^5*Ycut^9 - 
          128800*m2*MBhat^5*Ycut^9 + 31500*m2^2*MBhat^5*Ycut^9 - 
          1680*m2^3*MBhat^5*Ycut^9 + 1050*m2^4*MBhat^5*Ycut^9 - 
          37520*MBhat^6*Ycut^9 + 8960*m2*MBhat^6*Ycut^9 + 
          1120*m2^2*MBhat^6*Ycut^9 - 35434*Ycut^10 + 37058*m2*Ycut^10 - 
          7350*m2^2*Ycut^10 - 9310*m2^3*Ycut^10 + 1295*m2^4*Ycut^10 - 
          63*m2^5*Ycut^10 - 62048*MBhat*Ycut^10 + 59584*m2*MBhat*Ycut^10 - 
          4368*m2^2*MBhat*Ycut^10 + 13440*m2^3*MBhat*Ycut^10 - 
          2240*m2^4*MBhat*Ycut^10 + 73766*MBhat^2*Ycut^10 - 
          29890*m2*MBhat^2*Ycut^10 - 14700*m2^2*MBhat^2*Ycut^10 - 
          140*m2^3*MBhat^2*Ycut^10 - 105*m2^4*MBhat^2*Ycut^10 - 
          105*m2^5*MBhat^2*Ycut^10 + 49616*MBhat^3*Ycut^10 - 
          31136*m2*MBhat^3*Ycut^10 + 30240*m2^2*MBhat^3*Ycut^10 - 
          20160*m2^3*MBhat^3*Ycut^10 + 6720*m2^4*MBhat^3*Ycut^10 - 
          1008*m2^5*MBhat^3*Ycut^10 - 23240*MBhat^4*Ycut^10 - 
          20860*m2*MBhat^4*Ycut^10 + 9030*m2^2*MBhat^4*Ycut^10 + 
          1470*m2^3*MBhat^4*Ycut^10 - 1050*m2^4*MBhat^4*Ycut^10 - 
          35952*MBhat^5*Ycut^10 + 29120*m2*MBhat^5*Ycut^10 - 
          6720*m2^2*MBhat^5*Ycut^10 + 8680*MBhat^6*Ycut^10 - 
          1120*m2*MBhat^6*Ycut^10 + 25256*Ycut^11 - 18984*m2*Ycut^11 + 
          840*m2^2*Ycut^11 - 280*m2^3*Ycut^11 + 12292*MBhat*Ycut^11 - 
          9632*m2*MBhat*Ycut^11 + 6804*m2^2*MBhat*Ycut^11 - 
          1680*m2^3*MBhat*Ycut^11 + 70*m2^4*MBhat*Ycut^11 - 
          40768*MBhat^2*Ycut^11 + 15008*m2*MBhat^2*Ycut^11 - 
          6720*m2^2*MBhat^2*Ycut^11 + 1120*m2^3*MBhat^2*Ycut^11 + 
          56*MBhat^3*Ycut^11 + 2408*m2*MBhat^3*Ycut^11 - 3780*m2^2*MBhat^3*
           Ycut^11 + 1680*m2^3*MBhat^3*Ycut^11 - 350*m2^4*MBhat^3*Ycut^11 + 
          10024*MBhat^4*Ycut^11 - 280*m2*MBhat^4*Ycut^11 + 
          4116*MBhat^5*Ycut^11 - 2800*m2*MBhat^5*Ycut^11 + 
          420*m2^2*MBhat^5*Ycut^11 - 560*MBhat^6*Ycut^11 - 11228*Ycut^12 + 
          5404*m2*Ycut^12 - 665*m2^2*Ycut^12 + 35*m2^3*Ycut^12 + 
          3472*MBhat*Ycut^12 - 1008*m2*MBhat*Ycut^12 + 336*m2^2*MBhat*
           Ycut^12 + 9443*MBhat^2*Ycut^12 - 2009*m2*MBhat^2*Ycut^12 + 
          840*m2^2*MBhat^2*Ycut^12 - 3248*MBhat^3*Ycut^12 + 
          672*m2*MBhat^3*Ycut^12 - 1127*MBhat^4*Ycut^12 + 
          315*m2*MBhat^4*Ycut^12 - 336*MBhat^5*Ycut^12 - 70*MBhat^6*Ycut^12 + 
          2968*Ycut^13 - 728*m2*Ycut^13 - 2198*MBhat*Ycut^13 + 
          336*m2*MBhat*Ycut^13 - 42*m2^2*MBhat*Ycut^13 - 
          616*MBhat^2*Ycut^13 - 56*m2*MBhat^2*Ycut^13 + 308*MBhat^3*Ycut^13 - 
          84*m2*MBhat^3*Ycut^13 + 112*MBhat^4*Ycut^13 + 42*MBhat^5*Ycut^13 - 
          431*Ycut^14 + 35*m2*Ycut^14 + 352*MBhat*Ycut^14 + 
          35*MBhat^2*Ycut^14 + 7*m2*MBhat^2*Ycut^14 - 16*MBhat^3*Ycut^14 - 
          14*MBhat^4*Ycut^14 + 32*Ycut^15 - 30*MBhat*Ycut^15 + 
          2*MBhat^3*Ycut^15)/(-1 + Ycut)^8 + 
       (8 + 8*m2 - 585*m2^2 - 925*m2^3 - 165*m2^4 + 15*m2^5 - 48*MBhat - 
         16*m2*MBhat + 1872*m2^2*MBhat + 1520*m2^3*MBhat + 120*MBhat^2 - 
         16*m2*MBhat^2 - 2208*m2^2*MBhat^2 - 670*m2^3*MBhat^2 + 
         58*m2^4*MBhat^2 - 160*MBhat^3 + 64*m2*MBhat^3 + 1152*m2^2*MBhat^3 + 
         120*MBhat^4 - 56*m2*MBhat^4 - 237*m2^2*MBhat^4 + 43*m2^3*MBhat^4 - 
         48*MBhat^5 + 16*m2*MBhat^5 + 8*MBhat^6 + 6*m2^2*MBhat^6)*Log[m2] + 
       (-8 - 8*m2 + 585*m2^2 + 925*m2^3 + 165*m2^4 - 15*m2^5 + 48*MBhat + 
         16*m2*MBhat - 1872*m2^2*MBhat - 1520*m2^3*MBhat - 120*MBhat^2 + 
         16*m2*MBhat^2 + 2208*m2^2*MBhat^2 + 670*m2^3*MBhat^2 - 
         58*m2^4*MBhat^2 + 160*MBhat^3 - 64*m2*MBhat^3 - 1152*m2^2*MBhat^3 - 
         120*MBhat^4 + 56*m2*MBhat^4 + 237*m2^2*MBhat^4 - 43*m2^3*MBhat^4 + 
         48*MBhat^5 - 16*m2*MBhat^5 - 8*MBhat^6 - 6*m2^2*MBhat^6)*
        Log[1 - Ycut]) + c[SL]^2*
      (-1/840*(-12340 + 36092*m2 + 310380*m2^2 - 147700*m2^3 - 193900*m2^4 + 
          7140*m2^5 + 308*m2^6 + 20*m2^7 + 69072*MBhat - 184464*m2*MBhat - 
          698544*m2^2*MBhat + 697200*m2^3*MBhat + 126000*m2^4*MBhat - 
          9072*m2^5*MBhat - 336*m2^6*MBhat + 144*m2^7*MBhat - 
          158949*MBhat^2 + 373800*m2*MBhat^2 + 419055*m2^2*MBhat^2 - 
          674100*m2^3*MBhat^2 + 42525*m2^4*MBhat^2 - 1176*m2^5*MBhat^2 - 
          1575*m2^6*MBhat^2 + 420*m2^7*MBhat^2 + 192000*MBhat^3 - 
          381696*m2*MBhat^3 + 45360*m2^2*MBhat^3 + 156800*m2^3*MBhat^3 - 
          11200*m2^4*MBhat^3 - 1904*m2^6*MBhat^3 + 640*m2^7*MBhat^3 - 
          128940*MBhat^4 + 209160*m2*MBhat^4 - 99750*m2^2*MBhat^4 + 
          23520*m2^3*MBhat^4 - 1260*m2^4*MBhat^4 - 4200*m2^5*MBhat^4 + 
          1470*m2^6*MBhat^4 + 46032*MBhat^5 - 58800*m2*MBhat^5 + 
          20160*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 - 1680*m2^4*MBhat^5 + 
          1008*m2^5*MBhat^5 - 6895*MBhat^6 + 6440*m2*MBhat^6 + 
          280*m2^3*MBhat^6 + 175*m2^4*MBhat^6 + 102080*Ycut - 
          275296*m2*Ycut - 2659440*m2^2*Ycut + 870800*m2^3*Ycut + 
          1492400*m2^4*Ycut - 52080*m2^5*Ycut - 2464*m2^6*Ycut - 
          160*m2^7*Ycut - 572736*MBhat*Ycut + 1415232*m2*MBhat*Ycut + 
          6132672*m2^2*MBhat*Ycut - 5073600*m2^3*MBhat*Ycut - 
          1008000*m2^4*MBhat*Ycut + 72576*m2^5*MBhat*Ycut + 
          2688*m2^6*MBhat*Ycut - 1152*m2^7*MBhat*Ycut + 1321992*MBhat^2*
           Ycut - 2879520*m2*MBhat^2*Ycut - 3966060*m2^2*MBhat^2*Ycut + 
          5176080*m2^3*MBhat^2*Ycut - 323820*m2^4*MBhat^2*Ycut + 
          9408*m2^5*MBhat^2*Ycut + 12600*m2^6*MBhat^2*Ycut - 
          3360*m2^7*MBhat^2*Ycut - 1603200*MBhat^3*Ycut + 
          2946048*m2*MBhat^3*Ycut - 60480*m2^2*MBhat^3*Ycut - 
          1254400*m2^3*MBhat^3*Ycut + 89600*m2^4*MBhat^3*Ycut + 
          15232*m2^6*MBhat^3*Ycut - 5120*m2^7*MBhat^3*Ycut + 
          1081920*MBhat^4*Ycut - 1612800*m2*MBhat^4*Ycut + 
          740040*m2^2*MBhat^4*Ycut - 178080*m2^3*MBhat^4*Ycut + 
          10080*m2^4*MBhat^4*Ycut + 33600*m2^5*MBhat^4*Ycut - 
          11760*m2^6*MBhat^4*Ycut - 388416*MBhat^5*Ycut + 
          450240*m2*MBhat^5*Ycut - 161280*m2^2*MBhat^5*Ycut + 
          53760*m2^3*MBhat^5*Ycut + 13440*m2^4*MBhat^5*Ycut - 
          8064*m2^5*MBhat^5*Ycut + 58520*MBhat^6*Ycut - 48160*m2*MBhat^6*
           Ycut + 1260*m2^2*MBhat^6*Ycut - 2240*m2^3*MBhat^6*Ycut - 
          1400*m2^4*MBhat^6*Ycut - 370720*Ycut^2 + 909776*m2*Ycut^2 + 
          10013640*m2^2*Ycut^2 - 1804600*m2^3*Ycut^2 - 4988200*m2^4*Ycut^2 + 
          162120*m2^5*Ycut^2 + 8624*m2^6*Ycut^2 + 560*m2^7*Ycut^2 + 
          2085216*MBhat*Ycut^2 - 4711392*m2*MBhat*Ycut^2 - 
          23641632*m2^2*MBhat*Ycut^2 + 15741600*m2^3*MBhat*Ycut^2 + 
          3528000*m2^4*MBhat*Ycut^2 - 254016*m2^5*MBhat*Ycut^2 - 
          9408*m2^6*MBhat*Ycut^2 + 4032*m2^7*MBhat*Ycut^2 - 
          4828572*MBhat^2*Ycut^2 + 9634800*m2*MBhat^2*Ycut^2 + 
          16335690*m2^2*MBhat^2*Ycut^2 - 17249400*m2^3*MBhat^2*Ycut^2 + 
          1067850*m2^4*MBhat^2*Ycut^2 - 32928*m2^5*MBhat^2*Ycut^2 - 
          44100*m2^6*MBhat^2*Ycut^2 + 11760*m2^7*MBhat^2*Ycut^2 + 
          5880000*MBhat^3*Ycut^2 - 9881088*m2*MBhat^3*Ycut^2 - 
          997920*m2^2*MBhat^3*Ycut^2 + 4390400*m2^3*MBhat^3*Ycut^2 - 
          313600*m2^4*MBhat^3*Ycut^2 - 53312*m2^6*MBhat^3*Ycut^2 + 
          17920*m2^7*MBhat^3*Ycut^2 - 3988320*MBhat^4*Ycut^2 + 
          5402880*m2*MBhat^4*Ycut^2 - 2358300*m2^2*MBhat^4*Ycut^2 + 
          582960*m2^3*MBhat^4*Ycut^2 - 35280*m2^4*MBhat^4*Ycut^2 - 
          117600*m2^5*MBhat^4*Ycut^2 + 41160*m2^6*MBhat^4*Ycut^2 + 
          1440096*MBhat^5*Ycut^2 - 1495200*m2*MBhat^5*Ycut^2 + 
          564480*m2^2*MBhat^5*Ycut^2 - 188160*m2^3*MBhat^5*Ycut^2 - 
          47040*m2^4*MBhat^5*Ycut^2 + 28224*m2^5*MBhat^5*Ycut^2 - 
          218260*MBhat^6*Ycut^2 + 155120*m2*MBhat^6*Ycut^2 - 
          9450*m2^2*MBhat^6*Ycut^2 + 7840*m2^3*MBhat^6*Ycut^2 + 
          4900*m2^4*MBhat^6*Ycut^2 + 772800*Ycut^3 - 1694112*m2*Ycut^3 - 
          21673680*m2^2*Ycut^3 + 708400*m2^3*Ycut^3 + 9427600*m2^4*Ycut^3 - 
          277200*m2^5*Ycut^3 - 17248*m2^6*Ycut^3 - 1120*m2^7*Ycut^3 - 
          4358592*MBhat*Ycut^3 + 8858304*m2*MBhat*Ycut^3 + 
          52363584*m2^2*MBhat*Ycut^3 - 26779200*m2^3*MBhat*Ycut^3 - 
          7056000*m2^4*MBhat*Ycut^3 + 508032*m2^5*MBhat*Ycut^3 + 
          18816*m2^6*MBhat*Ycut^3 - 8064*m2^7*MBhat*Ycut^3 + 
          10127544*MBhat^2*Ycut^3 - 18234720*m2*MBhat^2*Ycut^3 - 
          38398500*m2^2*MBhat^2*Ycut^3 + 32476080*m2^3*MBhat^2*Ycut^3 - 
          1982820*m2^4*MBhat^2*Ycut^3 + 65856*m2^5*MBhat^2*Ycut^3 + 
          88200*m2^6*MBhat^2*Ycut^3 - 23520*m2^7*MBhat^2*Ycut^3 - 
          12382790*MBhat^3*Ycut^3 + 18748856*m2*MBhat^3*Ycut^3 + 
          4820690*m2^2*MBhat^3*Ycut^3 - 8780800*m2^3*MBhat^3*Ycut^3 + 
          644350*m2^4*MBhat^3*Ycut^3 - 21560*m2^5*MBhat^3*Ycut^3 + 
          113974*m2^6*MBhat^3*Ycut^3 - 35840*m2^7*MBhat^3*Ycut^3 + 
          8438640*MBhat^4*Ycut^3 - 10227840*m2*MBhat^4*Ycut^3 + 
          4172280*m2^2*MBhat^4*Ycut^3 - 1058400*m2^3*MBhat^4*Ycut^3 + 
          42000*m2^4*MBhat^4*Ycut^3 + 248640*m2^5*MBhat^4*Ycut^3 - 
          82320*m2^6*MBhat^4*Ycut^3 - 3063102*MBhat^5*Ycut^3 + 
          2795940*m2*MBhat^5*Ycut^3 - 1124760*m2^2*MBhat^5*Ycut^3 + 
          365820*m2^3*MBhat^5*Ycut^3 + 101430*m2^4*MBhat^5*Ycut^3 - 
          56448*m2^5*MBhat^5*Ycut^3 + 466760*MBhat^6*Ycut^3 - 
          277760*m2*MBhat^6*Ycut^3 + 29540*m2^2*MBhat^6*Ycut^3 - 
          14560*m2^3*MBhat^6*Ycut^3 - 9800*m2^4*MBhat^6*Ycut^3 - 
          1013040*Ycut^4 + 1929480*m2*Ycut^4 + 29561700*m2^2*Ycut^4 + 
          3465700*m2^3*Ycut^4 - 10961300*m2^4*Ycut^4 + 275940*m2^5*Ycut^4 + 
          21560*m2^6*Ycut^4 + 1400*m2^7*Ycut^4 + 5730480*MBhat*Ycut^4 - 
          10226160*m2*MBhat*Ycut^4 - 73074960*m2^2*MBhat*Ycut^4 + 
          26418000*m2^3*MBhat*Ycut^4 + 8820000*m2^4*MBhat*Ycut^4 - 
          635040*m2^5*MBhat*Ycut^4 - 23520*m2^6*MBhat*Ycut^4 + 
          10080*m2^7*MBhat*Ycut^4 - 13371645*MBhat^2*Ycut^4 + 
          21255780*m2*MBhat^2*Ycut^4 + 56585130*m2^2*MBhat^2*Ycut^4 - 
          37561020*m2^3*MBhat^2*Ycut^4 + 2223480*m2^4*MBhat^2*Ycut^4 - 
          49980*m2^5*MBhat^2*Ycut^4 - 121275*m2^6*MBhat^2*Ycut^4 + 
          29400*m2^7*MBhat^2*Ycut^4 + 16403240*MBhat^3*Ycut^4 - 
          21893760*m2*MBhat^3*Ycut^4 - 10263120*m2^2*MBhat^3*Ycut^4 + 
          10935680*m2^3*MBhat^3*Ycut^4 - 792120*m2^4*MBhat^3*Ycut^4 + 
          47040*m2^5*MBhat^3*Ycut^4 - 152880*m2^6*MBhat^3*Ycut^4 + 
          44800*m2^7*MBhat^3*Ycut^4 - 11208750*MBhat^4*Ycut^4 + 
          11873610*m2*MBhat^4*Ycut^4 - 4406430*m2^2*MBhat^4*Ycut^4 + 
          1175160*m2^3*MBhat^4*Ycut^4 + 630*m2^4*MBhat^4*Ycut^4 - 
          335370*m2^5*MBhat^4*Ycut^4 + 102900*m2^6*MBhat^4*Ycut^4 + 
          4080720*MBhat^5*Ycut^4 - 3181920*m2*MBhat^5*Ycut^4 + 
          1396080*m2^2*MBhat^5*Ycut^4 - 431760*m2^3*MBhat^5*Ycut^4 - 
          142800*m2^4*MBhat^5*Ycut^4 + 70560*m2^5*MBhat^5*Ycut^4 - 
          623805*MBhat^6*Ycut^4 + 294560*m2*MBhat^6*Ycut^4 - 
          51030*m2^2*MBhat^6*Ycut^4 + 15540*m2^3*MBhat^6*Ycut^4 + 
          12250*m2^4*MBhat^6*Ycut^4 + 857472*Ycut^5 - 1355424*m2*Ycut^5 - 
          26118960*m2^2*Ycut^5 - 7123760*m2^3*Ycut^5 + 7945840*m2^4*Ycut^5 - 
          150192*m2^5*Ycut^5 - 17248*m2^6*Ycut^5 - 1120*m2^7*Ycut^5 - 
          4862655*MBhat*Ycut^5 + 7325388*m2*MBhat*Ycut^5 + 
          66082653*m2^2*MBhat*Ycut^5 - 14078400*m2^3*MBhat*Ycut^5 - 
          7040565*m2^4*MBhat*Ycut^5 + 488628*m2^5*MBhat*Ycut^5 + 
          25431*m2^6*MBhat*Ycut^5 - 8064*m2^7*MBhat*Ycut^5 + 
          11440968*MBhat^2*Ycut^5 - 15529920*m2*MBhat^2*Ycut^5 - 
          53849460*m2^2*MBhat^2*Ycut^5 + 27063120*m2^3*MBhat^2*Ycut^5 - 
          1545180*m2^4*MBhat^2*Ycut^5 + 8736*m2^5*MBhat^2*Ycut^5 + 
          105840*m2^6*MBhat^2*Ycut^5 - 23520*m2^7*MBhat^2*Ycut^5 - 
          14054404*MBhat^3*Ycut^5 + 15962394*m2*MBhat^3*Ycut^5 + 
          12459720*m2^2*MBhat^3*Ycut^5 - 8664040*m2^3*MBhat^3*Ycut^5 + 
          574560*m2^4*MBhat^3*Ycut^5 - 52626*m2^5*MBhat^3*Ycut^5 + 
          134652*m2^6*MBhat^3*Ycut^5 - 35840*m2^7*MBhat^3*Ycut^5 + 
          9564240*MBhat^4*Ycut^5 - 8512560*m2*MBhat^4*Ycut^5 + 
          2745960*m2^2*MBhat^4*Ycut^5 - 843360*m2^3*MBhat^4*Ycut^5 - 
          45360*m2^4*MBhat^4*Ycut^5 + 294000*m2^5*MBhat^4*Ycut^5 - 
          82320*m2^6*MBhat^4*Ycut^5 - 3465609*MBhat^5*Ycut^5 + 
          2195760*m2*MBhat^5*Ycut^5 - 1113840*m2^2*MBhat^5*Ycut^5 + 
          321720*m2^3*MBhat^5*Ycut^5 + 130305*m2^4*MBhat^5*Ycut^5 - 
          56448*m2^5*MBhat^5*Ycut^5 + 527520*MBhat^6*Ycut^5 - 
          175840*m2*MBhat^6*Ycut^5 + 54180*m2^2*MBhat^6*Ycut^5 - 
          10080*m2^3*MBhat^6*Ycut^5 - 9800*m2^4*MBhat^6*Ycut^5 - 
          460978*Ycut^6 + 554232*m2*Ycut^6 + 14705390*m2^2*Ycut^6 + 
          6462680*m2^3*Ycut^6 - 3427550*m2^4*Ycut^6 + 32368*m2^5*Ycut^6 + 
          7154*m2^6*Ycut^6 + 560*m2^7*Ycut^6 + 2592744*MBhat*Ycut^6 - 
          3045504*m2*MBhat*Ycut^6 - 38128104*m2^2*MBhat*Ycut^6 + 
          2308320*m2^3*MBhat*Ycut^6 + 3513720*m2^4*MBhat*Ycut^6 - 
          227808*m2^5*MBhat*Ycut^6 - 15288*m2^6*MBhat*Ycut^6 + 
          4032*m2^7*MBhat*Ycut^6 - 6285636*MBhat^2*Ycut^6 + 
          6911436*m2*MBhat^2*Ycut^6 + 32633370*m2^2*MBhat^2*Ycut^6 - 
          11640720*m2^3*MBhat^2*Ycut^6 + 680190*m2^4*MBhat^2*Ycut^6 + 
          5964*m2^5*MBhat^2*Ycut^6 - 59976*m2^6*MBhat^2*Ycut^6 + 
          11760*m2^7*MBhat^2*Ycut^6 + 7715792*MBhat^3*Ycut^6 - 
          6985552*m2*MBhat^3*Ycut^6 - 9118480*m2^2*MBhat^3*Ycut^6 + 
          4298560*m2^3*MBhat^3*Ycut^6 - 249200*m2^4*MBhat^3*Ycut^6 + 
          44688*m2^5*MBhat^3*Ycut^6 - 77616*m2^6*MBhat^3*Ycut^6 + 
          17920*m2^7*MBhat^3*Ycut^6 - 5109762*MBhat^4*Ycut^6 + 
          3518760*m2*MBhat^4*Ycut^6 - 885990*m2^2*MBhat^4*Ycut^6 + 
          399840*m2^3*MBhat^4*Ycut^6 + 54180*m2^4*MBhat^4*Ycut^6 - 
          168168*m2^5*MBhat^4*Ycut^6 + 41160*m2^6*MBhat^4*Ycut^6 + 
          1794072*MBhat^5*Ycut^6 - 826560*m2*MBhat^5*Ycut^6 + 
          571200*m2^2*MBhat^5*Ycut^6 - 151200*m2^3*MBhat^5*Ycut^6 - 
          76440*m2^4*MBhat^5*Ycut^6 + 28224*m2^5*MBhat^5*Ycut^6 - 
          264600*MBhat^6*Ycut^6 + 39760*m2*MBhat^6*Ycut^6 - 
          37030*m2^2*MBhat^6*Ycut^6 + 4200*m2^3*MBhat^6*Ycut^6 + 
          4900*m2^4*MBhat^6*Ycut^6 + 151632*Ycut^7 - 117152*m2*Ycut^7 - 
          4905600*m2^2*Ycut^7 - 3083920*m2^3*Ycut^7 + 745920*m2^4*Ycut^7 + 
          7728*m2^5*Ycut^7 - 1904*m2^6*Ycut^7 - 160*m2^7*Ycut^7 - 
          739962*MBhat*Ycut^7 + 486864*m2*MBhat*Ycut^7 + 13080564*m2^2*MBhat*
           Ycut^7 + 1439340*m2^3*MBhat*Ycut^7 - 1038870*m2^4*MBhat*Ycut^7 + 
          66276*m2^5*MBhat*Ycut^7 + 5628*m2^6*MBhat*Ycut^7 - 
          1152*m2^7*MBhat*Ycut^7 + 2121840*MBhat^2*Ycut^7 - 
          1770048*m2*MBhat^2*Ycut^7 - 11727660*m2^2*MBhat^2*Ycut^7 + 
          2582160*m2^3*MBhat^2*Ycut^7 - 184380*m2^4*MBhat^2*Ycut^7 - 
          9408*m2^5*MBhat^2*Ycut^7 + 21168*m2^6*MBhat^2*Ycut^7 - 
          3360*m2^7*MBhat^2*Ycut^7 - 2648682*MBhat^3*Ycut^7 + 
          1661352*m2*MBhat^3*Ycut^7 + 3935820*m2^2*MBhat^3*Ycut^7 - 
          1276240*m2^3*MBhat^3*Ycut^7 + 65590*m2^4*MBhat^3*Ycut^7 - 
          23688*m2^5*MBhat^3*Ycut^7 + 28056*m2^6*MBhat^3*Ycut^7 - 
          5120*m2^7*MBhat^3*Ycut^7 + 1561056*MBhat^4*Ycut^7 - 
          630000*m2*MBhat^4*Ycut^7 + 54600*m2^2*MBhat^4*Ycut^7 - 
          117600*m2^3*MBhat^4*Ycut^7 - 35280*m2^4*MBhat^4*Ycut^7 + 
          60144*m2^5*MBhat^4*Ycut^7 - 11760*m2^6*MBhat^4*Ycut^7 - 
          469392*MBhat^5*Ycut^7 + 96600*m2*MBhat^5*Ycut^7 - 
          183960*m2^2*MBhat^5*Ycut^7 + 41580*m2^3*MBhat^5*Ycut^7 + 
          28140*m2^4*MBhat^5*Ycut^7 - 8064*m2^5*MBhat^5*Ycut^7 + 
          57120*MBhat^6*Ycut^7 + 15680*m2*MBhat^6*Ycut^7 + 
          16380*m2^2*MBhat^6*Ycut^7 - 1120*m2^3*MBhat^6*Ycut^7 - 
          1400*m2^4*MBhat^6*Ycut^7 - 44028*Ycut^8 + 37324*m2*Ycut^8 + 
          787710*m2^2*Ycut^8 + 677180*m2^3*Ycut^8 - 26460*m2^4*Ycut^8 - 
          7392*m2^5*Ycut^8 + 238*m2^6*Ycut^8 + 20*m2^7*Ycut^8 - 
          33048*MBhat*Ycut^8 + 220752*m2*MBhat*Ycut^8 - 2195928*m2^2*MBhat*
           Ycut^8 - 779520*m2^3*MBhat*Ycut^8 + 168840*m2^4*MBhat*Ycut^8 - 
          10080*m2^5*MBhat*Ycut^8 - 1176*m2^6*MBhat*Ycut^8 + 
          144*m2^7*MBhat*Ycut^8 - 390159*MBhat^2*Ycut^8 + 
          298368*m2*MBhat^2*Ycut^8 + 1987125*m2^2*MBhat^2*Ycut^8 - 
          136920*m2^3*MBhat^2*Ycut^8 + 21315*m2^4*MBhat^2*Ycut^8 + 
          4200*m2^5*MBhat^2*Ycut^8 - 4158*m2^6*MBhat^2*Ycut^8 + 
          420*m2^7*MBhat^2*Ycut^8 + 632304*MBhat^3*Ycut^8 - 
          228144*m2*MBhat^3*Ycut^8 - 903840*m2^2*MBhat^3*Ycut^8 + 
          210560*m2^3*MBhat^3*Ycut^8 - 8120*m2^4*MBhat^3*Ycut^8 + 
          7056*m2^5*MBhat^3*Ycut^8 - 5712*m2^6*MBhat^3*Ycut^8 + 
          640*m2^7*MBhat^3*Ycut^8 - 224616*MBhat^4*Ycut^8 - 
          51660*m2*MBhat^4*Ycut^8 + 50820*m2^2*MBhat^4*Ycut^8 + 
          15960*m2^3*MBhat^4*Ycut^8 + 11970*m2^4*MBhat^4*Ycut^8 - 
          12054*m2^5*MBhat^4*Ycut^8 + 1470*m2^6*MBhat^4*Ycut^8 + 
          3864*MBhat^5*Ycut^8 + 28560*m2*MBhat^5*Ycut^8 + 
          35280*m2^2*MBhat^5*Ycut^8 - 5040*m2^3*MBhat^5*Ycut^8 - 
          5880*m2^4*MBhat^5*Ycut^8 + 1008*m2^5*MBhat^5*Ycut^8 + 
          10815*MBhat^6*Ycut^8 - 12040*m2*MBhat^6*Ycut^8 - 
          4410*m2^2*MBhat^6*Ycut^8 + 140*m2^3*MBhat^6*Ycut^8 + 
          175*m2^4*MBhat^6*Ycut^8 + 43792*Ycut^9 - 59360*m2*Ycut^9 - 
          17920*m2^2*Ycut^9 - 10080*m2^3*Ycut^9 - 16240*m2^4*Ycut^9 + 
          1568*m2^5*Ycut^9 + 164766*MBhat*Ycut^9 - 227640*m2*MBhat*Ycut^9 + 
          88830*m2^2*MBhat*Ycut^9 + 103320*m2^3*MBhat*Ycut^9 - 
          13125*m2^4*MBhat*Ycut^9 + 504*m2^5*MBhat*Ycut^9 + 
          105*m2^6*MBhat*Ycut^9 - 13608*MBhat^2*Ycut^9 - 
          72576*m2*MBhat^2*Ycut^9 + 23520*m2^2*MBhat^2*Ycut^9 - 
          40320*m2^3*MBhat^2*Ycut^9 + 840*m2^4*MBhat^2*Ycut^9 - 
          672*m2^5*MBhat^2*Ycut^9 + 336*m2^6*MBhat^2*Ycut^9 - 
          194040*MBhat^3*Ycut^9 + 79100*m2*MBhat^3*Ycut^9 + 
          83720*m2^2*MBhat^3*Ycut^9 - 16520*m2^3*MBhat^3*Ycut^9 + 
          140*m2^4*MBhat^3*Ycut^9 - 910*m2^5*MBhat^3*Ycut^9 + 
          490*m2^6*MBhat^3*Ycut^9 + 27552*MBhat^4*Ycut^9 + 
          38640*m2*MBhat^4*Ycut^9 - 15120*m2^2*MBhat^4*Ycut^9 - 
          1680*m2^4*MBhat^4*Ycut^9 + 1008*m2^5*MBhat^4*Ycut^9 + 
          23730*MBhat^5*Ycut^9 - 1680*m2*MBhat^5*Ycut^9 - 
          3360*m2^2*MBhat^5*Ycut^9 + 525*m2^4*MBhat^5*Ycut^9 - 
          8120*MBhat^6*Ycut^9 + 2240*m2*MBhat^6*Ycut^9 + 
          560*m2^2*MBhat^6*Ycut^9 - 52892*Ycut^10 + 60032*m2*Ycut^10 - 
          3990*m2^2*Ycut^10 - 15680*m2^3*Ycut^10 + 1890*m2^4*Ycut^10 - 
          109032*MBhat*Ycut^10 + 122976*m2*MBhat*Ycut^10 - 
          20664*m2^2*MBhat*Ycut^10 + 3360*m2^3*MBhat*Ycut^10 + 
          71316*MBhat^2*Ycut^10 + 9240*m2*MBhat^2*Ycut^10 - 
          46620*m2^2*MBhat^2*Ycut^10 + 5040*m2^3*MBhat^2*Ycut^10 + 
          75264*MBhat^3*Ycut^10 - 34608*m2*MBhat^3*Ycut^10 - 
          1680*m2^2*MBhat^3*Ycut^10 - 15540*MBhat^4*Ycut^10 - 
          12600*m2*MBhat^4*Ycut^10 + 1890*m2^2*MBhat^4*Ycut^10 - 
          1848*MBhat^5*Ycut^10 - 3360*m2*MBhat^5*Ycut^10 + 
          700*MBhat^6*Ycut^10 + 42448*Ycut^11 - 36736*m2*Ycut^11 + 
          1680*m2^2*Ycut^11 + 1120*m2^3*Ycut^11 + 40152*MBhat*Ycut^11 - 
          41328*m2*MBhat*Ycut^11 + 12852*m2^2*MBhat*Ycut^11 - 
          420*m2^3*MBhat*Ycut^11 - 49896*MBhat^2*Ycut^11 + 
          4704*m2*MBhat^2*Ycut^11 + 5040*m2^2*MBhat^2*Ycut^11 - 
          18522*MBhat^3*Ycut^11 + 9744*m2*MBhat^3*Ycut^11 + 
          210*m2^2*MBhat^3*Ycut^11 + 2352*MBhat^4*Ycut^11 + 
          5040*m2*MBhat^4*Ycut^11 + 294*MBhat^5*Ycut^11 + 
          420*m2*MBhat^5*Ycut^11 + 280*MBhat^6*Ycut^11 - 22512*Ycut^12 + 
          13776*m2*Ycut^12 - 910*m2^2*Ycut^12 - 140*m2^3*Ycut^12 - 
          5544*MBhat*Ycut^12 + 7728*m2*MBhat*Ycut^12 - 1512*m2^2*MBhat*
           Ycut^12 + 18417*MBhat^2*Ycut^12 - 1932*m2*MBhat^2*Ycut^12 - 
          630*m2^2*MBhat^2*Ycut^12 + 3752*MBhat^3*Ycut^12 - 
          3024*m2*MBhat^3*Ycut^12 - 126*MBhat^4*Ycut^12 - 
          630*m2*MBhat^4*Ycut^12 - 504*MBhat^5*Ycut^12 - 35*MBhat^6*Ycut^12 + 
          7728*Ycut^13 - 2912*m2*Ycut^13 - 1575*MBhat*Ycut^13 - 
          756*m2*MBhat*Ycut^13 + 189*m2^2*MBhat*Ycut^13 - 
          4200*MBhat^2*Ycut^13 + 672*m2*MBhat^2*Ycut^13 - 
          644*MBhat^3*Ycut^13 + 378*m2*MBhat^3*Ycut^13 + 
          336*MBhat^4*Ycut^13 + 63*MBhat^5*Ycut^13 - 1602*Ycut^14 + 
          280*m2*Ycut^14 + 864*MBhat*Ycut^14 + 588*MBhat^2*Ycut^14 - 
          84*m2*MBhat^2*Ycut^14 - 80*MBhat^3*Ycut^14 - 42*MBhat^4*Ycut^14 + 
          160*Ycut^15 - 150*MBhat*Ycut^15 + 10*MBhat^3*Ycut^15)/
         (-1 + Ycut)^8 + ((8 + 32*m2 - 420*m2^2 - 740*m2^3 - 140*m2^4 + 
          12*m2^5 - 48*MBhat - 144*m2*MBhat + 1296*m2^2*MBhat + 
          1200*m2^3*MBhat + 120*MBhat^2 + 264*m2*MBhat^2 - 
          1461*m2^2*MBhat^2 - 516*m2^3*MBhat^2 + 39*m2^4*MBhat^2 - 
          160*MBhat^3 - 256*m2*MBhat^3 + 720*m2^2*MBhat^3 + 120*MBhat^4 + 
          144*m2*MBhat^4 - 138*m2^2*MBhat^4 + 24*m2^3*MBhat^4 - 48*MBhat^5 - 
          48*m2*MBhat^5 + 8*MBhat^6 + 8*m2*MBhat^6 + 3*m2^2*MBhat^6)*Log[m2])/
        2 + ((-8 - 32*m2 + 420*m2^2 + 740*m2^3 + 140*m2^4 - 12*m2^5 + 
          48*MBhat + 144*m2*MBhat - 1296*m2^2*MBhat - 1200*m2^3*MBhat - 
          120*MBhat^2 - 264*m2*MBhat^2 + 1461*m2^2*MBhat^2 + 
          516*m2^3*MBhat^2 - 39*m2^4*MBhat^2 + 160*MBhat^3 + 256*m2*MBhat^3 - 
          720*m2^2*MBhat^3 - 120*MBhat^4 - 144*m2*MBhat^4 + 
          138*m2^2*MBhat^4 - 24*m2^3*MBhat^4 + 48*MBhat^5 + 48*m2*MBhat^5 - 
          8*MBhat^6 - 8*m2*MBhat^6 - 3*m2^2*MBhat^6)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/840*(-12340 + 36092*m2 + 310380*m2^2 - 147700*m2^3 - 
          193900*m2^4 + 7140*m2^5 + 308*m2^6 + 20*m2^7 + 69072*MBhat - 
          184464*m2*MBhat - 698544*m2^2*MBhat + 697200*m2^3*MBhat + 
          126000*m2^4*MBhat - 9072*m2^5*MBhat - 336*m2^6*MBhat + 
          144*m2^7*MBhat - 158949*MBhat^2 + 373800*m2*MBhat^2 + 
          419055*m2^2*MBhat^2 - 674100*m2^3*MBhat^2 + 42525*m2^4*MBhat^2 - 
          1176*m2^5*MBhat^2 - 1575*m2^6*MBhat^2 + 420*m2^7*MBhat^2 + 
          192000*MBhat^3 - 381696*m2*MBhat^3 + 45360*m2^2*MBhat^3 + 
          156800*m2^3*MBhat^3 - 11200*m2^4*MBhat^3 - 1904*m2^6*MBhat^3 + 
          640*m2^7*MBhat^3 - 128940*MBhat^4 + 209160*m2*MBhat^4 - 
          99750*m2^2*MBhat^4 + 23520*m2^3*MBhat^4 - 1260*m2^4*MBhat^4 - 
          4200*m2^5*MBhat^4 + 1470*m2^6*MBhat^4 + 46032*MBhat^5 - 
          58800*m2*MBhat^5 + 20160*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 - 
          1680*m2^4*MBhat^5 + 1008*m2^5*MBhat^5 - 6895*MBhat^6 + 
          6440*m2*MBhat^6 + 280*m2^3*MBhat^6 + 175*m2^4*MBhat^6 + 
          102080*Ycut - 275296*m2*Ycut - 2659440*m2^2*Ycut + 
          870800*m2^3*Ycut + 1492400*m2^4*Ycut - 52080*m2^5*Ycut - 
          2464*m2^6*Ycut - 160*m2^7*Ycut - 572736*MBhat*Ycut + 
          1415232*m2*MBhat*Ycut + 6132672*m2^2*MBhat*Ycut - 
          5073600*m2^3*MBhat*Ycut - 1008000*m2^4*MBhat*Ycut + 
          72576*m2^5*MBhat*Ycut + 2688*m2^6*MBhat*Ycut - 
          1152*m2^7*MBhat*Ycut + 1321992*MBhat^2*Ycut - 2879520*m2*MBhat^2*
           Ycut - 3966060*m2^2*MBhat^2*Ycut + 5176080*m2^3*MBhat^2*Ycut - 
          323820*m2^4*MBhat^2*Ycut + 9408*m2^5*MBhat^2*Ycut + 
          12600*m2^6*MBhat^2*Ycut - 3360*m2^7*MBhat^2*Ycut - 
          1603200*MBhat^3*Ycut + 2946048*m2*MBhat^3*Ycut - 
          60480*m2^2*MBhat^3*Ycut - 1254400*m2^3*MBhat^3*Ycut + 
          89600*m2^4*MBhat^3*Ycut + 15232*m2^6*MBhat^3*Ycut - 
          5120*m2^7*MBhat^3*Ycut + 1081920*MBhat^4*Ycut - 
          1612800*m2*MBhat^4*Ycut + 740040*m2^2*MBhat^4*Ycut - 
          178080*m2^3*MBhat^4*Ycut + 10080*m2^4*MBhat^4*Ycut + 
          33600*m2^5*MBhat^4*Ycut - 11760*m2^6*MBhat^4*Ycut - 
          388416*MBhat^5*Ycut + 450240*m2*MBhat^5*Ycut - 161280*m2^2*MBhat^5*
           Ycut + 53760*m2^3*MBhat^5*Ycut + 13440*m2^4*MBhat^5*Ycut - 
          8064*m2^5*MBhat^5*Ycut + 58520*MBhat^6*Ycut - 48160*m2*MBhat^6*
           Ycut + 1260*m2^2*MBhat^6*Ycut - 2240*m2^3*MBhat^6*Ycut - 
          1400*m2^4*MBhat^6*Ycut - 370720*Ycut^2 + 909776*m2*Ycut^2 + 
          10013640*m2^2*Ycut^2 - 1804600*m2^3*Ycut^2 - 4988200*m2^4*Ycut^2 + 
          162120*m2^5*Ycut^2 + 8624*m2^6*Ycut^2 + 560*m2^7*Ycut^2 + 
          2085216*MBhat*Ycut^2 - 4711392*m2*MBhat*Ycut^2 - 
          23641632*m2^2*MBhat*Ycut^2 + 15741600*m2^3*MBhat*Ycut^2 + 
          3528000*m2^4*MBhat*Ycut^2 - 254016*m2^5*MBhat*Ycut^2 - 
          9408*m2^6*MBhat*Ycut^2 + 4032*m2^7*MBhat*Ycut^2 - 
          4828572*MBhat^2*Ycut^2 + 9634800*m2*MBhat^2*Ycut^2 + 
          16335690*m2^2*MBhat^2*Ycut^2 - 17249400*m2^3*MBhat^2*Ycut^2 + 
          1067850*m2^4*MBhat^2*Ycut^2 - 32928*m2^5*MBhat^2*Ycut^2 - 
          44100*m2^6*MBhat^2*Ycut^2 + 11760*m2^7*MBhat^2*Ycut^2 + 
          5880000*MBhat^3*Ycut^2 - 9881088*m2*MBhat^3*Ycut^2 - 
          997920*m2^2*MBhat^3*Ycut^2 + 4390400*m2^3*MBhat^3*Ycut^2 - 
          313600*m2^4*MBhat^3*Ycut^2 - 53312*m2^6*MBhat^3*Ycut^2 + 
          17920*m2^7*MBhat^3*Ycut^2 - 3988320*MBhat^4*Ycut^2 + 
          5402880*m2*MBhat^4*Ycut^2 - 2358300*m2^2*MBhat^4*Ycut^2 + 
          582960*m2^3*MBhat^4*Ycut^2 - 35280*m2^4*MBhat^4*Ycut^2 - 
          117600*m2^5*MBhat^4*Ycut^2 + 41160*m2^6*MBhat^4*Ycut^2 + 
          1440096*MBhat^5*Ycut^2 - 1495200*m2*MBhat^5*Ycut^2 + 
          564480*m2^2*MBhat^5*Ycut^2 - 188160*m2^3*MBhat^5*Ycut^2 - 
          47040*m2^4*MBhat^5*Ycut^2 + 28224*m2^5*MBhat^5*Ycut^2 - 
          218260*MBhat^6*Ycut^2 + 155120*m2*MBhat^6*Ycut^2 - 
          9450*m2^2*MBhat^6*Ycut^2 + 7840*m2^3*MBhat^6*Ycut^2 + 
          4900*m2^4*MBhat^6*Ycut^2 + 772800*Ycut^3 - 1694112*m2*Ycut^3 - 
          21673680*m2^2*Ycut^3 + 708400*m2^3*Ycut^3 + 9427600*m2^4*Ycut^3 - 
          277200*m2^5*Ycut^3 - 17248*m2^6*Ycut^3 - 1120*m2^7*Ycut^3 - 
          4358592*MBhat*Ycut^3 + 8858304*m2*MBhat*Ycut^3 + 
          52363584*m2^2*MBhat*Ycut^3 - 26779200*m2^3*MBhat*Ycut^3 - 
          7056000*m2^4*MBhat*Ycut^3 + 508032*m2^5*MBhat*Ycut^3 + 
          18816*m2^6*MBhat*Ycut^3 - 8064*m2^7*MBhat*Ycut^3 + 
          10127544*MBhat^2*Ycut^3 - 18234720*m2*MBhat^2*Ycut^3 - 
          38398500*m2^2*MBhat^2*Ycut^3 + 32476080*m2^3*MBhat^2*Ycut^3 - 
          1982820*m2^4*MBhat^2*Ycut^3 + 65856*m2^5*MBhat^2*Ycut^3 + 
          88200*m2^6*MBhat^2*Ycut^3 - 23520*m2^7*MBhat^2*Ycut^3 - 
          12382790*MBhat^3*Ycut^3 + 18748856*m2*MBhat^3*Ycut^3 + 
          4820690*m2^2*MBhat^3*Ycut^3 - 8780800*m2^3*MBhat^3*Ycut^3 + 
          644350*m2^4*MBhat^3*Ycut^3 - 21560*m2^5*MBhat^3*Ycut^3 + 
          113974*m2^6*MBhat^3*Ycut^3 - 35840*m2^7*MBhat^3*Ycut^3 + 
          8438640*MBhat^4*Ycut^3 - 10227840*m2*MBhat^4*Ycut^3 + 
          4172280*m2^2*MBhat^4*Ycut^3 - 1058400*m2^3*MBhat^4*Ycut^3 + 
          42000*m2^4*MBhat^4*Ycut^3 + 248640*m2^5*MBhat^4*Ycut^3 - 
          82320*m2^6*MBhat^4*Ycut^3 - 3063102*MBhat^5*Ycut^3 + 
          2795940*m2*MBhat^5*Ycut^3 - 1124760*m2^2*MBhat^5*Ycut^3 + 
          365820*m2^3*MBhat^5*Ycut^3 + 101430*m2^4*MBhat^5*Ycut^3 - 
          56448*m2^5*MBhat^5*Ycut^3 + 466760*MBhat^6*Ycut^3 - 
          277760*m2*MBhat^6*Ycut^3 + 29540*m2^2*MBhat^6*Ycut^3 - 
          14560*m2^3*MBhat^6*Ycut^3 - 9800*m2^4*MBhat^6*Ycut^3 - 
          1013040*Ycut^4 + 1929480*m2*Ycut^4 + 29561700*m2^2*Ycut^4 + 
          3465700*m2^3*Ycut^4 - 10961300*m2^4*Ycut^4 + 275940*m2^5*Ycut^4 + 
          21560*m2^6*Ycut^4 + 1400*m2^7*Ycut^4 + 5730480*MBhat*Ycut^4 - 
          10226160*m2*MBhat*Ycut^4 - 73074960*m2^2*MBhat*Ycut^4 + 
          26418000*m2^3*MBhat*Ycut^4 + 8820000*m2^4*MBhat*Ycut^4 - 
          635040*m2^5*MBhat*Ycut^4 - 23520*m2^6*MBhat*Ycut^4 + 
          10080*m2^7*MBhat*Ycut^4 - 13371645*MBhat^2*Ycut^4 + 
          21255780*m2*MBhat^2*Ycut^4 + 56585130*m2^2*MBhat^2*Ycut^4 - 
          37561020*m2^3*MBhat^2*Ycut^4 + 2223480*m2^4*MBhat^2*Ycut^4 - 
          49980*m2^5*MBhat^2*Ycut^4 - 121275*m2^6*MBhat^2*Ycut^4 + 
          29400*m2^7*MBhat^2*Ycut^4 + 16403240*MBhat^3*Ycut^4 - 
          21893760*m2*MBhat^3*Ycut^4 - 10263120*m2^2*MBhat^3*Ycut^4 + 
          10935680*m2^3*MBhat^3*Ycut^4 - 792120*m2^4*MBhat^3*Ycut^4 + 
          47040*m2^5*MBhat^3*Ycut^4 - 152880*m2^6*MBhat^3*Ycut^4 + 
          44800*m2^7*MBhat^3*Ycut^4 - 11208750*MBhat^4*Ycut^4 + 
          11873610*m2*MBhat^4*Ycut^4 - 4406430*m2^2*MBhat^4*Ycut^4 + 
          1175160*m2^3*MBhat^4*Ycut^4 + 630*m2^4*MBhat^4*Ycut^4 - 
          335370*m2^5*MBhat^4*Ycut^4 + 102900*m2^6*MBhat^4*Ycut^4 + 
          4080720*MBhat^5*Ycut^4 - 3181920*m2*MBhat^5*Ycut^4 + 
          1396080*m2^2*MBhat^5*Ycut^4 - 431760*m2^3*MBhat^5*Ycut^4 - 
          142800*m2^4*MBhat^5*Ycut^4 + 70560*m2^5*MBhat^5*Ycut^4 - 
          623805*MBhat^6*Ycut^4 + 294560*m2*MBhat^6*Ycut^4 - 
          51030*m2^2*MBhat^6*Ycut^4 + 15540*m2^3*MBhat^6*Ycut^4 + 
          12250*m2^4*MBhat^6*Ycut^4 + 857472*Ycut^5 - 1355424*m2*Ycut^5 - 
          26118960*m2^2*Ycut^5 - 7123760*m2^3*Ycut^5 + 7945840*m2^4*Ycut^5 - 
          150192*m2^5*Ycut^5 - 17248*m2^6*Ycut^5 - 1120*m2^7*Ycut^5 - 
          4862655*MBhat*Ycut^5 + 7325388*m2*MBhat*Ycut^5 + 
          66082653*m2^2*MBhat*Ycut^5 - 14078400*m2^3*MBhat*Ycut^5 - 
          7040565*m2^4*MBhat*Ycut^5 + 488628*m2^5*MBhat*Ycut^5 + 
          25431*m2^6*MBhat*Ycut^5 - 8064*m2^7*MBhat*Ycut^5 + 
          11440968*MBhat^2*Ycut^5 - 15529920*m2*MBhat^2*Ycut^5 - 
          53849460*m2^2*MBhat^2*Ycut^5 + 27063120*m2^3*MBhat^2*Ycut^5 - 
          1545180*m2^4*MBhat^2*Ycut^5 + 8736*m2^5*MBhat^2*Ycut^5 + 
          105840*m2^6*MBhat^2*Ycut^5 - 23520*m2^7*MBhat^2*Ycut^5 - 
          14054404*MBhat^3*Ycut^5 + 15962394*m2*MBhat^3*Ycut^5 + 
          12459720*m2^2*MBhat^3*Ycut^5 - 8664040*m2^3*MBhat^3*Ycut^5 + 
          574560*m2^4*MBhat^3*Ycut^5 - 52626*m2^5*MBhat^3*Ycut^5 + 
          134652*m2^6*MBhat^3*Ycut^5 - 35840*m2^7*MBhat^3*Ycut^5 + 
          9564240*MBhat^4*Ycut^5 - 8512560*m2*MBhat^4*Ycut^5 + 
          2745960*m2^2*MBhat^4*Ycut^5 - 843360*m2^3*MBhat^4*Ycut^5 - 
          45360*m2^4*MBhat^4*Ycut^5 + 294000*m2^5*MBhat^4*Ycut^5 - 
          82320*m2^6*MBhat^4*Ycut^5 - 3465609*MBhat^5*Ycut^5 + 
          2195760*m2*MBhat^5*Ycut^5 - 1113840*m2^2*MBhat^5*Ycut^5 + 
          321720*m2^3*MBhat^5*Ycut^5 + 130305*m2^4*MBhat^5*Ycut^5 - 
          56448*m2^5*MBhat^5*Ycut^5 + 527520*MBhat^6*Ycut^5 - 
          175840*m2*MBhat^6*Ycut^5 + 54180*m2^2*MBhat^6*Ycut^5 - 
          10080*m2^3*MBhat^6*Ycut^5 - 9800*m2^4*MBhat^6*Ycut^5 - 
          460978*Ycut^6 + 554232*m2*Ycut^6 + 14705390*m2^2*Ycut^6 + 
          6462680*m2^3*Ycut^6 - 3427550*m2^4*Ycut^6 + 32368*m2^5*Ycut^6 + 
          7154*m2^6*Ycut^6 + 560*m2^7*Ycut^6 + 2592744*MBhat*Ycut^6 - 
          3045504*m2*MBhat*Ycut^6 - 38128104*m2^2*MBhat*Ycut^6 + 
          2308320*m2^3*MBhat*Ycut^6 + 3513720*m2^4*MBhat*Ycut^6 - 
          227808*m2^5*MBhat*Ycut^6 - 15288*m2^6*MBhat*Ycut^6 + 
          4032*m2^7*MBhat*Ycut^6 - 6285636*MBhat^2*Ycut^6 + 
          6911436*m2*MBhat^2*Ycut^6 + 32633370*m2^2*MBhat^2*Ycut^6 - 
          11640720*m2^3*MBhat^2*Ycut^6 + 680190*m2^4*MBhat^2*Ycut^6 + 
          5964*m2^5*MBhat^2*Ycut^6 - 59976*m2^6*MBhat^2*Ycut^6 + 
          11760*m2^7*MBhat^2*Ycut^6 + 7715792*MBhat^3*Ycut^6 - 
          6985552*m2*MBhat^3*Ycut^6 - 9118480*m2^2*MBhat^3*Ycut^6 + 
          4298560*m2^3*MBhat^3*Ycut^6 - 249200*m2^4*MBhat^3*Ycut^6 + 
          44688*m2^5*MBhat^3*Ycut^6 - 77616*m2^6*MBhat^3*Ycut^6 + 
          17920*m2^7*MBhat^3*Ycut^6 - 5109762*MBhat^4*Ycut^6 + 
          3518760*m2*MBhat^4*Ycut^6 - 885990*m2^2*MBhat^4*Ycut^6 + 
          399840*m2^3*MBhat^4*Ycut^6 + 54180*m2^4*MBhat^4*Ycut^6 - 
          168168*m2^5*MBhat^4*Ycut^6 + 41160*m2^6*MBhat^4*Ycut^6 + 
          1794072*MBhat^5*Ycut^6 - 826560*m2*MBhat^5*Ycut^6 + 
          571200*m2^2*MBhat^5*Ycut^6 - 151200*m2^3*MBhat^5*Ycut^6 - 
          76440*m2^4*MBhat^5*Ycut^6 + 28224*m2^5*MBhat^5*Ycut^6 - 
          264600*MBhat^6*Ycut^6 + 39760*m2*MBhat^6*Ycut^6 - 
          37030*m2^2*MBhat^6*Ycut^6 + 4200*m2^3*MBhat^6*Ycut^6 + 
          4900*m2^4*MBhat^6*Ycut^6 + 151632*Ycut^7 - 117152*m2*Ycut^7 - 
          4905600*m2^2*Ycut^7 - 3083920*m2^3*Ycut^7 + 745920*m2^4*Ycut^7 + 
          7728*m2^5*Ycut^7 - 1904*m2^6*Ycut^7 - 160*m2^7*Ycut^7 - 
          739962*MBhat*Ycut^7 + 486864*m2*MBhat*Ycut^7 + 13080564*m2^2*MBhat*
           Ycut^7 + 1439340*m2^3*MBhat*Ycut^7 - 1038870*m2^4*MBhat*Ycut^7 + 
          66276*m2^5*MBhat*Ycut^7 + 5628*m2^6*MBhat*Ycut^7 - 
          1152*m2^7*MBhat*Ycut^7 + 2121840*MBhat^2*Ycut^7 - 
          1770048*m2*MBhat^2*Ycut^7 - 11727660*m2^2*MBhat^2*Ycut^7 + 
          2582160*m2^3*MBhat^2*Ycut^7 - 184380*m2^4*MBhat^2*Ycut^7 - 
          9408*m2^5*MBhat^2*Ycut^7 + 21168*m2^6*MBhat^2*Ycut^7 - 
          3360*m2^7*MBhat^2*Ycut^7 - 2648682*MBhat^3*Ycut^7 + 
          1661352*m2*MBhat^3*Ycut^7 + 3935820*m2^2*MBhat^3*Ycut^7 - 
          1276240*m2^3*MBhat^3*Ycut^7 + 65590*m2^4*MBhat^3*Ycut^7 - 
          23688*m2^5*MBhat^3*Ycut^7 + 28056*m2^6*MBhat^3*Ycut^7 - 
          5120*m2^7*MBhat^3*Ycut^7 + 1561056*MBhat^4*Ycut^7 - 
          630000*m2*MBhat^4*Ycut^7 + 54600*m2^2*MBhat^4*Ycut^7 - 
          117600*m2^3*MBhat^4*Ycut^7 - 35280*m2^4*MBhat^4*Ycut^7 + 
          60144*m2^5*MBhat^4*Ycut^7 - 11760*m2^6*MBhat^4*Ycut^7 - 
          469392*MBhat^5*Ycut^7 + 96600*m2*MBhat^5*Ycut^7 - 
          183960*m2^2*MBhat^5*Ycut^7 + 41580*m2^3*MBhat^5*Ycut^7 + 
          28140*m2^4*MBhat^5*Ycut^7 - 8064*m2^5*MBhat^5*Ycut^7 + 
          57120*MBhat^6*Ycut^7 + 15680*m2*MBhat^6*Ycut^7 + 
          16380*m2^2*MBhat^6*Ycut^7 - 1120*m2^3*MBhat^6*Ycut^7 - 
          1400*m2^4*MBhat^6*Ycut^7 - 44028*Ycut^8 + 37324*m2*Ycut^8 + 
          787710*m2^2*Ycut^8 + 677180*m2^3*Ycut^8 - 26460*m2^4*Ycut^8 - 
          7392*m2^5*Ycut^8 + 238*m2^6*Ycut^8 + 20*m2^7*Ycut^8 - 
          33048*MBhat*Ycut^8 + 220752*m2*MBhat*Ycut^8 - 2195928*m2^2*MBhat*
           Ycut^8 - 779520*m2^3*MBhat*Ycut^8 + 168840*m2^4*MBhat*Ycut^8 - 
          10080*m2^5*MBhat*Ycut^8 - 1176*m2^6*MBhat*Ycut^8 + 
          144*m2^7*MBhat*Ycut^8 - 390159*MBhat^2*Ycut^8 + 
          298368*m2*MBhat^2*Ycut^8 + 1987125*m2^2*MBhat^2*Ycut^8 - 
          136920*m2^3*MBhat^2*Ycut^8 + 21315*m2^4*MBhat^2*Ycut^8 + 
          4200*m2^5*MBhat^2*Ycut^8 - 4158*m2^6*MBhat^2*Ycut^8 + 
          420*m2^7*MBhat^2*Ycut^8 + 632304*MBhat^3*Ycut^8 - 
          228144*m2*MBhat^3*Ycut^8 - 903840*m2^2*MBhat^3*Ycut^8 + 
          210560*m2^3*MBhat^3*Ycut^8 - 8120*m2^4*MBhat^3*Ycut^8 + 
          7056*m2^5*MBhat^3*Ycut^8 - 5712*m2^6*MBhat^3*Ycut^8 + 
          640*m2^7*MBhat^3*Ycut^8 - 224616*MBhat^4*Ycut^8 - 
          51660*m2*MBhat^4*Ycut^8 + 50820*m2^2*MBhat^4*Ycut^8 + 
          15960*m2^3*MBhat^4*Ycut^8 + 11970*m2^4*MBhat^4*Ycut^8 - 
          12054*m2^5*MBhat^4*Ycut^8 + 1470*m2^6*MBhat^4*Ycut^8 + 
          3864*MBhat^5*Ycut^8 + 28560*m2*MBhat^5*Ycut^8 + 
          35280*m2^2*MBhat^5*Ycut^8 - 5040*m2^3*MBhat^5*Ycut^8 - 
          5880*m2^4*MBhat^5*Ycut^8 + 1008*m2^5*MBhat^5*Ycut^8 + 
          10815*MBhat^6*Ycut^8 - 12040*m2*MBhat^6*Ycut^8 - 
          4410*m2^2*MBhat^6*Ycut^8 + 140*m2^3*MBhat^6*Ycut^8 + 
          175*m2^4*MBhat^6*Ycut^8 + 43792*Ycut^9 - 59360*m2*Ycut^9 - 
          17920*m2^2*Ycut^9 - 10080*m2^3*Ycut^9 - 16240*m2^4*Ycut^9 + 
          1568*m2^5*Ycut^9 + 164766*MBhat*Ycut^9 - 227640*m2*MBhat*Ycut^9 + 
          88830*m2^2*MBhat*Ycut^9 + 103320*m2^3*MBhat*Ycut^9 - 
          13125*m2^4*MBhat*Ycut^9 + 504*m2^5*MBhat*Ycut^9 + 
          105*m2^6*MBhat*Ycut^9 - 13608*MBhat^2*Ycut^9 - 
          72576*m2*MBhat^2*Ycut^9 + 23520*m2^2*MBhat^2*Ycut^9 - 
          40320*m2^3*MBhat^2*Ycut^9 + 840*m2^4*MBhat^2*Ycut^9 - 
          672*m2^5*MBhat^2*Ycut^9 + 336*m2^6*MBhat^2*Ycut^9 - 
          194040*MBhat^3*Ycut^9 + 79100*m2*MBhat^3*Ycut^9 + 
          83720*m2^2*MBhat^3*Ycut^9 - 16520*m2^3*MBhat^3*Ycut^9 + 
          140*m2^4*MBhat^3*Ycut^9 - 910*m2^5*MBhat^3*Ycut^9 + 
          490*m2^6*MBhat^3*Ycut^9 + 27552*MBhat^4*Ycut^9 + 
          38640*m2*MBhat^4*Ycut^9 - 15120*m2^2*MBhat^4*Ycut^9 - 
          1680*m2^4*MBhat^4*Ycut^9 + 1008*m2^5*MBhat^4*Ycut^9 + 
          23730*MBhat^5*Ycut^9 - 1680*m2*MBhat^5*Ycut^9 - 
          3360*m2^2*MBhat^5*Ycut^9 + 525*m2^4*MBhat^5*Ycut^9 - 
          8120*MBhat^6*Ycut^9 + 2240*m2*MBhat^6*Ycut^9 + 
          560*m2^2*MBhat^6*Ycut^9 - 52892*Ycut^10 + 60032*m2*Ycut^10 - 
          3990*m2^2*Ycut^10 - 15680*m2^3*Ycut^10 + 1890*m2^4*Ycut^10 - 
          109032*MBhat*Ycut^10 + 122976*m2*MBhat*Ycut^10 - 
          20664*m2^2*MBhat*Ycut^10 + 3360*m2^3*MBhat*Ycut^10 + 
          71316*MBhat^2*Ycut^10 + 9240*m2*MBhat^2*Ycut^10 - 
          46620*m2^2*MBhat^2*Ycut^10 + 5040*m2^3*MBhat^2*Ycut^10 + 
          75264*MBhat^3*Ycut^10 - 34608*m2*MBhat^3*Ycut^10 - 
          1680*m2^2*MBhat^3*Ycut^10 - 15540*MBhat^4*Ycut^10 - 
          12600*m2*MBhat^4*Ycut^10 + 1890*m2^2*MBhat^4*Ycut^10 - 
          1848*MBhat^5*Ycut^10 - 3360*m2*MBhat^5*Ycut^10 + 
          700*MBhat^6*Ycut^10 + 42448*Ycut^11 - 36736*m2*Ycut^11 + 
          1680*m2^2*Ycut^11 + 1120*m2^3*Ycut^11 + 40152*MBhat*Ycut^11 - 
          41328*m2*MBhat*Ycut^11 + 12852*m2^2*MBhat*Ycut^11 - 
          420*m2^3*MBhat*Ycut^11 - 49896*MBhat^2*Ycut^11 + 
          4704*m2*MBhat^2*Ycut^11 + 5040*m2^2*MBhat^2*Ycut^11 - 
          18522*MBhat^3*Ycut^11 + 9744*m2*MBhat^3*Ycut^11 + 
          210*m2^2*MBhat^3*Ycut^11 + 2352*MBhat^4*Ycut^11 + 
          5040*m2*MBhat^4*Ycut^11 + 294*MBhat^5*Ycut^11 + 
          420*m2*MBhat^5*Ycut^11 + 280*MBhat^6*Ycut^11 - 22512*Ycut^12 + 
          13776*m2*Ycut^12 - 910*m2^2*Ycut^12 - 140*m2^3*Ycut^12 - 
          5544*MBhat*Ycut^12 + 7728*m2*MBhat*Ycut^12 - 1512*m2^2*MBhat*
           Ycut^12 + 18417*MBhat^2*Ycut^12 - 1932*m2*MBhat^2*Ycut^12 - 
          630*m2^2*MBhat^2*Ycut^12 + 3752*MBhat^3*Ycut^12 - 
          3024*m2*MBhat^3*Ycut^12 - 126*MBhat^4*Ycut^12 - 
          630*m2*MBhat^4*Ycut^12 - 504*MBhat^5*Ycut^12 - 35*MBhat^6*Ycut^12 + 
          7728*Ycut^13 - 2912*m2*Ycut^13 - 1575*MBhat*Ycut^13 - 
          756*m2*MBhat*Ycut^13 + 189*m2^2*MBhat*Ycut^13 - 
          4200*MBhat^2*Ycut^13 + 672*m2*MBhat^2*Ycut^13 - 
          644*MBhat^3*Ycut^13 + 378*m2*MBhat^3*Ycut^13 + 
          336*MBhat^4*Ycut^13 + 63*MBhat^5*Ycut^13 - 1602*Ycut^14 + 
          280*m2*Ycut^14 + 864*MBhat*Ycut^14 + 588*MBhat^2*Ycut^14 - 
          84*m2*MBhat^2*Ycut^14 - 80*MBhat^3*Ycut^14 - 42*MBhat^4*Ycut^14 + 
          160*Ycut^15 - 150*MBhat*Ycut^15 + 10*MBhat^3*Ycut^15)/
         (-1 + Ycut)^8 + ((8 + 32*m2 - 420*m2^2 - 740*m2^3 - 140*m2^4 + 
          12*m2^5 - 48*MBhat - 144*m2*MBhat + 1296*m2^2*MBhat + 
          1200*m2^3*MBhat + 120*MBhat^2 + 264*m2*MBhat^2 - 
          1461*m2^2*MBhat^2 - 516*m2^3*MBhat^2 + 39*m2^4*MBhat^2 - 
          160*MBhat^3 - 256*m2*MBhat^3 + 720*m2^2*MBhat^3 + 120*MBhat^4 + 
          144*m2*MBhat^4 - 138*m2^2*MBhat^4 + 24*m2^3*MBhat^4 - 48*MBhat^5 - 
          48*m2*MBhat^5 + 8*MBhat^6 + 8*m2*MBhat^6 + 3*m2^2*MBhat^6)*Log[m2])/
        2 + ((-8 - 32*m2 + 420*m2^2 + 740*m2^3 + 140*m2^4 - 12*m2^5 + 
          48*MBhat + 144*m2*MBhat - 1296*m2^2*MBhat - 1200*m2^3*MBhat - 
          120*MBhat^2 - 264*m2*MBhat^2 + 1461*m2^2*MBhat^2 + 
          516*m2^3*MBhat^2 - 39*m2^4*MBhat^2 + 160*MBhat^3 + 256*m2*MBhat^3 - 
          720*m2^2*MBhat^3 - 120*MBhat^4 - 144*m2*MBhat^4 + 
          138*m2^2*MBhat^4 - 24*m2^3*MBhat^4 + 48*MBhat^5 + 48*m2*MBhat^5 - 
          8*MBhat^6 - 8*m2*MBhat^6 - 3*m2^2*MBhat^6)*Log[1 - Ycut])/2) + 
     c[VR]*((Sqrt[m2]*(4680 + 38550*m2 + 13200*m2^2 - 46800*m2^3 - 
          10200*m2^4 + 570*m2^5 - 23392*MBhat - 120768*m2*MBhat + 
          45600*m2^2*MBhat + 96000*m2^3*MBhat + 2400*m2^4*MBhat + 
          192*m2^5*MBhat - 32*m2^6*MBhat + 47294*MBhat^2 + 
          131655*m2*MBhat^2 - 127965*m2^2*MBhat^2 - 52140*m2^3*MBhat^2 + 
          510*m2^4*MBhat^2 + 741*m2^5*MBhat^2 - 95*m2^6*MBhat^2 - 
          49008*MBhat^3 - 51456*m2*MBhat^3 + 93840*m2^2*MBhat^3 + 
          7680*m2^3*MBhat^3 - 1680*m2^4*MBhat^3 + 768*m2^5*MBhat^3 - 
          144*m2^6*MBhat^3 + 27120*MBhat^4 - 3675*m2*MBhat^4 - 
          23280*m2^2*MBhat^4 - 1740*m2^3*MBhat^4 + 1920*m2^4*MBhat^4 - 
          345*m2^5*MBhat^4 - 7440*MBhat^5 + 6720*m2*MBhat^5 + 
          960*m2^3*MBhat^5 - 240*m2^4*MBhat^5 + 750*MBhat^6 - 
          1170*m2*MBhat^6 + 450*m2^2*MBhat^6 - 30*m2^3*MBhat^6 - 33840*Ycut - 
          290550*m2*Ycut - 150000*m2^2*Ycut + 295200*m2^3*Ycut + 
          69600*m2^4*Ycut - 3810*m2^5*Ycut + 169504*MBhat*Ycut + 
          926016*m2*MBhat*Ycut - 175200*m2^2*MBhat*Ycut - 
          633600*m2^3*MBhat*Ycut - 16800*m2^4*MBhat*Ycut - 
          1344*m2^5*MBhat*Ycut + 224*m2^6*MBhat*Ycut - 343658*MBhat^2*Ycut - 
          1044525*m2*MBhat^2*Ycut + 767055*m2^2*MBhat^2*Ycut + 
          352620*m2^3*MBhat^2*Ycut - 2490*m2^4*MBhat^2*Ycut - 
          5187*m2^5*MBhat^2*Ycut + 665*m2^6*MBhat^2*Ycut + 
          357456*MBhat^3*Ycut + 452352*m2*MBhat^3*Ycut - 607920*m2^2*MBhat^3*
           Ycut - 53760*m2^3*MBhat^3*Ycut + 11760*m2^4*MBhat^3*Ycut - 
          5376*m2^5*MBhat^3*Ycut + 1008*m2^6*MBhat^3*Ycut - 
          198840*MBhat^4*Ycut - 9015*m2*MBhat^4*Ycut + 156120*m2^2*MBhat^4*
           Ycut + 13260*m2^3*MBhat^4*Ycut - 13440*m2^4*MBhat^4*Ycut + 
          2415*m2^5*MBhat^4*Ycut + 54960*MBhat^5*Ycut - 41280*m2*MBhat^5*
           Ycut - 6720*m2^3*MBhat^5*Ycut + 1680*m2^4*MBhat^5*Ycut - 
          5610*MBhat^6*Ycut + 8010*m2*MBhat^6*Ycut - 2970*m2^2*MBhat^6*Ycut + 
          210*m2^3*MBhat^6*Ycut + 105300*Ycut^2 + 944100*m2*Ycut^2 + 
          651600*m2^2*Ycut^2 - 772200*m2^3*Ycut^2 - 202500*m2^4*Ycut^2 + 
          10800*m2^5*Ycut^2 - 528672*MBhat*Ycut^2 - 3060288*m2*MBhat*Ycut^2 + 
          21600*m2^2*MBhat*Ycut^2 + 1766400*m2^3*MBhat*Ycut^2 + 
          50400*m2^4*MBhat*Ycut^2 + 4032*m2^5*MBhat*Ycut^2 - 
          672*m2^6*MBhat*Ycut^2 + 1075074*MBhat^2*Ycut^2 + 
          3563865*m2*MBhat^2*Ycut^2 - 1850715*m2^2*MBhat^2*Ycut^2 - 
          1014600*m2^3*MBhat^2*Ycut^2 + 3690*m2^4*MBhat^2*Ycut^2 + 
          15561*m2^5*MBhat^2*Ycut^2 - 1995*m2^6*MBhat^2*Ycut^2 - 
          1122768*MBhat^3*Ycut^2 - 1679616*m2*MBhat^3*Ycut^2 + 
          1652400*m2^2*MBhat^3*Ycut^2 + 161280*m2^3*MBhat^3*Ycut^2 - 
          35280*m2^4*MBhat^3*Ycut^2 + 16128*m2^5*MBhat^3*Ycut^2 - 
          3024*m2^6*MBhat^3*Ycut^2 + 628020*MBhat^4*Ycut^2 + 
          148635*m2*MBhat^4*Ycut^2 - 444420*m2^2*MBhat^4*Ycut^2 - 
          43560*m2^3*MBhat^4*Ycut^2 + 40320*m2^4*MBhat^4*Ycut^2 - 
          7245*m2^5*MBhat^4*Ycut^2 - 174960*MBhat^5*Ycut^2 + 
          103680*m2*MBhat^5*Ycut^2 + 20160*m2^3*MBhat^5*Ycut^2 - 
          5040*m2^4*MBhat^5*Ycut^2 + 18090*MBhat^6*Ycut^2 - 
          23400*m2*MBhat^6*Ycut^2 + 8280*m2^2*MBhat^6*Ycut^2 - 
          630*m2^3*MBhat^6*Ycut^2 - 183060*Ycut^3 - 1718400*m2*Ycut^3 - 
          1489200*m2^2*Ycut^3 + 1060200*m2^3*Ycut^3 + 324900*m2^4*Ycut^3 - 
          16740*m2^5*Ycut^3 + 921440*MBhat*Ycut^3 + 5664960*m2*MBhat*Ycut^3 + 
          972000*m2^2*MBhat*Ycut^3 - 2675200*m2^3*MBhat*Ycut^3 - 
          84000*m2^4*MBhat*Ycut^3 - 6720*m2^5*MBhat*Ycut^3 + 
          1120*m2^6*MBhat*Ycut^3 - 1879990*MBhat^2*Ycut^3 - 
          6800355*m2*MBhat^2*Ycut^3 + 2183625*m2^2*MBhat^2*Ycut^3 + 
          1604480*m2^3*MBhat^2*Ycut^3 + 1410*m2^4*MBhat^2*Ycut^3 - 
          25935*m2^5*MBhat^2*Ycut^3 + 3325*m2^6*MBhat^2*Ycut^3 + 
          1971590*MBhat^3*Ycut^3 + 3445530*m2*MBhat^3*Ycut^3 - 
          2410580*m2^2*MBhat^3*Ycut^3 - 272300*m2^3*MBhat^3*Ycut^3 + 
          61950*m2^4*MBhat^3*Ycut^3 - 27790*m2^5*MBhat^3*Ycut^3 + 
          5040*m2^6*MBhat^3*Ycut^3 - 1108860*MBhat^4*Ycut^3 - 
          491865*m2*MBhat^4*Ycut^3 + 690660*m2^2*MBhat^4*Ycut^3 + 
          84000*m2^3*MBhat^4*Ycut^3 - 68760*m2^4*MBhat^4*Ycut^3 + 
          12075*m2^5*MBhat^4*Ycut^3 + 311310*MBhat^5*Ycut^3 - 
          132330*m2*MBhat^5*Ycut^3 + 1050*m2^2*MBhat^5*Ycut^3 - 
          34350*m2^3*MBhat^5*Ycut^3 + 8400*m2^4*MBhat^5*Ycut^3 - 
          32590*MBhat^6*Ycut^3 + 37740*m2*MBhat^6*Ycut^3 - 
          12620*m2^2*MBhat^6*Ycut^3 + 1050*m2^3*MBhat^6*Ycut^3 + 
          192510*Ycut^4 + 1899525*m2*Ycut^4 + 1993200*m2^2*Ycut^4 - 
          776700*m2^3*Ycut^4 - 309150*m2^4*Ycut^4 + 15165*m2^5*Ycut^4 - 
          971840*MBhat*Ycut^4 - 6370560*m2*MBhat*Ycut^4 - 
          2232000*m2^2*MBhat*Ycut^4 + 2339200*m2^3*MBhat*Ycut^4 + 
          84000*m2^4*MBhat*Ycut^4 + 6720*m2^5*MBhat*Ycut^4 - 
          1120*m2^6*MBhat*Ycut^4 + 1990825*MBhat^2*Ycut^4 + 
          7875105*m2*MBhat^2*Ycut^4 - 1059450*m2^2*MBhat^2*Ycut^4 - 
          1490480*m2^3*MBhat^2*Ycut^4 - 15735*m2^4*MBhat^2*Ycut^4 + 
          27300*m2^5*MBhat^2*Ycut^4 - 3325*m2^6*MBhat^2*Ycut^4 - 
          2095610*MBhat^3*Ycut^4 - 4258070*m2*MBhat^3*Ycut^4 + 
          1987580*m2^2*MBhat^3*Ycut^4 + 273140*m2^3*MBhat^3*Ycut^4 - 
          65170*m2^4*MBhat^3*Ycut^4 + 28930*m2^5*MBhat^3*Ycut^4 - 
          5040*m2^6*MBhat^3*Ycut^4 + 1183005*MBhat^4*Ycut^4 + 
          803430*m2*MBhat^4*Ycut^4 - 629640*m2^2*MBhat^4*Ycut^4 - 
          100140*m2^3*MBhat^4*Ycut^4 + 71295*m2^4*MBhat^4*Ycut^4 - 
          12075*m2^5*MBhat^4*Ycut^4 - 333810*MBhat^5*Ycut^4 + 
          79590*m2*MBhat^5*Ycut^4 - 2790*m2^2*MBhat^5*Ycut^4 + 
          35730*m2^3*MBhat^5*Ycut^4 - 8400*m2^4*MBhat^5*Ycut^4 + 
          35230*MBhat^6*Ycut^4 - 35900*m2*MBhat^6*Ycut^4 + 
          11150*m2^2*MBhat^6*Ycut^4 - 1050*m2^3*MBhat^6*Ycut^4 - 
          123066*Ycut^5 - 1284615*m2*Ycut^5 - 1599120*m2^2*Ycut^5 + 
          239220*m2^3*Ycut^5 + 172890*m2^4*Ycut^5 - 7839*m2^5*Ycut^5 + 
          623103*MBhat*Ycut^5 + 4387281*m2*MBhat*Ycut^5 + 
          2348550*m2^2*MBhat*Ycut^5 - 1138350*m2^3*MBhat*Ycut^5 - 
          47445*m2^4*MBhat*Ycut^5 - 4851*m2^5*MBhat*Ycut^5 + 
          672*m2^6*MBhat*Ycut^5 - 1285779*MBhat^2*Ycut^5 - 
          5579115*m2*MBhat^2*Ycut^5 - 265350*m2^2*MBhat^2*Ycut^5 + 
          799800*m2^3*MBhat^2*Ycut^5 + 22605*m2^4*MBhat^2*Ycut^5 - 
          17340*m2^5*MBhat^2*Ycut^5 + 1995*m2^6*MBhat^2*Ycut^5 + 
          1355625*MBhat^3*Ycut^5 + 3212537*m2*MBhat^3*Ycut^5 - 
          868080*m2^2*MBhat^3*Ycut^5 - 156000*m2^3*MBhat^3*Ycut^5 + 
          40495*m2^4*MBhat^3*Ycut^5 - 18417*m2^5*MBhat^3*Ycut^5 + 
          3024*m2^6*MBhat^3*Ycut^5 - 761475*MBhat^4*Ycut^5 - 
          747030*m2*MBhat^4*Ycut^5 + 339120*m2^2*MBhat^4*Ycut^5 + 
          70860*m2^3*MBhat^4*Ycut^5 - 44745*m2^4*MBhat^4*Ycut^5 + 
          7245*m2^5*MBhat^4*Ycut^5 + 213030*MBhat^5*Ycut^5 + 
          990*m2*MBhat^5*Ycut^5 + 840*m2^2*MBhat^5*Ycut^5 - 
          22260*m2^3*MBhat^5*Ycut^5 + 5040*m2^4*MBhat^5*Ycut^5 - 
          22170*MBhat^6*Ycut^5 + 19160*m2*MBhat^6*Ycut^5 - 
          5310*m2^2*MBhat^6*Ycut^5 + 630*m2^3*MBhat^6*Ycut^5 + 44870*Ycut^6 + 
          500565*m2*Ycut^6 + 734320*m2^2*Ycut^6 + 34480*m2^3*Ycut^6 - 
          51990*m2^4*Ycut^6 + 2165*m2^5*Ycut^6 - 225957*MBhat*Ycut^6 - 
          1748007*m2*MBhat*Ycut^6 - 1288530*m2^2*MBhat*Ycut^6 + 
          251090*m2^3*MBhat*Ycut^6 + 12615*m2^4*MBhat*Ycut^6 + 
          1893*m2^5*MBhat*Ycut^6 - 224*m2^6*MBhat*Ycut^6 + 
          479505*MBhat^2*Ycut^6 + 2273865*m2*MBhat^2*Ycut^6 + 
          549840*m2^2*MBhat^2*Ycut^6 - 223600*m2^3*MBhat^2*Ycut^6 - 
          13155*m2^4*MBhat^2*Ycut^6 + 6258*m2^5*MBhat^2*Ycut^6 - 
          665*m2^6*MBhat^2*Ycut^6 - 503455*MBhat^3*Ycut^6 - 
          1403439*m2*MBhat^3*Ycut^6 + 138320*m2^2*MBhat^3*Ycut^6 + 
          42960*m2^3*MBhat^3*Ycut^6 - 14265*m2^4*MBhat^3*Ycut^6 + 
          6839*m2^5*MBhat^3*Ycut^6 - 1008*m2^6*MBhat^3*Ycut^6 + 
          269625*MBhat^4*Ycut^6 + 402810*m2*MBhat^4*Ycut^6 - 
          107820*m2^2*MBhat^4*Ycut^6 - 26280*m2^3*MBhat^4*Ycut^6 + 
          16065*m2^4*MBhat^4*Ycut^6 - 2415*m2^5*MBhat^4*Ycut^6 - 
          69210*MBhat^5*Ycut^6 - 35010*m2*MBhat^5*Ycut^6 + 
          4080*m2^2*MBhat^5*Ycut^6 + 7500*m2^3*MBhat^5*Ycut^6 - 
          1680*m2^4*MBhat^5*Ycut^6 + 6110*MBhat^6*Ycut^6 - 
          3720*m2*MBhat^6*Ycut^6 + 800*m2^2*MBhat^6*Ycut^6 - 
          210*m2^3*MBhat^6*Ycut^6 - 7922*Ycut^7 - 91535*m2*Ycut^7 - 
          161920*m2^2*Ycut^7 - 38800*m2^3*Ycut^7 + 6290*m2^4*Ycut^7 - 
          143*m2^5*Ycut^7 + 33135*MBhat*Ycut^7 + 339381*m2*MBhat*Ycut^7 + 
          325080*m2^2*MBhat*Ycut^7 + 1060*m2^3*MBhat*Ycut^7 - 
          1455*m2^4*MBhat*Ycut^7 - 273*m2^5*MBhat*Ycut^7 + 
          32*m2^6*MBhat*Ycut^7 - 88623*MBhat^2*Ycut^7 - 428715*m2*MBhat^2*
           Ycut^7 - 223260*m2^2*MBhat^2*Ycut^7 + 23800*m2^3*MBhat^2*Ycut^7 + 
          3405*m2^4*MBhat^2*Ycut^7 - 1086*m2^5*MBhat^2*Ycut^7 + 
          95*m2^6*MBhat^2*Ycut^7 + 93625*MBhat^3*Ycut^7 + 
          291857*m2*MBhat^3*Ycut^7 + 19300*m2^2*MBhat^3*Ycut^7 - 
          1140*m2^3*MBhat^3*Ycut^7 + 1955*m2^4*MBhat^3*Ycut^7 - 
          1277*m2^5*MBhat^3*Ycut^7 + 144*m2^6*MBhat^3*Ycut^7 - 
          34695*MBhat^4*Ycut^7 - 119610*m2*MBhat^4*Ycut^7 + 
          25020*m2^2*MBhat^4*Ycut^7 + 2640*m2^3*MBhat^4*Ycut^7 - 
          2775*m2^4*MBhat^4*Ycut^7 + 345*m2^5*MBhat^4*Ycut^7 + 
          810*MBhat^5*Ycut^7 + 24930*m2*MBhat^5*Ycut^7 - 5130*m2^2*MBhat^5*
           Ycut^7 - 990*m2^3*MBhat^5*Ycut^7 + 240*m2^4*MBhat^5*Ycut^7 + 
          1510*MBhat^6*Ycut^7 - 1660*m2*MBhat^6*Ycut^7 + 
          340*m2^2*MBhat^6*Ycut^7 + 30*m2^3*MBhat^6*Ycut^7 + 1338*Ycut^8 + 
          665*m2*Ycut^8 + 7440*m2^2*Ycut^8 + 4500*m2^3*Ycut^8 + 
          280*m2^4*Ycut^8 - 48*m2^5*Ycut^8 + 6279*MBhat*Ycut^8 - 
          22275*m2*MBhat*Ycut^8 - 11160*m2^2*MBhat*Ycut^8 - 
          6780*m2^3*MBhat*Ycut^8 + 345*m2^4*MBhat*Ycut^8 - 
          9*m2^5*MBhat*Ycut^8 + 4857*MBhat^2*Ycut^8 + 4980*m2*MBhat^2*
           Ycut^8 + 24375*m2^2*MBhat^2*Ycut^8 - 60*m2^3*MBhat^2*Ycut^8 - 
          150*m2^4*MBhat^2*Ycut^8 + 48*m2^5*MBhat^2*Ycut^8 - 
          11055*MBhat^3*Ycut^8 - 5255*m2*MBhat^3*Ycut^8 - 
          3420*m2^2*MBhat^3*Ycut^8 - 2580*m2^3*MBhat^3*Ycut^8 + 
          355*m2^4*MBhat^3*Ycut^8 + 75*m2^5*MBhat^3*Ycut^8 - 
          4665*MBhat^4*Ycut^8 + 18165*m2*MBhat^4*Ycut^8 - 
          7560*m2^2*MBhat^4*Ycut^8 + 1320*m2^3*MBhat^4*Ycut^8 + 
          120*m2^4*MBhat^4*Ycut^8 + 7290*MBhat^5*Ycut^8 - 
          8910*m2*MBhat^5*Ycut^8 + 2310*m2^2*MBhat^5*Ycut^8 - 
          30*m2^3*MBhat^5*Ycut^8 - 1860*MBhat^6*Ycut^8 + 
          1150*m2*MBhat^6*Ycut^8 - 120*m2^2*MBhat^6*Ycut^8 - 1750*Ycut^9 + 
          2865*m2*Ycut^9 - 400*m2^2*Ycut^9 + 980*m2^3*Ycut^9 - 
          120*m2^4*Ycut^9 - 5135*MBhat*Ycut^9 + 6195*m2*MBhat*Ycut^9 - 
          5550*m2^2*MBhat*Ycut^9 + 170*m2^3*MBhat*Ycut^9 - 
          60*m2^4*MBhat*Ycut^9 + 1945*MBhat^2*Ycut^9 + 1620*m2*MBhat^2*
           Ycut^9 + 1575*m2^2*MBhat^2*Ycut^9 + 260*m2^3*MBhat^2*Ycut^9 - 
          90*m2^4*MBhat^2*Ycut^9 + 4475*MBhat^3*Ycut^9 - 
          4005*m2*MBhat^3*Ycut^9 - 1680*m2^2*MBhat^3*Ycut^9 + 
          800*m2^3*MBhat^3*Ycut^9 - 120*m2^4*MBhat^3*Ycut^9 + 
          495*MBhat^4*Ycut^9 - 2355*m2*MBhat^4*Ycut^9 + 2040*m2^2*MBhat^4*
           Ycut^9 - 360*m2^3*MBhat^4*Ycut^9 - 2190*MBhat^5*Ycut^9 + 
          1770*m2*MBhat^5*Ycut^9 - 360*m2^2*MBhat^5*Ycut^9 + 
          620*MBhat^6*Ycut^9 - 210*m2*MBhat^6*Ycut^9 + 1486*Ycut^10 - 
          1645*m2*Ycut^10 + 880*m2^2*Ycut^10 - 80*m2^3*Ycut^10 + 
          1649*MBhat*Ycut^10 - 1653*m2*MBhat*Ycut^10 - 390*m2^2*MBhat*
           Ycut^10 + 10*m2^3*MBhat*Ycut^10 - 1975*MBhat^2*Ycut^10 + 
          1410*m2*MBhat^2*Ycut^10 + 315*m2^2*MBhat^2*Ycut^10 - 
          80*m2^3*MBhat^2*Ycut^10 - 797*MBhat^3*Ycut^10 - 
          381*m2*MBhat^3*Ycut^10 + 240*m2^2*MBhat^3*Ycut^10 - 
          80*m2^3*MBhat^3*Ycut^10 + 255*MBhat^4*Ycut^10 + 
          555*m2*MBhat^4*Ycut^10 - 240*m2^2*MBhat^4*Ycut^10 + 
          210*MBhat^5*Ycut^10 - 150*m2*MBhat^5*Ycut^10 - 80*MBhat^6*Ycut^10 - 
          690*Ycut^11 + 475*m2*Ycut^11 - 27*MBhat*Ycut^11 - 
          249*m2*MBhat*Ycut^11 + 513*MBhat^2*Ycut^11 + 210*m2*MBhat^2*
           Ycut^11 - 45*m2^2*MBhat^2*Ycut^11 - 51*MBhat^3*Ycut^11 - 
          63*m2*MBhat^3*Ycut^11 + 15*MBhat^4*Ycut^11 - 45*m2*MBhat^4*
           Ycut^11 + 152*Ycut^12 - 83*MBhat*Ycut^12 - 33*m2*MBhat*Ycut^12 + 
          4*MBhat^2*Ycut^12 - 27*MBhat^3*Ycut^12 + 9*m2*MBhat^3*Ycut^12 - 
          8*Ycut^13 - 4*MBhat*Ycut^13 + 8*MBhat^2*Ycut^13))/
        (15*(-1 + Ycut)^7) + 4*Sqrt[m2]*(-18 - 345*m2 - 960*m2^2 - 540*m2^3 - 
         30*m2^4 + 3*m2^5 + 96*MBhat + 1344*m2*MBhat + 2400*m2^2*MBhat + 
         640*m2^3*MBhat - 210*MBhat^2 - 2049*m2*MBhat^2 - 2145*m2^2*MBhat^2 - 
         206*m2^3*MBhat^2 + 18*m2^4*MBhat^2 + 240*MBhat^3 + 1536*m2*MBhat^3 + 
         816*m2^2*MBhat^3 - 150*MBhat^4 - 579*m2*MBhat^4 - 114*m2^2*MBhat^4 + 
         18*m2^3*MBhat^4 + 48*MBhat^5 + 96*m2*MBhat^5 - 6*MBhat^6 - 
         3*m2*MBhat^6 + 3*m2^2*MBhat^6)*Log[m2] - 
       4*Sqrt[m2]*(-18 - 345*m2 - 960*m2^2 - 540*m2^3 - 30*m2^4 + 3*m2^5 + 
         96*MBhat + 1344*m2*MBhat + 2400*m2^2*MBhat + 640*m2^3*MBhat - 
         210*MBhat^2 - 2049*m2*MBhat^2 - 2145*m2^2*MBhat^2 - 
         206*m2^3*MBhat^2 + 18*m2^4*MBhat^2 + 240*MBhat^3 + 1536*m2*MBhat^3 + 
         816*m2^2*MBhat^3 - 150*MBhat^4 - 579*m2*MBhat^4 - 114*m2^2*MBhat^4 + 
         18*m2^3*MBhat^4 + 48*MBhat^5 + 96*m2*MBhat^5 - 6*MBhat^6 - 
         3*m2*MBhat^6 + 3*m2^2*MBhat^6)*Log[1 - Ycut]) + 
     c[T]^2*((-2*(-11454 + 97594*m2 + 444570*m2^2 - 266350*m2^3 - 
          276850*m2^4 + 12054*m2^5 + 406*m2^6 + 30*m2^7 + 62672*MBhat - 
          474768*m2*MBhat - 916272*m2^2*MBhat + 1167600*m2^3*MBhat + 
          170800*m2^4*MBhat - 9072*m2^5*MBhat - 1232*m2^6*MBhat + 
          272*m2^7*MBhat - 138943*MBhat^2 + 910000*m2*MBhat^2 + 
          287385*m2^2*MBhat^2 - 1115520*m2^3*MBhat^2 + 48055*m2^4*MBhat^2 + 
          14448*m2^5*MBhat^2 - 6545*m2^6*MBhat^2 + 1120*m2^7*MBhat^2 + 
          156736*MBhat^3 - 853888*m2*MBhat^3 + 451920*m2^2*MBhat^3 + 
          282240*m2^3*MBhat^3 - 58240*m2^4*MBhat^3 + 34944*m2^5*MBhat^3 - 
          16912*m2^6*MBhat^3 + 3200*m2^7*MBhat^3 - 95130*MBhat^4 + 
          407330*m2*MBhat^4 - 350490*m2^2*MBhat^4 + 15680*m2^3*MBhat^4 + 
          45010*m2^4*MBhat^4 - 28770*m2^5*MBhat^4 + 6370*m2^6*MBhat^4 + 
          29904*MBhat^5 - 90160*m2*MBhat^5 + 73920*m2^2*MBhat^5 - 
          6720*m2^3*MBhat^5 - 10640*m2^4*MBhat^5 + 3696*m2^5*MBhat^5 - 
          3885*MBhat^6 + 5880*m2*MBhat^6 - 3360*m2^2*MBhat^6 + 
          840*m2^3*MBhat^6 + 525*m2^4*MBhat^6 + 94992*Ycut - 787472*m2*Ycut - 
          3871560*m2^2*Ycut + 1664600*m2^3*Ycut + 2135000*m2^4*Ycut - 
          88872*m2^5*Ycut - 3248*m2^6*Ycut - 240*m2^7*Ycut - 
          521536*MBhat*Ycut + 3845184*m2*MBhat*Ycut + 8358336*m2^2*MBhat*
           Ycut - 8568000*m2^3*MBhat*Ycut - 1366400*m2^4*MBhat*Ycut + 
          72576*m2^5*MBhat*Ycut + 9856*m2^6*MBhat*Ycut - 
          2176*m2^7*MBhat*Ycut + 1161944*MBhat^2*Ycut - 7404320*m2*MBhat^2*
           Ycut - 3540180*m2^2*MBhat^2*Ycut + 8578080*m2^3*MBhat^2*Ycut - 
          352100*m2^4*MBhat^2*Ycut - 115584*m2^5*MBhat^2*Ycut + 
          52360*m2^6*MBhat^2*Ycut - 8960*m2^7*MBhat^2*Ycut - 
          1321088*MBhat^3*Ycut + 6992384*m2*MBhat^3*Ycut - 
          2950080*m2^2*MBhat^3*Ycut - 2257920*m2^3*MBhat^3*Ycut + 
          465920*m2^4*MBhat^3*Ycut - 279552*m2^5*MBhat^3*Ycut + 
          135296*m2^6*MBhat^3*Ycut - 25600*m2^7*MBhat^3*Ycut + 
          811440*MBhat^4*Ycut - 3366160*m2*MBhat^4*Ycut + 
          2662800*m2^2*MBhat^4*Ycut - 99400*m2^3*MBhat^4*Ycut - 
          360080*m2^4*MBhat^4*Ycut + 230160*m2^5*MBhat^4*Ycut - 
          50960*m2^6*MBhat^4*Ycut - 259392*MBhat^5*Ycut + 
          754880*m2*MBhat^5*Ycut - 591360*m2^2*MBhat^5*Ycut + 
          53760*m2^3*MBhat^5*Ycut + 85120*m2^4*MBhat^5*Ycut - 
          29568*m2^5*MBhat^5*Ycut + 34440*MBhat^6*Ycut - 
          50400*m2*MBhat^6*Ycut + 30660*m2^2*MBhat^6*Ycut - 
          6720*m2^3*MBhat^6*Ycut - 4200*m2^4*MBhat^6*Ycut - 345912*Ycut^2 + 
          2783032*m2*Ycut^2 + 14810460*m2^2*Ycut^2 - 3961300*m2^3*Ycut^2 - 
          7153300*m2^4*Ycut^2 + 280812*m2^5*Ycut^2 + 11368*m2^6*Ycut^2 + 
          840*m2^7*Ycut^2 + 1906016*MBhat*Ycut^2 - 13646304*m2*MBhat*Ycut^2 - 
          33366816*m2^2*MBhat*Ycut^2 + 26896800*m2^3*MBhat*Ycut^2 + 
          4782400*m2^4*MBhat*Ycut^2 - 254016*m2^5*MBhat*Ycut^2 - 
          34496*m2^6*MBhat*Ycut^2 + 7616*m2^7*MBhat*Ycut^2 - 
          4268404*MBhat^2*Ycut^2 + 26412400*m2*MBhat^2*Ycut^2 + 
          17355030*m2^2*MBhat^2*Ycut^2 - 28638960*m2^3*MBhat^2*Ycut^2 + 
          1102990*m2^4*MBhat^2*Ycut^2 + 404544*m2^5*MBhat^2*Ycut^2 - 
          183260*m2^6*MBhat^2*Ycut^2 + 31360*m2^7*MBhat^2*Ycut^2 + 
          4892608*MBhat^3*Ycut^2 - 25118464*m2*MBhat^3*Ycut^2 + 
          7664160*m2^2*MBhat^3*Ycut^2 + 7902720*m2^3*MBhat^3*Ycut^2 - 
          1630720*m2^4*MBhat^3*Ycut^2 + 978432*m2^5*MBhat^3*Ycut^2 - 
          473536*m2^6*MBhat^3*Ycut^2 + 89600*m2^7*MBhat^3*Ycut^2 - 
          3041640*MBhat^4*Ycut^2 + 12211640*m2*MBhat^4*Ycut^2 - 
          8755320*m2^2*MBhat^4*Ycut^2 + 243740*m2^3*MBhat^4*Ycut^2 + 
          1260280*m2^4*MBhat^4*Ycut^2 - 805560*m2^5*MBhat^4*Ycut^2 + 
          178360*m2^6*MBhat^4*Ycut^2 + 988512*MBhat^5*Ycut^2 - 
          2776480*m2*MBhat^5*Ycut^2 + 2069760*m2^2*MBhat^5*Ycut^2 - 
          188160*m2^3*MBhat^5*Ycut^2 - 297920*m2^4*MBhat^5*Ycut^2 + 
          103488*m2^5*MBhat^5*Ycut^2 - 133980*MBhat^6*Ycut^2 + 
          189840*m2*MBhat^6*Ycut^2 - 122430*m2^2*MBhat^6*Ycut^2 + 
          23520*m2^3*MBhat^6*Ycut^2 + 14700*m2^4*MBhat^6*Ycut^2 + 
          723184*Ycut^3 - 5628784*m2*Ycut^3 - 32560920*m2^2*Ycut^3 + 
          3571400*m2^3*Ycut^3 + 13561800*m2^4*Ycut^3 - 491064*m2^5*Ycut^3 - 
          22736*m2^6*Ycut^3 - 1680*m2^7*Ycut^3 - 4000192*MBhat*Ycut^3 + 
          27731648*m2*MBhat*Ycut^3 + 76329792*m2^2*MBhat*Ycut^3 - 
          46580800*m2^3*MBhat*Ycut^3 - 9564800*m2^4*MBhat*Ycut^3 + 
          508032*m2^5*MBhat*Ycut^3 + 68992*m2^6*MBhat*Ycut^3 - 
          15232*m2^7*MBhat*Ycut^3 + 9007208*MBhat^2*Ycut^3 - 
          53985120*m2*MBhat^2*Ycut^3 - 46293660*m2^2*MBhat^2*Ycut^3 + 
          54047840*m2^3*MBhat^2*Ycut^3 - 1904140*m2^4*MBhat^2*Ycut^3 - 
          809088*m2^5*MBhat^2*Ycut^3 + 366520*m2^6*MBhat^2*Ycut^3 - 
          62720*m2^7*MBhat^2*Ycut^3 - 10399326*MBhat^3*Ycut^3 + 
          51711408*m2*MBhat^3*Ycut^3 - 9119390*m2^2*MBhat^3*Ycut^3 - 
          15757840*m2^3*MBhat^3*Ycut^3 + 3232390*m2^4*MBhat^3*Ycut^3 - 
          1964704*m2^5*MBhat^3*Ycut^3 + 954422*m2^6*MBhat^3*Ycut^3 - 
          179200*m2^7*MBhat^3*Ycut^3 + 6528480*MBhat^4*Ycut^3 - 
          25386480*m2*MBhat^4*Ycut^3 + 16210320*m2^2*MBhat^4*Ycut^3 - 
          284760*m2^3*MBhat^4*Ycut^3 - 2525600*m2^4*MBhat^4*Ycut^3 + 
          1624560*m2^5*MBhat^4*Ycut^3 - 356720*m2^6*MBhat^4*Ycut^3 - 
          2149014*MBhat^5*Ycut^3 + 5851020*m2*MBhat^5*Ycut^3 - 
          4149600*m2^2*MBhat^5*Ycut^3 + 378420*m2^3*MBhat^5*Ycut^3 + 
          603190*m2^4*MBhat^5*Ycut^3 - 206976*m2^5*MBhat^5*Ycut^3 + 
          295960*MBhat^6*Ycut^3 - 409920*m2*MBhat^6*Ycut^3 + 
          281260*m2^2*MBhat^6*Ycut^3 - 45920*m2^3*MBhat^6*Ycut^3 - 
          29400*m2^4*MBhat^6*Ycut^3 - 951020*Ycut^4 + 7130060*m2*Ycut^4 + 
          45111150*m2^2*Ycut^4 + 2062550*m2^3*Ycut^4 - 15835050*m2^4*Ycut^4 + 
          507990*m2^5*Ycut^4 + 28420*m2^6*Ycut^4 + 2100*m2^7*Ycut^4 + 
          5282480*MBhat*Ycut^4 - 35323120*m2*MBhat*Ycut^4 - 
          109806480*m2^2*MBhat*Ycut^4 + 47406800*m2^3*MBhat*Ycut^4 + 
          11956000*m2^4*MBhat*Ycut^4 - 635040*m2^5*MBhat*Ycut^4 - 
          86240*m2^6*MBhat*Ycut^4 + 19040*m2^7*MBhat*Ycut^4 - 
          11978995*MBhat^2*Ycut^4 + 69254430*m2*MBhat^2*Ycut^4 + 
          75245100*m2^2*MBhat^2*Ycut^4 - 62762980*m2^3*MBhat^2*Ycut^4 + 
          1946840*m2^4*MBhat^2*Ycut^4 + 1030470*m2^5*MBhat^2*Ycut^4 - 
          469175*m2^6*MBhat^2*Ycut^4 + 78400*m2^7*MBhat^2*Ycut^4 + 
          13877080*MBhat^3*Ycut^4 - 66740800*m2*MBhat^3*Ycut^4 + 
          2056880*m2^2*MBhat^3*Ycut^4 + 19483520*m2^3*MBhat^3*Ycut^4 - 
          3849160*m2^4*MBhat^3*Ycut^4 + 2423680*m2^5*MBhat^3*Ycut^4 - 
          1203440*m2^6*MBhat^3*Ycut^4 + 224000*m2^7*MBhat^3*Ycut^4 - 
          8712270*MBhat^4*Ycut^4 + 33000660*m2*MBhat^4*Ycut^4 - 
          18362190*m2^2*MBhat^4*Ycut^4 + 214200*m2^3*MBhat^4*Ycut^4 + 
          3117520*m2^4*MBhat^4*Ycut^4 - 2055270*m2^5*MBhat^4*Ycut^4 + 
          445900*m2^6*MBhat^4*Ycut^4 + 2864400*MBhat^5*Ycut^4 - 
          7691040*m2*MBhat^5*Ycut^4 + 5239920*m2^2*MBhat^5*Ycut^4 - 
          498960*m2^3*MBhat^5*Ycut^4 - 770000*m2^4*MBhat^5*Ycut^4 + 
          258720*m2^5*MBhat^5*Ycut^4 - 395395*MBhat^6*Ycut^4 + 
          553280*m2*MBhat^6*Ycut^4 - 410830*m2^2*MBhat^6*Ycut^4 + 
          54740*m2^3*MBhat^6*Ycut^4 + 36750*m2^4*MBhat^6*Ycut^4 + 
          807856*Ycut^5 - 5798128*m2*Ycut^5 - 40498920*m2^2*Ycut^5 - 
          8176840*m2^3*Ycut^5 + 11550840*m2^4*Ycut^5 - 300552*m2^5*Ycut^5 - 
          22736*m2^6*Ycut^5 - 1680*m2^7*Ycut^5 - 4500643*MBhat*Ycut^5 + 
          28900256*m2*MBhat*Ycut^5 + 102237429*m2^2*MBhat*Ycut^5 - 
          27081880*m2^3*MBhat*Ycut^5 - 9571625*m2^4*MBhat*Ycut^5 + 
          495096*m2^5*MBhat*Ycut^5 + 75607*m2^6*MBhat*Ycut^5 - 
          15232*m2^7*MBhat*Ycut^5 + 10378424*MBhat^2*Ycut^5 - 
          57336720*m2*MBhat^2*Ycut^5 - 77565180*m2^2*MBhat^2*Ycut^5 + 
          45597440*m2^3*MBhat^2*Ycut^5 - 1256500*m2^4*MBhat^2*Ycut^5 - 
          816816*m2^5*MBhat^2*Ycut^5 + 384160*m2^6*MBhat^2*Ycut^5 - 
          62720*m2^7*MBhat^2*Ycut^5 - 11929568*MBhat^3*Ycut^5 + 
          55302198*m2*MBhat^3*Ycut^5 + 7828520*m2^2*MBhat^3*Ycut^5 - 
          15204280*m2^3*MBhat^3*Ycut^5 + 2675260*m2^4*MBhat^3*Ycut^5 - 
          1854398*m2^5*MBhat^3*Ycut^5 + 975100*m2^6*MBhat^3*Ycut^5 - 
          179200*m2^7*MBhat^3*Ycut^5 + 7237440*MBhat^4*Ycut^5 - 
          27244560*m2*MBhat^4*Ycut^5 + 12841920*m2^2*MBhat^4*Ycut^5 - 
          311640*m2^3*MBhat^4*Ycut^5 - 2366000*m2^4*MBhat^4*Ycut^5 + 
          1669920*m2^5*MBhat^4*Ycut^5 - 356720*m2^6*MBhat^4*Ycut^5 - 
          2256513*MBhat^5*Ycut^5 + 6358800*m2*MBhat^5*Ycut^5 - 
          4310880*m2^2*MBhat^5*Ycut^5 + 470400*m2^3*MBhat^5*Ycut^5 + 
          632065*m2^4*MBhat^5*Ycut^5 - 206976*m2^5*MBhat^5*Ycut^5 + 
          297920*MBhat^6*Ycut^5 - 473760*m2*MBhat^6*Ycut^5 + 
          396620*m2^2*MBhat^6*Ycut^5 - 41440*m2^3*MBhat^6*Ycut^5 - 
          29400*m2^4*MBhat^6*Ycut^5 - 436856*Ycut^6 + 2965214*m2*Ycut^6 + 
          23189950*m2^2*Ycut^6 + 8434720*m2^3*Ycut^6 - 5029640*m2^4*Ycut^6 + 
          82754*m2^5*Ycut^6 + 9898*m2^6*Ycut^6 + 840*m2^7*Ycut^6 + 
          2384872*MBhat*Ycut^6 - 14781088*m2*MBhat*Ycut^6 - 
          60709992*m2^2*MBhat*Ycut^6 + 6203680*m2^3*MBhat*Ycut^6 + 
          4853240*m2^4*MBhat*Ycut^6 - 247296*m2^5*MBhat*Ycut^6 - 
          40376*m2^6*MBhat*Ycut^6 + 7616*m2^7*MBhat*Ycut^6 - 
          5907720*MBhat^2*Ycut^6 + 30343166*m2*MBhat^2*Ycut^6 + 
          50278410*m2^2*MBhat^2*Ycut^6 - 19998160*m2^3*MBhat^2*Ycut^6 + 
          624330*m2^4*MBhat^2*Ycut^6 + 368466*m2^5*MBhat^2*Ycut^6 - 
          199136*m2^6*MBhat^2*Ycut^6 + 31360*m2^7*MBhat^2*Ycut^6 + 
          6562864*MBhat^3*Ycut^6 - 28754544*m2*MBhat^3*Ycut^6 - 
          10491600*m2^2*MBhat^3*Ycut^6 + 7230720*m2^3*MBhat^3*Ycut^6 - 
          857360*m2^4*MBhat^3*Ycut^6 + 823984*m2^5*MBhat^3*Ycut^6 - 
          497840*m2^6*MBhat^3*Ycut^6 + 89600*m2^7*MBhat^3*Ycut^6 - 
          3351712*MBhat^4*Ycut^6 + 13514760*m2*MBhat^4*Ycut^6 - 
          5181750*m2^2*MBhat^4*Ycut^6 + 547960*m2^3*MBhat^4*Ycut^6 + 
          1016050*m2^4*MBhat^4*Ycut^6 - 856128*m2^5*MBhat^4*Ycut^6 + 
          178360*m2^6*MBhat^4*Ycut^6 + 729624*MBhat^5*Ycut^6 - 
          3037440*m2*MBhat^5*Ycut^6 + 2298240*m2^2*MBhat^5*Ycut^6 - 
          325920*m2^3*MBhat^5*Ycut^6 - 327320*m2^4*MBhat^5*Ycut^6 + 
          103488*m2^5*MBhat^5*Ycut^6 - 60760*MBhat^6*Ycut^6 + 
          243600*m2*MBhat^6*Ycut^6 - 252630*m2^2*MBhat^6*Ycut^6 + 
          19880*m2^3*MBhat^6*Ycut^6 + 14700*m2^4*MBhat^6*Ycut^6 + 
          150224*Ycut^7 - 897344*m2*Ycut^7 - 7887880*m2^2*Ycut^7 - 
          4248440*m2^3*Ycut^7 + 1105160*m2^4*Ycut^7 + 6328*m2^5*Ycut^7 - 
          2688*m2^6*Ycut^7 - 240*m2^7*Ycut^7 - 589598*MBhat*Ycut^7 + 
          4097408*m2*MBhat*Ycut^7 + 21471492*m2^2*MBhat*Ycut^7 + 
          1563940*m2^3*MBhat*Ycut^7 - 1521730*m2^4*MBhat*Ycut^7 + 
          87612*m2^5*MBhat*Ycut^7 + 12796*m2^6*MBhat*Ycut^7 - 
          2176*m2^7*MBhat*Ycut^7 + 2271696*MBhat^2*Ycut^7 - 
          9959488*m2*MBhat^2*Ycut^7 - 19075140*m2^2*MBhat^2*Ycut^7 + 
          4716320*m2^3*MBhat^2*Ycut^7 - 314580*m2^4*MBhat^2*Ycut^7 - 
          74592*m2^5*MBhat^2*Ycut^7 + 60928*m2^6*MBhat^2*Ycut^7 - 
          8960*m2^7*MBhat^2*Ycut^7 - 2386942*MBhat^3*Ycut^7 + 
          8679944*m2*MBhat^3*Ycut^7 + 6104140*m2^2*MBhat^3*Ycut^7 - 
          1806560*m2^3*MBhat^3*Ycut^7 - 164150*m2^4*MBhat^3*Ycut^7 - 
          144536*m2^5*MBhat^3*Ycut^7 + 148120*m2^6*MBhat^3*Ycut^7 - 
          25600*m2^7*MBhat^3*Ycut^7 + 373856*MBhat^4*Ycut^7 - 
          3066000*m2*MBhat^4*Ycut^7 + 871920*m2^2*MBhat^4*Ycut^7 - 
          557480*m2^3*MBhat^4*Ycut^7 - 156800*m2^4*MBhat^4*Ycut^7 + 
          256704*m2^5*MBhat^4*Ycut^7 - 50960*m2^6*MBhat^4*Ycut^7 + 
          428736*MBhat^5*Ycut^7 + 484680*m2*MBhat^5*Ycut^7 - 
          748440*m2^2*MBhat^5*Ycut^7 + 156660*m2^3*MBhat^5*Ycut^7 + 
          99820*m2^4*MBhat^5*Ycut^7 - 29568*m2^5*MBhat^5*Ycut^7 - 
          112000*MBhat^6*Ycut^7 - 58240*m2*MBhat^6*Ycut^7 + 
          100660*m2^2*MBhat^6*Ycut^7 - 5600*m2^3*MBhat^6*Ycut^7 - 
          4200*m2^4*MBhat^6*Ycut^7 - 63886*Ycut^8 + 189938*m2*Ycut^8 + 
          1303400*m2^2*Ycut^8 + 942550*m2^3*Ycut^8 - 31640*m2^4*Ycut^8 - 
          12068*m2^5*Ycut^8 + 336*m2^6*Ycut^8 + 30*m2^7*Ycut^8 - 
          234008*MBhat*Ycut^8 - 24976*m2*MBhat*Ycut^8 - 3780504*m2^2*MBhat*
           Ycut^8 - 1205120*m2^3*MBhat*Ycut^8 + 298760*m2^4*MBhat*Ycut^8 - 
          20160*m2^5*MBhat*Ycut^8 - 2072*m2^6*MBhat*Ycut^8 + 
          272*m2^7*MBhat*Ycut^8 - 659253*MBhat^2*Ycut^8 + 
          2121308*m2*MBhat^2*Ycut^8 + 3266235*m2^2*MBhat^2*Ycut^8 - 
          418180*m2^3*MBhat^2*Ycut^8 + 142905*m2^4*MBhat^2*Ycut^8 - 
          6930*m2^5*MBhat^2*Ycut^8 - 9128*m2^6*MBhat^2*Ycut^8 + 
          1120*m2^7*MBhat^2*Ycut^8 + 872912*MBhat^3*Ycut^8 - 
          1375248*m2*MBhat^3*Ycut^8 - 1722560*m2^2*MBhat^3*Ycut^8 + 
          44800*m2^3*MBhat^3*Ycut^8 + 279160*m2^4*MBhat^3*Ycut^8 - 
          36176*m2^5*MBhat^3*Ycut^8 - 20720*m2^6*MBhat^3*Ycut^8 + 
          3200*m2^7*MBhat^3*Ycut^8 + 424634*MBhat^4*Ycut^8 - 
          421470*m2*MBhat^4*Ycut^8 + 131880*m2^2*MBhat^4*Ycut^8 + 
          316120*m2^3*MBhat^4*Ycut^8 - 54110*m2^4*MBhat^4*Ycut^8 - 
          36624*m2^5*MBhat^4*Ycut^8 + 6370*m2^6*MBhat^4*Ycut^8 - 
          633192*MBhat^5*Ycut^8 + 304080*m2*MBhat^5*Ycut^8 + 
          115920*m2^2*MBhat^5*Ycut^8 - 45360*m2^3*MBhat^5*Ycut^8 - 
          14840*m2^4*MBhat^5*Ycut^8 + 3696*m2^5*MBhat^5*Ycut^8 + 
          124565*MBhat^6*Ycut^8 - 5880*m2*MBhat^6*Ycut^8 - 
          20930*m2^2*MBhat^6*Ycut^8 + 700*m2^3*MBhat^6*Ycut^8 + 
          525*m2^4*MBhat^6*Ycut^8 + 87584*Ycut^9 - 128240*m2*Ycut^9 - 
          31920*m2^2*Ycut^9 + 5040*m2^3*Ycut^9 - 30240*m2^4*Ycut^9 + 
          2576*m2^5*Ycut^9 + 400022*MBhat*Ycut^9 - 575680*m2*MBhat*Ycut^9 + 
          229950*m2^2*MBhat*Ycut^9 + 199920*m2^3*MBhat*Ycut^9 - 
          39025*m2^4*MBhat*Ycut^9 + 2268*m2^5*MBhat*Ycut^9 + 
          105*m2^6*MBhat*Ycut^9 + 95368*MBhat^2*Ycut^9 - 441056*m2*MBhat^2*
           Ycut^9 + 142800*m2^2*MBhat^2*Ycut^9 + 2240*m2^3*MBhat^2*Ycut^9 - 
          44520*m2^4*MBhat^2*Ycut^9 + 5712*m2^5*MBhat^2*Ycut^9 + 
          336*m2^6*MBhat^2*Ycut^9 - 532784*MBhat^3*Ycut^9 + 
          270340*m2*MBhat^3*Ycut^9 + 167720*m2^2*MBhat^3*Ycut^9 + 
          112840*m2^3*MBhat^3*Ycut^9 - 110320*m2^4*MBhat^3*Ycut^9 + 
          21014*m2^5*MBhat^3*Ycut^9 + 490*m2^6*MBhat^3*Ycut^9 - 
          180208*MBhat^4*Ycut^9 + 444640*m2*MBhat^4*Ycut^9 - 
          78960*m2^2*MBhat^4*Ycut^9 - 97440*m2^3*MBhat^4*Ycut^9 + 
          26880*m2^4*MBhat^4*Ycut^9 + 1008*m2^5*MBhat^4*Ycut^9 + 
          341250*MBhat^5*Ycut^9 - 202160*m2*MBhat^5*Ycut^9 + 
          8400*m2^2*MBhat^5*Ycut^9 + 5880*m2^3*MBhat^5*Ycut^9 + 
          525*m2^4*MBhat^5*Ycut^9 - 59080*MBhat^6*Ycut^9 + 
          6720*m2*MBhat^6*Ycut^9 + 560*m2^2*MBhat^6*Ycut^9 - 111664*Ycut^10 + 
          134484*m2*Ycut^10 - 13370*m2^2*Ycut^10 - 30380*m2^3*Ycut^10 + 
          3920*m2^4*Ycut^10 + 42*m2^5*Ycut^10 - 285544*MBhat*Ycut^10 + 
          355712*m2*MBhat*Ycut^10 - 77112*m2^2*MBhat*Ycut^10 - 
          3360*m2^3*MBhat*Ycut^10 + 2240*m2^4*MBhat*Ycut^10 + 
          123844*MBhat^2*Ycut^10 + 72940*m2*MBhat^2*Ycut^10 - 
          106680*m2^2*MBhat^2*Ycut^10 - 10360*m2^3*MBhat^2*Ycut^10 + 
          6720*m2^4*MBhat^2*Ycut^10 - 630*m2^5*MBhat^2*Ycut^10 + 
          273280*MBhat^3*Ycut^10 - 149520*m2*MBhat^3*Ycut^10 + 
          11760*m2^2*MBhat^3*Ycut^10 - 33600*m2^3*MBhat^3*Ycut^10 + 
          17920*m2^4*MBhat^3*Ycut^10 - 2688*m2^5*MBhat^3*Ycut^10 - 
          15400*MBhat^4*Ycut^10 - 104720*m2*MBhat^4*Ycut^10 + 
          9870*m2^2*MBhat^4*Ycut^10 + 13020*m2^3*MBhat^4*Ycut^10 - 
          3150*m2^4*MBhat^4*Ycut^10 - 98616*MBhat^5*Ycut^10 + 
          48160*m2*MBhat^5*Ycut^10 - 6720*m2^2*MBhat^5*Ycut^10 + 
          12740*MBhat^6*Ycut^10 - 1120*m2*MBhat^6*Ycut^10 + 
          420*m2^2*MBhat^6*Ycut^10 + 94304*Ycut^11 - 88592*m2*Ycut^11 + 
          7840*m2^2*Ycut^11 + 2800*m2^3*Ycut^11 + 116144*MBhat*Ycut^11 - 
          124096*m2*MBhat*Ycut^11 + 32676*m2^2*MBhat*Ycut^11 + 
          420*m2^3*MBhat*Ycut^11 + 140*m2^4*MBhat*Ycut^11 - 
          130424*MBhat^2*Ycut^11 + 22624*m2*MBhat^2*Ycut^11 + 
          6720*m2^2*MBhat^2*Ycut^11 + 2240*m2^3*MBhat^2*Ycut^11 - 
          72506*MBhat^3*Ycut^11 + 40600*m2*MBhat^3*Ycut^11 - 
          1470*m2^2*MBhat^3*Ycut^11 + 3360*m2^3*MBhat^3*Ycut^11 - 
          700*m2^4*MBhat^3*Ycut^11 + 26432*MBhat^4*Ycut^11 + 
          11200*m2*MBhat^4*Ycut^11 + 15918*MBhat^5*Ycut^11 - 
          4340*m2*MBhat^5*Ycut^11 + 840*m2^2*MBhat^5*Ycut^11 - 
          280*MBhat^6*Ycut^11 - 52472*Ycut^12 + 35112*m2*Ycut^12 - 
          2800*m2^2*Ycut^12 - 350*m2^3*Ycut^12 - 18088*MBhat*Ycut^12 + 
          20496*m2*MBhat*Ycut^12 - 2856*m2^2*MBhat*Ycut^12 + 
          55783*MBhat^2*Ycut^12 - 11242*m2*MBhat^2*Ycut^12 - 
          840*m2^2*MBhat^2*Ycut^12 + 6104*MBhat^3*Ycut^12 - 
          5040*m2*MBhat^3*Ycut^12 - 6706*MBhat^4*Ycut^12 - 
          840*m2*MBhat^4*Ycut^12 - 1848*MBhat^5*Ycut^12 - 
          245*MBhat^6*Ycut^12 + 18592*Ycut^13 - 7504*m2*Ycut^13 - 
          4795*MBhat*Ycut^13 - 672*m2*MBhat*Ycut^13 + 357*m2^2*MBhat*
           Ycut^13 - 11480*MBhat^2*Ycut^13 + 1232*m2*MBhat^2*Ycut^13 + 
          784*MBhat^3*Ycut^13 + 630*m2*MBhat^3*Ycut^13 + 
          896*MBhat^4*Ycut^13 + 231*MBhat^5*Ycut^13 - 3824*Ycut^14 + 
          630*m2*Ycut^14 + 2528*MBhat*Ycut^14 + 952*MBhat^2*Ycut^14 - 
          154*m2*MBhat^2*Ycut^14 - 176*MBhat^3*Ycut^14 - 
          112*MBhat^4*Ycut^14 + 352*Ycut^15 - 330*MBhat*Ycut^15 + 
          22*MBhat^3*Ycut^15))/(105*(-1 + Ycut)^8) + 
       8*(8 - 16*m2 - 750*m2^2 - 1110*m2^3 - 190*m2^4 + 18*m2^5 - 48*MBhat + 
         112*m2*MBhat + 2448*m2^2*MBhat + 1840*m2^3*MBhat + 120*MBhat^2 - 
         296*m2*MBhat^2 - 2955*m2^2*MBhat^2 - 824*m2^3*MBhat^2 + 
         77*m2^4*MBhat^2 - 160*MBhat^3 + 384*m2*MBhat^3 + 1584*m2^2*MBhat^3 + 
         120*MBhat^4 - 256*m2*MBhat^4 - 336*m2^2*MBhat^4 + 62*m2^3*MBhat^4 - 
         48*MBhat^5 + 80*m2*MBhat^5 + 8*MBhat^6 - 8*m2*MBhat^6 + 
         9*m2^2*MBhat^6)*Log[m2] - 8*(8 - 16*m2 - 750*m2^2 - 1110*m2^3 - 
         190*m2^4 + 18*m2^5 - 48*MBhat + 112*m2*MBhat + 2448*m2^2*MBhat + 
         1840*m2^3*MBhat + 120*MBhat^2 - 296*m2*MBhat^2 - 2955*m2^2*MBhat^2 - 
         824*m2^3*MBhat^2 + 77*m2^4*MBhat^2 - 160*MBhat^3 + 384*m2*MBhat^3 + 
         1584*m2^2*MBhat^3 + 120*MBhat^4 - 256*m2*MBhat^4 - 
         336*m2^2*MBhat^4 + 62*m2^3*MBhat^4 - 48*MBhat^5 + 80*m2*MBhat^5 + 
         8*MBhat^6 - 8*m2*MBhat^6 + 9*m2^2*MBhat^6)*Log[1 - Ycut]) + 
     c[SL]*(-1/30*(Ycut^3*(-350*MBhat^3 + 700*m2*MBhat^3 + 
           1750*m2^2*MBhat^3 - 7000*m2^3*MBhat^3 + 8750*m2^4*MBhat^3 - 
           4900*m2^5*MBhat^3 + 1050*m2^6*MBhat^3 + 480*MBhat^4 - 
           4800*m2^2*MBhat^4 + 9600*m2^3*MBhat^4 - 7200*m2^4*MBhat^4 + 
           1920*m2^5*MBhat^4 - 150*MBhat^5 - 600*m2*MBhat^5 + 
           2700*m2^2*MBhat^5 - 3000*m2^3*MBhat^5 + 1050*m2^4*MBhat^5 + 
           160*m2*MBhat^6 - 320*m2^2*MBhat^6 + 160*m2^3*MBhat^6 + 
           525*MBhat^2*Ycut - 1050*m2*MBhat^2*Ycut - 2625*m2^2*MBhat^2*Ycut + 
           10500*m2^3*MBhat^2*Ycut - 13125*m2^4*MBhat^2*Ycut + 
           7350*m2^5*MBhat^2*Ycut - 1575*m2^6*MBhat^2*Ycut + 
           2240*MBhat^3*Ycut - 5600*m2*MBhat^3*Ycut - 2800*m2^2*MBhat^3*
            Ycut + 22400*m2^3*MBhat^3*Ycut - 28000*m2^4*MBhat^3*Ycut + 
           14560*m2^5*MBhat^3*Ycut - 2800*m2^6*MBhat^3*Ycut - 
           3765*MBhat^4*Ycut + 1350*m2*MBhat^4*Ycut + 24450*m2^2*MBhat^4*
            Ycut - 43800*m2^3*MBhat^4*Ycut + 27675*m2^4*MBhat^4*Ycut - 
           5910*m2^5*MBhat^4*Ycut + 1200*MBhat^5*Ycut + 3840*m2*MBhat^5*
            Ycut - 14880*m2^2*MBhat^5*Ycut + 13440*m2^3*MBhat^5*Ycut - 
           3600*m2^4*MBhat^5*Ycut + 10*MBhat^6*Ycut - 1040*m2*MBhat^6*Ycut + 
           1610*m2^2*MBhat^6*Ycut - 580*m2^3*MBhat^6*Ycut - 
           315*MBhat*Ycut^2 + 630*m2*MBhat*Ycut^2 + 1575*m2^2*MBhat*Ycut^2 - 
           6300*m2^3*MBhat*Ycut^2 + 7875*m2^4*MBhat*Ycut^2 - 
           4410*m2^5*MBhat*Ycut^2 + 945*m2^6*MBhat*Ycut^2 - 
           4008*MBhat^2*Ycut^2 + 8400*m2*MBhat^2*Ycut^2 + 9000*m2^2*MBhat^2*
            Ycut^2 - 39840*m2^3*MBhat^2*Ycut^2 + 41640*m2^4*MBhat^2*Ycut^2 - 
           17712*m2^5*MBhat^2*Ycut^2 + 2520*m2^6*MBhat^2*Ycut^2 - 
           5230*MBhat^3*Ycut^2 + 17916*m2*MBhat^3*Ycut^2 - 
           12400*m2^2*MBhat^3*Ycut^2 - 22000*m2^3*MBhat^3*Ycut^2 + 
           39690*m2^4*MBhat^3*Ycut^2 - 21980*m2^5*MBhat^3*Ycut^2 + 
           4004*m2^6*MBhat^3*Ycut^2 + 12840*MBhat^4*Ycut^2 - 
           8400*m2*MBhat^4*Ycut^2 - 51600*m2^2*MBhat^4*Ycut^2 + 
           85440*m2^3*MBhat^4*Ycut^2 - 46680*m2^4*MBhat^4*Ycut^2 + 
           8400*m2^5*MBhat^4*Ycut^2 - 4215*MBhat^5*Ycut^2 - 
           10560*m2*MBhat^5*Ycut^2 + 34740*m2^2*MBhat^5*Ycut^2 - 
           25140*m2^3*MBhat^5*Ycut^2 + 5175*m2^4*MBhat^5*Ycut^2 - 
           80*MBhat^6*Ycut^2 + 2880*m2*MBhat^6*Ycut^2 - 3280*m2^2*MBhat^6*
            Ycut^2 + 800*m2^3*MBhat^6*Ycut^2 + 70*Ycut^3 - 140*m2*Ycut^3 - 
           350*m2^2*Ycut^3 + 1400*m2^3*Ycut^3 - 1750*m2^4*Ycut^3 + 
           980*m2^5*Ycut^3 - 210*m2^6*Ycut^3 + 2568*MBhat*Ycut^3 - 
           5040*m2*MBhat*Ycut^3 - 6360*m2^2*MBhat*Ycut^3 + 
           24480*m2^3*MBhat*Ycut^3 - 23400*m2^4*MBhat*Ycut^3 + 
           8592*m2^5*MBhat*Ycut^3 - 840*m2^6*MBhat*Ycut^3 + 
           13059*MBhat^2*Ycut^3 - 28182*m2*MBhat^2*Ycut^3 - 
           5850*m2^2*MBhat^2*Ycut^3 + 58320*m2^3*MBhat^2*Ycut^3 - 
           53925*m2^4*MBhat^2*Ycut^3 + 18846*m2^5*MBhat^2*Ycut^3 - 
           2268*m2^6*MBhat^2*Ycut^3 + 3200*MBhat^3*Ycut^3 - 
           28928*m2*MBhat^3*Ycut^3 + 44400*m2^2*MBhat^3*Ycut^3 - 
           480*m2^3*MBhat^3*Ycut^3 - 36000*m2^4*MBhat^3*Ycut^3 + 
           21280*m2^5*MBhat^3*Ycut^3 - 3472*m2^6*MBhat^3*Ycut^3 - 
           24771*MBhat^4*Ycut^3 + 22200*m2*MBhat^4*Ycut^3 + 
           58350*m2^2*MBhat^4*Ycut^3 - 94080*m2^3*MBhat^4*Ycut^3 + 
           45525*m2^4*MBhat^4*Ycut^3 - 7224*m2^5*MBhat^4*Ycut^3 + 
           8520*MBhat^5*Ycut^3 + 16320*m2*MBhat^5*Ycut^3 - 
           44640*m2^2*MBhat^5*Ycut^3 + 25440*m2^3*MBhat^5*Ycut^3 - 
           4200*m2^4*MBhat^5*Ycut^3 + 280*MBhat^6*Ycut^3 - 
           4400*m2*MBhat^6*Ycut^3 + 3390*m2^2*MBhat^6*Ycut^3 - 
           520*m2^3*MBhat^6*Ycut^3 - 592*Ycut^4 + 1120*m2*Ycut^4 + 
           1520*m2^2*Ycut^4 - 5440*m2^3*Ycut^4 + 4880*m2^4*Ycut^4 - 
           1568*m2^5*Ycut^4 + 80*m2^6*Ycut^4 - 9174*MBhat*Ycut^4 + 
           17160*m2*MBhat*Ycut^4 + 8400*m2^2*MBhat*Ycut^4 - 
           36840*m2^3*MBhat*Ycut^4 + 26130*m2^4*MBhat*Ycut^4 - 
           6096*m2^5*MBhat*Ycut^4 + 420*m2^6*MBhat*Ycut^4 - 
           23184*MBhat^2*Ycut^4 + 51456*m2*MBhat^2*Ycut^4 - 
           15000*m2^2*MBhat^2*Ycut^4 - 41280*m2^3*MBhat^2*Ycut^4 + 
           38880*m2^4*MBhat^2*Ycut^4 - 12096*m2^5*MBhat^2*Ycut^4 + 
           1224*m2^6*MBhat^2*Ycut^4 + 9378*MBhat^3*Ycut^4 + 
           22648*m2*MBhat^3*Ycut^4 - 59300*m2^2*MBhat^3*Ycut^4 + 
           14920*m2^3*MBhat^3*Ycut^4 + 23130*m2^4*MBhat^3*Ycut^4 - 
           12608*m2^5*MBhat^3*Ycut^4 + 1832*m2^6*MBhat^3*Ycut^4 + 
           29328*MBhat^4*Ycut^4 - 32400*m2*MBhat^4*Ycut^4 - 
           38400*m2^2*MBhat^4*Ycut^4 + 64320*m2^3*MBhat^4*Ycut^4 - 
           27120*m2^4*MBhat^4*Ycut^4 + 3792*m2^5*MBhat^4*Ycut^4 - 
           10920*MBhat^5*Ycut^4 - 15600*m2*MBhat^5*Ycut^4 + 
           34260*m2^2*MBhat^5*Ycut^4 - 14880*m2^3*MBhat^5*Ycut^4 + 
           2100*m2^4*MBhat^5*Ycut^4 - 560*MBhat^6*Ycut^4 + 
           4000*m2*MBhat^6*Ycut^4 - 1760*m2^2*MBhat^6*Ycut^4 + 
           160*m2^3*MBhat^6*Ycut^4 + 2216*Ycut^5 - 3840*m2*Ycut^5 - 
           2410*m2^2*Ycut^5 + 8120*m2^3*Ycut^5 - 4740*m2^4*Ycut^5 + 
           664*m2^5*Ycut^5 - 10*m2^6*Ycut^5 + 18744*MBhat*Ycut^5 - 
           32400*m2*MBhat*Ycut^5 - 1320*m2^2*MBhat*Ycut^5 + 
           26880*m2^3*MBhat*Ycut^5 - 14040*m2^4*MBhat*Ycut^5 + 
           2256*m2^5*MBhat*Ycut^5 - 120*m2^6*MBhat*Ycut^5 + 
           23058*MBhat^2*Ycut^5 - 54996*m2*MBhat^2*Ycut^5 + 
           30600*m2^2*MBhat^2*Ycut^5 + 14820*m2^3*MBhat^2*Ycut^5 - 
           17415*m2^4*MBhat^2*Ycut^5 + 4302*m2^5*MBhat^2*Ycut^5 - 
           369*m2^6*MBhat^2*Ycut^5 - 24624*MBhat^3*Ycut^5 - 
           2496*m2*MBhat^3*Ycut^5 + 41200*m2^2*MBhat^3*Ycut^5 - 
           10880*m2^3*MBhat^3*Ycut^5 - 9840*m2^4*MBhat^3*Ycut^5 + 
           4384*m2^5*MBhat^3*Ycut^5 - 544*m2^6*MBhat^3*Ycut^5 - 
           21378*MBhat^4*Ycut^5 + 28500*m2*MBhat^4*Ycut^5 + 
           15150*m2^2*MBhat^4*Ycut^5 - 27480*m2^3*MBhat^4*Ycut^5 + 
           9285*m2^4*MBhat^4*Ycut^5 - 1122*m2^5*MBhat^4*Ycut^5 + 
           9240*MBhat^5*Ycut^5 + 9600*m2*MBhat^5*Ycut^5 - 15840*m2^2*MBhat^5*
            Ycut^5 + 4800*m2^3*MBhat^5*Ycut^5 - 600*m2^4*MBhat^5*Ycut^5 + 
           700*MBhat^6*Ycut^5 - 2160*m2*MBhat^6*Ycut^5 + 310*m2^2*MBhat^6*
            Ycut^5 - 20*m2^3*MBhat^6*Ycut^5 - 4816*Ycut^6 + 7360*m2*Ycut^6 + 
           1440*m2^2*Ycut^6 - 5760*m2^3*Ycut^6 + 1840*m2^4*Ycut^6 - 
           64*m2^5*Ycut^6 - 23898*MBhat*Ycut^6 + 36900*m2*MBhat*Ycut^6 - 
           6690*m2^2*MBhat*Ycut^6 - 9900*m2^3*MBhat*Ycut^6 + 
           3915*m2^4*MBhat*Ycut^6 - 342*m2^5*MBhat*Ycut^6 + 
           15*m2^6*MBhat*Ycut^6 - 10080*MBhat^2*Ycut^6 + 33792*m2*MBhat^2*
            Ycut^6 - 23400*m2^2*MBhat^2*Ycut^6 - 2880*m2^3*MBhat^2*Ycut^6 + 
           4440*m2^4*MBhat^2*Ycut^6 - 720*m2^5*MBhat^2*Ycut^6 + 
           48*m2^6*MBhat^2*Ycut^6 + 27804*MBhat^3*Ycut^6 - 
           10280*m2*MBhat^3*Ycut^6 - 16000*m2^2*MBhat^3*Ycut^6 + 
           3520*m2^3*MBhat^3*Ycut^6 + 2590*m2^4*MBhat^3*Ycut^6 - 
           784*m2^5*MBhat^3*Ycut^6 + 70*m2^6*MBhat^3*Ycut^6 + 
           8736*MBhat^4*Ycut^6 - 15600*m2*MBhat^4*Ycut^6 - 
           3600*m2^2*MBhat^4*Ycut^6 + 6720*m2^3*MBhat^4*Ycut^6 - 
           1560*m2^4*MBhat^4*Ycut^6 + 144*m2^5*MBhat^4*Ycut^6 - 
           5250*MBhat^5*Ycut^6 - 3840*m2*MBhat^5*Ycut^6 + 4140*m2^2*MBhat^5*
            Ycut^6 - 660*m2^3*MBhat^5*Ycut^6 + 75*m2^4*MBhat^5*Ycut^6 - 
           560*MBhat^6*Ycut^6 + 640*m2*MBhat^6*Ycut^6 + 80*m2^2*MBhat^6*
            Ycut^6 + 6692*Ycut^7 - 8600*m2*Ycut^7 + 190*m2^2*Ycut^7 + 
           1960*m2^3*Ycut^7 - 230*m2^4*Ycut^7 - 12*m2^5*Ycut^7 + 
           19320*MBhat*Ycut^7 - 25680*m2*MBhat*Ycut^7 + 6360*m2^2*MBhat*
            Ycut^7 + 1920*m2^3*MBhat*Ycut^7 - 480*m2^4*MBhat*Ycut^7 - 
           3402*MBhat^2*Ycut^7 - 10140*m2*MBhat^2*Ycut^7 + 
           8850*m2^2*MBhat^2*Ycut^7 + 360*m2^3*MBhat^2*Ycut^7 - 
           495*m2^4*MBhat^2*Ycut^7 + 30*m2^5*MBhat^2*Ycut^7 - 
           17808*MBhat^3*Ycut^7 + 8704*m2*MBhat^3*Ycut^7 + 
           3600*m2^2*MBhat^3*Ycut^7 - 480*m2^3*MBhat^3*Ycut^7 - 
           320*m2^4*MBhat^3*Ycut^7 + 48*m2^5*MBhat^3*Ycut^7 - 
           1110*MBhat^4*Ycut^7 + 5400*m2*MBhat^4*Ycut^7 + 450*m2^2*MBhat^4*
            Ycut^7 - 720*m2^3*MBhat^4*Ycut^7 + 75*m2^4*MBhat^4*Ycut^7 + 
           2040*MBhat^5*Ycut^7 + 960*m2*MBhat^5*Ycut^7 - 480*m2^2*MBhat^5*
            Ycut^7 + 280*MBhat^6*Ycut^7 - 80*m2*MBhat^6*Ycut^7 - 
           30*m2^2*MBhat^6*Ycut^7 - 6160*Ycut^8 + 6240*m2*Ycut^8 - 
           560*m2^2*Ycut^8 - 320*m2^3*Ycut^8 - 9408*MBhat*Ycut^8 + 
           10440*m2*MBhat*Ycut^8 - 2280*m2^2*MBhat*Ycut^8 - 
           240*m2^3*MBhat*Ycut^8 + 7056*MBhat^2*Ycut^8 + 192*m2*MBhat^2*
            Ycut^8 - 1800*m2^2*MBhat^2*Ycut^8 + 6510*MBhat^3*Ycut^8 - 
           3252*m2*MBhat^3*Ycut^8 - 450*m2^2*MBhat^3*Ycut^8 - 
           624*MBhat^4*Ycut^8 - 1200*m2*MBhat^4*Ycut^8 - 570*MBhat^5*Ycut^8 - 
           120*m2*MBhat^5*Ycut^8 - 80*MBhat^6*Ycut^8 + 3752*Ycut^9 - 
           2720*m2*Ycut^9 + 170*m2^2*Ycut^9 + 40*m2^3*Ycut^9 + 
           2184*MBhat*Ycut^9 - 2160*m2*MBhat*Ycut^9 + 360*m2^2*MBhat*Ycut^9 - 
           3951*MBhat^2*Ycut^9 + 654*m2*MBhat^2*Ycut^9 + 225*m2^2*MBhat^2*
            Ycut^9 - 1168*MBhat^3*Ycut^9 + 672*m2*MBhat^3*Ycut^9 + 
           327*MBhat^4*Ycut^9 + 150*m2*MBhat^4*Ycut^9 + 120*MBhat^5*Ycut^9 + 
           10*MBhat^6*Ycut^9 - 1456*Ycut^10 + 640*m2*Ycut^10 + 
           141*MBhat*Ycut^10 + 150*m2*MBhat*Ycut^10 - 45*m2^2*MBhat*Ycut^10 + 
           1032*MBhat^2*Ycut^10 - 144*m2*MBhat^2*Ycut^10 + 
           34*MBhat^3*Ycut^10 - 84*m2*MBhat^3*Ycut^10 - 72*MBhat^4*Ycut^10 - 
           15*MBhat^5*Ycut^10 + 326*Ycut^11 - 60*m2*Ycut^11 - 
           192*MBhat*Ycut^11 - 105*MBhat^2*Ycut^11 + 18*m2*MBhat^2*Ycut^11 + 
           16*MBhat^3*Ycut^11 + 9*MBhat^4*Ycut^11 - 32*Ycut^12 + 
           30*MBhat*Ycut^12 - 2*MBhat^3*Ycut^12)*c[T])/(-1 + Ycut)^8 + 
       c[SR]*(-1/30*(Sqrt[m2]*(5738 + 48498*m2 + 19950*m2^2 - 59200*m2^3 - 
             15450*m2^4 + 462*m2^5 + 2*m2^6 - 29400*MBhat - 158976*m2*MBhat + 
             48600*m2^2*MBhat + 134400*m2^3*MBhat + 5400*m2^4*MBhat - 
             24*m2^6*MBhat + 61386*MBhat^2 + 186675*m2*MBhat^2 - 
             167025*m2^2*MBhat^2 - 83100*m2^3*MBhat^2 + 1650*m2^4*MBhat^2 + 
             489*m2^5*MBhat^2 - 75*m2^6*MBhat^2 - 66480*MBhat^3 - 
             86496*m2*MBhat^3 + 141000*m2^2*MBhat^3 + 12800*m2^3*MBhat^3 - 
             1200*m2^4*MBhat^3 + 480*m2^5*MBhat^3 - 104*m2^6*MBhat^3 + 
             39210*MBhat^4 + 4005*m2*MBhat^4 - 45000*m2^2*MBhat^4 + 
             1020*m2^3*MBhat^4 + 990*m2^4*MBhat^4 - 225*m2^5*MBhat^4 - 
             11880*MBhat^5 + 7680*m2*MBhat^5 + 4320*m2^2*MBhat^5 - 
             120*m2^4*MBhat^5 + 1430*MBhat^6 - 1530*m2*MBhat^6 + 
             90*m2^2*MBhat^6 + 10*m2^3*MBhat^6 - 41486*Ycut - 
             365226*m2*Ycut - 213450*m2^2*Ycut + 370000*m2^3*Ycut + 
             104550*m2^4*Ycut - 3054*m2^5*Ycut - 14*m2^6*Ycut + 
             213000*MBhat*Ycut + 1216512*m2*MBhat*Ycut - 145800*m2^2*MBhat*
              Ycut - 883200*m2^3*MBhat*Ycut - 37800*m2^4*MBhat*Ycut + 
             168*m2^6*MBhat*Ycut - 445902*MBhat^2*Ycut - 1472145*m2*MBhat^2*
              Ycut + 982155*m2^2*MBhat^2*Ycut + 561180*m2^3*MBhat^2*Ycut - 
             10470*m2^4*MBhat^2*Ycut - 3423*m2^5*MBhat^2*Ycut + 
             525*m2^6*MBhat^2*Ycut + 484560*MBhat^3*Ycut + 737952*m2*MBhat^3*
              Ycut - 907800*m2^2*MBhat^3*Ycut - 89600*m2^3*MBhat^3*Ycut + 
             8400*m2^4*MBhat^3*Ycut - 3360*m2^5*MBhat^3*Ycut + 
             728*m2^6*MBhat^3*Ycut - 287070*MBhat^4*Ycut - 83655*m2*MBhat^4*
              Ycut + 302040*m2^2*MBhat^4*Ycut - 6060*m2^3*MBhat^4*Ycut - 
             6930*m2^4*MBhat^4*Ycut + 1575*m2^5*MBhat^4*Ycut + 
             87480*MBhat^5*Ycut - 42240*m2*MBhat^5*Ycut - 30240*m2^2*MBhat^5*
              Ycut + 840*m2^4*MBhat^5*Ycut - 10610*MBhat^6*Ycut + 
             9810*m2*MBhat^6*Ycut - 450*m2^2*MBhat^6*Ycut - 
             70*m2^3*MBhat^6*Ycut + 129078*Ycut^2 + 1185768*m2*Ycut^2 + 
             898650*m2^2*Ycut^2 - 954600*m2^3*Ycut^2 - 301050*m2^4*Ycut^2 + 
             8532*m2^5*Ycut^2 + 42*m2^6*Ycut^2 - 664200*MBhat*Ycut^2 - 
             4012416*m2*MBhat*Ycut^2 - 243000*m2^2*MBhat*Ycut^2 + 
             2448000*m2^3*MBhat*Ycut^2 + 113400*m2^4*MBhat*Ycut^2 - 
             504*m2^6*MBhat*Ycut^2 + 1394406*MBhat^2*Ycut^2 + 
             4995405*m2*MBhat^2*Ycut^2 - 2291895*m2^2*MBhat^2*Ycut^2 - 
             1611720*m2^3*MBhat^2*Ycut^2 + 27630*m2^4*MBhat^2*Ycut^2 + 
             10269*m2^5*MBhat^2*Ycut^2 - 1575*m2^6*MBhat^2*Ycut^2 - 
             1520880*MBhat^3*Ycut^2 - 2677536*m2*MBhat^3*Ycut^2 + 
             2446200*m2^2*MBhat^3*Ycut^2 + 268800*m2^3*MBhat^3*Ycut^2 - 
             25200*m2^4*MBhat^3*Ycut^2 + 10080*m2^5*MBhat^3*Ycut^2 - 
             2184*m2^6*MBhat^3*Ycut^2 + 905310*MBhat^4*Ycut^2 + 
             445635*m2*MBhat^4*Ycut^2 - 860760*m2^2*MBhat^4*Ycut^2 + 
             14400*m2^3*MBhat^4*Ycut^2 + 20790*m2^4*MBhat^4*Ycut^2 - 
             4725*m2^5*MBhat^4*Ycut^2 - 277560*MBhat^5*Ycut^2 + 
             86400*m2*MBhat^5*Ycut^2 + 90720*m2^2*MBhat^5*Ycut^2 - 
             2520*m2^4*MBhat^5*Ycut^2 + 33930*MBhat^6*Ycut^2 - 
             26280*m2*MBhat^6*Ycut^2 + 720*m2^2*MBhat^6*Ycut^2 + 
             210*m2^3*MBhat^6*Ycut^2 - 224370*Ycut^3 - 2156460*m2*Ycut^3 - 
             2014350*m2^2*Ycut^3 + 1280200*m2^3*Ycut^3 + 476550*m2^4*Ycut^3 - 
             12960*m2^5*Ycut^3 - 70*m2^6*Ycut^3 + 1157400*MBhat*Ycut^3 + 
             7413120*m2*MBhat*Ycut^3 + 1765800*m2^2*MBhat*Ycut^3 - 
             3676800*m2^3*MBhat*Ycut^3 - 189000*m2^4*MBhat*Ycut^3 + 
             840*m2^6*MBhat*Ycut^3 - 2437410*MBhat^2*Ycut^3 - 
             9483615*m2*MBhat^2*Ycut^3 + 2510685*m2^2*MBhat^2*Ycut^3 + 
             2542560*m2^3*MBhat^2*Ycut^3 - 38490*m2^4*MBhat^2*Ycut^3 - 
             17115*m2^5*MBhat^2*Ycut^3 + 2625*m2^6*MBhat^2*Ycut^3 + 
             2668710*MBhat^3*Ycut^3 + 5390970*m2*MBhat^3*Ycut^3 - 
             3521900*m2^2*MBhat^3*Ycut^3 - 451500*m2^3*MBhat^3*Ycut^3 + 
             45150*m2^4*MBhat^3*Ycut^3 - 17710*m2^5*MBhat^3*Ycut^3 + 
             3640*m2^6*MBhat^3*Ycut^3 - 1596210*MBhat^4*Ycut^3 - 
             1133025*m2*MBhat^4*Ycut^3 + 1341720*m2^2*MBhat^4*Ycut^3 - 
             12600*m2^3*MBhat^4*Ycut^3 - 36210*m2^4*MBhat^4*Ycut^3 + 
             7875*m2^5*MBhat^4*Ycut^3 + 492390*MBhat^5*Ycut^3 - 
             63210*m2*MBhat^5*Ycut^3 - 150150*m2^2*MBhat^5*Ycut^3 - 
             750*m2^3*MBhat^5*Ycut^3 + 4200*m2^4*MBhat^5*Ycut^3 - 
             60670*MBhat^6*Ycut^3 + 37500*m2*MBhat^6*Ycut^3 - 
             20*m2^2*MBhat^6*Ycut^3 - 350*m2^3*MBhat^6*Ycut^3 + 
             235920*Ycut^4 + 2381685*m2*Ycut^4 + 2660100*m2^2*Ycut^4 - 
             891700*m2^3*Ycut^4 - 445050*m2^4*Ycut^4 + 11385*m2^5*Ycut^4 + 
             70*m2^6*Ycut^4 - 1220400*MBhat*Ycut^4 - 8320320*m2*MBhat*
              Ycut^4 - 3466800*m2^2*MBhat*Ycut^4 + 3172800*m2^3*MBhat*
              Ycut^4 + 189000*m2^4*MBhat*Ycut^4 - 840*m2^6*MBhat*Ycut^4 + 
             2579895*MBhat^2*Ycut^4 + 10929465*m2*MBhat^2*Ycut^4 - 
             875310*m2^2*MBhat^2*Ycut^4 - 2357760*m2^3*MBhat^2*Ycut^4 + 
             24315*m2^4*MBhat^2*Ycut^4 + 18480*m2^5*MBhat^2*Ycut^4 - 
             2625*m2^6*MBhat^2*Ycut^4 - 2835270*MBhat^3*Ycut^4 - 
             6554390*m2*MBhat^3*Ycut^4 + 2831780*m2^2*MBhat^3*Ycut^4 + 
             453780*m2^3*MBhat^3*Ycut^4 - 48670*m2^4*MBhat^3*Ycut^4 + 
             18850*m2^5*MBhat^3*Ycut^4 - 3640*m2^6*MBhat^3*Ycut^4 + 
             1702545*MBhat^4*Ycut^4 + 1625310*m2*MBhat^4*Ycut^4 - 
             1225260*m2^2*MBhat^4*Ycut^4 - 4140*m2^3*MBhat^4*Ycut^4 + 
             38745*m2^4*MBhat^4*Ycut^4 - 7875*m2^5*MBhat^4*Ycut^4 - 
             527850*MBhat^5*Ycut^4 - 39210*m2*MBhat^5*Ycut^4 + 
             148050*m2^2*MBhat^5*Ycut^4 + 2130*m2^3*MBhat^5*Ycut^4 - 
             4200*m2^4*MBhat^5*Ycut^4 + 65470*MBhat^6*Ycut^4 - 
             29420*m2*MBhat^6*Ycut^4 - 1450*m2^2*MBhat^6*Ycut^4 + 
             350*m2^3*MBhat^6*Ycut^4 - 150792*Ycut^5 - 1609191*m2*Ycut^5 - 
             2112660*m2^2*Ycut^5 + 224220*m2^3*Ycut^5 + 241830*m2^4*Ycut^5 - 
             5571*m2^5*Ycut^5 - 42*m2^6*Ycut^5 + 782199*MBhat*Ycut^5 + 
             5718897*m2*MBhat*Ycut^5 + 3441510*m2^2*MBhat*Ycut^5 - 
             1503630*m2^3*MBhat*Ycut^5 - 110565*m2^4*MBhat*Ycut^5 - 
             819*m2^5*MBhat*Ycut^5 + 504*m2^6*MBhat*Ycut^5 - 
             1664853*MBhat^2*Ycut^5 - 7707555*m2*MBhat^2*Ycut^5 - 
             786090*m2^2*MBhat^2*Ycut^5 + 1264200*m2^3*MBhat^2*Ycut^5 - 
             1665*m2^4*MBhat^2*Ycut^5 - 12048*m2^5*MBhat^2*Ycut^5 + 
             1575*m2^6*MBhat^2*Ycut^5 + 1835589*MBhat^3*Ycut^5 + 
             4864217*m2*MBhat^3*Ycut^5 - 1152840*m2^2*MBhat^3*Ycut^5 - 
             269760*m2^3*MBhat^3*Ycut^5 + 31675*m2^4*MBhat^3*Ycut^5 - 
             12369*m2^5*MBhat^3*Ycut^5 + 2184*m2^6*MBhat^3*Ycut^5 - 
             1102095*MBhat^4*Ycut^5 - 1376670*m2*MBhat^4*Ycut^5 + 
             645300*m2^2*MBhat^4*Ycut^5 + 15660*m2^3*MBhat^4*Ycut^5 - 
             25215*m2^4*MBhat^4*Ycut^5 + 4725*m2^5*MBhat^4*Ycut^5 + 
             341550*MBhat^5*Ycut^5 + 108990*m2*MBhat^5*Ycut^5 - 
             88080*m2^2*MBhat^5*Ycut^5 - 2100*m2^3*MBhat^5*Ycut^5 + 
             2520*m2^4*MBhat^5*Ycut^5 - 42330*MBhat^6*Ycut^5 + 
             10520*m2*MBhat^6*Ycut^5 + 2250*m2^2*MBhat^6*Ycut^5 - 
             210*m2^3*MBhat^6*Ycut^5 + 54982*Ycut^6 + 626277*m2*Ycut^6 + 
             962380*m2^2*Ycut^6 + 81360*m2^3*Ycut^6 - 68640*m2^4*Ycut^6 + 
             1409*m2^5*Ycut^6 + 14*m2^6*Ycut^6 - 283449*MBhat*Ycut^6 - 
             2274519*m2*MBhat*Ycut^6 - 1826730*m2^2*MBhat*Ycut^6 + 
             304290*m2^3*MBhat*Ycut^6 + 33915*m2^4*MBhat*Ycut^6 + 
             549*m2^5*MBhat*Ycut^6 - 168*m2^6*MBhat*Ycut^6 + 
             618333*MBhat^2*Ycut^6 + 3132465*m2*MBhat^2*Ycut^6 + 
             926940*m2^2*MBhat^2*Ycut^6 - 349440*m2^3*MBhat^2*Ycut^6 - 
             5175*m2^4*MBhat^2*Ycut^6 + 4494*m2^5*MBhat^2*Ycut^6 - 
             525*m2^6*MBhat^2*Ycut^6 - 685203*MBhat^3*Ycut^6 - 
             2080239*m2*MBhat^3*Ycut^6 + 110600*m2^2*MBhat^3*Ycut^6 + 
             89440*m2^3*MBhat^3*Ycut^6 - 13005*m2^4*MBhat^3*Ycut^6 + 
             4823*m2^5*MBhat^3*Ycut^6 - 728*m2^6*MBhat^3*Ycut^6 + 
             404445*MBhat^4*Ycut^6 + 665250*m2*MBhat^4*Ycut^6 - 
             172440*m2^2*MBhat^4*Ycut^6 - 12000*m2^3*MBhat^4*Ycut^6 + 
             9555*m2^4*MBhat^4*Ycut^6 - 1575*m2^5*MBhat^4*Ycut^6 - 
             122130*MBhat^5*Ycut^6 - 83250*m2*MBhat^5*Ycut^6 + 
             30720*m2^2*MBhat^5*Ycut^6 + 780*m2^3*MBhat^5*Ycut^6 - 
             840*m2^4*MBhat^5*Ycut^6 + 14510*MBhat^6*Ycut^6 + 
             1080*m2*MBhat^6*Ycut^6 - 1720*m2^2*MBhat^6*Ycut^6 + 
             70*m2^3*MBhat^6*Ycut^6 - 9778*Ycut^7 - 113951*m2*Ycut^7 - 
             211420*m2^2*Ycut^7 - 57120*m2^3*Ycut^7 + 6800*m2^4*Ycut^7 - 
             35*m2^5*Ycut^7 - 2*m2^6*Ycut^7 + 41691*MBhat*Ycut^7 + 
             440757*m2*MBhat*Ycut^7 + 449280*m2^2*MBhat*Ycut^7 + 
             13860*m2^3*MBhat*Ycut^7 - 4635*m2^4*MBhat*Ycut^7 - 
             81*m2^5*MBhat*Ycut^7 + 24*m2^6*MBhat*Ycut^7 - 109827*MBhat^2*
              Ycut^7 - 599955*m2*MBhat^2*Ycut^7 - 329280*m2^2*MBhat^2*
              Ycut^7 + 30840*m2^3*MBhat^2*Ycut^7 + 2745*m2^4*MBhat^2*Ycut^7 - 
             834*m2^5*MBhat^2*Ycut^7 + 75*m2^6*MBhat^2*Ycut^7 + 
             128709*MBhat^3*Ycut^7 + 416177*m2*MBhat^3*Ycut^7 + 
             67900*m2^2*MBhat^3*Ycut^7 - 15220*m2^3*MBhat^3*Ycut^7 + 
             3215*m2^4*MBhat^3*Ycut^7 - 989*m2^5*MBhat^3*Ycut^7 + 
             104*m2^6*MBhat^3*Ycut^7 - 68835*MBhat^4*Ycut^7 - 
             153810*m2*MBhat^4*Ycut^7 + 11160*m2^2*MBhat^4*Ycut^7 + 
             4440*m2^3*MBhat^4*Ycut^7 - 1845*m2^4*MBhat^4*Ycut^7 + 
             225*m2^5*MBhat^4*Ycut^7 + 17010*MBhat^5*Ycut^7 + 
             27810*m2*MBhat^5*Ycut^7 - 5850*m2^2*MBhat^5*Ycut^7 - 
             30*m2^3*MBhat^5*Ycut^7 + 120*m2^4*MBhat^5*Ycut^7 - 
             1130*MBhat^6*Ycut^7 - 2380*m2*MBhat^6*Ycut^7 + 700*m2^2*MBhat^6*
              Ycut^7 - 10*m2^3*MBhat^6*Ycut^7 + 1788*Ycut^8 + 125*m2*Ycut^8 + 
             10590*m2^2*Ycut^8 + 5580*m2^3*Ycut^8 + 550*m2^4*Ycut^8 - 
             48*m2^5*Ycut^8 + 6939*MBhat*Ycut^8 - 26835*m2*MBhat*Ycut^8 - 
             16560*m2^2*MBhat*Ycut^8 - 9180*m2^3*MBhat*Ycut^8 + 
             285*m2^4*MBhat*Ycut^8 - 9*m2^5*MBhat*Ycut^8 + 1317*MBhat^2*
              Ycut^8 + 19440*m2*MBhat^2*Ycut^8 + 24015*m2^2*MBhat^2*Ycut^8 + 
             3780*m2^3*MBhat^2*Ycut^8 - 540*m2^4*MBhat^2*Ycut^8 + 
             48*m2^5*MBhat^2*Ycut^8 - 12075*MBhat^3*Ycut^8 - 
             6695*m2*MBhat^3*Ycut^8 - 13500*m2^2*MBhat^3*Ycut^8 + 
             1260*m2^3*MBhat^3*Ycut^8 - 365*m2^4*MBhat^3*Ycut^8 + 
             75*m2^5*MBhat^3*Ycut^8 + 4185*MBhat^4*Ycut^8 + 
             4125*m2*MBhat^4*Ycut^8 + 3780*m2^2*MBhat^4*Ycut^8 - 
             720*m2^3*MBhat^4*Ycut^8 + 120*m2^4*MBhat^4*Ycut^8 + 
             810*MBhat^5*Ycut^8 - 2430*m2*MBhat^5*Ycut^8 + 510*m2^2*MBhat^5*
              Ycut^8 - 30*m2^3*MBhat^5*Ycut^8 - 780*MBhat^6*Ycut^8 + 
             790*m2*MBhat^6*Ycut^8 - 120*m2^2*MBhat^6*Ycut^8 - 2260*Ycut^9 + 
             4005*m2*Ycut^9 - 850*m2^2*Ycut^9 + 1260*m2^3*Ycut^9 - 
             90*m2^4*Ycut^9 - 4875*MBhat*Ycut^9 + 4755*m2*MBhat*Ycut^9 - 
             4950*m2^2*MBhat*Ycut^9 - 630*m2^3*MBhat*Ycut^9 + 
             4725*MBhat^2*Ycut^9 - 2640*m2*MBhat^2*Ycut^9 + 5535*m2^2*MBhat^2*
              Ycut^9 - 540*m2^3*MBhat^2*Ycut^9 + 1935*MBhat^3*Ycut^9 - 
             2085*m2*MBhat^3*Ycut^9 - 1680*m2^2*MBhat^3*Ycut^9 - 
             1695*MBhat^4*Ycut^9 + 2565*m2*MBhat^4*Ycut^9 - 540*m2^2*MBhat^4*
              Ycut^9 + 450*MBhat^5*Ycut^9 - 630*m2*MBhat^5*Ycut^9 + 
             180*MBhat^6*Ycut^9 - 90*m2*MBhat^6*Ycut^9 + 1804*Ycut^10 - 
             2017*m2*Ycut^10 + 970*m2^2*Ycut^10 + 861*MBhat*Ycut^10 - 
             261*m2*MBhat*Ycut^10 - 1350*m2^2*MBhat*Ycut^10 + 
             90*m2^3*MBhat*Ycut^10 - 2343*MBhat^2*Ycut^10 + 
             1830*m2*MBhat^2*Ycut^10 + 315*m2^2*MBhat^2*Ycut^10 + 
             711*MBhat^3*Ycut^10 - 1821*m2*MBhat^3*Ycut^10 + 
             240*m2^2*MBhat^3*Ycut^10 - 15*MBhat^4*Ycut^10 + 
             315*m2*MBhat^4*Ycut^10 - 270*MBhat^5*Ycut^10 + 
             90*m2*MBhat^5*Ycut^10 - 756*Ycut^11 + 439*m2*Ycut^11 + 
             90*m2^2*Ycut^11 + 369*MBhat*Ycut^11 - 633*m2*MBhat*Ycut^11 + 
             129*MBhat^2*Ycut^11 + 630*m2*MBhat^2*Ycut^11 - 
             45*m2^2*MBhat^2*Ycut^11 - 207*MBhat^3*Ycut^11 - 
             63*m2*MBhat^3*Ycut^11 + 225*MBhat^4*Ycut^11 - 45*m2*MBhat^4*
              Ycut^11 + 130*Ycut^12 + 48*m2*Ycut^12 - 111*MBhat*Ycut^12 - 
             81*m2*MBhat*Ycut^12 + 126*MBhat^2*Ycut^12 - 99*MBhat^3*Ycut^12 + 
             9*m2*MBhat^3*Ycut^12 + 2*Ycut^13 - 24*MBhat*Ycut^13 + 
             18*MBhat^2*Ycut^13))/(-1 + Ycut)^7 - 2*Sqrt[m2]*
          (-22 - 429*m2 - 1230*m2^2 - 740*m2^3 - 60*m2^4 + 3*m2^5 + 
           120*MBhat + 1728*m2*MBhat + 3240*m2^2*MBhat + 960*m2^3*MBhat - 
           270*MBhat^2 - 2757*m2*MBhat^2 - 3117*m2^2*MBhat^2 - 
           342*m2^3*MBhat^2 + 18*m2^4*MBhat^2 + 320*MBhat^3 + 
           2208*m2*MBhat^3 + 1320*m2^2*MBhat^3 - 210*MBhat^4 - 
           927*m2*MBhat^4 - 216*m2^2*MBhat^4 + 18*m2^3*MBhat^4 + 72*MBhat^5 + 
           192*m2*MBhat^5 - 10*MBhat^6 - 15*m2*MBhat^6 + 3*m2^2*MBhat^6)*
          Log[m2] + 2*Sqrt[m2]*(-22 - 429*m2 - 1230*m2^2 - 740*m2^3 - 
           60*m2^4 + 3*m2^5 + 120*MBhat + 1728*m2*MBhat + 3240*m2^2*MBhat + 
           960*m2^3*MBhat - 270*MBhat^2 - 2757*m2*MBhat^2 - 
           3117*m2^2*MBhat^2 - 342*m2^3*MBhat^2 + 18*m2^4*MBhat^2 + 
           320*MBhat^3 + 2208*m2*MBhat^3 + 1320*m2^2*MBhat^3 - 210*MBhat^4 - 
           927*m2*MBhat^4 - 216*m2^2*MBhat^4 + 18*m2^3*MBhat^4 + 72*MBhat^5 + 
           192*m2*MBhat^5 - 10*MBhat^6 - 15*m2*MBhat^6 + 3*m2^2*MBhat^6)*
          Log[1 - Ycut])) + 
     c[VL]*(-1/210*(-11897 + 66843*m2 + 377475*m2^2 - 207025*m2^3 - 
          235375*m2^4 + 9597*m2^5 + 357*m2^6 + 25*m2^7 + 65872*MBhat - 
          329616*m2*MBhat - 807408*m2^2*MBhat + 932400*m2^3*MBhat + 
          148400*m2^4*MBhat - 9072*m2^5*MBhat - 784*m2^6*MBhat + 
          208*m2^7*MBhat - 148946*MBhat^2 + 641900*m2*MBhat^2 + 
          353220*m2^2*MBhat^2 - 894810*m2^3*MBhat^2 + 45290*m2^4*MBhat^2 + 
          6636*m2^5*MBhat^2 - 4060*m2^6*MBhat^2 + 770*m2^7*MBhat^2 + 
          174368*MBhat^3 - 617792*m2*MBhat^3 + 248640*m2^2*MBhat^3 + 
          219520*m2^3*MBhat^3 - 34720*m2^4*MBhat^3 + 17472*m2^5*MBhat^3 - 
          9408*m2^6*MBhat^3 + 1920*m2^7*MBhat^3 - 112035*MBhat^4 + 
          308245*m2*MBhat^4 - 225120*m2^2*MBhat^4 + 19600*m2^3*MBhat^4 + 
          21875*m2^4*MBhat^4 - 16485*m2^5*MBhat^4 + 3920*m2^6*MBhat^4 + 
          37968*MBhat^5 - 74480*m2*MBhat^5 + 47040*m2^2*MBhat^5 - 
          6720*m2^3*MBhat^5 - 6160*m2^4*MBhat^5 + 2352*m2^5*MBhat^5 - 
          5390*MBhat^6 + 6160*m2*MBhat^6 - 1680*m2^2*MBhat^6 + 
          560*m2^3*MBhat^6 + 350*m2^4*MBhat^6 + 98536*Ycut - 531384*m2*Ycut - 
          3265500*m2^2*Ycut + 1267700*m2^3*Ycut + 1813700*m2^4*Ycut - 
          70476*m2^5*Ycut - 2856*m2^6*Ycut - 200*m2^7*Ycut - 
          547136*MBhat*Ycut + 2630208*m2*MBhat*Ycut + 7245504*m2^2*MBhat*
           Ycut - 6820800*m2^3*MBhat*Ycut - 1187200*m2^4*MBhat*Ycut + 
          72576*m2^5*MBhat*Ycut + 6272*m2^6*MBhat*Ycut - 
          1664*m2^7*MBhat*Ycut + 1241968*MBhat^2*Ycut - 5141920*m2*MBhat^2*
           Ycut - 3753120*m2^2*MBhat^2*Ycut + 6877080*m2^3*MBhat^2*Ycut - 
          337960*m2^4*MBhat^2*Ycut - 53088*m2^5*MBhat^2*Ycut + 
          32480*m2^6*MBhat^2*Ycut - 6160*m2^7*MBhat^2*Ycut - 
          1462144*MBhat^3*Ycut + 4969216*m2*MBhat^3*Ycut - 
          1505280*m2^2*MBhat^3*Ycut - 1756160*m2^3*MBhat^3*Ycut + 
          277760*m2^4*MBhat^3*Ycut - 139776*m2^5*MBhat^3*Ycut + 
          75264*m2^6*MBhat^3*Ycut - 15360*m2^7*MBhat^3*Ycut + 
          946680*MBhat^4*Ycut - 2489480*m2*MBhat^4*Ycut + 
          1701420*m2^2*MBhat^4*Ycut - 138740*m2^3*MBhat^4*Ycut - 
          175000*m2^4*MBhat^4*Ycut + 131880*m2^5*MBhat^4*Ycut - 
          31360*m2^6*MBhat^4*Ycut - 323904*MBhat^5*Ycut + 
          602560*m2*MBhat^5*Ycut - 376320*m2^2*MBhat^5*Ycut + 
          53760*m2^3*MBhat^5*Ycut + 49280*m2^4*MBhat^5*Ycut - 
          18816*m2^5*MBhat^5*Ycut + 46480*MBhat^6*Ycut - 
          49280*m2*MBhat^6*Ycut + 15960*m2^2*MBhat^6*Ycut - 
          4480*m2^3*MBhat^6*Ycut - 2800*m2^4*MBhat^6*Ycut - 358316*Ycut^2 + 
          1846404*m2*Ycut^2 + 12412050*m2^2*Ycut^2 - 2882950*m2^3*Ycut^2 - 
          6070750*m2^4*Ycut^2 + 221466*m2^5*Ycut^2 + 9996*m2^6*Ycut^2 + 
          700*m2^7*Ycut^2 + 1995616*MBhat*Ycut^2 - 9178848*m2*MBhat*Ycut^2 - 
          28504224*m2^2*MBhat*Ycut^2 + 21319200*m2^3*MBhat*Ycut^2 + 
          4155200*m2^4*MBhat*Ycut^2 - 254016*m2^5*MBhat*Ycut^2 - 
          21952*m2^6*MBhat*Ycut^2 + 5824*m2^7*MBhat*Ycut^2 - 
          4548488*MBhat^2*Ycut^2 + 18023600*m2*MBhat^2*Ycut^2 + 
          16845360*m2^2*MBhat^2*Ycut^2 - 22944180*m2^3*MBhat^2*Ycut^2 + 
          1085420*m2^4*MBhat^2*Ycut^2 + 185808*m2^5*MBhat^2*Ycut^2 - 
          113680*m2^6*MBhat^2*Ycut^2 + 21560*m2^7*MBhat^2*Ycut^2 + 
          5386304*MBhat^3*Ycut^2 - 17499776*m2*MBhat^3*Ycut^2 + 
          3333120*m2^2*MBhat^3*Ycut^2 + 6146560*m2^3*MBhat^3*Ycut^2 - 
          972160*m2^4*MBhat^3*Ycut^2 + 489216*m2^5*MBhat^3*Ycut^2 - 
          263424*m2^6*MBhat^3*Ycut^2 + 53760*m2^7*MBhat^3*Ycut^2 - 
          3514980*MBhat^4*Ycut^2 + 8807260*m2*MBhat^4*Ycut^2 - 
          5556810*m2^2*MBhat^4*Ycut^2 + 413350*m2^3*MBhat^4*Ycut^2 + 
          612500*m2^4*MBhat^4*Ycut^2 - 461580*m2^5*MBhat^4*Ycut^2 + 
          109760*m2^6*MBhat^4*Ycut^2 + 1214304*MBhat^5*Ycut^2 - 
          2135840*m2*MBhat^5*Ycut^2 + 1317120*m2^2*MBhat^5*Ycut^2 - 
          188160*m2^3*MBhat^5*Ycut^2 - 172480*m2^4*MBhat^5*Ycut^2 + 
          65856*m2^5*MBhat^5*Ycut^2 - 176120*MBhat^6*Ycut^2 + 
          172480*m2*MBhat^6*Ycut^2 - 65940*m2^2*MBhat^6*Ycut^2 + 
          15680*m2^3*MBhat^6*Ycut^2 + 9800*m2^4*MBhat^6*Ycut^2 + 
          747992*Ycut^3 - 3661448*m2*Ycut^3 - 27117300*m2^2*Ycut^3 + 
          2139900*m2^3*Ycut^3 + 11494700*m2^4*Ycut^3 - 384132*m2^5*Ycut^3 - 
          19992*m2^6*Ycut^3 - 1400*m2^7*Ycut^3 - 4179392*MBhat*Ycut^3 + 
          18294976*m2*MBhat*Ycut^3 + 64346688*m2^2*MBhat*Ycut^3 - 
          36680000*m2^3*MBhat*Ycut^3 - 8310400*m2^4*MBhat*Ycut^3 + 
          508032*m2^5*MBhat*Ycut^3 + 43904*m2^6*MBhat*Ycut^3 - 
          11648*m2^7*MBhat*Ycut^3 + 9567376*MBhat^2*Ycut^3 - 
          36109920*m2*MBhat^2*Ycut^3 - 42346080*m2^2*MBhat^2*Ycut^3 + 
          43261960*m2^3*MBhat^2*Ycut^3 - 1943480*m2^4*MBhat^2*Ycut^3 - 
          371616*m2^5*MBhat^2*Ycut^3 + 227360*m2^6*MBhat^2*Ycut^3 - 
          43120*m2^7*MBhat^2*Ycut^3 - 11393508*MBhat^3*Ycut^3 + 
          35235032*m2*MBhat^3*Ycut^3 - 2137100*m2^2*MBhat^3*Ycut^3 - 
          12318320*m2^3*MBhat^3*Ycut^3 + 1999620*m2^4*MBhat^3*Ycut^3 - 
          1027432*m2^5*MBhat^3*Ycut^3 + 541548*m2^6*MBhat^3*Ycut^3 - 
          107520*m2^7*MBhat^3*Ycut^3 + 7486920*MBhat^4*Ycut^3 - 
          17807160*m2*MBhat^4*Ycut^3 + 10157700*m2^2*MBhat^4*Ycut^3 - 
          604380*m2^3*MBhat^4*Ycut^3 - 1292200*m2^4*MBhat^4*Ycut^3 + 
          950040*m2^5*MBhat^4*Ycut^3 - 219520*m2^6*MBhat^4*Ycut^3 - 
          2607108*MBhat^5*Ycut^3 + 4319280*m2*MBhat^5*Ycut^3 - 
          2618280*m2^2*MBhat^5*Ycut^3 + 351120*m2^3*MBhat^5*Ycut^3 + 
          359660*m2^4*MBhat^5*Ycut^3 - 131712*m2^5*MBhat^5*Ycut^3 + 
          381360*MBhat^6*Ycut^3 - 342720*m2*MBhat^6*Ycut^3 + 
          153160*m2^2*MBhat^6*Ycut^3 - 29120*m2^3*MBhat^6*Ycut^3 - 
          19600*m2^4*MBhat^6*Ycut^3 - 982030*Ycut^4 + 4529770*m2*Ycut^4 + 
          37336425*m2^2*Ycut^4 + 2764125*m2^3*Ycut^4 - 13398175*m2^4*Ycut^4 + 
          391965*m2^5*Ycut^4 + 24990*m2^6*Ycut^4 + 1750*m2^7*Ycut^4 + 
          5506480*MBhat*Ycut^4 - 22774640*m2*MBhat*Ycut^4 - 
          91440720*m2^2*MBhat*Ycut^4 + 36912400*m2^3*MBhat*Ycut^4 + 
          10388000*m2^4*MBhat*Ycut^4 - 635040*m2^5*MBhat*Ycut^4 - 
          54880*m2^6*MBhat*Ycut^4 + 14560*m2^7*MBhat*Ycut^4 - 
          12671645*MBhat^2*Ycut^4 + 45247755*m2*MBhat^2*Ycut^4 + 
          65896740*m2^2*MBhat^2*Ycut^4 - 50088500*m2^3*MBhat^2*Ycut^4 + 
          1993285*m2^4*MBhat^2*Ycut^4 + 541695*m2^5*MBhat^2*Ycut^4 - 
          306250*m2^6*MBhat^2*Ycut^4 + 53900*m2^7*MBhat^2*Ycut^4 + 
          15155840*MBhat^3*Ycut^4 - 44356480*m2*MBhat^3*Ycut^4 - 
          4122720*m2^2*MBhat^3*Ycut^4 + 15366400*m2^3*MBhat^3*Ycut^4 - 
          2516640*m2^4*MBhat^3*Ycut^4 + 1337280*m2^5*MBhat^3*Ycut^4 - 
          697760*m2^6*MBhat^3*Ycut^4 + 134400*m2^7*MBhat^3*Ycut^4 - 
          9986865*MBhat^4*Ycut^4 + 22446585*m2*MBhat^4*Ycut^4 - 
          11213160*m2^2*MBhat^4*Ycut^4 + 388080*m2^3*MBhat^4*Ycut^4 + 
          1752800*m2^4*MBhat^4*Ycut^4 - 1236690*m2^5*MBhat^4*Ycut^4 + 
          274400*m2^6*MBhat^4*Ycut^4 + 3480960*MBhat^5*Ycut^4 - 
          5409600*m2*MBhat^5*Ycut^4 + 3213840*m2^2*MBhat^5*Ycut^4 - 
          371280*m2^3*MBhat^5*Ycut^4 - 481600*m2^4*MBhat^5*Ycut^4 + 
          164640*m2^5*MBhat^5*Ycut^4 - 509530*MBhat^6*Ycut^4 + 
          416640*m2*MBhat^6*Ycut^4 - 219660*m2^2*MBhat^6*Ycut^4 + 
          31080*m2^3*MBhat^6*Ycut^4 + 24500*m2^4*MBhat^6*Ycut^4 + 
          832664*Ycut^5 - 3576776*m2*Ycut^5 - 33308940*m2^2*Ycut^5 - 
          7650300*m2^3*Ycut^5 + 9748340*m2^4*Ycut^5 - 225372*m2^5*Ycut^5 - 
          19992*m2^6*Ycut^5 - 1400*m2^7*Ycut^5 - 4683854*MBhat*Ycut^5 + 
          18117232*m2*MBhat*Ycut^5 + 84171066*m2^2*MBhat*Ycut^5 - 
          20624240*m2^3*MBhat*Ycut^5 - 8250970*m2^4*MBhat*Ycut^5 + 
          460992*m2^5*MBhat*Ycut^5 + 57134*m2^6*MBhat*Ycut^5 - 
          11648*m2^7*MBhat*Ycut^5 + 10881640*MBhat^2*Ycut^5 - 
          36374520*m2*MBhat^2*Ycut^5 - 65644320*m2^2*MBhat^2*Ycut^5 + 
          36051400*m2^3*MBhat^2*Ycut^5 - 1109360*m2^4*MBhat^2*Ycut^5 - 
          528024*m2^5*MBhat^2*Ycut^5 + 262640*m2^6*MBhat^2*Ycut^5 - 
          43120*m2^7*MBhat^2*Ycut^5 - 13028596*MBhat^3*Ycut^5 + 
          35757708*m2*MBhat^3*Ycut^5 + 10057320*m2^2*MBhat^3*Ycut^5 - 
          12088160*m2^3*MBhat^3*Ycut^5 + 1902740*m2^4*MBhat^3*Ycut^5 - 
          1107372*m2^5*MBhat^3*Ycut^5 + 582904*m2^6*MBhat^3*Ycut^5 - 
          107520*m2^7*MBhat^3*Ycut^5 + 8490720*MBhat^4*Ycut^5 - 
          17937360*m2*MBhat^4*Ycut^5 + 7432740*m2^2*MBhat^4*Ycut^5 + 
          20580*m2^3*MBhat^4*Ycut^5 - 1532440*m2^4*MBhat^4*Ycut^5 + 
          1040760*m2^5*MBhat^4*Ycut^5 - 219520*m2^6*MBhat^4*Ycut^5 - 
          2890566*MBhat^5*Ycut^5 + 4203360*m2*MBhat^5*Ycut^5 - 
          2469180*m2^2*MBhat^5*Ycut^5 + 220080*m2^3*MBhat^5*Ycut^5 + 
          417410*m2^4*MBhat^5*Ycut^5 - 131712*m2^5*MBhat^5*Ycut^5 + 
          412160*MBhat^6*Ycut^5 - 304640*m2*MBhat^6*Ycut^5 + 
          202440*m2^2*MBhat^6*Ycut^5 - 20160*m2^3*MBhat^6*Ycut^5 - 
          19600*m2^4*MBhat^6*Ycut^5 - 448427*Ycut^6 + 1758743*m2*Ycut^6 + 
          18945220*m2^2*Ycut^6 + 7458500*m2^3*Ycut^6 - 4240845*m2^4*Ycut^6 + 
          64421*m2^5*Ycut^6 + 7056*m2^6*Ycut^6 + 700*m2^7*Ycut^6 + 
          2506784*MBhat*Ycut^6 - 8948576*m2*MBhat*Ycut^6 - 
          49463568*m2^2*MBhat*Ycut^6 + 4427360*m2^3*MBhat*Ycut^6 + 
          4019680*m2^4*MBhat*Ycut^6 - 177408*m2^5*MBhat*Ycut^6 - 
          33712*m2^6*MBhat*Ycut^6 + 5824*m2^7*MBhat*Ycut^6 - 
          6005265*MBhat^2*Ycut^6 + 18430027*m2*MBhat^2*Ycut^6 + 
          41414940*m2^2*MBhat^2*Ycut^6 - 15411200*m2^3*MBhat^2*Ycut^6 + 
          274785*m2^4*MBhat^2*Ycut^6 + 319137*m2^5*MBhat^2*Ycut^6 - 
          145432*m2^6*MBhat^2*Ycut^6 + 21560*m2^7*MBhat^2*Ycut^6 + 
          7161728*MBhat^3*Ycut^6 - 18072544*m2*MBhat^3*Ycut^6 - 
          9494240*m2^2*MBhat^3*Ycut^6 + 5761280*m2^3*MBhat^3*Ycut^6 - 
          805280*m2^4*MBhat^3*Ycut^6 + 583296*m2^5*MBhat^3*Ycut^6 - 
          312032*m2^6*MBhat^3*Ycut^6 + 53760*m2^7*MBhat^3*Ycut^6 - 
          4404134*MBhat^4*Ycut^6 + 8672160*m2*MBhat^4*Ycut^6 - 
          2625420*m2^2*MBhat^4*Ycut^6 - 184660*m2^3*MBhat^4*Ycut^6 + 
          853790*m2^4*MBhat^4*Ycut^6 - 562716*m2^5*MBhat^4*Ycut^6 + 
          109760*m2^6*MBhat^4*Ycut^6 + 1321488*MBhat^5*Ycut^6 - 
          1817760*m2*MBhat^5*Ycut^6 + 1122240*m2^2*MBhat^5*Ycut^6 - 
          60480*m2^3*MBhat^5*Ycut^6 - 231280*m2^4*MBhat^5*Ycut^6 + 
          65856*m2^5*MBhat^5*Ycut^6 - 160720*MBhat^6*Ycut^6 + 
          110880*m2*MBhat^6*Ycut^6 - 121100*m2^2*MBhat^6*Ycut^6 + 
          8400*m2^3*MBhat^6*Ycut^6 + 9800*m2^4*MBhat^6*Ycut^6 + 
          146784*Ycut^7 - 499408*m2*Ycut^7 - 6386100*m2^2*Ycut^7 - 
          3704260*m2^3*Ycut^7 + 959700*m2^4*Ycut^7 - 3948*m2^5*Ycut^7 - 
          1736*m2^6*Ycut^7 - 200*m2^7*Ycut^7 - 728998*MBhat*Ycut^7 + 
          2412256*m2*MBhat*Ycut^7 + 17334828*m2^2*MBhat*Ycut^7 + 
          1243760*m2^3*MBhat*Ycut^7 - 1097390*m2^4*MBhat*Ycut^7 + 
          34272*m2^5*MBhat*Ycut^7 + 12152*m2^6*MBhat*Ycut^7 - 
          1664*m2^7*MBhat*Ycut^7 + 2034480*MBhat^2*Ycut^7 - 
          5504576*m2*MBhat^2*Ycut^7 - 15506400*m2^2*MBhat^2*Ycut^7 + 
          3360280*m2^3*MBhat^2*Ycut^7 + 22680*m2^4*MBhat^2*Ycut^7 - 
          126672*m2^5*MBhat^2*Ycut^7 + 49616*m2^6*MBhat^2*Ycut^7 - 
          6160*m2^7*MBhat^2*Ycut^7 - 2452166*MBhat^3*Ycut^7 + 
          5329184*m2*MBhat^3*Ycut^7 + 4604880*m2^2*MBhat^3*Ycut^7 - 
          1436960*m2^3*MBhat^3*Ycut^7 + 112630*m2^4*MBhat^3*Ycut^7 - 
          172368*m2^5*MBhat^3*Ycut^7 + 100912*m2^6*MBhat^3*Ycut^7 - 
          15360*m2^7*MBhat^3*Ycut^7 + 1172752*MBhat^4*Ycut^7 - 
          2074800*m2*MBhat^4*Ycut^7 + 194460*m2^2*MBhat^4*Ycut^7 + 
          112700*m2^3*MBhat^4*Ycut^7 - 285880*m2^4*MBhat^4*Ycut^7 + 
          184968*m2^5*MBhat^4*Ycut^7 - 31360*m2^6*MBhat^4*Ycut^7 - 
          96768*MBhat^5*Ycut^7 + 181440*m2*MBhat^5*Ycut^7 - 
          226380*m2^2*MBhat^5*Ycut^7 - 5040*m2^3*MBhat^5*Ycut^7 + 
          78680*m2^4*MBhat^5*Ycut^7 - 18816*m2^5*MBhat^5*Ycut^7 - 
          31360*MBhat^6*Ycut^7 + 6720*m2*MBhat^6*Ycut^7 + 
          46200*m2^2*MBhat^6*Ycut^7 - 2240*m2^3*MBhat^6*Ycut^7 - 
          2800*m2^4*MBhat^6*Ycut^7 - 38445*Ycut^8 + 86751*m2*Ycut^8 + 
          1028685*m2^2*Ycut^8 + 866705*m2^3*Ycut^8 - 62230*m2^4*Ycut^8 - 
          5082*m2^5*Ycut^8 + 217*m2^6*Ycut^8 + 25*m2^7*Ycut^8 - 
          2320*MBhat*Ycut^8 - 128912*m2*MBhat*Ycut^8 - 2997456*m2^2*MBhat*
           Ycut^8 - 804160*m2^3*MBhat*Ycut^8 + 135520*m2^4*MBhat*Ycut^8 + 
          672*m2^5*MBhat*Ycut^8 - 2464*m2^6*MBhat*Ycut^8 + 
          208*m2^7*MBhat*Ycut^8 - 363300*MBhat^2*Ycut^8 + 
          824866*m2*MBhat^2*Ycut^8 + 2840880*m2^2*MBhat^2*Ycut^8 - 
          173810*m2^3*MBhat^2*Ycut^8 - 39795*m2^4*MBhat^2*Ycut^8 + 
          28749*m2^5*MBhat^2*Ycut^8 - 9226*m2^6*MBhat^2*Ycut^8 + 
          770*m2^7*MBhat^2*Ycut^8 + 580240*MBhat^3*Ycut^8 - 
          819168*m2*MBhat^3*Ycut^8 - 1024800*m2^2*MBhat^3*Ycut^8 + 
          51520*m2^3*MBhat^3*Ycut^8 + 66640*m2^4*MBhat^3*Ycut^8 + 
          16128*m2^5*MBhat^3*Ycut^8 - 17024*m2^6*MBhat^3*Ycut^8 + 
          1920*m2^7*MBhat^3*Ycut^8 - 49637*MBhat^4*Ycut^8 - 
          37065*m2*MBhat^4*Ycut^8 + 197400*m2^2*MBhat^4*Ycut^8 - 
          26320*m2^3*MBhat^4*Ycut^8 + 43925*m2^4*MBhat^4*Ycut^8 - 
          32193*m2^5*MBhat^4*Ycut^8 + 3920*m2^6*MBhat^4*Ycut^8 - 
          249984*MBhat^5*Ycut^8 + 233520*m2*MBhat^5*Ycut^8 - 
          35280*m2^2*MBhat^5*Ycut^8 + 8400*m2^3*MBhat^5*Ycut^8 - 
          14560*m2^4*MBhat^5*Ycut^8 + 2352*m2^5*MBhat^5*Ycut^8 + 
          72590*MBhat^6*Ycut^8 - 24080*m2*MBhat^6*Ycut^8 - 
          10500*m2^2*MBhat^6*Ycut^8 + 280*m2^3*MBhat^6*Ycut^8 + 
          350*m2^4*MBhat^6*Ycut^8 + 31976*Ycut^9 - 42280*m2*Ycut^9 - 
          14840*m2^2*Ycut^9 - 42840*m2^3*Ycut^9 - 10360*m2^4*Ycut^9 + 
          1624*m2^5*Ycut^9 + 115108*MBhat*Ycut^9 - 143360*m2*MBhat*Ycut^9 + 
          112560*m2^2*MBhat*Ycut^9 + 82320*m2^3*MBhat*Ycut^9 + 
          1330*m2^4*MBhat*Ycut^9 - 1008*m2^5*MBhat*Ycut^9 + 
          210*m2^6*MBhat*Ycut^9 - 29680*MBhat^2*Ycut^9 - 
          20272*m2*MBhat^2*Ycut^9 - 80640*m2^2*MBhat^2*Ycut^9 - 
          39200*m2^3*MBhat^2*Ycut^9 + 9240*m2^4*MBhat^2*Ycut^9 - 
          2520*m2^5*MBhat^2*Ycut^9 + 672*m2^6*MBhat^2*Ycut^9 - 
          168784*MBhat^3*Ycut^9 + 102760*m2*MBhat^3*Ycut^9 + 
          13720*m2^2*MBhat^3*Ycut^9 + 72800*m2^3*MBhat^3*Ycut^9 - 
          36960*m2^4*MBhat^3*Ycut^9 + 4564*m2^5*MBhat^3*Ycut^9 + 
          980*m2^6*MBhat^3*Ycut^9 - 15176*MBhat^4*Ycut^9 + 
          132440*m2*MBhat^4*Ycut^9 - 72240*m2^2*MBhat^4*Ycut^9 - 
          1680*m2^3*MBhat^4*Ycut^9 + 1680*m2^4*MBhat^4*Ycut^9 + 
          2016*m2^5*MBhat^4*Ycut^9 + 145740*MBhat^5*Ycut^9 - 
          128800*m2*MBhat^5*Ycut^9 + 31500*m2^2*MBhat^5*Ycut^9 - 
          1680*m2^3*MBhat^5*Ycut^9 + 1050*m2^4*MBhat^5*Ycut^9 - 
          37520*MBhat^6*Ycut^9 + 8960*m2*MBhat^6*Ycut^9 + 
          1120*m2^2*MBhat^6*Ycut^9 - 35434*Ycut^10 + 37058*m2*Ycut^10 - 
          7350*m2^2*Ycut^10 - 9310*m2^3*Ycut^10 + 1295*m2^4*Ycut^10 - 
          63*m2^5*Ycut^10 - 62048*MBhat*Ycut^10 + 59584*m2*MBhat*Ycut^10 - 
          4368*m2^2*MBhat*Ycut^10 + 13440*m2^3*MBhat*Ycut^10 - 
          2240*m2^4*MBhat*Ycut^10 + 73766*MBhat^2*Ycut^10 - 
          29890*m2*MBhat^2*Ycut^10 - 14700*m2^2*MBhat^2*Ycut^10 - 
          140*m2^3*MBhat^2*Ycut^10 - 105*m2^4*MBhat^2*Ycut^10 - 
          105*m2^5*MBhat^2*Ycut^10 + 49616*MBhat^3*Ycut^10 - 
          31136*m2*MBhat^3*Ycut^10 + 30240*m2^2*MBhat^3*Ycut^10 - 
          20160*m2^3*MBhat^3*Ycut^10 + 6720*m2^4*MBhat^3*Ycut^10 - 
          1008*m2^5*MBhat^3*Ycut^10 - 23240*MBhat^4*Ycut^10 - 
          20860*m2*MBhat^4*Ycut^10 + 9030*m2^2*MBhat^4*Ycut^10 + 
          1470*m2^3*MBhat^4*Ycut^10 - 1050*m2^4*MBhat^4*Ycut^10 - 
          35952*MBhat^5*Ycut^10 + 29120*m2*MBhat^5*Ycut^10 - 
          6720*m2^2*MBhat^5*Ycut^10 + 8680*MBhat^6*Ycut^10 - 
          1120*m2*MBhat^6*Ycut^10 + 25256*Ycut^11 - 18984*m2*Ycut^11 + 
          840*m2^2*Ycut^11 - 280*m2^3*Ycut^11 + 12292*MBhat*Ycut^11 - 
          9632*m2*MBhat*Ycut^11 + 6804*m2^2*MBhat*Ycut^11 - 
          1680*m2^3*MBhat*Ycut^11 + 70*m2^4*MBhat*Ycut^11 - 
          40768*MBhat^2*Ycut^11 + 15008*m2*MBhat^2*Ycut^11 - 
          6720*m2^2*MBhat^2*Ycut^11 + 1120*m2^3*MBhat^2*Ycut^11 + 
          56*MBhat^3*Ycut^11 + 2408*m2*MBhat^3*Ycut^11 - 3780*m2^2*MBhat^3*
           Ycut^11 + 1680*m2^3*MBhat^3*Ycut^11 - 350*m2^4*MBhat^3*Ycut^11 + 
          10024*MBhat^4*Ycut^11 - 280*m2*MBhat^4*Ycut^11 + 
          4116*MBhat^5*Ycut^11 - 2800*m2*MBhat^5*Ycut^11 + 
          420*m2^2*MBhat^5*Ycut^11 - 560*MBhat^6*Ycut^11 - 11228*Ycut^12 + 
          5404*m2*Ycut^12 - 665*m2^2*Ycut^12 + 35*m2^3*Ycut^12 + 
          3472*MBhat*Ycut^12 - 1008*m2*MBhat*Ycut^12 + 336*m2^2*MBhat*
           Ycut^12 + 9443*MBhat^2*Ycut^12 - 2009*m2*MBhat^2*Ycut^12 + 
          840*m2^2*MBhat^2*Ycut^12 - 3248*MBhat^3*Ycut^12 + 
          672*m2*MBhat^3*Ycut^12 - 1127*MBhat^4*Ycut^12 + 
          315*m2*MBhat^4*Ycut^12 - 336*MBhat^5*Ycut^12 - 70*MBhat^6*Ycut^12 + 
          2968*Ycut^13 - 728*m2*Ycut^13 - 2198*MBhat*Ycut^13 + 
          336*m2*MBhat*Ycut^13 - 42*m2^2*MBhat*Ycut^13 - 
          616*MBhat^2*Ycut^13 - 56*m2*MBhat^2*Ycut^13 + 308*MBhat^3*Ycut^13 - 
          84*m2*MBhat^3*Ycut^13 + 112*MBhat^4*Ycut^13 + 42*MBhat^5*Ycut^13 - 
          431*Ycut^14 + 35*m2*Ycut^14 + 352*MBhat*Ycut^14 + 
          35*MBhat^2*Ycut^14 + 7*m2*MBhat^2*Ycut^14 - 16*MBhat^3*Ycut^14 - 
          14*MBhat^4*Ycut^14 + 32*Ycut^15 - 30*MBhat*Ycut^15 + 
          2*MBhat^3*Ycut^15)/(-1 + Ycut)^8 + 
       2*(8 + 8*m2 - 585*m2^2 - 925*m2^3 - 165*m2^4 + 15*m2^5 - 48*MBhat - 
         16*m2*MBhat + 1872*m2^2*MBhat + 1520*m2^3*MBhat + 120*MBhat^2 - 
         16*m2*MBhat^2 - 2208*m2^2*MBhat^2 - 670*m2^3*MBhat^2 + 
         58*m2^4*MBhat^2 - 160*MBhat^3 + 64*m2*MBhat^3 + 1152*m2^2*MBhat^3 + 
         120*MBhat^4 - 56*m2*MBhat^4 - 237*m2^2*MBhat^4 + 43*m2^3*MBhat^4 - 
         48*MBhat^5 + 16*m2*MBhat^5 + 8*MBhat^6 + 6*m2^2*MBhat^6)*Log[m2] - 
       2*(8 + 8*m2 - 585*m2^2 - 925*m2^3 - 165*m2^4 + 15*m2^5 - 48*MBhat - 
         16*m2*MBhat + 1872*m2^2*MBhat + 1520*m2^3*MBhat + 120*MBhat^2 - 
         16*m2*MBhat^2 - 2208*m2^2*MBhat^2 - 670*m2^3*MBhat^2 + 
         58*m2^4*MBhat^2 - 160*MBhat^3 + 64*m2*MBhat^3 + 1152*m2^2*MBhat^3 + 
         120*MBhat^4 - 56*m2*MBhat^4 - 237*m2^2*MBhat^4 + 43*m2^3*MBhat^4 - 
         48*MBhat^5 + 16*m2*MBhat^5 + 8*MBhat^6 + 6*m2^2*MBhat^6)*
        Log[1 - Ycut] + c[VR]*((Sqrt[m2]*(4680 + 38550*m2 + 13200*m2^2 - 
            46800*m2^3 - 10200*m2^4 + 570*m2^5 - 23392*MBhat - 
            120768*m2*MBhat + 45600*m2^2*MBhat + 96000*m2^3*MBhat + 
            2400*m2^4*MBhat + 192*m2^5*MBhat - 32*m2^6*MBhat + 
            47294*MBhat^2 + 131655*m2*MBhat^2 - 127965*m2^2*MBhat^2 - 
            52140*m2^3*MBhat^2 + 510*m2^4*MBhat^2 + 741*m2^5*MBhat^2 - 
            95*m2^6*MBhat^2 - 49008*MBhat^3 - 51456*m2*MBhat^3 + 
            93840*m2^2*MBhat^3 + 7680*m2^3*MBhat^3 - 1680*m2^4*MBhat^3 + 
            768*m2^5*MBhat^3 - 144*m2^6*MBhat^3 + 27120*MBhat^4 - 
            3675*m2*MBhat^4 - 23280*m2^2*MBhat^4 - 1740*m2^3*MBhat^4 + 
            1920*m2^4*MBhat^4 - 345*m2^5*MBhat^4 - 7440*MBhat^5 + 
            6720*m2*MBhat^5 + 960*m2^3*MBhat^5 - 240*m2^4*MBhat^5 + 
            750*MBhat^6 - 1170*m2*MBhat^6 + 450*m2^2*MBhat^6 - 
            30*m2^3*MBhat^6 - 33840*Ycut - 290550*m2*Ycut - 
            150000*m2^2*Ycut + 295200*m2^3*Ycut + 69600*m2^4*Ycut - 
            3810*m2^5*Ycut + 169504*MBhat*Ycut + 926016*m2*MBhat*Ycut - 
            175200*m2^2*MBhat*Ycut - 633600*m2^3*MBhat*Ycut - 
            16800*m2^4*MBhat*Ycut - 1344*m2^5*MBhat*Ycut + 
            224*m2^6*MBhat*Ycut - 343658*MBhat^2*Ycut - 1044525*m2*MBhat^2*
             Ycut + 767055*m2^2*MBhat^2*Ycut + 352620*m2^3*MBhat^2*Ycut - 
            2490*m2^4*MBhat^2*Ycut - 5187*m2^5*MBhat^2*Ycut + 
            665*m2^6*MBhat^2*Ycut + 357456*MBhat^3*Ycut + 452352*m2*MBhat^3*
             Ycut - 607920*m2^2*MBhat^3*Ycut - 53760*m2^3*MBhat^3*Ycut + 
            11760*m2^4*MBhat^3*Ycut - 5376*m2^5*MBhat^3*Ycut + 
            1008*m2^6*MBhat^3*Ycut - 198840*MBhat^4*Ycut - 
            9015*m2*MBhat^4*Ycut + 156120*m2^2*MBhat^4*Ycut + 
            13260*m2^3*MBhat^4*Ycut - 13440*m2^4*MBhat^4*Ycut + 
            2415*m2^5*MBhat^4*Ycut + 54960*MBhat^5*Ycut - 41280*m2*MBhat^5*
             Ycut - 6720*m2^3*MBhat^5*Ycut + 1680*m2^4*MBhat^5*Ycut - 
            5610*MBhat^6*Ycut + 8010*m2*MBhat^6*Ycut - 2970*m2^2*MBhat^6*
             Ycut + 210*m2^3*MBhat^6*Ycut + 105300*Ycut^2 + 
            944100*m2*Ycut^2 + 651600*m2^2*Ycut^2 - 772200*m2^3*Ycut^2 - 
            202500*m2^4*Ycut^2 + 10800*m2^5*Ycut^2 - 528672*MBhat*Ycut^2 - 
            3060288*m2*MBhat*Ycut^2 + 21600*m2^2*MBhat*Ycut^2 + 
            1766400*m2^3*MBhat*Ycut^2 + 50400*m2^4*MBhat*Ycut^2 + 
            4032*m2^5*MBhat*Ycut^2 - 672*m2^6*MBhat*Ycut^2 + 
            1075074*MBhat^2*Ycut^2 + 3563865*m2*MBhat^2*Ycut^2 - 
            1850715*m2^2*MBhat^2*Ycut^2 - 1014600*m2^3*MBhat^2*Ycut^2 + 
            3690*m2^4*MBhat^2*Ycut^2 + 15561*m2^5*MBhat^2*Ycut^2 - 
            1995*m2^6*MBhat^2*Ycut^2 - 1122768*MBhat^3*Ycut^2 - 
            1679616*m2*MBhat^3*Ycut^2 + 1652400*m2^2*MBhat^3*Ycut^2 + 
            161280*m2^3*MBhat^3*Ycut^2 - 35280*m2^4*MBhat^3*Ycut^2 + 
            16128*m2^5*MBhat^3*Ycut^2 - 3024*m2^6*MBhat^3*Ycut^2 + 
            628020*MBhat^4*Ycut^2 + 148635*m2*MBhat^4*Ycut^2 - 
            444420*m2^2*MBhat^4*Ycut^2 - 43560*m2^3*MBhat^4*Ycut^2 + 
            40320*m2^4*MBhat^4*Ycut^2 - 7245*m2^5*MBhat^4*Ycut^2 - 
            174960*MBhat^5*Ycut^2 + 103680*m2*MBhat^5*Ycut^2 + 
            20160*m2^3*MBhat^5*Ycut^2 - 5040*m2^4*MBhat^5*Ycut^2 + 
            18090*MBhat^6*Ycut^2 - 23400*m2*MBhat^6*Ycut^2 + 
            8280*m2^2*MBhat^6*Ycut^2 - 630*m2^3*MBhat^6*Ycut^2 - 
            183060*Ycut^3 - 1718400*m2*Ycut^3 - 1489200*m2^2*Ycut^3 + 
            1060200*m2^3*Ycut^3 + 324900*m2^4*Ycut^3 - 16740*m2^5*Ycut^3 + 
            921440*MBhat*Ycut^3 + 5664960*m2*MBhat*Ycut^3 + 
            972000*m2^2*MBhat*Ycut^3 - 2675200*m2^3*MBhat*Ycut^3 - 
            84000*m2^4*MBhat*Ycut^3 - 6720*m2^5*MBhat*Ycut^3 + 
            1120*m2^6*MBhat*Ycut^3 - 1879990*MBhat^2*Ycut^3 - 
            6800355*m2*MBhat^2*Ycut^3 + 2183625*m2^2*MBhat^2*Ycut^3 + 
            1604480*m2^3*MBhat^2*Ycut^3 + 1410*m2^4*MBhat^2*Ycut^3 - 
            25935*m2^5*MBhat^2*Ycut^3 + 3325*m2^6*MBhat^2*Ycut^3 + 
            1971590*MBhat^3*Ycut^3 + 3445530*m2*MBhat^3*Ycut^3 - 
            2410580*m2^2*MBhat^3*Ycut^3 - 272300*m2^3*MBhat^3*Ycut^3 + 
            61950*m2^4*MBhat^3*Ycut^3 - 27790*m2^5*MBhat^3*Ycut^3 + 
            5040*m2^6*MBhat^3*Ycut^3 - 1108860*MBhat^4*Ycut^3 - 
            491865*m2*MBhat^4*Ycut^3 + 690660*m2^2*MBhat^4*Ycut^3 + 
            84000*m2^3*MBhat^4*Ycut^3 - 68760*m2^4*MBhat^4*Ycut^3 + 
            12075*m2^5*MBhat^4*Ycut^3 + 311310*MBhat^5*Ycut^3 - 
            132330*m2*MBhat^5*Ycut^3 + 1050*m2^2*MBhat^5*Ycut^3 - 
            34350*m2^3*MBhat^5*Ycut^3 + 8400*m2^4*MBhat^5*Ycut^3 - 
            32590*MBhat^6*Ycut^3 + 37740*m2*MBhat^6*Ycut^3 - 
            12620*m2^2*MBhat^6*Ycut^3 + 1050*m2^3*MBhat^6*Ycut^3 + 
            192510*Ycut^4 + 1899525*m2*Ycut^4 + 1993200*m2^2*Ycut^4 - 
            776700*m2^3*Ycut^4 - 309150*m2^4*Ycut^4 + 15165*m2^5*Ycut^4 - 
            971840*MBhat*Ycut^4 - 6370560*m2*MBhat*Ycut^4 - 
            2232000*m2^2*MBhat*Ycut^4 + 2339200*m2^3*MBhat*Ycut^4 + 
            84000*m2^4*MBhat*Ycut^4 + 6720*m2^5*MBhat*Ycut^4 - 
            1120*m2^6*MBhat*Ycut^4 + 1990825*MBhat^2*Ycut^4 + 
            7875105*m2*MBhat^2*Ycut^4 - 1059450*m2^2*MBhat^2*Ycut^4 - 
            1490480*m2^3*MBhat^2*Ycut^4 - 15735*m2^4*MBhat^2*Ycut^4 + 
            27300*m2^5*MBhat^2*Ycut^4 - 3325*m2^6*MBhat^2*Ycut^4 - 
            2095610*MBhat^3*Ycut^4 - 4258070*m2*MBhat^3*Ycut^4 + 
            1987580*m2^2*MBhat^3*Ycut^4 + 273140*m2^3*MBhat^3*Ycut^4 - 
            65170*m2^4*MBhat^3*Ycut^4 + 28930*m2^5*MBhat^3*Ycut^4 - 
            5040*m2^6*MBhat^3*Ycut^4 + 1183005*MBhat^4*Ycut^4 + 
            803430*m2*MBhat^4*Ycut^4 - 629640*m2^2*MBhat^4*Ycut^4 - 
            100140*m2^3*MBhat^4*Ycut^4 + 71295*m2^4*MBhat^4*Ycut^4 - 
            12075*m2^5*MBhat^4*Ycut^4 - 333810*MBhat^5*Ycut^4 + 
            79590*m2*MBhat^5*Ycut^4 - 2790*m2^2*MBhat^5*Ycut^4 + 
            35730*m2^3*MBhat^5*Ycut^4 - 8400*m2^4*MBhat^5*Ycut^4 + 
            35230*MBhat^6*Ycut^4 - 35900*m2*MBhat^6*Ycut^4 + 
            11150*m2^2*MBhat^6*Ycut^4 - 1050*m2^3*MBhat^6*Ycut^4 - 
            123066*Ycut^5 - 1284615*m2*Ycut^5 - 1599120*m2^2*Ycut^5 + 
            239220*m2^3*Ycut^5 + 172890*m2^4*Ycut^5 - 7839*m2^5*Ycut^5 + 
            623103*MBhat*Ycut^5 + 4387281*m2*MBhat*Ycut^5 + 
            2348550*m2^2*MBhat*Ycut^5 - 1138350*m2^3*MBhat*Ycut^5 - 
            47445*m2^4*MBhat*Ycut^5 - 4851*m2^5*MBhat*Ycut^5 + 
            672*m2^6*MBhat*Ycut^5 - 1285779*MBhat^2*Ycut^5 - 
            5579115*m2*MBhat^2*Ycut^5 - 265350*m2^2*MBhat^2*Ycut^5 + 
            799800*m2^3*MBhat^2*Ycut^5 + 22605*m2^4*MBhat^2*Ycut^5 - 
            17340*m2^5*MBhat^2*Ycut^5 + 1995*m2^6*MBhat^2*Ycut^5 + 
            1355625*MBhat^3*Ycut^5 + 3212537*m2*MBhat^3*Ycut^5 - 
            868080*m2^2*MBhat^3*Ycut^5 - 156000*m2^3*MBhat^3*Ycut^5 + 
            40495*m2^4*MBhat^3*Ycut^5 - 18417*m2^5*MBhat^3*Ycut^5 + 
            3024*m2^6*MBhat^3*Ycut^5 - 761475*MBhat^4*Ycut^5 - 
            747030*m2*MBhat^4*Ycut^5 + 339120*m2^2*MBhat^4*Ycut^5 + 
            70860*m2^3*MBhat^4*Ycut^5 - 44745*m2^4*MBhat^4*Ycut^5 + 
            7245*m2^5*MBhat^4*Ycut^5 + 213030*MBhat^5*Ycut^5 + 
            990*m2*MBhat^5*Ycut^5 + 840*m2^2*MBhat^5*Ycut^5 - 
            22260*m2^3*MBhat^5*Ycut^5 + 5040*m2^4*MBhat^5*Ycut^5 - 
            22170*MBhat^6*Ycut^5 + 19160*m2*MBhat^6*Ycut^5 - 
            5310*m2^2*MBhat^6*Ycut^5 + 630*m2^3*MBhat^6*Ycut^5 + 
            44870*Ycut^6 + 500565*m2*Ycut^6 + 734320*m2^2*Ycut^6 + 
            34480*m2^3*Ycut^6 - 51990*m2^4*Ycut^6 + 2165*m2^5*Ycut^6 - 
            225957*MBhat*Ycut^6 - 1748007*m2*MBhat*Ycut^6 - 
            1288530*m2^2*MBhat*Ycut^6 + 251090*m2^3*MBhat*Ycut^6 + 
            12615*m2^4*MBhat*Ycut^6 + 1893*m2^5*MBhat*Ycut^6 - 
            224*m2^6*MBhat*Ycut^6 + 479505*MBhat^2*Ycut^6 + 
            2273865*m2*MBhat^2*Ycut^6 + 549840*m2^2*MBhat^2*Ycut^6 - 
            223600*m2^3*MBhat^2*Ycut^6 - 13155*m2^4*MBhat^2*Ycut^6 + 
            6258*m2^5*MBhat^2*Ycut^6 - 665*m2^6*MBhat^2*Ycut^6 - 
            503455*MBhat^3*Ycut^6 - 1403439*m2*MBhat^3*Ycut^6 + 
            138320*m2^2*MBhat^3*Ycut^6 + 42960*m2^3*MBhat^3*Ycut^6 - 
            14265*m2^4*MBhat^3*Ycut^6 + 6839*m2^5*MBhat^3*Ycut^6 - 
            1008*m2^6*MBhat^3*Ycut^6 + 269625*MBhat^4*Ycut^6 + 
            402810*m2*MBhat^4*Ycut^6 - 107820*m2^2*MBhat^4*Ycut^6 - 
            26280*m2^3*MBhat^4*Ycut^6 + 16065*m2^4*MBhat^4*Ycut^6 - 
            2415*m2^5*MBhat^4*Ycut^6 - 69210*MBhat^5*Ycut^6 - 
            35010*m2*MBhat^5*Ycut^6 + 4080*m2^2*MBhat^5*Ycut^6 + 
            7500*m2^3*MBhat^5*Ycut^6 - 1680*m2^4*MBhat^5*Ycut^6 + 
            6110*MBhat^6*Ycut^6 - 3720*m2*MBhat^6*Ycut^6 + 800*m2^2*MBhat^6*
             Ycut^6 - 210*m2^3*MBhat^6*Ycut^6 - 7922*Ycut^7 - 
            91535*m2*Ycut^7 - 161920*m2^2*Ycut^7 - 38800*m2^3*Ycut^7 + 
            6290*m2^4*Ycut^7 - 143*m2^5*Ycut^7 + 33135*MBhat*Ycut^7 + 
            339381*m2*MBhat*Ycut^7 + 325080*m2^2*MBhat*Ycut^7 + 
            1060*m2^3*MBhat*Ycut^7 - 1455*m2^4*MBhat*Ycut^7 - 
            273*m2^5*MBhat*Ycut^7 + 32*m2^6*MBhat*Ycut^7 - 
            88623*MBhat^2*Ycut^7 - 428715*m2*MBhat^2*Ycut^7 - 
            223260*m2^2*MBhat^2*Ycut^7 + 23800*m2^3*MBhat^2*Ycut^7 + 
            3405*m2^4*MBhat^2*Ycut^7 - 1086*m2^5*MBhat^2*Ycut^7 + 
            95*m2^6*MBhat^2*Ycut^7 + 93625*MBhat^3*Ycut^7 + 
            291857*m2*MBhat^3*Ycut^7 + 19300*m2^2*MBhat^3*Ycut^7 - 
            1140*m2^3*MBhat^3*Ycut^7 + 1955*m2^4*MBhat^3*Ycut^7 - 
            1277*m2^5*MBhat^3*Ycut^7 + 144*m2^6*MBhat^3*Ycut^7 - 
            34695*MBhat^4*Ycut^7 - 119610*m2*MBhat^4*Ycut^7 + 
            25020*m2^2*MBhat^4*Ycut^7 + 2640*m2^3*MBhat^4*Ycut^7 - 
            2775*m2^4*MBhat^4*Ycut^7 + 345*m2^5*MBhat^4*Ycut^7 + 
            810*MBhat^5*Ycut^7 + 24930*m2*MBhat^5*Ycut^7 - 5130*m2^2*MBhat^5*
             Ycut^7 - 990*m2^3*MBhat^5*Ycut^7 + 240*m2^4*MBhat^5*Ycut^7 + 
            1510*MBhat^6*Ycut^7 - 1660*m2*MBhat^6*Ycut^7 + 340*m2^2*MBhat^6*
             Ycut^7 + 30*m2^3*MBhat^6*Ycut^7 + 1338*Ycut^8 + 665*m2*Ycut^8 + 
            7440*m2^2*Ycut^8 + 4500*m2^3*Ycut^8 + 280*m2^4*Ycut^8 - 
            48*m2^5*Ycut^8 + 6279*MBhat*Ycut^8 - 22275*m2*MBhat*Ycut^8 - 
            11160*m2^2*MBhat*Ycut^8 - 6780*m2^3*MBhat*Ycut^8 + 
            345*m2^4*MBhat*Ycut^8 - 9*m2^5*MBhat*Ycut^8 + 
            4857*MBhat^2*Ycut^8 + 4980*m2*MBhat^2*Ycut^8 + 24375*m2^2*MBhat^2*
             Ycut^8 - 60*m2^3*MBhat^2*Ycut^8 - 150*m2^4*MBhat^2*Ycut^8 + 
            48*m2^5*MBhat^2*Ycut^8 - 11055*MBhat^3*Ycut^8 - 
            5255*m2*MBhat^3*Ycut^8 - 3420*m2^2*MBhat^3*Ycut^8 - 
            2580*m2^3*MBhat^3*Ycut^8 + 355*m2^4*MBhat^3*Ycut^8 + 
            75*m2^5*MBhat^3*Ycut^8 - 4665*MBhat^4*Ycut^8 + 18165*m2*MBhat^4*
             Ycut^8 - 7560*m2^2*MBhat^4*Ycut^8 + 1320*m2^3*MBhat^4*Ycut^8 + 
            120*m2^4*MBhat^4*Ycut^8 + 7290*MBhat^5*Ycut^8 - 
            8910*m2*MBhat^5*Ycut^8 + 2310*m2^2*MBhat^5*Ycut^8 - 
            30*m2^3*MBhat^5*Ycut^8 - 1860*MBhat^6*Ycut^8 + 
            1150*m2*MBhat^6*Ycut^8 - 120*m2^2*MBhat^6*Ycut^8 - 1750*Ycut^9 + 
            2865*m2*Ycut^9 - 400*m2^2*Ycut^9 + 980*m2^3*Ycut^9 - 
            120*m2^4*Ycut^9 - 5135*MBhat*Ycut^9 + 6195*m2*MBhat*Ycut^9 - 
            5550*m2^2*MBhat*Ycut^9 + 170*m2^3*MBhat*Ycut^9 - 
            60*m2^4*MBhat*Ycut^9 + 1945*MBhat^2*Ycut^9 + 1620*m2*MBhat^2*
             Ycut^9 + 1575*m2^2*MBhat^2*Ycut^9 + 260*m2^3*MBhat^2*Ycut^9 - 
            90*m2^4*MBhat^2*Ycut^9 + 4475*MBhat^3*Ycut^9 - 
            4005*m2*MBhat^3*Ycut^9 - 1680*m2^2*MBhat^3*Ycut^9 + 
            800*m2^3*MBhat^3*Ycut^9 - 120*m2^4*MBhat^3*Ycut^9 + 
            495*MBhat^4*Ycut^9 - 2355*m2*MBhat^4*Ycut^9 + 2040*m2^2*MBhat^4*
             Ycut^9 - 360*m2^3*MBhat^4*Ycut^9 - 2190*MBhat^5*Ycut^9 + 
            1770*m2*MBhat^5*Ycut^9 - 360*m2^2*MBhat^5*Ycut^9 + 
            620*MBhat^6*Ycut^9 - 210*m2*MBhat^6*Ycut^9 + 1486*Ycut^10 - 
            1645*m2*Ycut^10 + 880*m2^2*Ycut^10 - 80*m2^3*Ycut^10 + 
            1649*MBhat*Ycut^10 - 1653*m2*MBhat*Ycut^10 - 390*m2^2*MBhat*
             Ycut^10 + 10*m2^3*MBhat*Ycut^10 - 1975*MBhat^2*Ycut^10 + 
            1410*m2*MBhat^2*Ycut^10 + 315*m2^2*MBhat^2*Ycut^10 - 
            80*m2^3*MBhat^2*Ycut^10 - 797*MBhat^3*Ycut^10 - 
            381*m2*MBhat^3*Ycut^10 + 240*m2^2*MBhat^3*Ycut^10 - 
            80*m2^3*MBhat^3*Ycut^10 + 255*MBhat^4*Ycut^10 + 
            555*m2*MBhat^4*Ycut^10 - 240*m2^2*MBhat^4*Ycut^10 + 
            210*MBhat^5*Ycut^10 - 150*m2*MBhat^5*Ycut^10 - 
            80*MBhat^6*Ycut^10 - 690*Ycut^11 + 475*m2*Ycut^11 - 
            27*MBhat*Ycut^11 - 249*m2*MBhat*Ycut^11 + 513*MBhat^2*Ycut^11 + 
            210*m2*MBhat^2*Ycut^11 - 45*m2^2*MBhat^2*Ycut^11 - 
            51*MBhat^3*Ycut^11 - 63*m2*MBhat^3*Ycut^11 + 15*MBhat^4*Ycut^11 - 
            45*m2*MBhat^4*Ycut^11 + 152*Ycut^12 - 83*MBhat*Ycut^12 - 
            33*m2*MBhat*Ycut^12 + 4*MBhat^2*Ycut^12 - 27*MBhat^3*Ycut^12 + 
            9*m2*MBhat^3*Ycut^12 - 8*Ycut^13 - 4*MBhat*Ycut^13 + 
            8*MBhat^2*Ycut^13))/(15*(-1 + Ycut)^7) + 
         4*Sqrt[m2]*(-18 - 345*m2 - 960*m2^2 - 540*m2^3 - 30*m2^4 + 3*m2^5 + 
           96*MBhat + 1344*m2*MBhat + 2400*m2^2*MBhat + 640*m2^3*MBhat - 
           210*MBhat^2 - 2049*m2*MBhat^2 - 2145*m2^2*MBhat^2 - 
           206*m2^3*MBhat^2 + 18*m2^4*MBhat^2 + 240*MBhat^3 + 
           1536*m2*MBhat^3 + 816*m2^2*MBhat^3 - 150*MBhat^4 - 
           579*m2*MBhat^4 - 114*m2^2*MBhat^4 + 18*m2^3*MBhat^4 + 48*MBhat^5 + 
           96*m2*MBhat^5 - 6*MBhat^6 - 3*m2*MBhat^6 + 3*m2^2*MBhat^6)*
          Log[m2] - 4*Sqrt[m2]*(-18 - 345*m2 - 960*m2^2 - 540*m2^3 - 
           30*m2^4 + 3*m2^5 + 96*MBhat + 1344*m2*MBhat + 2400*m2^2*MBhat + 
           640*m2^3*MBhat - 210*MBhat^2 - 2049*m2*MBhat^2 - 
           2145*m2^2*MBhat^2 - 206*m2^3*MBhat^2 + 18*m2^4*MBhat^2 + 
           240*MBhat^3 + 1536*m2*MBhat^3 + 816*m2^2*MBhat^3 - 150*MBhat^4 - 
           579*m2*MBhat^4 - 114*m2^2*MBhat^4 + 18*m2^3*MBhat^4 + 48*MBhat^5 + 
           96*m2*MBhat^5 - 6*MBhat^6 - 3*m2*MBhat^6 + 3*m2^2*MBhat^6)*
          Log[1 - Ycut]))) + 
   rhoLS*(-1/420*((-1 + m2 + Ycut)*(-345 - 12378*m2 + 16497*m2^2 + 
         49572*m2^3 - 6603*m2^4 - 1458*m2^5 + 75*m2^6 + 2160*MBhat + 
         36432*m2*MBhat - 138456*m2^2*MBhat - 75456*m2^3*MBhat + 
         16944*m2^4*MBhat - 3216*m2^5*MBhat + 312*m2^6*MBhat - 5810*MBhat^2 - 
         24150*m2*MBhat^2 + 233730*m2^2*MBhat^2 - 30660*m2^3*MBhat^2 + 
         7350*m2^4*MBhat^2 - 4830*m2^5*MBhat^2 + 770*m2^6*MBhat^2 + 
         8832*MBhat^3 - 23760*m2*MBhat^3 - 114480*m2^2*MBhat^3 + 
         30000*m2^3*MBhat^3 + 3120*m2^4*MBhat^3 - 5952*m2^5*MBhat^3 + 
         1440*m2^6*MBhat^3 - 7735*MBhat^4 + 36890*m2*MBhat^4 - 
         15190*m2^2*MBhat^4 + 20090*m2^3*MBhat^4 - 16975*m2^4*MBhat^4 + 
         3920*m2^5*MBhat^4 + 3528*MBhat^5 - 14112*m2*MBhat^5 + 
         21168*m2^2*MBhat^5 - 14112*m2^3*MBhat^5 + 3528*m2^4*MBhat^5 - 
         630*MBhat^6 + 1050*m2*MBhat^6 - 3990*m2^2*MBhat^6 + 
         1050*m2^3*MBhat^6 + 2070*Ycut + 78963*m2*Ycut - 100365*m2^2*Ycut - 
         326418*m2^3*Ycut + 41304*m2^4*Ycut + 10131*m2^5*Ycut - 
         525*m2^6*Ycut - 12960*MBhat*Ycut - 236592*m2*MBhat*Ycut + 
         879408*m2^2*MBhat*Ycut + 514152*m2^3*MBhat*Ycut - 
         115704*m2^4*MBhat*Ycut + 22200*m2^5*MBhat*Ycut - 
         2184*m2^6*MBhat*Ycut + 34860*MBhat^2*Ycut + 169330*m2*MBhat^2*Ycut - 
         1518020*m2^2*MBhat^2*Ycut + 186970*m2^3*MBhat^2*Ycut - 
         47390*m2^4*MBhat^2*Ycut + 33040*m2^5*MBhat^2*Ycut - 
         5390*m2^6*MBhat^2*Ycut - 52992*MBhat^3*Ycut + 131232*m2*MBhat^3*
          Ycut + 772752*m2^2*MBhat^3*Ycut - 208608*m2^3*MBhat^3*Ycut - 
         17328*m2^4*MBhat^3*Ycut + 40224*m2^5*MBhat^3*Ycut - 
         10080*m2^6*MBhat^3*Ycut + 46410*MBhat^4*Ycut - 
         224035*m2*MBhat^4*Ycut + 81235*m2^2*MBhat^4*Ycut - 
         127575*m2^3*MBhat^4*Ycut + 114905*m2^4*MBhat^4*Ycut - 
         27440*m2^5*MBhat^4*Ycut - 21168*MBhat^5*Ycut + 
         88200*m2*MBhat^5*Ycut - 137592*m2^2*MBhat^5*Ycut + 
         95256*m2^3*MBhat^5*Ycut - 24696*m2^4*MBhat^5*Ycut + 
         3780*MBhat^6*Ycut - 6930*m2*MBhat^6*Ycut + 26880*m2^2*MBhat^6*Ycut - 
         7350*m2^3*MBhat^6*Ycut - 5175*Ycut^2 - 211665*m2*Ycut^2 + 
         253395*m2^2*Ycut^2 + 908202*m2^3*Ycut^2 - 107319*m2^4*Ycut^2 - 
         30093*m2^5*Ycut^2 + 1575*m2^6*Ycut^2 + 32400*MBhat*Ycut^2 + 
         646560*m2*MBhat*Ycut^2 - 2343240*m2^2*MBhat*Ycut^2 - 
         1488888*m2^3*MBhat*Ycut^2 + 335808*m2^4*MBhat*Ycut^2 - 
         65352*m2^5*MBhat*Ycut^2 + 6552*m2^6*MBhat*Ycut^2 - 
         87150*MBhat^2*Ycut^2 - 499520*m2*MBhat^2*Ycut^2 + 
         4151420*m2^2*MBhat^2*Ycut^2 - 465780*m2^3*MBhat^2*Ycut^2 + 
         126700*m2^4*MBhat^2*Ycut^2 - 96040*m2^5*MBhat^2*Ycut^2 + 
         16170*m2^6*MBhat^2*Ycut^2 + 132480*MBhat^3*Ycut^2 - 
         289680*m2*MBhat^3*Ycut^2 - 2208288*m2^2*MBhat^3*Ycut^2 + 
         617184*m2^3*MBhat^3*Ycut^2 + 35376*m2^4*MBhat^3*Ycut^2 - 
         114912*m2^5*MBhat^3*Ycut^2 + 30240*m2^6*MBhat^3*Ycut^2 - 
         116025*MBhat^4*Ycut^2 + 564305*m2*MBhat^4*Ycut^2 - 
         161490*m2^2*MBhat^4*Ycut^2 + 334425*m2^3*MBhat^4*Ycut^2 - 
         329035*m2^4*MBhat^4*Ycut^2 + 82320*m2^5*MBhat^4*Ycut^2 + 
         52920*MBhat^5*Ycut^2 - 229320*m2*MBhat^5*Ycut^2 + 
         373968*m2^2*MBhat^5*Ycut^2 - 271656*m2^3*MBhat^5*Ycut^2 + 
         74088*m2^4*MBhat^5*Ycut^2 - 9450*MBhat^6*Ycut^2 + 
         18900*m2*MBhat^6*Ycut^2 - 76440*m2^2*MBhat^6*Ycut^2 + 
         22050*m2^3*MBhat^6*Ycut^2 + 6900*Ycut^3 + 306270*m2*Ycut^3 - 
         338610*m2^2*Ycut^3 - 1374483*m2^3*Ycut^3 + 147273*m2^4*Ycut^3 + 
         49455*m2^5*Ycut^3 - 2625*m2^6*Ycut^3 - 43200*MBhat*Ycut^3 - 
         955680*m2*MBhat*Ycut^3 + 3361440*m2^2*MBhat*Ycut^3 + 
         2363952*m2^3*MBhat*Ycut^3 - 534240*m2^4*MBhat*Ycut^3 + 
         106008*m2^5*MBhat*Ycut^3 - 10920*m2^6*MBhat*Ycut^3 + 
         116200*MBhat^2*Ycut^3 + 797860*m2*MBhat^2*Ycut^3 - 
         6143760*m2^2*MBhat^2*Ycut^3 + 591850*m2^3*MBhat^2*Ycut^3 - 
         177380*m2^4*MBhat^2*Ycut^3 + 152880*m2^5*MBhat^2*Ycut^3 - 
         26950*m2^6*MBhat^2*Ycut^3 - 178740*MBhat^3*Ycut^3 + 
         315300*m2*MBhat^3*Ycut^3 + 3441432*m2^2*MBhat^3*Ycut^3 - 
         989784*m2^3*MBhat^3*Ycut^3 - 45108*m2^4*MBhat^3*Ycut^3 + 
         186900*m2^5*MBhat^3*Ycut^3 - 50400*m2^6*MBhat^3*Ycut^3 + 
         158060*MBhat^4*Ycut^3 - 749630*m2*MBhat^4*Ycut^3 + 
         125230*m2^2*MBhat^4*Ycut^3 - 486675*m2^3*MBhat^4*Ycut^3 + 
         531965*m2^4*MBhat^4*Ycut^3 - 137200*m2^5*MBhat^4*Ycut^3 - 
         71820*MBhat^5*Ycut^3 + 314580*m2*MBhat^5*Ycut^3 - 
         553812*m2^2*MBhat^5*Ycut^3 + 434532*m2^3*MBhat^5*Ycut^3 - 
         123480*m2^4*MBhat^5*Ycut^3 + 12600*MBhat^6*Ycut^3 - 
         27300*m2*MBhat^6*Ycut^3 + 120960*m2^2*MBhat^6*Ycut^3 - 
         36750*m2^3*MBhat^6*Ycut^3 - 5175*Ycut^4 - 254040*m2*Ycut^4 + 
         250500*m2^2*Ycut^4 + 1205967*m2^3*Ycut^4 - 110460*m2^4*Ycut^4 - 
         48405*m2^5*Ycut^4 + 2625*m2^6*Ycut^4 + 32400*MBhat*Ycut^4 + 
         812160*m2*MBhat*Ycut^4 - 2751360*m2^2*MBhat*Ycut^4 - 
         2201808*m2^3*MBhat*Ycut^4 + 497952*m2^4*MBhat*Ycut^4 - 
         101640*m2^5*MBhat*Ycut^4 + 10920*m2^6*MBhat*Ycut^4 - 
         84525*MBhat^2*Ycut^4 - 737800*m2*MBhat^2*Ycut^4 + 
         5234180*m2^2*MBhat^2*Ycut^4 - 400960*m2^3*MBhat^2*Ycut^4 + 
         165865*m2^4*MBhat^2*Ycut^4 - 155330*m2^5*MBhat^2*Ycut^4 + 
         26950*m2^6*MBhat^2*Ycut^4 + 141160*MBhat^3*Ycut^4 - 
         160940*m2*MBhat^3*Ycut^4 - 3148448*m2^2*MBhat^3*Ycut^4 + 
         978488*m2^3*MBhat^3*Ycut^4 + 6440*m2^4*MBhat^3*Ycut^4 - 
         180460*m2^5*MBhat^3*Ycut^4 + 50400*m2^6*MBhat^3*Ycut^4 - 
         134820*MBhat^4*Ycut^4 + 542500*m2*MBhat^4*Ycut^4 + 
         54740*m2^2*MBhat^4*Ycut^4 + 400995*m2^3*MBhat^4*Ycut^4 - 
         518665*m2^4*MBhat^4*Ycut^4 + 137200*m2^5*MBhat^4*Ycut^4 + 
         60480*MBhat^5*Ycut^4 - 230580*m2*MBhat^5*Ycut^4 + 
         475608*m2^2*MBhat^5*Ycut^4 - 422940*m2^3*MBhat^5*Ycut^4 + 
         123480*m2^4*MBhat^5*Ycut^4 - 9520*MBhat^6*Ycut^4 + 
         21980*m2*MBhat^6*Ycut^4 - 116620*m2^2*MBhat^6*Ycut^4 + 
         36750*m2^3*MBhat^6*Ycut^4 + 2070*Ycut^5 + 116391*m2*Ycut^5 - 
         94899*m2^2*Ycut^5 - 595602*m2^3*Ycut^5 + 39858*m2^4*Ycut^5 + 
         27993*m2^5*Ycut^5 - 1575*m2^6*Ycut^5 - 14430*MBhat*Ycut^5 - 
         381126*m2*MBhat*Ycut^5 + 1226820*m2^2*MBhat*Ycut^5 + 
         1195572*m2^3*MBhat*Ycut^5 - 286566*m2^4*MBhat*Ycut^5 + 
         64554*m2^5*MBhat*Ycut^5 - 6552*m2^6*MBhat*Ycut^5 + 
         21294*MBhat^2*Ycut^5 + 394079*m2*MBhat^2*Ycut^5 - 
         2473121*m2^2*MBhat^2*Ycut^5 + 108969*m2^3*MBhat^2*Ycut^5 - 
         98441*m2^4*MBhat^2*Ycut^5 + 96530*m2^5*MBhat^2*Ycut^5 - 
         16170*m2^6*MBhat^2*Ycut^5 - 61728*MBhat^3*Ycut^5 + 
         20680*m2*MBhat^3*Ycut^5 + 1690052*m2^2*MBhat^3*Ycut^5 - 
         657300*m2^3*MBhat^3*Ycut^5 + 60620*m2^4*MBhat^3*Ycut^5 + 
         101836*m2^5*MBhat^3*Ycut^5 - 30240*m2^6*MBhat^3*Ycut^5 + 
         88620*MBhat^4*Ycut^5 - 184730*m2*MBhat^4*Ycut^5 - 
         225120*m2^2*MBhat^4*Ycut^5 - 134295*m2^3*MBhat^4*Ycut^5 + 
         298655*m2^4*MBhat^4*Ycut^5 - 82320*m2^5*MBhat^4*Ycut^5 - 
         40026*MBhat^5*Ycut^5 + 64554*m2*MBhat^5*Ycut^5 - 
         217098*m2^2*MBhat^5*Ycut^5 + 247842*m2^3*MBhat^5*Ycut^5 - 
         74088*m2^4*MBhat^5*Ycut^5 + 4200*MBhat^6*Ycut^5 - 
         9100*m2*MBhat^6*Ycut^5 + 69160*m2^2*MBhat^6*Ycut^5 - 
         22050*m2^3*MBhat^6*Ycut^5 - 30*Ycut^6 - 24885*m2*Ycut^6 + 
         13986*m2^2*Ycut^6 + 135954*m2^3*Ycut^6 + 2457*m2^4*Ycut^6 - 
         10395*m2^5*Ycut^6 + 525*m2^6*Ycut^6 + 10476*MBhat*Ycut^6 + 
         72702*m2*MBhat*Ycut^6 - 230580*m2^2*MBhat*Ycut^6 - 
         352128*m2^3*MBhat*Ycut^6 + 103656*m2^4*MBhat*Ycut^6 - 
         24486*m2^5*MBhat*Ycut^6 + 2184*m2^6*MBhat*Ycut^6 + 
         20664*MBhat^2*Ycut^6 - 125965*m2*MBhat^2*Ycut^6 + 
         515634*m2^2*MBhat^2*Ycut^6 + 55573*m2^3*MBhat^2*Ycut^6 + 
         20482*m2^4*MBhat^2*Ycut^6 - 34398*m2^5*MBhat^2*Ycut^6 + 
         5390*m2^6*MBhat^2*Ycut^6 - 3432*MBhat^3*Ycut^6 + 
         7252*m2*MBhat^3*Ycut^6 - 488796*m2^2*MBhat^3*Ycut^6 + 
         308784*m2^3*MBhat^3*Ycut^6 - 73276*m2^4*MBhat^3*Ycut^6 - 
         29652*m2^5*MBhat^3*Ycut^6 + 10080*m2^6*MBhat^3*Ycut^6 - 
         54474*MBhat^4*Ycut^6 + 3346*m2*MBhat^4*Ycut^6 + 
         240016*m2^2*MBhat^4*Ycut^6 - 51009*m2^3*MBhat^4*Ycut^6 - 
         91189*m2^4*MBhat^4*Ycut^6 + 27440*m2^5*MBhat^4*Ycut^6 + 
         28476*MBhat^5*Ycut^6 + 28350*m2*MBhat^5*Ycut^6 + 
         21672*m2^2*MBhat^5*Ycut^6 - 79086*m2^3*MBhat^5*Ycut^6 + 
         24696*m2^4*MBhat^5*Ycut^6 - 1680*MBhat^6*Ycut^6 + 
         980*m2*MBhat^6*Ycut^6 - 23520*m2^2*MBhat^6*Ycut^6 + 
         7350*m2^3*MBhat^6*Ycut^6 - 1874*Ycut^7 + 3355*m2*Ycut^7 - 
         3869*m2^2*Ycut^7 + 3635*m2^3*Ycut^7 - 8923*m2^4*Ycut^7 + 
         2081*m2^5*Ycut^7 - 75*m2^6*Ycut^7 - 19044*MBhat*Ycut^7 + 
         20562*m2*MBhat*Ycut^7 - 8292*m2^2*MBhat*Ycut^7 + 
         41100*m2^3*MBhat*Ycut^7 - 18624*m2^4*MBhat*Ycut^7 + 
         4770*m2^5*MBhat*Ycut^7 - 312*m2^6*MBhat*Ycut^7 - 
         20958*MBhat^2*Ycut^7 + 41153*m2*MBhat^2*Ycut^7 + 
         15827*m2^2*MBhat^2*Ycut^7 - 63910*m2^3*MBhat^2*Ycut^7 + 
         6202*m2^4*MBhat^2*Ycut^7 + 5866*m2^5*MBhat^2*Ycut^7 - 
         770*m2^6*MBhat^2*Ycut^7 + 35562*MBhat^3*Ycut^7 - 
         2798*m2*MBhat^3*Ycut^7 + 47266*m2^2*MBhat^3*Ycut^7 - 
         95310*m2^3*MBhat^3*Ycut^7 + 39524*m2^4*MBhat^3*Ycut^7 + 
         2396*m2^5*MBhat^3*Ycut^7 - 1440*m2^6*MBhat^3*Ycut^7 + 
         23184*MBhat^4*Ycut^7 + 12040*m2*MBhat^4*Ycut^7 - 
         131614*m2^2*MBhat^4*Ycut^7 + 62307*m2^3*MBhat^4*Ycut^7 + 
         9443*m2^4*MBhat^4*Ycut^7 - 3920*m2^5*MBhat^4*Ycut^7 - 
         18270*MBhat^5*Ycut^7 - 31080*m2*MBhat^5*Ycut^7 + 
         25872*m2^2*MBhat^5*Ycut^7 + 9786*m2^3*MBhat^5*Ycut^7 - 
         3528*m2^4*MBhat^5*Ycut^7 + 1400*MBhat^6*Ycut^7 + 
         700*m2*MBhat^6*Ycut^7 + 3640*m2^2*MBhat^6*Ycut^7 - 
         1050*m2^3*MBhat^6*Ycut^7 + 4629*Ycut^8 - 5435*m2*Ycut^8 + 
         5711*m2^2*Ycut^8 - 7839*m2^3*Ycut^8 + 2453*m2^4*Ycut^8 - 
         149*m2^5*Ycut^8 + 21948*MBhat*Ycut^8 - 25530*m2*MBhat*Ycut^8 + 
         2928*m2^2*MBhat*Ycut^8 + 5388*m2^3*MBhat*Ycut^8 + 
         624*m2^4*MBhat*Ycut^8 - 318*m2^5*MBhat*Ycut^8 - 
         1260*MBhat^2*Ycut^8 - 18865*m2*MBhat^2*Ycut^8 - 
         12698*m2^2*MBhat^2*Ycut^8 + 19572*m2^3*MBhat^2*Ycut^8 - 
         3941*m2^4*MBhat^2*Ycut^8 - 238*m2^5*MBhat^2*Ycut^8 - 
         31092*MBhat^3*Ycut^8 + 6850*m2*MBhat^3*Ycut^8 + 
         11696*m2^2*MBhat^3*Ycut^8 + 18306*m2^3*MBhat^3*Ycut^8 - 
         10420*m2^4*MBhat^3*Ycut^8 + 460*m2^5*MBhat^3*Ycut^8 + 
         105*MBhat^4*Ycut^8 + 490*m2*MBhat^4*Ycut^8 + 35826*m2^2*MBhat^4*
          Ycut^8 - 20517*m2^3*MBhat^4*Ycut^8 + 896*m2^4*MBhat^4*Ycut^8 + 
         6720*MBhat^5*Ycut^8 + 10920*m2*MBhat^5*Ycut^8 - 
         11088*m2^2*MBhat^5*Ycut^8 + 378*m2^3*MBhat^5*Ycut^8 - 
         1050*MBhat^6*Ycut^8 - 350*m2*MBhat^6*Ycut^8 - 70*m2^2*MBhat^6*
          Ycut^8 - 6060*Ycut^9 + 5830*m2*Ycut^9 - 2844*m2^2*Ycut^9 + 
         972*m2^3*Ycut^9 - 40*m2^4*Ycut^9 - 12240*MBhat*Ycut^9 + 
         13470*m2*MBhat*Ycut^9 + 1068*m2^2*MBhat*Ycut^9 - 
         1944*m2^3*MBhat*Ycut^9 + 150*m2^4*MBhat*Ycut^9 + 
         12950*MBhat^2*Ycut^9 + 2975*m2*MBhat^2*Ycut^9 - 
         4263*m2^2*MBhat^2*Ycut^9 - 1631*m2^3*MBhat^2*Ycut^9 + 
         553*m2^4*MBhat^2*Ycut^9 + 10110*MBhat^3*Ycut^9 - 
         5300*m2*MBhat^3*Ycut^9 - 3264*m2^2*MBhat^3*Ycut^9 - 
         1758*m2^3*MBhat^3*Ycut^9 + 1052*m2^4*MBhat^3*Ycut^9 - 
         4550*MBhat^4*Ycut^9 - 1435*m2*MBhat^4*Ycut^9 - 
         3619*m2^2*MBhat^4*Ycut^9 + 2254*m2^3*MBhat^4*Ycut^9 - 
         630*MBhat^5*Ycut^9 - 1470*m2*MBhat^5*Ycut^9 + 1302*m2^2*MBhat^5*
          Ycut^9 + 420*MBhat^6*Ycut^9 + 70*m2*MBhat^6*Ycut^9 + 4405*Ycut^10 - 
         3058*m2*Ycut^10 + 503*m2^2*Ycut^10 + 40*m2^3*Ycut^10 + 
         1620*MBhat*Ycut^10 - 2886*m2*MBhat*Ycut^10 + 324*m2^2*MBhat*
          Ycut^10 + 60*m2^3*MBhat*Ycut^10 - 7434*MBhat^2*Ycut^10 + 
         1141*m2*MBhat^2*Ycut^10 + 1078*m2^2*MBhat^2*Ycut^10 + 
         7*m2^3*MBhat^2*Ycut^10 + 576*MBhat^3*Ycut^10 + 
         1240*m2*MBhat^3*Ycut^10 + 76*m2^2*MBhat^3*Ycut^10 - 
         2*m2^3*MBhat^3*Ycut^10 + 1155*MBhat^4*Ycut^10 + 
         245*m2*MBhat^4*Ycut^10 - 14*m2^2*MBhat^4*Ycut^10 - 
         252*MBhat^5*Ycut^10 - 42*m2*MBhat^5*Ycut^10 - 70*MBhat^6*Ycut^10 - 
         1650*Ycut^11 + 647*m2*Ycut^11 - 5*m2^2*Ycut^11 + 
         1284*MBhat*Ycut^11 - 90*m2*MBhat*Ycut^11 - 60*m2^2*MBhat*Ycut^11 + 
         966*MBhat^2*Ycut^11 - 245*m2*MBhat^2*Ycut^11 - 
         7*m2^2*MBhat^2*Ycut^11 - 726*MBhat^3*Ycut^11 - 
         74*m2*MBhat^3*Ycut^11 + 2*m2^2*MBhat^3*Ycut^11 + 
         84*MBhat^4*Ycut^11 + 14*m2*MBhat^4*Ycut^11 + 42*MBhat^5*Ycut^11 + 
         219*Ycut^12 + 5*m2*Ycut^12 - 396*MBhat*Ycut^12 + 
         18*m2*MBhat*Ycut^12 + 203*MBhat^2*Ycut^12 + 7*m2*MBhat^2*Ycut^12 - 
         12*MBhat^3*Ycut^12 - 2*m2*MBhat^3*Ycut^12 - 14*MBhat^4*Ycut^12 + 
         16*Ycut^13 - 18*MBhat*Ycut^13 + 2*MBhat^3*Ycut^13))/(-1 + Ycut)^7 + 
     (4*Sqrt[m2]*Ycut^3*(-1 + m2 + Ycut)*(10*MBhat^3 - 30*m2*MBhat^3 + 
        30*m2^2*MBhat^3 - 10*m2^3*MBhat^3 - 20*MBhat^4 + 40*m2*MBhat^4 - 
        20*m2^2*MBhat^4 + 12*MBhat^5 - 12*m2*MBhat^5 - 2*MBhat^6 - 
        15*MBhat^2*Ycut + 45*m2*MBhat^2*Ycut - 45*m2^2*MBhat^2*Ycut + 
        15*m2^3*MBhat^2*Ycut - 5*MBhat^3*Ycut + 25*m2*MBhat^3*Ycut - 
        35*m2^2*MBhat^3*Ycut + 15*m2^3*MBhat^3*Ycut + 50*MBhat^4*Ycut - 
        90*m2*MBhat^4*Ycut + 40*m2^2*MBhat^4*Ycut - 36*MBhat^5*Ycut + 
        30*m2*MBhat^5*Ycut + 6*MBhat^6*Ycut + 9*MBhat*Ycut^2 - 
        27*m2*MBhat*Ycut^2 + 27*m2^2*MBhat*Ycut^2 - 9*m2^3*MBhat*Ycut^2 + 
        31*MBhat^2*Ycut^2 - 74*m2*MBhat^2*Ycut^2 + 55*m2^2*MBhat^2*Ycut^2 - 
        12*m2^3*MBhat^2*Ycut^2 - 40*MBhat^3*Ycut^2 + 45*m2*MBhat^3*Ycut^2 + 
        4*m2^2*MBhat^3*Ycut^2 - 9*m2^3*MBhat^3*Ycut^2 - 30*MBhat^4*Ycut^2 + 
        60*m2*MBhat^4*Ycut^2 - 26*m2^2*MBhat^4*Ycut^2 + 36*MBhat^5*Ycut^2 - 
        24*m2*MBhat^5*Ycut^2 - 6*MBhat^6*Ycut^2 - 2*Ycut^3 + 6*m2*Ycut^3 - 
        6*m2^2*Ycut^3 + 2*m2^3*Ycut^3 - 24*MBhat*Ycut^3 + 
        51*m2*MBhat*Ycut^3 - 30*m2^2*MBhat*Ycut^3 + 3*m2^3*MBhat*Ycut^3 - 
        4*MBhat^2*Ycut^3 + 12*m2*MBhat^2*Ycut^3 - 11*m2^2*MBhat^2*Ycut^3 + 
        3*m2^3*MBhat^2*Ycut^3 + 50*MBhat^3*Ycut^3 - 45*m2*MBhat^3*Ycut^3 + 
        m2^2*MBhat^3*Ycut^3 + 2*m2^3*MBhat^3*Ycut^3 - 10*MBhat^4*Ycut^3 - 
        10*m2*MBhat^4*Ycut^3 + 6*m2^2*MBhat^4*Ycut^3 - 12*MBhat^5*Ycut^3 + 
        6*m2*MBhat^5*Ycut^3 + 2*MBhat^6*Ycut^3 + 6*Ycut^4 - 12*m2*Ycut^4 + 
        6*m2^2*Ycut^4 + 18*MBhat*Ycut^4 - 21*m2*MBhat*Ycut^4 + 
        3*m2^2*MBhat*Ycut^4 - 24*MBhat^2*Ycut^4 + 18*m2*MBhat^2*Ycut^4 + 
        m2^2*MBhat^2*Ycut^4 - 10*MBhat^3*Ycut^4 + 5*m2*MBhat^3*Ycut^4 + 
        10*MBhat^4*Ycut^4 - 6*Ycut^5 + 6*m2*Ycut^5 - 3*m2*MBhat*Ycut^5 + 
        11*MBhat^2*Ycut^5 - m2*MBhat^2*Ycut^5 - 5*MBhat^3*Ycut^5 + 2*Ycut^6 - 
        3*MBhat*Ycut^6 + MBhat^2*Ycut^6)*c[SR]*c[T])/(-1 + Ycut)^5 + 
     m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 48*MBhat + 72*m2*MBhat + 
       360*m2^2*MBhat + 72*MBhat^2 - 276*m2*MBhat^2 - 274*m2^2*MBhat^2 + 
       58*m2^3*MBhat^2 - 48*MBhat^3 + 288*m2*MBhat^3 + 12*MBhat^4 - 
       105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 6*m2*MBhat^6)*Log[m2] - 
     m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 48*MBhat + 72*m2*MBhat + 
       360*m2^2*MBhat + 72*MBhat^2 - 276*m2*MBhat^2 - 274*m2^2*MBhat^2 + 
       58*m2^3*MBhat^2 - 48*MBhat^3 + 288*m2*MBhat^3 + 12*MBhat^4 - 
       105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 6*m2*MBhat^6)*Log[1 - Ycut] + 
     c[SL]^2*(-1/840*((-1 + m2 + Ycut)*(732 + 66504*m2 + 286164*m2^2 + 
           191664*m2^3 + 564*m2^4 - 1368*m2^5 + 60*m2^6 - 4320*MBhat - 
           271944*m2*MBhat - 698328*m2^2*MBhat - 131328*m2^3*MBhat + 
           19872*m2^4*MBhat - 2808*m2^5*MBhat + 216*m2^6*MBhat + 
           10731*MBhat^2 + 442071*m2*MBhat^2 + 578466*m2^2*MBhat^2 - 
           32634*m2^3*MBhat^2 + 9891*m2^4*MBhat^2 - 3465*m2^5*MBhat^2 + 
           420*m2^6*MBhat^2 - 14640*MBhat^3 - 366096*m2*MBhat^3 - 
           158616*m2^2*MBhat^3 + 26184*m2^3*MBhat^3 + 984*m2^4*MBhat^3 - 
           2376*m2^5*MBhat^3 + 480*m2^6*MBhat^3 + 12180*MBhat^4 + 
           163380*m2*MBhat^4 - 22470*m2^2*MBhat^4 + 17850*m2^3*MBhat^4 - 
           8610*m2^4*MBhat^4 + 1470*m2^5*MBhat^4 - 6048*MBhat^5 - 
           38808*m2*MBhat^5 + 21672*m2^2*MBhat^5 - 8568*m2^3*MBhat^5 + 
           1512*m2^4*MBhat^5 + 1365*MBhat^6 + 4725*m2*MBhat^6 - 
           2835*m2^2*MBhat^6 + 525*m2^3*MBhat^6 - 4392*Ycut - 
           418452*m2*Ycut - 1846308*m2^2*Ycut - 1270344*m2^3*Ycut - 
           7680*m2^4*Ycut + 9516*m2^5*Ycut - 420*m2^6*Ycut + 
           25920*MBhat*Ycut + 1718064*m2*MBhat*Ycut + 4548744*m2^2*MBhat*
            Ycut + 902016*m2^3*MBhat*Ycut - 136512*m2^4*MBhat*Ycut + 
           19440*m2^5*MBhat*Ycut - 1512*m2^6*MBhat*Ycut - 
           64386*MBhat^2*Ycut - 2808015*m2*MBhat^2*Ycut - 
           3823134*m2^2*MBhat^2*Ycut + 205212*m2^3*MBhat^2*Ycut - 
           66192*m2^4*MBhat^2*Ycut + 23835*m2^5*MBhat^2*Ycut - 
           2940*m2^6*MBhat^2*Ycut + 87840*MBhat^3*Ycut + 2343216*m2*MBhat^3*
            Ycut + 1085040*m2^2*MBhat^3*Ycut - 182376*m2^3*MBhat^3*Ycut - 
           4992*m2^4*MBhat^3*Ycut + 16152*m2^5*MBhat^3*Ycut - 
           3360*m2^6*MBhat^3*Ycut - 73080*MBhat^4*Ycut - 1058820*m2*MBhat^4*
            Ycut + 136500*m2^2*MBhat^4*Ycut - 117810*m2^3*MBhat^4*Ycut + 
           58800*m2^4*MBhat^4*Ycut - 10290*m2^5*MBhat^4*Ycut + 
           36288*MBhat^5*Ycut + 257040*m2*MBhat^5*Ycut - 144648*m2^2*MBhat^5*
            Ycut + 58464*m2^3*MBhat^5*Ycut - 10584*m2^4*MBhat^5*Ycut - 
           8190*MBhat^6*Ycut - 32025*m2*MBhat^6*Ycut + 19320*m2^2*MBhat^6*
            Ycut - 3675*m2^3*MBhat^6*Ycut + 10980*Ycut^2 + 
           1104780*m2*Ycut^2 + 5017932*m2^2*Ycut^2 + 3564888*m2^3*Ycut^2 + 
           35508*m2^4*Ycut^2 - 28308*m2^5*Ycut^2 + 1260*m2^6*Ycut^2 - 
           64800*MBhat*Ycut^2 - 4556520*m2*MBhat*Ycut^2 - 12499920*m2^2*MBhat*
            Ycut^2 - 2639304*m2^3*MBhat*Ycut^2 + 399384*m2^4*MBhat*Ycut^2 - 
           57456*m2^5*MBhat*Ycut^2 + 4536*m2^6*MBhat*Ycut^2 + 
           160965*MBhat^2*Ycut^2 + 7492170*m2*MBhat^2*Ycut^2 + 
           10685661*m2^2*MBhat^2*Ycut^2 - 533547*m2^3*MBhat^2*Ycut^2 + 
           186816*m2^4*MBhat^2*Ycut^2 - 69825*m2^5*MBhat^2*Ycut^2 + 
           8820*m2^6*MBhat^2*Ycut^2 - 219600*MBhat^3*Ycut^2 - 
           6305280*m2*MBhat^3*Ycut^2 - 3156360*m2^2*MBhat^3*Ycut^2 + 
           542064*m2^3*MBhat^3*Ycut^2 + 7872*m2^4*MBhat^3*Ycut^2 - 
           46536*m2^5*MBhat^3*Ycut^2 + 10080*m2^6*MBhat^3*Ycut^2 + 
           182700*MBhat^4*Ycut^2 + 2888760*m2*MBhat^4*Ycut^2 - 
           337050*m2^2*MBhat^4*Ycut^2 + 326340*m2^3*MBhat^4*Ycut^2 - 
           170520*m2^4*MBhat^4*Ycut^2 + 30870*m2^5*MBhat^4*Ycut^2 - 
           90720*MBhat^5*Ycut^2 - 718200*m2*MBhat^5*Ycut^2 + 
           407232*m2^2*MBhat^5*Ycut^2 - 169344*m2^3*MBhat^5*Ycut^2 + 
           31752*m2^4*MBhat^5*Ycut^2 + 20475*MBhat^6*Ycut^2 + 
           91770*m2*MBhat^6*Ycut^2 - 55860*m2^2*MBhat^6*Ycut^2 + 
           11025*m2^3*MBhat^6*Ycut^2 - 14640*Ycut^3 - 1571400*m2*Ycut^3 - 
           7387368*m2^2*Ycut^3 - 5458380*m2^3*Ycut^3 - 82572*m2^4*Ycut^3 + 
           46620*m2^5*Ycut^3 - 2100*m2^6*Ycut^3 + 86400*MBhat*Ycut^3 + 
           6514560*m2*MBhat*Ycut^3 + 18645120*m2^2*MBhat*Ycut^3 + 
           4250016*m2^3*MBhat*Ycut^3 - 642600*m2^4*MBhat*Ycut^3 + 
           93744*m2^5*MBhat*Ycut^3 - 7560*m2^6*MBhat*Ycut^3 - 
           214620*MBhat^2*Ycut^3 - 10785390*m2*MBhat^2*Ycut^3 - 
           16265844*m2^2*MBhat^2*Ycut^3 + 724269*m2^3*MBhat^2*Ycut^3 - 
           285180*m2^4*MBhat^2*Ycut^3 + 112455*m2^5*MBhat^2*Ycut^3 - 
           14700*m2^6*MBhat^2*Ycut^3 + 291330*MBhat^3*Ycut^3 + 
           9174930*m2*MBhat^3*Ycut^3 + 5011620*m2^2*MBhat^3*Ycut^3 - 
           855516*m2^3*MBhat^3*Ycut^3 - 17094*m2^4*MBhat^3*Ycut^3 + 
           77490*m2^5*MBhat^3*Ycut^3 - 16800*m2^6*MBhat^3*Ycut^3 - 
           241080*MBhat^4*Ycut^3 - 4282320*m2*MBhat^4*Ycut^3 + 
           457800*m2^2*MBhat^4*Ycut^3 - 517860*m2^3*MBhat^4*Ycut^3 + 
           280560*m2^4*MBhat^4*Ycut^3 - 51450*m2^5*MBhat^4*Ycut^3 + 
           119910*MBhat^5*Ycut^3 + 1098090*m2*MBhat^5*Ycut^3 - 
           636678*m2^2*MBhat^5*Ycut^3 + 275478*m2^3*MBhat^5*Ycut^3 - 
           52920*m2^4*MBhat^5*Ycut^3 - 27300*MBhat^6*Ycut^3 - 
           144690*m2*MBhat^6*Ycut^3 + 89880*m2^2*MBhat^6*Ycut^3 - 
           18375*m2^3*MBhat^6*Ycut^3 + 10980*Ycut^4 + 1277520*m2*Ycut^4 + 
           6267552*m2^2*Ycut^4 + 4870572*m2^3*Ycut^4 + 109200*m2^4*Ycut^4 - 
           45780*m2^5*Ycut^4 + 2100*m2^6*Ycut^4 - 64800*MBhat*Ycut^4 - 
           5328720*m2*MBhat*Ycut^4 - 16076880*m2^2*MBhat*Ycut^4 - 
           4040064*m2^3*MBhat*Ycut^4 + 609336*m2^4*MBhat*Ycut^4 - 
           90720*m2^5*MBhat*Ycut^4 + 7560*m2^6*MBhat*Ycut^4 + 
           163170*MBhat^2*Ycut^4 + 8878380*m2*MBhat^2*Ycut^4 + 
           14423451*m2^2*MBhat^2*Ycut^4 - 567840*m2^3*MBhat^2*Ycut^4 + 
           277095*m2^4*MBhat^2*Ycut^4 - 113190*m2^5*MBhat^2*Ycut^4 + 
           14700*m2^6*MBhat^2*Ycut^4 - 214840*MBhat^3*Ycut^4 - 
           7666870*m2*MBhat^3*Ycut^4 - 4668520*m2^2*MBhat^3*Ycut^4 + 
           791644*m2^3*MBhat^3*Ycut^4 + 21280*m2^4*MBhat^3*Ycut^4 - 
           77630*m2^5*MBhat^3*Ycut^4 + 16800*m2^6*MBhat^3*Ycut^4 + 
           169470*MBhat^4*Ycut^4 + 3690960*m2*MBhat^4*Ycut^4 - 
           382200*m2^2*MBhat^4*Ycut^4 + 510300*m2^3*MBhat^4*Ycut^4 - 
           280770*m2^4*MBhat^4*Ycut^4 + 51450*m2^5*MBhat^4*Ycut^4 - 
           84420*MBhat^5*Ycut^4 - 992670*m2*MBhat^5*Ycut^4 + 
           603372*m2^2*MBhat^5*Ycut^4 - 273210*m2^3*MBhat^5*Ycut^4 + 
           52920*m2^4*MBhat^5*Ycut^4 + 20440*MBhat^6*Ycut^4 + 
           136150*m2*MBhat^6*Ycut^4 - 87710*m2^2*MBhat^6*Ycut^4 + 
           18375*m2^3*MBhat^6*Ycut^4 - 4392*Ycut^5 - 570756*m2*Ycut^5 - 
           2964444*m2^2*Ycut^5 - 2471112*m2^3*Ycut^5 - 83832*m2^4*Ycut^5 + 
           26628*m2^5*Ycut^5 - 1260*m2^6*Ycut^5 + 24597*MBhat*Ycut^5 + 
           2408589*m2*MBhat*Ycut^5 + 7744842*m2^2*MBhat*Ycut^5 + 
           2260818*m2^3*MBhat*Ycut^5 - 351351*m2^4*MBhat*Ycut^5 + 
           55377*m2^5*MBhat*Ycut^5 - 4536*m2^6*MBhat*Ycut^5 - 
           74844*MBhat^2*Ycut^5 - 3990504*m2*MBhat^2*Ycut^5 - 
           7301238*m2^2*MBhat^2*Ycut^5 + 268002*m2^3*MBhat^2*Ycut^5 - 
           172788*m2^4*MBhat^2*Ycut^5 + 69090*m2^5*MBhat^2*Ycut^5 - 
           8820*m2^6*MBhat^2*Ycut^5 + 88638*MBhat^3*Ycut^5 + 
           3513842*m2*MBhat^3*Ycut^5 + 2556652*m2^2*MBhat^3*Ycut^5 - 
           454944*m2^3*MBhat^3*Ycut^5 - 8834*m2^4*MBhat^3*Ycut^5 + 
           46718*m2^5*MBhat^3*Ycut^5 - 10080*m2^6*MBhat^3*Ycut^5 - 
           46620*MBhat^4*Ycut^5 - 1807050*m2*MBhat^4*Ycut^5 + 
           192150*m2^2*MBhat^4*Ycut^5 - 303030*m2^3*MBhat^4*Ycut^5 + 
           169050*m2^4*MBhat^4*Ycut^5 - 30870*m2^5*MBhat^4*Ycut^5 + 
           20601*MBhat^5*Ycut^5 + 531111*m2*MBhat^5*Ycut^5 - 
           347277*m2^2*MBhat^5*Ycut^5 + 164493*m2^3*MBhat^5*Ycut^5 - 
           31752*m2^4*MBhat^5*Ycut^5 - 7980*MBhat^6*Ycut^5 - 
           76790*m2*MBhat^6*Ycut^5 + 52220*m2^2*MBhat^6*Ycut^5 - 
           11025*m2^3*MBhat^6*Ycut^5 + 1026*Ycut^6 + 113106*m2*Ycut^6 + 
           660492*m2^2*Ycut^6 + 606900*m2^3*Ycut^6 + 38598*m2^4*Ycut^6 - 
           9198*m2^5*Ycut^6 + 420*m2^6*Ycut^6 + 2778*MBhat*Ycut^6 - 
           527457*m2*MBhat*Ycut^6 - 1700706*m2^2*MBhat*Ycut^6 - 
           700728*m2^3*MBhat*Ycut^6 + 121296*m2^4*MBhat*Ycut^6 - 
           19383*m2^5*MBhat*Ycut^6 + 1512*m2^6*MBhat*Ycut^6 + 
           27762*MBhat^2*Ycut^6 + 776454*m2*MBhat^2*Ycut^6 + 
           1824921*m2^2*MBhat^2*Ycut^6 - 59997*m2^3*MBhat^2*Ycut^6 + 
           58800*m2^4*MBhat^2*Ycut^6 - 23814*m2^5*MBhat^2*Ycut^6 + 
           2940*m2^6*MBhat^2*Ycut^6 - 37068*MBhat^3*Ycut^6 - 
           667744*m2*MBhat^3*Ycut^6 - 773682*m2^2*MBhat^3*Ycut^6 + 
           158214*m2^3*MBhat^3*Ycut^6 - 350*m2^4*MBhat^3*Ycut^6 - 
           15666*m2^5*MBhat^3*Ycut^6 + 3360*m2^6*MBhat^3*Ycut^6 - 
           9912*MBhat^4*Ycut^6 + 427728*m2*MBhat^4*Ycut^6 - 
           43512*m2^2*MBhat^4*Ycut^6 + 96138*m2^3*MBhat^4*Ycut^6 - 
           56742*m2^4*MBhat^4*Ycut^6 + 10290*m2^5*MBhat^4*Ycut^6 + 
           14574*MBhat^5*Ycut^6 - 156975*m2*MBhat^5*Ycut^6 + 
           110628*m2^2*MBhat^5*Ycut^6 - 55419*m2^3*MBhat^5*Ycut^6 + 
           10584*m2^4*MBhat^5*Ycut^6 + 840*MBhat^6*Ycut^6 + 
           24010*m2*MBhat^6*Ycut^6 - 17640*m2^2*MBhat^6*Ycut^6 + 
           3675*m2^3*MBhat^6*Ycut^6 - 1684*Ycut^7 + 6002*m2*Ycut^7 - 
           41836*m2^2*Ycut^7 - 20696*m2^3*Ycut^7 - 11288*m2^4*Ycut^7 + 
           1522*m2^5*Ycut^7 - 60*m2^6*Ycut^7 - 14895*MBhat*Ycut^7 + 
           69000*m2*MBhat*Ycut^7 - 1833*m2^2*MBhat*Ycut^7 + 
           100899*m2^3*MBhat*Ycut^7 - 20040*m2^4*MBhat*Ycut^7 + 
           3165*m2^5*MBhat*Ycut^7 - 216*m2^6*MBhat*Ycut^7 - 
           6048*MBhat^2*Ycut^7 - 16926*m2*MBhat^2*Ycut^7 - 
           89880*m2^2*MBhat^2*Ycut^7 - 7917*m2^3*MBhat^2*Ycut^7 - 
           8232*m2^4*MBhat^2*Ycut^7 + 3738*m2^5*MBhat^2*Ycut^7 - 
           420*m2^6*MBhat^2*Ycut^7 + 36480*MBhat^3*Ycut^7 - 
           55150*m2*MBhat^3*Ycut^7 + 105038*m2^2*MBhat^3*Ycut^7 - 
           25428*m2^3*MBhat^3*Ycut^7 + 1522*m2^4*MBhat^3*Ycut^7 + 
           2278*m2^5*MBhat^3*Ycut^7 - 480*m2^6*MBhat^3*Ycut^7 + 
           252*MBhat^4*Ycut^7 - 13650*m2*MBhat^4*Ycut^7 - 2352*m2^2*MBhat^4*
            Ycut^7 - 11214*m2^3*MBhat^4*Ycut^7 + 8274*m2^4*MBhat^4*Ycut^7 - 
           1470*m2^5*MBhat^4*Ycut^7 - 14805*MBhat^5*Ycut^7 + 
           21840*m2*MBhat^5*Ycut^7 - 13692*m2^2*MBhat^5*Ycut^7 + 
           8169*m2^3*MBhat^5*Ycut^7 - 1512*m2^4*MBhat^5*Ycut^7 + 
           700*MBhat^6*Ycut^7 - 3010*m2*MBhat^6*Ycut^7 + 2660*m2^2*MBhat^6*
            Ycut^7 - 525*m2^3*MBhat^6*Ycut^7 + 3930*Ycut^8 - 
           15100*m2*Ycut^8 + 16144*m2^2*Ycut^8 - 13932*m2^3*Ycut^8 + 
           1450*m2^4*Ycut^8 - 52*m2^5*Ycut^8 + 14400*MBhat*Ycut^8 - 
           32520*m2*MBhat*Ycut^8 + 34632*m2^2*MBhat*Ycut^8 - 
           1809*m2^3*MBhat*Ycut^8 + 516*m2^4*MBhat*Ycut^8 - 
           99*m2^5*MBhat*Ycut^8 - 13545*MBhat^2*Ycut^8 + 29505*m2*MBhat^2*
            Ycut^8 - 33285*m2^2*MBhat^2*Ycut^8 + 4158*m2^3*MBhat^2*Ycut^8 - 
           294*m2^4*MBhat^2*Ycut^8 - 84*m2^5*MBhat^2*Ycut^8 - 
           21900*MBhat^3*Ycut^8 + 29000*m2*MBhat^3*Ycut^8 - 
           1412*m2^2*MBhat^3*Ycut^8 - 240*m2^3*MBhat^3*Ycut^8 - 
           398*m2^4*MBhat^3*Ycut^8 - 10*m2^5*MBhat^3*Ycut^8 + 
           12600*MBhat^4*Ycut^8 - 10500*m2*MBhat^4*Ycut^8 + 
           378*m2^2*MBhat^4*Ycut^8 - 756*m2^3*MBhat^4*Ycut^8 - 
           42*m2^4*MBhat^4*Ycut^8 + 5040*MBhat^5*Ycut^8 - 
           2100*m2*MBhat^5*Ycut^8 - 672*m2^2*MBhat^5*Ycut^8 - 
           63*m2^3*MBhat^5*Ycut^8 - 525*MBhat^6*Ycut^8 - 175*m2*MBhat^6*
            Ycut^8 - 35*m2^2*MBhat^6*Ycut^8 - 4680*Ycut^9 + 11300*m2*Ycut^9 - 
           8676*m2^2*Ycut^9 + 492*m2^3*Ycut^9 + 52*m2^4*Ycut^9 - 
           4395*MBhat*Ycut^9 + 3405*m2*MBhat*Ycut^9 + 4332*m2^2*MBhat*
            Ycut^9 - 417*m2^3*MBhat*Ycut^9 + 99*m2^4*MBhat*Ycut^9 + 
           16590*MBhat^2*Ycut^9 - 20265*m2*MBhat^2*Ycut^9 + 
           1260*m2^2*MBhat^2*Ycut^9 + 378*m2^3*MBhat^2*Ycut^9 + 
           84*m2^4*MBhat^2*Ycut^9 + 360*MBhat^3*Ycut^9 + 590*m2*MBhat^3*
            Ycut^9 + 648*m2^2*MBhat^3*Ycut^9 + 408*m2^3*MBhat^3*Ycut^9 + 
           10*m2^4*MBhat^3*Ycut^9 - 7980*MBhat^4*Ycut^9 + 
           2310*m2*MBhat^4*Ycut^9 + 798*m2^2*MBhat^4*Ycut^9 + 
           42*m2^3*MBhat^4*Ycut^9 - 105*MBhat^5*Ycut^9 + 735*m2*MBhat^5*
            Ycut^9 + 63*m2^2*MBhat^5*Ycut^9 + 210*MBhat^6*Ycut^9 + 
           35*m2*MBhat^6*Ycut^9 + 2810*Ycut^10 - 3698*m2*Ycut^10 + 
           436*m2^2*Ycut^10 - 52*m2^3*Ycut^10 - 2862*MBhat*Ycut^10 + 
           4575*m2*MBhat*Ycut^10 - 102*m2^2*MBhat*Ycut^10 - 
           99*m2^3*MBhat*Ycut^10 - 5607*MBhat^2*Ycut^10 + 
           2688*m2*MBhat^2*Ycut^10 - 462*m2^2*MBhat^2*Ycut^10 - 
           84*m2^3*MBhat^2*Ycut^10 + 4812*MBhat^3*Ycut^10 - 
           856*m2*MBhat^3*Ycut^10 - 418*m2^2*MBhat^3*Ycut^10 - 
           10*m2^3*MBhat^3*Ycut^10 + 1260*MBhat^4*Ycut^10 - 
           840*m2*MBhat^4*Ycut^10 - 42*m2^2*MBhat^4*Ycut^10 - 
           378*MBhat^5*Ycut^10 - 63*m2*MBhat^5*Ycut^10 - 35*MBhat^6*Ycut^10 - 
           564*Ycut^11 + 106*m2*Ycut^11 - 88*m2^2*Ycut^11 + 
           2367*MBhat*Ycut^11 - 1122*m2*MBhat*Ycut^11 + 99*m2^2*MBhat*
            Ycut^11 - 756*MBhat^2*Ycut^11 - 84*m2*MBhat^2*Ycut^11 + 
           84*m2^2*MBhat^2*Ycut^11 - 1362*MBhat^3*Ycut^11 + 
           428*m2*MBhat^3*Ycut^11 + 10*m2^2*MBhat^3*Ycut^11 + 
           252*MBhat^4*Ycut^11 + 42*m2*MBhat^4*Ycut^11 + 63*MBhat^5*Ycut^11 - 
           186*Ycut^12 + 88*m2*Ycut^12 - 300*MBhat*Ycut^12 + 
           90*m2*MBhat*Ycut^12 + 588*MBhat^2*Ycut^12 - 84*m2*MBhat^2*
            Ycut^12 - 60*MBhat^3*Ycut^12 - 10*m2*MBhat^3*Ycut^12 - 
           42*MBhat^4*Ycut^12 + 80*Ycut^13 - 90*MBhat*Ycut^13 + 
           10*MBhat^3*Ycut^13))/(-1 + Ycut)^7 + 
       (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 4*m2^4 + 72*MBhat + 
          432*m2*MBhat + 360*m2^2*MBhat - 132*MBhat^2 - 507*m2*MBhat^2 - 
          172*m2^2*MBhat^2 + 13*m2^3*MBhat^2 + 128*MBhat^3 + 280*m2*MBhat^3 - 
          72*MBhat^4 - 66*m2*MBhat^4 + 8*m2^2*MBhat^4 + 24*MBhat^5 - 
          4*MBhat^6 + m2*MBhat^6)*Log[m2])/2 - 
       (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 4*m2^4 + 72*MBhat + 
          432*m2*MBhat + 360*m2^2*MBhat - 132*MBhat^2 - 507*m2*MBhat^2 - 
          172*m2^2*MBhat^2 + 13*m2^3*MBhat^2 + 128*MBhat^3 + 280*m2*MBhat^3 - 
          72*MBhat^4 - 66*m2*MBhat^4 + 8*m2^2*MBhat^4 + 24*MBhat^5 - 
          4*MBhat^6 + m2*MBhat^6)*Log[1 - Ycut])/2) + 
     c[SR]^2*(-1/840*((-1 + m2 + Ycut)*(732 + 66504*m2 + 286164*m2^2 + 
           191664*m2^3 + 564*m2^4 - 1368*m2^5 + 60*m2^6 - 4320*MBhat - 
           271944*m2*MBhat - 698328*m2^2*MBhat - 131328*m2^3*MBhat + 
           19872*m2^4*MBhat - 2808*m2^5*MBhat + 216*m2^6*MBhat + 
           10731*MBhat^2 + 442071*m2*MBhat^2 + 578466*m2^2*MBhat^2 - 
           32634*m2^3*MBhat^2 + 9891*m2^4*MBhat^2 - 3465*m2^5*MBhat^2 + 
           420*m2^6*MBhat^2 - 14640*MBhat^3 - 366096*m2*MBhat^3 - 
           158616*m2^2*MBhat^3 + 26184*m2^3*MBhat^3 + 984*m2^4*MBhat^3 - 
           2376*m2^5*MBhat^3 + 480*m2^6*MBhat^3 + 12180*MBhat^4 + 
           163380*m2*MBhat^4 - 22470*m2^2*MBhat^4 + 17850*m2^3*MBhat^4 - 
           8610*m2^4*MBhat^4 + 1470*m2^5*MBhat^4 - 6048*MBhat^5 - 
           38808*m2*MBhat^5 + 21672*m2^2*MBhat^5 - 8568*m2^3*MBhat^5 + 
           1512*m2^4*MBhat^5 + 1365*MBhat^6 + 4725*m2*MBhat^6 - 
           2835*m2^2*MBhat^6 + 525*m2^3*MBhat^6 - 4392*Ycut - 
           418452*m2*Ycut - 1846308*m2^2*Ycut - 1270344*m2^3*Ycut - 
           7680*m2^4*Ycut + 9516*m2^5*Ycut - 420*m2^6*Ycut + 
           25920*MBhat*Ycut + 1718064*m2*MBhat*Ycut + 4548744*m2^2*MBhat*
            Ycut + 902016*m2^3*MBhat*Ycut - 136512*m2^4*MBhat*Ycut + 
           19440*m2^5*MBhat*Ycut - 1512*m2^6*MBhat*Ycut - 
           64386*MBhat^2*Ycut - 2808015*m2*MBhat^2*Ycut - 
           3823134*m2^2*MBhat^2*Ycut + 205212*m2^3*MBhat^2*Ycut - 
           66192*m2^4*MBhat^2*Ycut + 23835*m2^5*MBhat^2*Ycut - 
           2940*m2^6*MBhat^2*Ycut + 87840*MBhat^3*Ycut + 2343216*m2*MBhat^3*
            Ycut + 1085040*m2^2*MBhat^3*Ycut - 182376*m2^3*MBhat^3*Ycut - 
           4992*m2^4*MBhat^3*Ycut + 16152*m2^5*MBhat^3*Ycut - 
           3360*m2^6*MBhat^3*Ycut - 73080*MBhat^4*Ycut - 1058820*m2*MBhat^4*
            Ycut + 136500*m2^2*MBhat^4*Ycut - 117810*m2^3*MBhat^4*Ycut + 
           58800*m2^4*MBhat^4*Ycut - 10290*m2^5*MBhat^4*Ycut + 
           36288*MBhat^5*Ycut + 257040*m2*MBhat^5*Ycut - 144648*m2^2*MBhat^5*
            Ycut + 58464*m2^3*MBhat^5*Ycut - 10584*m2^4*MBhat^5*Ycut - 
           8190*MBhat^6*Ycut - 32025*m2*MBhat^6*Ycut + 19320*m2^2*MBhat^6*
            Ycut - 3675*m2^3*MBhat^6*Ycut + 10980*Ycut^2 + 
           1104780*m2*Ycut^2 + 5017932*m2^2*Ycut^2 + 3564888*m2^3*Ycut^2 + 
           35508*m2^4*Ycut^2 - 28308*m2^5*Ycut^2 + 1260*m2^6*Ycut^2 - 
           64800*MBhat*Ycut^2 - 4556520*m2*MBhat*Ycut^2 - 12499920*m2^2*MBhat*
            Ycut^2 - 2639304*m2^3*MBhat*Ycut^2 + 399384*m2^4*MBhat*Ycut^2 - 
           57456*m2^5*MBhat*Ycut^2 + 4536*m2^6*MBhat*Ycut^2 + 
           160965*MBhat^2*Ycut^2 + 7492170*m2*MBhat^2*Ycut^2 + 
           10685661*m2^2*MBhat^2*Ycut^2 - 533547*m2^3*MBhat^2*Ycut^2 + 
           186816*m2^4*MBhat^2*Ycut^2 - 69825*m2^5*MBhat^2*Ycut^2 + 
           8820*m2^6*MBhat^2*Ycut^2 - 219600*MBhat^3*Ycut^2 - 
           6305280*m2*MBhat^3*Ycut^2 - 3156360*m2^2*MBhat^3*Ycut^2 + 
           542064*m2^3*MBhat^3*Ycut^2 + 7872*m2^4*MBhat^3*Ycut^2 - 
           46536*m2^5*MBhat^3*Ycut^2 + 10080*m2^6*MBhat^3*Ycut^2 + 
           182700*MBhat^4*Ycut^2 + 2888760*m2*MBhat^4*Ycut^2 - 
           337050*m2^2*MBhat^4*Ycut^2 + 326340*m2^3*MBhat^4*Ycut^2 - 
           170520*m2^4*MBhat^4*Ycut^2 + 30870*m2^5*MBhat^4*Ycut^2 - 
           90720*MBhat^5*Ycut^2 - 718200*m2*MBhat^5*Ycut^2 + 
           407232*m2^2*MBhat^5*Ycut^2 - 169344*m2^3*MBhat^5*Ycut^2 + 
           31752*m2^4*MBhat^5*Ycut^2 + 20475*MBhat^6*Ycut^2 + 
           91770*m2*MBhat^6*Ycut^2 - 55860*m2^2*MBhat^6*Ycut^2 + 
           11025*m2^3*MBhat^6*Ycut^2 - 14640*Ycut^3 - 1571400*m2*Ycut^3 - 
           7387368*m2^2*Ycut^3 - 5458380*m2^3*Ycut^3 - 82572*m2^4*Ycut^3 + 
           46620*m2^5*Ycut^3 - 2100*m2^6*Ycut^3 + 86400*MBhat*Ycut^3 + 
           6514560*m2*MBhat*Ycut^3 + 18645120*m2^2*MBhat*Ycut^3 + 
           4250016*m2^3*MBhat*Ycut^3 - 642600*m2^4*MBhat*Ycut^3 + 
           93744*m2^5*MBhat*Ycut^3 - 7560*m2^6*MBhat*Ycut^3 - 
           214620*MBhat^2*Ycut^3 - 10785390*m2*MBhat^2*Ycut^3 - 
           16265844*m2^2*MBhat^2*Ycut^3 + 724269*m2^3*MBhat^2*Ycut^3 - 
           285180*m2^4*MBhat^2*Ycut^3 + 112455*m2^5*MBhat^2*Ycut^3 - 
           14700*m2^6*MBhat^2*Ycut^3 + 291330*MBhat^3*Ycut^3 + 
           9174930*m2*MBhat^3*Ycut^3 + 5011620*m2^2*MBhat^3*Ycut^3 - 
           855516*m2^3*MBhat^3*Ycut^3 - 17094*m2^4*MBhat^3*Ycut^3 + 
           77490*m2^5*MBhat^3*Ycut^3 - 16800*m2^6*MBhat^3*Ycut^3 - 
           241080*MBhat^4*Ycut^3 - 4282320*m2*MBhat^4*Ycut^3 + 
           457800*m2^2*MBhat^4*Ycut^3 - 517860*m2^3*MBhat^4*Ycut^3 + 
           280560*m2^4*MBhat^4*Ycut^3 - 51450*m2^5*MBhat^4*Ycut^3 + 
           119910*MBhat^5*Ycut^3 + 1098090*m2*MBhat^5*Ycut^3 - 
           636678*m2^2*MBhat^5*Ycut^3 + 275478*m2^3*MBhat^5*Ycut^3 - 
           52920*m2^4*MBhat^5*Ycut^3 - 27300*MBhat^6*Ycut^3 - 
           144690*m2*MBhat^6*Ycut^3 + 89880*m2^2*MBhat^6*Ycut^3 - 
           18375*m2^3*MBhat^6*Ycut^3 + 10980*Ycut^4 + 1277520*m2*Ycut^4 + 
           6267552*m2^2*Ycut^4 + 4870572*m2^3*Ycut^4 + 109200*m2^4*Ycut^4 - 
           45780*m2^5*Ycut^4 + 2100*m2^6*Ycut^4 - 64800*MBhat*Ycut^4 - 
           5328720*m2*MBhat*Ycut^4 - 16076880*m2^2*MBhat*Ycut^4 - 
           4040064*m2^3*MBhat*Ycut^4 + 609336*m2^4*MBhat*Ycut^4 - 
           90720*m2^5*MBhat*Ycut^4 + 7560*m2^6*MBhat*Ycut^4 + 
           163170*MBhat^2*Ycut^4 + 8878380*m2*MBhat^2*Ycut^4 + 
           14423451*m2^2*MBhat^2*Ycut^4 - 567840*m2^3*MBhat^2*Ycut^4 + 
           277095*m2^4*MBhat^2*Ycut^4 - 113190*m2^5*MBhat^2*Ycut^4 + 
           14700*m2^6*MBhat^2*Ycut^4 - 214840*MBhat^3*Ycut^4 - 
           7666870*m2*MBhat^3*Ycut^4 - 4668520*m2^2*MBhat^3*Ycut^4 + 
           791644*m2^3*MBhat^3*Ycut^4 + 21280*m2^4*MBhat^3*Ycut^4 - 
           77630*m2^5*MBhat^3*Ycut^4 + 16800*m2^6*MBhat^3*Ycut^4 + 
           169470*MBhat^4*Ycut^4 + 3690960*m2*MBhat^4*Ycut^4 - 
           382200*m2^2*MBhat^4*Ycut^4 + 510300*m2^3*MBhat^4*Ycut^4 - 
           280770*m2^4*MBhat^4*Ycut^4 + 51450*m2^5*MBhat^4*Ycut^4 - 
           84420*MBhat^5*Ycut^4 - 992670*m2*MBhat^5*Ycut^4 + 
           603372*m2^2*MBhat^5*Ycut^4 - 273210*m2^3*MBhat^5*Ycut^4 + 
           52920*m2^4*MBhat^5*Ycut^4 + 20440*MBhat^6*Ycut^4 + 
           136150*m2*MBhat^6*Ycut^4 - 87710*m2^2*MBhat^6*Ycut^4 + 
           18375*m2^3*MBhat^6*Ycut^4 - 4392*Ycut^5 - 570756*m2*Ycut^5 - 
           2964444*m2^2*Ycut^5 - 2471112*m2^3*Ycut^5 - 83832*m2^4*Ycut^5 + 
           26628*m2^5*Ycut^5 - 1260*m2^6*Ycut^5 + 24597*MBhat*Ycut^5 + 
           2408589*m2*MBhat*Ycut^5 + 7744842*m2^2*MBhat*Ycut^5 + 
           2260818*m2^3*MBhat*Ycut^5 - 351351*m2^4*MBhat*Ycut^5 + 
           55377*m2^5*MBhat*Ycut^5 - 4536*m2^6*MBhat*Ycut^5 - 
           74844*MBhat^2*Ycut^5 - 3990504*m2*MBhat^2*Ycut^5 - 
           7301238*m2^2*MBhat^2*Ycut^5 + 268002*m2^3*MBhat^2*Ycut^5 - 
           172788*m2^4*MBhat^2*Ycut^5 + 69090*m2^5*MBhat^2*Ycut^5 - 
           8820*m2^6*MBhat^2*Ycut^5 + 88638*MBhat^3*Ycut^5 + 
           3513842*m2*MBhat^3*Ycut^5 + 2556652*m2^2*MBhat^3*Ycut^5 - 
           454944*m2^3*MBhat^3*Ycut^5 - 8834*m2^4*MBhat^3*Ycut^5 + 
           46718*m2^5*MBhat^3*Ycut^5 - 10080*m2^6*MBhat^3*Ycut^5 - 
           46620*MBhat^4*Ycut^5 - 1807050*m2*MBhat^4*Ycut^5 + 
           192150*m2^2*MBhat^4*Ycut^5 - 303030*m2^3*MBhat^4*Ycut^5 + 
           169050*m2^4*MBhat^4*Ycut^5 - 30870*m2^5*MBhat^4*Ycut^5 + 
           20601*MBhat^5*Ycut^5 + 531111*m2*MBhat^5*Ycut^5 - 
           347277*m2^2*MBhat^5*Ycut^5 + 164493*m2^3*MBhat^5*Ycut^5 - 
           31752*m2^4*MBhat^5*Ycut^5 - 7980*MBhat^6*Ycut^5 - 
           76790*m2*MBhat^6*Ycut^5 + 52220*m2^2*MBhat^6*Ycut^5 - 
           11025*m2^3*MBhat^6*Ycut^5 + 1026*Ycut^6 + 113106*m2*Ycut^6 + 
           660492*m2^2*Ycut^6 + 606900*m2^3*Ycut^6 + 38598*m2^4*Ycut^6 - 
           9198*m2^5*Ycut^6 + 420*m2^6*Ycut^6 + 2778*MBhat*Ycut^6 - 
           527457*m2*MBhat*Ycut^6 - 1700706*m2^2*MBhat*Ycut^6 - 
           700728*m2^3*MBhat*Ycut^6 + 121296*m2^4*MBhat*Ycut^6 - 
           19383*m2^5*MBhat*Ycut^6 + 1512*m2^6*MBhat*Ycut^6 + 
           27762*MBhat^2*Ycut^6 + 776454*m2*MBhat^2*Ycut^6 + 
           1824921*m2^2*MBhat^2*Ycut^6 - 59997*m2^3*MBhat^2*Ycut^6 + 
           58800*m2^4*MBhat^2*Ycut^6 - 23814*m2^5*MBhat^2*Ycut^6 + 
           2940*m2^6*MBhat^2*Ycut^6 - 37068*MBhat^3*Ycut^6 - 
           667744*m2*MBhat^3*Ycut^6 - 773682*m2^2*MBhat^3*Ycut^6 + 
           158214*m2^3*MBhat^3*Ycut^6 - 350*m2^4*MBhat^3*Ycut^6 - 
           15666*m2^5*MBhat^3*Ycut^6 + 3360*m2^6*MBhat^3*Ycut^6 - 
           9912*MBhat^4*Ycut^6 + 427728*m2*MBhat^4*Ycut^6 - 
           43512*m2^2*MBhat^4*Ycut^6 + 96138*m2^3*MBhat^4*Ycut^6 - 
           56742*m2^4*MBhat^4*Ycut^6 + 10290*m2^5*MBhat^4*Ycut^6 + 
           14574*MBhat^5*Ycut^6 - 156975*m2*MBhat^5*Ycut^6 + 
           110628*m2^2*MBhat^5*Ycut^6 - 55419*m2^3*MBhat^5*Ycut^6 + 
           10584*m2^4*MBhat^5*Ycut^6 + 840*MBhat^6*Ycut^6 + 
           24010*m2*MBhat^6*Ycut^6 - 17640*m2^2*MBhat^6*Ycut^6 + 
           3675*m2^3*MBhat^6*Ycut^6 - 1684*Ycut^7 + 6002*m2*Ycut^7 - 
           41836*m2^2*Ycut^7 - 20696*m2^3*Ycut^7 - 11288*m2^4*Ycut^7 + 
           1522*m2^5*Ycut^7 - 60*m2^6*Ycut^7 - 14895*MBhat*Ycut^7 + 
           69000*m2*MBhat*Ycut^7 - 1833*m2^2*MBhat*Ycut^7 + 
           100899*m2^3*MBhat*Ycut^7 - 20040*m2^4*MBhat*Ycut^7 + 
           3165*m2^5*MBhat*Ycut^7 - 216*m2^6*MBhat*Ycut^7 - 
           6048*MBhat^2*Ycut^7 - 16926*m2*MBhat^2*Ycut^7 - 
           89880*m2^2*MBhat^2*Ycut^7 - 7917*m2^3*MBhat^2*Ycut^7 - 
           8232*m2^4*MBhat^2*Ycut^7 + 3738*m2^5*MBhat^2*Ycut^7 - 
           420*m2^6*MBhat^2*Ycut^7 + 36480*MBhat^3*Ycut^7 - 
           55150*m2*MBhat^3*Ycut^7 + 105038*m2^2*MBhat^3*Ycut^7 - 
           25428*m2^3*MBhat^3*Ycut^7 + 1522*m2^4*MBhat^3*Ycut^7 + 
           2278*m2^5*MBhat^3*Ycut^7 - 480*m2^6*MBhat^3*Ycut^7 + 
           252*MBhat^4*Ycut^7 - 13650*m2*MBhat^4*Ycut^7 - 2352*m2^2*MBhat^4*
            Ycut^7 - 11214*m2^3*MBhat^4*Ycut^7 + 8274*m2^4*MBhat^4*Ycut^7 - 
           1470*m2^5*MBhat^4*Ycut^7 - 14805*MBhat^5*Ycut^7 + 
           21840*m2*MBhat^5*Ycut^7 - 13692*m2^2*MBhat^5*Ycut^7 + 
           8169*m2^3*MBhat^5*Ycut^7 - 1512*m2^4*MBhat^5*Ycut^7 + 
           700*MBhat^6*Ycut^7 - 3010*m2*MBhat^6*Ycut^7 + 2660*m2^2*MBhat^6*
            Ycut^7 - 525*m2^3*MBhat^6*Ycut^7 + 3930*Ycut^8 - 
           15100*m2*Ycut^8 + 16144*m2^2*Ycut^8 - 13932*m2^3*Ycut^8 + 
           1450*m2^4*Ycut^8 - 52*m2^5*Ycut^8 + 14400*MBhat*Ycut^8 - 
           32520*m2*MBhat*Ycut^8 + 34632*m2^2*MBhat*Ycut^8 - 
           1809*m2^3*MBhat*Ycut^8 + 516*m2^4*MBhat*Ycut^8 - 
           99*m2^5*MBhat*Ycut^8 - 13545*MBhat^2*Ycut^8 + 29505*m2*MBhat^2*
            Ycut^8 - 33285*m2^2*MBhat^2*Ycut^8 + 4158*m2^3*MBhat^2*Ycut^8 - 
           294*m2^4*MBhat^2*Ycut^8 - 84*m2^5*MBhat^2*Ycut^8 - 
           21900*MBhat^3*Ycut^8 + 29000*m2*MBhat^3*Ycut^8 - 
           1412*m2^2*MBhat^3*Ycut^8 - 240*m2^3*MBhat^3*Ycut^8 - 
           398*m2^4*MBhat^3*Ycut^8 - 10*m2^5*MBhat^3*Ycut^8 + 
           12600*MBhat^4*Ycut^8 - 10500*m2*MBhat^4*Ycut^8 + 
           378*m2^2*MBhat^4*Ycut^8 - 756*m2^3*MBhat^4*Ycut^8 - 
           42*m2^4*MBhat^4*Ycut^8 + 5040*MBhat^5*Ycut^8 - 
           2100*m2*MBhat^5*Ycut^8 - 672*m2^2*MBhat^5*Ycut^8 - 
           63*m2^3*MBhat^5*Ycut^8 - 525*MBhat^6*Ycut^8 - 175*m2*MBhat^6*
            Ycut^8 - 35*m2^2*MBhat^6*Ycut^8 - 4680*Ycut^9 + 11300*m2*Ycut^9 - 
           8676*m2^2*Ycut^9 + 492*m2^3*Ycut^9 + 52*m2^4*Ycut^9 - 
           4395*MBhat*Ycut^9 + 3405*m2*MBhat*Ycut^9 + 4332*m2^2*MBhat*
            Ycut^9 - 417*m2^3*MBhat*Ycut^9 + 99*m2^4*MBhat*Ycut^9 + 
           16590*MBhat^2*Ycut^9 - 20265*m2*MBhat^2*Ycut^9 + 
           1260*m2^2*MBhat^2*Ycut^9 + 378*m2^3*MBhat^2*Ycut^9 + 
           84*m2^4*MBhat^2*Ycut^9 + 360*MBhat^3*Ycut^9 + 590*m2*MBhat^3*
            Ycut^9 + 648*m2^2*MBhat^3*Ycut^9 + 408*m2^3*MBhat^3*Ycut^9 + 
           10*m2^4*MBhat^3*Ycut^9 - 7980*MBhat^4*Ycut^9 + 
           2310*m2*MBhat^4*Ycut^9 + 798*m2^2*MBhat^4*Ycut^9 + 
           42*m2^3*MBhat^4*Ycut^9 - 105*MBhat^5*Ycut^9 + 735*m2*MBhat^5*
            Ycut^9 + 63*m2^2*MBhat^5*Ycut^9 + 210*MBhat^6*Ycut^9 + 
           35*m2*MBhat^6*Ycut^9 + 2810*Ycut^10 - 3698*m2*Ycut^10 + 
           436*m2^2*Ycut^10 - 52*m2^3*Ycut^10 - 2862*MBhat*Ycut^10 + 
           4575*m2*MBhat*Ycut^10 - 102*m2^2*MBhat*Ycut^10 - 
           99*m2^3*MBhat*Ycut^10 - 5607*MBhat^2*Ycut^10 + 
           2688*m2*MBhat^2*Ycut^10 - 462*m2^2*MBhat^2*Ycut^10 - 
           84*m2^3*MBhat^2*Ycut^10 + 4812*MBhat^3*Ycut^10 - 
           856*m2*MBhat^3*Ycut^10 - 418*m2^2*MBhat^3*Ycut^10 - 
           10*m2^3*MBhat^3*Ycut^10 + 1260*MBhat^4*Ycut^10 - 
           840*m2*MBhat^4*Ycut^10 - 42*m2^2*MBhat^4*Ycut^10 - 
           378*MBhat^5*Ycut^10 - 63*m2*MBhat^5*Ycut^10 - 35*MBhat^6*Ycut^10 - 
           564*Ycut^11 + 106*m2*Ycut^11 - 88*m2^2*Ycut^11 + 
           2367*MBhat*Ycut^11 - 1122*m2*MBhat*Ycut^11 + 99*m2^2*MBhat*
            Ycut^11 - 756*MBhat^2*Ycut^11 - 84*m2*MBhat^2*Ycut^11 + 
           84*m2^2*MBhat^2*Ycut^11 - 1362*MBhat^3*Ycut^11 + 
           428*m2*MBhat^3*Ycut^11 + 10*m2^2*MBhat^3*Ycut^11 + 
           252*MBhat^4*Ycut^11 + 42*m2*MBhat^4*Ycut^11 + 63*MBhat^5*Ycut^11 - 
           186*Ycut^12 + 88*m2*Ycut^12 - 300*MBhat*Ycut^12 + 
           90*m2*MBhat*Ycut^12 + 588*MBhat^2*Ycut^12 - 84*m2*MBhat^2*
            Ycut^12 - 60*MBhat^3*Ycut^12 - 10*m2*MBhat^3*Ycut^12 - 
           42*MBhat^4*Ycut^12 + 80*Ycut^13 - 90*MBhat*Ycut^13 + 
           10*MBhat^3*Ycut^13))/(-1 + Ycut)^7 + 
       (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 4*m2^4 + 72*MBhat + 
          432*m2*MBhat + 360*m2^2*MBhat - 132*MBhat^2 - 507*m2*MBhat^2 - 
          172*m2^2*MBhat^2 + 13*m2^3*MBhat^2 + 128*MBhat^3 + 280*m2*MBhat^3 - 
          72*MBhat^4 - 66*m2*MBhat^4 + 8*m2^2*MBhat^4 + 24*MBhat^5 - 
          4*MBhat^6 + m2*MBhat^6)*Log[m2])/2 - 
       (3*m2*(-16 - 140*m2 - 220*m2^2 - 60*m2^3 + 4*m2^4 + 72*MBhat + 
          432*m2*MBhat + 360*m2^2*MBhat - 132*MBhat^2 - 507*m2*MBhat^2 - 
          172*m2^2*MBhat^2 + 13*m2^3*MBhat^2 + 128*MBhat^3 + 280*m2*MBhat^3 - 
          72*MBhat^4 - 66*m2*MBhat^4 + 8*m2^2*MBhat^4 + 24*MBhat^5 - 
          4*MBhat^6 + m2*MBhat^6)*Log[1 - Ycut])/2) + 
     c[VR]^2*(((-1 + m2 + Ycut)*(345 + 12378*m2 - 16497*m2^2 - 49572*m2^3 + 
          6603*m2^4 + 1458*m2^5 - 75*m2^6 - 2160*MBhat - 36432*m2*MBhat + 
          138456*m2^2*MBhat + 75456*m2^3*MBhat - 16944*m2^4*MBhat + 
          3216*m2^5*MBhat - 312*m2^6*MBhat + 5810*MBhat^2 + 
          24150*m2*MBhat^2 - 233730*m2^2*MBhat^2 + 30660*m2^3*MBhat^2 - 
          7350*m2^4*MBhat^2 + 4830*m2^5*MBhat^2 - 770*m2^6*MBhat^2 - 
          8832*MBhat^3 + 23760*m2*MBhat^3 + 114480*m2^2*MBhat^3 - 
          30000*m2^3*MBhat^3 - 3120*m2^4*MBhat^3 + 5952*m2^5*MBhat^3 - 
          1440*m2^6*MBhat^3 + 7735*MBhat^4 - 36890*m2*MBhat^4 + 
          15190*m2^2*MBhat^4 - 20090*m2^3*MBhat^4 + 16975*m2^4*MBhat^4 - 
          3920*m2^5*MBhat^4 - 3528*MBhat^5 + 14112*m2*MBhat^5 - 
          21168*m2^2*MBhat^5 + 14112*m2^3*MBhat^5 - 3528*m2^4*MBhat^5 + 
          630*MBhat^6 - 1050*m2*MBhat^6 + 3990*m2^2*MBhat^6 - 
          1050*m2^3*MBhat^6 - 1380*Ycut - 54207*m2*Ycut + 67371*m2^2*Ycut + 
          227274*m2^3*Ycut - 28098*m2^4*Ycut - 7215*m2^5*Ycut + 
          375*m2^6*Ycut + 8640*MBhat*Ycut + 163728*m2*MBhat*Ycut - 
          602496*m2^2*MBhat*Ycut - 363240*m2^3*MBhat*Ycut + 
          81816*m2^4*MBhat*Ycut - 15768*m2^5*MBhat*Ycut + 
          1560*m2^6*MBhat*Ycut - 23240*MBhat^2*Ycut - 121030*m2*MBhat^2*
           Ycut + 1050560*m2^2*MBhat^2*Ycut - 125650*m2^3*MBhat^2*Ycut + 
          32690*m2^4*MBhat^2*Ycut - 23380*m2^5*MBhat^2*Ycut + 
          3850*m2^6*MBhat^2*Ycut + 35328*MBhat^3*Ycut - 83712*m2*MBhat^3*
           Ycut - 543792*m2^2*MBhat^3*Ycut + 148608*m2^3*MBhat^3*Ycut + 
          11088*m2^4*MBhat^3*Ycut - 28320*m2^5*MBhat^3*Ycut + 
          7200*m2^6*MBhat^3*Ycut - 30940*MBhat^4*Ycut + 150255*m2*MBhat^4*
           Ycut - 50855*m2^2*MBhat^4*Ycut + 87395*m2^3*MBhat^4*Ycut - 
          80955*m2^4*MBhat^4*Ycut + 19600*m2^5*MBhat^4*Ycut + 
          14112*MBhat^5*Ycut - 59976*m2*MBhat^5*Ycut + 95256*m2^2*MBhat^5*
           Ycut - 67032*m2^3*MBhat^5*Ycut + 17640*m2^4*MBhat^5*Ycut - 
          2520*MBhat^6*Ycut + 4830*m2*MBhat^6*Ycut - 18900*m2^2*MBhat^6*
           Ycut + 5250*m2^3*MBhat^6*Ycut + 2070*Ycut^2 + 90873*m2*Ycut^2 - 
          102156*m2^2*Ycut^2 - 404082*m2^3*Ycut^2 + 44520*m2^4*Ycut^2 + 
          14205*m2^5*Ycut^2 - 750*m2^6*Ycut^2 - 12960*MBhat*Ycut^2 - 
          282672*m2*MBhat*Ycut^2 + 999792*m2^2*MBhat*Ycut^2 + 
          686952*m2^3*MBhat*Ycut^2 - 155232*m2^4*MBhat*Ycut^2 + 
          30600*m2^5*MBhat*Ycut^2 - 3120*m2^6*MBhat*Ycut^2 + 
          34860*MBhat^2*Ycut^2 + 233310*m2*MBhat^2*Ycut^2 - 
          1816570*m2^2*MBhat^2*Ycut^2 + 183820*m2^3*MBhat^2*Ycut^2 - 
          53970*m2^4*MBhat^2*Ycut^2 + 44450*m2^5*MBhat^2*Ycut^2 - 
          7700*m2^6*MBhat^2*Ycut^2 - 52992*MBhat^3*Ycut^2 + 
          98496*m2*MBhat^3*Ycut^2 + 1006224*m2^2*MBhat^3*Ycut^2 - 
          289968*m2^3*MBhat^3*Ycut^2 - 10080*m2^4*MBhat^3*Ycut^2 + 
          52320*m2^5*MBhat^3*Ycut^2 - 14400*m2^6*MBhat^3*Ycut^2 + 
          46410*MBhat^4*Ycut^2 - 226905*m2*MBhat^4*Ycut^2 + 
          44590*m2^2*MBhat^4*Ycut^2 - 139545*m2^3*MBhat^4*Ycut^2 + 
          150150*m2^4*MBhat^4*Ycut^2 - 39200*m2^5*MBhat^4*Ycut^2 - 
          21168*MBhat^5*Ycut^2 + 95256*m2*MBhat^5*Ycut^2 - 
          162288*m2^2*MBhat^5*Ycut^2 + 123480*m2^3*MBhat^5*Ycut^2 - 
          35280*m2^4*MBhat^5*Ycut^2 + 3780*MBhat^6*Ycut^2 - 
          8190*m2*MBhat^6*Ycut^2 + 34650*m2^2*MBhat^6*Ycut^2 - 
          10500*m2^3*MBhat^6*Ycut^2 - 1380*Ycut^3 - 70317*m2*Ycut^3 + 
          66927*m2^2*Ycut^3 + 339045*m2^3*Ycut^3 - 30135*m2^4*Ycut^3 - 
          13830*m2^5*Ycut^3 + 750*m2^6*Ycut^3 + 8640*MBhat*Ycut^3 + 
          226608*m2*MBhat*Ycut^3 - 759360*m2^2*MBhat*Ycut^3 - 
          626808*m2^3*MBhat*Ycut^3 + 141960*m2^4*MBhat*Ycut^3 - 
          29040*m2^5*MBhat*Ycut^3 + 3120*m2^6*MBhat*Ycut^3 - 
          23240*MBhat^2*Ycut^3 - 210210*m2*MBhat^2*Ycut^3 + 
          1460060*m2^2*MBhat^2*Ycut^3 - 98560*m2^3*MBhat^2*Ycut^3 + 
          36750*m2^4*MBhat^2*Ycut^3 - 40600*m2^5*MBhat^2*Ycut^3 + 
          7700*m2^6*MBhat^2*Ycut^3 + 35328*MBhat^3*Ycut^3 - 
          34176*m2*MBhat^3*Ycut^3 - 882672*m2^2*MBhat^3*Ycut^3 + 
          272160*m2^3*MBhat^3*Ycut^3 - 6720*m2^4*MBhat^3*Ycut^3 - 
          45120*m2^5*MBhat^3*Ycut^3 + 14400*m2^6*MBhat^3*Ycut^3 - 
          30940*MBhat^4*Ycut^3 + 148925*m2*MBhat^4*Ycut^3 + 
          18165*m2^2*MBhat^4*Ycut^3 + 89950*m2^3*MBhat^4*Ycut^3 - 
          130550*m2^4*MBhat^4*Ycut^3 + 39200*m2^5*MBhat^4*Ycut^3 + 
          14112*MBhat^5*Ycut^3 - 67032*m2*MBhat^5*Ycut^3 + 
          123480*m2^2*MBhat^5*Ycut^3 - 105840*m2^3*MBhat^5*Ycut^3 + 
          35280*m2^4*MBhat^5*Ycut^3 - 2520*MBhat^6*Ycut^3 + 
          6090*m2*MBhat^6*Ycut^3 - 29400*m2^2*MBhat^6*Ycut^3 + 
          10500*m2^3*MBhat^6*Ycut^3 + 345*Ycut^4 + 22533*m2*Ycut^4 - 
          14490*m2^2*Ycut^4 - 123795*m2^3*Ycut^4 + 5670*m2^4*Ycut^4 + 
          6540*m2^5*Ycut^4 - 375*m2^6*Ycut^4 - 2160*MBhat*Ycut^4 - 
          76272*m2*MBhat*Ycut^4 + 232848*m2^2*MBhat*Ycut^4 + 
          261240*m2^3*MBhat*Ycut^4 - 58800*m2^4*MBhat*Ycut^4 + 
          12960*m2^5*MBhat*Ycut^4 - 1560*m2^6*MBhat*Ycut^4 + 
          6335*MBhat^2*Ycut^4 + 83440*m2*MBhat^2*Ycut^4 - 
          501270*m2^2*MBhat^2*Ycut^4 + 3640*m2^3*MBhat^2*Ycut^4 - 
          7525*m2^4*MBhat^2*Ycut^4 + 16450*m2^5*MBhat^2*Ycut^4 - 
          3850*m2^6*MBhat^2*Ycut^4 - 8832*MBhat^3*Ycut^4 - 
          14448*m2*MBhat^3*Ycut^4 + 356160*m2^2*MBhat^3*Ycut^4 - 
          134400*m2^3*MBhat^3*Ycut^4 + 23520*m2^4*MBhat^3*Ycut^4 + 
          15360*m2^5*MBhat^3*Ycut^4 - 7200*m2^6*MBhat^3*Ycut^4 + 
          7000*MBhat^4*Ycut^4 - 31815*m2*MBhat^4*Ycut^4 - 
          54810*m2^2*MBhat^4*Ycut^4 + 5600*m2^3*MBhat^4*Ycut^4 + 
          45675*m2^4*MBhat^4*Ycut^4 - 19600*m2^5*MBhat^4*Ycut^4 - 
          3528*MBhat^5*Ycut^4 + 17640*m2*MBhat^5*Ycut^4 - 
          27720*m2^2*MBhat^5*Ycut^4 + 35280*m2^3*MBhat^5*Ycut^4 - 
          17640*m2^4*MBhat^5*Ycut^4 + 840*MBhat^6*Ycut^4 - 
          1470*m2*MBhat^6*Ycut^4 + 9450*m2^2*MBhat^6*Ycut^4 - 
          5250*m2^3*MBhat^6*Ycut^4 - 1008*m2*Ycut^5 - 1008*m2^2*Ycut^5 + 
          8967*m2^3*Ycut^5 + 1617*m2^4*Ycut^5 - 1083*m2^5*Ycut^5 + 
          75*m2^6*Ycut^5 - 420*MBhat*Ycut^5 + 2352*m2*MBhat*Ycut^5 + 
          504*m2^2*MBhat*Ycut^5 - 36456*m2^3*MBhat*Ycut^5 + 
          8484*m2^4*MBhat*Ycut^5 - 1656*m2^5*MBhat*Ycut^5 + 
          312*m2^6*MBhat*Ycut^5 - 2940*MBhat^2*Ycut^5 - 8715*m2*MBhat^2*
           Ycut^5 + 37065*m2^2*MBhat^2*Ycut^5 + 11235*m2^3*MBhat^2*Ycut^5 - 
          7035*m2^4*MBhat^2*Ycut^5 - 980*m2^5*MBhat^2*Ycut^5 + 
          770*m2^6*MBhat^2*Ycut^5 + 1512*MBhat^3*Ycut^5 + 
          20580*m2*MBhat^3*Ycut^5 - 60900*m2^2*MBhat^3*Ycut^5 + 
          39900*m2^3*MBhat^3*Ycut^5 - 20580*m2^4*MBhat^3*Ycut^5 + 
          1248*m2^5*MBhat^3*Ycut^5 + 1440*m2^6*MBhat^3*Ycut^5 + 
          2940*MBhat^4*Ycut^5 - 9975*m2*MBhat^4*Ycut^5 + 41475*m2^2*MBhat^4*
           Ycut^5 - 39165*m2^3*MBhat^4*Ycut^5 + 2625*m2^4*MBhat^4*Ycut^5 + 
          3920*m2^5*MBhat^4*Ycut^5 - 252*MBhat^5*Ycut^5 - 
          252*m2*MBhat^5*Ycut^5 - 16632*m2^2*MBhat^5*Ycut^5 + 
          3528*m2^3*MBhat^5*Ycut^5 + 3528*m2^4*MBhat^5*Ycut^5 - 
          840*MBhat^6*Ycut^5 - 630*m2*MBhat^6*Ycut^5 + 1260*m2^2*MBhat^6*
           Ycut^5 + 1050*m2^3*MBhat^6*Ycut^5 + 105*Ycut^6 + 252*m2*Ycut^6 - 
          2016*m2^2*Ycut^6 + 3591*m2^3*Ycut^6 - 777*m2^4*Ycut^6 - 
          75*m2^5*Ycut^6 + 2520*MBhat*Ycut^6 + 4452*m2*MBhat*Ycut^6 - 
          16044*m2^2*MBhat*Ycut^6 + 3780*m2^3*MBhat*Ycut^6 + 
          1764*m2^4*MBhat*Ycut^6 - 312*m2^5*MBhat*Ycut^6 + 
          5425*MBhat^2*Ycut^6 - 7455*m2*MBhat^2*Ycut^6 + 6510*m2^2*MBhat^2*
           Ycut^6 - 4235*m2^3*MBhat^2*Ycut^6 + 4725*m2^4*MBhat^2*Ycut^6 - 
          770*m2^5*MBhat^2*Ycut^6 - 6048*MBhat^3*Ycut^6 - 
          16968*m2*MBhat^3*Ycut^6 + 15372*m2^2*MBhat^3*Ycut^6 - 
          6888*m2^3*MBhat^3*Ycut^6 + 7812*m2^4*MBhat^3*Ycut^6 - 
          1440*m2^5*MBhat^3*Ycut^6 - 4270*MBhat^4*Ycut^6 + 
          12845*m2*MBhat^4*Ycut^6 - 15190*m2^2*MBhat^4*Ycut^6 + 
          18235*m2^3*MBhat^4*Ycut^6 - 3920*m2^4*MBhat^4*Ycut^6 + 
          1008*MBhat^5*Ycut^6 + 756*m2*MBhat^5*Ycut^6 + 10584*m2^2*MBhat^5*
           Ycut^6 - 3528*m2^3*MBhat^5*Ycut^6 + 1260*MBhat^6*Ycut^6 + 
          630*m2*MBhat^6*Ycut^6 - 1050*m2^2*MBhat^6*Ycut^6 - 660*Ycut^7 - 
          1143*m2*Ycut^7 + 3981*m2^2*Ycut^7 - 1668*m2^3*Ycut^7 - 
          240*m2^4*Ycut^7 - 5610*MBhat*Ycut^7 - 318*m2*MBhat*Ycut^7 + 
          6738*m2^2*MBhat*Ycut^7 - 1242*m2^3*MBhat*Ycut^7 - 
          528*m2^4*MBhat*Ycut^7 - 2800*MBhat^2*Ycut^7 + 13090*m2*MBhat^2*
           Ycut^7 - 4340*m2^2*MBhat^2*Ycut^7 - 1155*m2^3*MBhat^2*Ycut^7 - 
          805*m2^4*MBhat^2*Ycut^7 + 9042*MBhat^3*Ycut^7 + 
          4674*m2*MBhat^3*Ycut^7 - 5994*m2^2*MBhat^3*Ycut^7 + 
          558*m2^3*MBhat^3*Ycut^7 - 1080*m2^4*MBhat^3*Ycut^7 + 
          2380*MBhat^4*Ycut^7 - 8505*m2*MBhat^4*Ycut^7 + 1295*m2^2*MBhat^4*
           Ycut^7 - 2380*m2^3*MBhat^4*Ycut^7 - 1512*MBhat^5*Ycut^7 - 
          756*m2*MBhat^5*Ycut^7 - 1512*m2^2*MBhat^5*Ycut^7 - 
          840*MBhat^6*Ycut^7 - 210*m2*MBhat^6*Ycut^7 + 1590*Ycut^8 + 
          657*m2*Ycut^8 - 2397*m2^2*Ycut^8 + 240*m2^3*Ycut^8 + 
          5640*MBhat*Ycut^8 - 3918*m2*MBhat*Ycut^8 - 120*m2^2*MBhat*Ycut^8 + 
          318*m2^3*MBhat*Ycut^8 - 2625*MBhat^2*Ycut^8 - 6930*m2*MBhat^2*
           Ycut^8 + 1960*m2^2*MBhat^2*Ycut^8 + 245*m2^3*MBhat^2*Ycut^8 - 
          5928*MBhat^3*Ycut^8 + 2946*m2*MBhat^3*Ycut^8 + 1152*m2^2*MBhat^3*
           Ycut^8 + 30*m2^3*MBhat^3*Ycut^8 + 105*MBhat^4*Ycut^8 + 
          2205*m2*MBhat^4*Ycut^8 + 140*m2^2*MBhat^4*Ycut^8 + 
          1008*MBhat^5*Ycut^8 + 252*m2*MBhat^5*Ycut^8 + 210*MBhat^6*Ycut^8 - 
          1860*Ycut^9 + 267*m2*Ycut^9 + 285*m2^2*Ycut^9 - 2160*MBhat*Ycut^9 + 
          2742*m2*MBhat*Ycut^9 - 318*m2^2*MBhat*Ycut^9 + 
          3500*MBhat^2*Ycut^9 + 105*m2*MBhat^2*Ycut^9 - 245*m2^2*MBhat^2*
           Ycut^9 + 1332*MBhat^3*Ycut^9 - 1182*m2*MBhat^3*Ycut^9 - 
          30*m2^2*MBhat^3*Ycut^9 - 560*MBhat^4*Ycut^9 - 140*m2*MBhat^4*
           Ycut^9 - 252*MBhat^5*Ycut^9 + 1065*Ycut^10 - 285*m2*Ycut^10 - 
          240*MBhat*Ycut^10 - 270*m2*MBhat*Ycut^10 - 1085*MBhat^2*Ycut^10 + 
          245*m2*MBhat^2*Ycut^10 + 120*MBhat^3*Ycut^10 + 
          30*m2*MBhat^3*Ycut^10 + 140*MBhat^4*Ycut^10 - 240*Ycut^11 + 
          270*MBhat*Ycut^11 - 30*MBhat^3*Ycut^11))/(420*(-1 + Ycut)^5) + 
       m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 48*MBhat + 
         72*m2*MBhat + 360*m2^2*MBhat + 72*MBhat^2 - 276*m2*MBhat^2 - 
         274*m2^2*MBhat^2 + 58*m2^3*MBhat^2 - 48*MBhat^3 + 288*m2*MBhat^3 + 
         12*MBhat^4 - 105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 6*m2*MBhat^6)*
        Log[m2] - m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 48*MBhat + 
         72*m2*MBhat + 360*m2^2*MBhat + 72*MBhat^2 - 276*m2*MBhat^2 - 
         274*m2^2*MBhat^2 + 58*m2^3*MBhat^2 - 48*MBhat^3 + 288*m2*MBhat^3 + 
         12*MBhat^4 - 105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 6*m2*MBhat^6)*
        Log[1 - Ycut]) + c[VL]^2*
      (-1/420*((-1 + m2 + Ycut)*(-345 - 12378*m2 + 16497*m2^2 + 49572*m2^3 - 
           6603*m2^4 - 1458*m2^5 + 75*m2^6 + 2160*MBhat + 36432*m2*MBhat - 
           138456*m2^2*MBhat - 75456*m2^3*MBhat + 16944*m2^4*MBhat - 
           3216*m2^5*MBhat + 312*m2^6*MBhat - 5810*MBhat^2 - 
           24150*m2*MBhat^2 + 233730*m2^2*MBhat^2 - 30660*m2^3*MBhat^2 + 
           7350*m2^4*MBhat^2 - 4830*m2^5*MBhat^2 + 770*m2^6*MBhat^2 + 
           8832*MBhat^3 - 23760*m2*MBhat^3 - 114480*m2^2*MBhat^3 + 
           30000*m2^3*MBhat^3 + 3120*m2^4*MBhat^3 - 5952*m2^5*MBhat^3 + 
           1440*m2^6*MBhat^3 - 7735*MBhat^4 + 36890*m2*MBhat^4 - 
           15190*m2^2*MBhat^4 + 20090*m2^3*MBhat^4 - 16975*m2^4*MBhat^4 + 
           3920*m2^5*MBhat^4 + 3528*MBhat^5 - 14112*m2*MBhat^5 + 
           21168*m2^2*MBhat^5 - 14112*m2^3*MBhat^5 + 3528*m2^4*MBhat^5 - 
           630*MBhat^6 + 1050*m2*MBhat^6 - 3990*m2^2*MBhat^6 + 
           1050*m2^3*MBhat^6 + 2070*Ycut + 78963*m2*Ycut - 100365*m2^2*Ycut - 
           326418*m2^3*Ycut + 41304*m2^4*Ycut + 10131*m2^5*Ycut - 
           525*m2^6*Ycut - 12960*MBhat*Ycut - 236592*m2*MBhat*Ycut + 
           879408*m2^2*MBhat*Ycut + 514152*m2^3*MBhat*Ycut - 
           115704*m2^4*MBhat*Ycut + 22200*m2^5*MBhat*Ycut - 
           2184*m2^6*MBhat*Ycut + 34860*MBhat^2*Ycut + 169330*m2*MBhat^2*
            Ycut - 1518020*m2^2*MBhat^2*Ycut + 186970*m2^3*MBhat^2*Ycut - 
           47390*m2^4*MBhat^2*Ycut + 33040*m2^5*MBhat^2*Ycut - 
           5390*m2^6*MBhat^2*Ycut - 52992*MBhat^3*Ycut + 131232*m2*MBhat^3*
            Ycut + 772752*m2^2*MBhat^3*Ycut - 208608*m2^3*MBhat^3*Ycut - 
           17328*m2^4*MBhat^3*Ycut + 40224*m2^5*MBhat^3*Ycut - 
           10080*m2^6*MBhat^3*Ycut + 46410*MBhat^4*Ycut - 224035*m2*MBhat^4*
            Ycut + 81235*m2^2*MBhat^4*Ycut - 127575*m2^3*MBhat^4*Ycut + 
           114905*m2^4*MBhat^4*Ycut - 27440*m2^5*MBhat^4*Ycut - 
           21168*MBhat^5*Ycut + 88200*m2*MBhat^5*Ycut - 137592*m2^2*MBhat^5*
            Ycut + 95256*m2^3*MBhat^5*Ycut - 24696*m2^4*MBhat^5*Ycut + 
           3780*MBhat^6*Ycut - 6930*m2*MBhat^6*Ycut + 26880*m2^2*MBhat^6*
            Ycut - 7350*m2^3*MBhat^6*Ycut - 5175*Ycut^2 - 211665*m2*Ycut^2 + 
           253395*m2^2*Ycut^2 + 908202*m2^3*Ycut^2 - 107319*m2^4*Ycut^2 - 
           30093*m2^5*Ycut^2 + 1575*m2^6*Ycut^2 + 32400*MBhat*Ycut^2 + 
           646560*m2*MBhat*Ycut^2 - 2343240*m2^2*MBhat*Ycut^2 - 
           1488888*m2^3*MBhat*Ycut^2 + 335808*m2^4*MBhat*Ycut^2 - 
           65352*m2^5*MBhat*Ycut^2 + 6552*m2^6*MBhat*Ycut^2 - 
           87150*MBhat^2*Ycut^2 - 499520*m2*MBhat^2*Ycut^2 + 
           4151420*m2^2*MBhat^2*Ycut^2 - 465780*m2^3*MBhat^2*Ycut^2 + 
           126700*m2^4*MBhat^2*Ycut^2 - 96040*m2^5*MBhat^2*Ycut^2 + 
           16170*m2^6*MBhat^2*Ycut^2 + 132480*MBhat^3*Ycut^2 - 
           289680*m2*MBhat^3*Ycut^2 - 2208288*m2^2*MBhat^3*Ycut^2 + 
           617184*m2^3*MBhat^3*Ycut^2 + 35376*m2^4*MBhat^3*Ycut^2 - 
           114912*m2^5*MBhat^3*Ycut^2 + 30240*m2^6*MBhat^3*Ycut^2 - 
           116025*MBhat^4*Ycut^2 + 564305*m2*MBhat^4*Ycut^2 - 
           161490*m2^2*MBhat^4*Ycut^2 + 334425*m2^3*MBhat^4*Ycut^2 - 
           329035*m2^4*MBhat^4*Ycut^2 + 82320*m2^5*MBhat^4*Ycut^2 + 
           52920*MBhat^5*Ycut^2 - 229320*m2*MBhat^5*Ycut^2 + 
           373968*m2^2*MBhat^5*Ycut^2 - 271656*m2^3*MBhat^5*Ycut^2 + 
           74088*m2^4*MBhat^5*Ycut^2 - 9450*MBhat^6*Ycut^2 + 
           18900*m2*MBhat^6*Ycut^2 - 76440*m2^2*MBhat^6*Ycut^2 + 
           22050*m2^3*MBhat^6*Ycut^2 + 6900*Ycut^3 + 306270*m2*Ycut^3 - 
           338610*m2^2*Ycut^3 - 1374483*m2^3*Ycut^3 + 147273*m2^4*Ycut^3 + 
           49455*m2^5*Ycut^3 - 2625*m2^6*Ycut^3 - 43200*MBhat*Ycut^3 - 
           955680*m2*MBhat*Ycut^3 + 3361440*m2^2*MBhat*Ycut^3 + 
           2363952*m2^3*MBhat*Ycut^3 - 534240*m2^4*MBhat*Ycut^3 + 
           106008*m2^5*MBhat*Ycut^3 - 10920*m2^6*MBhat*Ycut^3 + 
           116200*MBhat^2*Ycut^3 + 797860*m2*MBhat^2*Ycut^3 - 
           6143760*m2^2*MBhat^2*Ycut^3 + 591850*m2^3*MBhat^2*Ycut^3 - 
           177380*m2^4*MBhat^2*Ycut^3 + 152880*m2^5*MBhat^2*Ycut^3 - 
           26950*m2^6*MBhat^2*Ycut^3 - 178740*MBhat^3*Ycut^3 + 
           315300*m2*MBhat^3*Ycut^3 + 3441432*m2^2*MBhat^3*Ycut^3 - 
           989784*m2^3*MBhat^3*Ycut^3 - 45108*m2^4*MBhat^3*Ycut^3 + 
           186900*m2^5*MBhat^3*Ycut^3 - 50400*m2^6*MBhat^3*Ycut^3 + 
           158060*MBhat^4*Ycut^3 - 749630*m2*MBhat^4*Ycut^3 + 
           125230*m2^2*MBhat^4*Ycut^3 - 486675*m2^3*MBhat^4*Ycut^3 + 
           531965*m2^4*MBhat^4*Ycut^3 - 137200*m2^5*MBhat^4*Ycut^3 - 
           71820*MBhat^5*Ycut^3 + 314580*m2*MBhat^5*Ycut^3 - 
           553812*m2^2*MBhat^5*Ycut^3 + 434532*m2^3*MBhat^5*Ycut^3 - 
           123480*m2^4*MBhat^5*Ycut^3 + 12600*MBhat^6*Ycut^3 - 
           27300*m2*MBhat^6*Ycut^3 + 120960*m2^2*MBhat^6*Ycut^3 - 
           36750*m2^3*MBhat^6*Ycut^3 - 5175*Ycut^4 - 254040*m2*Ycut^4 + 
           250500*m2^2*Ycut^4 + 1205967*m2^3*Ycut^4 - 110460*m2^4*Ycut^4 - 
           48405*m2^5*Ycut^4 + 2625*m2^6*Ycut^4 + 32400*MBhat*Ycut^4 + 
           812160*m2*MBhat*Ycut^4 - 2751360*m2^2*MBhat*Ycut^4 - 
           2201808*m2^3*MBhat*Ycut^4 + 497952*m2^4*MBhat*Ycut^4 - 
           101640*m2^5*MBhat*Ycut^4 + 10920*m2^6*MBhat*Ycut^4 - 
           84525*MBhat^2*Ycut^4 - 737800*m2*MBhat^2*Ycut^4 + 
           5234180*m2^2*MBhat^2*Ycut^4 - 400960*m2^3*MBhat^2*Ycut^4 + 
           165865*m2^4*MBhat^2*Ycut^4 - 155330*m2^5*MBhat^2*Ycut^4 + 
           26950*m2^6*MBhat^2*Ycut^4 + 141160*MBhat^3*Ycut^4 - 
           160940*m2*MBhat^3*Ycut^4 - 3148448*m2^2*MBhat^3*Ycut^4 + 
           978488*m2^3*MBhat^3*Ycut^4 + 6440*m2^4*MBhat^3*Ycut^4 - 
           180460*m2^5*MBhat^3*Ycut^4 + 50400*m2^6*MBhat^3*Ycut^4 - 
           134820*MBhat^4*Ycut^4 + 542500*m2*MBhat^4*Ycut^4 + 
           54740*m2^2*MBhat^4*Ycut^4 + 400995*m2^3*MBhat^4*Ycut^4 - 
           518665*m2^4*MBhat^4*Ycut^4 + 137200*m2^5*MBhat^4*Ycut^4 + 
           60480*MBhat^5*Ycut^4 - 230580*m2*MBhat^5*Ycut^4 + 
           475608*m2^2*MBhat^5*Ycut^4 - 422940*m2^3*MBhat^5*Ycut^4 + 
           123480*m2^4*MBhat^5*Ycut^4 - 9520*MBhat^6*Ycut^4 + 
           21980*m2*MBhat^6*Ycut^4 - 116620*m2^2*MBhat^6*Ycut^4 + 
           36750*m2^3*MBhat^6*Ycut^4 + 2070*Ycut^5 + 116391*m2*Ycut^5 - 
           94899*m2^2*Ycut^5 - 595602*m2^3*Ycut^5 + 39858*m2^4*Ycut^5 + 
           27993*m2^5*Ycut^5 - 1575*m2^6*Ycut^5 - 14430*MBhat*Ycut^5 - 
           381126*m2*MBhat*Ycut^5 + 1226820*m2^2*MBhat*Ycut^5 + 
           1195572*m2^3*MBhat*Ycut^5 - 286566*m2^4*MBhat*Ycut^5 + 
           64554*m2^5*MBhat*Ycut^5 - 6552*m2^6*MBhat*Ycut^5 + 
           21294*MBhat^2*Ycut^5 + 394079*m2*MBhat^2*Ycut^5 - 
           2473121*m2^2*MBhat^2*Ycut^5 + 108969*m2^3*MBhat^2*Ycut^5 - 
           98441*m2^4*MBhat^2*Ycut^5 + 96530*m2^5*MBhat^2*Ycut^5 - 
           16170*m2^6*MBhat^2*Ycut^5 - 61728*MBhat^3*Ycut^5 + 
           20680*m2*MBhat^3*Ycut^5 + 1690052*m2^2*MBhat^3*Ycut^5 - 
           657300*m2^3*MBhat^3*Ycut^5 + 60620*m2^4*MBhat^3*Ycut^5 + 
           101836*m2^5*MBhat^3*Ycut^5 - 30240*m2^6*MBhat^3*Ycut^5 + 
           88620*MBhat^4*Ycut^5 - 184730*m2*MBhat^4*Ycut^5 - 
           225120*m2^2*MBhat^4*Ycut^5 - 134295*m2^3*MBhat^4*Ycut^5 + 
           298655*m2^4*MBhat^4*Ycut^5 - 82320*m2^5*MBhat^4*Ycut^5 - 
           40026*MBhat^5*Ycut^5 + 64554*m2*MBhat^5*Ycut^5 - 
           217098*m2^2*MBhat^5*Ycut^5 + 247842*m2^3*MBhat^5*Ycut^5 - 
           74088*m2^4*MBhat^5*Ycut^5 + 4200*MBhat^6*Ycut^5 - 
           9100*m2*MBhat^6*Ycut^5 + 69160*m2^2*MBhat^6*Ycut^5 - 
           22050*m2^3*MBhat^6*Ycut^5 - 30*Ycut^6 - 24885*m2*Ycut^6 + 
           13986*m2^2*Ycut^6 + 135954*m2^3*Ycut^6 + 2457*m2^4*Ycut^6 - 
           10395*m2^5*Ycut^6 + 525*m2^6*Ycut^6 + 10476*MBhat*Ycut^6 + 
           72702*m2*MBhat*Ycut^6 - 230580*m2^2*MBhat*Ycut^6 - 
           352128*m2^3*MBhat*Ycut^6 + 103656*m2^4*MBhat*Ycut^6 - 
           24486*m2^5*MBhat*Ycut^6 + 2184*m2^6*MBhat*Ycut^6 + 
           20664*MBhat^2*Ycut^6 - 125965*m2*MBhat^2*Ycut^6 + 
           515634*m2^2*MBhat^2*Ycut^6 + 55573*m2^3*MBhat^2*Ycut^6 + 
           20482*m2^4*MBhat^2*Ycut^6 - 34398*m2^5*MBhat^2*Ycut^6 + 
           5390*m2^6*MBhat^2*Ycut^6 - 3432*MBhat^3*Ycut^6 + 
           7252*m2*MBhat^3*Ycut^6 - 488796*m2^2*MBhat^3*Ycut^6 + 
           308784*m2^3*MBhat^3*Ycut^6 - 73276*m2^4*MBhat^3*Ycut^6 - 
           29652*m2^5*MBhat^3*Ycut^6 + 10080*m2^6*MBhat^3*Ycut^6 - 
           54474*MBhat^4*Ycut^6 + 3346*m2*MBhat^4*Ycut^6 + 
           240016*m2^2*MBhat^4*Ycut^6 - 51009*m2^3*MBhat^4*Ycut^6 - 
           91189*m2^4*MBhat^4*Ycut^6 + 27440*m2^5*MBhat^4*Ycut^6 + 
           28476*MBhat^5*Ycut^6 + 28350*m2*MBhat^5*Ycut^6 + 
           21672*m2^2*MBhat^5*Ycut^6 - 79086*m2^3*MBhat^5*Ycut^6 + 
           24696*m2^4*MBhat^5*Ycut^6 - 1680*MBhat^6*Ycut^6 + 
           980*m2*MBhat^6*Ycut^6 - 23520*m2^2*MBhat^6*Ycut^6 + 
           7350*m2^3*MBhat^6*Ycut^6 - 1874*Ycut^7 + 3355*m2*Ycut^7 - 
           3869*m2^2*Ycut^7 + 3635*m2^3*Ycut^7 - 8923*m2^4*Ycut^7 + 
           2081*m2^5*Ycut^7 - 75*m2^6*Ycut^7 - 19044*MBhat*Ycut^7 + 
           20562*m2*MBhat*Ycut^7 - 8292*m2^2*MBhat*Ycut^7 + 
           41100*m2^3*MBhat*Ycut^7 - 18624*m2^4*MBhat*Ycut^7 + 
           4770*m2^5*MBhat*Ycut^7 - 312*m2^6*MBhat*Ycut^7 - 
           20958*MBhat^2*Ycut^7 + 41153*m2*MBhat^2*Ycut^7 + 
           15827*m2^2*MBhat^2*Ycut^7 - 63910*m2^3*MBhat^2*Ycut^7 + 
           6202*m2^4*MBhat^2*Ycut^7 + 5866*m2^5*MBhat^2*Ycut^7 - 
           770*m2^6*MBhat^2*Ycut^7 + 35562*MBhat^3*Ycut^7 - 
           2798*m2*MBhat^3*Ycut^7 + 47266*m2^2*MBhat^3*Ycut^7 - 
           95310*m2^3*MBhat^3*Ycut^7 + 39524*m2^4*MBhat^3*Ycut^7 + 
           2396*m2^5*MBhat^3*Ycut^7 - 1440*m2^6*MBhat^3*Ycut^7 + 
           23184*MBhat^4*Ycut^7 + 12040*m2*MBhat^4*Ycut^7 - 
           131614*m2^2*MBhat^4*Ycut^7 + 62307*m2^3*MBhat^4*Ycut^7 + 
           9443*m2^4*MBhat^4*Ycut^7 - 3920*m2^5*MBhat^4*Ycut^7 - 
           18270*MBhat^5*Ycut^7 - 31080*m2*MBhat^5*Ycut^7 + 
           25872*m2^2*MBhat^5*Ycut^7 + 9786*m2^3*MBhat^5*Ycut^7 - 
           3528*m2^4*MBhat^5*Ycut^7 + 1400*MBhat^6*Ycut^7 + 
           700*m2*MBhat^6*Ycut^7 + 3640*m2^2*MBhat^6*Ycut^7 - 
           1050*m2^3*MBhat^6*Ycut^7 + 4629*Ycut^8 - 5435*m2*Ycut^8 + 
           5711*m2^2*Ycut^8 - 7839*m2^3*Ycut^8 + 2453*m2^4*Ycut^8 - 
           149*m2^5*Ycut^8 + 21948*MBhat*Ycut^8 - 25530*m2*MBhat*Ycut^8 + 
           2928*m2^2*MBhat*Ycut^8 + 5388*m2^3*MBhat*Ycut^8 + 
           624*m2^4*MBhat*Ycut^8 - 318*m2^5*MBhat*Ycut^8 - 
           1260*MBhat^2*Ycut^8 - 18865*m2*MBhat^2*Ycut^8 - 
           12698*m2^2*MBhat^2*Ycut^8 + 19572*m2^3*MBhat^2*Ycut^8 - 
           3941*m2^4*MBhat^2*Ycut^8 - 238*m2^5*MBhat^2*Ycut^8 - 
           31092*MBhat^3*Ycut^8 + 6850*m2*MBhat^3*Ycut^8 + 
           11696*m2^2*MBhat^3*Ycut^8 + 18306*m2^3*MBhat^3*Ycut^8 - 
           10420*m2^4*MBhat^3*Ycut^8 + 460*m2^5*MBhat^3*Ycut^8 + 
           105*MBhat^4*Ycut^8 + 490*m2*MBhat^4*Ycut^8 + 35826*m2^2*MBhat^4*
            Ycut^8 - 20517*m2^3*MBhat^4*Ycut^8 + 896*m2^4*MBhat^4*Ycut^8 + 
           6720*MBhat^5*Ycut^8 + 10920*m2*MBhat^5*Ycut^8 - 
           11088*m2^2*MBhat^5*Ycut^8 + 378*m2^3*MBhat^5*Ycut^8 - 
           1050*MBhat^6*Ycut^8 - 350*m2*MBhat^6*Ycut^8 - 70*m2^2*MBhat^6*
            Ycut^8 - 6060*Ycut^9 + 5830*m2*Ycut^9 - 2844*m2^2*Ycut^9 + 
           972*m2^3*Ycut^9 - 40*m2^4*Ycut^9 - 12240*MBhat*Ycut^9 + 
           13470*m2*MBhat*Ycut^9 + 1068*m2^2*MBhat*Ycut^9 - 
           1944*m2^3*MBhat*Ycut^9 + 150*m2^4*MBhat*Ycut^9 + 
           12950*MBhat^2*Ycut^9 + 2975*m2*MBhat^2*Ycut^9 - 
           4263*m2^2*MBhat^2*Ycut^9 - 1631*m2^3*MBhat^2*Ycut^9 + 
           553*m2^4*MBhat^2*Ycut^9 + 10110*MBhat^3*Ycut^9 - 
           5300*m2*MBhat^3*Ycut^9 - 3264*m2^2*MBhat^3*Ycut^9 - 
           1758*m2^3*MBhat^3*Ycut^9 + 1052*m2^4*MBhat^3*Ycut^9 - 
           4550*MBhat^4*Ycut^9 - 1435*m2*MBhat^4*Ycut^9 - 3619*m2^2*MBhat^4*
            Ycut^9 + 2254*m2^3*MBhat^4*Ycut^9 - 630*MBhat^5*Ycut^9 - 
           1470*m2*MBhat^5*Ycut^9 + 1302*m2^2*MBhat^5*Ycut^9 + 
           420*MBhat^6*Ycut^9 + 70*m2*MBhat^6*Ycut^9 + 4405*Ycut^10 - 
           3058*m2*Ycut^10 + 503*m2^2*Ycut^10 + 40*m2^3*Ycut^10 + 
           1620*MBhat*Ycut^10 - 2886*m2*MBhat*Ycut^10 + 324*m2^2*MBhat*
            Ycut^10 + 60*m2^3*MBhat*Ycut^10 - 7434*MBhat^2*Ycut^10 + 
           1141*m2*MBhat^2*Ycut^10 + 1078*m2^2*MBhat^2*Ycut^10 + 
           7*m2^3*MBhat^2*Ycut^10 + 576*MBhat^3*Ycut^10 + 
           1240*m2*MBhat^3*Ycut^10 + 76*m2^2*MBhat^3*Ycut^10 - 
           2*m2^3*MBhat^3*Ycut^10 + 1155*MBhat^4*Ycut^10 + 
           245*m2*MBhat^4*Ycut^10 - 14*m2^2*MBhat^4*Ycut^10 - 
           252*MBhat^5*Ycut^10 - 42*m2*MBhat^5*Ycut^10 - 70*MBhat^6*Ycut^10 - 
           1650*Ycut^11 + 647*m2*Ycut^11 - 5*m2^2*Ycut^11 + 
           1284*MBhat*Ycut^11 - 90*m2*MBhat*Ycut^11 - 60*m2^2*MBhat*Ycut^11 + 
           966*MBhat^2*Ycut^11 - 245*m2*MBhat^2*Ycut^11 - 
           7*m2^2*MBhat^2*Ycut^11 - 726*MBhat^3*Ycut^11 - 
           74*m2*MBhat^3*Ycut^11 + 2*m2^2*MBhat^3*Ycut^11 + 
           84*MBhat^4*Ycut^11 + 14*m2*MBhat^4*Ycut^11 + 42*MBhat^5*Ycut^11 + 
           219*Ycut^12 + 5*m2*Ycut^12 - 396*MBhat*Ycut^12 + 
           18*m2*MBhat*Ycut^12 + 203*MBhat^2*Ycut^12 + 7*m2*MBhat^2*Ycut^12 - 
           12*MBhat^3*Ycut^12 - 2*m2*MBhat^3*Ycut^12 - 14*MBhat^4*Ycut^12 + 
           16*Ycut^13 - 18*MBhat*Ycut^13 + 2*MBhat^3*Ycut^13))/
         (-1 + Ycut)^7 + m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 
         48*MBhat + 72*m2*MBhat + 360*m2^2*MBhat + 72*MBhat^2 - 
         276*m2*MBhat^2 - 274*m2^2*MBhat^2 + 58*m2^3*MBhat^2 - 48*MBhat^3 + 
         288*m2*MBhat^3 + 12*MBhat^4 - 105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 
         6*m2*MBhat^6)*Log[m2] - m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 
         15*m2^4 - 48*MBhat + 72*m2*MBhat + 360*m2^2*MBhat + 72*MBhat^2 - 
         276*m2*MBhat^2 - 274*m2^2*MBhat^2 + 58*m2^3*MBhat^2 - 48*MBhat^3 + 
         288*m2*MBhat^3 + 12*MBhat^4 - 105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 
         6*m2*MBhat^6)*Log[1 - Ycut]) + 
     c[T]^2*((-2*(-1 + m2 + Ycut)*(-582 - 27588*m2 - 25698*m2^2 + 
          34152*m2^3 - 8898*m2^4 - 1716*m2^5 + 90*m2^6 + 4160*MBhat + 
          115880*m2*MBhat - 42712*m2^2*MBhat - 80512*m2^3*MBhat + 
          20288*m2^4*MBhat - 4072*m2^5*MBhat + 408*m2^6*MBhat - 
          13167*MBhat^2 - 188867*m2*MBhat^2 + 192178*m2^2*MBhat^2 - 
          34622*m2^3*MBhat^2 + 8953*m2^4*MBhat^2 - 6755*m2^5*MBhat^2 + 
          1120*m2^6*MBhat^2 + 24240*MBhat^3 + 142512*m2*MBhat^3 - 
          138888*m2^2*MBhat^3 + 45912*m2^3*MBhat^3 + 3912*m2^4*MBhat^3 - 
          9528*m2^5*MBhat^3 + 2400*m2^6*MBhat^3 - 25130*MBhat^4 - 
          50120*m2*MBhat^4 + 1330*m2^2*MBhat^4 + 21490*m2^3*MBhat^4 - 
          25340*m2^4*MBhat^4 + 6370*m2^5*MBhat^4 + 13104*MBhat^5 + 
          10584*m2*MBhat^5 + 20664*m2^2*MBhat^5 - 19656*m2^3*MBhat^5 + 
          5544*m2^4*MBhat^5 - 2625*MBhat^6 - 2625*m2*MBhat^6 - 
          5145*m2^2*MBhat^6 + 1575*m2^3*MBhat^6 + 3492*Ycut + 
          175026*m2*Ycut + 173898*m2^2*Ycut - 223500*m2^3*Ycut + 
          56352*m2^4*Ycut + 11922*m2^5*Ycut - 630*m2^6*Ycut - 
          24960*MBhat*Ycut - 741520*m2*MBhat*Ycut + 245272*m2^2*MBhat*Ycut + 
          546960*m2^3*MBhat*Ycut - 138352*m2^4*MBhat*Ycut + 
          28096*m2^5*MBhat*Ycut - 2856*m2^6*MBhat*Ycut + 79002*MBhat^2*Ycut + 
          1225875*m2*MBhat^2*Ycut - 1225322*m2^2*MBhat^2*Ycut + 
          206696*m2^3*MBhat^2*Ycut - 57036*m2^4*MBhat^2*Ycut + 
          46165*m2^5*MBhat^2*Ycut - 7840*m2^6*MBhat^2*Ycut - 
          145440*MBhat^3*Ycut - 951792*m2*MBhat^3*Ycut + 929520*m2^2*MBhat^3*
           Ycut - 318168*m2^3*MBhat^3*Ycut - 20256*m2^4*MBhat^3*Ycut + 
          64296*m2^5*MBhat^3*Ycut - 16800*m2^6*MBhat^3*Ycut + 
          150780*MBhat^4*Ycut + 356230*m2*MBhat^4*Ycut - 37870*m2^2*MBhat^4*
           Ycut - 131460*m2^3*MBhat^4*Ycut + 171010*m2^4*MBhat^4*Ycut - 
          44590*m2^5*MBhat^4*Ycut - 78624*MBhat^5*Ycut - 
          80640*m2*MBhat^5*Ycut - 130536*m2^2*MBhat^5*Ycut + 
          132048*m2^3*MBhat^5*Ycut - 38808*m2^4*MBhat^5*Ycut + 
          15750*MBhat^6*Ycut + 18165*m2*MBhat^6*Ycut + 34440*m2^2*MBhat^6*
           Ycut - 11025*m2^3*MBhat^6*Ycut - 8730*Ycut^2 - 466350*m2*Ycut^2 - 
          498462*m2^2*Ycut^2 + 616788*m2^3*Ycut^2 - 149010*m2^4*Ycut^2 - 
          35406*m2^5*Ycut^2 + 1890*m2^6*Ycut^2 + 62400*MBhat*Ycut^2 + 
          1994600*m2*MBhat*Ycut^2 - 566400*m2^2*MBhat*Ycut^2 - 
          1577640*m2^3*MBhat*Ycut^2 + 400808*m2^4*MBhat*Ycut^2 - 
          82656*m2^5*MBhat*Ycut^2 + 8568*m2^6*MBhat*Ycut^2 - 
          197505*MBhat^2*Ycut^2 - 3349290*m2*MBhat^2*Ycut^2 + 
          3271723*m2^2*MBhat^2*Ycut^2 - 498141*m2^3*MBhat^2*Ycut^2 + 
          149688*m2^4*MBhat^2*Ycut^2 - 134015*m2^5*MBhat^2*Ycut^2 + 
          23520*m2^6*MBhat^2*Ycut^2 + 363600*MBhat^3*Ycut^2 + 
          2681760*m2*MBhat^3*Ycut^2 - 2625720*m2^2*MBhat^3*Ycut^2 + 
          936912*m2^3*MBhat^3*Ycut^2 + 34656*m2^4*MBhat^3*Ycut^2 - 
          183288*m2^5*MBhat^3*Ycut^2 + 50400*m2^6*MBhat^3*Ycut^2 - 
          376950*MBhat^4*Ycut^2 - 1069670*m2*MBhat^4*Ycut^2 + 
          202230*m2^2*MBhat^4*Ycut^2 + 324870*m2^3*MBhat^4*Ycut^2 - 
          487550*m2^4*MBhat^4*Ycut^2 + 133770*m2^5*MBhat^4*Ycut^2 + 
          196560*MBhat^5*Ycut^2 + 259560*m2*MBhat^5*Ycut^2 + 
          340704*m2^2*MBhat^5*Ycut^2 - 373968*m2^3*MBhat^5*Ycut^2 + 
          116424*m2^4*MBhat^5*Ycut^2 - 39375*MBhat^6*Ycut^2 - 
          53970*m2*MBhat^6*Ycut^2 - 97020*m2^2*MBhat^6*Ycut^2 + 
          33075*m2^3*MBhat^6*Ycut^2 + 11640*Ycut^3 + 670260*m2*Ycut^3 + 
          779748*m2^2*Ycut^3 - 922914*m2^3*Ycut^3 + 210126*m2^4*Ycut^3 + 
          58170*m2^5*Ycut^3 - 3150*m2^6*Ycut^3 - 83200*MBhat*Ycut^3 - 
          2897600*m2*MBhat*Ycut^3 + 648640*m2^2*MBhat*Ycut^3 + 
          2491200*m2^3*MBhat*Ycut^3 - 635992*m2^4*MBhat*Ycut^3 + 
          133952*m2^5*MBhat*Ycut^3 - 14280*m2^6*MBhat*Ycut^3 + 
          263340*MBhat^2*Ycut^3 + 4951030*m2*MBhat^2*Ycut^3 - 
          4686892*m2^2*MBhat^2*Ycut^3 + 595847*m2^3*MBhat^2*Ycut^3 - 
          202860*m2^4*MBhat^2*Ycut^3 + 212905*m2^5*MBhat^2*Ycut^3 - 
          39200*m2^6*MBhat^2*Ycut^3 - 485430*MBhat^3*Ycut^3 - 
          4109550*m2*MBhat^3*Ycut^3 + 4051380*m2^2*MBhat^3*Ycut^3 - 
          1530108*m2^3*MBhat^3*Ycut^3 - 5502*m2^4*MBhat^3*Ycut^3 + 
          287490*m2^5*MBhat^3*Ycut^3 - 84000*m2^6*MBhat^3*Ycut^3 + 
          503440*MBhat^4*Ycut^3 + 1766660*m2*MBhat^4*Ycut^3 - 
          516460*m2^2*MBhat^4*Ycut^3 - 395850*m2^3*MBhat^4*Ycut^3 + 
          763210*m2^4*MBhat^4*Ycut^3 - 222950*m2^5*MBhat^4*Ycut^3 - 
          262290*MBhat^5*Ycut^3 - 465990*m2*MBhat^5*Ycut^3 - 
          460446*m2^2*MBhat^5*Ycut^3 + 578886*m2^3*MBhat^5*Ycut^3 - 
          194040*m2^4*MBhat^5*Ycut^3 + 52500*MBhat^6*Ycut^3 + 
          90090*m2*MBhat^6*Ycut^3 + 148680*m2^2*MBhat^6*Ycut^3 - 
          55125*m2^3*MBhat^6*Ycut^3 - 8730*Ycut^4 - 551640*m2*Ycut^4 - 
          710592*m2^2*Ycut^4 + 796194*m2^3*Ycut^4 - 165480*m2^4*Ycut^4 - 
          56910*m2^5*Ycut^4 + 3150*m2^6*Ycut^4 + 62400*MBhat*Ycut^4 + 
          2414800*m2*MBhat*Ycut^4 - 343600*m2^2*MBhat*Ycut^4 - 
          2301600*m2^3*MBhat*Ycut^4 + 590408*m2^4*MBhat*Ycut^4 - 
          128240*m2^5*MBhat*Ycut^4 + 14280*m2^6*MBhat*Ycut^4 - 
          201810*MBhat^2*Ycut^4 - 4186350*m2*MBhat^2*Ycut^4 + 
          3766553*m2^2*MBhat^2*Ycut^4 - 307580*m2^3*MBhat^2*Ycut^4 + 
          149205*m2^4*MBhat^2*Ycut^4 - 203840*m2^5*MBhat^2*Ycut^4 + 
          39200*m2^6*MBhat^2*Ycut^4 + 374240*MBhat^3*Ycut^4 + 
          3630650*m2*MBhat^3*Ycut^4 - 3651880*m2^2*MBhat^3*Ycut^4 + 
          1531852*m2^3*MBhat^3*Ycut^4 - 88760*m2^4*MBhat^3*Ycut^4 - 
          260750*m2^5*MBhat^3*Ycut^4 + 84000*m2^6*MBhat^3*Ycut^4 - 
          384300*MBhat^4*Ycut^4 - 1735090*m2*MBhat^4*Ycut^4 + 
          777490*m2^2*MBhat^4*Ycut^4 + 175140*m2^3*MBhat^4*Ycut^4 - 
          694820*m2^4*MBhat^4*Ycut^4 + 222950*m2^5*MBhat^4*Ycut^4 + 
          197820*MBhat^5*Ycut^4 + 515130*m2*MBhat^5*Ycut^4 + 
          309204*m2^2*MBhat^5*Ycut^4 - 520170*m2^3*MBhat^5*Ycut^4 + 
          194040*m2^4*MBhat^5*Ycut^4 - 39620*MBhat^6*Ycut^4 - 
          92330*m2*MBhat^6*Ycut^4 - 131810*m2^2*MBhat^6*Ycut^4 + 
          55125*m2^3*MBhat^6*Ycut^4 + 3492*Ycut^5 + 250314*m2*Ycut^5 + 
          367542*m2^2*Ycut^5 - 382284*m2^3*Ycut^5 + 67116*m2^4*Ycut^5 + 
          32886*m2^5*Ycut^5 - 1890*m2^6*Ycut^5 - 21327*MBhat*Ycut^5 - 
          1128563*m2*MBhat*Ycut^5 + 44226*m2^2*MBhat*Ycut^5 + 
          1208466*m2^3*MBhat*Ycut^5 - 319291*m2^4*MBhat*Ycut^5 + 
          75201*m2^5*MBhat*Ycut^5 - 8568*m2^6*MBhat*Ycut^5 + 
          100380*MBhat^2*Ycut^5 + 1894620*m2*MBhat^2*Ycut^5 - 
          1523452*m2^2*MBhat^2*Ycut^5 - 64932*m2^3*MBhat^2*Ycut^5 - 
          37842*m2^4*MBhat^2*Ycut^5 + 113680*m2^5*MBhat^2*Ycut^5 - 
          23520*m2^6*MBhat^2*Ycut^5 - 195462*MBhat^3*Ycut^5 - 
          1718254*m2*MBhat^3*Ycut^5 + 1865836*m2^2*MBhat^3*Ycut^5 - 
          990192*m2^3*MBhat^3*Ycut^5 + 172018*m2^4*MBhat^3*Ycut^5 + 
          129710*m2^5*MBhat^3*Ycut^5 - 50400*m2^6*MBhat^3*Ycut^5 + 
          177240*MBhat^4*Ycut^5 + 1006460*m2*MBhat^4*Ycut^5 - 
          751170*m2^2*MBhat^4*Ycut^5 + 146790*m2^3*MBhat^4*Ycut^5 + 
          348880*m2^4*MBhat^4*Ycut^5 - 133770*m2^5*MBhat^4*Ycut^5 - 
          81543*MBhat^5*Ycut^5 - 363993*m2*MBhat^5*Ycut^5 - 
          32109*m2^2*MBhat^5*Ycut^5 + 256221*m2^3*MBhat^5*Ycut^5 - 
          116424*m2^4*MBhat^5*Ycut^5 + 17220*MBhat^6*Ycut^5 + 
          59290*m2*MBhat^6*Ycut^5 + 63980*m2^2*MBhat^6*Ycut^5 - 
          33075*m2^3*MBhat^6*Ycut^5 - 1506*Ycut^6 - 48048*m2*Ycut^6 - 
          96936*m2^2*Ycut^6 + 85680*m2^3*Ycut^6 - 7224*m2^4*Ycut^6 - 
          11004*m2^5*Ycut^6 + 630*m2^6*Ycut^6 - 17022*MBhat*Ycut^6 + 
          306187*m2*MBhat*Ycut^6 - 28070*m2^2*MBhat*Ycut^6 - 
          295764*m2^3*MBhat*Ycut^6 + 90860*m2^4*MBhat*Ycut^6 - 
          24199*m2^5*MBhat*Ycut^6 + 2856*m2^6*MBhat*Ycut^6 - 
          50610*MBhat^2*Ycut^6 - 298914*m2*MBhat^2*Ycut^6 + 
          111419*m2^2*MBhat^2*Ycut^6 + 188027*m2^3*MBhat^2*Ycut^6 - 
          32340*m2^4*MBhat^2*Ycut^6 - 32144*m2^5*MBhat^2*Ycut^6 + 
          7840*m2^6*MBhat^2*Ycut^6 + 136212*MBhat^3*Ycut^6 + 
          223412*m2*MBhat^3*Ycut^6 - 442302*m2^2*MBhat^3*Ycut^6 + 
          428106*m2^3*MBhat^3*Ycut^6 - 154826*m2^4*MBhat^3*Ycut^6 - 
          25410*m2^5*MBhat^3*Ycut^6 + 16800*m2^6*MBhat^3*Ycut^6 - 
          76692*MBhat^4*Ycut^6 - 309442*m2*MBhat^4*Ycut^6 + 
          483518*m2^2*MBhat^4*Ycut^6 - 255192*m2^3*MBhat^4*Ycut^6 - 
          70952*m2^4*MBhat^4*Ycut^6 + 44590*m2^5*MBhat^4*Ycut^6 + 
          15918*MBhat^5*Ycut^6 + 166425*m2*MBhat^5*Ycut^6 - 
          105084*m2^2*MBhat^5*Ycut^6 - 48363*m2^3*MBhat^5*Ycut^6 + 
          38808*m2^4*MBhat^5*Ycut^6 - 6300*MBhat^6*Ycut^6 - 
          23450*m2*MBhat^6*Ycut^6 - 11760*m2^2*MBhat^6*Ycut^6 + 
          11025*m2^3*MBhat^6*Ycut^6 + 5720*Ycut^7 - 15952*m2*Ycut^7 + 
          20882*m2^2*Ycut^7 - 4178*m2^3*Ycut^7 - 4262*m2^4*Ycut^7 + 
          1660*m2^5*Ycut^7 - 90*m2^6*Ycut^7 + 50601*MBhat*Ycut^7 - 
          134528*m2*MBhat*Ycut^7 + 90851*m2^2*MBhat*Ycut^7 - 
          5133*m2^3*MBhat*Ycut^7 - 6148*m2^4*MBhat*Ycut^7 + 
          3085*m2^5*MBhat*Ycut^7 - 408*m2^6*MBhat*Ycut^7 + 
          16800*MBhat^2*Ycut^7 - 58926*m2*MBhat^2*Ycut^7 + 
          129458*m2^2*MBhat^2*Ycut^7 - 113855*m2^3*MBhat^2*Ycut^7 + 
          31500*m2^4*MBhat^2*Ycut^7 + 2128*m2^5*MBhat^2*Ycut^7 - 
          1120*m2^6*MBhat^2*Ycut^7 - 137808*MBhat^3*Ycut^7 + 
          181322*m2*MBhat^3*Ycut^7 - 5830*m2^2*MBhat^3*Ycut^7 - 
          126804*m2^3*MBhat^3*Ycut^7 + 76630*m2^4*MBhat^3*Ycut^7 - 
          4010*m2^5*MBhat^3*Ycut^7 - 2400*m2^6*MBhat^3*Ycut^7 + 
          59472*MBhat^4*Ycut^7 + 34580*m2*MBhat^4*Ycut^7 - 
          205772*m2^2*MBhat^4*Ycut^7 + 152376*m2^3*MBhat^4*Ycut^7 - 
          9296*m2^4*MBhat^4*Ycut^7 - 6370*m2^5*MBhat^4*Ycut^7 + 
          315*MBhat^5*Ycut^7 - 50400*m2*MBhat^5*Ycut^7 + 78876*m2^2*MBhat^5*
           Ycut^7 - 8967*m2^3*MBhat^5*Ycut^7 - 5544*m2^4*MBhat^5*Ycut^7 + 
          4900*MBhat^6*Ycut^7 + 5810*m2*MBhat^6*Ycut^7 - 2380*m2^2*MBhat^6*
           Ycut^7 - 1575*m2^3*MBhat^6*Ycut^7 - 14916*Ycut^8 + 
          33350*m2*Ycut^8 - 19058*m2^2*Ycut^8 - 66*m2^3*Ycut^8 + 
          1132*m2^4*Ycut^8 - 22*m2^5*Ycut^8 - 62232*MBhat*Ycut^8 + 
          111100*m2*MBhat*Ycut^8 - 62544*m2^2*MBhat*Ycut^8 + 
          16743*m2^3*MBhat*Ycut^8 - 3160*m2^4*MBhat*Ycut^8 + 
          93*m2^5*MBhat*Ycut^8 + 30765*MBhat^2*Ycut^8 - 4725*m2*MBhat^2*
           Ycut^8 - 40747*m2^2*MBhat^2*Ycut^8 + 31038*m2^3*MBhat^2*Ycut^8 - 
          10542*m2^4*MBhat^2*Ycut^8 + 616*m2^5*MBhat^2*Ycut^8 + 
          94788*MBhat^3*Ycut^8 - 99580*m2*MBhat^3*Ycut^8 + 
          19540*m2^2*MBhat^3*Ycut^8 + 25176*m2^3*MBhat^3*Ycut^8 - 
          19994*m2^4*MBhat^3*Ycut^8 + 1910*m2^5*MBhat^3*Ycut^8 - 
          41370*MBhat^4*Ycut^8 - 3430*m2*MBhat^4*Ycut^8 + 
          52248*m2^2*MBhat^4*Ycut^8 - 42756*m2^3*MBhat^4*Ycut^8 + 
          4858*m2^4*MBhat^4*Ycut^8 - 3360*MBhat^5*Ycut^8 + 
          10500*m2*MBhat^5*Ycut^8 - 24024*m2^2*MBhat^5*Ycut^8 + 
          3969*m2^3*MBhat^5*Ycut^8 - 3675*MBhat^6*Ycut^8 - 
          1225*m2*MBhat^6*Ycut^8 + 1015*m2^2*MBhat^6*Ycut^8 + 21120*Ycut^9 - 
          31840*m2*Ycut^9 + 10212*m2^2*Ycut^9 + 276*m2^3*Ycut^9 + 
          148*m2^4*Ycut^9 + 39205*MBhat*Ycut^9 - 48355*m2*MBhat*Ycut^9 + 
          15836*m2^2*MBhat*Ycut^9 - 2561*m2^3*MBhat*Ycut^9 + 
          579*m2^4*MBhat*Ycut^9 - 49350*MBhat^2*Ycut^9 + 
          19985*m2*MBhat^2*Ycut^9 + 6118*m2^2*MBhat^2*Ycut^9 - 
          2324*m2^3*MBhat^2*Ycut^9 + 1274*m2^4*MBhat^2*Ycut^9 - 
          32640*MBhat^3*Ycut^9 + 23390*m2*MBhat^3*Ycut^9 - 
          960*m2^2*MBhat^3*Ycut^9 - 2664*m2^3*MBhat^3*Ycut^9 + 
          2122*m2^4*MBhat^3*Ycut^9 + 16940*MBhat^4*Ycut^9 + 
          5110*m2*MBhat^4*Ycut^9 - 5432*m2^2*MBhat^4*Ycut^9 + 
          4592*m2^3*MBhat^4*Ycut^9 + 3255*MBhat^5*Ycut^9 - 
          945*m2*MBhat^5*Ycut^9 + 2751*m2^2*MBhat^5*Ycut^9 + 
          1470*MBhat^6*Ycut^9 + 245*m2*MBhat^6*Ycut^9 - 17380*Ycut^10 + 
          15754*m2*Ycut^10 - 1334*m2^2*Ycut^10 - 148*m2^3*Ycut^10 - 
          8598*MBhat*Ycut^10 + 8483*m2*MBhat*Ycut^10 - 1658*m2^2*MBhat*
           Ycut^10 - 159*m2^3*MBhat*Ycut^10 + 28707*MBhat^2*Ycut^10 - 
          4788*m2*MBhat^2*Ycut^10 - 1190*m2^2*MBhat^2*Ycut^10 - 
          154*m2^3*MBhat^2*Ycut^10 + 2892*MBhat^3*Ycut^10 - 
          4588*m2*MBhat^3*Ycut^10 - 718*m2^2*MBhat^3*Ycut^10 - 
          22*m2^3*MBhat^3*Ycut^10 - 3990*MBhat^4*Ycut^10 - 
          1400*m2*MBhat^4*Ycut^10 - 112*m2^2*MBhat^4*Ycut^10 - 
          1386*MBhat^5*Ycut^10 - 231*m2*MBhat^5*Ycut^10 - 
          245*MBhat^6*Ycut^10 + 8184*Ycut^11 - 3488*m2*Ycut^11 - 
          202*m2^2*Ycut^11 - 3033*MBhat*Ycut^11 - 682*m2*MBhat*Ycut^11 + 
          159*m2^2*MBhat*Ycut^11 - 6972*MBhat^2*Ycut^11 + 
          504*m2*MBhat^2*Ycut^11 + 154*m2^2*MBhat^2*Ycut^11 + 
          918*MBhat^3*Ycut^11 + 740*m2*MBhat^3*Ycut^11 + 
          22*m2^2*MBhat^3*Ycut^11 + 672*MBhat^4*Ycut^11 + 
          112*m2*MBhat^4*Ycut^11 + 231*MBhat^5*Ycut^11 - 1980*Ycut^12 + 
          202*m2*Ycut^12 + 1804*MBhat*Ycut^12 + 198*m2*MBhat*Ycut^12 + 
          420*MBhat^2*Ycut^12 - 154*m2*MBhat^2*Ycut^12 - 
          132*MBhat^3*Ycut^12 - 22*m2*MBhat^3*Ycut^12 - 112*MBhat^4*Ycut^12 + 
          176*Ycut^13 - 198*MBhat*Ycut^13 + 22*MBhat^3*Ycut^13))/
        (105*(-1 + Ycut)^7) + 8*m2*(24 + 90*m2 - 30*m2^2 - 30*m2^3 + 
         18*m2^4 - 120*MBhat - 192*m2*MBhat + 280*m2^2*MBhat + 252*MBhat^2 + 
         57*m2*MBhat^2 - 288*m2^2*MBhat^2 + 77*m2^3*MBhat^2 - 288*MBhat^3 + 
         120*m2*MBhat^3 + 192*MBhat^4 - 84*m2*MBhat^4 + 62*m2^2*MBhat^4 - 
         72*MBhat^5 + 12*MBhat^6 + 9*m2*MBhat^6)*Log[m2] - 
       8*m2*(24 + 90*m2 - 30*m2^2 - 30*m2^3 + 18*m2^4 - 120*MBhat - 
         192*m2*MBhat + 280*m2^2*MBhat + 252*MBhat^2 + 57*m2*MBhat^2 - 
         288*m2^2*MBhat^2 + 77*m2^3*MBhat^2 - 288*MBhat^3 + 120*m2*MBhat^3 + 
         192*MBhat^4 - 84*m2*MBhat^4 + 62*m2^2*MBhat^4 - 72*MBhat^5 + 
         12*MBhat^6 + 9*m2*MBhat^6)*Log[1 - Ycut]) + 
     c[VR]*(-1/15*(Sqrt[m2]*(-1 + m2 + Ycut)*(-460 - 430*m2 + 8270*m2^2 + 
           5470*m2^3 - 230*m2^4 - 20*m2^5 + 2484*MBhat - 4464*m2*MBhat - 
           33264*m2^2*MBhat - 5664*m2^3*MBhat + 636*m2^4*MBhat - 
           48*m2^5*MBhat - 5386*MBhat^2 + 19589*m2*MBhat^2 + 
           37124*m2^2*MBhat^2 - 3016*m2^3*MBhat^2 + 494*m2^4*MBhat^2 - 
           85*m2^5*MBhat^2 + 5832*MBhat^3 - 26904*m2*MBhat^3 - 
           10464*m2^2*MBhat^3 + 416*m2^3*MBhat^3 + 536*m2^4*MBhat^3 - 
           136*m2^5*MBhat^3 - 3300*MBhat^4 + 15825*m2*MBhat^4 - 
           3855*m2^2*MBhat^4 + 1605*m2^3*MBhat^4 - 375*m2^4*MBhat^4 + 
           960*MBhat^5 - 3720*m2*MBhat^5 + 1680*m2^2*MBhat^5 - 
           360*m2^3*MBhat^5 - 130*MBhat^6 + 140*m2*MBhat^6 - 
           130*m2^2*MBhat^6 + 1960*Ycut + 2280*m2*Ycut - 36550*m2^2*Ycut - 
           25480*m2^3*Ycut + 990*m2^4*Ycut + 100*m2^5*Ycut - 
           10656*MBhat*Ycut + 17460*m2*MBhat*Ycut + 149796*m2^2*MBhat*Ycut + 
           27732*m2^3*MBhat*Ycut - 3132*m2^4*MBhat*Ycut + 
           240*m2^5*MBhat*Ycut + 23344*MBhat^2*Ycut - 82482*m2*MBhat^2*Ycut - 
           171733*m2^2*MBhat^2*Ycut + 13591*m2^3*MBhat^2*Ycut - 
           2385*m2^4*MBhat^2*Ycut + 425*m2^5*MBhat^2*Ycut - 
           25728*MBhat^3*Ycut + 116808*m2*MBhat^3*Ycut + 51504*m2^2*MBhat^3*
            Ycut - 2480*m2^3*MBhat^3*Ycut - 2544*m2^4*MBhat^3*Ycut + 
           680*m2^5*MBhat^3*Ycut + 15000*MBhat^4*Ycut - 70740*m2*MBhat^4*
            Ycut + 16965*m2^2*MBhat^4*Ycut - 7650*m2^3*MBhat^4*Ycut + 
           1875*m2^4*MBhat^4*Ycut - 4560*MBhat^5*Ycut + 17280*m2*MBhat^5*
            Ycut - 8040*m2^2*MBhat^5*Ycut + 1800*m2^3*MBhat^5*Ycut + 
           640*MBhat^6*Ycut - 750*m2*MBhat^6*Ycut + 650*m2^2*MBhat^6*Ycut - 
           3180*Ycut^2 - 4650*m2*Ycut^2 + 62000*m2^2*Ycut^2 + 
           46320*m2^3*Ycut^2 - 1590*m2^4*Ycut^2 - 200*m2^5*Ycut^2 + 
           17424*MBhat*Ycut^2 - 24876*m2*MBhat*Ycut^2 - 260280*m2^2*MBhat*
            Ycut^2 - 53748*m2^3*MBhat*Ycut^2 + 6120*m2^4*MBhat*Ycut^2 - 
           480*m2^5*MBhat*Ycut^2 - 38616*MBhat^2*Ycut^2 + 131082*m2*MBhat^2*
            Ycut^2 + 308849*m2^2*MBhat^2*Ycut^2 - 23340*m2^3*MBhat^2*Ycut^2 + 
           4515*m2^4*MBhat^2*Ycut^2 - 850*m2^5*MBhat^2*Ycut^2 + 
           43392*MBhat^3*Ycut^2 - 193080*m2*MBhat^3*Ycut^2 - 
           100296*m2^2*MBhat^3*Ycut^2 + 6024*m2^3*MBhat^3*Ycut^2 + 
           4680*m2^4*MBhat^3*Ycut^2 - 1360*m2^5*MBhat^3*Ycut^2 - 
           26100*MBhat^4*Ycut^2 + 121140*m2*MBhat^4*Ycut^2 - 
           27915*m2^2*MBhat^4*Ycut^2 + 14175*m2^3*MBhat^4*Ycut^2 - 
           3750*m2^4*MBhat^4*Ycut^2 + 8280*MBhat^5*Ycut^2 - 
           30960*m2*MBhat^5*Ycut^2 + 15000*m2^2*MBhat^5*Ycut^2 - 
           3600*m2^3*MBhat^5*Ycut^2 - 1200*MBhat^6*Ycut^2 + 
           1560*m2*MBhat^6*Ycut^2 - 1300*m2^2*MBhat^6*Ycut^2 + 2360*Ycut^3 + 
           4460*m2*Ycut^3 - 48740*m2^2*Ycut^3 - 40220*m2^3*Ycut^3 + 
           1090*m2^4*Ycut^3 + 200*m2^5*Ycut^3 - 13056*MBhat*Ycut^3 + 
           14628*m2*MBhat*Ycut^3 + 211548*m2^2*MBhat*Ycut^3 + 
           51000*m2^3*MBhat*Ycut^3 - 5880*m2^4*MBhat*Ycut^3 + 
           480*m2^5*MBhat*Ycut^3 + 29344*MBhat^2*Ycut^3 - 93554*m2*MBhat^2*
            Ycut^3 - 263205*m2^2*MBhat^2*Ycut^3 + 18035*m2^3*MBhat^2*Ycut^3 - 
           4090*m2^4*MBhat^2*Ycut^3 + 850*m2^5*MBhat^2*Ycut^3 - 
           33338*MBhat^3*Ycut^3 + 144712*m2*MBhat^3*Ycut^3 + 
           95836*m2^2*MBhat^3*Ycut^3 - 7240*m2^3*MBhat^3*Ycut^3 - 
           4210*m2^4*MBhat^3*Ycut^3 + 1360*m2^5*MBhat^3*Ycut^3 + 
           20340*MBhat^4*Ycut^3 - 94860*m2*MBhat^4*Ycut^3 + 
           20085*m2^2*MBhat^4*Ycut^3 - 12840*m2^3*MBhat^4*Ycut^3 + 
           3750*m2^4*MBhat^4*Ycut^3 - 6690*MBhat^5*Ycut^3 + 
           25980*m2*MBhat^5*Ycut^3 - 13650*m2^2*MBhat^5*Ycut^3 + 
           3600*m2^3*MBhat^5*Ycut^3 + 1040*MBhat^6*Ycut^3 - 
           1630*m2*MBhat^6*Ycut^3 + 1300*m2^2*MBhat^6*Ycut^3 - 710*Ycut^4 - 
           1875*m2*Ycut^4 + 15985*m2^2*Ycut^4 + 15665*m2^3*Ycut^4 - 
           195*m2^4*Ycut^4 - 100*m2^5*Ycut^4 + 3984*MBhat*Ycut^4 - 
           2268*m2*MBhat*Ycut^4 - 73320*m2^2*MBhat*Ycut^4 - 
           22920*m2^3*MBhat*Ycut^4 + 2700*m2^4*MBhat*Ycut^4 - 
           240*m2^5*MBhat*Ycut^4 - 9421*MBhat^2*Ycut^4 + 26190*m2*MBhat^2*
            Ycut^4 + 98535*m2^2*MBhat^2*Ycut^4 - 5570*m2^3*MBhat^2*Ycut^4 + 
           1935*m2^4*MBhat^2*Ycut^4 - 425*m2^5*MBhat^2*Ycut^4 + 
           9872*MBhat^3*Ycut^4 - 40746*m2*MBhat^3*Ycut^4 - 
           45770*m2^2*MBhat^3*Ycut^4 + 5530*m2^3*MBhat^3*Ycut^4 + 
           1650*m2^4*MBhat^3*Ycut^4 - 680*m2^5*MBhat^3*Ycut^4 - 
           4875*MBhat^4*Ycut^4 + 27435*m2*MBhat^4*Ycut^4 - 
           3480*m2^2*MBhat^4*Ycut^4 + 5400*m2^3*MBhat^4*Ycut^4 - 
           1875*m2^4*MBhat^4*Ycut^4 + 1560*MBhat^5*Ycut^4 - 
           9090*m2*MBhat^5*Ycut^4 + 5970*m2^2*MBhat^5*Ycut^4 - 
           1800*m2^3*MBhat^5*Ycut^4 - 410*MBhat^6*Ycut^4 + 
           930*m2*MBhat^6*Ycut^4 - 650*m2^2*MBhat^6*Ycut^4 + 24*Ycut^5 + 
           174*m2*Ycut^5 - 761*m2^2*Ycut^5 - 1476*m2^3*Ycut^5 - 
           81*m2^4*Ycut^5 + 20*m2^5*Ycut^5 - 33*MBhat*Ycut^5 - 
           540*m2*MBhat*Ycut^5 + 4170*m2^2*MBhat*Ycut^5 + 
           3660*m2^3*MBhat*Ycut^5 - 585*m2^4*MBhat*Ycut^5 + 
           48*m2^5*MBhat*Ycut^5 + 1122*MBhat^2*Ycut^5 - 2013*m2*MBhat^2*
            Ycut^5 - 6588*m2^2*MBhat^2*Ycut^5 - 378*m2^3*MBhat^2*Ycut^5 - 
           348*m2^4*MBhat^2*Ycut^5 + 85*m2^5*MBhat^2*Ycut^5 - 
           33*MBhat^3*Ycut^5 - 2352*m2*MBhat^3*Ycut^5 + 10498*m2^2*MBhat^3*
            Ycut^5 - 3072*m2^3*MBhat^3*Ycut^5 - 57*m2^4*MBhat^3*Ycut^5 + 
           136*m2^5*MBhat^3*Ycut^5 - 2100*MBhat^4*Ycut^5 + 
           4185*m2*MBhat^4*Ycut^5 - 3495*m2^2*MBhat^4*Ycut^5 - 
           495*m2^3*MBhat^4*Ycut^5 + 375*m2^4*MBhat^4*Ycut^5 + 
           900*MBhat^5*Ycut^5 - 90*m2*MBhat^5*Ycut^5 - 870*m2^2*MBhat^5*
            Ycut^5 + 360*m2^3*MBhat^5*Ycut^5 + 120*MBhat^6*Ycut^5 - 
           300*m2*MBhat^6*Ycut^5 + 130*m2^2*MBhat^6*Ycut^5 - 14*Ycut^6 + 
           40*m2*Ycut^6 - 61*m2^2*Ycut^6 - 317*m2^3*Ycut^6 + 52*m2^4*Ycut^6 - 
           342*MBhat*Ycut^6 + 393*m2*MBhat*Ycut^6 + 873*m2^2*MBhat*Ycut^6 + 
           3*m2^3*MBhat*Ycut^6 + 33*m2^4*MBhat*Ycut^6 - 446*MBhat^2*Ycut^6 + 
           1831*m2*MBhat^2*Ycut^6 - 3287*m2^2*MBhat^2*Ycut^6 + 
           755*m2^3*MBhat^2*Ycut^6 - 13*m2^4*MBhat^2*Ycut^6 + 
           332*MBhat^3*Ycut^6 + 965*m2*MBhat^3*Ycut^6 - 1317*m2^2*MBhat^3*
            Ycut^6 + 911*m2^3*MBhat^3*Ycut^6 - 91*m2^4*MBhat^3*Ycut^6 + 
           1050*MBhat^4*Ycut^6 - 3225*m2*MBhat^4*Ycut^6 + 1860*m2^2*MBhat^4*
            Ycut^6 - 195*m2^3*MBhat^4*Ycut^6 - 480*MBhat^5*Ycut^6 + 
           690*m2*MBhat^5*Ycut^6 - 90*m2^2*MBhat^5*Ycut^6 - 
           100*MBhat^6*Ycut^6 + 50*m2*MBhat^6*Ycut^6 + 56*Ycut^7 - 
           24*m2*Ycut^7 - 145*m2^2*Ycut^7 + 38*m2^3*Ycut^7 + 
           258*MBhat*Ycut^7 - 459*m2*MBhat*Ycut^7 + 504*m2^2*MBhat*Ycut^7 - 
           63*m2^3*MBhat*Ycut^7 - 136*MBhat^2*Ycut^7 - 435*m2*MBhat^2*
            Ycut^7 + 268*m2^2*MBhat^2*Ycut^7 - 77*m2^3*MBhat^2*Ycut^7 - 
           248*MBhat^3*Ycut^7 + 597*m2*MBhat^3*Ycut^7 - 89*m2^3*MBhat^3*
            Ycut^7 + 195*m2*MBhat^4*Ycut^7 - 165*m2^2*MBhat^4*Ycut^7 + 
           30*MBhat^5*Ycut^7 - 90*m2*MBhat^5*Ycut^7 + 40*MBhat^6*Ycut^7 - 
           54*Ycut^8 + 27*m2*Ycut^8 + 2*m2^2*Ycut^8 - 12*MBhat*Ycut^8 + 
           99*m2*MBhat*Ycut^8 - 27*m2^2*MBhat*Ycut^8 + 189*MBhat^2*Ycut^8 - 
           216*m2*MBhat^2*Ycut^8 + 37*m2^2*MBhat^2*Ycut^8 - 
           108*MBhat^3*Ycut^8 + 9*m2*MBhat^3*Ycut^8 + 9*m2^2*MBhat^3*Ycut^8 - 
           15*MBhat^4*Ycut^8 + 45*m2*MBhat^4*Ycut^8 + 16*Ycut^9 - 
           2*m2*Ycut^9 - 57*MBhat*Ycut^9 + 27*m2*MBhat*Ycut^9 + 
           14*MBhat^2*Ycut^9 + 8*m2*MBhat^2*Ycut^9 + 27*MBhat^3*Ycut^9 - 
           9*m2*MBhat^3*Ycut^9 + 2*Ycut^10 + 6*MBhat*Ycut^10 - 
           8*MBhat^2*Ycut^10))/(-1 + Ycut)^5 + 
       4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5 - 
         12*MBhat - 36*m2*MBhat + 360*m2^2*MBhat + 360*m2^3*MBhat + 
         30*MBhat^2 - 9*m2*MBhat^2 - 645*m2^2*MBhat^2 - 206*m2^3*MBhat^2 + 
         18*m2^4*MBhat^2 - 40*MBhat^3 + 96*m2*MBhat^3 + 456*m2^2*MBhat^3 + 
         30*MBhat^4 - 99*m2*MBhat^4 - 114*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 
         12*MBhat^5 + 36*m2*MBhat^5 + 2*MBhat^6 - 3*m2*MBhat^6 + 
         3*m2^2*MBhat^6)*Log[m2] - 4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 
         140*m2^3 - 30*m2^4 + 3*m2^5 - 12*MBhat - 36*m2*MBhat + 
         360*m2^2*MBhat + 360*m2^3*MBhat + 30*MBhat^2 - 9*m2*MBhat^2 - 
         645*m2^2*MBhat^2 - 206*m2^3*MBhat^2 + 18*m2^4*MBhat^2 - 40*MBhat^3 + 
         96*m2*MBhat^3 + 456*m2^2*MBhat^3 + 30*MBhat^4 - 99*m2*MBhat^4 - 
         114*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 12*MBhat^5 + 36*m2*MBhat^5 + 
         2*MBhat^6 - 3*m2*MBhat^6 + 3*m2^2*MBhat^6)*Log[1 - Ycut]) + 
     c[SL]*(-1/30*(Ycut^3*(-1 + m2 + Ycut)*(-150*MBhat^3 + 30*m2*MBhat^3 + 
           180*m2^2*MBhat^3 + 780*m2^3*MBhat^3 - 1470*m2^4*MBhat^3 + 
           630*m2^5*MBhat^3 + 240*MBhat^4 + 240*m2*MBhat^4 + 
           240*m2^2*MBhat^4 - 2160*m2^3*MBhat^4 + 1440*m2^4*MBhat^4 - 
           90*MBhat^5 - 210*m2*MBhat^5 - 750*m2^2*MBhat^5 + 
           1050*m2^3*MBhat^5 + 240*m2^2*MBhat^6 + 225*MBhat^2*Ycut - 
           45*m2*MBhat^2*Ycut - 270*m2^2*MBhat^2*Ycut - 1170*m2^3*MBhat^2*
            Ycut + 2205*m2^4*MBhat^2*Ycut - 945*m2^5*MBhat^2*Ycut + 
           620*MBhat^3*Ycut - 610*m2*MBhat^3*Ycut - 1480*m2^2*MBhat^3*Ycut + 
           700*m2^3*MBhat^3*Ycut + 2380*m2^4*MBhat^3*Ycut - 
           1610*m2^5*MBhat^3*Ycut - 1395*MBhat^4*Ycut - 1005*m2*MBhat^4*
            Ycut + 585*m2^2*MBhat^4*Ycut + 6225*m2^3*MBhat^4*Ycut - 
           4410*m2^4*MBhat^4*Ycut + 540*MBhat^5*Ycut + 1170*m2*MBhat^5*Ycut + 
           2760*m2^2*MBhat^5*Ycut - 3750*m2^3*MBhat^5*Ycut + 
           10*MBhat^6*Ycut + 10*m2*MBhat^6*Ycut - 980*m2^2*MBhat^6*Ycut - 
           135*MBhat*Ycut^2 + 27*m2*MBhat*Ycut^2 + 162*m2^2*MBhat*Ycut^2 + 
           702*m2^3*MBhat*Ycut^2 - 1323*m2^4*MBhat*Ycut^2 + 
           567*m2^5*MBhat*Ycut^2 - 1254*MBhat^2*Ycut^2 + 591*m2*MBhat^2*
            Ycut^2 + 1896*m2^2*MBhat^2*Ycut^2 + 1026*m2^3*MBhat^2*Ycut^2 - 
           3834*m2^4*MBhat^2*Ycut^2 + 1575*m2^5*MBhat^2*Ycut^2 - 
           516*MBhat^3*Ycut^2 + 2570*m2*MBhat^3*Ycut^2 + 2440*m2^2*MBhat^3*
            Ycut^2 - 5460*m2^3*MBhat^3*Ycut^2 - 980*m2^4*MBhat^3*Ycut^2 + 
           1946*m2^5*MBhat^3*Ycut^2 + 3330*MBhat^4*Ycut^2 + 
           1275*m2*MBhat^4*Ycut^2 - 3990*m2^2*MBhat^4*Ycut^2 - 
           6765*m2^3*MBhat^4*Ycut^2 + 5670*m2^4*MBhat^4*Ycut^2 - 
           1365*MBhat^5*Ycut^2 - 2715*m2*MBhat^5*Ycut^2 - 3915*m2^2*MBhat^5*
            Ycut^2 + 5355*m2^3*MBhat^5*Ycut^2 - 60*MBhat^6*Ycut^2 - 
           50*m2*MBhat^6*Ycut^2 + 1580*m2^2*MBhat^6*Ycut^2 + 30*Ycut^3 - 
           6*m2*Ycut^3 - 36*m2^2*Ycut^3 - 156*m2^3*Ycut^3 + 294*m2^4*Ycut^3 - 
           126*m2^5*Ycut^3 + 834*MBhat*Ycut^3 - 273*m2*MBhat*Ycut^3 - 
           1056*m2^2*MBhat*Ycut^3 - 1014*m2^3*MBhat*Ycut^3 + 
           2118*m2^4*MBhat*Ycut^3 - 609*m2^5*MBhat*Ycut^3 + 
           2736*MBhat^2*Ycut^3 - 2325*m2*MBhat^2*Ycut^3 - 3804*m2^2*MBhat^2*
            Ycut^3 + 2322*m2^3*MBhat^2*Ycut^3 + 2268*m2^4*MBhat^2*Ycut^3 - 
           1197*m2^5*MBhat^2*Ycut^3 - 1524*MBhat^3*Ycut^3 - 
           4666*m2*MBhat^3*Ycut^3 + 324*m2^2*MBhat^3*Ycut^3 + 
           6264*m2^3*MBhat^3*Ycut^3 - 56*m2^4*MBhat^3*Ycut^3 - 
           1302*m2^5*MBhat^3*Ycut^3 - 4116*MBhat^4*Ycut^3 + 
           309*m2*MBhat^4*Ycut^3 + 6219*m2^2*MBhat^4*Ycut^3 + 
           3654*m2^3*MBhat^4*Ycut^3 - 3906*m2^4*MBhat^4*Ycut^3 + 
           1890*MBhat^5*Ycut^3 + 3375*m2*MBhat^5*Ycut^3 + 2700*m2^2*MBhat^5*
            Ycut^3 - 3885*m2^3*MBhat^5*Ycut^3 + 150*MBhat^6*Ycut^3 + 
           100*m2*MBhat^6*Ycut^3 - 1260*m2^2*MBhat^6*Ycut^3 - 196*Ycut^4 + 
           50*m2*Ycut^4 + 224*m2^2*Ycut^4 + 268*m2^3*Ycut^4 - 
           428*m2^4*Ycut^4 + 82*m2^5*Ycut^4 - 2151*MBhat*Ycut^4 + 
           978*m2*MBhat*Ycut^4 + 2217*m2^2*MBhat*Ycut^4 - 
           297*m2^3*MBhat*Ycut^4 - 1014*m2^4*MBhat*Ycut^4 + 
           267*m2^5*MBhat*Ycut^4 - 2682*MBhat^2*Ycut^4 + 4317*m2*MBhat^2*
            Ycut^4 + 2538*m2^2*MBhat^2*Ycut^4 - 3240*m2^3*MBhat^2*Ycut^4 - 
           792*m2^4*MBhat^2*Ycut^4 + 459*m2^5*MBhat^2*Ycut^4 + 
           4158*MBhat^3*Ycut^4 + 4028*m2*MBhat^3*Ycut^4 - 3598*m2^2*MBhat^3*
            Ycut^4 - 2934*m2^3*MBhat^3*Ycut^4 + 160*m2^4*MBhat^3*Ycut^4 + 
           466*m2^5*MBhat^3*Ycut^4 + 2646*MBhat^4*Ycut^4 - 
           2295*m2*MBhat^4*Ycut^4 - 4176*m2^2*MBhat^4*Ycut^4 - 
           1122*m2^3*MBhat^4*Ycut^4 + 1422*m2^4*MBhat^4*Ycut^4 - 
           1575*MBhat^5*Ycut^4 - 2400*m2*MBhat^5*Ycut^4 - 960*m2^2*MBhat^5*
            Ycut^4 + 1455*m2^3*MBhat^5*Ycut^4 - 200*MBhat^6*Ycut^4 - 
           100*m2*MBhat^6*Ycut^4 + 500*m2^2*MBhat^6*Ycut^4 + 546*Ycut^5 - 
           160*m2*Ycut^5 - 476*m2^2*Ycut^5 - 48*m2^3*Ycut^5 + 
           154*m2^4*Ycut^5 - 16*m2^5*Ycut^5 + 2952*MBhat*Ycut^5 - 
           1740*m2*MBhat*Ycut^5 - 1908*m2^2*MBhat*Ycut^5 + 
           855*m2^3*MBhat*Ycut^5 + 246*m2^4*MBhat*Ycut^5 - 
           45*m2^5*MBhat*Ycut^5 + 510*MBhat^2*Ycut^5 - 4245*m2*MBhat^2*
            Ycut^5 + 318*m2^2*MBhat^2*Ycut^5 + 1278*m2^3*MBhat^2*Ycut^5 + 
           171*m2^4*MBhat^2*Ycut^5 - 72*m2^5*MBhat^2*Ycut^5 - 
           4368*MBhat^3*Ycut^5 - 1180*m2*MBhat^3*Ycut^5 + 2872*m2^2*MBhat^3*
            Ycut^5 + 738*m2^3*MBhat^3*Ycut^5 - 32*m2^4*MBhat^3*Ycut^5 - 
           70*m2^5*MBhat^3*Ycut^5 - 630*MBhat^4*Ycut^5 + 2325*m2*MBhat^4*
            Ycut^5 + 1299*m2^2*MBhat^4*Ycut^5 + 177*m2^3*MBhat^4*Ycut^5 - 
           216*m2^4*MBhat^4*Ycut^5 + 840*MBhat^5*Ycut^5 + 
           960*m2*MBhat^5*Ycut^5 + 180*m2^2*MBhat^5*Ycut^5 - 
           225*m2^3*MBhat^5*Ycut^5 + 150*MBhat^6*Ycut^5 + 
           50*m2*MBhat^6*Ycut^5 - 80*m2^2*MBhat^6*Ycut^5 - 840*Ycut^6 + 
           260*m2*Ycut^6 + 444*m2^2*Ycut^6 - 84*m2^3*Ycut^6 - 
           20*m2^4*Ycut^6 - 2235*MBhat*Ycut^6 + 1695*m2*MBhat*Ycut^6 + 
           552*m2^2*MBhat*Ycut^6 - 273*m2^3*MBhat*Ycut^6 - 
           27*m2^4*MBhat*Ycut^6 + 1350*MBhat^2*Ycut^6 + 2145*m2*MBhat^2*
            Ycut^6 - 912*m2^2*MBhat^2*Ycut^6 - 234*m2^3*MBhat^2*Ycut^6 - 
           18*m2^4*MBhat^2*Ycut^6 + 2310*MBhat^3*Ycut^6 - 
           550*m2*MBhat^3*Ycut^6 - 828*m2^2*MBhat^3*Ycut^6 - 
           90*m2^3*MBhat^3*Ycut^6 - 2*m2^4*MBhat^3*Ycut^6 - 
           210*MBhat^4*Ycut^6 - 1035*m2*MBhat^4*Ycut^6 - 186*m2^2*MBhat^4*
            Ycut^6 - 9*m2^3*MBhat^4*Ycut^6 - 315*MBhat^5*Ycut^6 - 
           195*m2*MBhat^5*Ycut^6 - 15*m2^2*MBhat^5*Ycut^6 - 
           60*MBhat^6*Ycut^6 - 10*m2*MBhat^6*Ycut^6 + 770*Ycut^7 - 
           230*m2*Ycut^7 - 176*m2^2*Ycut^7 + 20*m2^3*Ycut^7 + 
           810*MBhat*Ycut^7 - 897*m2*MBhat*Ycut^7 + 60*m2^2*MBhat*Ycut^7 + 
           27*m2^3*MBhat*Ycut^7 - 1296*MBhat^2*Ycut^7 - 411*m2*MBhat^2*
            Ycut^7 + 252*m2^2*MBhat^2*Ycut^7 + 18*m2^3*MBhat^2*Ycut^7 - 
           564*MBhat^3*Ycut^7 + 470*m2*MBhat^3*Ycut^7 + 92*m2^2*MBhat^3*
            Ycut^7 + 2*m2^3*MBhat^3*Ycut^7 + 180*MBhat^4*Ycut^7 + 
           195*m2*MBhat^4*Ycut^7 + 9*m2^2*MBhat^4*Ycut^7 + 
           90*MBhat^5*Ycut^7 + 15*m2*MBhat^5*Ycut^7 + 10*MBhat^6*Ycut^7 - 
           420*Ycut^8 + 106*m2*Ycut^8 + 20*m2^2*Ycut^8 - 9*MBhat*Ycut^8 + 
           228*m2*MBhat*Ycut^8 - 27*m2^2*MBhat*Ycut^8 + 474*MBhat^2*Ycut^8 - 
           45*m2*MBhat^2*Ycut^8 - 18*m2^2*MBhat^2*Ycut^8 + 
           24*MBhat^3*Ycut^8 - 94*m2*MBhat^3*Ycut^8 - 2*m2^2*MBhat^3*Ycut^8 - 
           54*MBhat^4*Ycut^8 - 9*m2*MBhat^4*Ycut^8 - 15*MBhat^5*Ycut^8 + 
           126*Ycut^9 - 20*m2*Ycut^9 - 84*MBhat*Ycut^9 - 18*m2*MBhat*Ycut^9 - 
           63*MBhat^2*Ycut^9 + 18*m2*MBhat^2*Ycut^9 + 12*MBhat^3*Ycut^9 + 
           2*m2*MBhat^3*Ycut^9 + 9*MBhat^4*Ycut^9 - 16*Ycut^10 + 
           18*MBhat*Ycut^10 - 2*MBhat^3*Ycut^10)*c[T])/(-1 + Ycut)^7 + 
       c[SR]*((Sqrt[m2]*(-1 + m2 + Ycut)*(542 + 6436*m2 + 11886*m2^2 + 
            3886*m2^3 - 64*m2^4 - 6*m2^5 - 3084*MBhat - 26196*m2*MBhat - 
            28896*m2^2*MBhat - 2496*m2^3*MBhat + 204*m2^4*MBhat - 
            12*m2^5*MBhat + 7242*MBhat^2 + 41427*m2*MBhat^2 + 
            22752*m2^2*MBhat^2 - 948*m2^3*MBhat^2 + 102*m2^4*MBhat^2 - 
            15*m2^5*MBhat^2 - 8960*MBhat^3 - 31672*m2*MBhat^3 - 
            5172*m2^2*MBhat^3 + 428*m2^3*MBhat^3 + 28*m2^4*MBhat^3 - 
            12*m2^5*MBhat^3 + 6150*MBhat^4 + 11685*m2*MBhat^4 - 
            915*m2^2*MBhat^4 + 225*m2^3*MBhat^4 - 45*m2^4*MBhat^4 - 
            2220*MBhat^5 - 1740*m2*MBhat^5 + 420*m2^2*MBhat^5 - 
            60*m2^3*MBhat^5 + 330*MBhat^6 + 60*m2*MBhat^6 - 30*m2^2*MBhat^6 - 
            2288*Ycut - 28142*m2*Ycut - 53706*m2^2*Ycut - 18220*m2^3*Ycut + 
            266*m2^4*Ycut + 30*m2^5*Ycut + 13056*MBhat*Ycut + 
            115380*m2*MBhat*Ycut + 132384*m2^2*MBhat*Ycut + 
            12288*m2^3*MBhat*Ycut - 1008*m2^4*MBhat*Ycut + 
            60*m2^5*MBhat*Ycut - 30768*MBhat^2*Ycut - 184206*m2*MBhat^2*
             Ycut - 106419*m2^2*MBhat^2*Ycut + 4293*m2^3*MBhat^2*Ycut - 
            495*m2^4*MBhat^2*Ycut + 75*m2^5*MBhat^2*Ycut + 
            38240*MBhat^3*Ycut + 142688*m2*MBhat^3*Ycut + 25416*m2^2*MBhat^3*
             Ycut - 2156*m2^3*MBhat^3*Ycut - 128*m2^4*MBhat^3*Ycut + 
            60*m2^5*MBhat^3*Ycut - 26400*MBhat^4*Ycut - 53730*m2*MBhat^4*
             Ycut + 4035*m2^2*MBhat^4*Ycut - 1080*m2^3*MBhat^4*Ycut + 
            225*m2^4*MBhat^4*Ycut + 9600*MBhat^5*Ycut + 8340*m2*MBhat^5*
             Ycut - 2040*m2^2*MBhat^5*Ycut + 300*m2^3*MBhat^5*Ycut - 
            1440*MBhat^6*Ycut - 330*m2*MBhat^6*Ycut + 150*m2^2*MBhat^6*Ycut + 
            3672*Ycut^2 + 47160*m2*Ycut^2 + 93854*m2^2*Ycut^2 + 
            33434*m2^3*Ycut^2 - 400*m2^4*Ycut^2 - 60*m2^5*Ycut^2 - 
            21024*MBhat*Ycut^2 - 195084*m2*MBhat*Ycut^2 - 235500*m2^2*MBhat*
             Ycut^2 - 24012*m2^3*MBhat*Ycut^2 + 1980*m2^4*MBhat*Ycut^2 - 
            120*m2^5*MBhat*Ycut^2 + 49752*MBhat^2*Ycut^2 + 315126*m2*MBhat^2*
             Ycut^2 + 194487*m2^2*MBhat^2*Ycut^2 - 7440*m2^3*MBhat^2*Ycut^2 + 
            945*m2^4*MBhat^2*Ycut^2 - 150*m2^5*MBhat^2*Ycut^2 - 
            62160*MBhat^3*Ycut^2 - 248112*m2*MBhat^3*Ycut^2 - 
            49496*m2^2*MBhat^3*Ycut^2 + 4348*m2^3*MBhat^3*Ycut^2 + 
            220*m2^4*MBhat^3*Ycut^2 - 120*m2^5*MBhat^3*Ycut^2 + 
            43200*MBhat^4*Ycut^2 + 95850*m2*MBhat^4*Ycut^2 - 
            6675*m2^2*MBhat^4*Ycut^2 + 2025*m2^3*MBhat^4*Ycut^2 - 
            450*m2^4*MBhat^4*Ycut^2 - 15840*MBhat^5*Ycut^2 - 
            15660*m2*MBhat^5*Ycut^2 + 3900*m2^2*MBhat^5*Ycut^2 - 
            600*m2^3*MBhat^5*Ycut^2 + 2400*MBhat^6*Ycut^2 + 
            720*m2*MBhat^6*Ycut^2 - 300*m2^2*MBhat^6*Ycut^2 - 2688*Ycut^3 - 
            36558*m2*Ycut^3 - 77104*m2^2*Ycut^3 - 29470*m2^3*Ycut^3 + 
            230*m2^4*Ycut^3 + 60*m2^5*Ycut^3 + 15456*MBhat*Ycut^3 + 
            153012*m2*MBhat*Ycut^3 + 198312*m2^2*MBhat*Ycut^3 + 
            23100*m2^3*MBhat*Ycut^3 - 1920*m2^4*MBhat*Ycut^3 + 
            120*m2^5*MBhat*Ycut^3 - 36768*MBhat^2*Ycut^3 - 251022*m2*MBhat^2*
             Ycut^3 - 170115*m2^2*MBhat^2*Ycut^3 + 5865*m2^3*MBhat^2*Ycut^3 - 
            870*m2^4*MBhat^2*Ycut^3 + 150*m2^5*MBhat^2*Ycut^3 + 
            46170*MBhat^3*Ycut^3 + 202248*m2*MBhat^3*Ycut^3 + 
            46852*m2^2*MBhat^3*Ycut^3 - 4100*m2^3*MBhat^3*Ycut^3 - 
            230*m2^4*MBhat^3*Ycut^3 + 120*m2^5*MBhat^3*Ycut^3 - 
            32220*MBhat^4*Ycut^3 - 81270*m2*MBhat^4*Ycut^3 + 
            5295*m2^2*MBhat^4*Ycut^3 - 1980*m2^3*MBhat^4*Ycut^3 + 
            450*m2^4*MBhat^4*Ycut^3 + 11850*MBhat^5*Ycut^3 + 
            14400*m2*MBhat^5*Ycut^3 - 3750*m2^2*MBhat^5*Ycut^3 + 
            600*m2^3*MBhat^5*Ycut^3 - 1800*MBhat^6*Ycut^3 - 
            810*m2*MBhat^6*Ycut^3 + 300*m2^2*MBhat^6*Ycut^3 + 792*Ycut^4 + 
            11799*m2*Ycut^4 + 27395*m2^2*Ycut^4 + 11825*m2^3*Ycut^4 + 
            5*m2^4*Ycut^4 - 30*m2^5*Ycut^4 - 4584*MBhat*Ycut^4 - 
            50292*m2*MBhat*Ycut^4 - 73380*m2^2*MBhat*Ycut^4 - 
            10680*m2^3*MBhat*Ycut^4 + 900*m2^4*MBhat*Ycut^4 - 
            60*m2^5*MBhat*Ycut^4 + 11097*MBhat^2*Ycut^4 + 84090*m2*MBhat^2*
             Ycut^4 + 67665*m2^2*MBhat^2*Ycut^4 - 2130*m2^3*MBhat^2*Ycut^4 + 
            465*m2^4*MBhat^2*Ycut^4 - 75*m2^5*MBhat^2*Ycut^4 - 
            14010*MBhat^3*Ycut^4 - 70272*m2*MBhat^3*Ycut^4 - 
            21260*m2^2*MBhat^3*Ycut^4 + 1900*m2^3*MBhat^3*Ycut^4 + 
            130*m2^4*MBhat^3*Ycut^4 - 60*m2^5*MBhat^3*Ycut^4 + 
            9555*MBhat^4*Ycut^4 + 30675*m2*MBhat^4*Ycut^4 - 
            2010*m2^2*MBhat^4*Ycut^4 + 1050*m2^3*MBhat^4*Ycut^4 - 
            225*m2^4*MBhat^4*Ycut^4 - 3300*MBhat^5*Ycut^4 - 
            6510*m2*MBhat^5*Ycut^4 + 1890*m2^2*MBhat^5*Ycut^4 - 
            300*m2^3*MBhat^5*Ycut^4 + 450*MBhat^6*Ycut^4 + 
            510*m2*MBhat^6*Ycut^4 - 150*m2^2*MBhat^6*Ycut^4 - 24*Ycut^5 - 
            558*m2*Ycut^5 - 1903*m2^2*Ycut^5 - 1258*m2^3*Ycut^5 - 
            43*m2^4*Ycut^5 + 6*m2^5*Ycut^5 + 81*MBhat*Ycut^5 + 
            2808*m2*MBhat*Ycut^5 + 5478*m2^2*MBhat*Ycut^5 + 
            1908*m2^3*MBhat*Ycut^5 - 207*m2^4*MBhat*Ycut^5 + 
            12*m2^5*MBhat*Ycut^5 - 510*MBhat^2*Ycut^5 - 4155*m2*MBhat^2*
             Ycut^5 - 7260*m2^2*MBhat^2*Ycut^5 + 270*m2^3*MBhat^2*Ycut^5 - 
            120*m2^4*MBhat^2*Ycut^5 + 15*m2^5*MBhat^2*Ycut^5 + 
            933*MBhat^3*Ycut^5 + 3270*m2*MBhat^3*Ycut^5 + 3850*m2^2*MBhat^3*
             Ycut^5 - 470*m2^3*MBhat^3*Ycut^5 - 35*m2^4*MBhat^3*Ycut^5 + 
            12*m2^5*MBhat^3*Ycut^5 - 420*MBhat^4*Ycut^5 - 2355*m2*MBhat^4*
             Ycut^5 + 135*m2^2*MBhat^4*Ycut^5 - 255*m2^3*MBhat^4*Ycut^5 + 
            45*m2^4*MBhat^4*Ycut^5 - 180*MBhat^5*Ycut^5 + 1170*m2*MBhat^5*
             Ycut^5 - 450*m2^2*MBhat^5*Ycut^5 + 60*m2^3*MBhat^5*Ycut^5 + 
            120*MBhat^6*Ycut^5 - 180*m2*MBhat^6*Ycut^5 + 30*m2^2*MBhat^6*
             Ycut^5 + 10*Ycut^6 - 148*m2*Ycut^6 - 211*m2^2*Ycut^6 - 
            209*m2^3*Ycut^6 + 18*m2^4*Ycut^6 + 168*MBhat*Ycut^6 - 
            39*m2*MBhat*Ycut^6 + 1389*m2^2*MBhat*Ycut^6 - 93*m2^3*MBhat*
             Ycut^6 + 15*m2^4*MBhat*Ycut^6 - 216*MBhat^2*Ycut^6 - 
            381*m2*MBhat^2*Ycut^6 - 1191*m2^2*MBhat^2*Ycut^6 + 
            99*m2^3*MBhat^2*Ycut^6 + 9*m2^4*MBhat^2*Ycut^6 - 
            232*MBhat^3*Ycut^6 + 1393*m2*MBhat^3*Ycut^6 - 137*m2^2*MBhat^3*
             Ycut^6 + 53*m2^3*MBhat^3*Ycut^6 + 3*m2^4*MBhat^3*Ycut^6 + 
            330*MBhat^4*Ycut^6 - 885*m2*MBhat^4*Ycut^6 + 150*m2^2*MBhat^4*
             Ycut^6 + 15*m2^3*MBhat^4*Ycut^6 + 30*m2*MBhat^5*Ycut^6 + 
            30*m2^2*MBhat^5*Ycut^6 - 60*MBhat^6*Ycut^6 + 30*m2*MBhat^6*
             Ycut^6 - 40*Ycut^7 + 92*m2*Ycut^7 - 199*m2^2*Ycut^7 + 
            12*m2^3*Ycut^7 - 42*MBhat*Ycut^7 + 189*m2*MBhat*Ycut^7 + 
            228*m2^2*MBhat*Ycut^7 - 15*m2^3*MBhat*Ycut^7 + 
            264*MBhat^2*Ycut^7 - 747*m2*MBhat^2*Ycut^7 + 72*m2^2*MBhat^2*
             Ycut^7 - 9*m2^3*MBhat^2*Ycut^7 - 152*MBhat^3*Ycut^7 + 
            481*m2*MBhat^3*Ycut^7 - 56*m2^2*MBhat^3*Ycut^7 - 
            3*m2^3*MBhat^3*Ycut^7 - 120*MBhat^4*Ycut^7 + 15*m2*MBhat^4*
             Ycut^7 - 15*m2^2*MBhat^4*Ycut^7 + 90*MBhat^5*Ycut^7 - 
            30*m2*MBhat^5*Ycut^7 + 30*Ycut^8 - 63*m2*Ycut^8 - 
            12*m2^2*Ycut^8 - 72*MBhat*Ycut^8 + 207*m2*MBhat*Ycut^8 - 
            15*m2^2*MBhat*Ycut^8 - 21*MBhat^2*Ycut^8 - 138*m2*MBhat^2*
             Ycut^8 + 9*m2^2*MBhat^2*Ycut^8 + 138*MBhat^3*Ycut^8 - 
            21*m2*MBhat^3*Ycut^8 + 3*m2^2*MBhat^3*Ycut^8 - 
            75*MBhat^4*Ycut^8 + 15*m2*MBhat^4*Ycut^8 - 18*m2*Ycut^9 + 
            33*MBhat*Ycut^9 + 15*m2*MBhat*Ycut^9 - 66*MBhat^2*Ycut^9 + 
            6*m2*MBhat^2*Ycut^9 + 33*MBhat^3*Ycut^9 - 3*m2*MBhat^3*Ycut^9 - 
            6*Ycut^10 + 12*MBhat*Ycut^10 - 6*MBhat^2*Ycut^10))/
          (10*(-1 + Ycut)^5) - 6*Sqrt[m2]*(-2 - 47*m2 - 170*m2^2 - 140*m2^3 - 
           20*m2^4 + m2^5 + 12*MBhat + 216*m2*MBhat + 540*m2^2*MBhat + 
           240*m2^3*MBhat - 30*MBhat^2 - 399*m2*MBhat^2 - 639*m2^2*MBhat^2 - 
           114*m2^3*MBhat^2 + 6*m2^4*MBhat^2 + 40*MBhat^3 + 376*m2*MBhat^3 + 
           340*m2^2*MBhat^3 - 30*MBhat^4 - 189*m2*MBhat^4 - 72*m2^2*MBhat^4 + 
           6*m2^3*MBhat^4 + 12*MBhat^5 + 48*m2*MBhat^5 - 2*MBhat^6 - 
           5*m2*MBhat^6 + m2^2*MBhat^6)*Log[m2] + 6*Sqrt[m2]*
          (-2 - 47*m2 - 170*m2^2 - 140*m2^3 - 20*m2^4 + m2^5 + 12*MBhat + 
           216*m2*MBhat + 540*m2^2*MBhat + 240*m2^3*MBhat - 30*MBhat^2 - 
           399*m2*MBhat^2 - 639*m2^2*MBhat^2 - 114*m2^3*MBhat^2 + 
           6*m2^4*MBhat^2 + 40*MBhat^3 + 376*m2*MBhat^3 + 340*m2^2*MBhat^3 - 
           30*MBhat^4 - 189*m2*MBhat^4 - 72*m2^2*MBhat^4 + 6*m2^3*MBhat^4 + 
           12*MBhat^5 + 48*m2*MBhat^5 - 2*MBhat^6 - 5*m2*MBhat^6 + 
           m2^2*MBhat^6)*Log[1 - Ycut])) + 
     c[VL]*(-1/210*((-1 + m2 + Ycut)*(-345 - 12378*m2 + 16497*m2^2 + 
           49572*m2^3 - 6603*m2^4 - 1458*m2^5 + 75*m2^6 + 2160*MBhat + 
           36432*m2*MBhat - 138456*m2^2*MBhat - 75456*m2^3*MBhat + 
           16944*m2^4*MBhat - 3216*m2^5*MBhat + 312*m2^6*MBhat - 
           5810*MBhat^2 - 24150*m2*MBhat^2 + 233730*m2^2*MBhat^2 - 
           30660*m2^3*MBhat^2 + 7350*m2^4*MBhat^2 - 4830*m2^5*MBhat^2 + 
           770*m2^6*MBhat^2 + 8832*MBhat^3 - 23760*m2*MBhat^3 - 
           114480*m2^2*MBhat^3 + 30000*m2^3*MBhat^3 + 3120*m2^4*MBhat^3 - 
           5952*m2^5*MBhat^3 + 1440*m2^6*MBhat^3 - 7735*MBhat^4 + 
           36890*m2*MBhat^4 - 15190*m2^2*MBhat^4 + 20090*m2^3*MBhat^4 - 
           16975*m2^4*MBhat^4 + 3920*m2^5*MBhat^4 + 3528*MBhat^5 - 
           14112*m2*MBhat^5 + 21168*m2^2*MBhat^5 - 14112*m2^3*MBhat^5 + 
           3528*m2^4*MBhat^5 - 630*MBhat^6 + 1050*m2*MBhat^6 - 
           3990*m2^2*MBhat^6 + 1050*m2^3*MBhat^6 + 2070*Ycut + 
           78963*m2*Ycut - 100365*m2^2*Ycut - 326418*m2^3*Ycut + 
           41304*m2^4*Ycut + 10131*m2^5*Ycut - 525*m2^6*Ycut - 
           12960*MBhat*Ycut - 236592*m2*MBhat*Ycut + 879408*m2^2*MBhat*Ycut + 
           514152*m2^3*MBhat*Ycut - 115704*m2^4*MBhat*Ycut + 
           22200*m2^5*MBhat*Ycut - 2184*m2^6*MBhat*Ycut + 
           34860*MBhat^2*Ycut + 169330*m2*MBhat^2*Ycut - 1518020*m2^2*MBhat^2*
            Ycut + 186970*m2^3*MBhat^2*Ycut - 47390*m2^4*MBhat^2*Ycut + 
           33040*m2^5*MBhat^2*Ycut - 5390*m2^6*MBhat^2*Ycut - 
           52992*MBhat^3*Ycut + 131232*m2*MBhat^3*Ycut + 772752*m2^2*MBhat^3*
            Ycut - 208608*m2^3*MBhat^3*Ycut - 17328*m2^4*MBhat^3*Ycut + 
           40224*m2^5*MBhat^3*Ycut - 10080*m2^6*MBhat^3*Ycut + 
           46410*MBhat^4*Ycut - 224035*m2*MBhat^4*Ycut + 81235*m2^2*MBhat^4*
            Ycut - 127575*m2^3*MBhat^4*Ycut + 114905*m2^4*MBhat^4*Ycut - 
           27440*m2^5*MBhat^4*Ycut - 21168*MBhat^5*Ycut + 88200*m2*MBhat^5*
            Ycut - 137592*m2^2*MBhat^5*Ycut + 95256*m2^3*MBhat^5*Ycut - 
           24696*m2^4*MBhat^5*Ycut + 3780*MBhat^6*Ycut - 6930*m2*MBhat^6*
            Ycut + 26880*m2^2*MBhat^6*Ycut - 7350*m2^3*MBhat^6*Ycut - 
           5175*Ycut^2 - 211665*m2*Ycut^2 + 253395*m2^2*Ycut^2 + 
           908202*m2^3*Ycut^2 - 107319*m2^4*Ycut^2 - 30093*m2^5*Ycut^2 + 
           1575*m2^6*Ycut^2 + 32400*MBhat*Ycut^2 + 646560*m2*MBhat*Ycut^2 - 
           2343240*m2^2*MBhat*Ycut^2 - 1488888*m2^3*MBhat*Ycut^2 + 
           335808*m2^4*MBhat*Ycut^2 - 65352*m2^5*MBhat*Ycut^2 + 
           6552*m2^6*MBhat*Ycut^2 - 87150*MBhat^2*Ycut^2 - 
           499520*m2*MBhat^2*Ycut^2 + 4151420*m2^2*MBhat^2*Ycut^2 - 
           465780*m2^3*MBhat^2*Ycut^2 + 126700*m2^4*MBhat^2*Ycut^2 - 
           96040*m2^5*MBhat^2*Ycut^2 + 16170*m2^6*MBhat^2*Ycut^2 + 
           132480*MBhat^3*Ycut^2 - 289680*m2*MBhat^3*Ycut^2 - 
           2208288*m2^2*MBhat^3*Ycut^2 + 617184*m2^3*MBhat^3*Ycut^2 + 
           35376*m2^4*MBhat^3*Ycut^2 - 114912*m2^5*MBhat^3*Ycut^2 + 
           30240*m2^6*MBhat^3*Ycut^2 - 116025*MBhat^4*Ycut^2 + 
           564305*m2*MBhat^4*Ycut^2 - 161490*m2^2*MBhat^4*Ycut^2 + 
           334425*m2^3*MBhat^4*Ycut^2 - 329035*m2^4*MBhat^4*Ycut^2 + 
           82320*m2^5*MBhat^4*Ycut^2 + 52920*MBhat^5*Ycut^2 - 
           229320*m2*MBhat^5*Ycut^2 + 373968*m2^2*MBhat^5*Ycut^2 - 
           271656*m2^3*MBhat^5*Ycut^2 + 74088*m2^4*MBhat^5*Ycut^2 - 
           9450*MBhat^6*Ycut^2 + 18900*m2*MBhat^6*Ycut^2 - 
           76440*m2^2*MBhat^6*Ycut^2 + 22050*m2^3*MBhat^6*Ycut^2 + 
           6900*Ycut^3 + 306270*m2*Ycut^3 - 338610*m2^2*Ycut^3 - 
           1374483*m2^3*Ycut^3 + 147273*m2^4*Ycut^3 + 49455*m2^5*Ycut^3 - 
           2625*m2^6*Ycut^3 - 43200*MBhat*Ycut^3 - 955680*m2*MBhat*Ycut^3 + 
           3361440*m2^2*MBhat*Ycut^3 + 2363952*m2^3*MBhat*Ycut^3 - 
           534240*m2^4*MBhat*Ycut^3 + 106008*m2^5*MBhat*Ycut^3 - 
           10920*m2^6*MBhat*Ycut^3 + 116200*MBhat^2*Ycut^3 + 
           797860*m2*MBhat^2*Ycut^3 - 6143760*m2^2*MBhat^2*Ycut^3 + 
           591850*m2^3*MBhat^2*Ycut^3 - 177380*m2^4*MBhat^2*Ycut^3 + 
           152880*m2^5*MBhat^2*Ycut^3 - 26950*m2^6*MBhat^2*Ycut^3 - 
           178740*MBhat^3*Ycut^3 + 315300*m2*MBhat^3*Ycut^3 + 
           3441432*m2^2*MBhat^3*Ycut^3 - 989784*m2^3*MBhat^3*Ycut^3 - 
           45108*m2^4*MBhat^3*Ycut^3 + 186900*m2^5*MBhat^3*Ycut^3 - 
           50400*m2^6*MBhat^3*Ycut^3 + 158060*MBhat^4*Ycut^3 - 
           749630*m2*MBhat^4*Ycut^3 + 125230*m2^2*MBhat^4*Ycut^3 - 
           486675*m2^3*MBhat^4*Ycut^3 + 531965*m2^4*MBhat^4*Ycut^3 - 
           137200*m2^5*MBhat^4*Ycut^3 - 71820*MBhat^5*Ycut^3 + 
           314580*m2*MBhat^5*Ycut^3 - 553812*m2^2*MBhat^5*Ycut^3 + 
           434532*m2^3*MBhat^5*Ycut^3 - 123480*m2^4*MBhat^5*Ycut^3 + 
           12600*MBhat^6*Ycut^3 - 27300*m2*MBhat^6*Ycut^3 + 
           120960*m2^2*MBhat^6*Ycut^3 - 36750*m2^3*MBhat^6*Ycut^3 - 
           5175*Ycut^4 - 254040*m2*Ycut^4 + 250500*m2^2*Ycut^4 + 
           1205967*m2^3*Ycut^4 - 110460*m2^4*Ycut^4 - 48405*m2^5*Ycut^4 + 
           2625*m2^6*Ycut^4 + 32400*MBhat*Ycut^4 + 812160*m2*MBhat*Ycut^4 - 
           2751360*m2^2*MBhat*Ycut^4 - 2201808*m2^3*MBhat*Ycut^4 + 
           497952*m2^4*MBhat*Ycut^4 - 101640*m2^5*MBhat*Ycut^4 + 
           10920*m2^6*MBhat*Ycut^4 - 84525*MBhat^2*Ycut^4 - 
           737800*m2*MBhat^2*Ycut^4 + 5234180*m2^2*MBhat^2*Ycut^4 - 
           400960*m2^3*MBhat^2*Ycut^4 + 165865*m2^4*MBhat^2*Ycut^4 - 
           155330*m2^5*MBhat^2*Ycut^4 + 26950*m2^6*MBhat^2*Ycut^4 + 
           141160*MBhat^3*Ycut^4 - 160940*m2*MBhat^3*Ycut^4 - 
           3148448*m2^2*MBhat^3*Ycut^4 + 978488*m2^3*MBhat^3*Ycut^4 + 
           6440*m2^4*MBhat^3*Ycut^4 - 180460*m2^5*MBhat^3*Ycut^4 + 
           50400*m2^6*MBhat^3*Ycut^4 - 134820*MBhat^4*Ycut^4 + 
           542500*m2*MBhat^4*Ycut^4 + 54740*m2^2*MBhat^4*Ycut^4 + 
           400995*m2^3*MBhat^4*Ycut^4 - 518665*m2^4*MBhat^4*Ycut^4 + 
           137200*m2^5*MBhat^4*Ycut^4 + 60480*MBhat^5*Ycut^4 - 
           230580*m2*MBhat^5*Ycut^4 + 475608*m2^2*MBhat^5*Ycut^4 - 
           422940*m2^3*MBhat^5*Ycut^4 + 123480*m2^4*MBhat^5*Ycut^4 - 
           9520*MBhat^6*Ycut^4 + 21980*m2*MBhat^6*Ycut^4 - 
           116620*m2^2*MBhat^6*Ycut^4 + 36750*m2^3*MBhat^6*Ycut^4 + 
           2070*Ycut^5 + 116391*m2*Ycut^5 - 94899*m2^2*Ycut^5 - 
           595602*m2^3*Ycut^5 + 39858*m2^4*Ycut^5 + 27993*m2^5*Ycut^5 - 
           1575*m2^6*Ycut^5 - 14430*MBhat*Ycut^5 - 381126*m2*MBhat*Ycut^5 + 
           1226820*m2^2*MBhat*Ycut^5 + 1195572*m2^3*MBhat*Ycut^5 - 
           286566*m2^4*MBhat*Ycut^5 + 64554*m2^5*MBhat*Ycut^5 - 
           6552*m2^6*MBhat*Ycut^5 + 21294*MBhat^2*Ycut^5 + 
           394079*m2*MBhat^2*Ycut^5 - 2473121*m2^2*MBhat^2*Ycut^5 + 
           108969*m2^3*MBhat^2*Ycut^5 - 98441*m2^4*MBhat^2*Ycut^5 + 
           96530*m2^5*MBhat^2*Ycut^5 - 16170*m2^6*MBhat^2*Ycut^5 - 
           61728*MBhat^3*Ycut^5 + 20680*m2*MBhat^3*Ycut^5 + 
           1690052*m2^2*MBhat^3*Ycut^5 - 657300*m2^3*MBhat^3*Ycut^5 + 
           60620*m2^4*MBhat^3*Ycut^5 + 101836*m2^5*MBhat^3*Ycut^5 - 
           30240*m2^6*MBhat^3*Ycut^5 + 88620*MBhat^4*Ycut^5 - 
           184730*m2*MBhat^4*Ycut^5 - 225120*m2^2*MBhat^4*Ycut^5 - 
           134295*m2^3*MBhat^4*Ycut^5 + 298655*m2^4*MBhat^4*Ycut^5 - 
           82320*m2^5*MBhat^4*Ycut^5 - 40026*MBhat^5*Ycut^5 + 
           64554*m2*MBhat^5*Ycut^5 - 217098*m2^2*MBhat^5*Ycut^5 + 
           247842*m2^3*MBhat^5*Ycut^5 - 74088*m2^4*MBhat^5*Ycut^5 + 
           4200*MBhat^6*Ycut^5 - 9100*m2*MBhat^6*Ycut^5 + 69160*m2^2*MBhat^6*
            Ycut^5 - 22050*m2^3*MBhat^6*Ycut^5 - 30*Ycut^6 - 
           24885*m2*Ycut^6 + 13986*m2^2*Ycut^6 + 135954*m2^3*Ycut^6 + 
           2457*m2^4*Ycut^6 - 10395*m2^5*Ycut^6 + 525*m2^6*Ycut^6 + 
           10476*MBhat*Ycut^6 + 72702*m2*MBhat*Ycut^6 - 230580*m2^2*MBhat*
            Ycut^6 - 352128*m2^3*MBhat*Ycut^6 + 103656*m2^4*MBhat*Ycut^6 - 
           24486*m2^5*MBhat*Ycut^6 + 2184*m2^6*MBhat*Ycut^6 + 
           20664*MBhat^2*Ycut^6 - 125965*m2*MBhat^2*Ycut^6 + 
           515634*m2^2*MBhat^2*Ycut^6 + 55573*m2^3*MBhat^2*Ycut^6 + 
           20482*m2^4*MBhat^2*Ycut^6 - 34398*m2^5*MBhat^2*Ycut^6 + 
           5390*m2^6*MBhat^2*Ycut^6 - 3432*MBhat^3*Ycut^6 + 
           7252*m2*MBhat^3*Ycut^6 - 488796*m2^2*MBhat^3*Ycut^6 + 
           308784*m2^3*MBhat^3*Ycut^6 - 73276*m2^4*MBhat^3*Ycut^6 - 
           29652*m2^5*MBhat^3*Ycut^6 + 10080*m2^6*MBhat^3*Ycut^6 - 
           54474*MBhat^4*Ycut^6 + 3346*m2*MBhat^4*Ycut^6 + 
           240016*m2^2*MBhat^4*Ycut^6 - 51009*m2^3*MBhat^4*Ycut^6 - 
           91189*m2^4*MBhat^4*Ycut^6 + 27440*m2^5*MBhat^4*Ycut^6 + 
           28476*MBhat^5*Ycut^6 + 28350*m2*MBhat^5*Ycut^6 + 
           21672*m2^2*MBhat^5*Ycut^6 - 79086*m2^3*MBhat^5*Ycut^6 + 
           24696*m2^4*MBhat^5*Ycut^6 - 1680*MBhat^6*Ycut^6 + 
           980*m2*MBhat^6*Ycut^6 - 23520*m2^2*MBhat^6*Ycut^6 + 
           7350*m2^3*MBhat^6*Ycut^6 - 1874*Ycut^7 + 3355*m2*Ycut^7 - 
           3869*m2^2*Ycut^7 + 3635*m2^3*Ycut^7 - 8923*m2^4*Ycut^7 + 
           2081*m2^5*Ycut^7 - 75*m2^6*Ycut^7 - 19044*MBhat*Ycut^7 + 
           20562*m2*MBhat*Ycut^7 - 8292*m2^2*MBhat*Ycut^7 + 
           41100*m2^3*MBhat*Ycut^7 - 18624*m2^4*MBhat*Ycut^7 + 
           4770*m2^5*MBhat*Ycut^7 - 312*m2^6*MBhat*Ycut^7 - 
           20958*MBhat^2*Ycut^7 + 41153*m2*MBhat^2*Ycut^7 + 
           15827*m2^2*MBhat^2*Ycut^7 - 63910*m2^3*MBhat^2*Ycut^7 + 
           6202*m2^4*MBhat^2*Ycut^7 + 5866*m2^5*MBhat^2*Ycut^7 - 
           770*m2^6*MBhat^2*Ycut^7 + 35562*MBhat^3*Ycut^7 - 
           2798*m2*MBhat^3*Ycut^7 + 47266*m2^2*MBhat^3*Ycut^7 - 
           95310*m2^3*MBhat^3*Ycut^7 + 39524*m2^4*MBhat^3*Ycut^7 + 
           2396*m2^5*MBhat^3*Ycut^7 - 1440*m2^6*MBhat^3*Ycut^7 + 
           23184*MBhat^4*Ycut^7 + 12040*m2*MBhat^4*Ycut^7 - 
           131614*m2^2*MBhat^4*Ycut^7 + 62307*m2^3*MBhat^4*Ycut^7 + 
           9443*m2^4*MBhat^4*Ycut^7 - 3920*m2^5*MBhat^4*Ycut^7 - 
           18270*MBhat^5*Ycut^7 - 31080*m2*MBhat^5*Ycut^7 + 
           25872*m2^2*MBhat^5*Ycut^7 + 9786*m2^3*MBhat^5*Ycut^7 - 
           3528*m2^4*MBhat^5*Ycut^7 + 1400*MBhat^6*Ycut^7 + 
           700*m2*MBhat^6*Ycut^7 + 3640*m2^2*MBhat^6*Ycut^7 - 
           1050*m2^3*MBhat^6*Ycut^7 + 4629*Ycut^8 - 5435*m2*Ycut^8 + 
           5711*m2^2*Ycut^8 - 7839*m2^3*Ycut^8 + 2453*m2^4*Ycut^8 - 
           149*m2^5*Ycut^8 + 21948*MBhat*Ycut^8 - 25530*m2*MBhat*Ycut^8 + 
           2928*m2^2*MBhat*Ycut^8 + 5388*m2^3*MBhat*Ycut^8 + 
           624*m2^4*MBhat*Ycut^8 - 318*m2^5*MBhat*Ycut^8 - 
           1260*MBhat^2*Ycut^8 - 18865*m2*MBhat^2*Ycut^8 - 
           12698*m2^2*MBhat^2*Ycut^8 + 19572*m2^3*MBhat^2*Ycut^8 - 
           3941*m2^4*MBhat^2*Ycut^8 - 238*m2^5*MBhat^2*Ycut^8 - 
           31092*MBhat^3*Ycut^8 + 6850*m2*MBhat^3*Ycut^8 + 
           11696*m2^2*MBhat^3*Ycut^8 + 18306*m2^3*MBhat^3*Ycut^8 - 
           10420*m2^4*MBhat^3*Ycut^8 + 460*m2^5*MBhat^3*Ycut^8 + 
           105*MBhat^4*Ycut^8 + 490*m2*MBhat^4*Ycut^8 + 35826*m2^2*MBhat^4*
            Ycut^8 - 20517*m2^3*MBhat^4*Ycut^8 + 896*m2^4*MBhat^4*Ycut^8 + 
           6720*MBhat^5*Ycut^8 + 10920*m2*MBhat^5*Ycut^8 - 
           11088*m2^2*MBhat^5*Ycut^8 + 378*m2^3*MBhat^5*Ycut^8 - 
           1050*MBhat^6*Ycut^8 - 350*m2*MBhat^6*Ycut^8 - 70*m2^2*MBhat^6*
            Ycut^8 - 6060*Ycut^9 + 5830*m2*Ycut^9 - 2844*m2^2*Ycut^9 + 
           972*m2^3*Ycut^9 - 40*m2^4*Ycut^9 - 12240*MBhat*Ycut^9 + 
           13470*m2*MBhat*Ycut^9 + 1068*m2^2*MBhat*Ycut^9 - 
           1944*m2^3*MBhat*Ycut^9 + 150*m2^4*MBhat*Ycut^9 + 
           12950*MBhat^2*Ycut^9 + 2975*m2*MBhat^2*Ycut^9 - 
           4263*m2^2*MBhat^2*Ycut^9 - 1631*m2^3*MBhat^2*Ycut^9 + 
           553*m2^4*MBhat^2*Ycut^9 + 10110*MBhat^3*Ycut^9 - 
           5300*m2*MBhat^3*Ycut^9 - 3264*m2^2*MBhat^3*Ycut^9 - 
           1758*m2^3*MBhat^3*Ycut^9 + 1052*m2^4*MBhat^3*Ycut^9 - 
           4550*MBhat^4*Ycut^9 - 1435*m2*MBhat^4*Ycut^9 - 3619*m2^2*MBhat^4*
            Ycut^9 + 2254*m2^3*MBhat^4*Ycut^9 - 630*MBhat^5*Ycut^9 - 
           1470*m2*MBhat^5*Ycut^9 + 1302*m2^2*MBhat^5*Ycut^9 + 
           420*MBhat^6*Ycut^9 + 70*m2*MBhat^6*Ycut^9 + 4405*Ycut^10 - 
           3058*m2*Ycut^10 + 503*m2^2*Ycut^10 + 40*m2^3*Ycut^10 + 
           1620*MBhat*Ycut^10 - 2886*m2*MBhat*Ycut^10 + 324*m2^2*MBhat*
            Ycut^10 + 60*m2^3*MBhat*Ycut^10 - 7434*MBhat^2*Ycut^10 + 
           1141*m2*MBhat^2*Ycut^10 + 1078*m2^2*MBhat^2*Ycut^10 + 
           7*m2^3*MBhat^2*Ycut^10 + 576*MBhat^3*Ycut^10 + 
           1240*m2*MBhat^3*Ycut^10 + 76*m2^2*MBhat^3*Ycut^10 - 
           2*m2^3*MBhat^3*Ycut^10 + 1155*MBhat^4*Ycut^10 + 
           245*m2*MBhat^4*Ycut^10 - 14*m2^2*MBhat^4*Ycut^10 - 
           252*MBhat^5*Ycut^10 - 42*m2*MBhat^5*Ycut^10 - 70*MBhat^6*Ycut^10 - 
           1650*Ycut^11 + 647*m2*Ycut^11 - 5*m2^2*Ycut^11 + 
           1284*MBhat*Ycut^11 - 90*m2*MBhat*Ycut^11 - 60*m2^2*MBhat*Ycut^11 + 
           966*MBhat^2*Ycut^11 - 245*m2*MBhat^2*Ycut^11 - 
           7*m2^2*MBhat^2*Ycut^11 - 726*MBhat^3*Ycut^11 - 
           74*m2*MBhat^3*Ycut^11 + 2*m2^2*MBhat^3*Ycut^11 + 
           84*MBhat^4*Ycut^11 + 14*m2*MBhat^4*Ycut^11 + 42*MBhat^5*Ycut^11 + 
           219*Ycut^12 + 5*m2*Ycut^12 - 396*MBhat*Ycut^12 + 
           18*m2*MBhat*Ycut^12 + 203*MBhat^2*Ycut^12 + 7*m2*MBhat^2*Ycut^12 - 
           12*MBhat^3*Ycut^12 - 2*m2*MBhat^3*Ycut^12 - 14*MBhat^4*Ycut^12 + 
           16*Ycut^13 - 18*MBhat*Ycut^13 + 2*MBhat^3*Ycut^13))/
         (-1 + Ycut)^7 + 2*m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 15*m2^4 - 
         48*MBhat + 72*m2*MBhat + 360*m2^2*MBhat + 72*MBhat^2 - 
         276*m2*MBhat^2 - 274*m2^2*MBhat^2 + 58*m2^3*MBhat^2 - 48*MBhat^3 + 
         288*m2*MBhat^3 + 12*MBhat^4 - 105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 
         6*m2*MBhat^6)*Log[m2] - 2*m2*(12 + 15*m2 - 105*m2^2 - 45*m2^3 + 
         15*m2^4 - 48*MBhat + 72*m2*MBhat + 360*m2^2*MBhat + 72*MBhat^2 - 
         276*m2*MBhat^2 - 274*m2^2*MBhat^2 + 58*m2^3*MBhat^2 - 48*MBhat^3 + 
         288*m2*MBhat^3 + 12*MBhat^4 - 105*m2*MBhat^4 + 43*m2^2*MBhat^4 + 
         6*m2*MBhat^6)*Log[1 - Ycut] + 
       c[VR]*(-1/15*(Sqrt[m2]*(-1 + m2 + Ycut)*(-460 - 430*m2 + 8270*m2^2 + 
             5470*m2^3 - 230*m2^4 - 20*m2^5 + 2484*MBhat - 4464*m2*MBhat - 
             33264*m2^2*MBhat - 5664*m2^3*MBhat + 636*m2^4*MBhat - 
             48*m2^5*MBhat - 5386*MBhat^2 + 19589*m2*MBhat^2 + 
             37124*m2^2*MBhat^2 - 3016*m2^3*MBhat^2 + 494*m2^4*MBhat^2 - 
             85*m2^5*MBhat^2 + 5832*MBhat^3 - 26904*m2*MBhat^3 - 
             10464*m2^2*MBhat^3 + 416*m2^3*MBhat^3 + 536*m2^4*MBhat^3 - 
             136*m2^5*MBhat^3 - 3300*MBhat^4 + 15825*m2*MBhat^4 - 
             3855*m2^2*MBhat^4 + 1605*m2^3*MBhat^4 - 375*m2^4*MBhat^4 + 
             960*MBhat^5 - 3720*m2*MBhat^5 + 1680*m2^2*MBhat^5 - 
             360*m2^3*MBhat^5 - 130*MBhat^6 + 140*m2*MBhat^6 - 
             130*m2^2*MBhat^6 + 1960*Ycut + 2280*m2*Ycut - 36550*m2^2*Ycut - 
             25480*m2^3*Ycut + 990*m2^4*Ycut + 100*m2^5*Ycut - 
             10656*MBhat*Ycut + 17460*m2*MBhat*Ycut + 149796*m2^2*MBhat*
              Ycut + 27732*m2^3*MBhat*Ycut - 3132*m2^4*MBhat*Ycut + 
             240*m2^5*MBhat*Ycut + 23344*MBhat^2*Ycut - 82482*m2*MBhat^2*
              Ycut - 171733*m2^2*MBhat^2*Ycut + 13591*m2^3*MBhat^2*Ycut - 
             2385*m2^4*MBhat^2*Ycut + 425*m2^5*MBhat^2*Ycut - 
             25728*MBhat^3*Ycut + 116808*m2*MBhat^3*Ycut + 51504*m2^2*MBhat^3*
              Ycut - 2480*m2^3*MBhat^3*Ycut - 2544*m2^4*MBhat^3*Ycut + 
             680*m2^5*MBhat^3*Ycut + 15000*MBhat^4*Ycut - 70740*m2*MBhat^4*
              Ycut + 16965*m2^2*MBhat^4*Ycut - 7650*m2^3*MBhat^4*Ycut + 
             1875*m2^4*MBhat^4*Ycut - 4560*MBhat^5*Ycut + 17280*m2*MBhat^5*
              Ycut - 8040*m2^2*MBhat^5*Ycut + 1800*m2^3*MBhat^5*Ycut + 
             640*MBhat^6*Ycut - 750*m2*MBhat^6*Ycut + 650*m2^2*MBhat^6*Ycut - 
             3180*Ycut^2 - 4650*m2*Ycut^2 + 62000*m2^2*Ycut^2 + 
             46320*m2^3*Ycut^2 - 1590*m2^4*Ycut^2 - 200*m2^5*Ycut^2 + 
             17424*MBhat*Ycut^2 - 24876*m2*MBhat*Ycut^2 - 260280*m2^2*MBhat*
              Ycut^2 - 53748*m2^3*MBhat*Ycut^2 + 6120*m2^4*MBhat*Ycut^2 - 
             480*m2^5*MBhat*Ycut^2 - 38616*MBhat^2*Ycut^2 + 131082*m2*MBhat^2*
              Ycut^2 + 308849*m2^2*MBhat^2*Ycut^2 - 23340*m2^3*MBhat^2*
              Ycut^2 + 4515*m2^4*MBhat^2*Ycut^2 - 850*m2^5*MBhat^2*Ycut^2 + 
             43392*MBhat^3*Ycut^2 - 193080*m2*MBhat^3*Ycut^2 - 
             100296*m2^2*MBhat^3*Ycut^2 + 6024*m2^3*MBhat^3*Ycut^2 + 
             4680*m2^4*MBhat^3*Ycut^2 - 1360*m2^5*MBhat^3*Ycut^2 - 
             26100*MBhat^4*Ycut^2 + 121140*m2*MBhat^4*Ycut^2 - 
             27915*m2^2*MBhat^4*Ycut^2 + 14175*m2^3*MBhat^4*Ycut^2 - 
             3750*m2^4*MBhat^4*Ycut^2 + 8280*MBhat^5*Ycut^2 - 
             30960*m2*MBhat^5*Ycut^2 + 15000*m2^2*MBhat^5*Ycut^2 - 
             3600*m2^3*MBhat^5*Ycut^2 - 1200*MBhat^6*Ycut^2 + 
             1560*m2*MBhat^6*Ycut^2 - 1300*m2^2*MBhat^6*Ycut^2 + 
             2360*Ycut^3 + 4460*m2*Ycut^3 - 48740*m2^2*Ycut^3 - 
             40220*m2^3*Ycut^3 + 1090*m2^4*Ycut^3 + 200*m2^5*Ycut^3 - 
             13056*MBhat*Ycut^3 + 14628*m2*MBhat*Ycut^3 + 211548*m2^2*MBhat*
              Ycut^3 + 51000*m2^3*MBhat*Ycut^3 - 5880*m2^4*MBhat*Ycut^3 + 
             480*m2^5*MBhat*Ycut^3 + 29344*MBhat^2*Ycut^3 - 93554*m2*MBhat^2*
              Ycut^3 - 263205*m2^2*MBhat^2*Ycut^3 + 18035*m2^3*MBhat^2*
              Ycut^3 - 4090*m2^4*MBhat^2*Ycut^3 + 850*m2^5*MBhat^2*Ycut^3 - 
             33338*MBhat^3*Ycut^3 + 144712*m2*MBhat^3*Ycut^3 + 
             95836*m2^2*MBhat^3*Ycut^3 - 7240*m2^3*MBhat^3*Ycut^3 - 
             4210*m2^4*MBhat^3*Ycut^3 + 1360*m2^5*MBhat^3*Ycut^3 + 
             20340*MBhat^4*Ycut^3 - 94860*m2*MBhat^4*Ycut^3 + 
             20085*m2^2*MBhat^4*Ycut^3 - 12840*m2^3*MBhat^4*Ycut^3 + 
             3750*m2^4*MBhat^4*Ycut^3 - 6690*MBhat^5*Ycut^3 + 
             25980*m2*MBhat^5*Ycut^3 - 13650*m2^2*MBhat^5*Ycut^3 + 
             3600*m2^3*MBhat^5*Ycut^3 + 1040*MBhat^6*Ycut^3 - 
             1630*m2*MBhat^6*Ycut^3 + 1300*m2^2*MBhat^6*Ycut^3 - 710*Ycut^4 - 
             1875*m2*Ycut^4 + 15985*m2^2*Ycut^4 + 15665*m2^3*Ycut^4 - 
             195*m2^4*Ycut^4 - 100*m2^5*Ycut^4 + 3984*MBhat*Ycut^4 - 
             2268*m2*MBhat*Ycut^4 - 73320*m2^2*MBhat*Ycut^4 - 
             22920*m2^3*MBhat*Ycut^4 + 2700*m2^4*MBhat*Ycut^4 - 
             240*m2^5*MBhat*Ycut^4 - 9421*MBhat^2*Ycut^4 + 26190*m2*MBhat^2*
              Ycut^4 + 98535*m2^2*MBhat^2*Ycut^4 - 5570*m2^3*MBhat^2*Ycut^4 + 
             1935*m2^4*MBhat^2*Ycut^4 - 425*m2^5*MBhat^2*Ycut^4 + 
             9872*MBhat^3*Ycut^4 - 40746*m2*MBhat^3*Ycut^4 - 
             45770*m2^2*MBhat^3*Ycut^4 + 5530*m2^3*MBhat^3*Ycut^4 + 
             1650*m2^4*MBhat^3*Ycut^4 - 680*m2^5*MBhat^3*Ycut^4 - 
             4875*MBhat^4*Ycut^4 + 27435*m2*MBhat^4*Ycut^4 - 
             3480*m2^2*MBhat^4*Ycut^4 + 5400*m2^3*MBhat^4*Ycut^4 - 
             1875*m2^4*MBhat^4*Ycut^4 + 1560*MBhat^5*Ycut^4 - 
             9090*m2*MBhat^5*Ycut^4 + 5970*m2^2*MBhat^5*Ycut^4 - 
             1800*m2^3*MBhat^5*Ycut^4 - 410*MBhat^6*Ycut^4 + 
             930*m2*MBhat^6*Ycut^4 - 650*m2^2*MBhat^6*Ycut^4 + 24*Ycut^5 + 
             174*m2*Ycut^5 - 761*m2^2*Ycut^5 - 1476*m2^3*Ycut^5 - 
             81*m2^4*Ycut^5 + 20*m2^5*Ycut^5 - 33*MBhat*Ycut^5 - 
             540*m2*MBhat*Ycut^5 + 4170*m2^2*MBhat*Ycut^5 + 
             3660*m2^3*MBhat*Ycut^5 - 585*m2^4*MBhat*Ycut^5 + 
             48*m2^5*MBhat*Ycut^5 + 1122*MBhat^2*Ycut^5 - 2013*m2*MBhat^2*
              Ycut^5 - 6588*m2^2*MBhat^2*Ycut^5 - 378*m2^3*MBhat^2*Ycut^5 - 
             348*m2^4*MBhat^2*Ycut^5 + 85*m2^5*MBhat^2*Ycut^5 - 
             33*MBhat^3*Ycut^5 - 2352*m2*MBhat^3*Ycut^5 + 10498*m2^2*MBhat^3*
              Ycut^5 - 3072*m2^3*MBhat^3*Ycut^5 - 57*m2^4*MBhat^3*Ycut^5 + 
             136*m2^5*MBhat^3*Ycut^5 - 2100*MBhat^4*Ycut^5 + 
             4185*m2*MBhat^4*Ycut^5 - 3495*m2^2*MBhat^4*Ycut^5 - 
             495*m2^3*MBhat^4*Ycut^5 + 375*m2^4*MBhat^4*Ycut^5 + 
             900*MBhat^5*Ycut^5 - 90*m2*MBhat^5*Ycut^5 - 870*m2^2*MBhat^5*
              Ycut^5 + 360*m2^3*MBhat^5*Ycut^5 + 120*MBhat^6*Ycut^5 - 
             300*m2*MBhat^6*Ycut^5 + 130*m2^2*MBhat^6*Ycut^5 - 14*Ycut^6 + 
             40*m2*Ycut^6 - 61*m2^2*Ycut^6 - 317*m2^3*Ycut^6 + 
             52*m2^4*Ycut^6 - 342*MBhat*Ycut^6 + 393*m2*MBhat*Ycut^6 + 
             873*m2^2*MBhat*Ycut^6 + 3*m2^3*MBhat*Ycut^6 + 33*m2^4*MBhat*
              Ycut^6 - 446*MBhat^2*Ycut^6 + 1831*m2*MBhat^2*Ycut^6 - 
             3287*m2^2*MBhat^2*Ycut^6 + 755*m2^3*MBhat^2*Ycut^6 - 
             13*m2^4*MBhat^2*Ycut^6 + 332*MBhat^3*Ycut^6 + 965*m2*MBhat^3*
              Ycut^6 - 1317*m2^2*MBhat^3*Ycut^6 + 911*m2^3*MBhat^3*Ycut^6 - 
             91*m2^4*MBhat^3*Ycut^6 + 1050*MBhat^4*Ycut^6 - 
             3225*m2*MBhat^4*Ycut^6 + 1860*m2^2*MBhat^4*Ycut^6 - 
             195*m2^3*MBhat^4*Ycut^6 - 480*MBhat^5*Ycut^6 + 
             690*m2*MBhat^5*Ycut^6 - 90*m2^2*MBhat^5*Ycut^6 - 
             100*MBhat^6*Ycut^6 + 50*m2*MBhat^6*Ycut^6 + 56*Ycut^7 - 
             24*m2*Ycut^7 - 145*m2^2*Ycut^7 + 38*m2^3*Ycut^7 + 
             258*MBhat*Ycut^7 - 459*m2*MBhat*Ycut^7 + 504*m2^2*MBhat*Ycut^7 - 
             63*m2^3*MBhat*Ycut^7 - 136*MBhat^2*Ycut^7 - 435*m2*MBhat^2*
              Ycut^7 + 268*m2^2*MBhat^2*Ycut^7 - 77*m2^3*MBhat^2*Ycut^7 - 
             248*MBhat^3*Ycut^7 + 597*m2*MBhat^3*Ycut^7 - 89*m2^3*MBhat^3*
              Ycut^7 + 195*m2*MBhat^4*Ycut^7 - 165*m2^2*MBhat^4*Ycut^7 + 
             30*MBhat^5*Ycut^7 - 90*m2*MBhat^5*Ycut^7 + 40*MBhat^6*Ycut^7 - 
             54*Ycut^8 + 27*m2*Ycut^8 + 2*m2^2*Ycut^8 - 12*MBhat*Ycut^8 + 
             99*m2*MBhat*Ycut^8 - 27*m2^2*MBhat*Ycut^8 + 189*MBhat^2*Ycut^8 - 
             216*m2*MBhat^2*Ycut^8 + 37*m2^2*MBhat^2*Ycut^8 - 
             108*MBhat^3*Ycut^8 + 9*m2*MBhat^3*Ycut^8 + 9*m2^2*MBhat^3*
              Ycut^8 - 15*MBhat^4*Ycut^8 + 45*m2*MBhat^4*Ycut^8 + 16*Ycut^9 - 
             2*m2*Ycut^9 - 57*MBhat*Ycut^9 + 27*m2*MBhat*Ycut^9 + 
             14*MBhat^2*Ycut^9 + 8*m2*MBhat^2*Ycut^9 + 27*MBhat^3*Ycut^9 - 
             9*m2*MBhat^3*Ycut^9 + 2*Ycut^10 + 6*MBhat*Ycut^10 - 
             8*MBhat^2*Ycut^10))/(-1 + Ycut)^5 + 4*Sqrt[m2]*
          (2 + 15*m2 - 60*m2^2 - 140*m2^3 - 30*m2^4 + 3*m2^5 - 12*MBhat - 
           36*m2*MBhat + 360*m2^2*MBhat + 360*m2^3*MBhat + 30*MBhat^2 - 
           9*m2*MBhat^2 - 645*m2^2*MBhat^2 - 206*m2^3*MBhat^2 + 
           18*m2^4*MBhat^2 - 40*MBhat^3 + 96*m2*MBhat^3 + 456*m2^2*MBhat^3 + 
           30*MBhat^4 - 99*m2*MBhat^4 - 114*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 
           12*MBhat^5 + 36*m2*MBhat^5 + 2*MBhat^6 - 3*m2*MBhat^6 + 
           3*m2^2*MBhat^6)*Log[m2] - 4*Sqrt[m2]*(2 + 15*m2 - 60*m2^2 - 
           140*m2^3 - 30*m2^4 + 3*m2^5 - 12*MBhat - 36*m2*MBhat + 
           360*m2^2*MBhat + 360*m2^3*MBhat + 30*MBhat^2 - 9*m2*MBhat^2 - 
           645*m2^2*MBhat^2 - 206*m2^3*MBhat^2 + 18*m2^4*MBhat^2 - 
           40*MBhat^3 + 96*m2*MBhat^3 + 456*m2^2*MBhat^3 + 30*MBhat^4 - 
           99*m2*MBhat^4 - 114*m2^2*MBhat^4 + 18*m2^3*MBhat^4 - 12*MBhat^5 + 
           36*m2*MBhat^5 + 2*MBhat^6 - 3*m2*MBhat^6 + 3*m2^2*MBhat^6)*
          Log[1 - Ycut]))))/mb^3 - 
 (5 - 175*m2 - 5495*m2^2 - 7875*m2^3 + 7875*m2^4 + 5495*m2^5 + 175*m2^6 - 
   5*m2^7 - 43*MBhat + 1225*m2*MBhat + 26313*m2^2*MBhat + 13125*m2^3*MBhat - 
   34125*m2^4*MBhat - 6657*m2^5*MBhat + 175*m2^6*MBhat - 13*m2^7*MBhat + 
   162*MBhat^2 - 3668*m2*MBhat^2 - 49728*m2^2*MBhat^2 + 11550*m2^3*MBhat^2 + 
   40950*m2^4*MBhat^2 + 588*m2^5*MBhat^2 + 168*m2^6*MBhat^2 - 
   22*m2^7*MBhat^2 - 348*MBhat^3 + 6076*m2*MBhat^3 + 45710*m2^2*MBhat^3 - 
   35770*m2^3*MBhat^3 - 15820*m2^4*MBhat^3 + 28*m2^5*MBhat^3 + 
   154*m2^6*MBhat^3 - 30*m2^7*MBhat^3 + 427*MBhat^4 - 5733*m2*MBhat^4 - 
   20160*m2^2*MBhat^4 + 25200*m2^3*MBhat^4 - 315*m2^4*MBhat^4 + 
   693*m2^5*MBhat^4 - 112*m2^6*MBhat^4 - 273*MBhat^5 + 2835*m2*MBhat^5 + 
   3360*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 + 945*m2^4*MBhat^5 - 
   147*m2^5*MBhat^5 + 70*MBhat^6 - 560*m2*MBhat^6 + 560*m2^3*MBhat^6 - 
   70*m2^4*MBhat^6 - 25*Ycut + 875*m2*Ycut + 29575*m2^2*Ycut + 
   49875*m2^3*Ycut - 28875*m2^4*Ycut - 25375*m2^5*Ycut - 875*m2^6*Ycut + 
   25*m2^7*Ycut + 215*MBhat*Ycut - 6125*m2*MBhat*Ycut - 
   142905*m2^2*MBhat*Ycut - 105525*m2^3*MBhat*Ycut + 147525*m2^4*MBhat*Ycut + 
   32025*m2^5*MBhat*Ycut - 875*m2^6*MBhat*Ycut + 65*m2^7*MBhat*Ycut - 
   810*MBhat^2*Ycut + 18340*m2*MBhat^2*Ycut + 273840*m2^2*MBhat^2*Ycut + 
   210*m2^3*MBhat^2*Ycut - 188790*m2^4*MBhat^2*Ycut - 
   2940*m2^5*MBhat^2*Ycut - 840*m2^6*MBhat^2*Ycut + 110*m2^7*MBhat^2*Ycut + 
   1740*MBhat^3*Ycut - 30380*m2*MBhat^3*Ycut - 257950*m2^2*MBhat^3*Ycut + 
   139370*m2^3*MBhat^3*Ycut + 75740*m2^4*MBhat^3*Ycut - 
   140*m2^5*MBhat^3*Ycut - 770*m2^6*MBhat^3*Ycut + 150*m2^7*MBhat^3*Ycut - 
   2135*MBhat^4*Ycut + 28665*m2*MBhat^4*Ycut + 119700*m2^2*MBhat^4*Ycut - 
   113820*m2^3*MBhat^4*Ycut + 1575*m2^4*MBhat^4*Ycut - 
   3465*m2^5*MBhat^4*Ycut + 560*m2^6*MBhat^4*Ycut + 1365*MBhat^5*Ycut - 
   14175*m2*MBhat^5*Ycut - 23100*m2^2*MBhat^5*Ycut + 
   32340*m2^3*MBhat^5*Ycut - 4725*m2^4*MBhat^5*Ycut + 735*m2^5*MBhat^5*Ycut - 
   350*MBhat^6*Ycut + 2800*m2*MBhat^6*Ycut + 840*m2^2*MBhat^6*Ycut - 
   2800*m2^3*MBhat^6*Ycut + 350*m2^4*MBhat^6*Ycut + 50*Ycut^2 - 
   1750*m2*Ycut^2 - 64400*m2^2*Ycut^2 - 126000*m2^3*Ycut^2 + 
   31500*m2^4*Ycut^2 + 45500*m2^5*Ycut^2 + 1750*m2^6*Ycut^2 - 
   50*m2^7*Ycut^2 - 430*MBhat*Ycut^2 + 12250*m2*MBhat*Ycut^2 + 
   314160*m2^2*MBhat*Ycut^2 + 310800*m2^3*MBhat*Ycut^2 - 
   237300*m2^4*MBhat*Ycut^2 - 60900*m2^5*MBhat*Ycut^2 + 
   1750*m2^6*MBhat*Ycut^2 - 130*m2^7*MBhat*Ycut^2 + 1620*MBhat^2*Ycut^2 - 
   36680*m2*MBhat^2*Ycut^2 - 610680*m2^2*MBhat^2*Ycut^2 - 
   145320*m2^3*MBhat^2*Ycut^2 + 337680*m2^4*MBhat^2*Ycut^2 + 
   5880*m2^5*MBhat^2*Ycut^2 + 1680*m2^6*MBhat^2*Ycut^2 - 
   220*m2^7*MBhat^2*Ycut^2 - 3480*MBhat^3*Ycut^2 + 60760*m2*MBhat^3*Ycut^2 + 
   589400*m2^2*MBhat^3*Ycut^2 - 180040*m2^3*MBhat^3*Ycut^2 - 
   143080*m2^4*MBhat^3*Ycut^2 + 280*m2^5*MBhat^3*Ycut^2 + 
   1540*m2^6*MBhat^3*Ycut^2 - 300*m2^7*MBhat^3*Ycut^2 + 4270*MBhat^4*Ycut^2 - 
   57330*m2*MBhat^4*Ycut^2 - 286650*m2^2*MBhat^4*Ycut^2 + 
   197190*m2^3*MBhat^4*Ycut^2 - 3150*m2^4*MBhat^4*Ycut^2 + 
   6930*m2^5*MBhat^4*Ycut^2 - 1120*m2^6*MBhat^4*Ycut^2 - 
   2730*MBhat^5*Ycut^2 + 28350*m2*MBhat^5*Ycut^2 + 
   61950*m2^2*MBhat^5*Ycut^2 - 61530*m2^3*MBhat^5*Ycut^2 + 
   9450*m2^4*MBhat^5*Ycut^2 - 1470*m2^5*MBhat^5*Ycut^2 + 700*MBhat^6*Ycut^2 - 
   5600*m2*MBhat^6*Ycut^2 - 3780*m2^2*MBhat^6*Ycut^2 + 
   5600*m2^3*MBhat^6*Ycut^2 - 700*m2^4*MBhat^6*Ycut^2 - 50*Ycut^3 + 
   1750*m2*Ycut^3 + 71400*m2^2*Ycut^3 + 161000*m2^3*Ycut^3 + 
   3500*m2^4*Ycut^3 - 38500*m2^5*Ycut^3 - 1750*m2^6*Ycut^3 + 50*m2^7*Ycut^3 + 
   430*MBhat*Ycut^3 - 12250*m2*MBhat*Ycut^3 - 351960*m2^2*MBhat*Ycut^3 - 
   443800*m2^3*MBhat*Ycut^3 + 160300*m2^4*MBhat*Ycut^3 + 
   56700*m2^5*MBhat*Ycut^3 - 1750*m2^6*MBhat*Ycut^3 + 130*m2^7*MBhat*Ycut^3 - 
   1620*MBhat^2*Ycut^3 + 36680*m2*MBhat^2*Ycut^3 + 
   694680*m2^2*MBhat^2*Ycut^3 + 338520*m2^3*MBhat^2*Ycut^3 - 
   284480*m2^4*MBhat^2*Ycut^3 - 5880*m2^5*MBhat^2*Ycut^3 - 
   1680*m2^6*MBhat^2*Ycut^3 + 220*m2^7*MBhat^2*Ycut^3 + 3620*MBhat^3*Ycut^3 - 
   61320*m2*MBhat^3*Ycut^3 - 686700*m2^2*MBhat^3*Ycut^3 + 
   48440*m2^3*MBhat^3*Ycut^3 + 131180*m2^4*MBhat^3*Ycut^3 + 
   280*m2^5*MBhat^3*Ycut^3 - 1680*m2^6*MBhat^3*Ycut^3 + 
   300*m2^7*MBhat^3*Ycut^3 - 4690*MBhat^4*Ycut^3 + 58590*m2*MBhat^4*Ycut^3 + 
   348810*m2^2*MBhat^4*Ycut^3 - 157430*m2^3*MBhat^4*Ycut^3 + 
   4410*m2^4*MBhat^4*Ycut^3 - 7350*m2^5*MBhat^4*Ycut^3 + 
   1120*m2^6*MBhat^4*Ycut^3 + 3150*MBhat^5*Ycut^3 - 29190*m2*MBhat^5*Ycut^3 - 
   82950*m2^2*MBhat^5*Ycut^3 + 58170*m2^3*MBhat^5*Ycut^3 - 
   9870*m2^4*MBhat^5*Ycut^3 + 1470*m2^5*MBhat^5*Ycut^3 - 840*MBhat^6*Ycut^3 + 
   5740*m2*MBhat^6*Ycut^3 + 6720*m2^2*MBhat^6*Ycut^3 - 
   5740*m2^3*MBhat^6*Ycut^3 + 700*m2^4*MBhat^6*Ycut^3 + 25*Ycut^4 - 
   875*m2*Ycut^4 - 40950*m2^2*Ycut^4 - 106750*m2^3*Ycut^4 - 
   28000*m2^4*Ycut^4 + 14000*m2^5*Ycut^4 + 875*m2^6*Ycut^4 - 25*m2^7*Ycut^4 - 
   215*MBhat*Ycut^4 + 6125*m2*MBhat*Ycut^4 + 204330*m2^2*MBhat*Ycut^4 + 
   321650*m2^3*MBhat*Ycut^4 - 22400*m2^4*MBhat*Ycut^4 - 
   25200*m2^5*MBhat*Ycut^4 + 875*m2^6*MBhat*Ycut^4 - 65*m2^7*MBhat*Ycut^4 + 
   705*MBhat^2*Ycut^4 - 18025*m2*MBhat^2*Ycut^4 - 
   410340*m2^2*MBhat^2*Ycut^4 - 315210*m2^3*MBhat^2*Ycut^4 + 
   103915*m2^4*MBhat^2*Ycut^4 + 1995*m2^5*MBhat^2*Ycut^4 + 
   1050*m2^6*MBhat^2*Ycut^4 - 110*m2^7*MBhat^2*Ycut^4 - 2195*MBhat^3*Ycut^4 + 
   32655*m2*MBhat^3*Ycut^4 + 412860*m2^2*MBhat^3*Ycut^4 + 
   78190*m2^3*MBhat^3*Ycut^4 - 58555*m2^4*MBhat^3*Ycut^4 - 
   245*m2^5*MBhat^3*Ycut^4 + 980*m2^6*MBhat^3*Ycut^4 - 
   150*m2^7*MBhat^3*Ycut^4 + 4130*MBhat^4*Ycut^4 - 34860*m2*MBhat^4*Ycut^4 - 
   216300*m2^2*MBhat^4*Ycut^4 + 47740*m2^3*MBhat^4*Ycut^4 - 
   3885*m2^4*MBhat^4*Ycut^4 + 4305*m2^5*MBhat^4*Ycut^4 - 
   560*m2^6*MBhat^4*Ycut^4 - 3570*MBhat^5*Ycut^4 + 18480*m2*MBhat^5*Ycut^4 + 
   56280*m2^2*MBhat^5*Ycut^4 - 27720*m2^3*MBhat^5*Ycut^4 + 
   5775*m2^4*MBhat^5*Ycut^4 - 735*m2^5*MBhat^5*Ycut^4 + 1120*MBhat^6*Ycut^4 - 
   3500*m2*MBhat^6*Ycut^4 - 5880*m2^2*MBhat^6*Ycut^4 + 
   3220*m2^3*MBhat^6*Ycut^4 - 350*m2^4*MBhat^6*Ycut^4 - 5*Ycut^5 + 
   175*m2*Ycut^5 + 10290*m2^2*Ycut^5 + 31850*m2^3*Ycut^5 + 
   16100*m2^4*Ycut^5 - 700*m2^5*Ycut^5 - 175*m2^6*Ycut^5 + 5*m2^7*Ycut^5 + 
   85*MBhat*Ycut^5 - 1309*m2*MBhat*Ycut^5 - 52416*m2^2*MBhat*Ycut^5 - 
   103390*m2^3*MBhat*Ycut^5 - 19670*m2^4*MBhat*Ycut^5 + 
   4368*m2^5*MBhat*Ycut^5 - 301*m2^6*MBhat*Ycut^5 + 13*m2^7*MBhat*Ycut^5 + 
   279*MBhat^2*Ycut^5 + 2219*m2*MBhat^2*Ycut^5 + 108486*m2^2*MBhat^2*Ycut^5 + 
   121590*m2^3*MBhat^2*Ycut^5 - 6335*m2^4*MBhat^2*Ycut^5 + 
   399*m2^5*MBhat^2*Ycut^5 - 336*m2^6*MBhat^2*Ycut^5 + 
   22*m2^7*MBhat^2*Ycut^5 + 523*MBhat^3*Ycut^5 - 9051*m2*MBhat^3*Ycut^5 - 
   104160*m2^2*MBhat^3*Ycut^5 - 64190*m2^3*MBhat^3*Ycut^5 + 
   12635*m2^4*MBhat^3*Ycut^5 - 455*m2^5*MBhat^3*Ycut^5 - 
   280*m2^6*MBhat^3*Ycut^5 + 30*m2^7*MBhat^3*Ycut^5 - 4018*MBhat^4*Ycut^5 + 
   17766*m2*MBhat^4*Ycut^5 + 49560*m2^2*MBhat^4*Ycut^5 + 
   7840*m2^3*MBhat^4*Ycut^5 + 945*m2^4*MBhat^4*Ycut^5 - 
   1239*m2^5*MBhat^4*Ycut^5 + 112*m2^6*MBhat^4*Ycut^5 + 4956*MBhat^5*Ycut^5 - 
   11760*m2*MBhat^5*Ycut^5 - 14280*m2^2*MBhat^5*Ycut^5 + 
   5460*m2^3*MBhat^5*Ycut^5 - 1785*m2^4*MBhat^5*Ycut^5 + 
   147*m2^5*MBhat^5*Ycut^5 - 1820*MBhat^6*Ycut^5 + 1960*m2*MBhat^6*Ycut^5 + 
   2520*m2^2*MBhat^6*Ycut^5 - 980*m2^3*MBhat^6*Ycut^5 + 
   70*m2^4*MBhat^6*Ycut^5 - 7*Ycut^6 + 7*m2*Ycut^6 - 280*m2^2*Ycut^6 - 
   1960*m2^3*Ycut^6 - 1505*m2^4*Ycut^6 - 483*m2^5*Ycut^6 + 28*m2^6*Ycut^6 - 
   203*MBhat*Ycut^6 + 413*m2*MBhat*Ycut^6 + 2310*m2^2*MBhat*Ycut^6 + 
   4900*m2^3*MBhat*Ycut^6 + 5495*m2^4*MBhat*Ycut^6 - 357*m2^5*MBhat*Ycut^6 + 
   42*m2^6*MBhat*Ycut^6 - 609*MBhat^2*Ycut^6 + 2513*m2*MBhat^2*Ycut^6 - 
   8190*m2^2*MBhat^2*Ycut^6 - 6720*m2^3*MBhat^2*Ycut^6 - 
   3535*m2^4*MBhat^2*Ycut^6 - 21*m2^5*MBhat^2*Ycut^6 + 
   42*m2^6*MBhat^2*Ycut^6 + 1015*MBhat^3*Ycut^6 + 357*m2*MBhat^3*Ycut^6 - 
   1400*m2^2*MBhat^3*Ycut^6 + 14420*m2^3*MBhat^3*Ycut^6 - 
   2695*m2^4*MBhat^3*Ycut^6 + 315*m2^5*MBhat^3*Ycut^6 + 
   28*m2^6*MBhat^3*Ycut^6 + 2744*MBhat^4*Ycut^6 - 11340*m2*MBhat^4*Ycut^6 + 
   11550*m2^2*MBhat^4*Ycut^6 - 8890*m2^3*MBhat^4*Ycut^6 + 
   630*m2^4*MBhat^4*Ycut^6 + 126*m2^5*MBhat^4*Ycut^6 - 5040*MBhat^5*Ycut^6 + 
   9450*m2*MBhat^5*Ycut^6 - 3570*m2^2*MBhat^5*Ycut^6 + 
   210*m2^3*MBhat^5*Ycut^6 + 210*m2^4*MBhat^5*Ycut^6 + 2100*MBhat^6*Ycut^6 - 
   1400*m2*MBhat^6*Ycut^6 - 420*m2^2*MBhat^6*Ycut^6 + 
   140*m2^3*MBhat^6*Ycut^6 + 37*Ycut^7 - 35*m2*Ycut^7 - 280*m2^2*Ycut^7 + 
   280*m2^3*Ycut^7 - 665*m2^4*Ycut^7 + 63*m2^5*Ycut^7 + 379*MBhat*Ycut^7 - 
   805*m2*MBhat*Ycut^7 + 420*m2^2*MBhat*Ycut^7 + 1540*m2^3*MBhat*Ycut^7 + 
   245*m2^4*MBhat*Ycut^7 + 21*m2^5*MBhat*Ycut^7 + 111*MBhat^2*Ycut^7 - 
   1855*m2*MBhat^2*Ycut^7 + 3780*m2^2*MBhat^2*Ycut^7 - 
   5040*m2^3*MBhat^2*Ycut^7 + 665*m2^4*MBhat^2*Ycut^7 - 
   21*m2^5*MBhat^2*Ycut^7 - 1577*MBhat^3*Ycut^7 + 2415*m2*MBhat^3*Ycut^7 + 
   700*m2^2*MBhat^3*Ycut^7 - 420*m2^3*MBhat^3*Ycut^7 + 
   665*m2^4*MBhat^3*Ycut^7 - 63*m2^5*MBhat^3*Ycut^7 - 280*MBhat^4*Ycut^7 + 
   4830*m2*MBhat^4*Ycut^7 - 7350*m2^2*MBhat^4*Ycut^7 + 
   2450*m2^3*MBhat^4*Ycut^7 - 210*m2^4*MBhat^4*Ycut^7 + 2730*MBhat^5*Ycut^7 - 
   5250*m2*MBhat^5*Ycut^7 + 2730*m2^2*MBhat^5*Ycut^7 - 
   210*m2^3*MBhat^5*Ycut^7 - 1400*MBhat^6*Ycut^7 + 700*m2*MBhat^6*Ycut^7 - 
   80*Ycut^8 + 70*m2*Ycut^8 + 245*m2^2*Ycut^8 - 455*m2^3*Ycut^8 + 
   70*m2^4*Ycut^8 - 320*MBhat*Ycut^8 + 770*m2*MBhat*Ycut^8 - 
   735*m2^2*MBhat*Ycut^8 + 805*m2^3*MBhat*Ycut^8 - 70*m2^4*MBhat*Ycut^8 + 
   495*MBhat^2*Ycut^8 + 245*m2*MBhat^2*Ycut^8 - 1680*m2^2*MBhat^2*Ycut^8 + 
   420*m2^3*MBhat^2*Ycut^8 - 70*m2^4*MBhat^2*Ycut^8 + 745*MBhat^3*Ycut^8 - 
   1995*m2*MBhat^3*Ycut^8 + 1750*m2^2*MBhat^3*Ycut^8 - 
   70*m2^4*MBhat^3*Ycut^8 - 805*MBhat^4*Ycut^8 - 315*m2*MBhat^4*Ycut^8 + 
   840*m2^2*MBhat^4*Ycut^8 - 280*m2^3*MBhat^4*Ycut^8 - 525*MBhat^5*Ycut^8 + 
   1365*m2*MBhat^5*Ycut^8 - 420*m2^2*MBhat^5*Ycut^8 + 490*MBhat^6*Ycut^8 - 
   140*m2*MBhat^6*Ycut^8 + 90*Ycut^9 - 70*m2*Ycut^9 - 105*m2^2*Ycut^9 + 
   35*m2^3*Ycut^9 + 80*MBhat*Ycut^9 - 350*m2*MBhat*Ycut^9 + 
   525*m2^2*MBhat*Ycut^9 - 105*m2^3*MBhat*Ycut^9 - 465*MBhat^2*Ycut^9 + 
   385*m2*MBhat^2*Ycut^9 - 210*m2^2*MBhat^2*Ycut^9 + 85*MBhat^3*Ycut^9 + 
   455*m2*MBhat^3*Ycut^9 - 210*m2^2*MBhat^3*Ycut^9 + 385*MBhat^4*Ycut^9 - 
   315*m2*MBhat^4*Ycut^9 - 105*MBhat^5*Ycut^9 - 105*m2*MBhat^5*Ycut^9 - 
   70*MBhat^6*Ycut^9 - 55*Ycut^10 + 35*m2*Ycut^10 + 53*MBhat*Ycut^10 + 
   49*m2*MBhat*Ycut^10 - 42*m2^2*MBhat*Ycut^10 + 129*MBhat^2*Ycut^10 - 
   161*m2*MBhat^2*Ycut^10 + 42*m2^2*MBhat^2*Ycut^10 - 155*MBhat^3*Ycut^10 + 
   35*m2*MBhat^3*Ycut^10 - 14*MBhat^4*Ycut^10 + 42*m2*MBhat^4*Ycut^10 + 
   42*MBhat^5*Ycut^10 + 17*Ycut^11 - 7*m2*Ycut^11 - 37*MBhat*Ycut^11 + 
   7*m2*MBhat*Ycut^11 + 9*MBhat^2*Ycut^11 + 7*m2*MBhat^2*Ycut^11 + 
   25*MBhat^3*Ycut^11 - 7*m2*MBhat^3*Ycut^11 - 14*MBhat^4*Ycut^11 - 
   2*Ycut^12 + 6*MBhat*Ycut^12 - 6*MBhat^2*Ycut^12 + 2*MBhat^3*Ycut^12 + 
   70*api*MBhat^6*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   350*api*MBhat^6*Ycut*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] + 
   700*api*MBhat^6*Ycut^2*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   700*api*MBhat^6*Ycut^3*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] + 
   350*api*MBhat^6*Ycut^4*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   70*api*MBhat^6*Ycut^5*X1mix[0, 0, SM^2, Ycut, m2, mu2hat] - 
   420*api*MBhat^5*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   2100*api*MBhat^5*Ycut*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] - 
   4200*api*MBhat^5*Ycut^2*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   4200*api*MBhat^5*Ycut^3*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] - 
   2100*api*MBhat^5*Ycut^4*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   420*api*MBhat^5*Ycut^5*X1mix[0, 1, SM^2, Ycut, m2, mu2hat] + 
   840*api*MBhat^4*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] - 
   4200*api*MBhat^4*Ycut*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] + 
   8400*api*MBhat^4*Ycut^2*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] - 
   8400*api*MBhat^4*Ycut^3*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] + 
   4200*api*MBhat^4*Ycut^4*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] - 
   840*api*MBhat^4*Ycut^5*X1mix[0, 2, SM^2, Ycut, m2, mu2hat] - 
   560*api*MBhat^3*X1mix[0, 3, SM^2, Ycut, m2, mu2hat] + 
   2800*api*MBhat^3*Ycut*X1mix[0, 3, SM^2, Ycut, m2, mu2hat] - 
   5600*api*MBhat^3*Ycut^2*X1mix[0, 3, SM^2, Ycut, m2, mu2hat] + 
   5600*api*MBhat^3*Ycut^3*X1mix[0, 3, SM^2, Ycut, m2, mu2hat] - 
   2800*api*MBhat^3*Ycut^4*X1mix[0, 3, SM^2, Ycut, m2, mu2hat] + 
   560*api*MBhat^3*Ycut^5*X1mix[0, 3, SM^2, Ycut, m2, mu2hat] + 
   210*api*MBhat^4*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   1050*api*MBhat^4*Ycut*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] + 
   2100*api*MBhat^4*Ycut^2*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   2100*api*MBhat^4*Ycut^3*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] + 
   1050*api*MBhat^4*Ycut^4*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   210*api*MBhat^4*Ycut^5*X1mix[1, 0, SM^2, Ycut, m2, mu2hat] - 
   840*api*MBhat^3*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   4200*api*MBhat^3*Ycut*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] - 
   8400*api*MBhat^3*Ycut^2*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   8400*api*MBhat^3*Ycut^3*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] - 
   4200*api*MBhat^3*Ycut^4*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   840*api*MBhat^3*Ycut^5*X1mix[1, 1, SM^2, Ycut, m2, mu2hat] + 
   840*api*MBhat^2*X1mix[1, 2, SM^2, Ycut, m2, mu2hat] - 
   4200*api*MBhat^2*Ycut*X1mix[1, 2, SM^2, Ycut, m2, mu2hat] + 
   8400*api*MBhat^2*Ycut^2*X1mix[1, 2, SM^2, Ycut, m2, mu2hat] - 
   8400*api*MBhat^2*Ycut^3*X1mix[1, 2, SM^2, Ycut, m2, mu2hat] + 
   4200*api*MBhat^2*Ycut^4*X1mix[1, 2, SM^2, Ycut, m2, mu2hat] - 
   840*api*MBhat^2*Ycut^5*X1mix[1, 2, SM^2, Ycut, m2, mu2hat] + 
   210*api*MBhat^2*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] - 
   1050*api*MBhat^2*Ycut*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] + 
   2100*api*MBhat^2*Ycut^2*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] - 
   2100*api*MBhat^2*Ycut^3*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] + 
   1050*api*MBhat^2*Ycut^4*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] - 
   210*api*MBhat^2*Ycut^5*X1mix[2, 0, SM^2, Ycut, m2, mu2hat] - 
   420*api*MBhat*X1mix[2, 1, SM^2, Ycut, m2, mu2hat] + 
   2100*api*MBhat*Ycut*X1mix[2, 1, SM^2, Ycut, m2, mu2hat] - 
   4200*api*MBhat*Ycut^2*X1mix[2, 1, SM^2, Ycut, m2, mu2hat] + 
   4200*api*MBhat*Ycut^3*X1mix[2, 1, SM^2, Ycut, m2, mu2hat] - 
   2100*api*MBhat*Ycut^4*X1mix[2, 1, SM^2, Ycut, m2, mu2hat] + 
   420*api*MBhat*Ycut^5*X1mix[2, 1, SM^2, Ycut, m2, mu2hat] + 
   70*api*X1mix[3, 0, SM^2, Ycut, m2, mu2hat] - 
   350*api*Ycut*X1mix[3, 0, SM^2, Ycut, m2, mu2hat] + 
   700*api*Ycut^2*X1mix[3, 0, SM^2, Ycut, m2, mu2hat] - 
   700*api*Ycut^3*X1mix[3, 0, SM^2, Ycut, m2, mu2hat] + 
   350*api*Ycut^4*X1mix[3, 0, SM^2, Ycut, m2, mu2hat] - 
   70*api*Ycut^5*X1mix[3, 0, SM^2, Ycut, m2, mu2hat])/(70*(-1 + Ycut)^5) + 
 c[SL]^2*(3*m2^2*(-4 - 20*m2 - 20*m2^2 - 4*m2^3 + 21*MBhat + 75*m2*MBhat + 
     45*m2^2*MBhat + 3*m2^3*MBhat - 45*MBhat^2 - 108*m2*MBhat^2 - 
     33*m2^2*MBhat^2 + 50*MBhat^3 + 74*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
     30*MBhat^4 - 24*m2*MBhat^4 + 9*MBhat^5 + 3*m2*MBhat^5 - MBhat^6)*
    Log[m2] - 3*m2^2*(-4 - 20*m2 - 20*m2^2 - 4*m2^3 + 21*MBhat + 
     75*m2*MBhat + 45*m2^2*MBhat + 3*m2^3*MBhat - 45*MBhat^2 - 
     108*m2*MBhat^2 - 33*m2^2*MBhat^2 + 50*MBhat^3 + 74*m2*MBhat^3 + 
     8*m2^2*MBhat^3 - 30*MBhat^4 - 24*m2*MBhat^4 + 9*MBhat^5 + 3*m2*MBhat^5 - 
     MBhat^6)*Log[1 - Ycut] - (4 - 140*m2 - 4396*m2^2 - 6300*m2^3 + 
     6300*m2^4 + 4396*m2^5 + 140*m2^6 - 4*m2^7 - 33*MBhat + 945*m2*MBhat + 
     20559*m2^2*MBhat + 11025*m2^3*MBhat - 26775*m2^4*MBhat - 
     5817*m2^5*MBhat + 105*m2^6*MBhat - 9*m2^7*MBhat + 117*MBhat^2 - 
     2688*m2*MBhat^2 - 37863*m2^2*MBhat^2 + 6300*m2^3*MBhat^2 + 
     33075*m2^4*MBhat^2 + 1008*m2^5*MBhat^2 + 63*m2^6*MBhat^2 - 
     12*m2^7*MBhat^2 - 228*MBhat^3 + 4116*m2*MBhat^3 + 34090*m2^2*MBhat^3 - 
     23870*m2^3*MBhat^3 - 14420*m2^4*MBhat^3 + 308*m2^5*MBhat^3 + 
     14*m2^6*MBhat^3 - 10*m2^7*MBhat^3 + 252*MBhat^4 - 3528*m2*MBhat^4 - 
     14910*m2^2*MBhat^4 + 16800*m2^3*MBhat^4 + 1260*m2^4*MBhat^4 + 
     168*m2^5*MBhat^4 - 42*m2^6*MBhat^4 - 147*MBhat^5 + 1575*m2*MBhat^5 + 
     2520*m2^2*MBhat^5 - 4200*m2^3*MBhat^5 + 315*m2^4*MBhat^5 - 
     63*m2^5*MBhat^5 + 35*MBhat^6 - 280*m2*MBhat^6 + 280*m2^3*MBhat^6 - 
     35*m2^4*MBhat^6 - 20*Ycut + 700*m2*Ycut + 23660*m2^2*Ycut + 
     39900*m2^3*Ycut - 23100*m2^4*Ycut - 20300*m2^5*Ycut - 700*m2^6*Ycut + 
     20*m2^7*Ycut + 165*MBhat*Ycut - 4725*m2*MBhat*Ycut - 
     111615*m2^2*MBhat*Ycut - 86625*m2^3*MBhat*Ycut + 
     114975*m2^4*MBhat*Ycut + 27825*m2^5*MBhat*Ycut - 525*m2^6*MBhat*Ycut + 
     45*m2^7*MBhat*Ycut - 585*MBhat^2*Ycut + 13440*m2*MBhat^2*Ycut + 
     208215*m2^2*MBhat^2*Ycut + 13860*m2^3*MBhat^2*Ycut - 
     151515*m2^4*MBhat^2*Ycut - 5040*m2^5*MBhat^2*Ycut - 
     315*m2^6*MBhat^2*Ycut + 60*m2^7*MBhat^2*Ycut + 1140*MBhat^3*Ycut - 
     20580*m2*MBhat^3*Ycut - 191450*m2^2*MBhat^3*Ycut + 
     88270*m2^3*MBhat^3*Ycut + 68740*m2^4*MBhat^3*Ycut - 
     1540*m2^5*MBhat^3*Ycut - 70*m2^6*MBhat^3*Ycut + 50*m2^7*MBhat^3*Ycut - 
     1260*MBhat^4*Ycut + 17640*m2*MBhat^4*Ycut + 87150*m2^2*MBhat^4*Ycut - 
     73920*m2^3*MBhat^4*Ycut - 6300*m2^4*MBhat^4*Ycut - 
     840*m2^5*MBhat^4*Ycut + 210*m2^6*MBhat^4*Ycut + 735*MBhat^5*Ycut - 
     7875*m2*MBhat^5*Ycut - 16380*m2^2*MBhat^5*Ycut + 
     19740*m2^3*MBhat^5*Ycut - 1575*m2^4*MBhat^5*Ycut + 
     315*m2^5*MBhat^5*Ycut - 175*MBhat^6*Ycut + 1400*m2*MBhat^6*Ycut + 
     420*m2^2*MBhat^6*Ycut - 1400*m2^3*MBhat^6*Ycut + 175*m2^4*MBhat^6*Ycut + 
     40*Ycut^2 - 1400*m2*Ycut^2 - 51520*m2^2*Ycut^2 - 100800*m2^3*Ycut^2 + 
     25200*m2^4*Ycut^2 + 36400*m2^5*Ycut^2 + 1400*m2^6*Ycut^2 - 
     40*m2^7*Ycut^2 - 330*MBhat*Ycut^2 + 9450*m2*MBhat*Ycut^2 + 
     245280*m2^2*MBhat*Ycut^2 + 252000*m2^3*MBhat*Ycut^2 - 
     182700*m2^4*MBhat*Ycut^2 - 52500*m2^5*MBhat*Ycut^2 + 
     1050*m2^6*MBhat*Ycut^2 - 90*m2^7*MBhat*Ycut^2 + 1170*MBhat^2*Ycut^2 - 
     26880*m2*MBhat^2*Ycut^2 - 463680*m2^2*MBhat^2*Ycut^2 - 
     141120*m2^3*MBhat^2*Ycut^2 + 268380*m2^4*MBhat^2*Ycut^2 + 
     10080*m2^5*MBhat^2*Ycut^2 + 630*m2^6*MBhat^2*Ycut^2 - 
     120*m2^7*MBhat^2*Ycut^2 - 2280*MBhat^3*Ycut^2 + 
     41160*m2*MBhat^3*Ycut^2 + 435400*m2^2*MBhat^3*Ycut^2 - 
     98840*m2^3*MBhat^3*Ycut^2 - 129080*m2^4*MBhat^3*Ycut^2 + 
     3080*m2^5*MBhat^3*Ycut^2 + 140*m2^6*MBhat^3*Ycut^2 - 
     100*m2^7*MBhat^3*Ycut^2 + 2520*MBhat^4*Ycut^2 - 
     35280*m2*MBhat^4*Ycut^2 - 205800*m2^2*MBhat^4*Ycut^2 + 
     122640*m2^3*MBhat^4*Ycut^2 + 12600*m2^4*MBhat^4*Ycut^2 + 
     1680*m2^5*MBhat^4*Ycut^2 - 420*m2^6*MBhat^4*Ycut^2 - 
     1470*MBhat^5*Ycut^2 + 15750*m2*MBhat^5*Ycut^2 + 
     42210*m2^2*MBhat^5*Ycut^2 - 36330*m2^3*MBhat^5*Ycut^2 + 
     3150*m2^4*MBhat^5*Ycut^2 - 630*m2^5*MBhat^5*Ycut^2 + 
     350*MBhat^6*Ycut^2 - 2800*m2*MBhat^6*Ycut^2 - 1890*m2^2*MBhat^6*Ycut^2 + 
     2800*m2^3*MBhat^6*Ycut^2 - 350*m2^4*MBhat^6*Ycut^2 - 40*Ycut^3 + 
     1400*m2*Ycut^3 + 57120*m2^2*Ycut^3 + 128800*m2^3*Ycut^3 + 
     2800*m2^4*Ycut^3 - 30800*m2^5*Ycut^3 - 1400*m2^6*Ycut^3 + 
     40*m2^7*Ycut^3 + 330*MBhat*Ycut^3 - 9450*m2*MBhat*Ycut^3 - 
     274680*m2^2*MBhat*Ycut^3 - 357000*m2^3*MBhat*Ycut^3 + 
     119700*m2^4*MBhat*Ycut^3 + 48300*m2^5*MBhat*Ycut^3 - 
     1050*m2^6*MBhat*Ycut^3 + 90*m2^7*MBhat*Ycut^3 - 1170*MBhat^2*Ycut^3 + 
     26880*m2*MBhat^2*Ycut^3 + 526680*m2^2*MBhat^2*Ycut^3 + 
     292320*m2^3*MBhat^2*Ycut^3 - 222180*m2^4*MBhat^2*Ycut^3 - 
     10080*m2^5*MBhat^2*Ycut^3 - 630*m2^6*MBhat^2*Ycut^3 + 
     120*m2^7*MBhat^2*Ycut^3 + 2350*MBhat^3*Ycut^3 - 
     41440*m2*MBhat^3*Ycut^3 - 505050*m2^2*MBhat^3*Ycut^3 - 
     4760*m2^3*MBhat^3*Ycut^3 + 117530*m2^4*MBhat^3*Ycut^3 - 
     2800*m2^5*MBhat^3*Ycut^3 - 210*m2^6*MBhat^3*Ycut^3 + 
     100*m2^7*MBhat^3*Ycut^3 - 2730*MBhat^4*Ycut^3 + 
     35910*m2*MBhat^4*Ycut^3 + 247380*m2^2*MBhat^4*Ycut^3 - 
     89460*m2^3*MBhat^4*Ycut^3 - 11970*m2^4*MBhat^4*Ycut^3 - 
     1890*m2^5*MBhat^4*Ycut^3 + 420*m2^6*MBhat^4*Ycut^3 + 
     1680*MBhat^5*Ycut^3 - 16170*m2*MBhat^5*Ycut^3 - 
     54810*m2^2*MBhat^5*Ycut^3 + 32550*m2^3*MBhat^5*Ycut^3 - 
     3360*m2^4*MBhat^5*Ycut^3 + 630*m2^5*MBhat^5*Ycut^3 - 
     420*MBhat^6*Ycut^3 + 2870*m2*MBhat^6*Ycut^3 + 3360*m2^2*MBhat^6*Ycut^3 - 
     2870*m2^3*MBhat^6*Ycut^3 + 350*m2^4*MBhat^6*Ycut^3 + 20*Ycut^4 - 
     700*m2*Ycut^4 - 32760*m2^2*Ycut^4 - 85400*m2^3*Ycut^4 - 
     22400*m2^4*Ycut^4 + 11200*m2^5*Ycut^4 + 700*m2^6*Ycut^4 - 
     20*m2^7*Ycut^4 - 165*MBhat*Ycut^4 + 4725*m2*MBhat*Ycut^4 + 
     159390*m2^2*MBhat*Ycut^4 + 257250*m2^3*MBhat*Ycut^4 - 
     12600*m2^4*MBhat*Ycut^4 - 21000*m2^5*MBhat*Ycut^4 + 
     525*m2^6*MBhat*Ycut^4 - 45*m2^7*MBhat*Ycut^4 + 480*MBhat^2*Ycut^4 - 
     13020*m2*MBhat^2*Ycut^4 - 311115*m2^2*MBhat^2*Ycut^4 - 
     259560*m2^3*MBhat^2*Ycut^4 + 76965*m2^4*MBhat^2*Ycut^4 + 
     4620*m2^5*MBhat^2*Ycut^4 + 420*m2^6*MBhat^2*Ycut^4 - 
     60*m2^7*MBhat^2*Ycut^4 - 1210*MBhat^3*Ycut^4 + 21035*m2*MBhat^3*Ycut^4 + 
     304185*m2^2*MBhat^3*Ycut^4 + 80990*m2^3*MBhat^3*Ycut^4 - 
     50680*m2^4*MBhat^3*Ycut^4 + 1295*m2^5*MBhat^3*Ycut^4 + 
     175*m2^6*MBhat^3*Ycut^4 - 50*m2^7*MBhat^3*Ycut^4 + 2100*MBhat^4*Ycut^4 - 
     20160*m2*MBhat^4*Ycut^4 - 153300*m2^2*MBhat^4*Ycut^4 + 
     19740*m2^3*MBhat^4*Ycut^4 + 5040*m2^4*MBhat^4*Ycut^4 + 
     1260*m2^5*MBhat^4*Ycut^4 - 210*m2^6*MBhat^4*Ycut^4 - 
     1785*MBhat^5*Ycut^4 + 9870*m2*MBhat^5*Ycut^4 + 
     36540*m2^2*MBhat^5*Ycut^4 - 14070*m2^3*MBhat^5*Ycut^4 + 
     2100*m2^4*MBhat^5*Ycut^4 - 315*m2^5*MBhat^5*Ycut^4 + 
     560*MBhat^6*Ycut^4 - 1750*m2*MBhat^6*Ycut^4 - 2940*m2^2*MBhat^6*Ycut^4 + 
     1610*m2^3*MBhat^6*Ycut^4 - 175*m2^4*MBhat^6*Ycut^4 - 4*Ycut^5 + 
     140*m2*Ycut^5 + 8232*m2^2*Ycut^5 + 25480*m2^3*Ycut^5 + 
     12880*m2^4*Ycut^5 - 560*m2^5*Ycut^5 - 140*m2^6*Ycut^5 + 4*m2^7*Ycut^5 + 
     96*MBhat*Ycut^5 - 1197*m2*MBhat*Ycut^5 - 40383*m2^2*MBhat*Ycut^5 - 
     82950*m2^3*MBhat*Ycut^5 - 16695*m2^4*MBhat*Ycut^5 + 
     3192*m2^5*MBhat*Ycut^5 - 168*m2^6*MBhat*Ycut^5 + 9*m2^7*MBhat*Ycut^5 + 
     282*MBhat^2*Ycut^5 + 1155*m2*MBhat^2*Ycut^5 + 
     83076*m2^2*MBhat^2*Ycut^5 + 96390*m2^3*MBhat^2*Ycut^5 - 
     1785*m2^4*MBhat^2*Ycut^5 - 609*m2^5*MBhat^2*Ycut^5 - 
     147*m2^6*MBhat^2*Ycut^5 + 12*m2^7*MBhat^2*Ycut^5 - 472*MBhat^3*Ycut^5 - 
     2569*m2*MBhat^3*Ycut^5 - 82215*m2^2*MBhat^3*Ycut^5 - 
     48790*m2^3*MBhat^3*Ycut^5 + 7910*m2^4*MBhat^3*Ycut^5 - 
     385*m2^5*MBhat^3*Ycut^5 - 77*m2^6*MBhat^3*Ycut^5 + 
     10*m2^7*MBhat^3*Ycut^5 - 1176*MBhat^4*Ycut^5 + 6741*m2*MBhat^4*Ycut^5 + 
     40110*m2^2*MBhat^4*Ycut^5 + 7140*m2^3*MBhat^4*Ycut^5 - 
     630*m2^4*MBhat^4*Ycut^5 - 441*m2^5*MBhat^4*Ycut^5 + 
     42*m2^6*MBhat^4*Ycut^5 + 2184*MBhat^5*Ycut^5 - 5250*m2*MBhat^5*Ycut^5 - 
     10080*m2^2*MBhat^5*Ycut^5 + 2310*m2^3*MBhat^5*Ycut^5 - 
     735*m2^4*MBhat^5*Ycut^5 + 63*m2^5*MBhat^5*Ycut^5 - 910*MBhat^6*Ycut^5 + 
     980*m2*MBhat^6*Ycut^5 + 1260*m2^2*MBhat^6*Ycut^5 - 
     490*m2^3*MBhat^6*Ycut^5 + 35*m2^4*MBhat^6*Ycut^5 - 14*Ycut^6 + 
     56*m2*Ycut^6 - 350*m2^2*Ycut^6 - 1400*m2^3*Ycut^6 - 1330*m2^4*Ycut^6 - 
     336*m2^5*Ycut^6 + 14*m2^6*Ycut^6 - 315*MBhat*Ycut^6 + 
     1134*m2*MBhat*Ycut^6 + 105*m2^2*MBhat*Ycut^6 + 5670*m2^3*MBhat*Ycut^6 + 
     3465*m2^4*MBhat*Ycut^6 + 21*m2^6*MBhat*Ycut^6 - 336*MBhat^2*Ycut^6 + 
     1449*m2*MBhat^2*Ycut^6 - 5565*m2^2*MBhat^2*Ycut^6 - 
     5670*m2^3*MBhat^2*Ycut^6 - 2940*m2^4*MBhat^2*Ycut^6 + 
     21*m2^5*MBhat^2*Ycut^6 + 21*m2^6*MBhat^2*Ycut^6 + 1988*MBhat^3*Ycut^6 - 
     4774*m2*MBhat^3*Ycut^6 + 6650*m2^2*MBhat^3*Ycut^6 + 
     5320*m2^3*MBhat^3*Ycut^6 + 42*m2^5*MBhat^3*Ycut^6 + 
     14*m2^6*MBhat^3*Ycut^6 - 588*MBhat^4*Ycut^6 - 315*m2*MBhat^4*Ycut^6 - 
     2940*m2^3*MBhat^4*Ycut^6 + 63*m2^5*MBhat^4*Ycut^6 - 
     1785*MBhat^5*Ycut^6 + 3150*m2*MBhat^5*Ycut^6 - 630*m2^2*MBhat^5*Ycut^6 + 
     105*m2^4*MBhat^5*Ycut^6 + 1050*MBhat^6*Ycut^6 - 700*m2*MBhat^6*Ycut^6 - 
     210*m2^2*MBhat^6*Ycut^6 + 70*m2^3*MBhat^6*Ycut^6 + 80*Ycut^7 - 
     280*m2*Ycut^7 + 280*m2^2*Ycut^7 - 280*m2^3*Ycut^7 - 280*m2^4*Ycut^7 + 
     600*MBhat*Ycut^7 - 1890*m2*MBhat*Ycut^7 + 2310*m2^2*MBhat*Ycut^7 - 
     210*m2^3*MBhat*Ycut^7 + 630*m2^4*MBhat*Ycut^7 - 600*MBhat^2*Ycut^7 + 
     1050*m2*MBhat^2*Ycut^7 - 420*m2^2*MBhat^2*Ycut^7 - 
     1890*m2^3*MBhat^2*Ycut^7 - 1900*MBhat^3*Ycut^7 + 
     4340*m2*MBhat^3*Ycut^7 - 2800*m2^2*MBhat^3*Ycut^7 + 
     1680*m2^3*MBhat^3*Ycut^7 + 2100*MBhat^4*Ycut^7 - 
     2520*m2*MBhat^4*Ycut^7 + 420*MBhat^5*Ycut^7 - 1050*m2*MBhat^5*Ycut^7 + 
     630*m2^2*MBhat^5*Ycut^7 - 700*MBhat^6*Ycut^7 + 350*m2*MBhat^6*Ycut^7 - 
     190*Ycut^8 + 560*m2*Ycut^8 - 560*m2^2*Ycut^8 + 140*m2^3*Ycut^8 - 
     70*m2^4*Ycut^8 - 480*MBhat*Ycut^8 + 1260*m2*MBhat*Ycut^8 - 
     1155*m2^2*MBhat*Ycut^8 + 735*m2^3*MBhat*Ycut^8 + 1425*MBhat^2*Ycut^8 - 
     2730*m2*MBhat^2*Ycut^8 + 1470*m2^2*MBhat^2*Ycut^8 - 
     630*m2^3*MBhat^2*Ycut^8 + 260*MBhat^3*Ycut^8 - 805*m2*MBhat^3*Ycut^8 + 
     875*m2^2*MBhat^3*Ycut^8 - 1680*MBhat^4*Ycut^8 + 1890*m2*MBhat^4*Ycut^8 - 
     630*m2^2*MBhat^4*Ycut^8 + 420*MBhat^5*Ycut^8 - 105*m2*MBhat^5*Ycut^8 + 
     245*MBhat^6*Ycut^8 - 70*m2*MBhat^6*Ycut^8 + 240*Ycut^9 - 560*m2*Ycut^9 + 
     420*m2^2*Ycut^9 - 140*m2^3*Ycut^9 + 15*MBhat*Ycut^9 + 
     105*m2^3*MBhat*Ycut^9 - 1065*MBhat^2*Ycut^9 + 1575*m2*MBhat^2*Ycut^9 - 
     735*m2^2*MBhat^2*Ycut^9 + 740*MBhat^3*Ycut^9 - 805*m2*MBhat^3*Ycut^9 + 
     315*m2^2*MBhat^3*Ycut^9 + 420*MBhat^4*Ycut^9 - 315*m2*MBhat^4*Ycut^9 - 
     315*MBhat^5*Ycut^9 + 105*m2*MBhat^5*Ycut^9 - 35*MBhat^6*Ycut^9 - 
     170*Ycut^10 + 280*m2*Ycut^10 - 126*m2^2*Ycut^10 + 237*MBhat*Ycut^10 - 
     378*m2*MBhat*Ycut^10 + 189*m2^2*MBhat*Ycut^10 + 246*MBhat^2*Ycut^10 - 
     147*m2*MBhat^2*Ycut^10 - 63*m2^2*MBhat^2*Ycut^10 - 460*MBhat^3*Ycut^10 + 
     308*m2*MBhat^3*Ycut^10 + 84*MBhat^4*Ycut^10 - 63*m2*MBhat^4*Ycut^10 + 
     63*MBhat^5*Ycut^10 + 64*Ycut^11 - 56*m2*Ycut^11 - 150*MBhat*Ycut^11 + 
     126*m2*MBhat*Ycut^11 + 66*MBhat^2*Ycut^11 - 84*m2*MBhat^2*Ycut^11 + 
     62*MBhat^3*Ycut^11 + 14*m2*MBhat^3*Ycut^11 - 42*MBhat^4*Ycut^11 - 
     10*Ycut^12 + 30*MBhat*Ycut^12 - 30*MBhat^2*Ycut^12 + 
     10*MBhat^3*Ycut^12 + 140*api*MBhat^6*X1mix[0, 0, c[SL]^2, Ycut, m2, 
       mu2hat] - 700*api*MBhat^6*Ycut*X1mix[0, 0, c[SL]^2, Ycut, m2, 
       mu2hat] + 1400*api*MBhat^6*Ycut^2*X1mix[0, 0, c[SL]^2, Ycut, m2, 
       mu2hat] - 1400*api*MBhat^6*Ycut^3*X1mix[0, 0, c[SL]^2, Ycut, m2, 
       mu2hat] + 700*api*MBhat^6*Ycut^4*X1mix[0, 0, c[SL]^2, Ycut, m2, 
       mu2hat] - 140*api*MBhat^6*Ycut^5*X1mix[0, 0, c[SL]^2, Ycut, m2, 
       mu2hat] - 840*api*MBhat^5*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^5*Ycut*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^5*Ycut^2*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^5*Ycut^3*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^5*Ycut^4*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^5*Ycut^5*X1mix[0, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^4*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^4*Ycut*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     16800*api*MBhat^4*Ycut^2*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     16800*api*MBhat^4*Ycut^3*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^4*Ycut^4*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^4*Ycut^5*X1mix[0, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     1120*api*MBhat^3*X1mix[0, 3, c[SL]^2, Ycut, m2, mu2hat] + 
     5600*api*MBhat^3*Ycut*X1mix[0, 3, c[SL]^2, Ycut, m2, mu2hat] - 
     11200*api*MBhat^3*Ycut^2*X1mix[0, 3, c[SL]^2, Ycut, m2, mu2hat] + 
     11200*api*MBhat^3*Ycut^3*X1mix[0, 3, c[SL]^2, Ycut, m2, mu2hat] - 
     5600*api*MBhat^3*Ycut^4*X1mix[0, 3, c[SL]^2, Ycut, m2, mu2hat] + 
     1120*api*MBhat^3*Ycut^5*X1mix[0, 3, c[SL]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^4*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^4*Ycut*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^4*Ycut^2*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^4*Ycut^3*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^4*Ycut^4*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^4*Ycut^5*X1mix[1, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^3*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^3*Ycut*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     16800*api*MBhat^3*Ycut^2*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     16800*api*MBhat^3*Ycut^3*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^3*Ycut^4*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^3*Ycut^5*X1mix[1, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^2*X1mix[1, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^2*Ycut*X1mix[1, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     16800*api*MBhat^2*Ycut^2*X1mix[1, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     16800*api*MBhat^2*Ycut^3*X1mix[1, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^2*Ycut^4*X1mix[1, 2, c[SL]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^2*Ycut^5*X1mix[1, 2, c[SL]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^2*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^2*Ycut*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^2*Ycut^2*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^2*Ycut^3*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^2*Ycut^4*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^2*Ycut^5*X1mix[2, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat*X1mix[2, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat*Ycut*X1mix[2, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat*Ycut^2*X1mix[2, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat*Ycut^3*X1mix[2, 1, c[SL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat*Ycut^4*X1mix[2, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat*Ycut^5*X1mix[2, 1, c[SL]^2, Ycut, m2, mu2hat] + 
     140*api*X1mix[3, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     700*api*Ycut*X1mix[3, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     1400*api*Ycut^2*X1mix[3, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     1400*api*Ycut^3*X1mix[3, 0, c[SL]^2, Ycut, m2, mu2hat] + 
     700*api*Ycut^4*X1mix[3, 0, c[SL]^2, Ycut, m2, mu2hat] - 
     140*api*Ycut^5*X1mix[3, 0, c[SL]^2, Ycut, m2, mu2hat])/
    (140*(-1 + Ycut)^5)) + 
 c[SR]^2*(3*m2^2*(-4 - 20*m2 - 20*m2^2 - 4*m2^3 + 21*MBhat + 75*m2*MBhat + 
     45*m2^2*MBhat + 3*m2^3*MBhat - 45*MBhat^2 - 108*m2*MBhat^2 - 
     33*m2^2*MBhat^2 + 50*MBhat^3 + 74*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
     30*MBhat^4 - 24*m2*MBhat^4 + 9*MBhat^5 + 3*m2*MBhat^5 - MBhat^6)*
    Log[m2] - 3*m2^2*(-4 - 20*m2 - 20*m2^2 - 4*m2^3 + 21*MBhat + 
     75*m2*MBhat + 45*m2^2*MBhat + 3*m2^3*MBhat - 45*MBhat^2 - 
     108*m2*MBhat^2 - 33*m2^2*MBhat^2 + 50*MBhat^3 + 74*m2*MBhat^3 + 
     8*m2^2*MBhat^3 - 30*MBhat^4 - 24*m2*MBhat^4 + 9*MBhat^5 + 3*m2*MBhat^5 - 
     MBhat^6)*Log[1 - Ycut] - (4 - 140*m2 - 4396*m2^2 - 6300*m2^3 + 
     6300*m2^4 + 4396*m2^5 + 140*m2^6 - 4*m2^7 - 33*MBhat + 945*m2*MBhat + 
     20559*m2^2*MBhat + 11025*m2^3*MBhat - 26775*m2^4*MBhat - 
     5817*m2^5*MBhat + 105*m2^6*MBhat - 9*m2^7*MBhat + 117*MBhat^2 - 
     2688*m2*MBhat^2 - 37863*m2^2*MBhat^2 + 6300*m2^3*MBhat^2 + 
     33075*m2^4*MBhat^2 + 1008*m2^5*MBhat^2 + 63*m2^6*MBhat^2 - 
     12*m2^7*MBhat^2 - 228*MBhat^3 + 4116*m2*MBhat^3 + 34090*m2^2*MBhat^3 - 
     23870*m2^3*MBhat^3 - 14420*m2^4*MBhat^3 + 308*m2^5*MBhat^3 + 
     14*m2^6*MBhat^3 - 10*m2^7*MBhat^3 + 252*MBhat^4 - 3528*m2*MBhat^4 - 
     14910*m2^2*MBhat^4 + 16800*m2^3*MBhat^4 + 1260*m2^4*MBhat^4 + 
     168*m2^5*MBhat^4 - 42*m2^6*MBhat^4 - 147*MBhat^5 + 1575*m2*MBhat^5 + 
     2520*m2^2*MBhat^5 - 4200*m2^3*MBhat^5 + 315*m2^4*MBhat^5 - 
     63*m2^5*MBhat^5 + 35*MBhat^6 - 280*m2*MBhat^6 + 280*m2^3*MBhat^6 - 
     35*m2^4*MBhat^6 - 20*Ycut + 700*m2*Ycut + 23660*m2^2*Ycut + 
     39900*m2^3*Ycut - 23100*m2^4*Ycut - 20300*m2^5*Ycut - 700*m2^6*Ycut + 
     20*m2^7*Ycut + 165*MBhat*Ycut - 4725*m2*MBhat*Ycut - 
     111615*m2^2*MBhat*Ycut - 86625*m2^3*MBhat*Ycut + 
     114975*m2^4*MBhat*Ycut + 27825*m2^5*MBhat*Ycut - 525*m2^6*MBhat*Ycut + 
     45*m2^7*MBhat*Ycut - 585*MBhat^2*Ycut + 13440*m2*MBhat^2*Ycut + 
     208215*m2^2*MBhat^2*Ycut + 13860*m2^3*MBhat^2*Ycut - 
     151515*m2^4*MBhat^2*Ycut - 5040*m2^5*MBhat^2*Ycut - 
     315*m2^6*MBhat^2*Ycut + 60*m2^7*MBhat^2*Ycut + 1140*MBhat^3*Ycut - 
     20580*m2*MBhat^3*Ycut - 191450*m2^2*MBhat^3*Ycut + 
     88270*m2^3*MBhat^3*Ycut + 68740*m2^4*MBhat^3*Ycut - 
     1540*m2^5*MBhat^3*Ycut - 70*m2^6*MBhat^3*Ycut + 50*m2^7*MBhat^3*Ycut - 
     1260*MBhat^4*Ycut + 17640*m2*MBhat^4*Ycut + 87150*m2^2*MBhat^4*Ycut - 
     73920*m2^3*MBhat^4*Ycut - 6300*m2^4*MBhat^4*Ycut - 
     840*m2^5*MBhat^4*Ycut + 210*m2^6*MBhat^4*Ycut + 735*MBhat^5*Ycut - 
     7875*m2*MBhat^5*Ycut - 16380*m2^2*MBhat^5*Ycut + 
     19740*m2^3*MBhat^5*Ycut - 1575*m2^4*MBhat^5*Ycut + 
     315*m2^5*MBhat^5*Ycut - 175*MBhat^6*Ycut + 1400*m2*MBhat^6*Ycut + 
     420*m2^2*MBhat^6*Ycut - 1400*m2^3*MBhat^6*Ycut + 175*m2^4*MBhat^6*Ycut + 
     40*Ycut^2 - 1400*m2*Ycut^2 - 51520*m2^2*Ycut^2 - 100800*m2^3*Ycut^2 + 
     25200*m2^4*Ycut^2 + 36400*m2^5*Ycut^2 + 1400*m2^6*Ycut^2 - 
     40*m2^7*Ycut^2 - 330*MBhat*Ycut^2 + 9450*m2*MBhat*Ycut^2 + 
     245280*m2^2*MBhat*Ycut^2 + 252000*m2^3*MBhat*Ycut^2 - 
     182700*m2^4*MBhat*Ycut^2 - 52500*m2^5*MBhat*Ycut^2 + 
     1050*m2^6*MBhat*Ycut^2 - 90*m2^7*MBhat*Ycut^2 + 1170*MBhat^2*Ycut^2 - 
     26880*m2*MBhat^2*Ycut^2 - 463680*m2^2*MBhat^2*Ycut^2 - 
     141120*m2^3*MBhat^2*Ycut^2 + 268380*m2^4*MBhat^2*Ycut^2 + 
     10080*m2^5*MBhat^2*Ycut^2 + 630*m2^6*MBhat^2*Ycut^2 - 
     120*m2^7*MBhat^2*Ycut^2 - 2280*MBhat^3*Ycut^2 + 
     41160*m2*MBhat^3*Ycut^2 + 435400*m2^2*MBhat^3*Ycut^2 - 
     98840*m2^3*MBhat^3*Ycut^2 - 129080*m2^4*MBhat^3*Ycut^2 + 
     3080*m2^5*MBhat^3*Ycut^2 + 140*m2^6*MBhat^3*Ycut^2 - 
     100*m2^7*MBhat^3*Ycut^2 + 2520*MBhat^4*Ycut^2 - 
     35280*m2*MBhat^4*Ycut^2 - 205800*m2^2*MBhat^4*Ycut^2 + 
     122640*m2^3*MBhat^4*Ycut^2 + 12600*m2^4*MBhat^4*Ycut^2 + 
     1680*m2^5*MBhat^4*Ycut^2 - 420*m2^6*MBhat^4*Ycut^2 - 
     1470*MBhat^5*Ycut^2 + 15750*m2*MBhat^5*Ycut^2 + 
     42210*m2^2*MBhat^5*Ycut^2 - 36330*m2^3*MBhat^5*Ycut^2 + 
     3150*m2^4*MBhat^5*Ycut^2 - 630*m2^5*MBhat^5*Ycut^2 + 
     350*MBhat^6*Ycut^2 - 2800*m2*MBhat^6*Ycut^2 - 1890*m2^2*MBhat^6*Ycut^2 + 
     2800*m2^3*MBhat^6*Ycut^2 - 350*m2^4*MBhat^6*Ycut^2 - 40*Ycut^3 + 
     1400*m2*Ycut^3 + 57120*m2^2*Ycut^3 + 128800*m2^3*Ycut^3 + 
     2800*m2^4*Ycut^3 - 30800*m2^5*Ycut^3 - 1400*m2^6*Ycut^3 + 
     40*m2^7*Ycut^3 + 330*MBhat*Ycut^3 - 9450*m2*MBhat*Ycut^3 - 
     274680*m2^2*MBhat*Ycut^3 - 357000*m2^3*MBhat*Ycut^3 + 
     119700*m2^4*MBhat*Ycut^3 + 48300*m2^5*MBhat*Ycut^3 - 
     1050*m2^6*MBhat*Ycut^3 + 90*m2^7*MBhat*Ycut^3 - 1170*MBhat^2*Ycut^3 + 
     26880*m2*MBhat^2*Ycut^3 + 526680*m2^2*MBhat^2*Ycut^3 + 
     292320*m2^3*MBhat^2*Ycut^3 - 222180*m2^4*MBhat^2*Ycut^3 - 
     10080*m2^5*MBhat^2*Ycut^3 - 630*m2^6*MBhat^2*Ycut^3 + 
     120*m2^7*MBhat^2*Ycut^3 + 2350*MBhat^3*Ycut^3 - 
     41440*m2*MBhat^3*Ycut^3 - 505050*m2^2*MBhat^3*Ycut^3 - 
     4760*m2^3*MBhat^3*Ycut^3 + 117530*m2^4*MBhat^3*Ycut^3 - 
     2800*m2^5*MBhat^3*Ycut^3 - 210*m2^6*MBhat^3*Ycut^3 + 
     100*m2^7*MBhat^3*Ycut^3 - 2730*MBhat^4*Ycut^3 + 
     35910*m2*MBhat^4*Ycut^3 + 247380*m2^2*MBhat^4*Ycut^3 - 
     89460*m2^3*MBhat^4*Ycut^3 - 11970*m2^4*MBhat^4*Ycut^3 - 
     1890*m2^5*MBhat^4*Ycut^3 + 420*m2^6*MBhat^4*Ycut^3 + 
     1680*MBhat^5*Ycut^3 - 16170*m2*MBhat^5*Ycut^3 - 
     54810*m2^2*MBhat^5*Ycut^3 + 32550*m2^3*MBhat^5*Ycut^3 - 
     3360*m2^4*MBhat^5*Ycut^3 + 630*m2^5*MBhat^5*Ycut^3 - 
     420*MBhat^6*Ycut^3 + 2870*m2*MBhat^6*Ycut^3 + 3360*m2^2*MBhat^6*Ycut^3 - 
     2870*m2^3*MBhat^6*Ycut^3 + 350*m2^4*MBhat^6*Ycut^3 + 20*Ycut^4 - 
     700*m2*Ycut^4 - 32760*m2^2*Ycut^4 - 85400*m2^3*Ycut^4 - 
     22400*m2^4*Ycut^4 + 11200*m2^5*Ycut^4 + 700*m2^6*Ycut^4 - 
     20*m2^7*Ycut^4 - 165*MBhat*Ycut^4 + 4725*m2*MBhat*Ycut^4 + 
     159390*m2^2*MBhat*Ycut^4 + 257250*m2^3*MBhat*Ycut^4 - 
     12600*m2^4*MBhat*Ycut^4 - 21000*m2^5*MBhat*Ycut^4 + 
     525*m2^6*MBhat*Ycut^4 - 45*m2^7*MBhat*Ycut^4 + 480*MBhat^2*Ycut^4 - 
     13020*m2*MBhat^2*Ycut^4 - 311115*m2^2*MBhat^2*Ycut^4 - 
     259560*m2^3*MBhat^2*Ycut^4 + 76965*m2^4*MBhat^2*Ycut^4 + 
     4620*m2^5*MBhat^2*Ycut^4 + 420*m2^6*MBhat^2*Ycut^4 - 
     60*m2^7*MBhat^2*Ycut^4 - 1210*MBhat^3*Ycut^4 + 21035*m2*MBhat^3*Ycut^4 + 
     304185*m2^2*MBhat^3*Ycut^4 + 80990*m2^3*MBhat^3*Ycut^4 - 
     50680*m2^4*MBhat^3*Ycut^4 + 1295*m2^5*MBhat^3*Ycut^4 + 
     175*m2^6*MBhat^3*Ycut^4 - 50*m2^7*MBhat^3*Ycut^4 + 2100*MBhat^4*Ycut^4 - 
     20160*m2*MBhat^4*Ycut^4 - 153300*m2^2*MBhat^4*Ycut^4 + 
     19740*m2^3*MBhat^4*Ycut^4 + 5040*m2^4*MBhat^4*Ycut^4 + 
     1260*m2^5*MBhat^4*Ycut^4 - 210*m2^6*MBhat^4*Ycut^4 - 
     1785*MBhat^5*Ycut^4 + 9870*m2*MBhat^5*Ycut^4 + 
     36540*m2^2*MBhat^5*Ycut^4 - 14070*m2^3*MBhat^5*Ycut^4 + 
     2100*m2^4*MBhat^5*Ycut^4 - 315*m2^5*MBhat^5*Ycut^4 + 
     560*MBhat^6*Ycut^4 - 1750*m2*MBhat^6*Ycut^4 - 2940*m2^2*MBhat^6*Ycut^4 + 
     1610*m2^3*MBhat^6*Ycut^4 - 175*m2^4*MBhat^6*Ycut^4 - 4*Ycut^5 + 
     140*m2*Ycut^5 + 8232*m2^2*Ycut^5 + 25480*m2^3*Ycut^5 + 
     12880*m2^4*Ycut^5 - 560*m2^5*Ycut^5 - 140*m2^6*Ycut^5 + 4*m2^7*Ycut^5 + 
     96*MBhat*Ycut^5 - 1197*m2*MBhat*Ycut^5 - 40383*m2^2*MBhat*Ycut^5 - 
     82950*m2^3*MBhat*Ycut^5 - 16695*m2^4*MBhat*Ycut^5 + 
     3192*m2^5*MBhat*Ycut^5 - 168*m2^6*MBhat*Ycut^5 + 9*m2^7*MBhat*Ycut^5 + 
     282*MBhat^2*Ycut^5 + 1155*m2*MBhat^2*Ycut^5 + 
     83076*m2^2*MBhat^2*Ycut^5 + 96390*m2^3*MBhat^2*Ycut^5 - 
     1785*m2^4*MBhat^2*Ycut^5 - 609*m2^5*MBhat^2*Ycut^5 - 
     147*m2^6*MBhat^2*Ycut^5 + 12*m2^7*MBhat^2*Ycut^5 - 472*MBhat^3*Ycut^5 - 
     2569*m2*MBhat^3*Ycut^5 - 82215*m2^2*MBhat^3*Ycut^5 - 
     48790*m2^3*MBhat^3*Ycut^5 + 7910*m2^4*MBhat^3*Ycut^5 - 
     385*m2^5*MBhat^3*Ycut^5 - 77*m2^6*MBhat^3*Ycut^5 + 
     10*m2^7*MBhat^3*Ycut^5 - 1176*MBhat^4*Ycut^5 + 6741*m2*MBhat^4*Ycut^5 + 
     40110*m2^2*MBhat^4*Ycut^5 + 7140*m2^3*MBhat^4*Ycut^5 - 
     630*m2^4*MBhat^4*Ycut^5 - 441*m2^5*MBhat^4*Ycut^5 + 
     42*m2^6*MBhat^4*Ycut^5 + 2184*MBhat^5*Ycut^5 - 5250*m2*MBhat^5*Ycut^5 - 
     10080*m2^2*MBhat^5*Ycut^5 + 2310*m2^3*MBhat^5*Ycut^5 - 
     735*m2^4*MBhat^5*Ycut^5 + 63*m2^5*MBhat^5*Ycut^5 - 910*MBhat^6*Ycut^5 + 
     980*m2*MBhat^6*Ycut^5 + 1260*m2^2*MBhat^6*Ycut^5 - 
     490*m2^3*MBhat^6*Ycut^5 + 35*m2^4*MBhat^6*Ycut^5 - 14*Ycut^6 + 
     56*m2*Ycut^6 - 350*m2^2*Ycut^6 - 1400*m2^3*Ycut^6 - 1330*m2^4*Ycut^6 - 
     336*m2^5*Ycut^6 + 14*m2^6*Ycut^6 - 315*MBhat*Ycut^6 + 
     1134*m2*MBhat*Ycut^6 + 105*m2^2*MBhat*Ycut^6 + 5670*m2^3*MBhat*Ycut^6 + 
     3465*m2^4*MBhat*Ycut^6 + 21*m2^6*MBhat*Ycut^6 - 336*MBhat^2*Ycut^6 + 
     1449*m2*MBhat^2*Ycut^6 - 5565*m2^2*MBhat^2*Ycut^6 - 
     5670*m2^3*MBhat^2*Ycut^6 - 2940*m2^4*MBhat^2*Ycut^6 + 
     21*m2^5*MBhat^2*Ycut^6 + 21*m2^6*MBhat^2*Ycut^6 + 1988*MBhat^3*Ycut^6 - 
     4774*m2*MBhat^3*Ycut^6 + 6650*m2^2*MBhat^3*Ycut^6 + 
     5320*m2^3*MBhat^3*Ycut^6 + 42*m2^5*MBhat^3*Ycut^6 + 
     14*m2^6*MBhat^3*Ycut^6 - 588*MBhat^4*Ycut^6 - 315*m2*MBhat^4*Ycut^6 - 
     2940*m2^3*MBhat^4*Ycut^6 + 63*m2^5*MBhat^4*Ycut^6 - 
     1785*MBhat^5*Ycut^6 + 3150*m2*MBhat^5*Ycut^6 - 630*m2^2*MBhat^5*Ycut^6 + 
     105*m2^4*MBhat^5*Ycut^6 + 1050*MBhat^6*Ycut^6 - 700*m2*MBhat^6*Ycut^6 - 
     210*m2^2*MBhat^6*Ycut^6 + 70*m2^3*MBhat^6*Ycut^6 + 80*Ycut^7 - 
     280*m2*Ycut^7 + 280*m2^2*Ycut^7 - 280*m2^3*Ycut^7 - 280*m2^4*Ycut^7 + 
     600*MBhat*Ycut^7 - 1890*m2*MBhat*Ycut^7 + 2310*m2^2*MBhat*Ycut^7 - 
     210*m2^3*MBhat*Ycut^7 + 630*m2^4*MBhat*Ycut^7 - 600*MBhat^2*Ycut^7 + 
     1050*m2*MBhat^2*Ycut^7 - 420*m2^2*MBhat^2*Ycut^7 - 
     1890*m2^3*MBhat^2*Ycut^7 - 1900*MBhat^3*Ycut^7 + 
     4340*m2*MBhat^3*Ycut^7 - 2800*m2^2*MBhat^3*Ycut^7 + 
     1680*m2^3*MBhat^3*Ycut^7 + 2100*MBhat^4*Ycut^7 - 
     2520*m2*MBhat^4*Ycut^7 + 420*MBhat^5*Ycut^7 - 1050*m2*MBhat^5*Ycut^7 + 
     630*m2^2*MBhat^5*Ycut^7 - 700*MBhat^6*Ycut^7 + 350*m2*MBhat^6*Ycut^7 - 
     190*Ycut^8 + 560*m2*Ycut^8 - 560*m2^2*Ycut^8 + 140*m2^3*Ycut^8 - 
     70*m2^4*Ycut^8 - 480*MBhat*Ycut^8 + 1260*m2*MBhat*Ycut^8 - 
     1155*m2^2*MBhat*Ycut^8 + 735*m2^3*MBhat*Ycut^8 + 1425*MBhat^2*Ycut^8 - 
     2730*m2*MBhat^2*Ycut^8 + 1470*m2^2*MBhat^2*Ycut^8 - 
     630*m2^3*MBhat^2*Ycut^8 + 260*MBhat^3*Ycut^8 - 805*m2*MBhat^3*Ycut^8 + 
     875*m2^2*MBhat^3*Ycut^8 - 1680*MBhat^4*Ycut^8 + 1890*m2*MBhat^4*Ycut^8 - 
     630*m2^2*MBhat^4*Ycut^8 + 420*MBhat^5*Ycut^8 - 105*m2*MBhat^5*Ycut^8 + 
     245*MBhat^6*Ycut^8 - 70*m2*MBhat^6*Ycut^8 + 240*Ycut^9 - 560*m2*Ycut^9 + 
     420*m2^2*Ycut^9 - 140*m2^3*Ycut^9 + 15*MBhat*Ycut^9 + 
     105*m2^3*MBhat*Ycut^9 - 1065*MBhat^2*Ycut^9 + 1575*m2*MBhat^2*Ycut^9 - 
     735*m2^2*MBhat^2*Ycut^9 + 740*MBhat^3*Ycut^9 - 805*m2*MBhat^3*Ycut^9 + 
     315*m2^2*MBhat^3*Ycut^9 + 420*MBhat^4*Ycut^9 - 315*m2*MBhat^4*Ycut^9 - 
     315*MBhat^5*Ycut^9 + 105*m2*MBhat^5*Ycut^9 - 35*MBhat^6*Ycut^9 - 
     170*Ycut^10 + 280*m2*Ycut^10 - 126*m2^2*Ycut^10 + 237*MBhat*Ycut^10 - 
     378*m2*MBhat*Ycut^10 + 189*m2^2*MBhat*Ycut^10 + 246*MBhat^2*Ycut^10 - 
     147*m2*MBhat^2*Ycut^10 - 63*m2^2*MBhat^2*Ycut^10 - 460*MBhat^3*Ycut^10 + 
     308*m2*MBhat^3*Ycut^10 + 84*MBhat^4*Ycut^10 - 63*m2*MBhat^4*Ycut^10 + 
     63*MBhat^5*Ycut^10 + 64*Ycut^11 - 56*m2*Ycut^11 - 150*MBhat*Ycut^11 + 
     126*m2*MBhat*Ycut^11 + 66*MBhat^2*Ycut^11 - 84*m2*MBhat^2*Ycut^11 + 
     62*MBhat^3*Ycut^11 + 14*m2*MBhat^3*Ycut^11 - 42*MBhat^4*Ycut^11 - 
     10*Ycut^12 + 30*MBhat*Ycut^12 - 30*MBhat^2*Ycut^12 + 
     10*MBhat^3*Ycut^12 + 140*api*MBhat^6*X1mix[0, 0, c[SR]^2, Ycut, m2, 
       mu2hat] - 700*api*MBhat^6*Ycut*X1mix[0, 0, c[SR]^2, Ycut, m2, 
       mu2hat] + 1400*api*MBhat^6*Ycut^2*X1mix[0, 0, c[SR]^2, Ycut, m2, 
       mu2hat] - 1400*api*MBhat^6*Ycut^3*X1mix[0, 0, c[SR]^2, Ycut, m2, 
       mu2hat] + 700*api*MBhat^6*Ycut^4*X1mix[0, 0, c[SR]^2, Ycut, m2, 
       mu2hat] - 140*api*MBhat^6*Ycut^5*X1mix[0, 0, c[SR]^2, Ycut, m2, 
       mu2hat] - 840*api*MBhat^5*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^5*Ycut*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^5*Ycut^2*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^5*Ycut^3*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^5*Ycut^4*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^5*Ycut^5*X1mix[0, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^4*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^4*Ycut*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     16800*api*MBhat^4*Ycut^2*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     16800*api*MBhat^4*Ycut^3*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^4*Ycut^4*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^4*Ycut^5*X1mix[0, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     1120*api*MBhat^3*X1mix[0, 3, c[SR]^2, Ycut, m2, mu2hat] + 
     5600*api*MBhat^3*Ycut*X1mix[0, 3, c[SR]^2, Ycut, m2, mu2hat] - 
     11200*api*MBhat^3*Ycut^2*X1mix[0, 3, c[SR]^2, Ycut, m2, mu2hat] + 
     11200*api*MBhat^3*Ycut^3*X1mix[0, 3, c[SR]^2, Ycut, m2, mu2hat] - 
     5600*api*MBhat^3*Ycut^4*X1mix[0, 3, c[SR]^2, Ycut, m2, mu2hat] + 
     1120*api*MBhat^3*Ycut^5*X1mix[0, 3, c[SR]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^4*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^4*Ycut*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^4*Ycut^2*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^4*Ycut^3*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^4*Ycut^4*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^4*Ycut^5*X1mix[1, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^3*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^3*Ycut*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     16800*api*MBhat^3*Ycut^2*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     16800*api*MBhat^3*Ycut^3*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^3*Ycut^4*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^3*Ycut^5*X1mix[1, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^2*X1mix[1, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^2*Ycut*X1mix[1, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     16800*api*MBhat^2*Ycut^2*X1mix[1, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     16800*api*MBhat^2*Ycut^3*X1mix[1, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^2*Ycut^4*X1mix[1, 2, c[SR]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^2*Ycut^5*X1mix[1, 2, c[SR]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^2*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^2*Ycut*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^2*Ycut^2*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^2*Ycut^3*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^2*Ycut^4*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^2*Ycut^5*X1mix[2, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat*X1mix[2, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat*Ycut*X1mix[2, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat*Ycut^2*X1mix[2, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat*Ycut^3*X1mix[2, 1, c[SR]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat*Ycut^4*X1mix[2, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat*Ycut^5*X1mix[2, 1, c[SR]^2, Ycut, m2, mu2hat] + 
     140*api*X1mix[3, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     700*api*Ycut*X1mix[3, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     1400*api*Ycut^2*X1mix[3, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     1400*api*Ycut^3*X1mix[3, 0, c[SR]^2, Ycut, m2, mu2hat] + 
     700*api*Ycut^4*X1mix[3, 0, c[SR]^2, Ycut, m2, mu2hat] - 
     140*api*Ycut^5*X1mix[3, 0, c[SR]^2, Ycut, m2, mu2hat])/
    (140*(-1 + Ycut)^5)) + 
 c[SL]*(c[SR]*(12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 6*MBhat - 
       45*m2*MBhat - 60*m2^2*MBhat - 15*m2^3*MBhat + 15*MBhat^2 + 
       81*m2*MBhat^2 + 66*m2^2*MBhat^2 + 6*m2^3*MBhat^2 - 20*MBhat^3 - 
       74*m2*MBhat^3 - 32*m2^2*MBhat^3 + 15*MBhat^4 + 36*m2*MBhat^4 + 
       6*m2^2*MBhat^4 - 6*MBhat^5 - 9*m2*MBhat^5 + MBhat^6 + m2*MBhat^6)*
      Log[m2] - 12*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 6*MBhat - 
       45*m2*MBhat - 60*m2^2*MBhat - 15*m2^3*MBhat + 15*MBhat^2 + 
       81*m2*MBhat^2 + 66*m2^2*MBhat^2 + 6*m2^3*MBhat^2 - 20*MBhat^3 - 
       74*m2*MBhat^3 - 32*m2^2*MBhat^3 + 15*MBhat^4 + 36*m2*MBhat^4 + 
       6*m2^2*MBhat^4 - 6*MBhat^5 - 9*m2*MBhat^5 + MBhat^6 + m2*MBhat^6)*
      Log[1 - Ycut] - (-2*Sqrt[m2] - 202*m2^(3/2) - 850*m2^(5/2) + 
       850*m2^(9/2) + 202*m2^(11/2) + 2*m2^(13/2) + 15*Sqrt[m2]*MBhat + 
       1122*m2^(3/2)*MBhat + 2925*m2^(5/2)*MBhat - 1800*m2^(7/2)*MBhat - 
       2175*m2^(9/2)*MBhat - 90*m2^(11/2)*MBhat + 3*m2^(13/2)*MBhat - 
       48*Sqrt[m2]*MBhat^2 - 2553*m2^(3/2)*MBhat^2 - 3375*m2^(5/2)*MBhat^2 + 
       4500*m2^(7/2)*MBhat^2 + 1500*m2^(9/2)*MBhat^2 - 27*m2^(11/2)*MBhat^2 + 
       3*m2^(13/2)*MBhat^2 + 84*Sqrt[m2]*MBhat^3 + 3028*m2^(3/2)*MBhat^3 + 
       1030*m2^(5/2)*MBhat^3 - 3920*m2^(7/2)*MBhat^3 - 220*m2^(9/2)*MBhat^3 - 
       4*m2^(11/2)*MBhat^3 + 2*m2^(13/2)*MBhat^3 - 84*Sqrt[m2]*MBhat^4 - 
       1965*m2^(3/2)*MBhat^4 + 720*m2^(5/2)*MBhat^4 + 1380*m2^(7/2)*MBhat^4 - 
       60*m2^(9/2)*MBhat^4 + 9*m2^(11/2)*MBhat^4 + 45*Sqrt[m2]*MBhat^5 + 
       660*m2^(3/2)*MBhat^5 - 540*m2^(5/2)*MBhat^5 - 180*m2^(7/2)*MBhat^5 + 
       15*m2^(9/2)*MBhat^5 - 10*Sqrt[m2]*MBhat^6 - 90*m2^(3/2)*MBhat^6 + 
       90*m2^(5/2)*MBhat^6 + 10*m2^(7/2)*MBhat^6 + 8*Sqrt[m2]*Ycut + 
       868*m2^(3/2)*Ycut + 4000*m2^(5/2)*Ycut + 1200*m2^(7/2)*Ycut - 
       2800*m2^(9/2)*Ycut - 748*m2^(11/2)*Ycut - 8*m2^(13/2)*Ycut - 
       60*Sqrt[m2]*MBhat*Ycut - 4848*m2^(3/2)*MBhat*Ycut - 
       14400*m2^(5/2)*MBhat*Ycut + 3600*m2^(7/2)*MBhat*Ycut + 
       7800*m2^(9/2)*MBhat*Ycut + 360*m2^(11/2)*MBhat*Ycut - 
       12*m2^(13/2)*MBhat*Ycut + 192*Sqrt[m2]*MBhat^2*Ycut + 
       11112*m2^(3/2)*MBhat^2*Ycut + 18360*m2^(5/2)*MBhat^2*Ycut - 
       14040*m2^(7/2)*MBhat^2*Ycut - 5640*m2^(9/2)*MBhat^2*Ycut + 
       108*m2^(11/2)*MBhat^2*Ycut - 12*m2^(13/2)*MBhat^2*Ycut - 
       336*Sqrt[m2]*MBhat^3*Ycut - 13312*m2^(3/2)*MBhat^3*Ycut - 
       8560*m2^(5/2)*MBhat^3*Ycut + 13760*m2^(7/2)*MBhat^3*Ycut + 
       880*m2^(9/2)*MBhat^3*Ycut + 16*m2^(11/2)*MBhat^3*Ycut - 
       8*m2^(13/2)*MBhat^3*Ycut + 336*Sqrt[m2]*MBhat^4*Ycut + 
       8760*m2^(3/2)*MBhat^4*Ycut - 720*m2^(5/2)*MBhat^4*Ycut - 
       5160*m2^(7/2)*MBhat^4*Ycut + 240*m2^(9/2)*MBhat^4*Ycut - 
       36*m2^(11/2)*MBhat^4*Ycut - 180*Sqrt[m2]*MBhat^5*Ycut - 
       3000*m2^(3/2)*MBhat^5*Ycut + 1620*m2^(5/2)*MBhat^5*Ycut + 
       720*m2^(7/2)*MBhat^5*Ycut - 60*m2^(9/2)*MBhat^5*Ycut + 
       40*Sqrt[m2]*MBhat^6*Ycut + 420*m2^(3/2)*MBhat^6*Ycut - 
       300*m2^(5/2)*MBhat^6*Ycut - 40*m2^(7/2)*MBhat^6*Ycut - 
       12*Sqrt[m2]*Ycut^2 - 1422*m2^(3/2)*Ycut^2 - 7200*m2^(5/2)*Ycut^2 - 
       4200*m2^(7/2)*Ycut^2 + 3000*m2^(9/2)*Ycut^2 + 1002*m2^(11/2)*Ycut^2 + 
       12*m2^(13/2)*Ycut^2 + 90*Sqrt[m2]*MBhat*Ycut^2 + 
       7992*m2^(3/2)*MBhat*Ycut^2 + 27000*m2^(5/2)*MBhat*Ycut^2 + 
       1800*m2^(7/2)*MBhat*Ycut^2 - 9900*m2^(9/2)*MBhat*Ycut^2 - 
       540*m2^(11/2)*MBhat*Ycut^2 + 18*m2^(13/2)*MBhat*Ycut^2 - 
       288*Sqrt[m2]*MBhat^2*Ycut^2 - 18468*m2^(3/2)*MBhat^2*Ycut^2 - 
       37260*m2^(5/2)*MBhat^2*Ycut^2 + 13140*m2^(7/2)*MBhat^2*Ycut^2 + 
       7740*m2^(9/2)*MBhat^2*Ycut^2 - 162*m2^(11/2)*MBhat^2*Ycut^2 + 
       18*m2^(13/2)*MBhat^2*Ycut^2 + 504*Sqrt[m2]*MBhat^3*Ycut^2 + 
       22368*m2^(3/2)*MBhat^3*Ycut^2 + 21720*m2^(5/2)*MBhat^3*Ycut^2 - 
       16800*m2^(7/2)*MBhat^3*Ycut^2 - 1320*m2^(9/2)*MBhat^3*Ycut^2 - 
       24*m2^(11/2)*MBhat^3*Ycut^2 + 12*m2^(13/2)*MBhat^3*Ycut^2 - 
       504*Sqrt[m2]*MBhat^4*Ycut^2 - 14940*m2^(3/2)*MBhat^4*Ycut^2 - 
       3240*m2^(5/2)*MBhat^4*Ycut^2 + 7020*m2^(7/2)*MBhat^4*Ycut^2 - 
       360*m2^(9/2)*MBhat^4*Ycut^2 + 54*m2^(11/2)*MBhat^4*Ycut^2 + 
       270*Sqrt[m2]*MBhat^5*Ycut^2 + 5220*m2^(3/2)*MBhat^5*Ycut^2 - 
       1350*m2^(5/2)*MBhat^5*Ycut^2 - 1080*m2^(7/2)*MBhat^5*Ycut^2 + 
       90*m2^(9/2)*MBhat^5*Ycut^2 - 60*Sqrt[m2]*MBhat^6*Ycut^2 - 
       750*m2^(3/2)*MBhat^6*Ycut^2 + 330*m2^(5/2)*MBhat^6*Ycut^2 + 
       60*m2^(7/2)*MBhat^6*Ycut^2 + 8*Sqrt[m2]*Ycut^3 + 
       1068*m2^(3/2)*Ycut^3 + 6000*m2^(5/2)*Ycut^3 + 5200*m2^(7/2)*Ycut^3 - 
       800*m2^(9/2)*Ycut^3 - 548*m2^(11/2)*Ycut^3 - 8*m2^(13/2)*Ycut^3 - 
       60*Sqrt[m2]*MBhat*Ycut^3 - 6048*m2^(3/2)*MBhat*Ycut^3 - 
       23400*m2^(5/2)*MBhat*Ycut^3 - 8400*m2^(7/2)*MBhat*Ycut^3 + 
       4800*m2^(9/2)*MBhat*Ycut^3 + 360*m2^(11/2)*MBhat*Ycut^3 - 
       12*m2^(13/2)*MBhat*Ycut^3 + 192*Sqrt[m2]*MBhat^2*Ycut^3 + 
       14112*m2^(3/2)*MBhat^2*Ycut^3 + 34560*m2^(5/2)*MBhat^2*Ycut^3 - 
       840*m2^(7/2)*MBhat^2*Ycut^3 - 4440*m2^(9/2)*MBhat^2*Ycut^3 + 
       108*m2^(11/2)*MBhat^2*Ycut^3 - 12*m2^(13/2)*MBhat^2*Ycut^3 - 
       346*Sqrt[m2]*MBhat^3*Ycut^3 - 17262*m2^(3/2)*MBhat^3*Ycut^3 - 
       23460*m2^(5/2)*MBhat^3*Ycut^3 + 7460*m2^(7/2)*MBhat^3*Ycut^3 + 
       830*m2^(9/2)*MBhat^3*Ycut^3 + 26*m2^(11/2)*MBhat^3*Ycut^3 - 
       8*m2^(13/2)*MBhat^3*Ycut^3 + 366*Sqrt[m2]*MBhat^4*Ycut^3 + 
       11640*m2^(3/2)*MBhat^4*Ycut^3 + 6660*m2^(5/2)*MBhat^4*Ycut^3 - 
       4080*m2^(7/2)*MBhat^4*Ycut^3 + 270*m2^(9/2)*MBhat^4*Ycut^3 - 
       36*m2^(11/2)*MBhat^4*Ycut^3 - 210*Sqrt[m2]*MBhat^5*Ycut^3 - 
       4110*m2^(3/2)*MBhat^5*Ycut^3 - 270*m2^(5/2)*MBhat^5*Ycut^3 + 
       750*m2^(7/2)*MBhat^5*Ycut^3 - 60*m2^(9/2)*MBhat^5*Ycut^3 + 
       50*Sqrt[m2]*MBhat^6*Ycut^3 + 600*m2^(3/2)*MBhat^6*Ycut^3 - 
       90*m2^(5/2)*MBhat^6*Ycut^3 - 40*m2^(7/2)*MBhat^6*Ycut^3 - 
       2*Sqrt[m2]*Ycut^4 - 327*m2^(3/2)*Ycut^4 - 2100*m2^(5/2)*Ycut^4 - 
       2500*m2^(7/2)*Ycut^4 - 400*m2^(9/2)*Ycut^4 + 77*m2^(11/2)*Ycut^4 + 
       2*m2^(13/2)*Ycut^4 + 15*Sqrt[m2]*MBhat*Ycut^4 + 
       1872*m2^(3/2)*MBhat*Ycut^4 + 8550*m2^(5/2)*MBhat*Ycut^4 + 
       5700*m2^(7/2)*MBhat*Ycut^4 - 300*m2^(9/2)*MBhat*Ycut^4 - 
       90*m2^(11/2)*MBhat*Ycut^4 + 3*m2^(13/2)*MBhat*Ycut^4 - 
       33*Sqrt[m2]*MBhat^2*Ycut^4 - 4503*m2^(3/2)*MBhat^2*Ycut^4 - 
       13350*m2^(5/2)*MBhat^2*Ycut^4 - 3900*m2^(7/2)*MBhat^2*Ycut^4 + 
       825*m2^(9/2)*MBhat^2*Ycut^4 - 42*m2^(11/2)*MBhat^2*Ycut^4 + 
       3*m2^(13/2)*MBhat^2*Ycut^4 + 79*Sqrt[m2]*MBhat^3*Ycut^4 + 
       5538*m2^(3/2)*MBhat^3*Ycut^4 + 10290*m2^(5/2)*MBhat^3*Ycut^4 + 
       40*m2^(7/2)*MBhat^3*Ycut^4 - 185*m2^(9/2)*MBhat^3*Ycut^4 - 
       14*m2^(11/2)*MBhat^3*Ycut^4 + 2*m2^(13/2)*MBhat^3*Ycut^4 - 
       159*Sqrt[m2]*MBhat^4*Ycut^4 - 3570*m2^(3/2)*MBhat^4*Ycut^4 - 
       4140*m2^(5/2)*MBhat^4*Ycut^4 + 840*m2^(7/2)*MBhat^4*Ycut^4 - 
       105*m2^(9/2)*MBhat^4*Ycut^4 + 9*m2^(11/2)*MBhat^4*Ycut^4 + 
       150*Sqrt[m2]*MBhat^5*Ycut^4 + 1140*m2^(3/2)*MBhat^5*Ycut^4 + 
       810*m2^(5/2)*MBhat^5*Ycut^4 - 240*m2^(7/2)*MBhat^5*Ycut^4 + 
       15*m2^(9/2)*MBhat^5*Ycut^4 - 50*Sqrt[m2]*MBhat^6*Ycut^4 - 
       150*m2^(3/2)*MBhat^6*Ycut^4 - 60*m2^(5/2)*MBhat^6*Ycut^4 + 
       10*m2^(7/2)*MBhat^6*Ycut^4 + 12*m2^(3/2)*Ycut^5 + 
       120*m2^(5/2)*Ycut^5 + 240*m2^(7/2)*Ycut^5 + 120*m2^(9/2)*Ycut^5 + 
       12*m2^(11/2)*Ycut^5 - 9*Sqrt[m2]*MBhat*Ycut^5 - 
       27*m2^(3/2)*MBhat*Ycut^5 - 630*m2^(5/2)*MBhat*Ycut^5 - 
       630*m2^(7/2)*MBhat*Ycut^5 - 225*m2^(9/2)*MBhat*Ycut^5 + 
       9*m2^(11/2)*MBhat*Ycut^5 - 33*Sqrt[m2]*MBhat^2*Ycut^5 + 
       318*m2^(3/2)*MBhat^2*Ycut^5 + 750*m2^(5/2)*MBhat^2*Ycut^5 + 
       960*m2^(7/2)*MBhat^2*Ycut^5 + 15*m2^(9/2)*MBhat^2*Ycut^5 + 
       6*m2^(11/2)*MBhat^2*Ycut^5 + 93*Sqrt[m2]*MBhat^3*Ycut^5 - 
       543*m2^(3/2)*MBhat^3*Ycut^5 - 540*m2^(5/2)*MBhat^3*Ycut^5 - 
       540*m2^(7/2)*MBhat^3*Ycut^5 + 15*m2^(9/2)*MBhat^3*Ycut^5 + 
       3*m2^(11/2)*MBhat^3*Ycut^5 + 9*Sqrt[m2]*MBhat^4*Ycut^5 + 
       120*m2^(3/2)*MBhat^4*Ycut^5 + 540*m2^(5/2)*MBhat^4*Ycut^5 + 
       15*m2^(9/2)*MBhat^4*Ycut^5 - 120*Sqrt[m2]*MBhat^5*Ycut^5 + 
       180*m2^(3/2)*MBhat^5*Ycut^5 - 270*m2^(5/2)*MBhat^5*Ycut^5 + 
       30*m2^(7/2)*MBhat^5*Ycut^5 + 60*Sqrt[m2]*MBhat^6*Ycut^5 - 
       60*m2^(3/2)*MBhat^6*Ycut^5 + 30*m2^(5/2)*MBhat^6*Ycut^5 + 
       2*Sqrt[m2]*Ycut^6 - 8*m2^(3/2)*Ycut^6 + 40*m2^(5/2)*Ycut^6 + 
       20*m2^(7/2)*Ycut^6 + 30*m2^(9/2)*Ycut^6 + 30*Sqrt[m2]*MBhat*Ycut^6 - 
       132*m2^(3/2)*MBhat*Ycut^6 + 90*m2^(5/2)*MBhat*Ycut^6 - 
       240*m2^(7/2)*MBhat*Ycut^6 - 12*Sqrt[m2]*MBhat^2*Ycut^6 + 
       78*m2^(3/2)*MBhat^2*Ycut^6 + 90*m2^(5/2)*MBhat^2*Ycut^6 + 
       180*m2^(7/2)*MBhat^2*Ycut^6 - 124*Sqrt[m2]*MBhat^3*Ycut^6 + 
       272*m2^(3/2)*MBhat^3*Ycut^6 - 400*m2^(5/2)*MBhat^3*Ycut^6 + 
       114*Sqrt[m2]*MBhat^4*Ycut^6 - 180*m2^(3/2)*MBhat^4*Ycut^6 + 
       180*m2^(5/2)*MBhat^4*Ycut^6 + 30*Sqrt[m2]*MBhat^5*Ycut^6 - 
       60*m2^(3/2)*MBhat^5*Ycut^6 - 40*Sqrt[m2]*MBhat^6*Ycut^6 + 
       30*m2^(3/2)*MBhat^6*Ycut^6 - 8*Sqrt[m2]*Ycut^7 + 32*m2^(3/2)*Ycut^7 - 
       40*m2^(5/2)*Ycut^7 + 40*m2^(7/2)*Ycut^7 - 30*Sqrt[m2]*MBhat*Ycut^7 + 
       78*m2^(3/2)*MBhat*Ycut^7 - 90*m2^(5/2)*MBhat*Ycut^7 - 
       30*m2^(7/2)*MBhat*Ycut^7 + 78*Sqrt[m2]*MBhat^2*Ycut^7 - 
       192*m2^(3/2)*MBhat^2*Ycut^7 + 210*m2^(5/2)*MBhat^2*Ycut^7 + 
       16*Sqrt[m2]*MBhat^3*Ycut^7 - 8*m2^(3/2)*MBhat^3*Ycut^7 - 
       80*m2^(5/2)*MBhat^3*Ycut^7 - 96*Sqrt[m2]*MBhat^4*Ycut^7 + 
       120*m2^(3/2)*MBhat^4*Ycut^7 + 30*Sqrt[m2]*MBhat^5*Ycut^7 - 
       30*m2^(3/2)*MBhat^5*Ycut^7 + 10*Sqrt[m2]*MBhat^6*Ycut^7 + 
       12*Sqrt[m2]*Ycut^8 - 33*m2^(3/2)*Ycut^8 + 30*m2^(5/2)*Ycut^8 + 
       18*m2^(3/2)*MBhat*Ycut^8 - 45*m2^(5/2)*MBhat*Ycut^8 - 
       57*Sqrt[m2]*MBhat^2*Ycut^8 + 78*m2^(3/2)*MBhat^2*Ycut^8 + 
       15*m2^(5/2)*MBhat^2*Ycut^8 + 51*Sqrt[m2]*MBhat^3*Ycut^8 - 
       78*m2^(3/2)*MBhat^3*Ycut^8 + 9*Sqrt[m2]*MBhat^4*Ycut^8 + 
       15*m2^(3/2)*MBhat^4*Ycut^8 - 15*Sqrt[m2]*MBhat^5*Ycut^8 - 
       8*Sqrt[m2]*Ycut^9 + 12*m2^(3/2)*Ycut^9 + 15*Sqrt[m2]*MBhat*Ycut^9 - 
       27*m2^(3/2)*MBhat*Ycut^9 + 3*Sqrt[m2]*MBhat^2*Ycut^9 + 
       18*m2^(3/2)*MBhat^2*Ycut^9 - 19*Sqrt[m2]*MBhat^3*Ycut^9 - 
       3*m2^(3/2)*MBhat^3*Ycut^9 + 9*Sqrt[m2]*MBhat^4*Ycut^9 + 
       2*Sqrt[m2]*Ycut^10 - 6*Sqrt[m2]*MBhat*Ycut^10 + 
       6*Sqrt[m2]*MBhat^2*Ycut^10 - 2*Sqrt[m2]*MBhat^3*Ycut^10 - 
       5*api*MBhat^6*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*MBhat^6*Ycut*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       30*api*MBhat^6*Ycut^2*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*MBhat^6*Ycut^3*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       5*api*MBhat^6*Ycut^4*X1mix[0, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       30*api*MBhat^5*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       120*api*MBhat^5*Ycut*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       180*api*MBhat^5*Ycut^2*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       120*api*MBhat^5*Ycut^3*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       30*api*MBhat^5*Ycut^4*X1mix[0, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat^4*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       240*api*MBhat^4*Ycut*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       360*api*MBhat^4*Ycut^2*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       240*api*MBhat^4*Ycut^3*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat^4*Ycut^4*X1mix[0, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       40*api*MBhat^3*X1mix[0, 3, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       160*api*MBhat^3*Ycut*X1mix[0, 3, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       240*api*MBhat^3*Ycut^2*X1mix[0, 3, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       160*api*MBhat^3*Ycut^3*X1mix[0, 3, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       40*api*MBhat^3*Ycut^4*X1mix[0, 3, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       15*api*MBhat^4*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^4*Ycut*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       90*api*MBhat^4*Ycut^2*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^4*Ycut^3*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       15*api*MBhat^4*Ycut^4*X1mix[1, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^3*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       240*api*MBhat^3*Ycut*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       360*api*MBhat^3*Ycut^2*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       240*api*MBhat^3*Ycut^3*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^3*Ycut^4*X1mix[1, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat^2*X1mix[1, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       240*api*MBhat^2*Ycut*X1mix[1, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       360*api*MBhat^2*Ycut^2*X1mix[1, 2, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       240*api*MBhat^2*Ycut^3*X1mix[1, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       60*api*MBhat^2*Ycut^4*X1mix[1, 2, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       15*api*MBhat^2*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^2*Ycut*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       90*api*MBhat^2*Ycut^2*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       60*api*MBhat^2*Ycut^3*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       15*api*MBhat^2*Ycut^4*X1mix[2, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       30*api*MBhat*X1mix[2, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       120*api*MBhat*Ycut*X1mix[2, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       180*api*MBhat*Ycut^2*X1mix[2, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       120*api*MBhat*Ycut^3*X1mix[2, 1, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       30*api*MBhat*Ycut^4*X1mix[2, 1, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       5*api*X1mix[3, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*Ycut*X1mix[3, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       30*api*Ycut^2*X1mix[3, 0, c[SL]*c[SR], Ycut, m2, mu2hat] + 
       20*api*Ycut^3*X1mix[3, 0, c[SL]*c[SR], Ycut, m2, mu2hat] - 
       5*api*Ycut^4*X1mix[3, 0, c[SL]*c[SR], Ycut, m2, mu2hat])/
      (5*(-1 + Ycut)^4)) - (c[T]*(-10*MBhat^3*Ycut^3 + 60*m2*MBhat^3*Ycut^3 - 
      150*m2^2*MBhat^3*Ycut^3 + 200*m2^3*MBhat^3*Ycut^3 - 
      150*m2^4*MBhat^3*Ycut^3 + 60*m2^5*MBhat^3*Ycut^3 - 
      10*m2^6*MBhat^3*Ycut^3 + 30*MBhat^4*Ycut^3 - 150*m2*MBhat^4*Ycut^3 + 
      300*m2^2*MBhat^4*Ycut^3 - 300*m2^3*MBhat^4*Ycut^3 + 
      150*m2^4*MBhat^4*Ycut^3 - 30*m2^5*MBhat^4*Ycut^3 - 30*MBhat^5*Ycut^3 + 
      120*m2*MBhat^5*Ycut^3 - 180*m2^2*MBhat^5*Ycut^3 + 
      120*m2^3*MBhat^5*Ycut^3 - 30*m2^4*MBhat^5*Ycut^3 + 10*MBhat^6*Ycut^3 - 
      30*m2*MBhat^6*Ycut^3 + 30*m2^2*MBhat^6*Ycut^3 - 
      10*m2^3*MBhat^6*Ycut^3 + 15*MBhat^2*Ycut^4 - 90*m2*MBhat^2*Ycut^4 + 
      225*m2^2*MBhat^2*Ycut^4 - 300*m2^3*MBhat^2*Ycut^4 + 
      225*m2^4*MBhat^2*Ycut^4 - 90*m2^5*MBhat^2*Ycut^4 + 
      15*m2^6*MBhat^2*Ycut^4 + 15*MBhat^3*Ycut^4 - 90*m2*MBhat^3*Ycut^4 + 
      225*m2^2*MBhat^3*Ycut^4 - 300*m2^3*MBhat^3*Ycut^4 + 
      225*m2^4*MBhat^3*Ycut^4 - 90*m2^5*MBhat^3*Ycut^4 + 
      15*m2^6*MBhat^3*Ycut^4 - 135*MBhat^4*Ycut^4 + 600*m2*MBhat^4*Ycut^4 - 
      1050*m2^2*MBhat^4*Ycut^4 + 900*m2^3*MBhat^4*Ycut^4 - 
      375*m2^4*MBhat^4*Ycut^4 + 60*m2^5*MBhat^4*Ycut^4 + 165*MBhat^5*Ycut^4 - 
      570*m2*MBhat^5*Ycut^4 + 720*m2^2*MBhat^5*Ycut^4 - 
      390*m2^3*MBhat^5*Ycut^4 + 75*m2^4*MBhat^5*Ycut^4 - 60*MBhat^6*Ycut^4 + 
      150*m2*MBhat^6*Ycut^4 - 120*m2^2*MBhat^6*Ycut^4 + 
      30*m2^3*MBhat^6*Ycut^4 - 9*MBhat*Ycut^5 + 54*m2*MBhat*Ycut^5 - 
      135*m2^2*MBhat*Ycut^5 + 180*m2^3*MBhat*Ycut^5 - 135*m2^4*MBhat*Ycut^5 + 
      54*m2^5*MBhat*Ycut^5 - 9*m2^6*MBhat*Ycut^5 - 63*MBhat^2*Ycut^5 + 
      327*m2*MBhat^2*Ycut^5 - 690*m2^2*MBhat^2*Ycut^5 + 
      750*m2^3*MBhat^2*Ycut^5 - 435*m2^4*MBhat^2*Ycut^5 + 
      123*m2^5*MBhat^2*Ycut^5 - 12*m2^6*MBhat^2*Ycut^5 + 93*MBhat^3*Ycut^5 - 
      366*m2*MBhat^3*Ycut^5 + 525*m2^2*MBhat^3*Ycut^5 - 
      300*m2^3*MBhat^3*Ycut^5 + 15*m2^4*MBhat^3*Ycut^5 + 
      42*m2^5*MBhat^3*Ycut^5 - 9*m2^6*MBhat^3*Ycut^5 + 189*MBhat^4*Ycut^5 - 
      765*m2*MBhat^4*Ycut^5 + 1200*m2^2*MBhat^4*Ycut^5 - 
      900*m2^3*MBhat^4*Ycut^5 + 315*m2^4*MBhat^4*Ycut^5 - 
      39*m2^5*MBhat^4*Ycut^5 - 360*MBhat^5*Ycut^5 + 1050*m2*MBhat^5*Ycut^5 - 
      1080*m2^2*MBhat^5*Ycut^5 + 450*m2^3*MBhat^5*Ycut^5 - 
      60*m2^4*MBhat^5*Ycut^5 + 150*MBhat^6*Ycut^5 - 300*m2*MBhat^6*Ycut^5 + 
      180*m2^2*MBhat^6*Ycut^5 - 30*m2^3*MBhat^6*Ycut^5 + 2*Ycut^6 - 
      12*m2*Ycut^6 + 30*m2^2*Ycut^6 - 40*m2^3*Ycut^6 + 30*m2^4*Ycut^6 - 
      12*m2^5*Ycut^6 + 2*m2^6*Ycut^6 + 48*MBhat*Ycut^6 - 
      243*m2*MBhat*Ycut^6 + 495*m2^2*MBhat*Ycut^6 - 510*m2^3*MBhat*Ycut^6 + 
      270*m2^4*MBhat*Ycut^6 - 63*m2^5*MBhat*Ycut^6 + 3*m2^6*MBhat*Ycut^6 + 
      69*MBhat^2*Ycut^6 - 303*m2*MBhat^2*Ycut^6 + 525*m2^2*MBhat^2*Ycut^6 - 
      450*m2^3*MBhat^2*Ycut^6 + 195*m2^4*MBhat^2*Ycut^6 - 
      39*m2^5*MBhat^2*Ycut^6 + 3*m2^6*MBhat^2*Ycut^6 - 315*MBhat^3*Ycut^6 + 
      1083*m2*MBhat^3*Ycut^6 - 1350*m2^2*MBhat^3*Ycut^6 + 
      700*m2^3*MBhat^3*Ycut^6 - 105*m2^4*MBhat^3*Ycut^6 - 
      15*m2^5*MBhat^3*Ycut^6 + 2*m2^6*MBhat^3*Ycut^6 + 21*MBhat^4*Ycut^6 + 
      75*m2*MBhat^4*Ycut^6 - 300*m2^2*MBhat^4*Ycut^6 + 
      300*m2^3*MBhat^4*Ycut^6 - 105*m2^4*MBhat^4*Ycut^6 + 
      9*m2^5*MBhat^4*Ycut^6 + 375*MBhat^5*Ycut^6 - 900*m2*MBhat^5*Ycut^6 + 
      720*m2^2*MBhat^5*Ycut^6 - 210*m2^3*MBhat^5*Ycut^6 + 
      15*m2^4*MBhat^5*Ycut^6 - 200*MBhat^6*Ycut^6 + 300*m2*MBhat^6*Ycut^6 - 
      120*m2^2*MBhat^6*Ycut^6 + 10*m2^3*MBhat^6*Ycut^6 - 12*Ycut^7 + 
      60*m2*Ycut^7 - 120*m2^2*Ycut^7 + 120*m2^3*Ycut^7 - 60*m2^4*Ycut^7 + 
      12*m2^5*Ycut^7 - 99*MBhat*Ycut^7 + 405*m2*MBhat*Ycut^7 - 
      630*m2^2*MBhat*Ycut^7 + 450*m2^3*MBhat*Ycut^7 - 135*m2^4*MBhat*Ycut^7 + 
      9*m2^5*MBhat*Ycut^7 + 69*MBhat^2*Ycut^7 - 240*m2*MBhat^2*Ycut^7 + 
      300*m2^2*MBhat^2*Ycut^7 - 150*m2^3*MBhat^2*Ycut^7 + 
      15*m2^4*MBhat^2*Ycut^7 + 6*m2^5*MBhat^2*Ycut^7 + 357*MBhat^3*Ycut^7 - 
      975*m2*MBhat^3*Ycut^7 + 900*m2^2*MBhat^3*Ycut^7 - 
      300*m2^3*MBhat^3*Ycut^7 + 15*m2^4*MBhat^3*Ycut^7 + 
      3*m2^5*MBhat^3*Ycut^7 - 315*MBhat^4*Ycut^7 + 600*m2*MBhat^4*Ycut^7 - 
      300*m2^2*MBhat^4*Ycut^7 + 15*m2^4*MBhat^4*Ycut^7 - 150*MBhat^5*Ycut^7 + 
      300*m2*MBhat^5*Ycut^7 - 180*m2^2*MBhat^5*Ycut^7 + 
      30*m2^3*MBhat^5*Ycut^7 + 150*MBhat^6*Ycut^7 - 150*m2*MBhat^6*Ycut^7 + 
      30*m2^2*MBhat^6*Ycut^7 + 30*Ycut^8 - 120*m2*Ycut^8 + 180*m2^2*Ycut^8 - 
      120*m2^3*Ycut^8 + 30*m2^4*Ycut^8 + 90*MBhat*Ycut^8 - 
      270*m2*MBhat*Ycut^8 + 270*m2^2*MBhat*Ycut^8 - 90*m2^3*MBhat*Ycut^8 - 
      225*MBhat^2*Ycut^8 + 600*m2*MBhat^2*Ycut^8 - 525*m2^2*MBhat^2*Ycut^8 + 
      150*m2^3*MBhat^2*Ycut^8 - 105*MBhat^3*Ycut^8 + 180*m2*MBhat^3*Ycut^8 - 
      75*m2^2*MBhat^3*Ycut^8 + 315*MBhat^4*Ycut^8 - 450*m2*MBhat^4*Ycut^8 + 
      150*m2^2*MBhat^4*Ycut^8 - 45*MBhat^5*Ycut^8 + 30*m2*MBhat^5*Ycut^8 - 
      60*MBhat^6*Ycut^8 + 30*m2*MBhat^6*Ycut^8 - 40*Ycut^9 + 120*m2*Ycut^9 - 
      120*m2^2*Ycut^9 + 40*m2^3*Ycut^9 - 15*MBhat*Ycut^9 + 
      45*m2^2*MBhat*Ycut^9 - 30*m2^3*MBhat*Ycut^9 + 195*MBhat^2*Ycut^9 - 
      345*m2*MBhat^2*Ycut^9 + 150*m2^2*MBhat^2*Ycut^9 - 105*MBhat^3*Ycut^9 + 
      180*m2*MBhat^3*Ycut^9 - 75*m2^2*MBhat^3*Ycut^9 - 105*MBhat^4*Ycut^9 + 
      75*m2*MBhat^4*Ycut^9 + 60*MBhat^5*Ycut^9 - 30*m2*MBhat^5*Ycut^9 + 
      10*MBhat^6*Ycut^9 + 30*Ycut^10 - 60*m2*Ycut^10 + 30*m2^2*Ycut^10 - 
      36*MBhat*Ycut^10 + 81*m2*MBhat*Ycut^10 - 45*m2^2*MBhat*Ycut^10 - 
      57*MBhat^2*Ycut^10 + 33*m2*MBhat^2*Ycut^10 + 15*m2^2*MBhat^2*Ycut^10 + 
      87*MBhat^3*Ycut^10 - 69*m2*MBhat^3*Ycut^10 - 9*MBhat^4*Ycut^10 + 
      15*m2*MBhat^4*Ycut^10 - 15*MBhat^5*Ycut^10 - 12*Ycut^11 + 
      12*m2*Ycut^11 + 27*MBhat*Ycut^11 - 27*m2*MBhat*Ycut^11 - 
      9*MBhat^2*Ycut^11 + 18*m2*MBhat^2*Ycut^11 - 15*MBhat^3*Ycut^11 - 
      3*m2*MBhat^3*Ycut^11 + 9*MBhat^4*Ycut^11 + 2*Ycut^12 - 
      6*MBhat*Ycut^12 + 6*MBhat^2*Ycut^12 - 2*MBhat^3*Ycut^12 + 
      5*api*MBhat^6*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      25*api*MBhat^6*Ycut*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      50*api*MBhat^6*Ycut^2*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      50*api*MBhat^6*Ycut^3*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      25*api*MBhat^6*Ycut^4*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      5*api*MBhat^6*Ycut^5*X1mix[0, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      30*api*MBhat^5*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      150*api*MBhat^5*Ycut*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      300*api*MBhat^5*Ycut^2*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      300*api*MBhat^5*Ycut^3*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      150*api*MBhat^5*Ycut^4*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      30*api*MBhat^5*Ycut^5*X1mix[0, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      60*api*MBhat^4*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      300*api*MBhat^4*Ycut*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      600*api*MBhat^4*Ycut^2*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      600*api*MBhat^4*Ycut^3*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      300*api*MBhat^4*Ycut^4*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      60*api*MBhat^4*Ycut^5*X1mix[0, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      40*api*MBhat^3*X1mix[0, 3, c[SL]*c[T], Ycut, m2, mu2hat] + 
      200*api*MBhat^3*Ycut*X1mix[0, 3, c[SL]*c[T], Ycut, m2, mu2hat] - 
      400*api*MBhat^3*Ycut^2*X1mix[0, 3, c[SL]*c[T], Ycut, m2, mu2hat] + 
      400*api*MBhat^3*Ycut^3*X1mix[0, 3, c[SL]*c[T], Ycut, m2, mu2hat] - 
      200*api*MBhat^3*Ycut^4*X1mix[0, 3, c[SL]*c[T], Ycut, m2, mu2hat] + 
      40*api*MBhat^3*Ycut^5*X1mix[0, 3, c[SL]*c[T], Ycut, m2, mu2hat] + 
      15*api*MBhat^4*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      75*api*MBhat^4*Ycut*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      150*api*MBhat^4*Ycut^2*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      150*api*MBhat^4*Ycut^3*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      75*api*MBhat^4*Ycut^4*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      15*api*MBhat^4*Ycut^5*X1mix[1, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      60*api*MBhat^3*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      300*api*MBhat^3*Ycut*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      600*api*MBhat^3*Ycut^2*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      600*api*MBhat^3*Ycut^3*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      300*api*MBhat^3*Ycut^4*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      60*api*MBhat^3*Ycut^5*X1mix[1, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      60*api*MBhat^2*X1mix[1, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      300*api*MBhat^2*Ycut*X1mix[1, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      600*api*MBhat^2*Ycut^2*X1mix[1, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      600*api*MBhat^2*Ycut^3*X1mix[1, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      300*api*MBhat^2*Ycut^4*X1mix[1, 2, c[SL]*c[T], Ycut, m2, mu2hat] - 
      60*api*MBhat^2*Ycut^5*X1mix[1, 2, c[SL]*c[T], Ycut, m2, mu2hat] + 
      15*api*MBhat^2*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      75*api*MBhat^2*Ycut*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      150*api*MBhat^2*Ycut^2*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      150*api*MBhat^2*Ycut^3*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      75*api*MBhat^2*Ycut^4*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      15*api*MBhat^2*Ycut^5*X1mix[2, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      30*api*MBhat*X1mix[2, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      150*api*MBhat*Ycut*X1mix[2, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      300*api*MBhat*Ycut^2*X1mix[2, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      300*api*MBhat*Ycut^3*X1mix[2, 1, c[SL]*c[T], Ycut, m2, mu2hat] - 
      150*api*MBhat*Ycut^4*X1mix[2, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      30*api*MBhat*Ycut^5*X1mix[2, 1, c[SL]*c[T], Ycut, m2, mu2hat] + 
      5*api*X1mix[3, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      25*api*Ycut*X1mix[3, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      50*api*Ycut^2*X1mix[3, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      50*api*Ycut^3*X1mix[3, 0, c[SL]*c[T], Ycut, m2, mu2hat] + 
      25*api*Ycut^4*X1mix[3, 0, c[SL]*c[T], Ycut, m2, mu2hat] - 
      5*api*Ycut^5*X1mix[3, 0, c[SL]*c[T], Ycut, m2, mu2hat]))/
    (5*(-1 + Ycut)^5)) + 
 c[T]^2*(48*m2^2*(-6 - 30*m2 - 30*m2^2 - 6*m2^3 + 33*MBhat + 115*m2*MBhat + 
     65*m2^2*MBhat + 3*m2^3*MBhat - 75*MBhat^2 - 168*m2*MBhat^2 - 
     43*m2^2*MBhat^2 + 90*MBhat^3 + 114*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
     60*MBhat^4 - 34*m2*MBhat^4 + 21*MBhat^5 + 3*m2*MBhat^5 - 3*MBhat^6)*
    Log[m2] - 48*m2^2*(-6 - 30*m2 - 30*m2^2 - 6*m2^3 + 33*MBhat + 
     115*m2*MBhat + 65*m2^2*MBhat + 3*m2^3*MBhat - 75*MBhat^2 - 
     168*m2*MBhat^2 - 43*m2^2*MBhat^2 + 90*MBhat^3 + 114*m2*MBhat^3 + 
     8*m2^2*MBhat^3 - 60*MBhat^4 - 34*m2*MBhat^4 + 21*MBhat^5 + 
     3*m2*MBhat^5 - 3*MBhat^6)*Log[1 - Ycut] - 
   (24 - 840*m2 - 26376*m2^2 - 37800*m2^3 + 37800*m2^4 + 26376*m2^5 + 
     840*m2^6 - 24*m2^7 - 212*MBhat + 6020*m2*MBhat + 128268*m2^2*MBhat + 
     60900*m2^3*MBhat - 165900*m2^4*MBhat - 29988*m2^5*MBhat + 
     980*m2^6*MBhat - 68*m2^7*MBhat + 828*MBhat^2 - 18592*m2*MBhat^2 - 
     246372*m2^2*MBhat^2 + 67200*m2^3*MBhat^2 + 195300*m2^4*MBhat^2 + 
     672*m2^5*MBhat^2 + 1092*m2^6*MBhat^2 - 128*m2^7*MBhat^2 - 1872*MBhat^3 + 
     32144*m2*MBhat^3 + 229320*m2^2*MBhat^3 - 190680*m2^3*MBhat^3 - 
     68880*m2^4*MBhat^3 - 1008*m2^5*MBhat^3 + 1176*m2^6*MBhat^3 - 
     200*m2^7*MBhat^3 + 2408*MBhat^4 - 31752*m2*MBhat^4 - 
     101640*m2^2*MBhat^4 + 134400*m2^3*MBhat^4 - 7560*m2^4*MBhat^4 + 
     4872*m2^5*MBhat^4 - 728*m2^6*MBhat^4 - 1596*MBhat^5 + 16380*m2*MBhat^5 + 
     16800*m2^2*MBhat^5 - 36960*m2^3*MBhat^5 + 6300*m2^4*MBhat^5 - 
     924*m2^5*MBhat^5 + 420*MBhat^6 - 3360*m2*MBhat^6 + 3360*m2^3*MBhat^6 - 
     420*m2^4*MBhat^6 - 120*Ycut + 4200*m2*Ycut + 141960*m2^2*Ycut + 
     239400*m2^3*Ycut - 138600*m2^4*Ycut - 121800*m2^5*Ycut - 
     4200*m2^6*Ycut + 120*m2^7*Ycut + 1060*MBhat*Ycut - 30100*m2*MBhat*Ycut - 
     696780*m2^2*MBhat*Ycut - 497700*m2^3*MBhat*Ycut + 
     720300*m2^4*MBhat*Ycut + 144900*m2^5*MBhat*Ycut - 4900*m2^6*MBhat*Ycut + 
     340*m2^7*MBhat*Ycut - 4140*MBhat^2*Ycut + 92960*m2*MBhat^2*Ycut + 
     1357860*m2^2*MBhat^2*Ycut - 53760*m2^3*MBhat^2*Ycut - 
     904260*m2^4*MBhat^2*Ycut - 3360*m2^5*MBhat^2*Ycut - 
     5460*m2^6*MBhat^2*Ycut + 640*m2^7*MBhat^2*Ycut + 9360*MBhat^3*Ycut - 
     160720*m2*MBhat^3*Ycut - 1297800*m2^2*MBhat^3*Ycut + 
     761880*m2^3*MBhat^3*Ycut + 330960*m2^4*MBhat^3*Ycut + 
     5040*m2^5*MBhat^3*Ycut - 5880*m2^6*MBhat^3*Ycut + 
     1000*m2^7*MBhat^3*Ycut - 12040*MBhat^4*Ycut + 158760*m2*MBhat^4*Ycut + 
     609000*m2^2*MBhat^4*Ycut - 614880*m2^3*MBhat^4*Ycut + 
     37800*m2^4*MBhat^4*Ycut - 24360*m2^5*MBhat^4*Ycut + 
     3640*m2^6*MBhat^4*Ycut + 7980*MBhat^5*Ycut - 81900*m2*MBhat^5*Ycut - 
     119280*m2^2*MBhat^5*Ycut + 179760*m2^3*MBhat^5*Ycut - 
     31500*m2^4*MBhat^5*Ycut + 4620*m2^5*MBhat^5*Ycut - 2100*MBhat^6*Ycut + 
     16800*m2*MBhat^6*Ycut + 5040*m2^2*MBhat^6*Ycut - 
     16800*m2^3*MBhat^6*Ycut + 2100*m2^4*MBhat^6*Ycut + 240*Ycut^2 - 
     8400*m2*Ycut^2 - 309120*m2^2*Ycut^2 - 604800*m2^3*Ycut^2 + 
     151200*m2^4*Ycut^2 + 218400*m2^5*Ycut^2 + 8400*m2^6*Ycut^2 - 
     240*m2^7*Ycut^2 - 2120*MBhat*Ycut^2 + 60200*m2*MBhat*Ycut^2 + 
     1532160*m2^2*MBhat*Ycut^2 + 1478400*m2^3*MBhat*Ycut^2 - 
     1167600*m2^4*MBhat*Ycut^2 - 277200*m2^5*MBhat*Ycut^2 + 
     9800*m2^6*MBhat*Ycut^2 - 680*m2^7*MBhat*Ycut^2 + 8280*MBhat^2*Ycut^2 - 
     185920*m2*MBhat^2*Ycut^2 - 3030720*m2^2*MBhat^2*Ycut^2 - 
     598080*m2^3*MBhat^2*Ycut^2 + 1627920*m2^4*MBhat^2*Ycut^2 + 
     6720*m2^5*MBhat^2*Ycut^2 + 10920*m2^6*MBhat^2*Ycut^2 - 
     1280*m2^7*MBhat^2*Ycut^2 - 18720*MBhat^3*Ycut^2 + 
     321440*m2*MBhat^3*Ycut^2 + 2973600*m2^2*MBhat^3*Ycut^2 - 
     1044960*m2^3*MBhat^3*Ycut^2 - 628320*m2^4*MBhat^3*Ycut^2 - 
     10080*m2^5*MBhat^3*Ycut^2 + 11760*m2^6*MBhat^3*Ycut^2 - 
     2000*m2^7*MBhat^3*Ycut^2 + 24080*MBhat^4*Ycut^2 - 
     317520*m2*MBhat^4*Ycut^2 - 1470000*m2^2*MBhat^4*Ycut^2 + 
     1086960*m2^3*MBhat^4*Ycut^2 - 75600*m2^4*MBhat^4*Ycut^2 + 
     48720*m2^5*MBhat^4*Ycut^2 - 7280*m2^6*MBhat^4*Ycut^2 - 
     15960*MBhat^5*Ycut^2 + 163800*m2*MBhat^5*Ycut^2 + 
     326760*m2^2*MBhat^5*Ycut^2 - 346920*m2^3*MBhat^5*Ycut^2 + 
     63000*m2^4*MBhat^5*Ycut^2 - 9240*m2^5*MBhat^5*Ycut^2 + 
     4200*MBhat^6*Ycut^2 - 33600*m2*MBhat^6*Ycut^2 - 
     22680*m2^2*MBhat^6*Ycut^2 + 33600*m2^3*MBhat^6*Ycut^2 - 
     4200*m2^4*MBhat^6*Ycut^2 - 240*Ycut^3 + 8400*m2*Ycut^3 + 
     342720*m2^2*Ycut^3 + 772800*m2^3*Ycut^3 + 16800*m2^4*Ycut^3 - 
     184800*m2^5*Ycut^3 - 8400*m2^6*Ycut^3 + 240*m2^7*Ycut^3 + 
     2120*MBhat*Ycut^3 - 60200*m2*MBhat*Ycut^3 - 1716960*m2^2*MBhat*Ycut^3 - 
     2122400*m2^3*MBhat*Ycut^3 + 803600*m2^4*MBhat*Ycut^3 + 
     260400*m2^5*MBhat*Ycut^3 - 9800*m2^6*MBhat*Ycut^3 + 
     680*m2^7*MBhat*Ycut^3 - 8280*MBhat^2*Ycut^3 + 185920*m2*MBhat^2*Ycut^3 + 
     3450720*m2^2*MBhat^2*Ycut^3 + 1538880*m2^3*MBhat^2*Ycut^3 - 
     1387120*m2^4*MBhat^2*Ycut^3 - 6720*m2^5*MBhat^2*Ycut^3 - 
     10920*m2^6*MBhat^2*Ycut^3 + 1280*m2^7*MBhat^2*Ycut^3 + 
     20120*MBhat^3*Ycut^3 - 328160*m2*MBhat^3*Ycut^3 - 
     3465000*m2^2*MBhat^3*Ycut^3 + 395360*m2^3*MBhat^3*Ycut^3 + 
     587720*m2^4*MBhat^3*Ycut^3 + 10080*m2^5*MBhat^3*Ycut^3 - 
     12040*m2^6*MBhat^3*Ycut^3 + 2000*m2^7*MBhat^3*Ycut^3 - 
     28280*MBhat^4*Ycut^3 + 333480*m2*MBhat^4*Ycut^3 + 
     1784160*m2^2*MBhat^4*Ycut^3 - 884800*m2^3*MBhat^4*Ycut^3 + 
     74760*m2^4*MBhat^4*Ycut^3 - 49560*m2^5*MBhat^4*Ycut^3 + 
     7280*m2^6*MBhat^4*Ycut^3 + 20160*MBhat^5*Ycut^3 - 
     175560*m2*MBhat^5*Ycut^3 - 434280*m2^2*MBhat^5*Ycut^3 + 
     328440*m2^3*MBhat^5*Ycut^3 - 63840*m2^4*MBhat^5*Ycut^3 + 
     9240*m2^5*MBhat^5*Ycut^3 - 5600*MBhat^6*Ycut^3 + 
     36120*m2*MBhat^6*Ycut^3 + 38640*m2^2*MBhat^6*Ycut^3 - 
     33880*m2^3*MBhat^6*Ycut^3 + 4200*m2^4*MBhat^6*Ycut^3 + 120*Ycut^4 - 
     4200*m2*Ycut^4 - 196560*m2^2*Ycut^4 - 512400*m2^3*Ycut^4 - 
     134400*m2^4*Ycut^4 + 67200*m2^5*Ycut^4 + 4200*m2^6*Ycut^4 - 
     120*m2^7*Ycut^4 - 1060*MBhat*Ycut^4 + 30100*m2*MBhat*Ycut^4 + 
     997080*m2^2*MBhat*Ycut^4 + 1544200*m2^3*MBhat*Ycut^4 - 
     128800*m2^4*MBhat*Ycut^4 - 117600*m2^5*MBhat*Ycut^4 + 
     4900*m2^6*MBhat*Ycut^4 - 340*m2^7*MBhat*Ycut^4 + 2880*MBhat^2*Ycut^4 - 
     87080*m2*MBhat^2*Ycut^4 - 2050860*m2^2*MBhat^2*Ycut^4 - 
     1466640*m2^3*MBhat^2*Ycut^4 + 510860*m2^4*MBhat^2*Ycut^4 + 
     2520*m2^5*MBhat^2*Ycut^4 + 5880*m2^6*MBhat^2*Ycut^4 - 
     640*m2^7*MBhat^2*Ycut^4 - 13560*MBhat^3*Ycut^4 + 
     182140*m2*MBhat^3*Ycut^4 + 2073540*m2^2*MBhat^3*Ycut^4 + 
     318360*m2^3*MBhat^3*Ycut^4 - 278320*m2^4*MBhat^3*Ycut^4 - 
     2100*m2^5*MBhat^3*Ycut^4 + 6300*m2^6*MBhat^3*Ycut^4 - 
     1000*m2^7*MBhat^3*Ycut^4 + 32200*MBhat^4*Ycut^4 - 
     231840*m2*MBhat^4*Ycut^4 - 1058400*m2^2*MBhat^4*Ycut^4 + 
     252560*m2^3*MBhat^4*Ycut^4 - 30240*m2^4*MBhat^4*Ycut^4 + 
     26040*m2^5*MBhat^4*Ycut^4 - 3640*m2^6*MBhat^4*Ycut^4 - 
     30660*MBhat^5*Ycut^4 + 140280*m2*MBhat^5*Ycut^4 + 
     263760*m2^2*MBhat^5*Ycut^4 - 143640*m2^3*MBhat^5*Ycut^4 + 
     33600*m2^4*MBhat^5*Ycut^4 - 4620*m2^5*MBhat^5*Ycut^4 + 
     10080*MBhat^6*Ycut^4 - 29400*m2*MBhat^6*Ycut^4 - 
     28560*m2^2*MBhat^6*Ycut^4 + 17640*m2^3*MBhat^6*Ycut^4 - 
     2100*m2^4*MBhat^6*Ycut^4 - 24*Ycut^5 + 840*m2*Ycut^5 + 
     49392*m2^2*Ycut^5 + 152880*m2^3*Ycut^5 + 77280*m2^4*Ycut^5 - 
     3360*m2^5*Ycut^5 - 840*m2^6*Ycut^5 + 24*m2^7*Ycut^5 + 800*MBhat*Ycut^5 - 
     8708*m2*MBhat*Ycut^5 - 250236*m2^2*MBhat*Ycut^5 - 
     505400*m2^3*MBhat*Ycut^5 - 83020*m2^4*MBhat*Ycut^5 + 
     19152*m2^5*MBhat*Ycut^5 - 1232*m2^6*MBhat*Ycut^5 + 
     68*m2^7*MBhat*Ycut^5 + 4632*MBhat^2*Ycut^5 - 5180*m2*MBhat^2*Ycut^5 + 
     574224*m2^2*MBhat^2*Ycut^5 + 545160*m2^3*MBhat^2*Ycut^5 - 
     19180*m2^4*MBhat^2*Ycut^5 - 1260*m2^5*MBhat^2*Ycut^5 - 
     1428*m2^6*MBhat^2*Ycut^5 + 128*m2^7*MBhat^2*Ycut^5 + 
     864*MBhat^3*Ycut^5 - 41636*m2*MBhat^3*Ycut^5 - 
     533820*m2^2*MBhat^3*Ycut^5 - 301560*m2^3*MBhat^3*Ycut^5 + 
     68600*m2^4*MBhat^3*Ycut^5 - 4452*m2^5*MBhat^3*Ycut^5 - 
     1428*m2^6*MBhat^3*Ycut^5 + 200*m2^7*MBhat^3*Ycut^5 - 
     38024*MBhat^4*Ycut^5 + 158004*m2*MBhat^4*Ycut^5 + 
     168840*m2^2*MBhat^4*Ycut^5 + 84560*m2^3*MBhat^4*Ycut^5 - 
     7560*m2^4*MBhat^4*Ycut^5 - 5964*m2^5*MBhat^4*Ycut^5 + 
     728*m2^6*MBhat^4*Ycut^5 + 51072*MBhat^5*Ycut^5 - 
     131880*m2*MBhat^5*Ycut^5 - 13440*m2^2*MBhat^5*Ycut^5 + 
     9240*m2^3*MBhat^5*Ycut^5 - 7980*m2^4*MBhat^5*Ycut^5 + 
     924*m2^5*MBhat^5*Ycut^5 - 19320*MBhat^6*Ycut^5 + 
     28560*m2*MBhat^6*Ycut^5 + 5040*m2^2*MBhat^6*Ycut^5 - 
     4200*m2^3*MBhat^6*Ycut^5 + 420*m2^4*MBhat^6*Ycut^5 - 112*Ycut^6 + 
     504*m2*Ycut^6 - 2520*m2^2*Ycut^6 - 7840*m2^3*Ycut^6 - 8400*m2^4*Ycut^6 - 
     1848*m2^5*Ycut^6 + 56*m2^6*Ycut^6 - 3052*MBhat*Ycut^6 + 
     12376*m2*MBhat*Ycut^6 - 9660*m2^2*MBhat*Ycut^6 + 
     45080*m2^3*MBhat*Ycut^6 + 14980*m2^4*MBhat*Ycut^6 + 
     672*m2^5*MBhat*Ycut^6 + 84*m2^6*MBhat*Ycut^6 - 7392*MBhat^2*Ycut^6 + 
     31276*m2*MBhat^2*Ycut^6 - 72660*m2^2*MBhat^2*Ycut^6 - 
     5880*m2^3*MBhat^2*Ycut^6 - 27440*m2^4*MBhat^2*Ycut^6 + 
     1932*m2^5*MBhat^2*Ycut^6 + 84*m2^6*MBhat^2*Ycut^6 + 
     17808*MBhat^3*Ycut^6 - 38696*m2*MBhat^3*Ycut^6 + 
     37800*m2^2*MBhat^3*Ycut^6 + 54880*m2^3*MBhat^3*Ycut^6 - 
     15680*m2^4*MBhat^3*Ycut^6 + 3192*m2^5*MBhat^3*Ycut^6 + 
     56*m2^6*MBhat^3*Ycut^6 + 23128*MBhat^4*Ycut^6 - 
     93660*m2*MBhat^4*Ycut^6 + 109200*m2^2*MBhat^4*Ycut^6 - 
     76160*m2^3*MBhat^4*Ycut^6 + 10920*m2^4*MBhat^4*Ycut^6 + 
     252*m2^5*MBhat^4*Ycut^6 - 54180*MBhat^5*Ycut^6 + 
     113400*m2*MBhat^5*Ycut^6 - 66360*m2^2*MBhat^5*Ycut^6 + 
     13440*m2^3*MBhat^5*Ycut^6 + 420*m2^4*MBhat^5*Ycut^6 + 
     23800*MBhat^6*Ycut^6 - 25200*m2*MBhat^6*Ycut^6 + 
     4200*m2^2*MBhat^6*Ycut^6 + 280*m2^3*MBhat^6*Ycut^6 + 648*Ycut^7 - 
     2520*m2*Ycut^7 + 3360*m2^2*Ycut^7 - 3360*m2^3*Ycut^7 - 840*m2^4*Ycut^7 - 
     168*m2^5*Ycut^7 + 6176*MBhat*Ycut^7 - 21560*m2*MBhat*Ycut^7 + 
     29400*m2^2*MBhat*Ycut^7 - 12040*m2^3*MBhat*Ycut^7 + 
     7000*m2^4*MBhat*Ycut^7 - 336*m2^5*MBhat*Ycut^7 - 576*MBhat^2*Ycut^7 - 
     5600*m2*MBhat^2*Ycut^7 + 15120*m2^2*MBhat^2*Ycut^7 - 
     24360*m2^3*MBhat^2*Ycut^7 + 4480*m2^4*MBhat^2*Ycut^7 - 
     504*m2^5*MBhat^2*Ycut^7 - 25008*MBhat^3*Ycut^7 + 
     56560*m2*MBhat^3*Ycut^7 - 33600*m2^2*MBhat^3*Ycut^7 + 
     6720*m2^3*MBhat^3*Ycut^7 + 4480*m2^4*MBhat^3*Ycut^7 - 
     672*m2^5*MBhat^3*Ycut^7 + 7000*MBhat^4*Ycut^7 + 
     15120*m2*MBhat^4*Ycut^7 - 42000*m2^2*MBhat^4*Ycut^7 + 
     19600*m2^3*MBhat^4*Ycut^7 - 2520*m2^4*MBhat^4*Ycut^7 + 
     28560*MBhat^5*Ycut^7 - 54600*m2*MBhat^5*Ycut^7 + 
     29400*m2^2*MBhat^5*Ycut^7 - 3360*m2^3*MBhat^5*Ycut^7 - 
     16800*MBhat^6*Ycut^7 + 12600*m2*MBhat^6*Ycut^7 - 
     1680*m2^2*MBhat^6*Ycut^7 - 1560*Ycut^8 + 5040*m2*Ycut^8 - 
     5880*m2^2*Ycut^8 + 2520*m2^3*Ycut^8 - 840*m2^4*Ycut^8 - 
     5680*MBhat*Ycut^8 + 16240*m2*MBhat*Ycut^8 - 16380*m2^2*MBhat*Ycut^8 + 
     8540*m2^3*MBhat*Ycut^8 - 560*m2^4*MBhat*Ycut^8 + 10860*MBhat^2*Ycut^8 - 
     20720*m2*MBhat^2*Ycut^8 + 10080*m2^2*MBhat^2*Ycut^8 - 
     2520*m2^3*MBhat^2*Ycut^8 - 560*m2^4*MBhat^2*Ycut^8 + 
     10800*MBhat^3*Ycut^8 - 22820*m2*MBhat^3*Ycut^8 + 
     14700*m2^2*MBhat^3*Ycut^8 - 560*m2^4*MBhat^3*Ycut^8 - 
     17360*MBhat^4*Ycut^8 + 15120*m2*MBhat^4*Ycut^8 + 
     840*m2^2*MBhat^4*Ycut^8 - 2240*m2^3*MBhat^4*Ycut^8 - 
     3360*MBhat^5*Ycut^8 + 9660*m2*MBhat^5*Ycut^8 - 
     3360*m2^2*MBhat^5*Ycut^8 + 6300*MBhat^6*Ycut^8 - 
     2520*m2*MBhat^6*Ycut^8 + 2000*Ycut^9 - 5040*m2*Ycut^9 + 
     4200*m2^2*Ycut^9 - 1400*m2^3*Ycut^9 + 1420*MBhat*Ycut^9 - 
     2800*m2*MBhat*Ycut^9 + 1680*m2^2*MBhat*Ycut^9 + 420*m2^3*MBhat*Ycut^9 - 
     10380*MBhat^2*Ycut^9 + 16100*m2*MBhat^2*Ycut^9 - 
     7140*m2^2*MBhat^2*Ycut^9 + 3600*MBhat^3*Ycut^9 - 
     3220*m2*MBhat^3*Ycut^9 + 1260*m2^2*MBhat^3*Ycut^9 + 
     7280*MBhat^4*Ycut^9 - 5460*m2*MBhat^4*Ycut^9 - 2940*MBhat^5*Ycut^9 + 
     420*m2*MBhat^5*Ycut^9 - 980*MBhat^6*Ycut^9 - 1440*Ycut^10 + 
     2520*m2*Ycut^10 - 1176*m2^2*Ycut^10 + 1492*MBhat*Ycut^10 - 
     2632*m2*MBhat*Ycut^10 + 1428*m2^2*MBhat*Ycut^10 + 3240*MBhat^2*Ycut^10 - 
     2548*m2*MBhat^2*Ycut^10 - 252*m2^2*MBhat^2*Ycut^10 - 
     4272*MBhat^3*Ycut^10 + 2912*m2*MBhat^3*Ycut^10 + 56*MBhat^4*Ycut^10 - 
     252*m2*MBhat^4*Ycut^10 + 924*MBhat^5*Ycut^10 + 552*Ycut^11 - 
     504*m2*Ycut^11 - 1208*MBhat*Ycut^11 + 1064*m2*MBhat*Ycut^11 + 
     312*MBhat^2*Ycut^11 - 616*m2*MBhat^2*Ycut^11 + 792*MBhat^3*Ycut^11 + 
     56*m2*MBhat^3*Ycut^11 - 448*MBhat^4*Ycut^11 - 88*Ycut^12 + 
     264*MBhat*Ycut^12 - 264*MBhat^2*Ycut^12 + 88*MBhat^3*Ycut^12 + 
     35*api*MBhat^6*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     175*api*MBhat^6*Ycut*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] + 
     350*api*MBhat^6*Ycut^2*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     350*api*MBhat^6*Ycut^3*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] + 
     175*api*MBhat^6*Ycut^4*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     35*api*MBhat^6*Ycut^5*X1mix[0, 0, c[T]^2, Ycut, m2, mu2hat] - 
     210*api*MBhat^5*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     1050*api*MBhat^5*Ycut*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^5*Ycut^2*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^5*Ycut^3*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] - 
     1050*api*MBhat^5*Ycut^4*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     210*api*MBhat^5*Ycut^5*X1mix[0, 1, c[T]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^4*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^4*Ycut*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^4*Ycut^2*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^4*Ycut^3*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^4*Ycut^4*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^4*Ycut^5*X1mix[0, 2, c[T]^2, Ycut, m2, mu2hat] - 
     280*api*MBhat^3*X1mix[0, 3, c[T]^2, Ycut, m2, mu2hat] + 
     1400*api*MBhat^3*Ycut*X1mix[0, 3, c[T]^2, Ycut, m2, mu2hat] - 
     2800*api*MBhat^3*Ycut^2*X1mix[0, 3, c[T]^2, Ycut, m2, mu2hat] + 
     2800*api*MBhat^3*Ycut^3*X1mix[0, 3, c[T]^2, Ycut, m2, mu2hat] - 
     1400*api*MBhat^3*Ycut^4*X1mix[0, 3, c[T]^2, Ycut, m2, mu2hat] + 
     280*api*MBhat^3*Ycut^5*X1mix[0, 3, c[T]^2, Ycut, m2, mu2hat] + 
     105*api*MBhat^4*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     525*api*MBhat^4*Ycut*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] + 
     1050*api*MBhat^4*Ycut^2*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     1050*api*MBhat^4*Ycut^3*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] + 
     525*api*MBhat^4*Ycut^4*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     105*api*MBhat^4*Ycut^5*X1mix[1, 0, c[T]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^3*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^3*Ycut*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^3*Ycut^2*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^3*Ycut^3*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^3*Ycut^4*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^3*Ycut^5*X1mix[1, 1, c[T]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^2*X1mix[1, 2, c[T]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^2*Ycut*X1mix[1, 2, c[T]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^2*Ycut^2*X1mix[1, 2, c[T]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^2*Ycut^3*X1mix[1, 2, c[T]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^2*Ycut^4*X1mix[1, 2, c[T]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^2*Ycut^5*X1mix[1, 2, c[T]^2, Ycut, m2, mu2hat] + 
     105*api*MBhat^2*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] - 
     525*api*MBhat^2*Ycut*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] + 
     1050*api*MBhat^2*Ycut^2*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] - 
     1050*api*MBhat^2*Ycut^3*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] + 
     525*api*MBhat^2*Ycut^4*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] - 
     105*api*MBhat^2*Ycut^5*X1mix[2, 0, c[T]^2, Ycut, m2, mu2hat] - 
     210*api*MBhat*X1mix[2, 1, c[T]^2, Ycut, m2, mu2hat] + 
     1050*api*MBhat*Ycut*X1mix[2, 1, c[T]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat*Ycut^2*X1mix[2, 1, c[T]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat*Ycut^3*X1mix[2, 1, c[T]^2, Ycut, m2, mu2hat] - 
     1050*api*MBhat*Ycut^4*X1mix[2, 1, c[T]^2, Ycut, m2, mu2hat] + 
     210*api*MBhat*Ycut^5*X1mix[2, 1, c[T]^2, Ycut, m2, mu2hat] + 
     35*api*X1mix[3, 0, c[T]^2, Ycut, m2, mu2hat] - 
     175*api*Ycut*X1mix[3, 0, c[T]^2, Ycut, m2, mu2hat] + 
     350*api*Ycut^2*X1mix[3, 0, c[T]^2, Ycut, m2, mu2hat] - 
     350*api*Ycut^3*X1mix[3, 0, c[T]^2, Ycut, m2, mu2hat] + 
     175*api*Ycut^4*X1mix[3, 0, c[T]^2, Ycut, m2, mu2hat] - 
     35*api*Ycut^5*X1mix[3, 0, c[T]^2, Ycut, m2, mu2hat])/
    (35*(-1 + Ycut)^5)) + 
 c[VL]^2*(6*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 95*m2*MBhat + 
     55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 138*m2*MBhat^2 - 
     38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
     45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 3*m2*MBhat^5 - 2*MBhat^6)*
    Log[m2] - 6*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 
     95*m2*MBhat + 55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 
     138*m2*MBhat^2 - 38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 
     8*m2^2*MBhat^3 - 45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 
     3*m2*MBhat^5 - 2*MBhat^6)*Log[1 - Ycut] - 
   (5 - 175*m2 - 5495*m2^2 - 7875*m2^3 + 7875*m2^4 + 5495*m2^5 + 175*m2^6 - 
     5*m2^7 - 43*MBhat + 1225*m2*MBhat + 26313*m2^2*MBhat + 
     13125*m2^3*MBhat - 34125*m2^4*MBhat - 6657*m2^5*MBhat + 175*m2^6*MBhat - 
     13*m2^7*MBhat + 162*MBhat^2 - 3668*m2*MBhat^2 - 49728*m2^2*MBhat^2 + 
     11550*m2^3*MBhat^2 + 40950*m2^4*MBhat^2 + 588*m2^5*MBhat^2 + 
     168*m2^6*MBhat^2 - 22*m2^7*MBhat^2 - 348*MBhat^3 + 6076*m2*MBhat^3 + 
     45710*m2^2*MBhat^3 - 35770*m2^3*MBhat^3 - 15820*m2^4*MBhat^3 + 
     28*m2^5*MBhat^3 + 154*m2^6*MBhat^3 - 30*m2^7*MBhat^3 + 427*MBhat^4 - 
     5733*m2*MBhat^4 - 20160*m2^2*MBhat^4 + 25200*m2^3*MBhat^4 - 
     315*m2^4*MBhat^4 + 693*m2^5*MBhat^4 - 112*m2^6*MBhat^4 - 273*MBhat^5 + 
     2835*m2*MBhat^5 + 3360*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 + 
     945*m2^4*MBhat^5 - 147*m2^5*MBhat^5 + 70*MBhat^6 - 560*m2*MBhat^6 + 
     560*m2^3*MBhat^6 - 70*m2^4*MBhat^6 - 25*Ycut + 875*m2*Ycut + 
     29575*m2^2*Ycut + 49875*m2^3*Ycut - 28875*m2^4*Ycut - 25375*m2^5*Ycut - 
     875*m2^6*Ycut + 25*m2^7*Ycut + 215*MBhat*Ycut - 6125*m2*MBhat*Ycut - 
     142905*m2^2*MBhat*Ycut - 105525*m2^3*MBhat*Ycut + 
     147525*m2^4*MBhat*Ycut + 32025*m2^5*MBhat*Ycut - 875*m2^6*MBhat*Ycut + 
     65*m2^7*MBhat*Ycut - 810*MBhat^2*Ycut + 18340*m2*MBhat^2*Ycut + 
     273840*m2^2*MBhat^2*Ycut + 210*m2^3*MBhat^2*Ycut - 
     188790*m2^4*MBhat^2*Ycut - 2940*m2^5*MBhat^2*Ycut - 
     840*m2^6*MBhat^2*Ycut + 110*m2^7*MBhat^2*Ycut + 1740*MBhat^3*Ycut - 
     30380*m2*MBhat^3*Ycut - 257950*m2^2*MBhat^3*Ycut + 
     139370*m2^3*MBhat^3*Ycut + 75740*m2^4*MBhat^3*Ycut - 
     140*m2^5*MBhat^3*Ycut - 770*m2^6*MBhat^3*Ycut + 150*m2^7*MBhat^3*Ycut - 
     2135*MBhat^4*Ycut + 28665*m2*MBhat^4*Ycut + 119700*m2^2*MBhat^4*Ycut - 
     113820*m2^3*MBhat^4*Ycut + 1575*m2^4*MBhat^4*Ycut - 
     3465*m2^5*MBhat^4*Ycut + 560*m2^6*MBhat^4*Ycut + 1365*MBhat^5*Ycut - 
     14175*m2*MBhat^5*Ycut - 23100*m2^2*MBhat^5*Ycut + 
     32340*m2^3*MBhat^5*Ycut - 4725*m2^4*MBhat^5*Ycut + 
     735*m2^5*MBhat^5*Ycut - 350*MBhat^6*Ycut + 2800*m2*MBhat^6*Ycut + 
     840*m2^2*MBhat^6*Ycut - 2800*m2^3*MBhat^6*Ycut + 350*m2^4*MBhat^6*Ycut + 
     50*Ycut^2 - 1750*m2*Ycut^2 - 64400*m2^2*Ycut^2 - 126000*m2^3*Ycut^2 + 
     31500*m2^4*Ycut^2 + 45500*m2^5*Ycut^2 + 1750*m2^6*Ycut^2 - 
     50*m2^7*Ycut^2 - 430*MBhat*Ycut^2 + 12250*m2*MBhat*Ycut^2 + 
     314160*m2^2*MBhat*Ycut^2 + 310800*m2^3*MBhat*Ycut^2 - 
     237300*m2^4*MBhat*Ycut^2 - 60900*m2^5*MBhat*Ycut^2 + 
     1750*m2^6*MBhat*Ycut^2 - 130*m2^7*MBhat*Ycut^2 + 1620*MBhat^2*Ycut^2 - 
     36680*m2*MBhat^2*Ycut^2 - 610680*m2^2*MBhat^2*Ycut^2 - 
     145320*m2^3*MBhat^2*Ycut^2 + 337680*m2^4*MBhat^2*Ycut^2 + 
     5880*m2^5*MBhat^2*Ycut^2 + 1680*m2^6*MBhat^2*Ycut^2 - 
     220*m2^7*MBhat^2*Ycut^2 - 3480*MBhat^3*Ycut^2 + 
     60760*m2*MBhat^3*Ycut^2 + 589400*m2^2*MBhat^3*Ycut^2 - 
     180040*m2^3*MBhat^3*Ycut^2 - 143080*m2^4*MBhat^3*Ycut^2 + 
     280*m2^5*MBhat^3*Ycut^2 + 1540*m2^6*MBhat^3*Ycut^2 - 
     300*m2^7*MBhat^3*Ycut^2 + 4270*MBhat^4*Ycut^2 - 
     57330*m2*MBhat^4*Ycut^2 - 286650*m2^2*MBhat^4*Ycut^2 + 
     197190*m2^3*MBhat^4*Ycut^2 - 3150*m2^4*MBhat^4*Ycut^2 + 
     6930*m2^5*MBhat^4*Ycut^2 - 1120*m2^6*MBhat^4*Ycut^2 - 
     2730*MBhat^5*Ycut^2 + 28350*m2*MBhat^5*Ycut^2 + 
     61950*m2^2*MBhat^5*Ycut^2 - 61530*m2^3*MBhat^5*Ycut^2 + 
     9450*m2^4*MBhat^5*Ycut^2 - 1470*m2^5*MBhat^5*Ycut^2 + 
     700*MBhat^6*Ycut^2 - 5600*m2*MBhat^6*Ycut^2 - 3780*m2^2*MBhat^6*Ycut^2 + 
     5600*m2^3*MBhat^6*Ycut^2 - 700*m2^4*MBhat^6*Ycut^2 - 50*Ycut^3 + 
     1750*m2*Ycut^3 + 71400*m2^2*Ycut^3 + 161000*m2^3*Ycut^3 + 
     3500*m2^4*Ycut^3 - 38500*m2^5*Ycut^3 - 1750*m2^6*Ycut^3 + 
     50*m2^7*Ycut^3 + 430*MBhat*Ycut^3 - 12250*m2*MBhat*Ycut^3 - 
     351960*m2^2*MBhat*Ycut^3 - 443800*m2^3*MBhat*Ycut^3 + 
     160300*m2^4*MBhat*Ycut^3 + 56700*m2^5*MBhat*Ycut^3 - 
     1750*m2^6*MBhat*Ycut^3 + 130*m2^7*MBhat*Ycut^3 - 1620*MBhat^2*Ycut^3 + 
     36680*m2*MBhat^2*Ycut^3 + 694680*m2^2*MBhat^2*Ycut^3 + 
     338520*m2^3*MBhat^2*Ycut^3 - 284480*m2^4*MBhat^2*Ycut^3 - 
     5880*m2^5*MBhat^2*Ycut^3 - 1680*m2^6*MBhat^2*Ycut^3 + 
     220*m2^7*MBhat^2*Ycut^3 + 3620*MBhat^3*Ycut^3 - 
     61320*m2*MBhat^3*Ycut^3 - 686700*m2^2*MBhat^3*Ycut^3 + 
     48440*m2^3*MBhat^3*Ycut^3 + 131180*m2^4*MBhat^3*Ycut^3 + 
     280*m2^5*MBhat^3*Ycut^3 - 1680*m2^6*MBhat^3*Ycut^3 + 
     300*m2^7*MBhat^3*Ycut^3 - 4690*MBhat^4*Ycut^3 + 
     58590*m2*MBhat^4*Ycut^3 + 348810*m2^2*MBhat^4*Ycut^3 - 
     157430*m2^3*MBhat^4*Ycut^3 + 4410*m2^4*MBhat^4*Ycut^3 - 
     7350*m2^5*MBhat^4*Ycut^3 + 1120*m2^6*MBhat^4*Ycut^3 + 
     3150*MBhat^5*Ycut^3 - 29190*m2*MBhat^5*Ycut^3 - 
     82950*m2^2*MBhat^5*Ycut^3 + 58170*m2^3*MBhat^5*Ycut^3 - 
     9870*m2^4*MBhat^5*Ycut^3 + 1470*m2^5*MBhat^5*Ycut^3 - 
     840*MBhat^6*Ycut^3 + 5740*m2*MBhat^6*Ycut^3 + 6720*m2^2*MBhat^6*Ycut^3 - 
     5740*m2^3*MBhat^6*Ycut^3 + 700*m2^4*MBhat^6*Ycut^3 + 25*Ycut^4 - 
     875*m2*Ycut^4 - 40950*m2^2*Ycut^4 - 106750*m2^3*Ycut^4 - 
     28000*m2^4*Ycut^4 + 14000*m2^5*Ycut^4 + 875*m2^6*Ycut^4 - 
     25*m2^7*Ycut^4 - 215*MBhat*Ycut^4 + 6125*m2*MBhat*Ycut^4 + 
     204330*m2^2*MBhat*Ycut^4 + 321650*m2^3*MBhat*Ycut^4 - 
     22400*m2^4*MBhat*Ycut^4 - 25200*m2^5*MBhat*Ycut^4 + 
     875*m2^6*MBhat*Ycut^4 - 65*m2^7*MBhat*Ycut^4 + 705*MBhat^2*Ycut^4 - 
     18025*m2*MBhat^2*Ycut^4 - 410340*m2^2*MBhat^2*Ycut^4 - 
     315210*m2^3*MBhat^2*Ycut^4 + 103915*m2^4*MBhat^2*Ycut^4 + 
     1995*m2^5*MBhat^2*Ycut^4 + 1050*m2^6*MBhat^2*Ycut^4 - 
     110*m2^7*MBhat^2*Ycut^4 - 2195*MBhat^3*Ycut^4 + 
     32655*m2*MBhat^3*Ycut^4 + 412860*m2^2*MBhat^3*Ycut^4 + 
     78190*m2^3*MBhat^3*Ycut^4 - 58555*m2^4*MBhat^3*Ycut^4 - 
     245*m2^5*MBhat^3*Ycut^4 + 980*m2^6*MBhat^3*Ycut^4 - 
     150*m2^7*MBhat^3*Ycut^4 + 4130*MBhat^4*Ycut^4 - 
     34860*m2*MBhat^4*Ycut^4 - 216300*m2^2*MBhat^4*Ycut^4 + 
     47740*m2^3*MBhat^4*Ycut^4 - 3885*m2^4*MBhat^4*Ycut^4 + 
     4305*m2^5*MBhat^4*Ycut^4 - 560*m2^6*MBhat^4*Ycut^4 - 
     3570*MBhat^5*Ycut^4 + 18480*m2*MBhat^5*Ycut^4 + 
     56280*m2^2*MBhat^5*Ycut^4 - 27720*m2^3*MBhat^5*Ycut^4 + 
     5775*m2^4*MBhat^5*Ycut^4 - 735*m2^5*MBhat^5*Ycut^4 + 
     1120*MBhat^6*Ycut^4 - 3500*m2*MBhat^6*Ycut^4 - 
     5880*m2^2*MBhat^6*Ycut^4 + 3220*m2^3*MBhat^6*Ycut^4 - 
     350*m2^4*MBhat^6*Ycut^4 - 5*Ycut^5 + 175*m2*Ycut^5 + 10290*m2^2*Ycut^5 + 
     31850*m2^3*Ycut^5 + 16100*m2^4*Ycut^5 - 700*m2^5*Ycut^5 - 
     175*m2^6*Ycut^5 + 5*m2^7*Ycut^5 + 85*MBhat*Ycut^5 - 
     1309*m2*MBhat*Ycut^5 - 52416*m2^2*MBhat*Ycut^5 - 
     103390*m2^3*MBhat*Ycut^5 - 19670*m2^4*MBhat*Ycut^5 + 
     4368*m2^5*MBhat*Ycut^5 - 301*m2^6*MBhat*Ycut^5 + 13*m2^7*MBhat*Ycut^5 + 
     279*MBhat^2*Ycut^5 + 2219*m2*MBhat^2*Ycut^5 + 
     108486*m2^2*MBhat^2*Ycut^5 + 121590*m2^3*MBhat^2*Ycut^5 - 
     6335*m2^4*MBhat^2*Ycut^5 + 399*m2^5*MBhat^2*Ycut^5 - 
     336*m2^6*MBhat^2*Ycut^5 + 22*m2^7*MBhat^2*Ycut^5 + 523*MBhat^3*Ycut^5 - 
     9051*m2*MBhat^3*Ycut^5 - 104160*m2^2*MBhat^3*Ycut^5 - 
     64190*m2^3*MBhat^3*Ycut^5 + 12635*m2^4*MBhat^3*Ycut^5 - 
     455*m2^5*MBhat^3*Ycut^5 - 280*m2^6*MBhat^3*Ycut^5 + 
     30*m2^7*MBhat^3*Ycut^5 - 4018*MBhat^4*Ycut^5 + 17766*m2*MBhat^4*Ycut^5 + 
     49560*m2^2*MBhat^4*Ycut^5 + 7840*m2^3*MBhat^4*Ycut^5 + 
     945*m2^4*MBhat^4*Ycut^5 - 1239*m2^5*MBhat^4*Ycut^5 + 
     112*m2^6*MBhat^4*Ycut^5 + 4956*MBhat^5*Ycut^5 - 
     11760*m2*MBhat^5*Ycut^5 - 14280*m2^2*MBhat^5*Ycut^5 + 
     5460*m2^3*MBhat^5*Ycut^5 - 1785*m2^4*MBhat^5*Ycut^5 + 
     147*m2^5*MBhat^5*Ycut^5 - 1820*MBhat^6*Ycut^5 + 1960*m2*MBhat^6*Ycut^5 + 
     2520*m2^2*MBhat^6*Ycut^5 - 980*m2^3*MBhat^6*Ycut^5 + 
     70*m2^4*MBhat^6*Ycut^5 - 7*Ycut^6 + 7*m2*Ycut^6 - 280*m2^2*Ycut^6 - 
     1960*m2^3*Ycut^6 - 1505*m2^4*Ycut^6 - 483*m2^5*Ycut^6 + 28*m2^6*Ycut^6 - 
     203*MBhat*Ycut^6 + 413*m2*MBhat*Ycut^6 + 2310*m2^2*MBhat*Ycut^6 + 
     4900*m2^3*MBhat*Ycut^6 + 5495*m2^4*MBhat*Ycut^6 - 
     357*m2^5*MBhat*Ycut^6 + 42*m2^6*MBhat*Ycut^6 - 609*MBhat^2*Ycut^6 + 
     2513*m2*MBhat^2*Ycut^6 - 8190*m2^2*MBhat^2*Ycut^6 - 
     6720*m2^3*MBhat^2*Ycut^6 - 3535*m2^4*MBhat^2*Ycut^6 - 
     21*m2^5*MBhat^2*Ycut^6 + 42*m2^6*MBhat^2*Ycut^6 + 1015*MBhat^3*Ycut^6 + 
     357*m2*MBhat^3*Ycut^6 - 1400*m2^2*MBhat^3*Ycut^6 + 
     14420*m2^3*MBhat^3*Ycut^6 - 2695*m2^4*MBhat^3*Ycut^6 + 
     315*m2^5*MBhat^3*Ycut^6 + 28*m2^6*MBhat^3*Ycut^6 + 2744*MBhat^4*Ycut^6 - 
     11340*m2*MBhat^4*Ycut^6 + 11550*m2^2*MBhat^4*Ycut^6 - 
     8890*m2^3*MBhat^4*Ycut^6 + 630*m2^4*MBhat^4*Ycut^6 + 
     126*m2^5*MBhat^4*Ycut^6 - 5040*MBhat^5*Ycut^6 + 9450*m2*MBhat^5*Ycut^6 - 
     3570*m2^2*MBhat^5*Ycut^6 + 210*m2^3*MBhat^5*Ycut^6 + 
     210*m2^4*MBhat^5*Ycut^6 + 2100*MBhat^6*Ycut^6 - 1400*m2*MBhat^6*Ycut^6 - 
     420*m2^2*MBhat^6*Ycut^6 + 140*m2^3*MBhat^6*Ycut^6 + 37*Ycut^7 - 
     35*m2*Ycut^7 - 280*m2^2*Ycut^7 + 280*m2^3*Ycut^7 - 665*m2^4*Ycut^7 + 
     63*m2^5*Ycut^7 + 379*MBhat*Ycut^7 - 805*m2*MBhat*Ycut^7 + 
     420*m2^2*MBhat*Ycut^7 + 1540*m2^3*MBhat*Ycut^7 + 245*m2^4*MBhat*Ycut^7 + 
     21*m2^5*MBhat*Ycut^7 + 111*MBhat^2*Ycut^7 - 1855*m2*MBhat^2*Ycut^7 + 
     3780*m2^2*MBhat^2*Ycut^7 - 5040*m2^3*MBhat^2*Ycut^7 + 
     665*m2^4*MBhat^2*Ycut^7 - 21*m2^5*MBhat^2*Ycut^7 - 1577*MBhat^3*Ycut^7 + 
     2415*m2*MBhat^3*Ycut^7 + 700*m2^2*MBhat^3*Ycut^7 - 
     420*m2^3*MBhat^3*Ycut^7 + 665*m2^4*MBhat^3*Ycut^7 - 
     63*m2^5*MBhat^3*Ycut^7 - 280*MBhat^4*Ycut^7 + 4830*m2*MBhat^4*Ycut^7 - 
     7350*m2^2*MBhat^4*Ycut^7 + 2450*m2^3*MBhat^4*Ycut^7 - 
     210*m2^4*MBhat^4*Ycut^7 + 2730*MBhat^5*Ycut^7 - 5250*m2*MBhat^5*Ycut^7 + 
     2730*m2^2*MBhat^5*Ycut^7 - 210*m2^3*MBhat^5*Ycut^7 - 
     1400*MBhat^6*Ycut^7 + 700*m2*MBhat^6*Ycut^7 - 80*Ycut^8 + 70*m2*Ycut^8 + 
     245*m2^2*Ycut^8 - 455*m2^3*Ycut^8 + 70*m2^4*Ycut^8 - 320*MBhat*Ycut^8 + 
     770*m2*MBhat*Ycut^8 - 735*m2^2*MBhat*Ycut^8 + 805*m2^3*MBhat*Ycut^8 - 
     70*m2^4*MBhat*Ycut^8 + 495*MBhat^2*Ycut^8 + 245*m2*MBhat^2*Ycut^8 - 
     1680*m2^2*MBhat^2*Ycut^8 + 420*m2^3*MBhat^2*Ycut^8 - 
     70*m2^4*MBhat^2*Ycut^8 + 745*MBhat^3*Ycut^8 - 1995*m2*MBhat^3*Ycut^8 + 
     1750*m2^2*MBhat^3*Ycut^8 - 70*m2^4*MBhat^3*Ycut^8 - 805*MBhat^4*Ycut^8 - 
     315*m2*MBhat^4*Ycut^8 + 840*m2^2*MBhat^4*Ycut^8 - 
     280*m2^3*MBhat^4*Ycut^8 - 525*MBhat^5*Ycut^8 + 1365*m2*MBhat^5*Ycut^8 - 
     420*m2^2*MBhat^5*Ycut^8 + 490*MBhat^6*Ycut^8 - 140*m2*MBhat^6*Ycut^8 + 
     90*Ycut^9 - 70*m2*Ycut^9 - 105*m2^2*Ycut^9 + 35*m2^3*Ycut^9 + 
     80*MBhat*Ycut^9 - 350*m2*MBhat*Ycut^9 + 525*m2^2*MBhat*Ycut^9 - 
     105*m2^3*MBhat*Ycut^9 - 465*MBhat^2*Ycut^9 + 385*m2*MBhat^2*Ycut^9 - 
     210*m2^2*MBhat^2*Ycut^9 + 85*MBhat^3*Ycut^9 + 455*m2*MBhat^3*Ycut^9 - 
     210*m2^2*MBhat^3*Ycut^9 + 385*MBhat^4*Ycut^9 - 315*m2*MBhat^4*Ycut^9 - 
     105*MBhat^5*Ycut^9 - 105*m2*MBhat^5*Ycut^9 - 70*MBhat^6*Ycut^9 - 
     55*Ycut^10 + 35*m2*Ycut^10 + 53*MBhat*Ycut^10 + 49*m2*MBhat*Ycut^10 - 
     42*m2^2*MBhat*Ycut^10 + 129*MBhat^2*Ycut^10 - 161*m2*MBhat^2*Ycut^10 + 
     42*m2^2*MBhat^2*Ycut^10 - 155*MBhat^3*Ycut^10 + 35*m2*MBhat^3*Ycut^10 - 
     14*MBhat^4*Ycut^10 + 42*m2*MBhat^4*Ycut^10 + 42*MBhat^5*Ycut^10 + 
     17*Ycut^11 - 7*m2*Ycut^11 - 37*MBhat*Ycut^11 + 7*m2*MBhat*Ycut^11 + 
     9*MBhat^2*Ycut^11 + 7*m2*MBhat^2*Ycut^11 + 25*MBhat^3*Ycut^11 - 
     7*m2*MBhat^3*Ycut^11 - 14*MBhat^4*Ycut^11 - 2*Ycut^12 + 
     6*MBhat*Ycut^12 - 6*MBhat^2*Ycut^12 + 2*MBhat^3*Ycut^12 + 
     70*api*MBhat^6*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     350*api*MBhat^6*Ycut*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     700*api*MBhat^6*Ycut^2*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     700*api*MBhat^6*Ycut^3*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     350*api*MBhat^6*Ycut^4*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     70*api*MBhat^6*Ycut^5*X1mix[0, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat^5*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^5*Ycut*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^5*Ycut^2*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^5*Ycut^3*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^5*Ycut^4*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^5*Ycut^5*X1mix[0, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^4*X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^4*Ycut*X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^4*Ycut^2*X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^4*Ycut^3*X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^4*Ycut^4*X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat^4*Ycut^5*X1mix[0, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     560*api*MBhat^3*X1mix[0, 3, c[VL]^2, Ycut, m2, mu2hat] + 
     2800*api*MBhat^3*Ycut*X1mix[0, 3, c[VL]^2, Ycut, m2, mu2hat] - 
     5600*api*MBhat^3*Ycut^2*X1mix[0, 3, c[VL]^2, Ycut, m2, mu2hat] + 
     5600*api*MBhat^3*Ycut^3*X1mix[0, 3, c[VL]^2, Ycut, m2, mu2hat] - 
     2800*api*MBhat^3*Ycut^4*X1mix[0, 3, c[VL]^2, Ycut, m2, mu2hat] + 
     560*api*MBhat^3*Ycut^5*X1mix[0, 3, c[VL]^2, Ycut, m2, mu2hat] + 
     210*api*MBhat^4*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     1050*api*MBhat^4*Ycut*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^4*Ycut^2*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^4*Ycut^3*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     1050*api*MBhat^4*Ycut^4*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     210*api*MBhat^4*Ycut^5*X1mix[1, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat^3*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^3*Ycut*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^3*Ycut^2*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^3*Ycut^3*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^3*Ycut^4*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^3*Ycut^5*X1mix[1, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^2*X1mix[1, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat^2*Ycut*X1mix[1, 2, c[VL]^2, Ycut, m2, mu2hat] + 
     8400*api*MBhat^2*Ycut^2*X1mix[1, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     8400*api*MBhat^2*Ycut^3*X1mix[1, 2, c[VL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat^2*Ycut^4*X1mix[1, 2, c[VL]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat^2*Ycut^5*X1mix[1, 2, c[VL]^2, Ycut, m2, mu2hat] + 
     210*api*MBhat^2*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     1050*api*MBhat^2*Ycut*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat^2*Ycut^2*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat^2*Ycut^3*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     1050*api*MBhat^2*Ycut^4*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     210*api*MBhat^2*Ycut^5*X1mix[2, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat*X1mix[2, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     2100*api*MBhat*Ycut*X1mix[2, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     4200*api*MBhat*Ycut^2*X1mix[2, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     4200*api*MBhat*Ycut^3*X1mix[2, 1, c[VL]^2, Ycut, m2, mu2hat] - 
     2100*api*MBhat*Ycut^4*X1mix[2, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat*Ycut^5*X1mix[2, 1, c[VL]^2, Ycut, m2, mu2hat] + 
     70*api*X1mix[3, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     350*api*Ycut*X1mix[3, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     700*api*Ycut^2*X1mix[3, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     700*api*Ycut^3*X1mix[3, 0, c[VL]^2, Ycut, m2, mu2hat] + 
     350*api*Ycut^4*X1mix[3, 0, c[VL]^2, Ycut, m2, mu2hat] - 
     70*api*Ycut^5*X1mix[3, 0, c[VL]^2, Ycut, m2, mu2hat])/
    (70*(-1 + Ycut)^5)) + 
 c[VR]*(-24*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 6*MBhat - 
     45*m2*MBhat - 60*m2^2*MBhat - 15*m2^3*MBhat + 15*MBhat^2 + 
     81*m2*MBhat^2 + 66*m2^2*MBhat^2 + 6*m2^3*MBhat^2 - 20*MBhat^3 - 
     74*m2*MBhat^3 - 32*m2^2*MBhat^3 + 15*MBhat^4 + 36*m2*MBhat^4 + 
     6*m2^2*MBhat^4 - 6*MBhat^5 - 9*m2*MBhat^5 + MBhat^6 + m2*MBhat^6)*
    Log[m2] + 24*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 6*MBhat - 
     45*m2*MBhat - 60*m2^2*MBhat - 15*m2^3*MBhat + 15*MBhat^2 + 
     81*m2*MBhat^2 + 66*m2^2*MBhat^2 + 6*m2^3*MBhat^2 - 20*MBhat^3 - 
     74*m2*MBhat^3 - 32*m2^2*MBhat^3 + 15*MBhat^4 + 36*m2*MBhat^4 + 
     6*m2^2*MBhat^4 - 6*MBhat^5 - 9*m2*MBhat^5 + MBhat^6 + m2*MBhat^6)*
    Log[1 - Ycut] + (-4*Sqrt[m2] - 404*m2^(3/2) - 1700*m2^(5/2) + 
     1700*m2^(9/2) + 404*m2^(11/2) + 4*m2^(13/2) + 30*Sqrt[m2]*MBhat + 
     2244*m2^(3/2)*MBhat + 5850*m2^(5/2)*MBhat - 3600*m2^(7/2)*MBhat - 
     4350*m2^(9/2)*MBhat - 180*m2^(11/2)*MBhat + 6*m2^(13/2)*MBhat - 
     96*Sqrt[m2]*MBhat^2 - 5106*m2^(3/2)*MBhat^2 - 6750*m2^(5/2)*MBhat^2 + 
     9000*m2^(7/2)*MBhat^2 + 3000*m2^(9/2)*MBhat^2 - 54*m2^(11/2)*MBhat^2 + 
     6*m2^(13/2)*MBhat^2 + 168*Sqrt[m2]*MBhat^3 + 6056*m2^(3/2)*MBhat^3 + 
     2060*m2^(5/2)*MBhat^3 - 7840*m2^(7/2)*MBhat^3 - 440*m2^(9/2)*MBhat^3 - 
     8*m2^(11/2)*MBhat^3 + 4*m2^(13/2)*MBhat^3 - 168*Sqrt[m2]*MBhat^4 - 
     3930*m2^(3/2)*MBhat^4 + 1440*m2^(5/2)*MBhat^4 + 2760*m2^(7/2)*MBhat^4 - 
     120*m2^(9/2)*MBhat^4 + 18*m2^(11/2)*MBhat^4 + 90*Sqrt[m2]*MBhat^5 + 
     1320*m2^(3/2)*MBhat^5 - 1080*m2^(5/2)*MBhat^5 - 360*m2^(7/2)*MBhat^5 + 
     30*m2^(9/2)*MBhat^5 - 20*Sqrt[m2]*MBhat^6 - 180*m2^(3/2)*MBhat^6 + 
     180*m2^(5/2)*MBhat^6 + 20*m2^(7/2)*MBhat^6 + 16*Sqrt[m2]*Ycut + 
     1736*m2^(3/2)*Ycut + 8000*m2^(5/2)*Ycut + 2400*m2^(7/2)*Ycut - 
     5600*m2^(9/2)*Ycut - 1496*m2^(11/2)*Ycut - 16*m2^(13/2)*Ycut - 
     120*Sqrt[m2]*MBhat*Ycut - 9696*m2^(3/2)*MBhat*Ycut - 
     28800*m2^(5/2)*MBhat*Ycut + 7200*m2^(7/2)*MBhat*Ycut + 
     15600*m2^(9/2)*MBhat*Ycut + 720*m2^(11/2)*MBhat*Ycut - 
     24*m2^(13/2)*MBhat*Ycut + 384*Sqrt[m2]*MBhat^2*Ycut + 
     22224*m2^(3/2)*MBhat^2*Ycut + 36720*m2^(5/2)*MBhat^2*Ycut - 
     28080*m2^(7/2)*MBhat^2*Ycut - 11280*m2^(9/2)*MBhat^2*Ycut + 
     216*m2^(11/2)*MBhat^2*Ycut - 24*m2^(13/2)*MBhat^2*Ycut - 
     672*Sqrt[m2]*MBhat^3*Ycut - 26624*m2^(3/2)*MBhat^3*Ycut - 
     17120*m2^(5/2)*MBhat^3*Ycut + 27520*m2^(7/2)*MBhat^3*Ycut + 
     1760*m2^(9/2)*MBhat^3*Ycut + 32*m2^(11/2)*MBhat^3*Ycut - 
     16*m2^(13/2)*MBhat^3*Ycut + 672*Sqrt[m2]*MBhat^4*Ycut + 
     17520*m2^(3/2)*MBhat^4*Ycut - 1440*m2^(5/2)*MBhat^4*Ycut - 
     10320*m2^(7/2)*MBhat^4*Ycut + 480*m2^(9/2)*MBhat^4*Ycut - 
     72*m2^(11/2)*MBhat^4*Ycut - 360*Sqrt[m2]*MBhat^5*Ycut - 
     6000*m2^(3/2)*MBhat^5*Ycut + 3240*m2^(5/2)*MBhat^5*Ycut + 
     1440*m2^(7/2)*MBhat^5*Ycut - 120*m2^(9/2)*MBhat^5*Ycut + 
     80*Sqrt[m2]*MBhat^6*Ycut + 840*m2^(3/2)*MBhat^6*Ycut - 
     600*m2^(5/2)*MBhat^6*Ycut - 80*m2^(7/2)*MBhat^6*Ycut - 
     24*Sqrt[m2]*Ycut^2 - 2844*m2^(3/2)*Ycut^2 - 14400*m2^(5/2)*Ycut^2 - 
     8400*m2^(7/2)*Ycut^2 + 6000*m2^(9/2)*Ycut^2 + 2004*m2^(11/2)*Ycut^2 + 
     24*m2^(13/2)*Ycut^2 + 180*Sqrt[m2]*MBhat*Ycut^2 + 
     15984*m2^(3/2)*MBhat*Ycut^2 + 54000*m2^(5/2)*MBhat*Ycut^2 + 
     3600*m2^(7/2)*MBhat*Ycut^2 - 19800*m2^(9/2)*MBhat*Ycut^2 - 
     1080*m2^(11/2)*MBhat*Ycut^2 + 36*m2^(13/2)*MBhat*Ycut^2 - 
     576*Sqrt[m2]*MBhat^2*Ycut^2 - 36936*m2^(3/2)*MBhat^2*Ycut^2 - 
     74520*m2^(5/2)*MBhat^2*Ycut^2 + 26280*m2^(7/2)*MBhat^2*Ycut^2 + 
     15480*m2^(9/2)*MBhat^2*Ycut^2 - 324*m2^(11/2)*MBhat^2*Ycut^2 + 
     36*m2^(13/2)*MBhat^2*Ycut^2 + 1008*Sqrt[m2]*MBhat^3*Ycut^2 + 
     44736*m2^(3/2)*MBhat^3*Ycut^2 + 43440*m2^(5/2)*MBhat^3*Ycut^2 - 
     33600*m2^(7/2)*MBhat^3*Ycut^2 - 2640*m2^(9/2)*MBhat^3*Ycut^2 - 
     48*m2^(11/2)*MBhat^3*Ycut^2 + 24*m2^(13/2)*MBhat^3*Ycut^2 - 
     1008*Sqrt[m2]*MBhat^4*Ycut^2 - 29880*m2^(3/2)*MBhat^4*Ycut^2 - 
     6480*m2^(5/2)*MBhat^4*Ycut^2 + 14040*m2^(7/2)*MBhat^4*Ycut^2 - 
     720*m2^(9/2)*MBhat^4*Ycut^2 + 108*m2^(11/2)*MBhat^4*Ycut^2 + 
     540*Sqrt[m2]*MBhat^5*Ycut^2 + 10440*m2^(3/2)*MBhat^5*Ycut^2 - 
     2700*m2^(5/2)*MBhat^5*Ycut^2 - 2160*m2^(7/2)*MBhat^5*Ycut^2 + 
     180*m2^(9/2)*MBhat^5*Ycut^2 - 120*Sqrt[m2]*MBhat^6*Ycut^2 - 
     1500*m2^(3/2)*MBhat^6*Ycut^2 + 660*m2^(5/2)*MBhat^6*Ycut^2 + 
     120*m2^(7/2)*MBhat^6*Ycut^2 + 16*Sqrt[m2]*Ycut^3 + 
     2136*m2^(3/2)*Ycut^3 + 12000*m2^(5/2)*Ycut^3 + 10400*m2^(7/2)*Ycut^3 - 
     1600*m2^(9/2)*Ycut^3 - 1096*m2^(11/2)*Ycut^3 - 16*m2^(13/2)*Ycut^3 - 
     120*Sqrt[m2]*MBhat*Ycut^3 - 12096*m2^(3/2)*MBhat*Ycut^3 - 
     46800*m2^(5/2)*MBhat*Ycut^3 - 16800*m2^(7/2)*MBhat*Ycut^3 + 
     9600*m2^(9/2)*MBhat*Ycut^3 + 720*m2^(11/2)*MBhat*Ycut^3 - 
     24*m2^(13/2)*MBhat*Ycut^3 + 384*Sqrt[m2]*MBhat^2*Ycut^3 + 
     28224*m2^(3/2)*MBhat^2*Ycut^3 + 69120*m2^(5/2)*MBhat^2*Ycut^3 - 
     1680*m2^(7/2)*MBhat^2*Ycut^3 - 8880*m2^(9/2)*MBhat^2*Ycut^3 + 
     216*m2^(11/2)*MBhat^2*Ycut^3 - 24*m2^(13/2)*MBhat^2*Ycut^3 - 
     692*Sqrt[m2]*MBhat^3*Ycut^3 - 34524*m2^(3/2)*MBhat^3*Ycut^3 - 
     46920*m2^(5/2)*MBhat^3*Ycut^3 + 14920*m2^(7/2)*MBhat^3*Ycut^3 + 
     1660*m2^(9/2)*MBhat^3*Ycut^3 + 52*m2^(11/2)*MBhat^3*Ycut^3 - 
     16*m2^(13/2)*MBhat^3*Ycut^3 + 732*Sqrt[m2]*MBhat^4*Ycut^3 + 
     23280*m2^(3/2)*MBhat^4*Ycut^3 + 13320*m2^(5/2)*MBhat^4*Ycut^3 - 
     8160*m2^(7/2)*MBhat^4*Ycut^3 + 540*m2^(9/2)*MBhat^4*Ycut^3 - 
     72*m2^(11/2)*MBhat^4*Ycut^3 - 420*Sqrt[m2]*MBhat^5*Ycut^3 - 
     8220*m2^(3/2)*MBhat^5*Ycut^3 - 540*m2^(5/2)*MBhat^5*Ycut^3 + 
     1500*m2^(7/2)*MBhat^5*Ycut^3 - 120*m2^(9/2)*MBhat^5*Ycut^3 + 
     100*Sqrt[m2]*MBhat^6*Ycut^3 + 1200*m2^(3/2)*MBhat^6*Ycut^3 - 
     180*m2^(5/2)*MBhat^6*Ycut^3 - 80*m2^(7/2)*MBhat^6*Ycut^3 - 
     4*Sqrt[m2]*Ycut^4 - 654*m2^(3/2)*Ycut^4 - 4200*m2^(5/2)*Ycut^4 - 
     5000*m2^(7/2)*Ycut^4 - 800*m2^(9/2)*Ycut^4 + 154*m2^(11/2)*Ycut^4 + 
     4*m2^(13/2)*Ycut^4 + 30*Sqrt[m2]*MBhat*Ycut^4 + 
     3744*m2^(3/2)*MBhat*Ycut^4 + 17100*m2^(5/2)*MBhat*Ycut^4 + 
     11400*m2^(7/2)*MBhat*Ycut^4 - 600*m2^(9/2)*MBhat*Ycut^4 - 
     180*m2^(11/2)*MBhat*Ycut^4 + 6*m2^(13/2)*MBhat*Ycut^4 - 
     66*Sqrt[m2]*MBhat^2*Ycut^4 - 9006*m2^(3/2)*MBhat^2*Ycut^4 - 
     26700*m2^(5/2)*MBhat^2*Ycut^4 - 7800*m2^(7/2)*MBhat^2*Ycut^4 + 
     1650*m2^(9/2)*MBhat^2*Ycut^4 - 84*m2^(11/2)*MBhat^2*Ycut^4 + 
     6*m2^(13/2)*MBhat^2*Ycut^4 + 158*Sqrt[m2]*MBhat^3*Ycut^4 + 
     11076*m2^(3/2)*MBhat^3*Ycut^4 + 20580*m2^(5/2)*MBhat^3*Ycut^4 + 
     80*m2^(7/2)*MBhat^3*Ycut^4 - 370*m2^(9/2)*MBhat^3*Ycut^4 - 
     28*m2^(11/2)*MBhat^3*Ycut^4 + 4*m2^(13/2)*MBhat^3*Ycut^4 - 
     318*Sqrt[m2]*MBhat^4*Ycut^4 - 7140*m2^(3/2)*MBhat^4*Ycut^4 - 
     8280*m2^(5/2)*MBhat^4*Ycut^4 + 1680*m2^(7/2)*MBhat^4*Ycut^4 - 
     210*m2^(9/2)*MBhat^4*Ycut^4 + 18*m2^(11/2)*MBhat^4*Ycut^4 + 
     300*Sqrt[m2]*MBhat^5*Ycut^4 + 2280*m2^(3/2)*MBhat^5*Ycut^4 + 
     1620*m2^(5/2)*MBhat^5*Ycut^4 - 480*m2^(7/2)*MBhat^5*Ycut^4 + 
     30*m2^(9/2)*MBhat^5*Ycut^4 - 100*Sqrt[m2]*MBhat^6*Ycut^4 - 
     300*m2^(3/2)*MBhat^6*Ycut^4 - 120*m2^(5/2)*MBhat^6*Ycut^4 + 
     20*m2^(7/2)*MBhat^6*Ycut^4 + 24*m2^(3/2)*Ycut^5 + 240*m2^(5/2)*Ycut^5 + 
     480*m2^(7/2)*Ycut^5 + 240*m2^(9/2)*Ycut^5 + 24*m2^(11/2)*Ycut^5 - 
     18*Sqrt[m2]*MBhat*Ycut^5 - 54*m2^(3/2)*MBhat*Ycut^5 - 
     1260*m2^(5/2)*MBhat*Ycut^5 - 1260*m2^(7/2)*MBhat*Ycut^5 - 
     450*m2^(9/2)*MBhat*Ycut^5 + 18*m2^(11/2)*MBhat*Ycut^5 - 
     66*Sqrt[m2]*MBhat^2*Ycut^5 + 636*m2^(3/2)*MBhat^2*Ycut^5 + 
     1500*m2^(5/2)*MBhat^2*Ycut^5 + 1920*m2^(7/2)*MBhat^2*Ycut^5 + 
     30*m2^(9/2)*MBhat^2*Ycut^5 + 12*m2^(11/2)*MBhat^2*Ycut^5 + 
     186*Sqrt[m2]*MBhat^3*Ycut^5 - 1086*m2^(3/2)*MBhat^3*Ycut^5 - 
     1080*m2^(5/2)*MBhat^3*Ycut^5 - 1080*m2^(7/2)*MBhat^3*Ycut^5 + 
     30*m2^(9/2)*MBhat^3*Ycut^5 + 6*m2^(11/2)*MBhat^3*Ycut^5 + 
     18*Sqrt[m2]*MBhat^4*Ycut^5 + 240*m2^(3/2)*MBhat^4*Ycut^5 + 
     1080*m2^(5/2)*MBhat^4*Ycut^5 + 30*m2^(9/2)*MBhat^4*Ycut^5 - 
     240*Sqrt[m2]*MBhat^5*Ycut^5 + 360*m2^(3/2)*MBhat^5*Ycut^5 - 
     540*m2^(5/2)*MBhat^5*Ycut^5 + 60*m2^(7/2)*MBhat^5*Ycut^5 + 
     120*Sqrt[m2]*MBhat^6*Ycut^5 - 120*m2^(3/2)*MBhat^6*Ycut^5 + 
     60*m2^(5/2)*MBhat^6*Ycut^5 + 4*Sqrt[m2]*Ycut^6 - 16*m2^(3/2)*Ycut^6 + 
     80*m2^(5/2)*Ycut^6 + 40*m2^(7/2)*Ycut^6 + 60*m2^(9/2)*Ycut^6 + 
     60*Sqrt[m2]*MBhat*Ycut^6 - 264*m2^(3/2)*MBhat*Ycut^6 + 
     180*m2^(5/2)*MBhat*Ycut^6 - 480*m2^(7/2)*MBhat*Ycut^6 - 
     24*Sqrt[m2]*MBhat^2*Ycut^6 + 156*m2^(3/2)*MBhat^2*Ycut^6 + 
     180*m2^(5/2)*MBhat^2*Ycut^6 + 360*m2^(7/2)*MBhat^2*Ycut^6 - 
     248*Sqrt[m2]*MBhat^3*Ycut^6 + 544*m2^(3/2)*MBhat^3*Ycut^6 - 
     800*m2^(5/2)*MBhat^3*Ycut^6 + 228*Sqrt[m2]*MBhat^4*Ycut^6 - 
     360*m2^(3/2)*MBhat^4*Ycut^6 + 360*m2^(5/2)*MBhat^4*Ycut^6 + 
     60*Sqrt[m2]*MBhat^5*Ycut^6 - 120*m2^(3/2)*MBhat^5*Ycut^6 - 
     80*Sqrt[m2]*MBhat^6*Ycut^6 + 60*m2^(3/2)*MBhat^6*Ycut^6 - 
     16*Sqrt[m2]*Ycut^7 + 64*m2^(3/2)*Ycut^7 - 80*m2^(5/2)*Ycut^7 + 
     80*m2^(7/2)*Ycut^7 - 60*Sqrt[m2]*MBhat*Ycut^7 + 
     156*m2^(3/2)*MBhat*Ycut^7 - 180*m2^(5/2)*MBhat*Ycut^7 - 
     60*m2^(7/2)*MBhat*Ycut^7 + 156*Sqrt[m2]*MBhat^2*Ycut^7 - 
     384*m2^(3/2)*MBhat^2*Ycut^7 + 420*m2^(5/2)*MBhat^2*Ycut^7 + 
     32*Sqrt[m2]*MBhat^3*Ycut^7 - 16*m2^(3/2)*MBhat^3*Ycut^7 - 
     160*m2^(5/2)*MBhat^3*Ycut^7 - 192*Sqrt[m2]*MBhat^4*Ycut^7 + 
     240*m2^(3/2)*MBhat^4*Ycut^7 + 60*Sqrt[m2]*MBhat^5*Ycut^7 - 
     60*m2^(3/2)*MBhat^5*Ycut^7 + 20*Sqrt[m2]*MBhat^6*Ycut^7 + 
     24*Sqrt[m2]*Ycut^8 - 66*m2^(3/2)*Ycut^8 + 60*m2^(5/2)*Ycut^8 + 
     36*m2^(3/2)*MBhat*Ycut^8 - 90*m2^(5/2)*MBhat*Ycut^8 - 
     114*Sqrt[m2]*MBhat^2*Ycut^8 + 156*m2^(3/2)*MBhat^2*Ycut^8 + 
     30*m2^(5/2)*MBhat^2*Ycut^8 + 102*Sqrt[m2]*MBhat^3*Ycut^8 - 
     156*m2^(3/2)*MBhat^3*Ycut^8 + 18*Sqrt[m2]*MBhat^4*Ycut^8 + 
     30*m2^(3/2)*MBhat^4*Ycut^8 - 30*Sqrt[m2]*MBhat^5*Ycut^8 - 
     16*Sqrt[m2]*Ycut^9 + 24*m2^(3/2)*Ycut^9 + 30*Sqrt[m2]*MBhat*Ycut^9 - 
     54*m2^(3/2)*MBhat*Ycut^9 + 6*Sqrt[m2]*MBhat^2*Ycut^9 + 
     36*m2^(3/2)*MBhat^2*Ycut^9 - 38*Sqrt[m2]*MBhat^3*Ycut^9 - 
     6*m2^(3/2)*MBhat^3*Ycut^9 + 18*Sqrt[m2]*MBhat^4*Ycut^9 + 
     4*Sqrt[m2]*Ycut^10 - 12*Sqrt[m2]*MBhat*Ycut^10 + 
     12*Sqrt[m2]*MBhat^2*Ycut^10 - 4*Sqrt[m2]*MBhat^3*Ycut^10 + 
     5*api*MBhat^6*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*MBhat^6*Ycut*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     30*api*MBhat^6*Ycut^2*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*MBhat^6*Ycut^3*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     5*api*MBhat^6*Ycut^4*X1mix[0, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     30*api*MBhat^5*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     120*api*MBhat^5*Ycut*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     180*api*MBhat^5*Ycut^2*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     120*api*MBhat^5*Ycut^3*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     30*api*MBhat^5*Ycut^4*X1mix[0, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat^4*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     240*api*MBhat^4*Ycut*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     360*api*MBhat^4*Ycut^2*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     240*api*MBhat^4*Ycut^3*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat^4*Ycut^4*X1mix[0, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     40*api*MBhat^3*X1mix[0, 3, SM*c[VR], Ycut, m2, mu2hat] + 
     160*api*MBhat^3*Ycut*X1mix[0, 3, SM*c[VR], Ycut, m2, mu2hat] - 
     240*api*MBhat^3*Ycut^2*X1mix[0, 3, SM*c[VR], Ycut, m2, mu2hat] + 
     160*api*MBhat^3*Ycut^3*X1mix[0, 3, SM*c[VR], Ycut, m2, mu2hat] - 
     40*api*MBhat^3*Ycut^4*X1mix[0, 3, SM*c[VR], Ycut, m2, mu2hat] + 
     15*api*MBhat^4*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^4*Ycut*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     90*api*MBhat^4*Ycut^2*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^4*Ycut^3*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     15*api*MBhat^4*Ycut^4*X1mix[1, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^3*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     240*api*MBhat^3*Ycut*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     360*api*MBhat^3*Ycut^2*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     240*api*MBhat^3*Ycut^3*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^3*Ycut^4*X1mix[1, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat^2*X1mix[1, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     240*api*MBhat^2*Ycut*X1mix[1, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     360*api*MBhat^2*Ycut^2*X1mix[1, 2, SM*c[VR], Ycut, m2, mu2hat] - 
     240*api*MBhat^2*Ycut^3*X1mix[1, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     60*api*MBhat^2*Ycut^4*X1mix[1, 2, SM*c[VR], Ycut, m2, mu2hat] + 
     15*api*MBhat^2*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^2*Ycut*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     90*api*MBhat^2*Ycut^2*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     60*api*MBhat^2*Ycut^3*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     15*api*MBhat^2*Ycut^4*X1mix[2, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     30*api*MBhat*X1mix[2, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     120*api*MBhat*Ycut*X1mix[2, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     180*api*MBhat*Ycut^2*X1mix[2, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     120*api*MBhat*Ycut^3*X1mix[2, 1, SM*c[VR], Ycut, m2, mu2hat] - 
     30*api*MBhat*Ycut^4*X1mix[2, 1, SM*c[VR], Ycut, m2, mu2hat] + 
     5*api*X1mix[3, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*Ycut*X1mix[3, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     30*api*Ycut^2*X1mix[3, 0, SM*c[VR], Ycut, m2, mu2hat] - 
     20*api*Ycut^3*X1mix[3, 0, SM*c[VR], Ycut, m2, mu2hat] + 
     5*api*Ycut^4*X1mix[3, 0, SM*c[VR], Ycut, m2, mu2hat])/
    (5*(-1 + Ycut)^4)) + 
 c[VL]*(12*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 95*m2*MBhat + 
     55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 138*m2*MBhat^2 - 
     38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
     45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 3*m2*MBhat^5 - 2*MBhat^6)*
    Log[m2] - 12*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 
     95*m2*MBhat + 55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 
     138*m2*MBhat^2 - 38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 
     8*m2^2*MBhat^3 - 45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 
     3*m2*MBhat^5 - 2*MBhat^6)*Log[1 - Ycut] - 
   (5 - 175*m2 - 5495*m2^2 - 7875*m2^3 + 7875*m2^4 + 5495*m2^5 + 175*m2^6 - 
     5*m2^7 - 43*MBhat + 1225*m2*MBhat + 26313*m2^2*MBhat + 
     13125*m2^3*MBhat - 34125*m2^4*MBhat - 6657*m2^5*MBhat + 175*m2^6*MBhat - 
     13*m2^7*MBhat + 162*MBhat^2 - 3668*m2*MBhat^2 - 49728*m2^2*MBhat^2 + 
     11550*m2^3*MBhat^2 + 40950*m2^4*MBhat^2 + 588*m2^5*MBhat^2 + 
     168*m2^6*MBhat^2 - 22*m2^7*MBhat^2 - 348*MBhat^3 + 6076*m2*MBhat^3 + 
     45710*m2^2*MBhat^3 - 35770*m2^3*MBhat^3 - 15820*m2^4*MBhat^3 + 
     28*m2^5*MBhat^3 + 154*m2^6*MBhat^3 - 30*m2^7*MBhat^3 + 427*MBhat^4 - 
     5733*m2*MBhat^4 - 20160*m2^2*MBhat^4 + 25200*m2^3*MBhat^4 - 
     315*m2^4*MBhat^4 + 693*m2^5*MBhat^4 - 112*m2^6*MBhat^4 - 273*MBhat^5 + 
     2835*m2*MBhat^5 + 3360*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 + 
     945*m2^4*MBhat^5 - 147*m2^5*MBhat^5 + 70*MBhat^6 - 560*m2*MBhat^6 + 
     560*m2^3*MBhat^6 - 70*m2^4*MBhat^6 - 25*Ycut + 875*m2*Ycut + 
     29575*m2^2*Ycut + 49875*m2^3*Ycut - 28875*m2^4*Ycut - 25375*m2^5*Ycut - 
     875*m2^6*Ycut + 25*m2^7*Ycut + 215*MBhat*Ycut - 6125*m2*MBhat*Ycut - 
     142905*m2^2*MBhat*Ycut - 105525*m2^3*MBhat*Ycut + 
     147525*m2^4*MBhat*Ycut + 32025*m2^5*MBhat*Ycut - 875*m2^6*MBhat*Ycut + 
     65*m2^7*MBhat*Ycut - 810*MBhat^2*Ycut + 18340*m2*MBhat^2*Ycut + 
     273840*m2^2*MBhat^2*Ycut + 210*m2^3*MBhat^2*Ycut - 
     188790*m2^4*MBhat^2*Ycut - 2940*m2^5*MBhat^2*Ycut - 
     840*m2^6*MBhat^2*Ycut + 110*m2^7*MBhat^2*Ycut + 1740*MBhat^3*Ycut - 
     30380*m2*MBhat^3*Ycut - 257950*m2^2*MBhat^3*Ycut + 
     139370*m2^3*MBhat^3*Ycut + 75740*m2^4*MBhat^3*Ycut - 
     140*m2^5*MBhat^3*Ycut - 770*m2^6*MBhat^3*Ycut + 150*m2^7*MBhat^3*Ycut - 
     2135*MBhat^4*Ycut + 28665*m2*MBhat^4*Ycut + 119700*m2^2*MBhat^4*Ycut - 
     113820*m2^3*MBhat^4*Ycut + 1575*m2^4*MBhat^4*Ycut - 
     3465*m2^5*MBhat^4*Ycut + 560*m2^6*MBhat^4*Ycut + 1365*MBhat^5*Ycut - 
     14175*m2*MBhat^5*Ycut - 23100*m2^2*MBhat^5*Ycut + 
     32340*m2^3*MBhat^5*Ycut - 4725*m2^4*MBhat^5*Ycut + 
     735*m2^5*MBhat^5*Ycut - 350*MBhat^6*Ycut + 2800*m2*MBhat^6*Ycut + 
     840*m2^2*MBhat^6*Ycut - 2800*m2^3*MBhat^6*Ycut + 350*m2^4*MBhat^6*Ycut + 
     50*Ycut^2 - 1750*m2*Ycut^2 - 64400*m2^2*Ycut^2 - 126000*m2^3*Ycut^2 + 
     31500*m2^4*Ycut^2 + 45500*m2^5*Ycut^2 + 1750*m2^6*Ycut^2 - 
     50*m2^7*Ycut^2 - 430*MBhat*Ycut^2 + 12250*m2*MBhat*Ycut^2 + 
     314160*m2^2*MBhat*Ycut^2 + 310800*m2^3*MBhat*Ycut^2 - 
     237300*m2^4*MBhat*Ycut^2 - 60900*m2^5*MBhat*Ycut^2 + 
     1750*m2^6*MBhat*Ycut^2 - 130*m2^7*MBhat*Ycut^2 + 1620*MBhat^2*Ycut^2 - 
     36680*m2*MBhat^2*Ycut^2 - 610680*m2^2*MBhat^2*Ycut^2 - 
     145320*m2^3*MBhat^2*Ycut^2 + 337680*m2^4*MBhat^2*Ycut^2 + 
     5880*m2^5*MBhat^2*Ycut^2 + 1680*m2^6*MBhat^2*Ycut^2 - 
     220*m2^7*MBhat^2*Ycut^2 - 3480*MBhat^3*Ycut^2 + 
     60760*m2*MBhat^3*Ycut^2 + 589400*m2^2*MBhat^3*Ycut^2 - 
     180040*m2^3*MBhat^3*Ycut^2 - 143080*m2^4*MBhat^3*Ycut^2 + 
     280*m2^5*MBhat^3*Ycut^2 + 1540*m2^6*MBhat^3*Ycut^2 - 
     300*m2^7*MBhat^3*Ycut^2 + 4270*MBhat^4*Ycut^2 - 
     57330*m2*MBhat^4*Ycut^2 - 286650*m2^2*MBhat^4*Ycut^2 + 
     197190*m2^3*MBhat^4*Ycut^2 - 3150*m2^4*MBhat^4*Ycut^2 + 
     6930*m2^5*MBhat^4*Ycut^2 - 1120*m2^6*MBhat^4*Ycut^2 - 
     2730*MBhat^5*Ycut^2 + 28350*m2*MBhat^5*Ycut^2 + 
     61950*m2^2*MBhat^5*Ycut^2 - 61530*m2^3*MBhat^5*Ycut^2 + 
     9450*m2^4*MBhat^5*Ycut^2 - 1470*m2^5*MBhat^5*Ycut^2 + 
     700*MBhat^6*Ycut^2 - 5600*m2*MBhat^6*Ycut^2 - 3780*m2^2*MBhat^6*Ycut^2 + 
     5600*m2^3*MBhat^6*Ycut^2 - 700*m2^4*MBhat^6*Ycut^2 - 50*Ycut^3 + 
     1750*m2*Ycut^3 + 71400*m2^2*Ycut^3 + 161000*m2^3*Ycut^3 + 
     3500*m2^4*Ycut^3 - 38500*m2^5*Ycut^3 - 1750*m2^6*Ycut^3 + 
     50*m2^7*Ycut^3 + 430*MBhat*Ycut^3 - 12250*m2*MBhat*Ycut^3 - 
     351960*m2^2*MBhat*Ycut^3 - 443800*m2^3*MBhat*Ycut^3 + 
     160300*m2^4*MBhat*Ycut^3 + 56700*m2^5*MBhat*Ycut^3 - 
     1750*m2^6*MBhat*Ycut^3 + 130*m2^7*MBhat*Ycut^3 - 1620*MBhat^2*Ycut^3 + 
     36680*m2*MBhat^2*Ycut^3 + 694680*m2^2*MBhat^2*Ycut^3 + 
     338520*m2^3*MBhat^2*Ycut^3 - 284480*m2^4*MBhat^2*Ycut^3 - 
     5880*m2^5*MBhat^2*Ycut^3 - 1680*m2^6*MBhat^2*Ycut^3 + 
     220*m2^7*MBhat^2*Ycut^3 + 3620*MBhat^3*Ycut^3 - 
     61320*m2*MBhat^3*Ycut^3 - 686700*m2^2*MBhat^3*Ycut^3 + 
     48440*m2^3*MBhat^3*Ycut^3 + 131180*m2^4*MBhat^3*Ycut^3 + 
     280*m2^5*MBhat^3*Ycut^3 - 1680*m2^6*MBhat^3*Ycut^3 + 
     300*m2^7*MBhat^3*Ycut^3 - 4690*MBhat^4*Ycut^3 + 
     58590*m2*MBhat^4*Ycut^3 + 348810*m2^2*MBhat^4*Ycut^3 - 
     157430*m2^3*MBhat^4*Ycut^3 + 4410*m2^4*MBhat^4*Ycut^3 - 
     7350*m2^5*MBhat^4*Ycut^3 + 1120*m2^6*MBhat^4*Ycut^3 + 
     3150*MBhat^5*Ycut^3 - 29190*m2*MBhat^5*Ycut^3 - 
     82950*m2^2*MBhat^5*Ycut^3 + 58170*m2^3*MBhat^5*Ycut^3 - 
     9870*m2^4*MBhat^5*Ycut^3 + 1470*m2^5*MBhat^5*Ycut^3 - 
     840*MBhat^6*Ycut^3 + 5740*m2*MBhat^6*Ycut^3 + 6720*m2^2*MBhat^6*Ycut^3 - 
     5740*m2^3*MBhat^6*Ycut^3 + 700*m2^4*MBhat^6*Ycut^3 + 25*Ycut^4 - 
     875*m2*Ycut^4 - 40950*m2^2*Ycut^4 - 106750*m2^3*Ycut^4 - 
     28000*m2^4*Ycut^4 + 14000*m2^5*Ycut^4 + 875*m2^6*Ycut^4 - 
     25*m2^7*Ycut^4 - 215*MBhat*Ycut^4 + 6125*m2*MBhat*Ycut^4 + 
     204330*m2^2*MBhat*Ycut^4 + 321650*m2^3*MBhat*Ycut^4 - 
     22400*m2^4*MBhat*Ycut^4 - 25200*m2^5*MBhat*Ycut^4 + 
     875*m2^6*MBhat*Ycut^4 - 65*m2^7*MBhat*Ycut^4 + 705*MBhat^2*Ycut^4 - 
     18025*m2*MBhat^2*Ycut^4 - 410340*m2^2*MBhat^2*Ycut^4 - 
     315210*m2^3*MBhat^2*Ycut^4 + 103915*m2^4*MBhat^2*Ycut^4 + 
     1995*m2^5*MBhat^2*Ycut^4 + 1050*m2^6*MBhat^2*Ycut^4 - 
     110*m2^7*MBhat^2*Ycut^4 - 2195*MBhat^3*Ycut^4 + 
     32655*m2*MBhat^3*Ycut^4 + 412860*m2^2*MBhat^3*Ycut^4 + 
     78190*m2^3*MBhat^3*Ycut^4 - 58555*m2^4*MBhat^3*Ycut^4 - 
     245*m2^5*MBhat^3*Ycut^4 + 980*m2^6*MBhat^3*Ycut^4 - 
     150*m2^7*MBhat^3*Ycut^4 + 4130*MBhat^4*Ycut^4 - 
     34860*m2*MBhat^4*Ycut^4 - 216300*m2^2*MBhat^4*Ycut^4 + 
     47740*m2^3*MBhat^4*Ycut^4 - 3885*m2^4*MBhat^4*Ycut^4 + 
     4305*m2^5*MBhat^4*Ycut^4 - 560*m2^6*MBhat^4*Ycut^4 - 
     3570*MBhat^5*Ycut^4 + 18480*m2*MBhat^5*Ycut^4 + 
     56280*m2^2*MBhat^5*Ycut^4 - 27720*m2^3*MBhat^5*Ycut^4 + 
     5775*m2^4*MBhat^5*Ycut^4 - 735*m2^5*MBhat^5*Ycut^4 + 
     1120*MBhat^6*Ycut^4 - 3500*m2*MBhat^6*Ycut^4 - 
     5880*m2^2*MBhat^6*Ycut^4 + 3220*m2^3*MBhat^6*Ycut^4 - 
     350*m2^4*MBhat^6*Ycut^4 - 5*Ycut^5 + 175*m2*Ycut^5 + 10290*m2^2*Ycut^5 + 
     31850*m2^3*Ycut^5 + 16100*m2^4*Ycut^5 - 700*m2^5*Ycut^5 - 
     175*m2^6*Ycut^5 + 5*m2^7*Ycut^5 + 85*MBhat*Ycut^5 - 
     1309*m2*MBhat*Ycut^5 - 52416*m2^2*MBhat*Ycut^5 - 
     103390*m2^3*MBhat*Ycut^5 - 19670*m2^4*MBhat*Ycut^5 + 
     4368*m2^5*MBhat*Ycut^5 - 301*m2^6*MBhat*Ycut^5 + 13*m2^7*MBhat*Ycut^5 + 
     279*MBhat^2*Ycut^5 + 2219*m2*MBhat^2*Ycut^5 + 
     108486*m2^2*MBhat^2*Ycut^5 + 121590*m2^3*MBhat^2*Ycut^5 - 
     6335*m2^4*MBhat^2*Ycut^5 + 399*m2^5*MBhat^2*Ycut^5 - 
     336*m2^6*MBhat^2*Ycut^5 + 22*m2^7*MBhat^2*Ycut^5 + 523*MBhat^3*Ycut^5 - 
     9051*m2*MBhat^3*Ycut^5 - 104160*m2^2*MBhat^3*Ycut^5 - 
     64190*m2^3*MBhat^3*Ycut^5 + 12635*m2^4*MBhat^3*Ycut^5 - 
     455*m2^5*MBhat^3*Ycut^5 - 280*m2^6*MBhat^3*Ycut^5 + 
     30*m2^7*MBhat^3*Ycut^5 - 4018*MBhat^4*Ycut^5 + 17766*m2*MBhat^4*Ycut^5 + 
     49560*m2^2*MBhat^4*Ycut^5 + 7840*m2^3*MBhat^4*Ycut^5 + 
     945*m2^4*MBhat^4*Ycut^5 - 1239*m2^5*MBhat^4*Ycut^5 + 
     112*m2^6*MBhat^4*Ycut^5 + 4956*MBhat^5*Ycut^5 - 
     11760*m2*MBhat^5*Ycut^5 - 14280*m2^2*MBhat^5*Ycut^5 + 
     5460*m2^3*MBhat^5*Ycut^5 - 1785*m2^4*MBhat^5*Ycut^5 + 
     147*m2^5*MBhat^5*Ycut^5 - 1820*MBhat^6*Ycut^5 + 1960*m2*MBhat^6*Ycut^5 + 
     2520*m2^2*MBhat^6*Ycut^5 - 980*m2^3*MBhat^6*Ycut^5 + 
     70*m2^4*MBhat^6*Ycut^5 - 7*Ycut^6 + 7*m2*Ycut^6 - 280*m2^2*Ycut^6 - 
     1960*m2^3*Ycut^6 - 1505*m2^4*Ycut^6 - 483*m2^5*Ycut^6 + 28*m2^6*Ycut^6 - 
     203*MBhat*Ycut^6 + 413*m2*MBhat*Ycut^6 + 2310*m2^2*MBhat*Ycut^6 + 
     4900*m2^3*MBhat*Ycut^6 + 5495*m2^4*MBhat*Ycut^6 - 
     357*m2^5*MBhat*Ycut^6 + 42*m2^6*MBhat*Ycut^6 - 609*MBhat^2*Ycut^6 + 
     2513*m2*MBhat^2*Ycut^6 - 8190*m2^2*MBhat^2*Ycut^6 - 
     6720*m2^3*MBhat^2*Ycut^6 - 3535*m2^4*MBhat^2*Ycut^6 - 
     21*m2^5*MBhat^2*Ycut^6 + 42*m2^6*MBhat^2*Ycut^6 + 1015*MBhat^3*Ycut^6 + 
     357*m2*MBhat^3*Ycut^6 - 1400*m2^2*MBhat^3*Ycut^6 + 
     14420*m2^3*MBhat^3*Ycut^6 - 2695*m2^4*MBhat^3*Ycut^6 + 
     315*m2^5*MBhat^3*Ycut^6 + 28*m2^6*MBhat^3*Ycut^6 + 2744*MBhat^4*Ycut^6 - 
     11340*m2*MBhat^4*Ycut^6 + 11550*m2^2*MBhat^4*Ycut^6 - 
     8890*m2^3*MBhat^4*Ycut^6 + 630*m2^4*MBhat^4*Ycut^6 + 
     126*m2^5*MBhat^4*Ycut^6 - 5040*MBhat^5*Ycut^6 + 9450*m2*MBhat^5*Ycut^6 - 
     3570*m2^2*MBhat^5*Ycut^6 + 210*m2^3*MBhat^5*Ycut^6 + 
     210*m2^4*MBhat^5*Ycut^6 + 2100*MBhat^6*Ycut^6 - 1400*m2*MBhat^6*Ycut^6 - 
     420*m2^2*MBhat^6*Ycut^6 + 140*m2^3*MBhat^6*Ycut^6 + 37*Ycut^7 - 
     35*m2*Ycut^7 - 280*m2^2*Ycut^7 + 280*m2^3*Ycut^7 - 665*m2^4*Ycut^7 + 
     63*m2^5*Ycut^7 + 379*MBhat*Ycut^7 - 805*m2*MBhat*Ycut^7 + 
     420*m2^2*MBhat*Ycut^7 + 1540*m2^3*MBhat*Ycut^7 + 245*m2^4*MBhat*Ycut^7 + 
     21*m2^5*MBhat*Ycut^7 + 111*MBhat^2*Ycut^7 - 1855*m2*MBhat^2*Ycut^7 + 
     3780*m2^2*MBhat^2*Ycut^7 - 5040*m2^3*MBhat^2*Ycut^7 + 
     665*m2^4*MBhat^2*Ycut^7 - 21*m2^5*MBhat^2*Ycut^7 - 1577*MBhat^3*Ycut^7 + 
     2415*m2*MBhat^3*Ycut^7 + 700*m2^2*MBhat^3*Ycut^7 - 
     420*m2^3*MBhat^3*Ycut^7 + 665*m2^4*MBhat^3*Ycut^7 - 
     63*m2^5*MBhat^3*Ycut^7 - 280*MBhat^4*Ycut^7 + 4830*m2*MBhat^4*Ycut^7 - 
     7350*m2^2*MBhat^4*Ycut^7 + 2450*m2^3*MBhat^4*Ycut^7 - 
     210*m2^4*MBhat^4*Ycut^7 + 2730*MBhat^5*Ycut^7 - 5250*m2*MBhat^5*Ycut^7 + 
     2730*m2^2*MBhat^5*Ycut^7 - 210*m2^3*MBhat^5*Ycut^7 - 
     1400*MBhat^6*Ycut^7 + 700*m2*MBhat^6*Ycut^7 - 80*Ycut^8 + 70*m2*Ycut^8 + 
     245*m2^2*Ycut^8 - 455*m2^3*Ycut^8 + 70*m2^4*Ycut^8 - 320*MBhat*Ycut^8 + 
     770*m2*MBhat*Ycut^8 - 735*m2^2*MBhat*Ycut^8 + 805*m2^3*MBhat*Ycut^8 - 
     70*m2^4*MBhat*Ycut^8 + 495*MBhat^2*Ycut^8 + 245*m2*MBhat^2*Ycut^8 - 
     1680*m2^2*MBhat^2*Ycut^8 + 420*m2^3*MBhat^2*Ycut^8 - 
     70*m2^4*MBhat^2*Ycut^8 + 745*MBhat^3*Ycut^8 - 1995*m2*MBhat^3*Ycut^8 + 
     1750*m2^2*MBhat^3*Ycut^8 - 70*m2^4*MBhat^3*Ycut^8 - 805*MBhat^4*Ycut^8 - 
     315*m2*MBhat^4*Ycut^8 + 840*m2^2*MBhat^4*Ycut^8 - 
     280*m2^3*MBhat^4*Ycut^8 - 525*MBhat^5*Ycut^8 + 1365*m2*MBhat^5*Ycut^8 - 
     420*m2^2*MBhat^5*Ycut^8 + 490*MBhat^6*Ycut^8 - 140*m2*MBhat^6*Ycut^8 + 
     90*Ycut^9 - 70*m2*Ycut^9 - 105*m2^2*Ycut^9 + 35*m2^3*Ycut^9 + 
     80*MBhat*Ycut^9 - 350*m2*MBhat*Ycut^9 + 525*m2^2*MBhat*Ycut^9 - 
     105*m2^3*MBhat*Ycut^9 - 465*MBhat^2*Ycut^9 + 385*m2*MBhat^2*Ycut^9 - 
     210*m2^2*MBhat^2*Ycut^9 + 85*MBhat^3*Ycut^9 + 455*m2*MBhat^3*Ycut^9 - 
     210*m2^2*MBhat^3*Ycut^9 + 385*MBhat^4*Ycut^9 - 315*m2*MBhat^4*Ycut^9 - 
     105*MBhat^5*Ycut^9 - 105*m2*MBhat^5*Ycut^9 - 70*MBhat^6*Ycut^9 - 
     55*Ycut^10 + 35*m2*Ycut^10 + 53*MBhat*Ycut^10 + 49*m2*MBhat*Ycut^10 - 
     42*m2^2*MBhat*Ycut^10 + 129*MBhat^2*Ycut^10 - 161*m2*MBhat^2*Ycut^10 + 
     42*m2^2*MBhat^2*Ycut^10 - 155*MBhat^3*Ycut^10 + 35*m2*MBhat^3*Ycut^10 - 
     14*MBhat^4*Ycut^10 + 42*m2*MBhat^4*Ycut^10 + 42*MBhat^5*Ycut^10 + 
     17*Ycut^11 - 7*m2*Ycut^11 - 37*MBhat*Ycut^11 + 7*m2*MBhat*Ycut^11 + 
     9*MBhat^2*Ycut^11 + 7*m2*MBhat^2*Ycut^11 + 25*MBhat^3*Ycut^11 - 
     7*m2*MBhat^3*Ycut^11 - 14*MBhat^4*Ycut^11 - 2*Ycut^12 + 
     6*MBhat*Ycut^12 - 6*MBhat^2*Ycut^12 + 2*MBhat^3*Ycut^12 + 
     35*api*MBhat^6*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     175*api*MBhat^6*Ycut*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     350*api*MBhat^6*Ycut^2*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     350*api*MBhat^6*Ycut^3*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     175*api*MBhat^6*Ycut^4*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     35*api*MBhat^6*Ycut^5*X1mix[0, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     210*api*MBhat^5*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     1050*api*MBhat^5*Ycut*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     2100*api*MBhat^5*Ycut^2*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     2100*api*MBhat^5*Ycut^3*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     1050*api*MBhat^5*Ycut^4*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     210*api*MBhat^5*Ycut^5*X1mix[0, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     420*api*MBhat^4*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     2100*api*MBhat^4*Ycut*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     4200*api*MBhat^4*Ycut^2*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     4200*api*MBhat^4*Ycut^3*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     2100*api*MBhat^4*Ycut^4*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     420*api*MBhat^4*Ycut^5*X1mix[0, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     280*api*MBhat^3*X1mix[0, 3, SM*c[VL], Ycut, m2, mu2hat] + 
     1400*api*MBhat^3*Ycut*X1mix[0, 3, SM*c[VL], Ycut, m2, mu2hat] - 
     2800*api*MBhat^3*Ycut^2*X1mix[0, 3, SM*c[VL], Ycut, m2, mu2hat] + 
     2800*api*MBhat^3*Ycut^3*X1mix[0, 3, SM*c[VL], Ycut, m2, mu2hat] - 
     1400*api*MBhat^3*Ycut^4*X1mix[0, 3, SM*c[VL], Ycut, m2, mu2hat] + 
     280*api*MBhat^3*Ycut^5*X1mix[0, 3, SM*c[VL], Ycut, m2, mu2hat] + 
     105*api*MBhat^4*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     525*api*MBhat^4*Ycut*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     1050*api*MBhat^4*Ycut^2*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     1050*api*MBhat^4*Ycut^3*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     525*api*MBhat^4*Ycut^4*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     105*api*MBhat^4*Ycut^5*X1mix[1, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     420*api*MBhat^3*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     2100*api*MBhat^3*Ycut*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     4200*api*MBhat^3*Ycut^2*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     4200*api*MBhat^3*Ycut^3*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     2100*api*MBhat^3*Ycut^4*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     420*api*MBhat^3*Ycut^5*X1mix[1, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     420*api*MBhat^2*X1mix[1, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     2100*api*MBhat^2*Ycut*X1mix[1, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     4200*api*MBhat^2*Ycut^2*X1mix[1, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     4200*api*MBhat^2*Ycut^3*X1mix[1, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     2100*api*MBhat^2*Ycut^4*X1mix[1, 2, SM*c[VL], Ycut, m2, mu2hat] - 
     420*api*MBhat^2*Ycut^5*X1mix[1, 2, SM*c[VL], Ycut, m2, mu2hat] + 
     105*api*MBhat^2*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     525*api*MBhat^2*Ycut*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     1050*api*MBhat^2*Ycut^2*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     1050*api*MBhat^2*Ycut^3*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     525*api*MBhat^2*Ycut^4*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     105*api*MBhat^2*Ycut^5*X1mix[2, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     210*api*MBhat*X1mix[2, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     1050*api*MBhat*Ycut*X1mix[2, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     2100*api*MBhat*Ycut^2*X1mix[2, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     2100*api*MBhat*Ycut^3*X1mix[2, 1, SM*c[VL], Ycut, m2, mu2hat] - 
     1050*api*MBhat*Ycut^4*X1mix[2, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     210*api*MBhat*Ycut^5*X1mix[2, 1, SM*c[VL], Ycut, m2, mu2hat] + 
     35*api*X1mix[3, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     175*api*Ycut*X1mix[3, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     350*api*Ycut^2*X1mix[3, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     350*api*Ycut^3*X1mix[3, 0, SM*c[VL], Ycut, m2, mu2hat] + 
     175*api*Ycut^4*X1mix[3, 0, SM*c[VL], Ycut, m2, mu2hat] - 
     35*api*Ycut^5*X1mix[3, 0, SM*c[VL], Ycut, m2, mu2hat])/
    (35*(-1 + Ycut)^5) + 
   c[VR]*(-24*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 6*MBhat - 
       45*m2*MBhat - 60*m2^2*MBhat - 15*m2^3*MBhat + 15*MBhat^2 + 
       81*m2*MBhat^2 + 66*m2^2*MBhat^2 + 6*m2^3*MBhat^2 - 20*MBhat^3 - 
       74*m2*MBhat^3 - 32*m2^2*MBhat^3 + 15*MBhat^4 + 36*m2*MBhat^4 + 
       6*m2^2*MBhat^4 - 6*MBhat^5 - 9*m2*MBhat^5 + MBhat^6 + m2*MBhat^6)*
      Log[m2] + 24*m2^(3/2)*(1 + 10*m2 + 20*m2^2 + 10*m2^3 + m2^4 - 6*MBhat - 
       45*m2*MBhat - 60*m2^2*MBhat - 15*m2^3*MBhat + 15*MBhat^2 + 
       81*m2*MBhat^2 + 66*m2^2*MBhat^2 + 6*m2^3*MBhat^2 - 20*MBhat^3 - 
       74*m2*MBhat^3 - 32*m2^2*MBhat^3 + 15*MBhat^4 + 36*m2*MBhat^4 + 
       6*m2^2*MBhat^4 - 6*MBhat^5 - 9*m2*MBhat^5 + MBhat^6 + m2*MBhat^6)*
      Log[1 - Ycut] + (-4*Sqrt[m2] - 404*m2^(3/2) - 1700*m2^(5/2) + 
       1700*m2^(9/2) + 404*m2^(11/2) + 4*m2^(13/2) + 30*Sqrt[m2]*MBhat + 
       2244*m2^(3/2)*MBhat + 5850*m2^(5/2)*MBhat - 3600*m2^(7/2)*MBhat - 
       4350*m2^(9/2)*MBhat - 180*m2^(11/2)*MBhat + 6*m2^(13/2)*MBhat - 
       96*Sqrt[m2]*MBhat^2 - 5106*m2^(3/2)*MBhat^2 - 6750*m2^(5/2)*MBhat^2 + 
       9000*m2^(7/2)*MBhat^2 + 3000*m2^(9/2)*MBhat^2 - 54*m2^(11/2)*MBhat^2 + 
       6*m2^(13/2)*MBhat^2 + 168*Sqrt[m2]*MBhat^3 + 6056*m2^(3/2)*MBhat^3 + 
       2060*m2^(5/2)*MBhat^3 - 7840*m2^(7/2)*MBhat^3 - 440*m2^(9/2)*MBhat^3 - 
       8*m2^(11/2)*MBhat^3 + 4*m2^(13/2)*MBhat^3 - 168*Sqrt[m2]*MBhat^4 - 
       3930*m2^(3/2)*MBhat^4 + 1440*m2^(5/2)*MBhat^4 + 
       2760*m2^(7/2)*MBhat^4 - 120*m2^(9/2)*MBhat^4 + 18*m2^(11/2)*MBhat^4 + 
       90*Sqrt[m2]*MBhat^5 + 1320*m2^(3/2)*MBhat^5 - 1080*m2^(5/2)*MBhat^5 - 
       360*m2^(7/2)*MBhat^5 + 30*m2^(9/2)*MBhat^5 - 20*Sqrt[m2]*MBhat^6 - 
       180*m2^(3/2)*MBhat^6 + 180*m2^(5/2)*MBhat^6 + 20*m2^(7/2)*MBhat^6 + 
       16*Sqrt[m2]*Ycut + 1736*m2^(3/2)*Ycut + 8000*m2^(5/2)*Ycut + 
       2400*m2^(7/2)*Ycut - 5600*m2^(9/2)*Ycut - 1496*m2^(11/2)*Ycut - 
       16*m2^(13/2)*Ycut - 120*Sqrt[m2]*MBhat*Ycut - 9696*m2^(3/2)*MBhat*
        Ycut - 28800*m2^(5/2)*MBhat*Ycut + 7200*m2^(7/2)*MBhat*Ycut + 
       15600*m2^(9/2)*MBhat*Ycut + 720*m2^(11/2)*MBhat*Ycut - 
       24*m2^(13/2)*MBhat*Ycut + 384*Sqrt[m2]*MBhat^2*Ycut + 
       22224*m2^(3/2)*MBhat^2*Ycut + 36720*m2^(5/2)*MBhat^2*Ycut - 
       28080*m2^(7/2)*MBhat^2*Ycut - 11280*m2^(9/2)*MBhat^2*Ycut + 
       216*m2^(11/2)*MBhat^2*Ycut - 24*m2^(13/2)*MBhat^2*Ycut - 
       672*Sqrt[m2]*MBhat^3*Ycut - 26624*m2^(3/2)*MBhat^3*Ycut - 
       17120*m2^(5/2)*MBhat^3*Ycut + 27520*m2^(7/2)*MBhat^3*Ycut + 
       1760*m2^(9/2)*MBhat^3*Ycut + 32*m2^(11/2)*MBhat^3*Ycut - 
       16*m2^(13/2)*MBhat^3*Ycut + 672*Sqrt[m2]*MBhat^4*Ycut + 
       17520*m2^(3/2)*MBhat^4*Ycut - 1440*m2^(5/2)*MBhat^4*Ycut - 
       10320*m2^(7/2)*MBhat^4*Ycut + 480*m2^(9/2)*MBhat^4*Ycut - 
       72*m2^(11/2)*MBhat^4*Ycut - 360*Sqrt[m2]*MBhat^5*Ycut - 
       6000*m2^(3/2)*MBhat^5*Ycut + 3240*m2^(5/2)*MBhat^5*Ycut + 
       1440*m2^(7/2)*MBhat^5*Ycut - 120*m2^(9/2)*MBhat^5*Ycut + 
       80*Sqrt[m2]*MBhat^6*Ycut + 840*m2^(3/2)*MBhat^6*Ycut - 
       600*m2^(5/2)*MBhat^6*Ycut - 80*m2^(7/2)*MBhat^6*Ycut - 
       24*Sqrt[m2]*Ycut^2 - 2844*m2^(3/2)*Ycut^2 - 14400*m2^(5/2)*Ycut^2 - 
       8400*m2^(7/2)*Ycut^2 + 6000*m2^(9/2)*Ycut^2 + 2004*m2^(11/2)*Ycut^2 + 
       24*m2^(13/2)*Ycut^2 + 180*Sqrt[m2]*MBhat*Ycut^2 + 
       15984*m2^(3/2)*MBhat*Ycut^2 + 54000*m2^(5/2)*MBhat*Ycut^2 + 
       3600*m2^(7/2)*MBhat*Ycut^2 - 19800*m2^(9/2)*MBhat*Ycut^2 - 
       1080*m2^(11/2)*MBhat*Ycut^2 + 36*m2^(13/2)*MBhat*Ycut^2 - 
       576*Sqrt[m2]*MBhat^2*Ycut^2 - 36936*m2^(3/2)*MBhat^2*Ycut^2 - 
       74520*m2^(5/2)*MBhat^2*Ycut^2 + 26280*m2^(7/2)*MBhat^2*Ycut^2 + 
       15480*m2^(9/2)*MBhat^2*Ycut^2 - 324*m2^(11/2)*MBhat^2*Ycut^2 + 
       36*m2^(13/2)*MBhat^2*Ycut^2 + 1008*Sqrt[m2]*MBhat^3*Ycut^2 + 
       44736*m2^(3/2)*MBhat^3*Ycut^2 + 43440*m2^(5/2)*MBhat^3*Ycut^2 - 
       33600*m2^(7/2)*MBhat^3*Ycut^2 - 2640*m2^(9/2)*MBhat^3*Ycut^2 - 
       48*m2^(11/2)*MBhat^3*Ycut^2 + 24*m2^(13/2)*MBhat^3*Ycut^2 - 
       1008*Sqrt[m2]*MBhat^4*Ycut^2 - 29880*m2^(3/2)*MBhat^4*Ycut^2 - 
       6480*m2^(5/2)*MBhat^4*Ycut^2 + 14040*m2^(7/2)*MBhat^4*Ycut^2 - 
       720*m2^(9/2)*MBhat^4*Ycut^2 + 108*m2^(11/2)*MBhat^4*Ycut^2 + 
       540*Sqrt[m2]*MBhat^5*Ycut^2 + 10440*m2^(3/2)*MBhat^5*Ycut^2 - 
       2700*m2^(5/2)*MBhat^5*Ycut^2 - 2160*m2^(7/2)*MBhat^5*Ycut^2 + 
       180*m2^(9/2)*MBhat^5*Ycut^2 - 120*Sqrt[m2]*MBhat^6*Ycut^2 - 
       1500*m2^(3/2)*MBhat^6*Ycut^2 + 660*m2^(5/2)*MBhat^6*Ycut^2 + 
       120*m2^(7/2)*MBhat^6*Ycut^2 + 16*Sqrt[m2]*Ycut^3 + 
       2136*m2^(3/2)*Ycut^3 + 12000*m2^(5/2)*Ycut^3 + 10400*m2^(7/2)*Ycut^3 - 
       1600*m2^(9/2)*Ycut^3 - 1096*m2^(11/2)*Ycut^3 - 16*m2^(13/2)*Ycut^3 - 
       120*Sqrt[m2]*MBhat*Ycut^3 - 12096*m2^(3/2)*MBhat*Ycut^3 - 
       46800*m2^(5/2)*MBhat*Ycut^3 - 16800*m2^(7/2)*MBhat*Ycut^3 + 
       9600*m2^(9/2)*MBhat*Ycut^3 + 720*m2^(11/2)*MBhat*Ycut^3 - 
       24*m2^(13/2)*MBhat*Ycut^3 + 384*Sqrt[m2]*MBhat^2*Ycut^3 + 
       28224*m2^(3/2)*MBhat^2*Ycut^3 + 69120*m2^(5/2)*MBhat^2*Ycut^3 - 
       1680*m2^(7/2)*MBhat^2*Ycut^3 - 8880*m2^(9/2)*MBhat^2*Ycut^3 + 
       216*m2^(11/2)*MBhat^2*Ycut^3 - 24*m2^(13/2)*MBhat^2*Ycut^3 - 
       692*Sqrt[m2]*MBhat^3*Ycut^3 - 34524*m2^(3/2)*MBhat^3*Ycut^3 - 
       46920*m2^(5/2)*MBhat^3*Ycut^3 + 14920*m2^(7/2)*MBhat^3*Ycut^3 + 
       1660*m2^(9/2)*MBhat^3*Ycut^3 + 52*m2^(11/2)*MBhat^3*Ycut^3 - 
       16*m2^(13/2)*MBhat^3*Ycut^3 + 732*Sqrt[m2]*MBhat^4*Ycut^3 + 
       23280*m2^(3/2)*MBhat^4*Ycut^3 + 13320*m2^(5/2)*MBhat^4*Ycut^3 - 
       8160*m2^(7/2)*MBhat^4*Ycut^3 + 540*m2^(9/2)*MBhat^4*Ycut^3 - 
       72*m2^(11/2)*MBhat^4*Ycut^3 - 420*Sqrt[m2]*MBhat^5*Ycut^3 - 
       8220*m2^(3/2)*MBhat^5*Ycut^3 - 540*m2^(5/2)*MBhat^5*Ycut^3 + 
       1500*m2^(7/2)*MBhat^5*Ycut^3 - 120*m2^(9/2)*MBhat^5*Ycut^3 + 
       100*Sqrt[m2]*MBhat^6*Ycut^3 + 1200*m2^(3/2)*MBhat^6*Ycut^3 - 
       180*m2^(5/2)*MBhat^6*Ycut^3 - 80*m2^(7/2)*MBhat^6*Ycut^3 - 
       4*Sqrt[m2]*Ycut^4 - 654*m2^(3/2)*Ycut^4 - 4200*m2^(5/2)*Ycut^4 - 
       5000*m2^(7/2)*Ycut^4 - 800*m2^(9/2)*Ycut^4 + 154*m2^(11/2)*Ycut^4 + 
       4*m2^(13/2)*Ycut^4 + 30*Sqrt[m2]*MBhat*Ycut^4 + 
       3744*m2^(3/2)*MBhat*Ycut^4 + 17100*m2^(5/2)*MBhat*Ycut^4 + 
       11400*m2^(7/2)*MBhat*Ycut^4 - 600*m2^(9/2)*MBhat*Ycut^4 - 
       180*m2^(11/2)*MBhat*Ycut^4 + 6*m2^(13/2)*MBhat*Ycut^4 - 
       66*Sqrt[m2]*MBhat^2*Ycut^4 - 9006*m2^(3/2)*MBhat^2*Ycut^4 - 
       26700*m2^(5/2)*MBhat^2*Ycut^4 - 7800*m2^(7/2)*MBhat^2*Ycut^4 + 
       1650*m2^(9/2)*MBhat^2*Ycut^4 - 84*m2^(11/2)*MBhat^2*Ycut^4 + 
       6*m2^(13/2)*MBhat^2*Ycut^4 + 158*Sqrt[m2]*MBhat^3*Ycut^4 + 
       11076*m2^(3/2)*MBhat^3*Ycut^4 + 20580*m2^(5/2)*MBhat^3*Ycut^4 + 
       80*m2^(7/2)*MBhat^3*Ycut^4 - 370*m2^(9/2)*MBhat^3*Ycut^4 - 
       28*m2^(11/2)*MBhat^3*Ycut^4 + 4*m2^(13/2)*MBhat^3*Ycut^4 - 
       318*Sqrt[m2]*MBhat^4*Ycut^4 - 7140*m2^(3/2)*MBhat^4*Ycut^4 - 
       8280*m2^(5/2)*MBhat^4*Ycut^4 + 1680*m2^(7/2)*MBhat^4*Ycut^4 - 
       210*m2^(9/2)*MBhat^4*Ycut^4 + 18*m2^(11/2)*MBhat^4*Ycut^4 + 
       300*Sqrt[m2]*MBhat^5*Ycut^4 + 2280*m2^(3/2)*MBhat^5*Ycut^4 + 
       1620*m2^(5/2)*MBhat^5*Ycut^4 - 480*m2^(7/2)*MBhat^5*Ycut^4 + 
       30*m2^(9/2)*MBhat^5*Ycut^4 - 100*Sqrt[m2]*MBhat^6*Ycut^4 - 
       300*m2^(3/2)*MBhat^6*Ycut^4 - 120*m2^(5/2)*MBhat^6*Ycut^4 + 
       20*m2^(7/2)*MBhat^6*Ycut^4 + 24*m2^(3/2)*Ycut^5 + 
       240*m2^(5/2)*Ycut^5 + 480*m2^(7/2)*Ycut^5 + 240*m2^(9/2)*Ycut^5 + 
       24*m2^(11/2)*Ycut^5 - 18*Sqrt[m2]*MBhat*Ycut^5 - 
       54*m2^(3/2)*MBhat*Ycut^5 - 1260*m2^(5/2)*MBhat*Ycut^5 - 
       1260*m2^(7/2)*MBhat*Ycut^5 - 450*m2^(9/2)*MBhat*Ycut^5 + 
       18*m2^(11/2)*MBhat*Ycut^5 - 66*Sqrt[m2]*MBhat^2*Ycut^5 + 
       636*m2^(3/2)*MBhat^2*Ycut^5 + 1500*m2^(5/2)*MBhat^2*Ycut^5 + 
       1920*m2^(7/2)*MBhat^2*Ycut^5 + 30*m2^(9/2)*MBhat^2*Ycut^5 + 
       12*m2^(11/2)*MBhat^2*Ycut^5 + 186*Sqrt[m2]*MBhat^3*Ycut^5 - 
       1086*m2^(3/2)*MBhat^3*Ycut^5 - 1080*m2^(5/2)*MBhat^3*Ycut^5 - 
       1080*m2^(7/2)*MBhat^3*Ycut^5 + 30*m2^(9/2)*MBhat^3*Ycut^5 + 
       6*m2^(11/2)*MBhat^3*Ycut^5 + 18*Sqrt[m2]*MBhat^4*Ycut^5 + 
       240*m2^(3/2)*MBhat^4*Ycut^5 + 1080*m2^(5/2)*MBhat^4*Ycut^5 + 
       30*m2^(9/2)*MBhat^4*Ycut^5 - 240*Sqrt[m2]*MBhat^5*Ycut^5 + 
       360*m2^(3/2)*MBhat^5*Ycut^5 - 540*m2^(5/2)*MBhat^5*Ycut^5 + 
       60*m2^(7/2)*MBhat^5*Ycut^5 + 120*Sqrt[m2]*MBhat^6*Ycut^5 - 
       120*m2^(3/2)*MBhat^6*Ycut^5 + 60*m2^(5/2)*MBhat^6*Ycut^5 + 
       4*Sqrt[m2]*Ycut^6 - 16*m2^(3/2)*Ycut^6 + 80*m2^(5/2)*Ycut^6 + 
       40*m2^(7/2)*Ycut^6 + 60*m2^(9/2)*Ycut^6 + 60*Sqrt[m2]*MBhat*Ycut^6 - 
       264*m2^(3/2)*MBhat*Ycut^6 + 180*m2^(5/2)*MBhat*Ycut^6 - 
       480*m2^(7/2)*MBhat*Ycut^6 - 24*Sqrt[m2]*MBhat^2*Ycut^6 + 
       156*m2^(3/2)*MBhat^2*Ycut^6 + 180*m2^(5/2)*MBhat^2*Ycut^6 + 
       360*m2^(7/2)*MBhat^2*Ycut^6 - 248*Sqrt[m2]*MBhat^3*Ycut^6 + 
       544*m2^(3/2)*MBhat^3*Ycut^6 - 800*m2^(5/2)*MBhat^3*Ycut^6 + 
       228*Sqrt[m2]*MBhat^4*Ycut^6 - 360*m2^(3/2)*MBhat^4*Ycut^6 + 
       360*m2^(5/2)*MBhat^4*Ycut^6 + 60*Sqrt[m2]*MBhat^5*Ycut^6 - 
       120*m2^(3/2)*MBhat^5*Ycut^6 - 80*Sqrt[m2]*MBhat^6*Ycut^6 + 
       60*m2^(3/2)*MBhat^6*Ycut^6 - 16*Sqrt[m2]*Ycut^7 + 64*m2^(3/2)*Ycut^7 - 
       80*m2^(5/2)*Ycut^7 + 80*m2^(7/2)*Ycut^7 - 60*Sqrt[m2]*MBhat*Ycut^7 + 
       156*m2^(3/2)*MBhat*Ycut^7 - 180*m2^(5/2)*MBhat*Ycut^7 - 
       60*m2^(7/2)*MBhat*Ycut^7 + 156*Sqrt[m2]*MBhat^2*Ycut^7 - 
       384*m2^(3/2)*MBhat^2*Ycut^7 + 420*m2^(5/2)*MBhat^2*Ycut^7 + 
       32*Sqrt[m2]*MBhat^3*Ycut^7 - 16*m2^(3/2)*MBhat^3*Ycut^7 - 
       160*m2^(5/2)*MBhat^3*Ycut^7 - 192*Sqrt[m2]*MBhat^4*Ycut^7 + 
       240*m2^(3/2)*MBhat^4*Ycut^7 + 60*Sqrt[m2]*MBhat^5*Ycut^7 - 
       60*m2^(3/2)*MBhat^5*Ycut^7 + 20*Sqrt[m2]*MBhat^6*Ycut^7 + 
       24*Sqrt[m2]*Ycut^8 - 66*m2^(3/2)*Ycut^8 + 60*m2^(5/2)*Ycut^8 + 
       36*m2^(3/2)*MBhat*Ycut^8 - 90*m2^(5/2)*MBhat*Ycut^8 - 
       114*Sqrt[m2]*MBhat^2*Ycut^8 + 156*m2^(3/2)*MBhat^2*Ycut^8 + 
       30*m2^(5/2)*MBhat^2*Ycut^8 + 102*Sqrt[m2]*MBhat^3*Ycut^8 - 
       156*m2^(3/2)*MBhat^3*Ycut^8 + 18*Sqrt[m2]*MBhat^4*Ycut^8 + 
       30*m2^(3/2)*MBhat^4*Ycut^8 - 30*Sqrt[m2]*MBhat^5*Ycut^8 - 
       16*Sqrt[m2]*Ycut^9 + 24*m2^(3/2)*Ycut^9 + 30*Sqrt[m2]*MBhat*Ycut^9 - 
       54*m2^(3/2)*MBhat*Ycut^9 + 6*Sqrt[m2]*MBhat^2*Ycut^9 + 
       36*m2^(3/2)*MBhat^2*Ycut^9 - 38*Sqrt[m2]*MBhat^3*Ycut^9 - 
       6*m2^(3/2)*MBhat^3*Ycut^9 + 18*Sqrt[m2]*MBhat^4*Ycut^9 + 
       4*Sqrt[m2]*Ycut^10 - 12*Sqrt[m2]*MBhat*Ycut^10 + 
       12*Sqrt[m2]*MBhat^2*Ycut^10 - 4*Sqrt[m2]*MBhat^3*Ycut^10 + 
       5*api*MBhat^6*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*MBhat^6*Ycut*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       30*api*MBhat^6*Ycut^2*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*MBhat^6*Ycut^3*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       5*api*MBhat^6*Ycut^4*X1mix[0, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       30*api*MBhat^5*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       120*api*MBhat^5*Ycut*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       180*api*MBhat^5*Ycut^2*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       120*api*MBhat^5*Ycut^3*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       30*api*MBhat^5*Ycut^4*X1mix[0, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat^4*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       240*api*MBhat^4*Ycut*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       360*api*MBhat^4*Ycut^2*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       240*api*MBhat^4*Ycut^3*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat^4*Ycut^4*X1mix[0, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       40*api*MBhat^3*X1mix[0, 3, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       160*api*MBhat^3*Ycut*X1mix[0, 3, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       240*api*MBhat^3*Ycut^2*X1mix[0, 3, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       160*api*MBhat^3*Ycut^3*X1mix[0, 3, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       40*api*MBhat^3*Ycut^4*X1mix[0, 3, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       15*api*MBhat^4*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^4*Ycut*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       90*api*MBhat^4*Ycut^2*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^4*Ycut^3*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       15*api*MBhat^4*Ycut^4*X1mix[1, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^3*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       240*api*MBhat^3*Ycut*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       360*api*MBhat^3*Ycut^2*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       240*api*MBhat^3*Ycut^3*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^3*Ycut^4*X1mix[1, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat^2*X1mix[1, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       240*api*MBhat^2*Ycut*X1mix[1, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       360*api*MBhat^2*Ycut^2*X1mix[1, 2, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       240*api*MBhat^2*Ycut^3*X1mix[1, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       60*api*MBhat^2*Ycut^4*X1mix[1, 2, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       15*api*MBhat^2*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^2*Ycut*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       90*api*MBhat^2*Ycut^2*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       60*api*MBhat^2*Ycut^3*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       15*api*MBhat^2*Ycut^4*X1mix[2, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       30*api*MBhat*X1mix[2, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       120*api*MBhat*Ycut*X1mix[2, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       180*api*MBhat*Ycut^2*X1mix[2, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       120*api*MBhat*Ycut^3*X1mix[2, 1, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       30*api*MBhat*Ycut^4*X1mix[2, 1, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       5*api*X1mix[3, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*Ycut*X1mix[3, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       30*api*Ycut^2*X1mix[3, 0, c[VL]*c[VR], Ycut, m2, mu2hat] - 
       20*api*Ycut^3*X1mix[3, 0, c[VL]*c[VR], Ycut, m2, mu2hat] + 
       5*api*Ycut^4*X1mix[3, 0, c[VL]*c[VR], Ycut, m2, mu2hat])/
      (5*(-1 + Ycut)^4))) + 
 c[VR]^2*(6*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 95*m2*MBhat + 
     55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 138*m2*MBhat^2 - 
     38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 8*m2^2*MBhat^3 - 
     45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 3*m2*MBhat^5 - 2*MBhat^6)*
    Log[m2] - 6*m2^2*(-5 - 25*m2 - 25*m2^2 - 5*m2^3 + 27*MBhat + 
     95*m2*MBhat + 55*m2^2*MBhat + 3*m2^3*MBhat - 60*MBhat^2 - 
     138*m2*MBhat^2 - 38*m2^2*MBhat^2 + 70*MBhat^3 + 94*m2*MBhat^3 + 
     8*m2^2*MBhat^3 - 45*MBhat^4 - 29*m2*MBhat^4 + 15*MBhat^5 + 
     3*m2*MBhat^5 - 2*MBhat^6)*Log[1 - Ycut] - 
   (5 - 175*m2 - 5495*m2^2 - 7875*m2^3 + 7875*m2^4 + 5495*m2^5 + 175*m2^6 - 
     5*m2^7 - 43*MBhat + 1225*m2*MBhat + 26313*m2^2*MBhat + 
     13125*m2^3*MBhat - 34125*m2^4*MBhat - 6657*m2^5*MBhat + 175*m2^6*MBhat - 
     13*m2^7*MBhat + 162*MBhat^2 - 3668*m2*MBhat^2 - 49728*m2^2*MBhat^2 + 
     11550*m2^3*MBhat^2 + 40950*m2^4*MBhat^2 + 588*m2^5*MBhat^2 + 
     168*m2^6*MBhat^2 - 22*m2^7*MBhat^2 - 348*MBhat^3 + 6076*m2*MBhat^3 + 
     45710*m2^2*MBhat^3 - 35770*m2^3*MBhat^3 - 15820*m2^4*MBhat^3 + 
     28*m2^5*MBhat^3 + 154*m2^6*MBhat^3 - 30*m2^7*MBhat^3 + 427*MBhat^4 - 
     5733*m2*MBhat^4 - 20160*m2^2*MBhat^4 + 25200*m2^3*MBhat^4 - 
     315*m2^4*MBhat^4 + 693*m2^5*MBhat^4 - 112*m2^6*MBhat^4 - 273*MBhat^5 + 
     2835*m2*MBhat^5 + 3360*m2^2*MBhat^5 - 6720*m2^3*MBhat^5 + 
     945*m2^4*MBhat^5 - 147*m2^5*MBhat^5 + 70*MBhat^6 - 560*m2*MBhat^6 + 
     560*m2^3*MBhat^6 - 70*m2^4*MBhat^6 - 15*Ycut + 525*m2*Ycut + 
     18585*m2^2*Ycut + 34125*m2^3*Ycut - 13125*m2^4*Ycut - 14385*m2^5*Ycut - 
     525*m2^6*Ycut + 15*m2^7*Ycut + 129*MBhat*Ycut - 3675*m2*MBhat*Ycut - 
     90279*m2^2*MBhat*Ycut - 79275*m2^3*MBhat*Ycut + 79275*m2^4*MBhat*Ycut + 
     18711*m2^5*MBhat*Ycut - 525*m2^6*MBhat*Ycut + 39*m2^7*MBhat*Ycut - 
     486*MBhat^2*Ycut + 11004*m2*MBhat^2*Ycut + 174384*m2^2*MBhat^2*Ycut + 
     23310*m2^3*MBhat^2*Ycut - 106890*m2^4*MBhat^2*Ycut - 
     1764*m2^5*MBhat^2*Ycut - 504*m2^6*MBhat^2*Ycut + 66*m2^7*MBhat^2*Ycut + 
     1044*MBhat^3*Ycut - 18228*m2*MBhat^3*Ycut - 166530*m2^2*MBhat^3*Ycut + 
     67830*m2^3*MBhat^3*Ycut + 44100*m2^4*MBhat^3*Ycut - 
     84*m2^5*MBhat^3*Ycut - 462*m2^6*MBhat^3*Ycut + 90*m2^7*MBhat^3*Ycut - 
     1281*MBhat^4*Ycut + 17199*m2*MBhat^4*Ycut + 79380*m2^2*MBhat^4*Ycut - 
     63420*m2^3*MBhat^4*Ycut + 945*m2^4*MBhat^4*Ycut - 
     2079*m2^5*MBhat^4*Ycut + 336*m2^6*MBhat^4*Ycut + 819*MBhat^5*Ycut - 
     8505*m2*MBhat^5*Ycut - 16380*m2^2*MBhat^5*Ycut + 
     18900*m2^3*MBhat^5*Ycut - 2835*m2^4*MBhat^5*Ycut + 
     441*m2^5*MBhat^5*Ycut - 210*MBhat^6*Ycut + 1680*m2*MBhat^6*Ycut + 
     840*m2^2*MBhat^6*Ycut - 1680*m2^3*MBhat^6*Ycut + 210*m2^4*MBhat^6*Ycut + 
     15*Ycut^2 - 525*m2*Ycut^2 - 21735*m2^2*Ycut^2 - 49875*m2^3*Ycut^2 - 
     2625*m2^4*Ycut^2 + 11235*m2^5*Ycut^2 + 525*m2^6*Ycut^2 - 
     15*m2^7*Ycut^2 - 129*MBhat*Ycut^2 + 3675*m2*MBhat*Ycut^2 + 
     107289*m2^2*MBhat*Ycut^2 + 139125*m2^3*MBhat*Ycut^2 - 
     44625*m2^4*MBhat*Ycut^2 - 16821*m2^5*MBhat*Ycut^2 + 
     525*m2^6*MBhat*Ycut^2 - 39*m2^7*MBhat*Ycut^2 + 486*MBhat^2*Ycut^2 - 
     11004*m2*MBhat^2*Ycut^2 - 212184*m2^2*MBhat^2*Ycut^2 - 
     110250*m2^3*MBhat^2*Ycut^2 + 82950*m2^4*MBhat^2*Ycut^2 + 
     1764*m2^5*MBhat^2*Ycut^2 + 504*m2^6*MBhat^2*Ycut^2 - 
     66*m2^7*MBhat^2*Ycut^2 - 1044*MBhat^3*Ycut^2 + 18228*m2*MBhat^3*Ycut^2 + 
     210630*m2^2*MBhat^3*Ycut^2 - 8610*m2^3*MBhat^3*Ycut^2 - 
     39060*m2^4*MBhat^3*Ycut^2 + 84*m2^5*MBhat^3*Ycut^2 + 
     462*m2^6*MBhat^3*Ycut^2 - 90*m2^7*MBhat^3*Ycut^2 + 1281*MBhat^4*Ycut^2 - 
     17199*m2*MBhat^4*Ycut^2 - 107730*m2^2*MBhat^4*Ycut^2 + 
     45150*m2^3*MBhat^4*Ycut^2 - 945*m2^4*MBhat^4*Ycut^2 + 
     2079*m2^5*MBhat^4*Ycut^2 - 336*m2^6*MBhat^4*Ycut^2 - 
     819*MBhat^5*Ycut^2 + 8505*m2*MBhat^5*Ycut^2 + 
     25830*m2^2*MBhat^5*Ycut^2 - 17010*m2^3*MBhat^5*Ycut^2 + 
     2835*m2^4*MBhat^5*Ycut^2 - 441*m2^5*MBhat^5*Ycut^2 + 
     210*MBhat^6*Ycut^2 - 1680*m2*MBhat^6*Ycut^2 - 2100*m2^2*MBhat^6*Ycut^2 + 
     1680*m2^3*MBhat^6*Ycut^2 - 210*m2^4*MBhat^6*Ycut^2 - 5*Ycut^3 + 
     175*m2*Ycut^3 + 9345*m2^2*Ycut^3 + 27125*m2^3*Ycut^3 + 
     11375*m2^4*Ycut^3 - 1645*m2^5*Ycut^3 - 175*m2^6*Ycut^3 + 5*m2^7*Ycut^3 + 
     43*MBhat*Ycut^3 - 1225*m2*MBhat*Ycut^3 - 47103*m2^2*MBhat*Ycut^3 - 
     86275*m2^3*MBhat*Ycut^3 - 8225*m2^4*MBhat*Ycut^3 + 
     4347*m2^5*MBhat*Ycut^3 - 175*m2^6*MBhat*Ycut^3 + 13*m2^7*MBhat*Ycut^3 - 
     162*MBhat^2*Ycut^3 + 3668*m2*MBhat^2*Ycut^3 + 
     95928*m2^2*MBhat^2*Ycut^3 + 94710*m2^3*MBhat^2*Ycut^3 - 
     11690*m2^4*MBhat^2*Ycut^3 - 588*m2^5*MBhat^2*Ycut^3 - 
     168*m2^6*MBhat^2*Ycut^3 + 22*m2^7*MBhat^2*Ycut^3 + 628*MBhat^3*Ycut^3 - 
     7476*m2*MBhat^3*Ycut^3 - 96810*m2^2*MBhat^3*Ycut^3 - 
     39410*m2^3*MBhat^3*Ycut^3 + 11060*m2^4*MBhat^3*Ycut^3 - 
     308*m2^5*MBhat^3*Ycut^3 - 154*m2^6*MBhat^3*Ycut^3 + 
     30*m2^7*MBhat^3*Ycut^3 - 1267*MBhat^4*Ycut^3 + 9093*m2*MBhat^4*Ycut^3 + 
     49770*m2^2*MBhat^4*Ycut^3 + 490*m2^3*MBhat^4*Ycut^3 - 
     525*m2^4*MBhat^4*Ycut^3 - 693*m2^5*MBhat^4*Ycut^3 + 
     112*m2^6*MBhat^4*Ycut^3 + 1113*MBhat^5*Ycut^3 - 5355*m2*MBhat^5*Ycut^3 - 
     12390*m2^2*MBhat^5*Ycut^3 + 3570*m2^3*MBhat^5*Ycut^3 - 
     945*m2^4*MBhat^5*Ycut^3 + 147*m2^5*MBhat^5*Ycut^3 - 350*MBhat^6*Ycut^3 + 
     1120*m2*MBhat^6*Ycut^3 + 1260*m2^2*MBhat^6*Ycut^3 - 
     560*m2^3*MBhat^6*Ycut^3 + 70*m2^4*MBhat^6*Ycut^3 - 525*m2^2*Ycut^4 - 
     2625*m2^3*Ycut^4 - 2625*m2^4*Ycut^4 - 525*m2^5*Ycut^4 + 
     2835*m2^2*MBhat*Ycut^4 + 9975*m2^3*MBhat*Ycut^4 + 
     5775*m2^4*MBhat*Ycut^4 + 315*m2^5*MBhat*Ycut^4 - 315*MBhat^2*Ycut^4 + 
     1575*m2*MBhat^2*Ycut^4 - 9450*m2^2*MBhat^2*Ycut^4 - 
     11340*m2^3*MBhat^2*Ycut^4 - 5565*m2^4*MBhat^2*Ycut^4 + 
     315*m2^5*MBhat^2*Ycut^4 - 105*MBhat^3*Ycut^4 + 735*m2*MBhat^3*Ycut^4 + 
     5460*m2^2*MBhat^3*Ycut^4 + 12180*m2^3*MBhat^3*Ycut^4 - 
     525*m2^4*MBhat^3*Ycut^4 + 315*m2^5*MBhat^3*Ycut^4 + 
     2205*MBhat^4*Ycut^4 - 7875*m2*MBhat^4*Ycut^4 + 
     5670*m2^2*MBhat^4*Ycut^4 - 9030*m2^3*MBhat^4*Ycut^4 + 
     1260*m2^4*MBhat^4*Ycut^4 - 2835*MBhat^5*Ycut^4 + 
     7245*m2*MBhat^5*Ycut^4 - 4410*m2^2*MBhat^5*Ycut^4 + 
     1890*m2^3*MBhat^5*Ycut^4 + 1050*MBhat^6*Ycut^4 - 
     1680*m2*MBhat^6*Ycut^4 + 420*m2^2*MBhat^6*Ycut^4 - 105*m2^2*Ycut^5 - 
     525*m2^3*Ycut^5 - 525*m2^4*Ycut^5 - 105*m2^5*Ycut^5 + 168*MBhat*Ycut^5 - 
     840*m2*MBhat*Ycut^5 + 2247*m2^2*MBhat*Ycut^5 + 315*m2^3*MBhat*Ycut^5 + 
     1995*m2^4*MBhat*Ycut^5 - 105*m2^5*MBhat*Ycut^5 + 693*MBhat^2*Ycut^5 - 
     2877*m2*MBhat^2*Ycut^5 + 3318*m2^2*MBhat^2*Ycut^5 - 
     6300*m2^3*MBhat^2*Ycut^5 + 315*m2^4*MBhat^2*Ycut^5 - 
     105*m2^5*MBhat^2*Ycut^5 - 1617*MBhat^3*Ycut^5 + 5019*m2*MBhat^3*Ycut^5 - 
     3780*m2^2*MBhat^3*Ycut^5 + 3780*m2^3*MBhat^3*Ycut^5 + 
     315*m2^4*MBhat^3*Ycut^5 - 105*m2^5*MBhat^3*Ycut^5 - 987*MBhat^4*Ycut^5 + 
     3633*m2*MBhat^4*Ycut^5 - 5670*m2^2*MBhat^4*Ycut^5 + 
     1890*m2^3*MBhat^4*Ycut^5 - 420*m2^4*MBhat^4*Ycut^5 + 
     3213*MBhat^5*Ycut^5 - 6615*m2*MBhat^5*Ycut^5 + 
     4410*m2^2*MBhat^5*Ycut^5 - 630*m2^3*MBhat^5*Ycut^5 - 
     1470*MBhat^6*Ycut^5 + 1680*m2*MBhat^6*Ycut^5 - 420*m2^2*MBhat^6*Ycut^5 - 
     35*Ycut^6 + 175*m2*Ycut^6 - 385*m2^2*Ycut^6 + 175*m2^3*Ycut^6 - 
     350*m2^4*Ycut^6 - 539*MBhat*Ycut^6 + 2135*m2*MBhat*Ycut^6 - 
     2961*m2^2*MBhat*Ycut^6 + 2695*m2^3*MBhat*Ycut^6 - 70*m2^4*MBhat*Ycut^6 + 
     126*MBhat^2*Ycut^6 - 574*m2*MBhat^2*Ycut^6 + 546*m2^2*MBhat^2*Ycut^6 - 
     1680*m2^3*MBhat^2*Ycut^6 - 70*m2^4*MBhat^2*Ycut^6 + 
     2296*MBhat^3*Ycut^6 - 5502*m2*MBhat^3*Ycut^6 + 
     4480*m2^2*MBhat^3*Ycut^6 - 70*m2^4*MBhat^3*Ycut^6 - 
     1729*MBhat^4*Ycut^6 + 2751*m2*MBhat^4*Ycut^6 - 
     1260*m2^2*MBhat^4*Ycut^6 - 280*m2^3*MBhat^4*Ycut^6 - 
     1029*MBhat^5*Ycut^6 + 1575*m2*MBhat^5*Ycut^6 - 420*m2^2*MBhat^5*Ycut^6 + 
     910*MBhat^6*Ycut^6 - 560*m2*MBhat^6*Ycut^6 + 135*Ycut^7 - 
     525*m2*Ycut^7 + 735*m2^2*Ycut^7 - 525*m2^3*Ycut^7 + 519*MBhat*Ycut^7 - 
     1365*m2*MBhat*Ycut^7 + 1071*m2^2*MBhat*Ycut^7 + 315*m2^3*MBhat*Ycut^7 - 
     1296*MBhat^2*Ycut^7 + 3234*m2*MBhat^2*Ycut^7 - 
     2646*m2^2*MBhat^2*Ycut^7 - 366*MBhat^3*Ycut^7 + 42*m2*MBhat^3*Ycut^7 + 
     840*m2^2*MBhat^3*Ycut^7 + 1659*MBhat^4*Ycut^7 - 1701*m2*MBhat^4*Ycut^7 - 
     441*MBhat^5*Ycut^7 + 315*m2*MBhat^5*Ycut^7 - 210*MBhat^6*Ycut^7 - 
     195*Ycut^8 + 525*m2*Ycut^8 - 420*m2^2*Ycut^8 - 3*MBhat*Ycut^8 - 
     315*m2*MBhat*Ycut^8 + 588*m2^2*MBhat*Ycut^8 + 927*MBhat^2*Ycut^8 - 
     1113*m2*MBhat^2*Ycut^8 - 168*m2^2*MBhat^2*Ycut^8 - 813*MBhat^3*Ycut^8 + 
     1071*m2*MBhat^3*Ycut^8 - 168*MBhat^4*Ycut^8 - 168*m2*MBhat^4*Ycut^8 + 
     252*MBhat^5*Ycut^8 + 125*Ycut^9 - 175*m2*Ycut^9 - 235*MBhat*Ycut^9 + 
     385*m2*MBhat*Ycut^9 - 45*MBhat^2*Ycut^9 - 245*m2*MBhat^2*Ycut^9 + 
     295*MBhat^3*Ycut^9 + 35*m2*MBhat^3*Ycut^9 - 140*MBhat^4*Ycut^9 - 
     30*Ycut^10 + 90*MBhat*Ycut^10 - 90*MBhat^2*Ycut^10 + 
     30*MBhat^3*Ycut^10 + 70*api*MBhat^6*X1mix[0, 0, c[VR]^2, Ycut, m2, 
       mu2hat] - 210*api*MBhat^6*Ycut*X1mix[0, 0, c[VR]^2, Ycut, m2, 
       mu2hat] + 210*api*MBhat^6*Ycut^2*X1mix[0, 0, c[VR]^2, Ycut, m2, 
       mu2hat] - 70*api*MBhat^6*Ycut^3*X1mix[0, 0, c[VR]^2, Ycut, m2, 
       mu2hat] - 420*api*MBhat^5*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     1260*api*MBhat^5*Ycut*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     1260*api*MBhat^5*Ycut^2*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat^5*Ycut^3*X1mix[0, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^4*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     2520*api*MBhat^4*Ycut*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] + 
     2520*api*MBhat^4*Ycut^2*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat^4*Ycut^3*X1mix[0, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     560*api*MBhat^3*X1mix[0, 3, c[VR]^2, Ycut, m2, mu2hat] + 
     1680*api*MBhat^3*Ycut*X1mix[0, 3, c[VR]^2, Ycut, m2, mu2hat] - 
     1680*api*MBhat^3*Ycut^2*X1mix[0, 3, c[VR]^2, Ycut, m2, mu2hat] + 
     560*api*MBhat^3*Ycut^3*X1mix[0, 3, c[VR]^2, Ycut, m2, mu2hat] + 
     210*api*MBhat^4*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     630*api*MBhat^4*Ycut*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     630*api*MBhat^4*Ycut^2*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     210*api*MBhat^4*Ycut^3*X1mix[1, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat^3*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     2520*api*MBhat^3*Ycut*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     2520*api*MBhat^3*Ycut^2*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^3*Ycut^3*X1mix[1, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     840*api*MBhat^2*X1mix[1, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     2520*api*MBhat^2*Ycut*X1mix[1, 2, c[VR]^2, Ycut, m2, mu2hat] + 
     2520*api*MBhat^2*Ycut^2*X1mix[1, 2, c[VR]^2, Ycut, m2, mu2hat] - 
     840*api*MBhat^2*Ycut^3*X1mix[1, 2, c[VR]^2, Ycut, m2, mu2hat] + 
     210*api*MBhat^2*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     630*api*MBhat^2*Ycut*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     630*api*MBhat^2*Ycut^2*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     210*api*MBhat^2*Ycut^3*X1mix[2, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     420*api*MBhat*X1mix[2, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     1260*api*MBhat*Ycut*X1mix[2, 1, c[VR]^2, Ycut, m2, mu2hat] - 
     1260*api*MBhat*Ycut^2*X1mix[2, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     420*api*MBhat*Ycut^3*X1mix[2, 1, c[VR]^2, Ycut, m2, mu2hat] + 
     70*api*X1mix[3, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     210*api*Ycut*X1mix[3, 0, c[VR]^2, Ycut, m2, mu2hat] + 
     210*api*Ycut^2*X1mix[3, 0, c[VR]^2, Ycut, m2, mu2hat] - 
     70*api*Ycut^3*X1mix[3, 0, c[VR]^2, Ycut, m2, mu2hat])/(70*(-1 + Ycut)^3))
