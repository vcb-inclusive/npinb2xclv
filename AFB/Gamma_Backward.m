((1 - 2*Sqrt[m2] + m2 - Q2hatcut)^2*(1 + 4*Sqrt[m2] - 10*m2 + 4*m2^(3/2) + 
    m2^2 + 2*Q2hatcut + 12*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut - 3*Q2hatcut^2))/
  8 + (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
   (1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - Q2hatcut^2 - 
    m2*Q2hatcut^2 + Q2hatcut^3))/2 - 
 (2*Sqrt[m2]*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
   Sqrt[(1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2)/m2]*
   (4 - 15*Sqrt[m2] + 20*m2 - 10*m2^(3/2) + m2^(5/2) - Sqrt[m2]*Q2hatcut^2)*
   rhoLS*c[SR]*c[T])/(mb^3*(1 - 2*Sqrt[m2] + m2 - Q2hatcut)*
   (1 + 2*Sqrt[m2] + m2 - Q2hatcut)) + 
 6*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
       2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
     Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
 rhoLS*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
      (-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 8*m2*Q2hatcut - 
       5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 5*Q2hatcut^3))/
     mb^3 - (5 - 20*m2 + 30*m2^2 - 20*m2^3 + 5*m2^4 + 2*Q2hatcut^2 + 
     12*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 + 8*Q2hatcut^3 + 
     40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(16*mb^3) - 
   (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
        Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
    mb^3) + rhoD*((-35 + 128*Sqrt[m2] - 140*m2 + 70*m2^2 - 28*m2^3 + 5*m2^4 - 
     6*Q2hatcut^2 + 36*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 - 8*Q2hatcut^3 + 
     40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(48*mb^3) + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
     (77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 151*Q2hatcut - 
      122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 10*m2^3*Q2hatcut - 
      15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 60*m2*Q2hatcut^2 + 
      28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 2*Q2hatcut^3 - 
      38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 17*Q2hatcut^4 - 
      15*m2*Q2hatcut^4 + 5*Q2hatcut^5))/
    (12*mb^3*(1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(1 + 2*Sqrt[m2] + m2 - 
      Q2hatcut)) - 
   ((4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
        Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
    mb^3) + 
 mupi*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
      (1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - Q2hatcut^2 - 
       m2*Q2hatcut^2 + Q2hatcut^3))/mb^2 - 
   ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(35 - 122*Sqrt[m2] + 141*m2 - 
      44*m2^(3/2) - 19*m2^2 + 6*m2^(5/2) + 3*m2^3 + 35*Q2hatcut - 
      52*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut + 12*m2^(3/2)*Q2hatcut + 
      3*m2^2*Q2hatcut + 17*Q2hatcut^2 - 18*Sqrt[m2]*Q2hatcut^2 - 
      15*m2*Q2hatcut^2 + 9*Q2hatcut^3))/(48*mb^2) - 
   (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
        Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
    mb^2) + 
 muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
     (-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 8*m2*Q2hatcut - 
      5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 5*Q2hatcut^3))/
    (4*mb^2) + ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(-65 + 62*Sqrt[m2] + 
      129*m2 - 124*m2^(3/2) - 47*m2^2 + 30*m2^(5/2) + 15*m2^3 - 65*Q2hatcut - 
      68*Sqrt[m2]*Q2hatcut + 58*m2*Q2hatcut + 60*m2^(3/2)*Q2hatcut + 
      15*m2^2*Q2hatcut - 11*Q2hatcut^2 - 90*Sqrt[m2]*Q2hatcut^2 - 
      75*m2*Q2hatcut^2 + 45*Q2hatcut^3))/(48*mb^2) + 
   (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
        Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
    mb^2) + 
 c[SL]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
     (1 - 7*m2 - 7*m2^2 + m2^3 + Q2hatcut - 4*m2*Q2hatcut + m2^2*Q2hatcut - 
      5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 3*Q2hatcut^3))/8 + 
   (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
        Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
    2 + rhoLS*(-1/16*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 
         12*m2*Q2hatcut + 5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 25*m2*Q2hatcut^2 + 
         15*Q2hatcut^3))/mb^3 - 
     (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      (4*mb^3)) + rhoD*((197 - 381*m2 + 184*m2^2 - 8*m2^3 + 3*m2^4 + 5*m2^5 - 
       389*Q2hatcut - 402*m2*Q2hatcut - 98*m2^2*Q2hatcut - 18*m2^3*Q2hatcut - 
       5*m2^4*Q2hatcut + 162*Q2hatcut^2 - 124*m2*Q2hatcut^2 + 
       68*m2^2*Q2hatcut^2 - 30*m2^3*Q2hatcut^2 - 26*Q2hatcut^3 - 
       94*m2*Q2hatcut^3 + 70*m2^2*Q2hatcut^3 + 41*Q2hatcut^4 - 
       55*m2*Q2hatcut^4 + 15*Q2hatcut^5)/
      (48*mb^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]) - ((8 + 8*m2 + 3*m2^2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      (4*mb^3)) + 
   muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 12*m2*Q2hatcut + 
        5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 25*m2*Q2hatcut^2 + 15*Q2hatcut^3))/
      (16*mb^2) + (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut + 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
         (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])])/(4*mb^2)) + 
   mupi*(-1/16*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 + Q2hatcut - 4*m2*Q2hatcut + 
         m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 3*Q2hatcut^3))/
       mb^2 - (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      (4*mb^2))) + 
 c[SR]^2*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
     (1 - 7*m2 - 7*m2^2 + m2^3 + Q2hatcut - 4*m2*Q2hatcut + m2^2*Q2hatcut - 
      5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 3*Q2hatcut^3))/8 + 
   (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
        Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
    2 + rhoLS*(-1/16*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 
         12*m2*Q2hatcut + 5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 25*m2*Q2hatcut^2 + 
         15*Q2hatcut^3))/mb^3 - 
     (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      (4*mb^3)) + rhoD*((197 - 381*m2 + 184*m2^2 - 8*m2^3 + 3*m2^4 + 5*m2^5 - 
       389*Q2hatcut - 402*m2*Q2hatcut - 98*m2^2*Q2hatcut - 18*m2^3*Q2hatcut - 
       5*m2^4*Q2hatcut + 162*Q2hatcut^2 - 124*m2*Q2hatcut^2 + 
       68*m2^2*Q2hatcut^2 - 30*m2^3*Q2hatcut^2 - 26*Q2hatcut^3 - 
       94*m2*Q2hatcut^3 + 70*m2^2*Q2hatcut^3 + 41*Q2hatcut^4 - 
       55*m2*Q2hatcut^4 + 15*Q2hatcut^5)/
      (48*mb^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]) - ((8 + 8*m2 + 3*m2^2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      (4*mb^3)) + 
   muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (13 + 45*m2 - 27*m2^2 + 5*m2^3 + 13*Q2hatcut - 12*m2*Q2hatcut + 
        5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 25*m2*Q2hatcut^2 + 15*Q2hatcut^3))/
      (16*mb^2) + (3*(-4 + m2)*m2*Log[(1 + m2 - Q2hatcut + 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
         (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])])/(4*mb^2)) + 
   mupi*(-1/16*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 + Q2hatcut - 4*m2*Q2hatcut + 
         m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 3*Q2hatcut^3))/
       mb^2 - (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      (4*mb^2))) + 
 c[VR]^2*(-1/8*((1 - 2*Sqrt[m2] + m2 - Q2hatcut)^2*(1 + 4*Sqrt[m2] - 10*m2 + 
      4*m2^(3/2) + m2^2 + 2*Q2hatcut + 12*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut - 
      3*Q2hatcut^2)) + (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
       Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
      Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3))/2 + 
   6*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
       Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
   rhoLS*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
         8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
         5*Q2hatcut^3))/mb^3 + (5 - 20*m2 + 30*m2^2 - 20*m2^3 + 5*m2^4 + 
       2*Q2hatcut^2 + 12*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 + 8*Q2hatcut^3 + 
       40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(16*mb^3) - 
     (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + rhoD*(-1/48*(-35 + 128*Sqrt[m2] - 140*m2 + 70*m2^2 - 28*m2^3 + 
        5*m2^4 - 6*Q2hatcut^2 + 36*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 - 
        8*Q2hatcut^3 + 40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/mb^3 + 
     (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 151*Q2hatcut - 
        122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 10*m2^3*Q2hatcut - 
        15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 60*m2*Q2hatcut^2 + 
        28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 2*Q2hatcut^3 - 
        38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 17*Q2hatcut^4 - 
        15*m2*Q2hatcut^4 + 5*Q2hatcut^5))/(12*mb^3*(1 - 2*Sqrt[m2] + m2 - 
        Q2hatcut)*(1 + 2*Sqrt[m2] + m2 - Q2hatcut)) - 
     ((4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + 
   mupi*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
         Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3))/mb^2 + 
     ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(35 - 122*Sqrt[m2] + 141*m2 - 
        44*m2^(3/2) - 19*m2^2 + 6*m2^(5/2) + 3*m2^3 + 35*Q2hatcut - 
        52*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut + 12*m2^(3/2)*Q2hatcut + 
        3*m2^2*Q2hatcut + 17*Q2hatcut^2 - 18*Sqrt[m2]*Q2hatcut^2 - 
        15*m2*Q2hatcut^2 + 9*Q2hatcut^3))/(48*mb^2) - 
     (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2) + muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
        8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
        5*Q2hatcut^3))/(4*mb^2) - ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*
       (-65 + 62*Sqrt[m2] + 129*m2 - 124*m2^(3/2) - 47*m2^2 + 30*m2^(5/2) + 
        15*m2^3 - 65*Q2hatcut - 68*Sqrt[m2]*Q2hatcut + 58*m2*Q2hatcut + 
        60*m2^(3/2)*Q2hatcut + 15*m2^2*Q2hatcut - 11*Q2hatcut^2 - 
        90*Sqrt[m2]*Q2hatcut^2 - 75*m2*Q2hatcut^2 + 45*Q2hatcut^3))/
      (48*mb^2) + (3*m2^2*Log[(1 + m2 - Q2hatcut + 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
         (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])])/mb^2)) + 
 c[VL]^2*(((1 - 2*Sqrt[m2] + m2 - Q2hatcut)^2*(1 + 4*Sqrt[m2] - 10*m2 + 
      4*m2^(3/2) + m2^2 + 2*Q2hatcut + 12*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut - 
      3*Q2hatcut^2))/8 + 
   (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
     (1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - Q2hatcut^2 - 
      m2*Q2hatcut^2 + Q2hatcut^3))/2 + 
   6*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
       Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
   rhoLS*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
         8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
         5*Q2hatcut^3))/mb^3 - (5 - 20*m2 + 30*m2^2 - 20*m2^3 + 5*m2^4 + 
       2*Q2hatcut^2 + 12*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 + 8*Q2hatcut^3 + 
       40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(16*mb^3) - 
     (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + rhoD*((-35 + 128*Sqrt[m2] - 140*m2 + 70*m2^2 - 28*m2^3 + 
       5*m2^4 - 6*Q2hatcut^2 + 36*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 - 
       8*Q2hatcut^3 + 40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(48*mb^3) + 
     (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 151*Q2hatcut - 
        122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 10*m2^3*Q2hatcut - 
        15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 60*m2*Q2hatcut^2 + 
        28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 2*Q2hatcut^3 - 
        38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 17*Q2hatcut^4 - 
        15*m2*Q2hatcut^4 + 5*Q2hatcut^5))/(12*mb^3*(1 - 2*Sqrt[m2] + m2 - 
        Q2hatcut)*(1 + 2*Sqrt[m2] + m2 - Q2hatcut)) - 
     ((4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + 
   mupi*(-1/4*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
         Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3))/mb^2 - 
     ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(35 - 122*Sqrt[m2] + 141*m2 - 
        44*m2^(3/2) - 19*m2^2 + 6*m2^(5/2) + 3*m2^3 + 35*Q2hatcut - 
        52*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut + 12*m2^(3/2)*Q2hatcut + 
        3*m2^2*Q2hatcut + 17*Q2hatcut^2 - 18*Sqrt[m2]*Q2hatcut^2 - 
        15*m2*Q2hatcut^2 + 9*Q2hatcut^3))/(48*mb^2) - 
     (3*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2) + muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
        8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
        5*Q2hatcut^3))/(4*mb^2) + ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*
       (-65 + 62*Sqrt[m2] + 129*m2 - 124*m2^(3/2) - 47*m2^2 + 30*m2^(5/2) + 
        15*m2^3 - 65*Q2hatcut - 68*Sqrt[m2]*Q2hatcut + 58*m2*Q2hatcut + 
        60*m2^(3/2)*Q2hatcut + 15*m2^2*Q2hatcut - 11*Q2hatcut^2 - 
        90*Sqrt[m2]*Q2hatcut^2 - 75*m2*Q2hatcut^2 + 45*Q2hatcut^3))/
      (48*mb^2) + (3*m2^2*Log[(1 + m2 - Q2hatcut + 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
         (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])])/mb^2)) + 
 c[T]^2*(2*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
    (3 - 21*m2 - 21*m2^2 + 3*m2^3 - 5*Q2hatcut + 4*m2*Q2hatcut - 
     5*m2^2*Q2hatcut + Q2hatcut^2 + m2*Q2hatcut^2 + Q2hatcut^3) + 
   72*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
       Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
   rhoLS*(-((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
        (-25 - 25*m2 - 49*m2^2 + 15*m2^3 - Q2hatcut + 12*m2*Q2hatcut - 
         25*m2^2*Q2hatcut + 5*Q2hatcut^2 + 5*m2*Q2hatcut^2 + 5*Q2hatcut^3))/
       mb^3) - (12*m2*(4 + 3*m2)*Log[(1 + m2 - Q2hatcut + 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
         (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])])/mb^3) + 
   rhoD*((111 - 279*m2 + 264*m2^2 - 120*m2^3 + 9*m2^4 + 15*m2^5 - 
       215*Q2hatcut - 86*m2*Q2hatcut - 54*m2^2*Q2hatcut - 22*m2^3*Q2hatcut - 
       55*m2^4*Q2hatcut + 38*Q2hatcut^2 - 116*m2*Q2hatcut^2 + 
       44*m2^2*Q2hatcut^2 + 70*m2^3*Q2hatcut^2 + 34*Q2hatcut^3 - 
       58*m2*Q2hatcut^3 - 30*m2^2*Q2hatcut^3 + 27*Q2hatcut^4 - 
       5*m2*Q2hatcut^4 + 5*Q2hatcut^5)/
      (3*mb^3*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]) - (4*(8 - 8*m2 + 9*m2^2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + 
   mupi*(-((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
        (3 - 21*m2 - 21*m2^2 + 3*m2^3 - 5*Q2hatcut + 4*m2*Q2hatcut - 
         5*m2^2*Q2hatcut + Q2hatcut^2 + m2*Q2hatcut^2 + Q2hatcut^3))/mb^2) - 
     (36*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2) + muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-25 - 25*m2 - 49*m2^2 + 15*m2^3 - Q2hatcut + 
        12*m2*Q2hatcut - 25*m2^2*Q2hatcut + 5*Q2hatcut^2 + 5*m2*Q2hatcut^2 + 
        5*Q2hatcut^3))/mb^2 + (12*m2*(4 + 3*m2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2)) + c[VR]*(-2*Sqrt[m2]*(1 + 10*m2 + m2^2 + Q2hatcut + 
     m2*Q2hatcut - 2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
      2*m2*Q2hatcut + Q2hatcut^2] + 12*m2^(3/2)*(1 + m2)*
    Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
       Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
   rhoD*(-((Sqrt[m2]*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(25 - 64*m2 + 54*m2^2 - 16*m2^3 + m2^4 - 45*Q2hatcut - 
         35*m2*Q2hatcut + 37*m2^2*Q2hatcut - 5*m2^3*Q2hatcut + 9*Q2hatcut^2 - 
         14*m2*Q2hatcut^2 + 9*m2^2*Q2hatcut^2 - 7*Q2hatcut^3 - 
         7*m2*Q2hatcut^3 + 2*Q2hatcut^4))/(mb^3*(1 - 2*Sqrt[m2] + m2 - 
         Q2hatcut)*(1 + 2*Sqrt[m2] + m2 - Q2hatcut))) - 
     (6*(-2 + m2)*Sqrt[m2]*(1 + m2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + rhoLS*((Sqrt[m2]*(13 - 14*m2 + 13*m2^2 + Q2hatcut + 
        m2*Q2hatcut - 14*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2])/(3*mb^3) - 
     (2*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + mupi*((Sqrt[m2]*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 
        2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2])/mb^2 - (6*m2^(3/2)*(1 + m2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2) + muG*(-1/3*(Sqrt[m2]*(13 - 14*m2 + 13*m2^2 + Q2hatcut + 
         m2*Q2hatcut - 14*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
          2*m2*Q2hatcut + Q2hatcut^2])/mb^2 + 
     (2*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*
       Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2)) + 
 c[SL]*((((1 - 2*Sqrt[m2] + m2 - Q2hatcut)^2*(1 + 4*Sqrt[m2] - 10*m2 + 
        4*m2^(3/2) + m2^2 + 2*Q2hatcut + 12*Sqrt[m2]*Q2hatcut + 
        2*m2*Q2hatcut - 3*Q2hatcut^2))/4 - 
     (mupi*(1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(35 - 122*Sqrt[m2] + 141*m2 - 
        44*m2^(3/2) - 19*m2^2 + 6*m2^(5/2) + 3*m2^3 + 35*Q2hatcut - 
        52*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut + 12*m2^(3/2)*Q2hatcut + 
        3*m2^2*Q2hatcut + 17*Q2hatcut^2 - 18*Sqrt[m2]*Q2hatcut^2 - 
        15*m2*Q2hatcut^2 + 9*Q2hatcut^3))/(24*mb^2) + 
     (muG*(1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(5 - 54*Sqrt[m2] + 107*m2 - 
        52*m2^(3/2) - 21*m2^2 + 10*m2^(5/2) + 5*m2^3 + 5*Q2hatcut - 
        44*Sqrt[m2]*Q2hatcut + 14*m2*Q2hatcut + 20*m2^(3/2)*Q2hatcut + 
        5*m2^2*Q2hatcut + 7*Q2hatcut^2 - 30*Sqrt[m2]*Q2hatcut^2 - 
        25*m2*Q2hatcut^2 + 15*Q2hatcut^3))/(8*mb^2) + 
     ((-35 + 128*Sqrt[m2] - 140*m2 + 70*m2^2 - 28*m2^3 + 5*m2^4 - 
        6*Q2hatcut^2 + 36*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 - 8*Q2hatcut^3 + 
        40*m2*Q2hatcut^3 - 15*Q2hatcut^4)*rhoD)/(24*mb^3) - 
     ((5 - 20*m2 + 30*m2^2 - 20*m2^3 + 5*m2^4 + 2*Q2hatcut^2 + 
        12*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 + 8*Q2hatcut^3 + 
        40*m2*Q2hatcut^3 - 15*Q2hatcut^4)*rhoLS)/(8*mb^3))*c[T] + 
   c[SR]*(Sqrt[m2]*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 2*Q2hatcut^2)*
      Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2] - 
     6*m2^(3/2)*(1 + m2)*Log[(1 + m2 - Q2hatcut + 
         Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
        (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2])] + 
     rhoLS*((-3*Sqrt[m2]*(-11 - 2*m2 + m2^2 - 3*Q2hatcut + m2*Q2hatcut - 
          2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2])/(2*mb^3) + (3*Sqrt[m2]*(-2 - 5*m2 + m2^2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^3) + 
     rhoD*(-1/6*(Sqrt[m2]*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2]*(-143 + 296*m2 - 162*m2^2 + 8*m2^3 + m2^4 + 
           263*Q2hatcut + 313*m2*Q2hatcut - 47*m2^2*Q2hatcut - 
           m2^3*Q2hatcut - 75*Q2hatcut^2 + 34*m2*Q2hatcut^2 - 
           3*m2^2*Q2hatcut^2 + 5*Q2hatcut^3 + 5*m2*Q2hatcut^3 - 
           2*Q2hatcut^4))/(mb^3*(1 - 2*Sqrt[m2] + m2 - Q2hatcut)*
          (1 + 2*Sqrt[m2] + m2 - Q2hatcut)) + 
       (Sqrt[m2]*(-10 - 15*m2 + 3*m2^2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^3) + 
     mupi*(-1/2*(Sqrt[m2]*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 
           2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2])/mb^2 + (3*m2^(3/2)*(1 + m2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^2) + 
     muG*((3*Sqrt[m2]*(-11 - 2*m2 + m2^2 - 3*Q2hatcut + m2*Q2hatcut - 
          2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2])/(2*mb^2) - (3*Sqrt[m2]*(-2 - 5*m2 + m2^2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^2))) + 
 c[VL]*(((1 - 2*Sqrt[m2] + m2 - Q2hatcut)^2*(1 + 4*Sqrt[m2] - 10*m2 + 
      4*m2^(3/2) + m2^2 + 2*Q2hatcut + 12*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut - 
      3*Q2hatcut^2))/4 + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
      Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
     Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3) + 
   12*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
         2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
       Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
   rhoLS*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
         8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
         5*Q2hatcut^3))/mb^3 - (5 - 20*m2 + 30*m2^2 - 20*m2^3 + 5*m2^4 + 
       2*Q2hatcut^2 + 12*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 + 8*Q2hatcut^3 + 
       40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(8*mb^3) - 
     (6*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + rhoD*((-35 + 128*Sqrt[m2] - 140*m2 + 70*m2^2 - 28*m2^3 + 
       5*m2^4 - 6*Q2hatcut^2 + 36*m2*Q2hatcut^2 - 30*m2^2*Q2hatcut^2 - 
       8*Q2hatcut^3 + 40*m2*Q2hatcut^3 - 15*Q2hatcut^4)/(24*mb^3) + 
     (Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2]*
       (77 - 165*m2 + 112*m2^2 - 32*m2^3 + 3*m2^4 + 5*m2^5 - 151*Q2hatcut - 
        122*m2*Q2hatcut - 38*m2^2*Q2hatcut - 10*m2^3*Q2hatcut - 
        15*m2^4*Q2hatcut + 50*Q2hatcut^2 - 60*m2*Q2hatcut^2 + 
        28*m2^2*Q2hatcut^2 + 10*m2^3*Q2hatcut^2 + 2*Q2hatcut^3 - 
        38*m2*Q2hatcut^3 + 10*m2^2*Q2hatcut^3 + 17*Q2hatcut^4 - 
        15*m2*Q2hatcut^4 + 5*Q2hatcut^5))/(6*mb^3*(1 - 2*Sqrt[m2] + m2 - 
        Q2hatcut)*(1 + 2*Sqrt[m2] + m2 - Q2hatcut)) - 
     (2*(4 + 3*m2^2)*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 
            2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^3) + 
   mupi*(-1/2*(Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
          Q2hatcut^2]*(1 - 7*m2 - 7*m2^2 + m2^3 - Q2hatcut - m2^2*Q2hatcut - 
         Q2hatcut^2 - m2*Q2hatcut^2 + Q2hatcut^3))/mb^2 - 
     ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*(35 - 122*Sqrt[m2] + 141*m2 - 
        44*m2^(3/2) - 19*m2^2 + 6*m2^(5/2) + 3*m2^3 + 35*Q2hatcut - 
        52*Sqrt[m2]*Q2hatcut + 2*m2*Q2hatcut + 12*m2^(3/2)*Q2hatcut + 
        3*m2^2*Q2hatcut + 17*Q2hatcut^2 - 18*Sqrt[m2]*Q2hatcut^2 - 
        15*m2*Q2hatcut^2 + 9*Q2hatcut^3))/(24*mb^2) - 
     (6*m2^2*Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])])/
      mb^2) + muG*((Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
         Q2hatcut^2]*(-3 + 5*m2 - 19*m2^2 + 5*m2^3 - 5*Q2hatcut + 
        8*m2*Q2hatcut - 5*m2^2*Q2hatcut - 5*Q2hatcut^2 - 5*m2*Q2hatcut^2 + 
        5*Q2hatcut^3))/(2*mb^2) + ((1 - 2*Sqrt[m2] + m2 - Q2hatcut)*
       (-65 + 62*Sqrt[m2] + 129*m2 - 124*m2^(3/2) - 47*m2^2 + 30*m2^(5/2) + 
        15*m2^3 - 65*Q2hatcut - 68*Sqrt[m2]*Q2hatcut + 58*m2*Q2hatcut + 
        60*m2^(3/2)*Q2hatcut + 15*m2^2*Q2hatcut - 11*Q2hatcut^2 - 
        90*Sqrt[m2]*Q2hatcut^2 - 75*m2*Q2hatcut^2 + 45*Q2hatcut^3))/
      (24*mb^2) + (6*m2^2*Log[(1 + m2 - Q2hatcut + 
          Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
         (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
            2*m2*Q2hatcut + Q2hatcut^2])])/mb^2) + 
   c[VR]*(-2*Sqrt[m2]*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 
       2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
        Q2hatcut^2] + 12*m2^(3/2)*(1 + m2)*
      Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
           2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
         Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])] + 
     rhoD*(-((Sqrt[m2]*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2]*(25 - 64*m2 + 54*m2^2 - 16*m2^3 + m2^4 - 
           45*Q2hatcut - 35*m2*Q2hatcut + 37*m2^2*Q2hatcut - 
           5*m2^3*Q2hatcut + 9*Q2hatcut^2 - 14*m2*Q2hatcut^2 + 
           9*m2^2*Q2hatcut^2 - 7*Q2hatcut^3 - 7*m2*Q2hatcut^3 + 
           2*Q2hatcut^4))/(mb^3*(1 - 2*Sqrt[m2] + m2 - Q2hatcut)*
          (1 + 2*Sqrt[m2] + m2 - Q2hatcut))) - 
       (6*(-2 + m2)*Sqrt[m2]*(1 + m2)*Log[(1 + m2 - Q2hatcut + 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + Q2hatcut^2])/
           (1 + m2 - Q2hatcut - Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])])/mb^3) + 
     rhoLS*((Sqrt[m2]*(13 - 14*m2 + 13*m2^2 + Q2hatcut + m2*Q2hatcut - 
          14*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2])/(3*mb^3) - (2*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^3) + 
     mupi*((Sqrt[m2]*(1 + 10*m2 + m2^2 + Q2hatcut + m2*Q2hatcut - 
          2*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
           Q2hatcut^2])/mb^2 - (6*m2^(3/2)*(1 + m2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^2) + 
     muG*(-1/3*(Sqrt[m2]*(13 - 14*m2 + 13*m2^2 + Q2hatcut + m2*Q2hatcut - 
           14*Q2hatcut^2)*Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
            Q2hatcut^2])/mb^2 + (2*Sqrt[m2]*(2 - 3*m2 + 3*m2^2)*
         Log[(1 + m2 - Q2hatcut + Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 
              2*m2*Q2hatcut + Q2hatcut^2])/(1 + m2 - Q2hatcut - 
            Sqrt[1 - 2*m2 + m2^2 - 2*Q2hatcut - 2*m2*Q2hatcut + 
              Q2hatcut^2])])/mb^2))) + 
 api*(X1AFBBackward[SM^2, Q2hatcut, m2, mu2hat] + 
   c[SL]^2*X1AFBBackward[c[SL]^2, Q2hatcut, m2, mu2hat] + 
   c[SL]*c[SR]*X1AFBBackward[c[SL]*c[SR], Q2hatcut, m2, mu2hat] + 
   c[SR]^2*X1AFBBackward[c[SR]^2, Q2hatcut, m2, mu2hat] + 
   c[SL]*c[T]*X1AFBBackward[c[SL]*c[T], Q2hatcut, m2, mu2hat] + 
   c[T]^2*X1AFBBackward[c[T]^2, Q2hatcut, m2, mu2hat] + 
   c[VL]*X1AFBBackward[SM*c[VL], Q2hatcut, m2, mu2hat] + 
   c[VL]^2*X1AFBBackward[c[VL]^2, Q2hatcut, m2, mu2hat] + 
   c[VR]*X1AFBBackward[SM*c[VR], Q2hatcut, m2, mu2hat] + 
   c[VL]*c[VR]*X1AFBBackward[c[VL]*c[VR], Q2hatcut, m2, mu2hat] + 
   c[VR]^2*X1AFBBackward[c[VR]^2, Q2hatcut, m2, mu2hat])
